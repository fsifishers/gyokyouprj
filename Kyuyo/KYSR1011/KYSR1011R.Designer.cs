﻿namespace jp.co.fsi.ky.kysr1011
{
    /// <summary>
    /// KYSR1011R の帳票
    /// </summary>
    partial class KYSR1011R
    {
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(KYSR1011R));
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.ボックス218 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ITEM101 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ボックス41 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス40 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス42 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス299 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ラベル338 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ボックス170 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス166 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス162 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス158 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス154 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス150 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス146 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス142 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス138 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス133 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス135 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ラベル174 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ボックス136 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス225 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス221 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス216 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス229 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス233 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス237 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス241 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス245 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス249 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス253 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス215 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス220 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス224 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス228 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス232 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス236 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス240 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス244 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス248 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス252 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ITEM100 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.shape1 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.shape2 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.shape3 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.shape4 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.shape5 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.shape6 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.shape7 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.shape8 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.shape9 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ITEM120 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM119 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM118 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM113 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM116 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM115 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM117 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM114 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM112 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM099 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM098 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM097 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM096 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM095 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM094 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM093 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM092 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM091 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM111 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ボックス254 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス250 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス246 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス242 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス238 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス234 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス230 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス226 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス222 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ITEM110 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM109 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM108 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM107 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM106 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM105 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM104 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM103 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM102 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ボックス217 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ラベル256 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ITEM051 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ボックス43 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ITEM001 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM006 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ボックス44 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス45 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ラベル0 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル37 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ラベル38 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ITEM005 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM002 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM003 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM004 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル49 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ITEM007 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ボックス134 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス137 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス139 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス140 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス141 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス143 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス144 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス145 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス147 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス148 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス149 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス151 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス152 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス153 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス155 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス156 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス157 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス159 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス160 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス161 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス163 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス164 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス165 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス167 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス168 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス169 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス171 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス172 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス173 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ITEM052 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM053 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM054 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM055 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM056 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM057 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM058 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM059 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM060 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM071 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM072 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM073 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM074 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM075 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM076 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM077 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM078 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM079 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM080 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM081 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM082 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM083 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM084 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM085 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM086 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM087 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM088 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM089 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM090 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ボックス219 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス223 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス227 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス231 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス235 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス239 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス243 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス247 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス251 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス255 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ITEM121 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM122 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM123 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM124 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM125 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM126 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM127 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM128 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM129 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM130 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ボックス297 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス298 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス302 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス303 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス306 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス307 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス310 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス311 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス314 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス315 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス318 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス319 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス322 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス323 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス326 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス327 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス330 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス331 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス334 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ボックス335 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ITEM131 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM132 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM133 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM134 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM135 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM136 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM137 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM138 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM139 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM140 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM141 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM142 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM143 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM144 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM145 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM146 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM147 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM148 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM149 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM150 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM070 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM069 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM068 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM067 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM066 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM065 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM064 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM063 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM062 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM061 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM101)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル338)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル174)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM100)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM120)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM119)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM118)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM113)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM116)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM115)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM117)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM114)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM112)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM099)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM098)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM097)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM096)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM095)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM094)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM093)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM092)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM091)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM111)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM110)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM109)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM108)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM107)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM106)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM105)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM104)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM103)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM102)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル256)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM051)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM001)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM006)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM005)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM002)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM003)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM004)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM007)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM052)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM053)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM054)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM055)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM056)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM057)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM058)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM059)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM060)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM071)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM072)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM073)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM074)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM075)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM076)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM077)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM078)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM079)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM080)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM081)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM082)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM083)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM084)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM085)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM086)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM087)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM088)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM089)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM090)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM121)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM122)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM123)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM124)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM125)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM126)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM127)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM128)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM129)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM130)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM131)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM132)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM133)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM134)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM135)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM136)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM137)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM138)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM139)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM140)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM141)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM142)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM143)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM144)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM145)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM146)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM147)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM148)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM149)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM150)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM070)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM069)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM068)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM067)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM066)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM065)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM064)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM063)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM062)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM061)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.ボックス218,
            this.ITEM101,
            this.ボックス41,
            this.ボックス40,
            this.ボックス42,
            this.ボックス299,
            this.ラベル338,
            this.ボックス170,
            this.ボックス166,
            this.ボックス162,
            this.ボックス158,
            this.ボックス154,
            this.ボックス150,
            this.ボックス146,
            this.ボックス142,
            this.ボックス138,
            this.ボックス133,
            this.ボックス135,
            this.ラベル174,
            this.ボックス136,
            this.ボックス225,
            this.ボックス221,
            this.ボックス216,
            this.ボックス229,
            this.ボックス233,
            this.ボックス237,
            this.ボックス241,
            this.ボックス245,
            this.ボックス249,
            this.ボックス253,
            this.ボックス215,
            this.ボックス220,
            this.ボックス224,
            this.ボックス228,
            this.ボックス232,
            this.ボックス236,
            this.ボックス240,
            this.ボックス244,
            this.ボックス248,
            this.ボックス252,
            this.ITEM100,
            this.shape1,
            this.shape2,
            this.shape3,
            this.shape4,
            this.shape5,
            this.shape6,
            this.shape7,
            this.shape8,
            this.shape9,
            this.ITEM120,
            this.ITEM119,
            this.ITEM118,
            this.ITEM113,
            this.ITEM116,
            this.ITEM115,
            this.ITEM117,
            this.ITEM114,
            this.ITEM112,
            this.ITEM099,
            this.ITEM098,
            this.ITEM097,
            this.ITEM096,
            this.ITEM095,
            this.ITEM094,
            this.ITEM093,
            this.ITEM092,
            this.ITEM091,
            this.ITEM111,
            this.ボックス254,
            this.ボックス250,
            this.ボックス246,
            this.ボックス242,
            this.ボックス238,
            this.ボックス234,
            this.ボックス230,
            this.ボックス226,
            this.ボックス222,
            this.ITEM110,
            this.ITEM109,
            this.ITEM108,
            this.ITEM107,
            this.ITEM106,
            this.ITEM105,
            this.ITEM104,
            this.ITEM103,
            this.ITEM102,
            this.ボックス217,
            this.ラベル256,
            this.ITEM051,
            this.ボックス43,
            this.ITEM001,
            this.ITEM006,
            this.ボックス44,
            this.ボックス45,
            this.ラベル0,
            this.ラベル4,
            this.ラベル37,
            this.ラベル38,
            this.ITEM005,
            this.ITEM002,
            this.ITEM003,
            this.ITEM004,
            this.ラベル49,
            this.ITEM007,
            this.ボックス134,
            this.ボックス137,
            this.ボックス139,
            this.ボックス140,
            this.ボックス141,
            this.ボックス143,
            this.ボックス144,
            this.ボックス145,
            this.ボックス147,
            this.ボックス148,
            this.ボックス149,
            this.ボックス151,
            this.ボックス152,
            this.ボックス153,
            this.ボックス155,
            this.ボックス156,
            this.ボックス157,
            this.ボックス159,
            this.ボックス160,
            this.ボックス161,
            this.ボックス163,
            this.ボックス164,
            this.ボックス165,
            this.ボックス167,
            this.ボックス168,
            this.ボックス169,
            this.ボックス171,
            this.ボックス172,
            this.ボックス173,
            this.ITEM052,
            this.ITEM053,
            this.ITEM054,
            this.ITEM055,
            this.ITEM056,
            this.ITEM057,
            this.ITEM058,
            this.ITEM059,
            this.ITEM060,
            this.ITEM071,
            this.ITEM072,
            this.ITEM073,
            this.ITEM074,
            this.ITEM075,
            this.ITEM076,
            this.ITEM077,
            this.ITEM078,
            this.ITEM079,
            this.ITEM080,
            this.ITEM081,
            this.ITEM082,
            this.ITEM083,
            this.ITEM084,
            this.ITEM085,
            this.ITEM086,
            this.ITEM087,
            this.ITEM088,
            this.ITEM089,
            this.ITEM090,
            this.ボックス219,
            this.ボックス223,
            this.ボックス227,
            this.ボックス231,
            this.ボックス235,
            this.ボックス239,
            this.ボックス243,
            this.ボックス247,
            this.ボックス251,
            this.ボックス255,
            this.ITEM121,
            this.ITEM122,
            this.ITEM123,
            this.ITEM124,
            this.ITEM125,
            this.ITEM126,
            this.ITEM127,
            this.ITEM128,
            this.ITEM129,
            this.ITEM130,
            this.ボックス297,
            this.ボックス298,
            this.ボックス302,
            this.ボックス303,
            this.ボックス306,
            this.ボックス307,
            this.ボックス310,
            this.ボックス311,
            this.ボックス314,
            this.ボックス315,
            this.ボックス318,
            this.ボックス319,
            this.ボックス322,
            this.ボックス323,
            this.ボックス326,
            this.ボックス327,
            this.ボックス330,
            this.ボックス331,
            this.ボックス334,
            this.ボックス335,
            this.ITEM131,
            this.ITEM132,
            this.ITEM133,
            this.ITEM134,
            this.ITEM135,
            this.ITEM136,
            this.ITEM137,
            this.ITEM138,
            this.ITEM139,
            this.ITEM140,
            this.ITEM141,
            this.ITEM142,
            this.ITEM143,
            this.ITEM144,
            this.ITEM145,
            this.ITEM146,
            this.ITEM147,
            this.ITEM148,
            this.ITEM149,
            this.ITEM150,
            this.ITEM070,
            this.ITEM069,
            this.ITEM068,
            this.ITEM067,
            this.ITEM066,
            this.ITEM065,
            this.ITEM064,
            this.ITEM063,
            this.ITEM062,
            this.ITEM061});
            this.detail.Height = 5.682744F;
            this.detail.Name = "detail";
            // 
            // ボックス218
            // 
            this.ボックス218.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ボックス218.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス218.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス218.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス218.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス218.Height = 0.1968504F;
            this.ボックス218.Left = 0.1574803F;
            this.ボックス218.Name = "ボックス218";
            this.ボックス218.RoundingRadius = 9.999999F;
            this.ボックス218.Tag = "";
            this.ボックス218.Top = 3.023622F;
            this.ボックス218.Width = 0.7480315F;
            // 
            // ITEM101
            // 
            this.ITEM101.DataField = "ITEM101";
            this.ITEM101.Height = 0.1666667F;
            this.ITEM101.Left = 0.1574803F;
            this.ITEM101.Name = "ITEM101";
            this.ITEM101.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ITEM101.Tag = "";
            this.ITEM101.Text = "ITEM101";
            this.ITEM101.Top = 3.059055F;
            this.ITEM101.Width = 0.7479166F;
            // 
            // ボックス41
            // 
            this.ボックス41.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ボックス41.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス41.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス41.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス41.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス41.Height = 0.1968504F;
            this.ボックス41.Left = 0.6700788F;
            this.ボックス41.Name = "ボックス41";
            this.ボックス41.RoundingRadius = 9.999999F;
            this.ボックス41.Tag = "";
            this.ボックス41.Top = 0.2362205F;
            this.ボックス41.Width = 0.7283465F;
            // 
            // ボックス40
            // 
            this.ボックス40.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ボックス40.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス40.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス40.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス40.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス40.Height = 0.1968504F;
            this.ボックス40.Left = 0.004626036F;
            this.ボックス40.Name = "ボックス40";
            this.ボックス40.RoundingRadius = 9.999999F;
            this.ボックス40.Tag = "";
            this.ボックス40.Top = 0.2362205F;
            this.ボックス40.Width = 0.6692914F;
            // 
            // ボックス42
            // 
            this.ボックス42.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ボックス42.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス42.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス42.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス42.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス42.Height = 0.1968504F;
            this.ボックス42.Left = 1.397638F;
            this.ボックス42.Name = "ボックス42";
            this.ボックス42.RoundingRadius = 9.999999F;
            this.ボックス42.Tag = "";
            this.ボックス42.Top = 0.2362205F;
            this.ボックス42.Width = 1.575197F;
            // 
            // ボックス299
            // 
            this.ボックス299.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ボックス299.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス299.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス299.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス299.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス299.Height = 0.5905512F;
            this.ボックス299.Left = 0F;
            this.ボックス299.Name = "ボックス299";
            this.ボックス299.RoundingRadius = 9.999999F;
            this.ボックス299.Tag = "";
            this.ボックス299.Top = 3.858268F;
            this.ボックス299.Width = 0.1574803F;
            // 
            // ラベル338
            // 
            this.ラベル338.Height = 0.5905512F;
            this.ラベル338.HyperLink = null;
            this.ラベル338.Left = 0F;
            this.ラベル338.LineSpacing = 6F;
            this.ラベル338.Name = "ラベル338";
            this.ラベル338.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ラベル338.Tag = "";
            this.ラベル338.Text = "合\r\n計";
            this.ラベル338.Top = 3.942126F;
            this.ラベル338.Width = 0.1574803F;
            // 
            // ボックス170
            // 
            this.ボックス170.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ボックス170.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス170.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス170.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス170.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス170.Height = 0.1968504F;
            this.ボックス170.Left = 6.88873F;
            this.ボックス170.Name = "ボックス170";
            this.ボックス170.RoundingRadius = 9.999999F;
            this.ボックス170.Tag = "";
            this.ボックス170.Top = 0.9901575F;
            this.ボックス170.Width = 0.7480315F;
            // 
            // ボックス166
            // 
            this.ボックス166.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ボックス166.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス166.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス166.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス166.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス166.Height = 0.1968504F;
            this.ボックス166.Left = 6.140813F;
            this.ボックス166.Name = "ボックス166";
            this.ボックス166.RoundingRadius = 9.999999F;
            this.ボックス166.Tag = "";
            this.ボックス166.Top = 0.9901575F;
            this.ボックス166.Width = 0.7480315F;
            // 
            // ボックス162
            // 
            this.ボックス162.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ボックス162.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス162.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス162.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス162.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス162.Height = 0.1968504F;
            this.ボックス162.Left = 5.392897F;
            this.ボックス162.Name = "ボックス162";
            this.ボックス162.RoundingRadius = 9.999999F;
            this.ボックス162.Tag = "";
            this.ボックス162.Top = 0.9901575F;
            this.ボックス162.Width = 0.7480315F;
            // 
            // ボックス158
            // 
            this.ボックス158.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ボックス158.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス158.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス158.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス158.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス158.Height = 0.1968504F;
            this.ボックス158.Left = 4.64498F;
            this.ボックス158.Name = "ボックス158";
            this.ボックス158.RoundingRadius = 9.999999F;
            this.ボックス158.Tag = "";
            this.ボックス158.Top = 0.9901575F;
            this.ボックス158.Width = 0.7480315F;
            // 
            // ボックス154
            // 
            this.ボックス154.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ボックス154.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス154.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス154.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス154.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス154.Height = 0.1968504F;
            this.ボックス154.Left = 3.897063F;
            this.ボックス154.Name = "ボックス154";
            this.ボックス154.RoundingRadius = 9.999999F;
            this.ボックス154.Tag = "";
            this.ボックス154.Top = 0.9901575F;
            this.ボックス154.Width = 0.7480315F;
            // 
            // ボックス150
            // 
            this.ボックス150.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ボックス150.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス150.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス150.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス150.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス150.Height = 0.1968504F;
            this.ボックス150.Left = 3.149148F;
            this.ボックス150.Name = "ボックス150";
            this.ボックス150.RoundingRadius = 9.999999F;
            this.ボックス150.Tag = "";
            this.ボックス150.Top = 0.9901575F;
            this.ボックス150.Width = 0.7480315F;
            // 
            // ボックス146
            // 
            this.ボックス146.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ボックス146.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス146.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス146.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス146.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス146.Height = 0.1968504F;
            this.ボックス146.Left = 2.40123F;
            this.ボックス146.Name = "ボックス146";
            this.ボックス146.RoundingRadius = 9.999999F;
            this.ボックス146.Tag = "";
            this.ボックス146.Top = 0.9901575F;
            this.ボックス146.Width = 0.7480315F;
            // 
            // ボックス142
            // 
            this.ボックス142.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ボックス142.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス142.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス142.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス142.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス142.Height = 0.1968504F;
            this.ボックス142.Left = 1.653313F;
            this.ボックス142.Name = "ボックス142";
            this.ボックス142.RoundingRadius = 9.999999F;
            this.ボックス142.Tag = "";
            this.ボックス142.Top = 0.9901575F;
            this.ボックス142.Width = 0.7480315F;
            // 
            // ボックス138
            // 
            this.ボックス138.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ボックス138.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス138.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス138.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス138.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス138.Height = 0.1968504F;
            this.ボックス138.Left = 0.9053969F;
            this.ボックス138.Name = "ボックス138";
            this.ボックス138.RoundingRadius = 9.999999F;
            this.ボックス138.Tag = "";
            this.ボックス138.Top = 0.9901575F;
            this.ボックス138.Width = 0.7480315F;
            // 
            // ボックス133
            // 
            this.ボックス133.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ボックス133.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス133.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス133.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス133.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス133.Height = 0.1968504F;
            this.ボックス133.Left = 0.1574803F;
            this.ボックス133.Name = "ボックス133";
            this.ボックス133.RoundingRadius = 9.999999F;
            this.ボックス133.Tag = "";
            this.ボックス133.Top = 0.9901575F;
            this.ボックス133.Width = 0.7480315F;
            // 
            // ボックス135
            // 
            this.ボックス135.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ボックス135.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス135.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス135.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス135.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス135.Height = 1.181102F;
            this.ボックス135.Left = 0F;
            this.ボックス135.Name = "ボックス135";
            this.ボックス135.RoundingRadius = 9.999999F;
            this.ボックス135.Tag = "";
            this.ボックス135.Top = 0.9901575F;
            this.ボックス135.Width = 0.1574803F;
            // 
            // ラベル174
            // 
            this.ラベル174.Height = 1.181102F;
            this.ラベル174.HyperLink = null;
            this.ラベル174.Left = 0F;
            this.ラベル174.LineSpacing = 3F;
            this.ラベル174.Name = "ラベル174";
            this.ラベル174.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1; ddo-font-vertical: none";
            this.ラベル174.Tag = "";
            this.ラベル174.Text = "\r\n\r\n支\r\n\r\n給";
            this.ラベル174.Top = 1.010296F;
            this.ラベル174.Width = 0.1574803F;
            // 
            // ボックス136
            // 
            this.ボックス136.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ボックス136.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス136.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス136.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス136.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス136.Height = 0.1968504F;
            this.ボックス136.Left = 0.1574803F;
            this.ボックス136.Name = "ボックス136";
            this.ボックス136.RoundingRadius = 9.999999F;
            this.ボックス136.Tag = "";
            this.ボックス136.Top = 1.594488F;
            this.ボックス136.Width = 0.7480315F;
            // 
            // ボックス225
            // 
            this.ボックス225.BackColor = System.Drawing.Color.White;
            this.ボックス225.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス225.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス225.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス225.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス225.Height = 0.3937008F;
            this.ボックス225.Left = 1.653543F;
            this.ボックス225.Name = "ボックス225";
            this.ボックス225.RoundingRadius = 9.999999F;
            this.ボックス225.Tag = "";
            this.ボックス225.Top = 2.625984F;
            this.ボックス225.Width = 0.7480315F;
            // 
            // ボックス221
            // 
            this.ボックス221.BackColor = System.Drawing.Color.White;
            this.ボックス221.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス221.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス221.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス221.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス221.Height = 0.3937008F;
            this.ボックス221.Left = 0.905627F;
            this.ボックス221.Name = "ボックス221";
            this.ボックス221.RoundingRadius = 9.999999F;
            this.ボックス221.Tag = "";
            this.ボックス221.Top = 2.625984F;
            this.ボックス221.Width = 0.7480315F;
            // 
            // ボックス216
            // 
            this.ボックス216.BackColor = System.Drawing.Color.White;
            this.ボックス216.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス216.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス216.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス216.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス216.Height = 0.3937008F;
            this.ボックス216.Left = 0.1575519F;
            this.ボックス216.Name = "ボックス216";
            this.ボックス216.RoundingRadius = 9.999999F;
            this.ボックス216.Tag = "";
            this.ボックス216.Top = 2.625984F;
            this.ボックス216.Width = 0.7480315F;
            // 
            // ボックス229
            // 
            this.ボックス229.BackColor = System.Drawing.Color.White;
            this.ボックス229.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス229.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス229.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス229.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス229.Height = 0.3937008F;
            this.ボックス229.Left = 2.40146F;
            this.ボックス229.Name = "ボックス229";
            this.ボックス229.RoundingRadius = 9.999999F;
            this.ボックス229.Tag = "";
            this.ボックス229.Top = 2.625984F;
            this.ボックス229.Width = 0.7480315F;
            // 
            // ボックス233
            // 
            this.ボックス233.BackColor = System.Drawing.Color.White;
            this.ボックス233.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス233.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス233.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス233.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス233.Height = 0.3937008F;
            this.ボックス233.Left = 3.145375F;
            this.ボックス233.Name = "ボックス233";
            this.ボックス233.RoundingRadius = 9.999999F;
            this.ボックス233.Tag = "";
            this.ボックス233.Top = 2.625984F;
            this.ボックス233.Width = 0.7480315F;
            // 
            // ボックス237
            // 
            this.ボックス237.BackColor = System.Drawing.Color.White;
            this.ボックス237.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス237.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス237.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス237.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス237.Height = 0.3937008F;
            this.ボックス237.Left = 3.897294F;
            this.ボックス237.Name = "ボックス237";
            this.ボックス237.RoundingRadius = 9.999999F;
            this.ボックス237.Tag = "";
            this.ボックス237.Top = 2.625984F;
            this.ボックス237.Width = 0.7480315F;
            // 
            // ボックス241
            // 
            this.ボックス241.BackColor = System.Drawing.Color.White;
            this.ボックス241.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス241.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス241.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス241.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス241.Height = 0.3937008F;
            this.ボックス241.Left = 4.645096F;
            this.ボックス241.Name = "ボックス241";
            this.ボックス241.RoundingRadius = 9.999999F;
            this.ボックス241.Tag = "";
            this.ボックス241.Top = 2.625984F;
            this.ボックス241.Width = 0.7480315F;
            // 
            // ボックス245
            // 
            this.ボックス245.BackColor = System.Drawing.Color.White;
            this.ボックス245.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス245.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス245.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス245.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス245.Height = 0.3937008F;
            this.ボックス245.Left = 5.393013F;
            this.ボックス245.Name = "ボックス245";
            this.ボックス245.RoundingRadius = 9.999999F;
            this.ボックス245.Tag = "";
            this.ボックス245.Top = 2.625984F;
            this.ボックス245.Width = 0.7480315F;
            // 
            // ボックス249
            // 
            this.ボックス249.BackColor = System.Drawing.Color.White;
            this.ボックス249.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス249.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス249.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス249.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス249.Height = 0.3937008F;
            this.ボックス249.Left = 6.140929F;
            this.ボックス249.Name = "ボックス249";
            this.ボックス249.RoundingRadius = 9.999999F;
            this.ボックス249.Tag = "";
            this.ボックス249.Top = 2.625984F;
            this.ボックス249.Width = 0.7480315F;
            // 
            // ボックス253
            // 
            this.ボックス253.BackColor = System.Drawing.Color.White;
            this.ボックス253.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス253.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス253.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス253.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス253.Height = 0.3937008F;
            this.ボックス253.Left = 6.889049F;
            this.ボックス253.Name = "ボックス253";
            this.ボックス253.RoundingRadius = 9.999999F;
            this.ボックス253.Tag = "";
            this.ボックス253.Top = 2.625984F;
            this.ボックス253.Width = 0.7480315F;
            // 
            // ボックス215
            // 
            this.ボックス215.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ボックス215.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス215.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス215.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス215.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス215.Height = 0.1968504F;
            this.ボックス215.Left = 0.1577104F;
            this.ボックス215.Name = "ボックス215";
            this.ボックス215.RoundingRadius = 9.999999F;
            this.ボックス215.Tag = "";
            this.ボックス215.Top = 2.425197F;
            this.ボックス215.Width = 0.7480315F;
            // 
            // ボックス220
            // 
            this.ボックス220.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ボックス220.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス220.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス220.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス220.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス220.Height = 0.1968504F;
            this.ボックス220.Left = 0.905627F;
            this.ボックス220.Name = "ボックス220";
            this.ボックス220.RoundingRadius = 9.999999F;
            this.ボックス220.Tag = "";
            this.ボックス220.Top = 2.425197F;
            this.ボックス220.Width = 0.7480315F;
            // 
            // ボックス224
            // 
            this.ボックス224.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ボックス224.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス224.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス224.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス224.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス224.Height = 0.1968504F;
            this.ボックス224.Left = 1.653543F;
            this.ボックス224.Name = "ボックス224";
            this.ボックス224.RoundingRadius = 9.999999F;
            this.ボックス224.Tag = "";
            this.ボックス224.Top = 2.425197F;
            this.ボックス224.Width = 0.7480315F;
            // 
            // ボックス228
            // 
            this.ボックス228.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ボックス228.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス228.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス228.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス228.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス228.Height = 0.1968504F;
            this.ボックス228.Left = 2.40146F;
            this.ボックス228.Name = "ボックス228";
            this.ボックス228.RoundingRadius = 9.999999F;
            this.ボックス228.Tag = "";
            this.ボックス228.Top = 2.425197F;
            this.ボックス228.Width = 0.7480315F;
            // 
            // ボックス232
            // 
            this.ボックス232.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ボックス232.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス232.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス232.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス232.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス232.Height = 0.1968504F;
            this.ボックス232.Left = 3.149378F;
            this.ボックス232.Name = "ボックス232";
            this.ボックス232.RoundingRadius = 9.999999F;
            this.ボックス232.Tag = "";
            this.ボックス232.Top = 2.425197F;
            this.ボックス232.Width = 0.7480315F;
            // 
            // ボックス236
            // 
            this.ボックス236.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ボックス236.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス236.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス236.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス236.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス236.Height = 0.1968504F;
            this.ボックス236.Left = 3.897294F;
            this.ボックス236.Name = "ボックス236";
            this.ボックス236.RoundingRadius = 9.999999F;
            this.ボックス236.Tag = "";
            this.ボックス236.Top = 2.425197F;
            this.ボックス236.Width = 0.7480315F;
            // 
            // ボックス240
            // 
            this.ボックス240.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ボックス240.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス240.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス240.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス240.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス240.Height = 0.1968504F;
            this.ボックス240.Left = 4.645211F;
            this.ボックス240.Name = "ボックス240";
            this.ボックス240.RoundingRadius = 9.999999F;
            this.ボックス240.Tag = "";
            this.ボックス240.Top = 2.425197F;
            this.ボックス240.Width = 0.7480315F;
            // 
            // ボックス244
            // 
            this.ボックス244.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ボックス244.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス244.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス244.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス244.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス244.Height = 0.1968504F;
            this.ボックス244.Left = 5.393127F;
            this.ボックス244.Name = "ボックス244";
            this.ボックス244.RoundingRadius = 9.999999F;
            this.ボックス244.Tag = "";
            this.ボックス244.Top = 2.425197F;
            this.ボックス244.Width = 0.7480315F;
            // 
            // ボックス248
            // 
            this.ボックス248.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ボックス248.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス248.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス248.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス248.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス248.Height = 0.1968504F;
            this.ボックス248.Left = 6.141044F;
            this.ボックス248.Name = "ボックス248";
            this.ボックス248.RoundingRadius = 9.999999F;
            this.ボックス248.Tag = "";
            this.ボックス248.Top = 2.425197F;
            this.ボックス248.Width = 0.7480315F;
            // 
            // ボックス252
            // 
            this.ボックス252.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ボックス252.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス252.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス252.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス252.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス252.Height = 0.1968504F;
            this.ボックス252.Left = 6.888655F;
            this.ボックス252.Name = "ボックス252";
            this.ボックス252.RoundingRadius = 9.999999F;
            this.ボックス252.Tag = "";
            this.ボックス252.Top = 2.425197F;
            this.ボックス252.Width = 0.7480315F;
            // 
            // ITEM100
            // 
            this.ITEM100.DataField = "ITEM100";
            this.ITEM100.Height = 0.1586616F;
            this.ITEM100.Left = 6.889049F;
            this.ITEM100.Name = "ITEM100";
            this.ITEM100.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ITEM100.Tag = "";
            this.ITEM100.Text = "ITEM100";
            this.ITEM100.Top = 2.453543F;
            this.ITEM100.Width = 0.7479166F;
            // 
            // shape1
            // 
            this.shape1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.shape1.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape1.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape1.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape1.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape1.Height = 0.1968504F;
            this.shape1.Left = 0.158471F;
            this.shape1.Name = "shape1";
            this.shape1.RoundingRadius = 9.999999F;
            this.shape1.Tag = "";
            this.shape1.Top = 2.425197F;
            this.shape1.Width = 0.7480315F;
            // 
            // shape2
            // 
            this.shape2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.shape2.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape2.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape2.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape2.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape2.Height = 0.1968504F;
            this.shape2.Left = 0.9063708F;
            this.shape2.Name = "shape2";
            this.shape2.RoundingRadius = 9.999999F;
            this.shape2.Tag = "";
            this.shape2.Top = 2.425197F;
            this.shape2.Width = 0.7480315F;
            // 
            // shape3
            // 
            this.shape3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.shape3.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape3.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape3.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape3.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape3.Height = 0.1968504F;
            this.shape3.Left = 1.654304F;
            this.shape3.Name = "shape3";
            this.shape3.RoundingRadius = 9.999999F;
            this.shape3.Tag = "";
            this.shape3.Top = 2.425197F;
            this.shape3.Width = 0.7480315F;
            // 
            // shape4
            // 
            this.shape4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.shape4.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape4.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape4.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape4.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape4.Height = 0.1968504F;
            this.shape4.Left = 2.40204F;
            this.shape4.Name = "shape4";
            this.shape4.RoundingRadius = 9.999999F;
            this.shape4.Tag = "";
            this.shape4.Top = 2.425197F;
            this.shape4.Width = 0.7480315F;
            // 
            // shape5
            // 
            this.shape5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.shape5.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape5.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape5.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape5.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape5.Height = 0.1968504F;
            this.shape5.Left = 3.146135F;
            this.shape5.Name = "shape5";
            this.shape5.RoundingRadius = 9.999999F;
            this.shape5.Tag = "";
            this.shape5.Top = 2.425197F;
            this.shape5.Width = 0.7480315F;
            // 
            // shape6
            // 
            this.shape6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.shape6.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape6.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape6.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape6.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape6.Height = 0.1968504F;
            this.shape6.Left = 3.898054F;
            this.shape6.Name = "shape6";
            this.shape6.RoundingRadius = 9.999999F;
            this.shape6.Tag = "";
            this.shape6.Top = 2.425197F;
            this.shape6.Width = 0.7480315F;
            // 
            // shape7
            // 
            this.shape7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.shape7.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape7.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape7.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape7.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape7.Height = 0.1968504F;
            this.shape7.Left = 4.645096F;
            this.shape7.Name = "shape7";
            this.shape7.RoundingRadius = 9.999999F;
            this.shape7.Tag = "";
            this.shape7.Top = 2.425197F;
            this.shape7.Width = 0.7480315F;
            // 
            // shape8
            // 
            this.shape8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.shape8.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape8.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape8.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape8.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape8.Height = 0.1968504F;
            this.shape8.Left = 5.393013F;
            this.shape8.Name = "shape8";
            this.shape8.RoundingRadius = 9.999999F;
            this.shape8.Tag = "";
            this.shape8.Top = 2.425197F;
            this.shape8.Width = 0.7480315F;
            // 
            // shape9
            // 
            this.shape9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.shape9.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape9.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape9.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape9.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.shape9.Height = 0.1968504F;
            this.shape9.Left = 6.140929F;
            this.shape9.Name = "shape9";
            this.shape9.RoundingRadius = 9.999999F;
            this.shape9.Tag = "";
            this.shape9.Top = 2.425197F;
            this.shape9.Width = 0.7437881F;
            // 
            // ITEM120
            // 
            this.ITEM120.DataField = "ITEM120";
            this.ITEM120.Height = 0.1666667F;
            this.ITEM120.Left = 6.865076F;
            this.ITEM120.Name = "ITEM120";
            this.ITEM120.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.ITEM120.Tag = "";
            this.ITEM120.Text = "ITEM120";
            this.ITEM120.Top = 2.740158F;
            this.ITEM120.Width = 0.7479166F;
            // 
            // ITEM119
            // 
            this.ITEM119.DataField = "ITEM119";
            this.ITEM119.Height = 0.1666667F;
            this.ITEM119.Left = 6.11716F;
            this.ITEM119.Name = "ITEM119";
            this.ITEM119.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.ITEM119.Tag = "";
            this.ITEM119.Text = "ITEM119";
            this.ITEM119.Top = 2.740158F;
            this.ITEM119.Width = 0.7479166F;
            // 
            // ITEM118
            // 
            this.ITEM118.DataField = "ITEM118";
            this.ITEM118.Height = 0.1666667F;
            this.ITEM118.Left = 5.369244F;
            this.ITEM118.Name = "ITEM118";
            this.ITEM118.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.ITEM118.Tag = "";
            this.ITEM118.Text = "ITEM118";
            this.ITEM118.Top = 2.740158F;
            this.ITEM118.Width = 0.7479166F;
            // 
            // ITEM113
            // 
            this.ITEM113.DataField = "ITEM113";
            this.ITEM113.Height = 0.1666667F;
            this.ITEM113.Left = 1.62966F;
            this.ITEM113.Name = "ITEM113";
            this.ITEM113.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.ITEM113.Tag = "";
            this.ITEM113.Text = "ITEM113";
            this.ITEM113.Top = 2.740158F;
            this.ITEM113.Width = 0.7479166F;
            // 
            // ITEM116
            // 
            this.ITEM116.DataField = "ITEM116";
            this.ITEM116.Height = 0.1666667F;
            this.ITEM116.Left = 3.87341F;
            this.ITEM116.Name = "ITEM116";
            this.ITEM116.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.ITEM116.Tag = "";
            this.ITEM116.Text = "ITEM116";
            this.ITEM116.Top = 2.740158F;
            this.ITEM116.Width = 0.7479166F;
            // 
            // ITEM115
            // 
            this.ITEM115.DataField = "ITEM115";
            this.ITEM115.Height = 0.1666667F;
            this.ITEM115.Left = 3.125494F;
            this.ITEM115.Name = "ITEM115";
            this.ITEM115.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.ITEM115.Tag = "";
            this.ITEM115.Text = "ITEM115";
            this.ITEM115.Top = 2.740158F;
            this.ITEM115.Width = 0.7479166F;
            // 
            // ITEM117
            // 
            this.ITEM117.DataField = "ITEM117";
            this.ITEM117.Height = 0.1666667F;
            this.ITEM117.Left = 4.621326F;
            this.ITEM117.Name = "ITEM117";
            this.ITEM117.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.ITEM117.Tag = "";
            this.ITEM117.Text = "ITEM117";
            this.ITEM117.Top = 2.740158F;
            this.ITEM117.Width = 0.7479166F;
            // 
            // ITEM114
            // 
            this.ITEM114.DataField = "ITEM114";
            this.ITEM114.Height = 0.1666667F;
            this.ITEM114.Left = 2.377577F;
            this.ITEM114.Name = "ITEM114";
            this.ITEM114.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.ITEM114.Tag = "";
            this.ITEM114.Text = "ITEM114";
            this.ITEM114.Top = 2.740158F;
            this.ITEM114.Width = 0.7479166F;
            // 
            // ITEM112
            // 
            this.ITEM112.DataField = "ITEM112";
            this.ITEM112.Height = 0.1666667F;
            this.ITEM112.Left = 0.8817431F;
            this.ITEM112.Name = "ITEM112";
            this.ITEM112.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.ITEM112.Tag = "";
            this.ITEM112.Text = "ITEM112";
            this.ITEM112.Top = 2.740158F;
            this.ITEM112.Width = 0.7479166F;
            // 
            // ITEM099
            // 
            this.ITEM099.DataField = "ITEM099";
            this.ITEM099.Height = 0.1586616F;
            this.ITEM099.Left = 6.141132F;
            this.ITEM099.Name = "ITEM099";
            this.ITEM099.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ITEM099.Tag = "";
            this.ITEM099.Text = "ITEM099";
            this.ITEM099.Top = 2.453543F;
            this.ITEM099.Width = 0.7479166F;
            // 
            // ITEM098
            // 
            this.ITEM098.DataField = "ITEM098";
            this.ITEM098.Height = 0.1586616F;
            this.ITEM098.Left = 5.393216F;
            this.ITEM098.Name = "ITEM098";
            this.ITEM098.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ITEM098.Tag = "";
            this.ITEM098.Text = "ITEM098";
            this.ITEM098.Top = 2.453543F;
            this.ITEM098.Width = 0.7479166F;
            // 
            // ITEM097
            // 
            this.ITEM097.DataField = "ITEM097";
            this.ITEM097.Height = 0.1586616F;
            this.ITEM097.Left = 4.645299F;
            this.ITEM097.Name = "ITEM097";
            this.ITEM097.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ITEM097.Tag = "";
            this.ITEM097.Text = "ITEM097";
            this.ITEM097.Top = 2.453543F;
            this.ITEM097.Width = 0.7479166F;
            // 
            // ITEM096
            // 
            this.ITEM096.DataField = "ITEM096";
            this.ITEM096.Height = 0.1586616F;
            this.ITEM096.Left = 3.897382F;
            this.ITEM096.Name = "ITEM096";
            this.ITEM096.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ITEM096.Tag = "";
            this.ITEM096.Text = "ITEM096";
            this.ITEM096.Top = 2.453543F;
            this.ITEM096.Width = 0.7479166F;
            // 
            // ITEM095
            // 
            this.ITEM095.DataField = "ITEM095";
            this.ITEM095.Height = 0.1586616F;
            this.ITEM095.Left = 3.149466F;
            this.ITEM095.Name = "ITEM095";
            this.ITEM095.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ITEM095.Tag = "";
            this.ITEM095.Text = "ITEM095";
            this.ITEM095.Top = 2.453543F;
            this.ITEM095.Width = 0.7479166F;
            // 
            // ITEM094
            // 
            this.ITEM094.DataField = "ITEM094";
            this.ITEM094.Height = 0.1586616F;
            this.ITEM094.Left = 2.401549F;
            this.ITEM094.Name = "ITEM094";
            this.ITEM094.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ITEM094.Tag = "";
            this.ITEM094.Text = "ITEM094";
            this.ITEM094.Top = 2.453543F;
            this.ITEM094.Width = 0.7479166F;
            // 
            // ITEM093
            // 
            this.ITEM093.DataField = "ITEM093";
            this.ITEM093.Height = 0.1586616F;
            this.ITEM093.Left = 1.653632F;
            this.ITEM093.Name = "ITEM093";
            this.ITEM093.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ITEM093.Tag = "";
            this.ITEM093.Text = "ITEM093";
            this.ITEM093.Top = 2.453543F;
            this.ITEM093.Width = 0.7479166F;
            // 
            // ITEM092
            // 
            this.ITEM092.DataField = "ITEM092";
            this.ITEM092.Height = 0.1586616F;
            this.ITEM092.Left = 0.9057152F;
            this.ITEM092.Name = "ITEM092";
            this.ITEM092.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ITEM092.Tag = "";
            this.ITEM092.Text = "ITEM092";
            this.ITEM092.Top = 2.453543F;
            this.ITEM092.Width = 0.7479166F;
            // 
            // ITEM091
            // 
            this.ITEM091.DataField = "ITEM091";
            this.ITEM091.Height = 0.1586616F;
            this.ITEM091.Left = 0.1577986F;
            this.ITEM091.Name = "ITEM091";
            this.ITEM091.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ITEM091.Tag = "";
            this.ITEM091.Text = "ITEM091";
            this.ITEM091.Top = 2.453543F;
            this.ITEM091.Width = 0.7479166F;
            // 
            // ITEM111
            // 
            this.ITEM111.DataField = "ITEM111";
            this.ITEM111.Height = 0.1666667F;
            this.ITEM111.Left = 0.1338265F;
            this.ITEM111.Name = "ITEM111";
            this.ITEM111.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.ITEM111.Tag = "";
            this.ITEM111.Text = "ITEM111";
            this.ITEM111.Top = 2.740158F;
            this.ITEM111.Width = 0.7479166F;
            // 
            // ボックス254
            // 
            this.ボックス254.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ボックス254.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス254.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス254.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス254.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス254.Height = 0.1968504F;
            this.ボックス254.Left = 6.888774F;
            this.ボックス254.Name = "ボックス254";
            this.ボックス254.RoundingRadius = 9.999999F;
            this.ボックス254.Tag = "";
            this.ボックス254.Top = 3.023622F;
            this.ボックス254.Width = 0.7480315F;
            // 
            // ボックス250
            // 
            this.ボックス250.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ボックス250.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス250.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス250.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス250.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス250.Height = 0.1968504F;
            this.ボックス250.Left = 6.140857F;
            this.ボックス250.Name = "ボックス250";
            this.ボックス250.RoundingRadius = 9.999999F;
            this.ボックス250.Tag = "";
            this.ボックス250.Top = 3.023622F;
            this.ボックス250.Width = 0.7480315F;
            // 
            // ボックス246
            // 
            this.ボックス246.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ボックス246.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス246.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス246.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス246.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス246.Height = 0.1968504F;
            this.ボックス246.Left = 5.392941F;
            this.ボックス246.Name = "ボックス246";
            this.ボックス246.RoundingRadius = 9.999999F;
            this.ボックス246.Tag = "";
            this.ボックス246.Top = 3.023622F;
            this.ボックス246.Width = 0.7480315F;
            // 
            // ボックス242
            // 
            this.ボックス242.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ボックス242.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス242.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス242.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス242.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス242.Height = 0.1968504F;
            this.ボックス242.Left = 4.645024F;
            this.ボックス242.Name = "ボックス242";
            this.ボックス242.RoundingRadius = 9.999999F;
            this.ボックス242.Tag = "";
            this.ボックス242.Top = 3.023622F;
            this.ボックス242.Width = 0.7480315F;
            // 
            // ボックス238
            // 
            this.ボックス238.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ボックス238.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス238.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス238.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス238.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス238.Height = 0.1968504F;
            this.ボックス238.Left = 3.897063F;
            this.ボックス238.Name = "ボックス238";
            this.ボックス238.RoundingRadius = 9.999999F;
            this.ボックス238.Tag = "";
            this.ボックス238.Top = 3.023622F;
            this.ボックス238.Width = 0.7480315F;
            // 
            // ボックス234
            // 
            this.ボックス234.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ボックス234.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス234.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス234.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス234.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス234.Height = 0.1968504F;
            this.ボックス234.Left = 3.149148F;
            this.ボックス234.Name = "ボックス234";
            this.ボックス234.RoundingRadius = 9.999999F;
            this.ボックス234.Tag = "";
            this.ボックス234.Top = 3.023622F;
            this.ボックス234.Width = 0.7480315F;
            // 
            // ボックス230
            // 
            this.ボックス230.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ボックス230.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス230.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス230.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス230.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス230.Height = 0.1968504F;
            this.ボックス230.Left = 2.40123F;
            this.ボックス230.Name = "ボックス230";
            this.ボックス230.RoundingRadius = 9.999999F;
            this.ボックス230.Tag = "";
            this.ボックス230.Top = 3.023622F;
            this.ボックス230.Width = 0.7480315F;
            // 
            // ボックス226
            // 
            this.ボックス226.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ボックス226.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス226.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス226.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス226.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス226.Height = 0.1968504F;
            this.ボックス226.Left = 1.653313F;
            this.ボックス226.Name = "ボックス226";
            this.ボックス226.RoundingRadius = 9.999999F;
            this.ボックス226.Tag = "";
            this.ボックス226.Top = 3.023622F;
            this.ボックス226.Width = 0.7480315F;
            // 
            // ボックス222
            // 
            this.ボックス222.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ボックス222.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス222.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス222.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス222.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス222.Height = 0.1968504F;
            this.ボックス222.Left = 0.9053969F;
            this.ボックス222.Name = "ボックス222";
            this.ボックス222.RoundingRadius = 9.999999F;
            this.ボックス222.Tag = "";
            this.ボックス222.Top = 3.023622F;
            this.ボックス222.Width = 0.7480315F;
            // 
            // ITEM110
            // 
            this.ITEM110.DataField = "ITEM110";
            this.ITEM110.Height = 0.1666667F;
            this.ITEM110.Left = 6.888889F;
            this.ITEM110.Name = "ITEM110";
            this.ITEM110.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ITEM110.Tag = "";
            this.ITEM110.Text = "ITEM110";
            this.ITEM110.Top = 3.059055F;
            this.ITEM110.Width = 0.7479166F;
            // 
            // ITEM109
            // 
            this.ITEM109.DataField = "ITEM109";
            this.ITEM109.Height = 0.1666667F;
            this.ITEM109.Left = 6.140972F;
            this.ITEM109.Name = "ITEM109";
            this.ITEM109.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ITEM109.Tag = "";
            this.ITEM109.Text = "ITEM109";
            this.ITEM109.Top = 3.059055F;
            this.ITEM109.Width = 0.7479166F;
            // 
            // ITEM108
            // 
            this.ITEM108.DataField = "ITEM108";
            this.ITEM108.Height = 0.1666667F;
            this.ITEM108.Left = 5.393056F;
            this.ITEM108.Name = "ITEM108";
            this.ITEM108.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ITEM108.Tag = "";
            this.ITEM108.Text = "ITEM108";
            this.ITEM108.Top = 3.059055F;
            this.ITEM108.Width = 0.7479166F;
            // 
            // ITEM107
            // 
            this.ITEM107.DataField = "ITEM107";
            this.ITEM107.Height = 0.1666667F;
            this.ITEM107.Left = 4.645139F;
            this.ITEM107.Name = "ITEM107";
            this.ITEM107.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ITEM107.Tag = "";
            this.ITEM107.Text = "ITEM107";
            this.ITEM107.Top = 3.059055F;
            this.ITEM107.Width = 0.7479166F;
            // 
            // ITEM106
            // 
            this.ITEM106.DataField = "ITEM106";
            this.ITEM106.Height = 0.1666667F;
            this.ITEM106.Left = 3.897063F;
            this.ITEM106.Name = "ITEM106";
            this.ITEM106.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ITEM106.Tag = "";
            this.ITEM106.Text = "ITEM106";
            this.ITEM106.Top = 3.059055F;
            this.ITEM106.Width = 0.7479166F;
            // 
            // ITEM105
            // 
            this.ITEM105.DataField = "ITEM105";
            this.ITEM105.Height = 0.1666667F;
            this.ITEM105.Left = 3.149148F;
            this.ITEM105.Name = "ITEM105";
            this.ITEM105.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ITEM105.Tag = "";
            this.ITEM105.Text = "ITEM105";
            this.ITEM105.Top = 3.059055F;
            this.ITEM105.Width = 0.7479166F;
            // 
            // ITEM104
            // 
            this.ITEM104.DataField = "ITEM104";
            this.ITEM104.Height = 0.1666667F;
            this.ITEM104.Left = 2.40123F;
            this.ITEM104.Name = "ITEM104";
            this.ITEM104.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ITEM104.Tag = "";
            this.ITEM104.Text = "ITEM104";
            this.ITEM104.Top = 3.059055F;
            this.ITEM104.Width = 0.7479166F;
            // 
            // ITEM103
            // 
            this.ITEM103.DataField = "ITEM103";
            this.ITEM103.Height = 0.1666667F;
            this.ITEM103.Left = 1.653313F;
            this.ITEM103.Name = "ITEM103";
            this.ITEM103.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ITEM103.Tag = "";
            this.ITEM103.Text = "ITEM103";
            this.ITEM103.Top = 3.059055F;
            this.ITEM103.Width = 0.7479166F;
            // 
            // ITEM102
            // 
            this.ITEM102.DataField = "ITEM102";
            this.ITEM102.Height = 0.1666667F;
            this.ITEM102.Left = 0.9053969F;
            this.ITEM102.Name = "ITEM102";
            this.ITEM102.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ITEM102.Tag = "";
            this.ITEM102.Text = "ITEM102";
            this.ITEM102.Top = 3.059055F;
            this.ITEM102.Width = 0.7479166F;
            // 
            // ボックス217
            // 
            this.ボックス217.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ボックス217.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス217.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス217.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス217.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス217.Height = 1.185039F;
            this.ボックス217.Left = 0F;
            this.ボックス217.Name = "ボックス217";
            this.ボックス217.RoundingRadius = 9.999999F;
            this.ボックス217.Tag = "";
            this.ボックス217.Top = 2.425197F;
            this.ボックス217.Width = 0.1574803F;
            // 
            // ラベル256
            // 
            this.ラベル256.Height = 1.169291F;
            this.ラベル256.HyperLink = null;
            this.ラベル256.Left = 0F;
            this.ラベル256.LineSpacing = 4F;
            this.ラベル256.Name = "ラベル256";
            this.ラベル256.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ラベル256.Tag = "";
            this.ラベル256.Text = "\r\n\r\n控\r\n\r\n除";
            this.ラベル256.Top = 2.433071F;
            this.ラベル256.Width = 0.1574803F;
            // 
            // ITEM051
            // 
            this.ITEM051.DataField = "ITEM051";
            this.ITEM051.Height = 0.1968504F;
            this.ITEM051.Left = 0.157874F;
            this.ITEM051.Name = "ITEM051";
            this.ITEM051.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ITEM051.Tag = "";
            this.ITEM051.Text = "ITEM051";
            this.ITEM051.Top = 1.023622F;
            this.ITEM051.Width = 0.7480315F;
            // 
            // ボックス43
            // 
            this.ボックス43.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス43.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス43.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス43.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス43.Height = 0.2362205F;
            this.ボックス43.Left = 0.004513979F;
            this.ボックス43.Name = "ボックス43";
            this.ボックス43.RoundingRadius = 9.999999F;
            this.ボックス43.Tag = "";
            this.ボックス43.Top = 0.4378992F;
            this.ボックス43.Width = 0.6692914F;
            // 
            // ITEM001
            // 
            this.ITEM001.DataField = "ITEM001";
            this.ITEM001.Height = 0.1458333F;
            this.ITEM001.Left = 0.03188977F;
            this.ITEM001.Name = "ITEM001";
            this.ITEM001.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.ITEM001.Tag = "";
            this.ITEM001.Text = "ITEM001";
            this.ITEM001.Top = 0.496063F;
            this.ITEM001.Width = 0.2543307F;
            // 
            // ITEM006
            // 
            this.ITEM006.DataField = "ITEM006";
            this.ITEM006.Height = 0.2291667F;
            this.ITEM006.Left = 5.039371F;
            this.ITEM006.Name = "ITEM006";
            this.ITEM006.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 16pt; font-" +
    "weight: bold; text-align: left; ddo-char-set: 1";
            this.ITEM006.Tag = "";
            this.ITEM006.Text = "ITEM006";
            this.ITEM006.Top = 0.2394849F;
            this.ITEM006.Width = 0.6708333F;
            // 
            // ボックス44
            // 
            this.ボックス44.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス44.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス44.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス44.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス44.Height = 0.2362205F;
            this.ボックス44.Left = 0.6692722F;
            this.ボックス44.Name = "ボックス44";
            this.ボックス44.RoundingRadius = 9.999999F;
            this.ボックス44.Tag = "";
            this.ボックス44.Top = 0.4378992F;
            this.ボックス44.Width = 0.7283465F;
            // 
            // ボックス45
            // 
            this.ボックス45.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス45.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス45.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス45.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス45.Height = 0.2362205F;
            this.ボックス45.Left = 1.396137F;
            this.ボックス45.Name = "ボックス45";
            this.ボックス45.RoundingRadius = 9.999999F;
            this.ボックス45.Tag = "";
            this.ボックス45.Top = 0.4378992F;
            this.ボックス45.Width = 1.575197F;
            // 
            // ラベル0
            // 
            this.ラベル0.Height = 0.2291667F;
            this.ラベル0.HyperLink = null;
            this.ラベル0.Left = 3.320634F;
            this.ラベル0.Name = "ラベル0";
            this.ラベル0.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 16pt; font-weight: bold; text-align:" +
    " center; ddo-char-set: 1";
            this.ラベル0.Tag = "";
            this.ラベル0.Text = "賞 与 明 細 書";
            this.ラベル0.Top = 0.2394849F;
            this.ラベル0.Width = 1.689182F;
            // 
            // ラベル4
            // 
            this.ラベル4.Height = 0.1666667F;
            this.ラベル4.HyperLink = null;
            this.ラベル4.Left = 0.004330709F;
            this.ラベル4.Name = "ラベル4";
            this.ラベル4.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ラベル4.Tag = "";
            this.ラベル4.Text = "所　　属";
            this.ラベル4.Top = 0.2716536F;
            this.ラベル4.Width = 0.6377953F;
            // 
            // ラベル37
            // 
            this.ラベル37.Height = 0.1666667F;
            this.ラベル37.HyperLink = null;
            this.ラベル37.Left = 0.6843668F;
            this.ラベル37.Name = "ラベル37";
            this.ラベル37.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ラベル37.Tag = "";
            this.ラベル37.Text = "社員番号";
            this.ラベル37.Top = 0.2716536F;
            this.ラベル37.Width = 0.6825625F;
            // 
            // ラベル38
            // 
            this.ラベル38.Height = 0.1666667F;
            this.ラベル38.HyperLink = null;
            this.ラベル38.Left = 1.457759F;
            this.ラベル38.Name = "ラベル38";
            this.ラベル38.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ラベル38.Tag = "";
            this.ラベル38.Text = "氏　　　　　名";
            this.ラベル38.Top = 0.2716536F;
            this.ラベル38.Width = 1.489583F;
            // 
            // ITEM005
            // 
            this.ITEM005.DataField = "ITEM005";
            this.ITEM005.Height = 0.1666667F;
            this.ITEM005.Left = 3.383071F;
            this.ITEM005.Name = "ITEM005";
            this.ITEM005.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: center; ddo-char-set: 1";
            this.ITEM005.Tag = "";
            this.ITEM005.Text = "ITEM005";
            this.ITEM005.Top = 0.6299213F;
            this.ITEM005.Width = 1.629167F;
            // 
            // ITEM002
            // 
            this.ITEM002.DataField = "ITEM002";
            this.ITEM002.Height = 0.1458333F;
            this.ITEM002.Left = 0.2980315F;
            this.ITEM002.Name = "ITEM002";
            this.ITEM002.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.ITEM002.Tag = "";
            this.ITEM002.Text = "ITEM002";
            this.ITEM002.Top = 0.496063F;
            this.ITEM002.Width = 0.3173228F;
            // 
            // ITEM003
            // 
            this.ITEM003.DataField = "ITEM003";
            this.ITEM003.Height = 0.1458333F;
            this.ITEM003.Left = 0.7F;
            this.ITEM003.Name = "ITEM003";
            this.ITEM003.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: right; ddo-char-set: 1";
            this.ITEM003.Tag = "";
            this.ITEM003.Text = "ITEM003";
            this.ITEM003.Top = 0.496063F;
            this.ITEM003.Width = 0.6083333F;
            // 
            // ITEM004
            // 
            this.ITEM004.DataField = "ITEM004";
            this.ITEM004.Height = 0.1458333F;
            this.ITEM004.Left = 1.458137F;
            this.ITEM004.Name = "ITEM004";
            this.ITEM004.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-" +
    "weight: normal; text-align: left; ddo-char-set: 1";
            this.ITEM004.Tag = "";
            this.ITEM004.Text = "ITEM004";
            this.ITEM004.Top = 0.496063F;
            this.ITEM004.Width = 1.214206F;
            // 
            // ラベル49
            // 
            this.ラベル49.Height = 0.1458333F;
            this.ラベル49.HyperLink = null;
            this.ラベル49.Left = 2.695964F;
            this.ラベル49.Name = "ラベル49";
            this.ラベル49.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 10pt; font-weight: normal; text-alig" +
    "n: center; ddo-char-set: 1";
            this.ラベル49.Tag = "";
            this.ラベル49.Text = "様";
            this.ラベル49.Top = 0.496063F;
            this.ラベル49.Width = 0.2395833F;
            // 
            // ITEM007
            // 
            this.ITEM007.DataField = "ITEM007";
            this.ITEM007.Height = 0.15625F;
            this.ITEM007.Left = 6.106597F;
            this.ITEM007.Name = "ITEM007";
            this.ITEM007.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.ITEM007.Tag = "";
            this.ITEM007.Text = "ITEM007";
            this.ITEM007.Top = 0.640338F;
            this.ITEM007.Width = 1.379167F;
            // 
            // ボックス134
            // 
            this.ボックス134.BackColor = System.Drawing.Color.White;
            this.ボックス134.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス134.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス134.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス134.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス134.Height = 0.3937008F;
            this.ボックス134.Left = 0.1574803F;
            this.ボックス134.Name = "ボックス134";
            this.ボックス134.RoundingRadius = 9.999999F;
            this.ボックス134.Tag = "";
            this.ボックス134.Top = 1.195669F;
            this.ボックス134.Width = 0.7480315F;
            // 
            // ボックス137
            // 
            this.ボックス137.BackColor = System.Drawing.Color.White;
            this.ボックス137.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス137.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス137.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス137.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス137.Height = 0.3937008F;
            this.ボックス137.Left = 0.1574803F;
            this.ボックス137.Name = "ボックス137";
            this.ボックス137.RoundingRadius = 9.999999F;
            this.ボックス137.Tag = "";
            this.ボックス137.Top = 1.775591F;
            this.ボックス137.Width = 0.7480315F;
            // 
            // ボックス139
            // 
            this.ボックス139.BackColor = System.Drawing.Color.White;
            this.ボックス139.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス139.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス139.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス139.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス139.Height = 0.3937008F;
            this.ボックス139.Left = 0.9055555F;
            this.ボックス139.Name = "ボックス139";
            this.ボックス139.RoundingRadius = 9.999999F;
            this.ボックス139.Tag = "";
            this.ボックス139.Top = 1.195669F;
            this.ボックス139.Width = 0.7480315F;
            // 
            // ボックス140
            // 
            this.ボックス140.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ボックス140.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス140.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス140.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス140.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス140.Height = 0.1968504F;
            this.ボックス140.Left = 0.9055555F;
            this.ボックス140.Name = "ボックス140";
            this.ボックス140.RoundingRadius = 9.999999F;
            this.ボックス140.Tag = "";
            this.ボックス140.Top = 1.594488F;
            this.ボックス140.Width = 0.7480315F;
            // 
            // ボックス141
            // 
            this.ボックス141.BackColor = System.Drawing.Color.White;
            this.ボックス141.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス141.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス141.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス141.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス141.Height = 0.3937008F;
            this.ボックス141.Left = 0.9055118F;
            this.ボックス141.Name = "ボックス141";
            this.ボックス141.RoundingRadius = 9.999999F;
            this.ボックス141.Tag = "";
            this.ボックス141.Top = 1.775591F;
            this.ボックス141.Width = 0.7480315F;
            // 
            // ボックス143
            // 
            this.ボックス143.BackColor = System.Drawing.Color.White;
            this.ボックス143.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス143.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス143.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス143.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス143.Height = 0.3937008F;
            this.ボックス143.Left = 1.653472F;
            this.ボックス143.Name = "ボックス143";
            this.ボックス143.RoundingRadius = 9.999999F;
            this.ボックス143.Tag = "";
            this.ボックス143.Top = 1.195669F;
            this.ボックス143.Width = 0.7480315F;
            // 
            // ボックス144
            // 
            this.ボックス144.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ボックス144.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス144.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス144.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス144.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス144.Height = 0.1968504F;
            this.ボックス144.Left = 1.653472F;
            this.ボックス144.Name = "ボックス144";
            this.ボックス144.RoundingRadius = 9.999999F;
            this.ボックス144.Tag = "";
            this.ボックス144.Top = 1.594488F;
            this.ボックス144.Width = 0.7480315F;
            // 
            // ボックス145
            // 
            this.ボックス145.BackColor = System.Drawing.Color.White;
            this.ボックス145.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス145.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス145.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス145.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス145.Height = 0.3937008F;
            this.ボックス145.Left = 1.653472F;
            this.ボックス145.Name = "ボックス145";
            this.ボックス145.RoundingRadius = 9.999999F;
            this.ボックス145.Tag = "";
            this.ボックス145.Top = 1.775591F;
            this.ボックス145.Width = 0.7480315F;
            // 
            // ボックス147
            // 
            this.ボックス147.BackColor = System.Drawing.Color.White;
            this.ボックス147.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス147.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス147.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス147.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス147.Height = 0.3937008F;
            this.ボックス147.Left = 2.401389F;
            this.ボックス147.Name = "ボックス147";
            this.ボックス147.RoundingRadius = 9.999999F;
            this.ボックス147.Tag = "";
            this.ボックス147.Top = 1.195669F;
            this.ボックス147.Width = 0.7480315F;
            // 
            // ボックス148
            // 
            this.ボックス148.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ボックス148.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス148.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス148.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス148.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス148.Height = 0.1968504F;
            this.ボックス148.Left = 2.401389F;
            this.ボックス148.Name = "ボックス148";
            this.ボックス148.RoundingRadius = 9.999999F;
            this.ボックス148.Tag = "";
            this.ボックス148.Top = 1.594488F;
            this.ボックス148.Width = 0.7480315F;
            // 
            // ボックス149
            // 
            this.ボックス149.BackColor = System.Drawing.Color.White;
            this.ボックス149.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス149.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス149.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス149.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス149.Height = 0.3937008F;
            this.ボックス149.Left = 2.401389F;
            this.ボックス149.Name = "ボックス149";
            this.ボックス149.RoundingRadius = 9.999999F;
            this.ボックス149.Tag = "";
            this.ボックス149.Top = 1.775591F;
            this.ボックス149.Width = 0.7480315F;
            // 
            // ボックス151
            // 
            this.ボックス151.BackColor = System.Drawing.Color.White;
            this.ボックス151.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス151.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス151.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス151.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス151.Height = 0.3937008F;
            this.ボックス151.Left = 3.149306F;
            this.ボックス151.Name = "ボックス151";
            this.ボックス151.RoundingRadius = 9.999999F;
            this.ボックス151.Tag = "";
            this.ボックス151.Top = 1.195669F;
            this.ボックス151.Width = 0.7480315F;
            // 
            // ボックス152
            // 
            this.ボックス152.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ボックス152.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス152.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス152.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス152.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス152.Height = 0.1968504F;
            this.ボックス152.Left = 3.149306F;
            this.ボックス152.Name = "ボックス152";
            this.ボックス152.RoundingRadius = 9.999999F;
            this.ボックス152.Tag = "";
            this.ボックス152.Top = 1.594488F;
            this.ボックス152.Width = 0.7480315F;
            // 
            // ボックス153
            // 
            this.ボックス153.BackColor = System.Drawing.Color.White;
            this.ボックス153.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス153.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス153.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス153.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス153.Height = 0.3937008F;
            this.ボックス153.Left = 3.149306F;
            this.ボックス153.Name = "ボックス153";
            this.ボックス153.RoundingRadius = 9.999999F;
            this.ボックス153.Tag = "";
            this.ボックス153.Top = 1.775591F;
            this.ボックス153.Width = 0.7480315F;
            // 
            // ボックス155
            // 
            this.ボックス155.BackColor = System.Drawing.Color.White;
            this.ボックス155.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス155.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス155.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス155.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス155.Height = 0.3937008F;
            this.ボックス155.Left = 3.897222F;
            this.ボックス155.Name = "ボックス155";
            this.ボックス155.RoundingRadius = 9.999999F;
            this.ボックス155.Tag = "";
            this.ボックス155.Top = 1.195669F;
            this.ボックス155.Width = 0.7480315F;
            // 
            // ボックス156
            // 
            this.ボックス156.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ボックス156.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス156.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス156.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス156.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス156.Height = 0.1968504F;
            this.ボックス156.Left = 3.897222F;
            this.ボックス156.Name = "ボックス156";
            this.ボックス156.RoundingRadius = 9.999999F;
            this.ボックス156.Tag = "";
            this.ボックス156.Top = 1.594488F;
            this.ボックス156.Width = 0.7480315F;
            // 
            // ボックス157
            // 
            this.ボックス157.BackColor = System.Drawing.Color.White;
            this.ボックス157.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス157.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス157.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス157.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス157.Height = 0.3937008F;
            this.ボックス157.Left = 3.897222F;
            this.ボックス157.Name = "ボックス157";
            this.ボックス157.RoundingRadius = 9.999999F;
            this.ボックス157.Tag = "";
            this.ボックス157.Top = 1.775591F;
            this.ボックス157.Width = 0.7480315F;
            // 
            // ボックス159
            // 
            this.ボックス159.BackColor = System.Drawing.Color.White;
            this.ボックス159.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス159.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス159.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス159.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス159.Height = 0.3937008F;
            this.ボックス159.Left = 4.645139F;
            this.ボックス159.Name = "ボックス159";
            this.ボックス159.RoundingRadius = 9.999999F;
            this.ボックス159.Tag = "";
            this.ボックス159.Top = 1.195669F;
            this.ボックス159.Width = 0.7480315F;
            // 
            // ボックス160
            // 
            this.ボックス160.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ボックス160.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス160.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス160.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス160.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス160.Height = 0.1968504F;
            this.ボックス160.Left = 4.645139F;
            this.ボックス160.Name = "ボックス160";
            this.ボックス160.RoundingRadius = 9.999999F;
            this.ボックス160.Tag = "";
            this.ボックス160.Top = 1.594488F;
            this.ボックス160.Width = 0.7480315F;
            // 
            // ボックス161
            // 
            this.ボックス161.BackColor = System.Drawing.Color.White;
            this.ボックス161.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス161.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス161.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス161.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス161.Height = 0.3937008F;
            this.ボックス161.Left = 4.645139F;
            this.ボックス161.Name = "ボックス161";
            this.ボックス161.RoundingRadius = 9.999999F;
            this.ボックス161.Tag = "";
            this.ボックス161.Top = 1.775591F;
            this.ボックス161.Width = 0.7480315F;
            // 
            // ボックス163
            // 
            this.ボックス163.BackColor = System.Drawing.Color.White;
            this.ボックス163.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス163.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス163.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス163.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス163.Height = 0.3937008F;
            this.ボックス163.Left = 5.393056F;
            this.ボックス163.Name = "ボックス163";
            this.ボックス163.RoundingRadius = 9.999999F;
            this.ボックス163.Tag = "";
            this.ボックス163.Top = 1.195669F;
            this.ボックス163.Width = 0.7480315F;
            // 
            // ボックス164
            // 
            this.ボックス164.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ボックス164.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス164.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス164.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス164.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス164.Height = 0.1968504F;
            this.ボックス164.Left = 5.393056F;
            this.ボックス164.Name = "ボックス164";
            this.ボックス164.RoundingRadius = 9.999999F;
            this.ボックス164.Tag = "";
            this.ボックス164.Top = 1.594488F;
            this.ボックス164.Width = 0.7480315F;
            // 
            // ボックス165
            // 
            this.ボックス165.BackColor = System.Drawing.Color.White;
            this.ボックス165.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス165.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス165.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス165.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス165.Height = 0.3937008F;
            this.ボックス165.Left = 5.393056F;
            this.ボックス165.Name = "ボックス165";
            this.ボックス165.RoundingRadius = 9.999999F;
            this.ボックス165.Tag = "";
            this.ボックス165.Top = 1.775591F;
            this.ボックス165.Width = 0.7480315F;
            // 
            // ボックス167
            // 
            this.ボックス167.BackColor = System.Drawing.Color.White;
            this.ボックス167.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス167.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス167.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス167.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス167.Height = 0.3937008F;
            this.ボックス167.Left = 6.140972F;
            this.ボックス167.Name = "ボックス167";
            this.ボックス167.RoundingRadius = 9.999999F;
            this.ボックス167.Tag = "";
            this.ボックス167.Top = 1.195669F;
            this.ボックス167.Width = 0.7480315F;
            // 
            // ボックス168
            // 
            this.ボックス168.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ボックス168.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス168.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス168.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス168.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス168.Height = 0.1968504F;
            this.ボックス168.Left = 6.140972F;
            this.ボックス168.Name = "ボックス168";
            this.ボックス168.RoundingRadius = 9.999999F;
            this.ボックス168.Tag = "";
            this.ボックス168.Top = 1.594488F;
            this.ボックス168.Width = 0.7480315F;
            // 
            // ボックス169
            // 
            this.ボックス169.BackColor = System.Drawing.Color.White;
            this.ボックス169.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス169.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス169.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス169.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス169.Height = 0.3937008F;
            this.ボックス169.Left = 6.140972F;
            this.ボックス169.Name = "ボックス169";
            this.ボックス169.RoundingRadius = 9.999999F;
            this.ボックス169.Tag = "";
            this.ボックス169.Top = 1.775591F;
            this.ボックス169.Width = 0.7480315F;
            // 
            // ボックス171
            // 
            this.ボックス171.BackColor = System.Drawing.Color.White;
            this.ボックス171.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス171.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス171.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス171.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス171.Height = 0.3937008F;
            this.ボックス171.Left = 6.888889F;
            this.ボックス171.Name = "ボックス171";
            this.ボックス171.RoundingRadius = 9.999999F;
            this.ボックス171.Tag = "";
            this.ボックス171.Top = 1.195669F;
            this.ボックス171.Width = 0.7480315F;
            // 
            // ボックス172
            // 
            this.ボックス172.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ボックス172.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス172.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス172.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス172.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス172.Height = 0.1968504F;
            this.ボックス172.Left = 6.888889F;
            this.ボックス172.Name = "ボックス172";
            this.ボックス172.RoundingRadius = 9.999999F;
            this.ボックス172.Tag = "";
            this.ボックス172.Top = 1.594488F;
            this.ボックス172.Width = 0.7480315F;
            // 
            // ボックス173
            // 
            this.ボックス173.BackColor = System.Drawing.Color.White;
            this.ボックス173.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス173.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス173.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス173.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス173.Height = 0.3937008F;
            this.ボックス173.Left = 6.888889F;
            this.ボックス173.Name = "ボックス173";
            this.ボックス173.RoundingRadius = 9.999999F;
            this.ボックス173.Tag = "";
            this.ボックス173.Top = 1.775591F;
            this.ボックス173.Width = 0.7480315F;
            // 
            // ITEM052
            // 
            this.ITEM052.DataField = "ITEM052";
            this.ITEM052.Height = 0.1968504F;
            this.ITEM052.Left = 0.9061353F;
            this.ITEM052.Name = "ITEM052";
            this.ITEM052.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ITEM052.Tag = "";
            this.ITEM052.Text = "ITEM052";
            this.ITEM052.Top = 1.023622F;
            this.ITEM052.Width = 0.7480315F;
            // 
            // ITEM053
            // 
            this.ITEM053.DataField = "ITEM053";
            this.ITEM053.Height = 0.1968504F;
            this.ITEM053.Left = 1.654052F;
            this.ITEM053.Name = "ITEM053";
            this.ITEM053.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ITEM053.Tag = "";
            this.ITEM053.Text = "ITEM053";
            this.ITEM053.Top = 1.023622F;
            this.ITEM053.Width = 0.7480315F;
            // 
            // ITEM054
            // 
            this.ITEM054.DataField = "ITEM054";
            this.ITEM054.Height = 0.1968504F;
            this.ITEM054.Left = 2.401969F;
            this.ITEM054.Name = "ITEM054";
            this.ITEM054.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ITEM054.Tag = "";
            this.ITEM054.Text = "ITEM054";
            this.ITEM054.Top = 1.023622F;
            this.ITEM054.Width = 0.7480315F;
            // 
            // ITEM055
            // 
            this.ITEM055.DataField = "ITEM055";
            this.ITEM055.Height = 0.1968504F;
            this.ITEM055.Left = 3.149886F;
            this.ITEM055.Name = "ITEM055";
            this.ITEM055.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ITEM055.Tag = "";
            this.ITEM055.Text = "ITEM055";
            this.ITEM055.Top = 1.023622F;
            this.ITEM055.Width = 0.7480315F;
            // 
            // ITEM056
            // 
            this.ITEM056.DataField = "ITEM056";
            this.ITEM056.Height = 0.1968504F;
            this.ITEM056.Left = 3.897802F;
            this.ITEM056.Name = "ITEM056";
            this.ITEM056.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ITEM056.Tag = "";
            this.ITEM056.Text = "ITEM056";
            this.ITEM056.Top = 1.023622F;
            this.ITEM056.Width = 0.7480315F;
            // 
            // ITEM057
            // 
            this.ITEM057.DataField = "ITEM057";
            this.ITEM057.Height = 0.1968504F;
            this.ITEM057.Left = 4.645719F;
            this.ITEM057.Name = "ITEM057";
            this.ITEM057.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ITEM057.Tag = "";
            this.ITEM057.Text = "ITEM057";
            this.ITEM057.Top = 1.023622F;
            this.ITEM057.Width = 0.7480315F;
            // 
            // ITEM058
            // 
            this.ITEM058.DataField = "ITEM058";
            this.ITEM058.Height = 0.1968504F;
            this.ITEM058.Left = 5.393636F;
            this.ITEM058.Name = "ITEM058";
            this.ITEM058.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ITEM058.Tag = "";
            this.ITEM058.Text = "ITEM058";
            this.ITEM058.Top = 1.023622F;
            this.ITEM058.Width = 0.7480315F;
            // 
            // ITEM059
            // 
            this.ITEM059.DataField = "ITEM059";
            this.ITEM059.Height = 0.1968504F;
            this.ITEM059.Left = 6.141552F;
            this.ITEM059.Name = "ITEM059";
            this.ITEM059.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ITEM059.Tag = "";
            this.ITEM059.Text = "ITEM059";
            this.ITEM059.Top = 1.023622F;
            this.ITEM059.Width = 0.7480315F;
            // 
            // ITEM060
            // 
            this.ITEM060.DataField = "ITEM060";
            this.ITEM060.Height = 0.1968504F;
            this.ITEM060.Left = 6.889469F;
            this.ITEM060.Name = "ITEM060";
            this.ITEM060.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ITEM060.Tag = "";
            this.ITEM060.Text = "ITEM060";
            this.ITEM060.Top = 1.023622F;
            this.ITEM060.Width = 0.7480315F;
            // 
            // ITEM071
            // 
            this.ITEM071.DataField = "ITEM071";
            this.ITEM071.Height = 0.1666667F;
            this.ITEM071.Left = 0.1334646F;
            this.ITEM071.Name = "ITEM071";
            this.ITEM071.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.ITEM071.Tag = "";
            this.ITEM071.Text = "ITEM071";
            this.ITEM071.Top = 1.338583F;
            this.ITEM071.Width = 0.7479166F;
            // 
            // ITEM072
            // 
            this.ITEM072.DataField = "ITEM072";
            this.ITEM072.Height = 0.1666667F;
            this.ITEM072.Left = 0.8815398F;
            this.ITEM072.Name = "ITEM072";
            this.ITEM072.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.ITEM072.Tag = "";
            this.ITEM072.Text = "ITEM072";
            this.ITEM072.Top = 1.338583F;
            this.ITEM072.Width = 0.7479166F;
            // 
            // ITEM073
            // 
            this.ITEM073.DataField = "ITEM073";
            this.ITEM073.Height = 0.1666667F;
            this.ITEM073.Left = 1.629456F;
            this.ITEM073.Name = "ITEM073";
            this.ITEM073.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.ITEM073.Tag = "";
            this.ITEM073.Text = "ITEM073";
            this.ITEM073.Top = 1.338583F;
            this.ITEM073.Width = 0.7479166F;
            // 
            // ITEM074
            // 
            this.ITEM074.DataField = "ITEM074";
            this.ITEM074.Height = 0.1666667F;
            this.ITEM074.Left = 2.377373F;
            this.ITEM074.Name = "ITEM074";
            this.ITEM074.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.ITEM074.Tag = "";
            this.ITEM074.Text = "ITEM074";
            this.ITEM074.Top = 1.338583F;
            this.ITEM074.Width = 0.7479166F;
            // 
            // ITEM075
            // 
            this.ITEM075.DataField = "ITEM075";
            this.ITEM075.Height = 0.1666667F;
            this.ITEM075.Left = 3.12529F;
            this.ITEM075.Name = "ITEM075";
            this.ITEM075.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.ITEM075.Tag = "";
            this.ITEM075.Text = "ITEM075";
            this.ITEM075.Top = 1.338583F;
            this.ITEM075.Width = 0.7479166F;
            // 
            // ITEM076
            // 
            this.ITEM076.DataField = "ITEM076";
            this.ITEM076.Height = 0.1666667F;
            this.ITEM076.Left = 3.873206F;
            this.ITEM076.Name = "ITEM076";
            this.ITEM076.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.ITEM076.Tag = "";
            this.ITEM076.Text = "ITEM076";
            this.ITEM076.Top = 1.338583F;
            this.ITEM076.Width = 0.7479166F;
            // 
            // ITEM077
            // 
            this.ITEM077.DataField = "ITEM077";
            this.ITEM077.Height = 0.1666667F;
            this.ITEM077.Left = 4.621123F;
            this.ITEM077.Name = "ITEM077";
            this.ITEM077.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.ITEM077.Tag = "";
            this.ITEM077.Text = "ITEM077";
            this.ITEM077.Top = 1.338583F;
            this.ITEM077.Width = 0.7479166F;
            // 
            // ITEM078
            // 
            this.ITEM078.DataField = "ITEM078";
            this.ITEM078.Height = 0.1666667F;
            this.ITEM078.Left = 5.36904F;
            this.ITEM078.Name = "ITEM078";
            this.ITEM078.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.ITEM078.Tag = "";
            this.ITEM078.Text = "ITEM078";
            this.ITEM078.Top = 1.338583F;
            this.ITEM078.Width = 0.7479166F;
            // 
            // ITEM079
            // 
            this.ITEM079.DataField = "ITEM079";
            this.ITEM079.Height = 0.1666667F;
            this.ITEM079.Left = 6.116956F;
            this.ITEM079.Name = "ITEM079";
            this.ITEM079.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.ITEM079.Tag = "";
            this.ITEM079.Text = "ITEM079";
            this.ITEM079.Top = 1.338583F;
            this.ITEM079.Width = 0.7479166F;
            // 
            // ITEM080
            // 
            this.ITEM080.DataField = "ITEM080";
            this.ITEM080.Height = 0.1666667F;
            this.ITEM080.Left = 6.864873F;
            this.ITEM080.Name = "ITEM080";
            this.ITEM080.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.ITEM080.Tag = "";
            this.ITEM080.Text = "ITEM080";
            this.ITEM080.Top = 1.338583F;
            this.ITEM080.Width = 0.7479166F;
            // 
            // ITEM081
            // 
            this.ITEM081.DataField = "ITEM081";
            this.ITEM081.Height = 0.1666667F;
            this.ITEM081.Left = 0.1335082F;
            this.ITEM081.Name = "ITEM081";
            this.ITEM081.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.ITEM081.Tag = "";
            this.ITEM081.Text = "ITEM081";
            this.ITEM081.Top = 1.933071F;
            this.ITEM081.Width = 0.7479166F;
            // 
            // ITEM082
            // 
            this.ITEM082.DataField = "ITEM082";
            this.ITEM082.Height = 0.1666667F;
            this.ITEM082.Left = 0.8815834F;
            this.ITEM082.Name = "ITEM082";
            this.ITEM082.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.ITEM082.Tag = "";
            this.ITEM082.Text = "ITEM082";
            this.ITEM082.Top = 1.933071F;
            this.ITEM082.Width = 0.7479166F;
            // 
            // ITEM083
            // 
            this.ITEM083.DataField = "ITEM083";
            this.ITEM083.Height = 0.1666667F;
            this.ITEM083.Left = 1.6295F;
            this.ITEM083.Name = "ITEM083";
            this.ITEM083.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.ITEM083.Tag = "";
            this.ITEM083.Text = "ITEM083";
            this.ITEM083.Top = 1.933071F;
            this.ITEM083.Width = 0.7479166F;
            // 
            // ITEM084
            // 
            this.ITEM084.DataField = "ITEM084";
            this.ITEM084.Height = 0.1666667F;
            this.ITEM084.Left = 2.377417F;
            this.ITEM084.Name = "ITEM084";
            this.ITEM084.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.ITEM084.Tag = "";
            this.ITEM084.Text = "ITEM084";
            this.ITEM084.Top = 1.933071F;
            this.ITEM084.Width = 0.7479166F;
            // 
            // ITEM085
            // 
            this.ITEM085.DataField = "ITEM085";
            this.ITEM085.Height = 0.1666667F;
            this.ITEM085.Left = 3.125334F;
            this.ITEM085.Name = "ITEM085";
            this.ITEM085.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.ITEM085.Tag = "";
            this.ITEM085.Text = "ITEM085";
            this.ITEM085.Top = 1.933071F;
            this.ITEM085.Width = 0.7479166F;
            // 
            // ITEM086
            // 
            this.ITEM086.DataField = "ITEM086";
            this.ITEM086.Height = 0.1666667F;
            this.ITEM086.Left = 3.87325F;
            this.ITEM086.Name = "ITEM086";
            this.ITEM086.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.ITEM086.Tag = "";
            this.ITEM086.Text = "ITEM086";
            this.ITEM086.Top = 1.933071F;
            this.ITEM086.Width = 0.7479166F;
            // 
            // ITEM087
            // 
            this.ITEM087.DataField = "ITEM087";
            this.ITEM087.Height = 0.1666667F;
            this.ITEM087.Left = 4.621167F;
            this.ITEM087.Name = "ITEM087";
            this.ITEM087.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.ITEM087.Tag = "";
            this.ITEM087.Text = "ITEM087";
            this.ITEM087.Top = 1.933071F;
            this.ITEM087.Width = 0.7479166F;
            // 
            // ITEM088
            // 
            this.ITEM088.DataField = "ITEM088";
            this.ITEM088.Height = 0.1666667F;
            this.ITEM088.Left = 5.369083F;
            this.ITEM088.Name = "ITEM088";
            this.ITEM088.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.ITEM088.Tag = "";
            this.ITEM088.Text = "ITEM088";
            this.ITEM088.Top = 1.933071F;
            this.ITEM088.Width = 0.7479166F;
            // 
            // ITEM089
            // 
            this.ITEM089.DataField = "ITEM089";
            this.ITEM089.Height = 0.1666667F;
            this.ITEM089.Left = 6.117F;
            this.ITEM089.Name = "ITEM089";
            this.ITEM089.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.ITEM089.Tag = "";
            this.ITEM089.Text = "ITEM089";
            this.ITEM089.Top = 1.933071F;
            this.ITEM089.Width = 0.7479166F;
            // 
            // ITEM090
            // 
            this.ITEM090.DataField = "ITEM090";
            this.ITEM090.Height = 0.1666667F;
            this.ITEM090.Left = 6.864917F;
            this.ITEM090.Name = "ITEM090";
            this.ITEM090.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.ITEM090.Tag = "";
            this.ITEM090.Text = "ITEM090";
            this.ITEM090.Top = 1.933071F;
            this.ITEM090.Width = 0.7479166F;
            // 
            // ボックス219
            // 
            this.ボックス219.BackColor = System.Drawing.Color.White;
            this.ボックス219.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス219.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス219.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス219.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス219.Height = 0.3937008F;
            this.ボックス219.Left = 0.1574803F;
            this.ボックス219.Name = "ボックス219";
            this.ボックス219.RoundingRadius = 9.999999F;
            this.ボックス219.Tag = "";
            this.ボックス219.Top = 3.216536F;
            this.ボックス219.Width = 0.7480315F;
            // 
            // ボックス223
            // 
            this.ボックス223.BackColor = System.Drawing.Color.White;
            this.ボックス223.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス223.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス223.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス223.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス223.Height = 0.3937008F;
            this.ボックス223.Left = 0.9053478F;
            this.ボックス223.Name = "ボックス223";
            this.ボックス223.RoundingRadius = 9.999999F;
            this.ボックス223.Tag = "";
            this.ボックス223.Top = 3.216536F;
            this.ボックス223.Width = 0.7480315F;
            // 
            // ボックス227
            // 
            this.ボックス227.BackColor = System.Drawing.Color.White;
            this.ボックス227.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス227.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス227.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス227.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス227.Height = 0.3937008F;
            this.ボックス227.Left = 1.653264F;
            this.ボックス227.Name = "ボックス227";
            this.ボックス227.RoundingRadius = 9.999999F;
            this.ボックス227.Tag = "";
            this.ボックス227.Top = 3.216536F;
            this.ボックス227.Width = 0.7480315F;
            // 
            // ボックス231
            // 
            this.ボックス231.BackColor = System.Drawing.Color.White;
            this.ボックス231.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス231.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス231.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス231.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス231.Height = 0.3937008F;
            this.ボックス231.Left = 2.401181F;
            this.ボックス231.Name = "ボックス231";
            this.ボックス231.RoundingRadius = 9.999999F;
            this.ボックス231.Tag = "";
            this.ボックス231.Top = 3.216536F;
            this.ボックス231.Width = 0.7480315F;
            // 
            // ボックス235
            // 
            this.ボックス235.BackColor = System.Drawing.Color.White;
            this.ボックス235.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス235.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス235.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス235.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス235.Height = 0.3937008F;
            this.ボックス235.Left = 3.149098F;
            this.ボックス235.Name = "ボックス235";
            this.ボックス235.RoundingRadius = 9.999999F;
            this.ボックス235.Tag = "";
            this.ボックス235.Top = 3.216536F;
            this.ボックス235.Width = 0.7480315F;
            // 
            // ボックス239
            // 
            this.ボックス239.BackColor = System.Drawing.Color.White;
            this.ボックス239.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス239.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス239.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス239.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス239.Height = 0.3937008F;
            this.ボックス239.Left = 3.897014F;
            this.ボックス239.Name = "ボックス239";
            this.ボックス239.RoundingRadius = 9.999999F;
            this.ボックス239.Tag = "";
            this.ボックス239.Top = 3.216536F;
            this.ボックス239.Width = 0.7480315F;
            // 
            // ボックス243
            // 
            this.ボックス243.BackColor = System.Drawing.Color.White;
            this.ボックス243.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス243.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス243.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス243.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス243.Height = 0.3937008F;
            this.ボックス243.Left = 4.645024F;
            this.ボックス243.Name = "ボックス243";
            this.ボックス243.RoundingRadius = 9.999999F;
            this.ボックス243.Tag = "";
            this.ボックス243.Top = 3.216536F;
            this.ボックス243.Width = 0.7480315F;
            // 
            // ボックス247
            // 
            this.ボックス247.BackColor = System.Drawing.Color.White;
            this.ボックス247.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス247.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス247.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス247.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス247.Height = 0.3937008F;
            this.ボックス247.Left = 5.392941F;
            this.ボックス247.Name = "ボックス247";
            this.ボックス247.RoundingRadius = 9.999999F;
            this.ボックス247.Tag = "";
            this.ボックス247.Top = 3.216536F;
            this.ボックス247.Width = 0.7480315F;
            // 
            // ボックス251
            // 
            this.ボックス251.BackColor = System.Drawing.Color.White;
            this.ボックス251.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス251.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス251.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス251.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス251.Height = 0.3937008F;
            this.ボックス251.Left = 6.140857F;
            this.ボックス251.Name = "ボックス251";
            this.ボックス251.RoundingRadius = 9.999999F;
            this.ボックス251.Tag = "";
            this.ボックス251.Top = 3.216536F;
            this.ボックス251.Width = 0.7480315F;
            // 
            // ボックス255
            // 
            this.ボックス255.BackColor = System.Drawing.Color.White;
            this.ボックス255.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス255.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス255.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス255.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス255.Height = 0.3937008F;
            this.ボックス255.Left = 6.888774F;
            this.ボックス255.Name = "ボックス255";
            this.ボックス255.RoundingRadius = 9.999999F;
            this.ボックス255.Tag = "";
            this.ボックス255.Top = 3.216536F;
            this.ボックス255.Width = 0.7480315F;
            // 
            // ITEM121
            // 
            this.ITEM121.DataField = "ITEM121";
            this.ITEM121.Height = 0.1666667F;
            this.ITEM121.Left = 0.1333613F;
            this.ITEM121.Name = "ITEM121";
            this.ITEM121.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.ITEM121.Tag = "";
            this.ITEM121.Text = "ITEM121";
            this.ITEM121.Top = 3.358268F;
            this.ITEM121.Width = 0.7479166F;
            // 
            // ITEM122
            // 
            this.ITEM122.DataField = "ITEM122";
            this.ITEM122.Height = 0.1666667F;
            this.ITEM122.Left = 0.881278F;
            this.ITEM122.Name = "ITEM122";
            this.ITEM122.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.ITEM122.Tag = "";
            this.ITEM122.Text = "ITEM122";
            this.ITEM122.Top = 3.358268F;
            this.ITEM122.Width = 0.7479166F;
            // 
            // ITEM123
            // 
            this.ITEM123.DataField = "ITEM123";
            this.ITEM123.Height = 0.1666667F;
            this.ITEM123.Left = 1.629194F;
            this.ITEM123.Name = "ITEM123";
            this.ITEM123.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.ITEM123.Tag = "";
            this.ITEM123.Text = "ITEM123";
            this.ITEM123.Top = 3.358268F;
            this.ITEM123.Width = 0.7479166F;
            // 
            // ITEM124
            // 
            this.ITEM124.DataField = "ITEM124";
            this.ITEM124.Height = 0.1666667F;
            this.ITEM124.Left = 2.377111F;
            this.ITEM124.Name = "ITEM124";
            this.ITEM124.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.ITEM124.Tag = "";
            this.ITEM124.Text = "ITEM124";
            this.ITEM124.Top = 3.358268F;
            this.ITEM124.Width = 0.7479166F;
            // 
            // ITEM125
            // 
            this.ITEM125.DataField = "ITEM125";
            this.ITEM125.Height = 0.1666667F;
            this.ITEM125.Left = 3.125029F;
            this.ITEM125.Name = "ITEM125";
            this.ITEM125.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.ITEM125.Tag = "";
            this.ITEM125.Text = "ITEM125";
            this.ITEM125.Top = 3.358268F;
            this.ITEM125.Width = 0.7479166F;
            // 
            // ITEM126
            // 
            this.ITEM126.DataField = "ITEM126";
            this.ITEM126.Height = 0.1666667F;
            this.ITEM126.Left = 3.872945F;
            this.ITEM126.Name = "ITEM126";
            this.ITEM126.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.ITEM126.Tag = "";
            this.ITEM126.Text = "ITEM126";
            this.ITEM126.Top = 3.358268F;
            this.ITEM126.Width = 0.7479166F;
            // 
            // ITEM127
            // 
            this.ITEM127.DataField = "ITEM127";
            this.ITEM127.Height = 0.1666667F;
            this.ITEM127.Left = 4.621167F;
            this.ITEM127.Name = "ITEM127";
            this.ITEM127.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.ITEM127.Tag = "";
            this.ITEM127.Text = "ITEM127";
            this.ITEM127.Top = 3.358268F;
            this.ITEM127.Width = 0.7479166F;
            // 
            // ITEM128
            // 
            this.ITEM128.DataField = "ITEM128";
            this.ITEM128.Height = 0.1666667F;
            this.ITEM128.Left = 5.369083F;
            this.ITEM128.Name = "ITEM128";
            this.ITEM128.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.ITEM128.Tag = "";
            this.ITEM128.Text = "ITEM128";
            this.ITEM128.Top = 3.358268F;
            this.ITEM128.Width = 0.7479166F;
            // 
            // ITEM129
            // 
            this.ITEM129.DataField = "ITEM129";
            this.ITEM129.Height = 0.1666667F;
            this.ITEM129.Left = 6.117F;
            this.ITEM129.Name = "ITEM129";
            this.ITEM129.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.ITEM129.Tag = "";
            this.ITEM129.Text = "ITEM129";
            this.ITEM129.Top = 3.358268F;
            this.ITEM129.Width = 0.7479166F;
            // 
            // ITEM130
            // 
            this.ITEM130.DataField = "ITEM130";
            this.ITEM130.Height = 0.1666667F;
            this.ITEM130.Left = 6.864917F;
            this.ITEM130.Name = "ITEM130";
            this.ITEM130.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.ITEM130.Tag = "";
            this.ITEM130.Text = "ITEM130";
            this.ITEM130.Top = 3.358268F;
            this.ITEM130.Width = 0.7479166F;
            // 
            // ボックス297
            // 
            this.ボックス297.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ボックス297.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス297.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス297.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス297.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス297.Height = 0.1968504F;
            this.ボックス297.Left = 0.1576389F;
            this.ボックス297.Name = "ボックス297";
            this.ボックス297.RoundingRadius = 9.999999F;
            this.ボックス297.Tag = "";
            this.ボックス297.Top = 3.858268F;
            this.ボックス297.Width = 0.7480315F;
            // 
            // ボックス298
            // 
            this.ボックス298.BackColor = System.Drawing.Color.White;
            this.ボックス298.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス298.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス298.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス298.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス298.Height = 0.3937008F;
            this.ボックス298.Left = 0.1574803F;
            this.ボックス298.Name = "ボックス298";
            this.ボックス298.RoundingRadius = 9.999999F;
            this.ボックス298.Tag = "";
            this.ボックス298.Top = 4.055906F;
            this.ボックス298.Width = 0.7480315F;
            // 
            // ボックス302
            // 
            this.ボックス302.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ボックス302.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス302.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス302.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス302.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス302.Height = 0.1968504F;
            this.ボックス302.Left = 0.9055555F;
            this.ボックス302.Name = "ボックス302";
            this.ボックス302.RoundingRadius = 9.999999F;
            this.ボックス302.Tag = "";
            this.ボックス302.Top = 3.858268F;
            this.ボックス302.Width = 0.7480315F;
            // 
            // ボックス303
            // 
            this.ボックス303.BackColor = System.Drawing.Color.White;
            this.ボックス303.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス303.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス303.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス303.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス303.Height = 0.3937008F;
            this.ボックス303.Left = 0.9053969F;
            this.ボックス303.Name = "ボックス303";
            this.ボックス303.RoundingRadius = 9.999999F;
            this.ボックス303.Tag = "";
            this.ボックス303.Top = 4.055906F;
            this.ボックス303.Width = 0.7480315F;
            // 
            // ボックス306
            // 
            this.ボックス306.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ボックス306.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス306.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス306.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス306.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス306.Height = 0.1968504F;
            this.ボックス306.Left = 1.653472F;
            this.ボックス306.Name = "ボックス306";
            this.ボックス306.RoundingRadius = 9.999999F;
            this.ボックス306.Tag = "";
            this.ボックス306.Top = 3.858268F;
            this.ボックス306.Width = 0.7480315F;
            // 
            // ボックス307
            // 
            this.ボックス307.BackColor = System.Drawing.Color.White;
            this.ボックス307.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス307.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス307.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス307.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス307.Height = 0.3937008F;
            this.ボックス307.Left = 1.653313F;
            this.ボックス307.Name = "ボックス307";
            this.ボックス307.RoundingRadius = 9.999999F;
            this.ボックス307.Tag = "";
            this.ボックス307.Top = 4.055906F;
            this.ボックス307.Width = 0.7480315F;
            // 
            // ボックス310
            // 
            this.ボックス310.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ボックス310.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス310.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス310.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス310.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス310.Height = 0.1968504F;
            this.ボックス310.Left = 2.401389F;
            this.ボックス310.Name = "ボックス310";
            this.ボックス310.RoundingRadius = 9.999999F;
            this.ボックス310.Tag = "";
            this.ボックス310.Top = 3.858268F;
            this.ボックス310.Width = 0.7480315F;
            // 
            // ボックス311
            // 
            this.ボックス311.BackColor = System.Drawing.Color.White;
            this.ボックス311.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス311.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス311.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス311.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス311.Height = 0.3937008F;
            this.ボックス311.Left = 2.40123F;
            this.ボックス311.Name = "ボックス311";
            this.ボックス311.RoundingRadius = 9.999999F;
            this.ボックス311.Tag = "";
            this.ボックス311.Top = 4.055906F;
            this.ボックス311.Width = 0.7480315F;
            // 
            // ボックス314
            // 
            this.ボックス314.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ボックス314.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス314.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス314.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス314.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス314.Height = 0.1968504F;
            this.ボックス314.Left = 3.149306F;
            this.ボックス314.Name = "ボックス314";
            this.ボックス314.RoundingRadius = 9.999999F;
            this.ボックス314.Tag = "";
            this.ボックス314.Top = 3.858268F;
            this.ボックス314.Width = 0.7480315F;
            // 
            // ボックス315
            // 
            this.ボックス315.BackColor = System.Drawing.Color.White;
            this.ボックス315.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス315.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス315.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス315.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス315.Height = 0.3937008F;
            this.ボックス315.Left = 3.149147F;
            this.ボックス315.Name = "ボックス315";
            this.ボックス315.RoundingRadius = 9.999999F;
            this.ボックス315.Tag = "";
            this.ボックス315.Top = 4.055906F;
            this.ボックス315.Width = 0.7480315F;
            // 
            // ボックス318
            // 
            this.ボックス318.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ボックス318.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス318.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス318.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス318.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス318.Height = 0.1968504F;
            this.ボックス318.Left = 3.897222F;
            this.ボックス318.Name = "ボックス318";
            this.ボックス318.RoundingRadius = 9.999999F;
            this.ボックス318.Tag = "";
            this.ボックス318.Top = 3.858268F;
            this.ボックス318.Width = 0.7480315F;
            // 
            // ボックス319
            // 
            this.ボックス319.BackColor = System.Drawing.Color.White;
            this.ボックス319.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス319.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス319.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス319.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス319.Height = 0.3937008F;
            this.ボックス319.Left = 3.897063F;
            this.ボックス319.Name = "ボックス319";
            this.ボックス319.RoundingRadius = 9.999999F;
            this.ボックス319.Tag = "";
            this.ボックス319.Top = 4.055906F;
            this.ボックス319.Width = 0.7480315F;
            // 
            // ボックス322
            // 
            this.ボックス322.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ボックス322.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス322.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス322.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス322.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス322.Height = 0.1968504F;
            this.ボックス322.Left = 4.645139F;
            this.ボックス322.Name = "ボックス322";
            this.ボックス322.RoundingRadius = 9.999999F;
            this.ボックス322.Tag = "";
            this.ボックス322.Top = 3.858268F;
            this.ボックス322.Width = 0.7480315F;
            // 
            // ボックス323
            // 
            this.ボックス323.BackColor = System.Drawing.Color.White;
            this.ボックス323.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス323.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス323.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス323.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス323.Height = 0.3937008F;
            this.ボックス323.Left = 4.644981F;
            this.ボックス323.Name = "ボックス323";
            this.ボックス323.RoundingRadius = 9.999999F;
            this.ボックス323.Tag = "";
            this.ボックス323.Top = 4.055906F;
            this.ボックス323.Width = 0.7480315F;
            // 
            // ボックス326
            // 
            this.ボックス326.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ボックス326.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス326.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス326.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス326.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス326.Height = 0.1968504F;
            this.ボックス326.Left = 5.393056F;
            this.ボックス326.Name = "ボックス326";
            this.ボックス326.RoundingRadius = 9.999999F;
            this.ボックス326.Tag = "";
            this.ボックス326.Top = 3.858268F;
            this.ボックス326.Width = 0.7480315F;
            // 
            // ボックス327
            // 
            this.ボックス327.BackColor = System.Drawing.Color.White;
            this.ボックス327.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス327.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス327.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス327.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス327.Height = 0.3937008F;
            this.ボックス327.Left = 5.392897F;
            this.ボックス327.Name = "ボックス327";
            this.ボックス327.RoundingRadius = 9.999999F;
            this.ボックス327.Tag = "";
            this.ボックス327.Top = 4.055906F;
            this.ボックス327.Width = 0.7480315F;
            // 
            // ボックス330
            // 
            this.ボックス330.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ボックス330.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス330.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス330.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス330.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス330.Height = 0.1968504F;
            this.ボックス330.Left = 6.140972F;
            this.ボックス330.Name = "ボックス330";
            this.ボックス330.RoundingRadius = 9.999999F;
            this.ボックス330.Tag = "";
            this.ボックス330.Top = 3.858268F;
            this.ボックス330.Width = 0.7480315F;
            // 
            // ボックス331
            // 
            this.ボックス331.BackColor = System.Drawing.Color.White;
            this.ボックス331.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス331.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス331.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス331.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス331.Height = 0.3937008F;
            this.ボックス331.Left = 6.140813F;
            this.ボックス331.Name = "ボックス331";
            this.ボックス331.RoundingRadius = 9.999999F;
            this.ボックス331.Tag = "";
            this.ボックス331.Top = 4.055906F;
            this.ボックス331.Width = 0.7480315F;
            // 
            // ボックス334
            // 
            this.ボックス334.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ボックス334.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス334.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス334.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス334.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス334.Height = 0.1968504F;
            this.ボックス334.Left = 6.888889F;
            this.ボックス334.Name = "ボックス334";
            this.ボックス334.RoundingRadius = 9.999999F;
            this.ボックス334.Tag = "";
            this.ボックス334.Top = 3.858268F;
            this.ボックス334.Width = 0.7480315F;
            // 
            // ボックス335
            // 
            this.ボックス335.BackColor = System.Drawing.Color.White;
            this.ボックス335.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス335.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス335.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス335.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ボックス335.Height = 0.3937008F;
            this.ボックス335.Left = 6.88873F;
            this.ボックス335.Name = "ボックス335";
            this.ボックス335.RoundingRadius = 9.999999F;
            this.ボックス335.Tag = "";
            this.ボックス335.Top = 4.055906F;
            this.ボックス335.Width = 0.7480315F;
            // 
            // ITEM131
            // 
            this.ITEM131.DataField = "ITEM131";
            this.ITEM131.Height = 0.1666667F;
            this.ITEM131.Left = 0.1574803F;
            this.ITEM131.Name = "ITEM131";
            this.ITEM131.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ITEM131.Tag = "";
            this.ITEM131.Text = "ITEM131";
            this.ITEM131.Top = 3.893701F;
            this.ITEM131.Width = 0.7479166F;
            // 
            // ITEM132
            // 
            this.ITEM132.DataField = "ITEM132";
            this.ITEM132.Height = 0.1666667F;
            this.ITEM132.Left = 0.9053969F;
            this.ITEM132.Name = "ITEM132";
            this.ITEM132.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ITEM132.Tag = "";
            this.ITEM132.Text = "ITEM132";
            this.ITEM132.Top = 3.893701F;
            this.ITEM132.Width = 0.7479166F;
            // 
            // ITEM133
            // 
            this.ITEM133.DataField = "ITEM133";
            this.ITEM133.Height = 0.1666667F;
            this.ITEM133.Left = 1.653313F;
            this.ITEM133.Name = "ITEM133";
            this.ITEM133.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ITEM133.Tag = "";
            this.ITEM133.Text = "ITEM133";
            this.ITEM133.Top = 3.893701F;
            this.ITEM133.Width = 0.7479166F;
            // 
            // ITEM134
            // 
            this.ITEM134.DataField = "ITEM134";
            this.ITEM134.Height = 0.1666667F;
            this.ITEM134.Left = 2.40123F;
            this.ITEM134.Name = "ITEM134";
            this.ITEM134.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ITEM134.Tag = "";
            this.ITEM134.Text = "ITEM134";
            this.ITEM134.Top = 3.893701F;
            this.ITEM134.Width = 0.7479166F;
            // 
            // ITEM135
            // 
            this.ITEM135.DataField = "ITEM135";
            this.ITEM135.Height = 0.1666667F;
            this.ITEM135.Left = 3.149148F;
            this.ITEM135.Name = "ITEM135";
            this.ITEM135.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ITEM135.Tag = "";
            this.ITEM135.Text = "ITEM135";
            this.ITEM135.Top = 3.893701F;
            this.ITEM135.Width = 0.7479166F;
            // 
            // ITEM136
            // 
            this.ITEM136.DataField = "ITEM136";
            this.ITEM136.Height = 0.1666667F;
            this.ITEM136.Left = 3.897063F;
            this.ITEM136.Name = "ITEM136";
            this.ITEM136.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ITEM136.Tag = "";
            this.ITEM136.Text = "ITEM136";
            this.ITEM136.Top = 3.893701F;
            this.ITEM136.Width = 0.7479166F;
            // 
            // ITEM137
            // 
            this.ITEM137.DataField = "ITEM137";
            this.ITEM137.Height = 0.1666667F;
            this.ITEM137.Left = 4.644981F;
            this.ITEM137.Name = "ITEM137";
            this.ITEM137.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ITEM137.Tag = "";
            this.ITEM137.Text = "ITEM137";
            this.ITEM137.Top = 3.893701F;
            this.ITEM137.Width = 0.7479166F;
            // 
            // ITEM138
            // 
            this.ITEM138.DataField = "ITEM138";
            this.ITEM138.Height = 0.1666667F;
            this.ITEM138.Left = 5.392897F;
            this.ITEM138.Name = "ITEM138";
            this.ITEM138.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ITEM138.Tag = "";
            this.ITEM138.Text = "ITEM138";
            this.ITEM138.Top = 3.893701F;
            this.ITEM138.Width = 0.7479166F;
            // 
            // ITEM139
            // 
            this.ITEM139.DataField = "ITEM139";
            this.ITEM139.Height = 0.1666667F;
            this.ITEM139.Left = 6.140813F;
            this.ITEM139.Name = "ITEM139";
            this.ITEM139.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ITEM139.Tag = "";
            this.ITEM139.Text = "ITEM139";
            this.ITEM139.Top = 3.893701F;
            this.ITEM139.Width = 0.7479166F;
            // 
            // ITEM140
            // 
            this.ITEM140.DataField = "ITEM140";
            this.ITEM140.Height = 0.1666667F;
            this.ITEM140.Left = 6.888731F;
            this.ITEM140.Name = "ITEM140";
            this.ITEM140.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ITEM140.Tag = "";
            this.ITEM140.Text = "ITEM140";
            this.ITEM140.Top = 3.893701F;
            this.ITEM140.Width = 0.7479166F;
            // 
            // ITEM141
            // 
            this.ITEM141.DataField = "ITEM141";
            this.ITEM141.Height = 0.1666667F;
            this.ITEM141.Left = 0.1299213F;
            this.ITEM141.Name = "ITEM141";
            this.ITEM141.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.ITEM141.Tag = "";
            this.ITEM141.Text = "ITEM141";
            this.ITEM141.Top = 4.196851F;
            this.ITEM141.Width = 0.7479166F;
            // 
            // ITEM142
            // 
            this.ITEM142.DataField = "ITEM142";
            this.ITEM142.Height = 0.1666667F;
            this.ITEM142.Left = 0.8778378F;
            this.ITEM142.Name = "ITEM142";
            this.ITEM142.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.ITEM142.Tag = "";
            this.ITEM142.Text = "ITEM142";
            this.ITEM142.Top = 4.196851F;
            this.ITEM142.Width = 0.7479166F;
            // 
            // ITEM143
            // 
            this.ITEM143.DataField = "ITEM143";
            this.ITEM143.Height = 0.1666667F;
            this.ITEM143.Left = 1.625754F;
            this.ITEM143.Name = "ITEM143";
            this.ITEM143.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.ITEM143.Tag = "";
            this.ITEM143.Text = "ITEM143";
            this.ITEM143.Top = 4.196851F;
            this.ITEM143.Width = 0.7479166F;
            // 
            // ITEM144
            // 
            this.ITEM144.DataField = "ITEM144";
            this.ITEM144.Height = 0.1666667F;
            this.ITEM144.Left = 2.373671F;
            this.ITEM144.Name = "ITEM144";
            this.ITEM144.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.ITEM144.Tag = "";
            this.ITEM144.Text = "ITEM144";
            this.ITEM144.Top = 4.196851F;
            this.ITEM144.Width = 0.7479166F;
            // 
            // ITEM145
            // 
            this.ITEM145.DataField = "ITEM145";
            this.ITEM145.Height = 0.1666667F;
            this.ITEM145.Left = 3.121588F;
            this.ITEM145.Name = "ITEM145";
            this.ITEM145.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.ITEM145.Tag = "";
            this.ITEM145.Text = "ITEM145";
            this.ITEM145.Top = 4.196851F;
            this.ITEM145.Width = 0.7479166F;
            // 
            // ITEM146
            // 
            this.ITEM146.DataField = "ITEM146";
            this.ITEM146.Height = 0.1666667F;
            this.ITEM146.Left = 3.869504F;
            this.ITEM146.Name = "ITEM146";
            this.ITEM146.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.ITEM146.Tag = "";
            this.ITEM146.Text = "ITEM146";
            this.ITEM146.Top = 4.196851F;
            this.ITEM146.Width = 0.7479166F;
            // 
            // ITEM147
            // 
            this.ITEM147.DataField = "ITEM147";
            this.ITEM147.Height = 0.1666667F;
            this.ITEM147.Left = 4.617423F;
            this.ITEM147.Name = "ITEM147";
            this.ITEM147.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.ITEM147.Tag = "";
            this.ITEM147.Text = "ITEM147";
            this.ITEM147.Top = 4.196851F;
            this.ITEM147.Width = 0.7479166F;
            // 
            // ITEM148
            // 
            this.ITEM148.DataField = "ITEM148";
            this.ITEM148.Height = 0.1666667F;
            this.ITEM148.Left = 5.365339F;
            this.ITEM148.Name = "ITEM148";
            this.ITEM148.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.ITEM148.Tag = "";
            this.ITEM148.Text = "ITEM148";
            this.ITEM148.Top = 4.196851F;
            this.ITEM148.Width = 0.7479166F;
            // 
            // ITEM149
            // 
            this.ITEM149.DataField = "ITEM149";
            this.ITEM149.Height = 0.1666667F;
            this.ITEM149.Left = 6.113256F;
            this.ITEM149.Name = "ITEM149";
            this.ITEM149.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.ITEM149.Tag = "";
            this.ITEM149.Text = "ITEM149";
            this.ITEM149.Top = 4.196851F;
            this.ITEM149.Width = 0.7479166F;
            // 
            // ITEM150
            // 
            this.ITEM150.DataField = "ITEM150";
            this.ITEM150.Height = 0.1666667F;
            this.ITEM150.Left = 6.861173F;
            this.ITEM150.Name = "ITEM150";
            this.ITEM150.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11pt; font-weight: normal; text-alig" +
    "n: right; ddo-char-set: 1";
            this.ITEM150.Tag = "";
            this.ITEM150.Text = "ITEM150";
            this.ITEM150.Top = 4.196851F;
            this.ITEM150.Width = 0.7479166F;
            // 
            // ITEM070
            // 
            this.ITEM070.DataField = "ITEM070";
            this.ITEM070.Height = 0.1968504F;
            this.ITEM070.Left = 6.888889F;
            this.ITEM070.Name = "ITEM070";
            this.ITEM070.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ITEM070.Tag = "";
            this.ITEM070.Text = "ITEM070";
            this.ITEM070.Top = 1.629921F;
            this.ITEM070.Width = 0.7480315F;
            // 
            // ITEM069
            // 
            this.ITEM069.DataField = "ITEM069";
            this.ITEM069.Height = 0.1968504F;
            this.ITEM069.Left = 6.140972F;
            this.ITEM069.Name = "ITEM069";
            this.ITEM069.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ITEM069.Tag = "";
            this.ITEM069.Text = "ITEM069";
            this.ITEM069.Top = 1.629921F;
            this.ITEM069.Width = 0.7480315F;
            // 
            // ITEM068
            // 
            this.ITEM068.DataField = "ITEM068";
            this.ITEM068.Height = 0.1968504F;
            this.ITEM068.Left = 5.393056F;
            this.ITEM068.Name = "ITEM068";
            this.ITEM068.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ITEM068.Tag = "";
            this.ITEM068.Text = "ITEM068";
            this.ITEM068.Top = 1.629921F;
            this.ITEM068.Width = 0.7480315F;
            // 
            // ITEM067
            // 
            this.ITEM067.DataField = "ITEM067";
            this.ITEM067.Height = 0.1968504F;
            this.ITEM067.Left = 4.645139F;
            this.ITEM067.Name = "ITEM067";
            this.ITEM067.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ITEM067.Tag = "";
            this.ITEM067.Text = "ITEM067";
            this.ITEM067.Top = 1.629921F;
            this.ITEM067.Width = 0.7480315F;
            // 
            // ITEM066
            // 
            this.ITEM066.DataField = "ITEM066";
            this.ITEM066.Height = 0.1968504F;
            this.ITEM066.Left = 3.897222F;
            this.ITEM066.Name = "ITEM066";
            this.ITEM066.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ITEM066.Tag = "";
            this.ITEM066.Text = "ITEM066";
            this.ITEM066.Top = 1.629921F;
            this.ITEM066.Width = 0.7480315F;
            // 
            // ITEM065
            // 
            this.ITEM065.DataField = "ITEM065";
            this.ITEM065.Height = 0.1968504F;
            this.ITEM065.Left = 3.149306F;
            this.ITEM065.Name = "ITEM065";
            this.ITEM065.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ITEM065.Tag = "";
            this.ITEM065.Text = "ITEM065";
            this.ITEM065.Top = 1.629921F;
            this.ITEM065.Width = 0.7480315F;
            // 
            // ITEM064
            // 
            this.ITEM064.DataField = "ITEM064";
            this.ITEM064.Height = 0.1968504F;
            this.ITEM064.Left = 2.401389F;
            this.ITEM064.Name = "ITEM064";
            this.ITEM064.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ITEM064.Tag = "";
            this.ITEM064.Text = "ITEM064";
            this.ITEM064.Top = 1.629921F;
            this.ITEM064.Width = 0.7480315F;
            // 
            // ITEM063
            // 
            this.ITEM063.DataField = "ITEM063";
            this.ITEM063.Height = 0.1968504F;
            this.ITEM063.Left = 1.653472F;
            this.ITEM063.Name = "ITEM063";
            this.ITEM063.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ITEM063.Tag = "";
            this.ITEM063.Text = "ITEM063";
            this.ITEM063.Top = 1.629921F;
            this.ITEM063.Width = 0.7480315F;
            // 
            // ITEM062
            // 
            this.ITEM062.DataField = "ITEM062";
            this.ITEM062.Height = 0.1968504F;
            this.ITEM062.Left = 0.9055555F;
            this.ITEM062.Name = "ITEM062";
            this.ITEM062.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ITEM062.Tag = "";
            this.ITEM062.Text = "ITEM062";
            this.ITEM062.Top = 1.629921F;
            this.ITEM062.Width = 0.7480315F;
            // 
            // ITEM061
            // 
            this.ITEM061.DataField = "ITEM061";
            this.ITEM061.Height = 0.1968504F;
            this.ITEM061.Left = 0.1574803F;
            this.ITEM061.Name = "ITEM061";
            this.ITEM061.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8.25pt; font-weight: normal; text-al" +
    "ign: center; ddo-char-set: 1";
            this.ITEM061.Tag = "";
            this.ITEM061.Text = "ITEM061";
            this.ITEM061.Top = 1.629921F;
            this.ITEM061.Width = 0.7480315F;
            // 
            // KYUR2021R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0F;
            this.PageSettings.Margins.Left = 0.3937007F;
            this.PageSettings.Margins.Right = 0F;
            this.PageSettings.Margins.Top = 0.1968504F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Portrait;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.267716F;
            this.PrintWidth = 7.645669F;
            this.Sections.Add(this.detail);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.ITEM101)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル338)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル174)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM100)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM120)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM119)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM118)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM113)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM116)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM115)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM117)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM114)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM112)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM099)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM098)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM097)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM096)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM095)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM094)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM093)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM092)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM091)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM111)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM110)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM109)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM108)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM107)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM106)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM105)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM104)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM103)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM102)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル256)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM051)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM001)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM006)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM005)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM002)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM003)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM004)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM007)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM052)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM053)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM054)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM055)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM056)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM057)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM058)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM059)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM060)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM071)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM072)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM073)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM074)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM075)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM076)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM077)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM078)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM079)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM080)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM081)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM082)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM083)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM084)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM085)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM086)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM087)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM088)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM089)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM090)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM121)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM122)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM123)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM124)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM125)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM126)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM127)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM128)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM129)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM130)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM131)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM132)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM133)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM134)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM135)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM136)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM137)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM138)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM139)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM140)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM141)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM142)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM143)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM144)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM145)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM146)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM147)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM148)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM149)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM150)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM070)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM069)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM068)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM067)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM066)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM065)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM064)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM063)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM062)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM061)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM001;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM006;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス40;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス41;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス42;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス43;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス44;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス45;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル0;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル4;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル37;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル38;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM005;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM002;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM003;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM004;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル49;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM007;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス133;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス134;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス135;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス136;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス137;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス138;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス139;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス140;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス141;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス142;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス143;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス144;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス145;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス146;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス147;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス148;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス149;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス150;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス151;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス152;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス153;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス154;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス155;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス156;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス157;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス158;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス159;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス160;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス161;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス162;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス163;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス164;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス165;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス166;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス167;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス168;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス169;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス170;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス171;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス172;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス173;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル174;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM051;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM052;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM053;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM054;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM055;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM056;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM057;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM058;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM059;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM060;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM071;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM072;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM073;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM074;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM075;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM076;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM077;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM078;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM079;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM080;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM061;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM062;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM063;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM064;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM065;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM066;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM067;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM068;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM069;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM070;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM081;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM082;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM083;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM084;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM085;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM086;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM087;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM088;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM089;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM090;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス215;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス216;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス217;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス218;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス219;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス220;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス221;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス222;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス223;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス224;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス225;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス226;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス227;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス228;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス229;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス230;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス231;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス232;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス233;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス234;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス235;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス236;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス237;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス238;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス239;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス240;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス241;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス242;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス243;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス244;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス245;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス246;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス247;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス248;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス249;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス250;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス251;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス252;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス253;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス254;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス255;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル256;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM091;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM092;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM093;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM094;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM095;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM096;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM097;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM098;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM099;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM100;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM111;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM112;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM113;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM114;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM115;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM116;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM117;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM118;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM119;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM120;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM101;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM102;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM103;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM104;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM105;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM106;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM107;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM108;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM109;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM110;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM121;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM122;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM123;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM124;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM125;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM126;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM127;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM128;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM129;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM130;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス297;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス298;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス299;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス302;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス303;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス306;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス307;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス310;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス311;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス314;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス315;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス318;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス319;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス322;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス323;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス326;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス327;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス330;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス331;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス334;
        private GrapeCity.ActiveReports.SectionReportModel.Shape ボックス335;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル338;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM131;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM132;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM133;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM134;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM135;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM136;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM137;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM138;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM139;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM140;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM141;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM142;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM143;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM144;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM145;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM146;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM147;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM148;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM149;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM150;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape1;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape2;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape3;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape4;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape5;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape6;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape7;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape8;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape9;
    }
}
