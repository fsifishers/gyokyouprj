﻿using System.Data;

using jp.co.fsi.common.report;

namespace jp.co.fsi.ky.kysr1011
{
    /// <summary>
    /// KYSR1011R の帳票
    /// </summary>
    public partial class KYSR1011R : BaseReport
    {

        public KYSR1011R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }
    }
}
