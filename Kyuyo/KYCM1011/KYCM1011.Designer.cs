﻿namespace jp.co.fsi.ky.kycm1011
{
    partial class KYCM1011
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpKoyohoken = new System.Windows.Forms.GroupBox();
            this.txtKoyoHokenHihokenshaFtnKs = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKoyoHokenHihokenshaFtnKs = new System.Windows.Forms.Label();
            this.txtKoyoHokenHihokenshaFtnRt = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKoyoHokenHihokenshaFtnRt = new System.Windows.Forms.Label();
            this.lblKoyoHokenGyoshuNm = new System.Windows.Forms.Label();
            this.txtKoyoHokenGyoshu = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKoyoHokenGyoshu = new System.Windows.Forms.Label();
            this.lblKoyoHokenKeisanHohoNm = new System.Windows.Forms.Label();
            this.txtKoyoHokenKeisanHoho = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKoyoHokenKeisanHoho = new System.Windows.Forms.Label();
            this.grpKenkohoken = new System.Windows.Forms.GroupBox();
            this.txtKenkoHokenJigyoshoBango = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKenkoHokenJigyoshoBango = new System.Windows.Forms.Label();
            this.txtKenkoHokenJigyoshoSeiriKg = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKenkoHokenJigyoshoSeiriKg = new System.Windows.Forms.Label();
            this.txtKnkhknHihokenshaFtnKsKig = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKnkhknHihokenshaFtnKsKig = new System.Windows.Forms.Label();
            this.txtKnkhknHihokenshaFtnRtKig = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKnkhknHihokenshaFtnRtKig = new System.Windows.Forms.Label();
            this.txtKenkoHokenHihokenshaFtnKs = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKenkoHokenHihokenshaFtnKs = new System.Windows.Forms.Label();
            this.txtKenkoHokenHihokenshaFtnRt = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKenkoHokenHihokenshaFtnRt = new System.Windows.Forms.Label();
            this.grpKoseinenkin = new System.Windows.Forms.GroupBox();
            this.txtKoseiNenkinJigyoshoBango = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKoseiNenkinJigyoshoBango = new System.Windows.Forms.Label();
            this.txtKoseiNenkinJigyoshoSeiriKg = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKoseiNenkinJigyoshoSeiriKg = new System.Windows.Forms.Label();
            this.txtKoseiNenkinHihokenshaFtnKs = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKoseiNenkinHihokenshaFtnKs = new System.Windows.Forms.Label();
            this.txtKoseiNenkinHihokenshaFtnRt = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKoseiNenkinHihokenshaFtnRt = new System.Windows.Forms.Label();
            this.grpGinkoJoho = new System.Windows.Forms.GroupBox();
            this.txtShumokuCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtItakuCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblItakuCd = new System.Windows.Forms.Label();
            this.txtShitenCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGinkoNm = new System.Windows.Forms.Label();
            this.txtGinkoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShitenCd = new System.Windows.Forms.Label();
            this.lblGinkoCd = new System.Windows.Forms.Label();
            this.lblShumokuCd = new System.Windows.Forms.Label();
            this.lblShitenNm = new System.Windows.Forms.Label();
            this.grpShotokuzei = new System.Windows.Forms.GroupBox();
            this.lblShotokuzeiKeisanHohoNm = new System.Windows.Forms.Label();
            this.txtShotokuzeiKeisanHoho = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShotokuzeiKeisanHoho = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.grpKoyohoken.SuspendLayout();
            this.grpKenkohoken.SuspendLayout();
            this.grpKoseinenkin.SuspendLayout();
            this.grpGinkoJoho.SuspendLayout();
            this.grpShotokuzei.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Text = "会社情報の登録";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            // 
            // grpKoyohoken
            // 
            this.grpKoyohoken.Controls.Add(this.txtKoyoHokenHihokenshaFtnKs);
            this.grpKoyohoken.Controls.Add(this.lblKoyoHokenHihokenshaFtnKs);
            this.grpKoyohoken.Controls.Add(this.txtKoyoHokenHihokenshaFtnRt);
            this.grpKoyohoken.Controls.Add(this.lblKoyoHokenHihokenshaFtnRt);
            this.grpKoyohoken.Controls.Add(this.lblKoyoHokenGyoshuNm);
            this.grpKoyohoken.Controls.Add(this.txtKoyoHokenGyoshu);
            this.grpKoyohoken.Controls.Add(this.lblKoyoHokenGyoshu);
            this.grpKoyohoken.Controls.Add(this.lblKoyoHokenKeisanHohoNm);
            this.grpKoyohoken.Controls.Add(this.txtKoyoHokenKeisanHoho);
            this.grpKoyohoken.Controls.Add(this.lblKoyoHokenKeisanHoho);
            this.grpKoyohoken.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.grpKoyohoken.Location = new System.Drawing.Point(17, 50);
            this.grpKoyohoken.Name = "grpKoyohoken";
            this.grpKoyohoken.Size = new System.Drawing.Size(347, 95);
            this.grpKoyohoken.TabIndex = 0;
            this.grpKoyohoken.TabStop = false;
            this.grpKoyohoken.Text = "雇用保険";
            // 
            // txtKoyoHokenHihokenshaFtnKs
            // 
            this.txtKoyoHokenHihokenshaFtnKs.AutoSizeFromLength = false;
            this.txtKoyoHokenHihokenshaFtnKs.DisplayLength = null;
            this.txtKoyoHokenHihokenshaFtnKs.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKoyoHokenHihokenshaFtnKs.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKoyoHokenHihokenshaFtnKs.Location = new System.Drawing.Point(222, 64);
            this.txtKoyoHokenHihokenshaFtnKs.MaxLength = 4;
            this.txtKoyoHokenHihokenshaFtnKs.Name = "txtKoyoHokenHihokenshaFtnKs";
            this.txtKoyoHokenHihokenshaFtnKs.Size = new System.Drawing.Size(63, 23);
            this.txtKoyoHokenHihokenshaFtnKs.TabIndex = 3;
            this.txtKoyoHokenHihokenshaFtnKs.Text = "0";
            this.txtKoyoHokenHihokenshaFtnKs.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKoyoHokenHihokenshaFtnKs.Validating += new System.ComponentModel.CancelEventHandler(this.txtKoyoHokenHihokenshaFtnKs_Validating);
            // 
            // lblKoyoHokenHihokenshaFtnKs
            // 
            this.lblKoyoHokenHihokenshaFtnKs.BackColor = System.Drawing.Color.Silver;
            this.lblKoyoHokenHihokenshaFtnKs.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKoyoHokenHihokenshaFtnKs.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKoyoHokenHihokenshaFtnKs.Location = new System.Drawing.Point(198, 64);
            this.lblKoyoHokenHihokenshaFtnKs.Name = "lblKoyoHokenHihokenshaFtnKs";
            this.lblKoyoHokenHihokenshaFtnKs.Size = new System.Drawing.Size(24, 23);
            this.lblKoyoHokenHihokenshaFtnKs.TabIndex = 920;
            this.lblKoyoHokenHihokenshaFtnKs.Text = "／";
            this.lblKoyoHokenHihokenshaFtnKs.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtKoyoHokenHihokenshaFtnRt
            // 
            this.txtKoyoHokenHihokenshaFtnRt.AutoSizeFromLength = false;
            this.txtKoyoHokenHihokenshaFtnRt.DisplayLength = null;
            this.txtKoyoHokenHihokenshaFtnRt.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKoyoHokenHihokenshaFtnRt.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKoyoHokenHihokenshaFtnRt.Location = new System.Drawing.Point(134, 64);
            this.txtKoyoHokenHihokenshaFtnRt.MaxLength = 6;
            this.txtKoyoHokenHihokenshaFtnRt.Name = "txtKoyoHokenHihokenshaFtnRt";
            this.txtKoyoHokenHihokenshaFtnRt.Size = new System.Drawing.Size(64, 23);
            this.txtKoyoHokenHihokenshaFtnRt.TabIndex = 2;
            this.txtKoyoHokenHihokenshaFtnRt.Text = "0";
            this.txtKoyoHokenHihokenshaFtnRt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKoyoHokenHihokenshaFtnRt.Validating += new System.ComponentModel.CancelEventHandler(this.txtKoyoHokenHihokenshaFtnRt_Validating);
            // 
            // lblKoyoHokenHihokenshaFtnRt
            // 
            this.lblKoyoHokenHihokenshaFtnRt.BackColor = System.Drawing.Color.Silver;
            this.lblKoyoHokenHihokenshaFtnRt.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKoyoHokenHihokenshaFtnRt.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKoyoHokenHihokenshaFtnRt.Location = new System.Drawing.Point(16, 64);
            this.lblKoyoHokenHihokenshaFtnRt.Name = "lblKoyoHokenHihokenshaFtnRt";
            this.lblKoyoHokenHihokenshaFtnRt.Size = new System.Drawing.Size(118, 23);
            this.lblKoyoHokenHihokenshaFtnRt.TabIndex = 919;
            this.lblKoyoHokenHihokenshaFtnRt.Text = "被保険者負担率";
            this.lblKoyoHokenHihokenshaFtnRt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKoyoHokenGyoshuNm
            // 
            this.lblKoyoHokenGyoshuNm.BackColor = System.Drawing.Color.Silver;
            this.lblKoyoHokenGyoshuNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKoyoHokenGyoshuNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKoyoHokenGyoshuNm.Location = new System.Drawing.Point(152, 41);
            this.lblKoyoHokenGyoshuNm.Name = "lblKoyoHokenGyoshuNm";
            this.lblKoyoHokenGyoshuNm.Size = new System.Drawing.Size(178, 23);
            this.lblKoyoHokenGyoshuNm.TabIndex = 917;
            this.lblKoyoHokenGyoshuNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKoyoHokenGyoshu
            // 
            this.txtKoyoHokenGyoshu.AutoSizeFromLength = true;
            this.txtKoyoHokenGyoshu.DisplayLength = null;
            this.txtKoyoHokenGyoshu.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKoyoHokenGyoshu.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtKoyoHokenGyoshu.Location = new System.Drawing.Point(134, 41);
            this.txtKoyoHokenGyoshu.MaxLength = 1;
            this.txtKoyoHokenGyoshu.Name = "txtKoyoHokenGyoshu";
            this.txtKoyoHokenGyoshu.Size = new System.Drawing.Size(18, 23);
            this.txtKoyoHokenGyoshu.TabIndex = 1;
            this.txtKoyoHokenGyoshu.Text = "0";
            this.txtKoyoHokenGyoshu.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtKoyoHokenGyoshu.Validating += new System.ComponentModel.CancelEventHandler(this.txtKoyoHokenGyoshu_Validating);
            // 
            // lblKoyoHokenGyoshu
            // 
            this.lblKoyoHokenGyoshu.BackColor = System.Drawing.Color.Silver;
            this.lblKoyoHokenGyoshu.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKoyoHokenGyoshu.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKoyoHokenGyoshu.Location = new System.Drawing.Point(16, 41);
            this.lblKoyoHokenGyoshu.Name = "lblKoyoHokenGyoshu";
            this.lblKoyoHokenGyoshu.Size = new System.Drawing.Size(118, 23);
            this.lblKoyoHokenGyoshu.TabIndex = 916;
            this.lblKoyoHokenGyoshu.Text = "業　　　　　種";
            this.lblKoyoHokenGyoshu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKoyoHokenKeisanHohoNm
            // 
            this.lblKoyoHokenKeisanHohoNm.BackColor = System.Drawing.Color.Silver;
            this.lblKoyoHokenKeisanHohoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKoyoHokenKeisanHohoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKoyoHokenKeisanHohoNm.Location = new System.Drawing.Point(152, 18);
            this.lblKoyoHokenKeisanHohoNm.Name = "lblKoyoHokenKeisanHohoNm";
            this.lblKoyoHokenKeisanHohoNm.Size = new System.Drawing.Size(178, 23);
            this.lblKoyoHokenKeisanHohoNm.TabIndex = 915;
            this.lblKoyoHokenKeisanHohoNm.Text = "0:しない　1:する";
            this.lblKoyoHokenKeisanHohoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKoyoHokenKeisanHoho
            // 
            this.txtKoyoHokenKeisanHoho.AutoSizeFromLength = true;
            this.txtKoyoHokenKeisanHoho.DisplayLength = null;
            this.txtKoyoHokenKeisanHoho.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKoyoHokenKeisanHoho.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtKoyoHokenKeisanHoho.Location = new System.Drawing.Point(134, 18);
            this.txtKoyoHokenKeisanHoho.MaxLength = 1;
            this.txtKoyoHokenKeisanHoho.Name = "txtKoyoHokenKeisanHoho";
            this.txtKoyoHokenKeisanHoho.Size = new System.Drawing.Size(18, 23);
            this.txtKoyoHokenKeisanHoho.TabIndex = 0;
            this.txtKoyoHokenKeisanHoho.Text = "0";
            this.txtKoyoHokenKeisanHoho.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtKoyoHokenKeisanHoho.Validating += new System.ComponentModel.CancelEventHandler(this.txtKoyoHokenKeisanHoho_Validating);
            // 
            // lblKoyoHokenKeisanHoho
            // 
            this.lblKoyoHokenKeisanHoho.BackColor = System.Drawing.Color.Silver;
            this.lblKoyoHokenKeisanHoho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKoyoHokenKeisanHoho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKoyoHokenKeisanHoho.Location = new System.Drawing.Point(16, 18);
            this.lblKoyoHokenKeisanHoho.Name = "lblKoyoHokenKeisanHoho";
            this.lblKoyoHokenKeisanHoho.Size = new System.Drawing.Size(118, 23);
            this.lblKoyoHokenKeisanHoho.TabIndex = 914;
            this.lblKoyoHokenKeisanHoho.Text = "計　算　方　法";
            this.lblKoyoHokenKeisanHoho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // grpKenkohoken
            // 
            this.grpKenkohoken.Controls.Add(this.txtKenkoHokenJigyoshoBango);
            this.grpKenkohoken.Controls.Add(this.lblKenkoHokenJigyoshoBango);
            this.grpKenkohoken.Controls.Add(this.txtKenkoHokenJigyoshoSeiriKg);
            this.grpKenkohoken.Controls.Add(this.lblKenkoHokenJigyoshoSeiriKg);
            this.grpKenkohoken.Controls.Add(this.txtKnkhknHihokenshaFtnKsKig);
            this.grpKenkohoken.Controls.Add(this.lblKnkhknHihokenshaFtnKsKig);
            this.grpKenkohoken.Controls.Add(this.txtKnkhknHihokenshaFtnRtKig);
            this.grpKenkohoken.Controls.Add(this.lblKnkhknHihokenshaFtnRtKig);
            this.grpKenkohoken.Controls.Add(this.txtKenkoHokenHihokenshaFtnKs);
            this.grpKenkohoken.Controls.Add(this.lblKenkoHokenHihokenshaFtnKs);
            this.grpKenkohoken.Controls.Add(this.txtKenkoHokenHihokenshaFtnRt);
            this.grpKenkohoken.Controls.Add(this.lblKenkoHokenHihokenshaFtnRt);
            this.grpKenkohoken.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.grpKenkohoken.Location = new System.Drawing.Point(17, 161);
            this.grpKenkohoken.Name = "grpKenkohoken";
            this.grpKenkohoken.Size = new System.Drawing.Size(347, 118);
            this.grpKenkohoken.TabIndex = 1;
            this.grpKenkohoken.TabStop = false;
            this.grpKenkohoken.Text = "健康保険";
            // 
            // txtKenkoHokenJigyoshoBango
            // 
            this.txtKenkoHokenJigyoshoBango.AutoSizeFromLength = false;
            this.txtKenkoHokenJigyoshoBango.DisplayLength = null;
            this.txtKenkoHokenJigyoshoBango.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKenkoHokenJigyoshoBango.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtKenkoHokenJigyoshoBango.Location = new System.Drawing.Point(134, 87);
            this.txtKenkoHokenJigyoshoBango.MaxLength = 15;
            this.txtKenkoHokenJigyoshoBango.Name = "txtKenkoHokenJigyoshoBango";
            this.txtKenkoHokenJigyoshoBango.Size = new System.Drawing.Size(171, 23);
            this.txtKenkoHokenJigyoshoBango.TabIndex = 5;
            this.txtKenkoHokenJigyoshoBango.Validating += new System.ComponentModel.CancelEventHandler(this.txtKenkoHokenJigyoshoBango_Validating);
            // 
            // lblKenkoHokenJigyoshoBango
            // 
            this.lblKenkoHokenJigyoshoBango.BackColor = System.Drawing.Color.Silver;
            this.lblKenkoHokenJigyoshoBango.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKenkoHokenJigyoshoBango.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKenkoHokenJigyoshoBango.Location = new System.Drawing.Point(16, 87);
            this.lblKenkoHokenJigyoshoBango.Name = "lblKenkoHokenJigyoshoBango";
            this.lblKenkoHokenJigyoshoBango.Size = new System.Drawing.Size(118, 23);
            this.lblKenkoHokenJigyoshoBango.TabIndex = 935;
            this.lblKenkoHokenJigyoshoBango.Text = "事 業 所 番 号";
            this.lblKenkoHokenJigyoshoBango.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKenkoHokenJigyoshoSeiriKg
            // 
            this.txtKenkoHokenJigyoshoSeiriKg.AutoSizeFromLength = false;
            this.txtKenkoHokenJigyoshoSeiriKg.DisplayLength = null;
            this.txtKenkoHokenJigyoshoSeiriKg.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKenkoHokenJigyoshoSeiriKg.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtKenkoHokenJigyoshoSeiriKg.Location = new System.Drawing.Point(134, 64);
            this.txtKenkoHokenJigyoshoSeiriKg.MaxLength = 20;
            this.txtKenkoHokenJigyoshoSeiriKg.Name = "txtKenkoHokenJigyoshoSeiriKg";
            this.txtKenkoHokenJigyoshoSeiriKg.Size = new System.Drawing.Size(171, 23);
            this.txtKenkoHokenJigyoshoSeiriKg.TabIndex = 4;
            this.txtKenkoHokenJigyoshoSeiriKg.Validating += new System.ComponentModel.CancelEventHandler(this.txtKenkoHokenJigyoshoSeiriKg_Validating);
            // 
            // lblKenkoHokenJigyoshoSeiriKg
            // 
            this.lblKenkoHokenJigyoshoSeiriKg.BackColor = System.Drawing.Color.Silver;
            this.lblKenkoHokenJigyoshoSeiriKg.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKenkoHokenJigyoshoSeiriKg.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKenkoHokenJigyoshoSeiriKg.Location = new System.Drawing.Point(16, 64);
            this.lblKenkoHokenJigyoshoSeiriKg.Name = "lblKenkoHokenJigyoshoSeiriKg";
            this.lblKenkoHokenJigyoshoSeiriKg.Size = new System.Drawing.Size(118, 23);
            this.lblKenkoHokenJigyoshoSeiriKg.TabIndex = 933;
            this.lblKenkoHokenJigyoshoSeiriKg.Text = "事業所整理記号";
            this.lblKenkoHokenJigyoshoSeiriKg.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKnkhknHihokenshaFtnKsKig
            // 
            this.txtKnkhknHihokenshaFtnKsKig.AutoSizeFromLength = false;
            this.txtKnkhknHihokenshaFtnKsKig.DisplayLength = null;
            this.txtKnkhknHihokenshaFtnKsKig.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKnkhknHihokenshaFtnKsKig.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKnkhknHihokenshaFtnKsKig.Location = new System.Drawing.Point(222, 41);
            this.txtKnkhknHihokenshaFtnKsKig.MaxLength = 4;
            this.txtKnkhknHihokenshaFtnKsKig.Name = "txtKnkhknHihokenshaFtnKsKig";
            this.txtKnkhknHihokenshaFtnKsKig.Size = new System.Drawing.Size(63, 23);
            this.txtKnkhknHihokenshaFtnKsKig.TabIndex = 3;
            this.txtKnkhknHihokenshaFtnKsKig.Text = "0";
            this.txtKnkhknHihokenshaFtnKsKig.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKnkhknHihokenshaFtnKsKig.Validating += new System.ComponentModel.CancelEventHandler(this.txtKnkhknHihokenshaFtnKsKig_Validating);
            // 
            // lblKnkhknHihokenshaFtnKsKig
            // 
            this.lblKnkhknHihokenshaFtnKsKig.BackColor = System.Drawing.Color.Silver;
            this.lblKnkhknHihokenshaFtnKsKig.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKnkhknHihokenshaFtnKsKig.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKnkhknHihokenshaFtnKsKig.Location = new System.Drawing.Point(198, 41);
            this.lblKnkhknHihokenshaFtnKsKig.Name = "lblKnkhknHihokenshaFtnKsKig";
            this.lblKnkhknHihokenshaFtnKsKig.Size = new System.Drawing.Size(24, 23);
            this.lblKnkhknHihokenshaFtnKsKig.TabIndex = 930;
            this.lblKnkhknHihokenshaFtnKsKig.Text = "／";
            this.lblKnkhknHihokenshaFtnKsKig.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtKnkhknHihokenshaFtnRtKig
            // 
            this.txtKnkhknHihokenshaFtnRtKig.AutoSizeFromLength = false;
            this.txtKnkhknHihokenshaFtnRtKig.DisplayLength = null;
            this.txtKnkhknHihokenshaFtnRtKig.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKnkhknHihokenshaFtnRtKig.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKnkhknHihokenshaFtnRtKig.Location = new System.Drawing.Point(134, 41);
            this.txtKnkhknHihokenshaFtnRtKig.MaxLength = 6;
            this.txtKnkhknHihokenshaFtnRtKig.Name = "txtKnkhknHihokenshaFtnRtKig";
            this.txtKnkhknHihokenshaFtnRtKig.Size = new System.Drawing.Size(64, 23);
            this.txtKnkhknHihokenshaFtnRtKig.TabIndex = 2;
            this.txtKnkhknHihokenshaFtnRtKig.Text = "0";
            this.txtKnkhknHihokenshaFtnRtKig.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKnkhknHihokenshaFtnRtKig.Validating += new System.ComponentModel.CancelEventHandler(this.txtKnkhknHihokenshaFtnRtKig_Validating);
            // 
            // lblKnkhknHihokenshaFtnRtKig
            // 
            this.lblKnkhknHihokenshaFtnRtKig.BackColor = System.Drawing.Color.Silver;
            this.lblKnkhknHihokenshaFtnRtKig.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKnkhknHihokenshaFtnRtKig.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKnkhknHihokenshaFtnRtKig.Location = new System.Drawing.Point(16, 41);
            this.lblKnkhknHihokenshaFtnRtKig.Name = "lblKnkhknHihokenshaFtnRtKig";
            this.lblKnkhknHihokenshaFtnRtKig.Size = new System.Drawing.Size(118, 23);
            this.lblKnkhknHihokenshaFtnRtKig.TabIndex = 929;
            this.lblKnkhknHihokenshaFtnRtKig.Text = "介護保険該当率";
            this.lblKnkhknHihokenshaFtnRtKig.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKenkoHokenHihokenshaFtnKs
            // 
            this.txtKenkoHokenHihokenshaFtnKs.AutoSizeFromLength = false;
            this.txtKenkoHokenHihokenshaFtnKs.DisplayLength = null;
            this.txtKenkoHokenHihokenshaFtnKs.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKenkoHokenHihokenshaFtnKs.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKenkoHokenHihokenshaFtnKs.Location = new System.Drawing.Point(222, 18);
            this.txtKenkoHokenHihokenshaFtnKs.MaxLength = 4;
            this.txtKenkoHokenHihokenshaFtnKs.Name = "txtKenkoHokenHihokenshaFtnKs";
            this.txtKenkoHokenHihokenshaFtnKs.Size = new System.Drawing.Size(63, 23);
            this.txtKenkoHokenHihokenshaFtnKs.TabIndex = 1;
            this.txtKenkoHokenHihokenshaFtnKs.Text = "0";
            this.txtKenkoHokenHihokenshaFtnKs.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKenkoHokenHihokenshaFtnKs.Validating += new System.ComponentModel.CancelEventHandler(this.txtKenkoHokenHihokenshaFtnKs_Validating);
            // 
            // lblKenkoHokenHihokenshaFtnKs
            // 
            this.lblKenkoHokenHihokenshaFtnKs.BackColor = System.Drawing.Color.Silver;
            this.lblKenkoHokenHihokenshaFtnKs.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKenkoHokenHihokenshaFtnKs.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKenkoHokenHihokenshaFtnKs.Location = new System.Drawing.Point(198, 18);
            this.lblKenkoHokenHihokenshaFtnKs.Name = "lblKenkoHokenHihokenshaFtnKs";
            this.lblKenkoHokenHihokenshaFtnKs.Size = new System.Drawing.Size(24, 23);
            this.lblKenkoHokenHihokenshaFtnKs.TabIndex = 926;
            this.lblKenkoHokenHihokenshaFtnKs.Text = "／";
            this.lblKenkoHokenHihokenshaFtnKs.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtKenkoHokenHihokenshaFtnRt
            // 
            this.txtKenkoHokenHihokenshaFtnRt.AutoSizeFromLength = false;
            this.txtKenkoHokenHihokenshaFtnRt.DisplayLength = null;
            this.txtKenkoHokenHihokenshaFtnRt.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKenkoHokenHihokenshaFtnRt.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKenkoHokenHihokenshaFtnRt.Location = new System.Drawing.Point(134, 18);
            this.txtKenkoHokenHihokenshaFtnRt.MaxLength = 6;
            this.txtKenkoHokenHihokenshaFtnRt.Name = "txtKenkoHokenHihokenshaFtnRt";
            this.txtKenkoHokenHihokenshaFtnRt.Size = new System.Drawing.Size(64, 23);
            this.txtKenkoHokenHihokenshaFtnRt.TabIndex = 0;
            this.txtKenkoHokenHihokenshaFtnRt.Text = "0";
            this.txtKenkoHokenHihokenshaFtnRt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKenkoHokenHihokenshaFtnRt.Validating += new System.ComponentModel.CancelEventHandler(this.txtKenkoHokenHihokenshaFtnRt_Validating);
            // 
            // lblKenkoHokenHihokenshaFtnRt
            // 
            this.lblKenkoHokenHihokenshaFtnRt.BackColor = System.Drawing.Color.Silver;
            this.lblKenkoHokenHihokenshaFtnRt.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKenkoHokenHihokenshaFtnRt.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKenkoHokenHihokenshaFtnRt.Location = new System.Drawing.Point(16, 18);
            this.lblKenkoHokenHihokenshaFtnRt.Name = "lblKenkoHokenHihokenshaFtnRt";
            this.lblKenkoHokenHihokenshaFtnRt.Size = new System.Drawing.Size(118, 23);
            this.lblKenkoHokenHihokenshaFtnRt.TabIndex = 925;
            this.lblKenkoHokenHihokenshaFtnRt.Text = "被保険者負担率";
            this.lblKenkoHokenHihokenshaFtnRt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // grpKoseinenkin
            // 
            this.grpKoseinenkin.Controls.Add(this.txtKoseiNenkinJigyoshoBango);
            this.grpKoseinenkin.Controls.Add(this.lblKoseiNenkinJigyoshoBango);
            this.grpKoseinenkin.Controls.Add(this.txtKoseiNenkinJigyoshoSeiriKg);
            this.grpKoseinenkin.Controls.Add(this.lblKoseiNenkinJigyoshoSeiriKg);
            this.grpKoseinenkin.Controls.Add(this.txtKoseiNenkinHihokenshaFtnKs);
            this.grpKoseinenkin.Controls.Add(this.lblKoseiNenkinHihokenshaFtnKs);
            this.grpKoseinenkin.Controls.Add(this.txtKoseiNenkinHihokenshaFtnRt);
            this.grpKoseinenkin.Controls.Add(this.lblKoseiNenkinHihokenshaFtnRt);
            this.grpKoseinenkin.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.grpKoseinenkin.Location = new System.Drawing.Point(17, 292);
            this.grpKoseinenkin.Name = "grpKoseinenkin";
            this.grpKoseinenkin.Size = new System.Drawing.Size(347, 93);
            this.grpKoseinenkin.TabIndex = 2;
            this.grpKoseinenkin.TabStop = false;
            this.grpKoseinenkin.Text = "厚生年金";
            // 
            // txtKoseiNenkinJigyoshoBango
            // 
            this.txtKoseiNenkinJigyoshoBango.AutoSizeFromLength = false;
            this.txtKoseiNenkinJigyoshoBango.DisplayLength = null;
            this.txtKoseiNenkinJigyoshoBango.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKoseiNenkinJigyoshoBango.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtKoseiNenkinJigyoshoBango.Location = new System.Drawing.Point(132, 62);
            this.txtKoseiNenkinJigyoshoBango.MaxLength = 15;
            this.txtKoseiNenkinJigyoshoBango.Name = "txtKoseiNenkinJigyoshoBango";
            this.txtKoseiNenkinJigyoshoBango.Size = new System.Drawing.Size(173, 23);
            this.txtKoseiNenkinJigyoshoBango.TabIndex = 3;
            this.txtKoseiNenkinJigyoshoBango.Validating += new System.ComponentModel.CancelEventHandler(this.txtKoseiNenkinJigyoshoBango_Validating);
            // 
            // lblKoseiNenkinJigyoshoBango
            // 
            this.lblKoseiNenkinJigyoshoBango.BackColor = System.Drawing.Color.Silver;
            this.lblKoseiNenkinJigyoshoBango.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKoseiNenkinJigyoshoBango.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKoseiNenkinJigyoshoBango.Location = new System.Drawing.Point(16, 62);
            this.lblKoseiNenkinJigyoshoBango.Name = "lblKoseiNenkinJigyoshoBango";
            this.lblKoseiNenkinJigyoshoBango.Size = new System.Drawing.Size(116, 23);
            this.lblKoseiNenkinJigyoshoBango.TabIndex = 939;
            this.lblKoseiNenkinJigyoshoBango.Text = "事 業 所 番 号";
            this.lblKoseiNenkinJigyoshoBango.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKoseiNenkinJigyoshoSeiriKg
            // 
            this.txtKoseiNenkinJigyoshoSeiriKg.AutoSizeFromLength = false;
            this.txtKoseiNenkinJigyoshoSeiriKg.DisplayLength = null;
            this.txtKoseiNenkinJigyoshoSeiriKg.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKoseiNenkinJigyoshoSeiriKg.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtKoseiNenkinJigyoshoSeiriKg.Location = new System.Drawing.Point(132, 39);
            this.txtKoseiNenkinJigyoshoSeiriKg.MaxLength = 20;
            this.txtKoseiNenkinJigyoshoSeiriKg.Name = "txtKoseiNenkinJigyoshoSeiriKg";
            this.txtKoseiNenkinJigyoshoSeiriKg.Size = new System.Drawing.Size(173, 23);
            this.txtKoseiNenkinJigyoshoSeiriKg.TabIndex = 2;
            this.txtKoseiNenkinJigyoshoSeiriKg.Validating += new System.ComponentModel.CancelEventHandler(this.txtKoseiNenkinJigyoshoSeiriKg_Validating);
            // 
            // lblKoseiNenkinJigyoshoSeiriKg
            // 
            this.lblKoseiNenkinJigyoshoSeiriKg.BackColor = System.Drawing.Color.Silver;
            this.lblKoseiNenkinJigyoshoSeiriKg.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKoseiNenkinJigyoshoSeiriKg.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKoseiNenkinJigyoshoSeiriKg.Location = new System.Drawing.Point(16, 39);
            this.lblKoseiNenkinJigyoshoSeiriKg.Name = "lblKoseiNenkinJigyoshoSeiriKg";
            this.lblKoseiNenkinJigyoshoSeiriKg.Size = new System.Drawing.Size(116, 23);
            this.lblKoseiNenkinJigyoshoSeiriKg.TabIndex = 937;
            this.lblKoseiNenkinJigyoshoSeiriKg.Text = "事業所整理記号";
            this.lblKoseiNenkinJigyoshoSeiriKg.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKoseiNenkinHihokenshaFtnKs
            // 
            this.txtKoseiNenkinHihokenshaFtnKs.AutoSizeFromLength = false;
            this.txtKoseiNenkinHihokenshaFtnKs.DisplayLength = null;
            this.txtKoseiNenkinHihokenshaFtnKs.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKoseiNenkinHihokenshaFtnKs.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKoseiNenkinHihokenshaFtnKs.Location = new System.Drawing.Point(220, 16);
            this.txtKoseiNenkinHihokenshaFtnKs.MaxLength = 4;
            this.txtKoseiNenkinHihokenshaFtnKs.Name = "txtKoseiNenkinHihokenshaFtnKs";
            this.txtKoseiNenkinHihokenshaFtnKs.Size = new System.Drawing.Size(63, 23);
            this.txtKoseiNenkinHihokenshaFtnKs.TabIndex = 1;
            this.txtKoseiNenkinHihokenshaFtnKs.Text = "0";
            this.txtKoseiNenkinHihokenshaFtnKs.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKoseiNenkinHihokenshaFtnKs.Validating += new System.ComponentModel.CancelEventHandler(this.txtKoseiNenkinHihokenshaFtnKs_Validating);
            // 
            // lblKoseiNenkinHihokenshaFtnKs
            // 
            this.lblKoseiNenkinHihokenshaFtnKs.BackColor = System.Drawing.Color.Silver;
            this.lblKoseiNenkinHihokenshaFtnKs.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKoseiNenkinHihokenshaFtnKs.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKoseiNenkinHihokenshaFtnKs.Location = new System.Drawing.Point(196, 16);
            this.lblKoseiNenkinHihokenshaFtnKs.Name = "lblKoseiNenkinHihokenshaFtnKs";
            this.lblKoseiNenkinHihokenshaFtnKs.Size = new System.Drawing.Size(24, 23);
            this.lblKoseiNenkinHihokenshaFtnKs.TabIndex = 934;
            this.lblKoseiNenkinHihokenshaFtnKs.Text = "／";
            this.lblKoseiNenkinHihokenshaFtnKs.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtKoseiNenkinHihokenshaFtnRt
            // 
            this.txtKoseiNenkinHihokenshaFtnRt.AutoSizeFromLength = false;
            this.txtKoseiNenkinHihokenshaFtnRt.DisplayLength = null;
            this.txtKoseiNenkinHihokenshaFtnRt.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKoseiNenkinHihokenshaFtnRt.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKoseiNenkinHihokenshaFtnRt.Location = new System.Drawing.Point(132, 16);
            this.txtKoseiNenkinHihokenshaFtnRt.MaxLength = 6;
            this.txtKoseiNenkinHihokenshaFtnRt.Name = "txtKoseiNenkinHihokenshaFtnRt";
            this.txtKoseiNenkinHihokenshaFtnRt.Size = new System.Drawing.Size(64, 23);
            this.txtKoseiNenkinHihokenshaFtnRt.TabIndex = 0;
            this.txtKoseiNenkinHihokenshaFtnRt.Text = "0";
            this.txtKoseiNenkinHihokenshaFtnRt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKoseiNenkinHihokenshaFtnRt.Validating += new System.ComponentModel.CancelEventHandler(this.txtKoseiNenkinHihokenshaFtnRt_Validating);
            // 
            // lblKoseiNenkinHihokenshaFtnRt
            // 
            this.lblKoseiNenkinHihokenshaFtnRt.BackColor = System.Drawing.Color.Silver;
            this.lblKoseiNenkinHihokenshaFtnRt.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKoseiNenkinHihokenshaFtnRt.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKoseiNenkinHihokenshaFtnRt.Location = new System.Drawing.Point(16, 16);
            this.lblKoseiNenkinHihokenshaFtnRt.Name = "lblKoseiNenkinHihokenshaFtnRt";
            this.lblKoseiNenkinHihokenshaFtnRt.Size = new System.Drawing.Size(116, 23);
            this.lblKoseiNenkinHihokenshaFtnRt.TabIndex = 933;
            this.lblKoseiNenkinHihokenshaFtnRt.Text = "被保険者負担率";
            this.lblKoseiNenkinHihokenshaFtnRt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // grpGinkoJoho
            // 
            this.grpGinkoJoho.Controls.Add(this.txtShumokuCd);
            this.grpGinkoJoho.Controls.Add(this.txtItakuCd);
            this.grpGinkoJoho.Controls.Add(this.lblItakuCd);
            this.grpGinkoJoho.Controls.Add(this.txtShitenCd);
            this.grpGinkoJoho.Controls.Add(this.lblGinkoNm);
            this.grpGinkoJoho.Controls.Add(this.txtGinkoCd);
            this.grpGinkoJoho.Controls.Add(this.lblShitenCd);
            this.grpGinkoJoho.Controls.Add(this.lblGinkoCd);
            this.grpGinkoJoho.Controls.Add(this.lblShumokuCd);
            this.grpGinkoJoho.Controls.Add(this.lblShitenNm);
            this.grpGinkoJoho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.grpGinkoJoho.Location = new System.Drawing.Point(392, 50);
            this.grpGinkoJoho.Name = "grpGinkoJoho";
            this.grpGinkoJoho.Size = new System.Drawing.Size(347, 95);
            this.grpGinkoJoho.TabIndex = 3;
            this.grpGinkoJoho.TabStop = false;
            this.grpGinkoJoho.Text = "銀行情報";
            // 
            // txtShumokuCd
            // 
            this.txtShumokuCd.AutoSizeFromLength = false;
            this.txtShumokuCd.DisplayLength = null;
            this.txtShumokuCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShumokuCd.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtShumokuCd.Location = new System.Drawing.Point(246, 64);
            this.txtShumokuCd.MaxLength = 10;
            this.txtShumokuCd.Name = "txtShumokuCd";
            this.txtShumokuCd.Size = new System.Drawing.Size(86, 23);
            this.txtShumokuCd.TabIndex = 3;
            this.txtShumokuCd.Text = "0";
            this.txtShumokuCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtShumokuCd_Validating);
            // 
            // txtItakuCd
            // 
            this.txtItakuCd.AutoSizeFromLength = false;
            this.txtItakuCd.DisplayLength = null;
            this.txtItakuCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtItakuCd.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtItakuCd.Location = new System.Drawing.Point(134, 64);
            this.txtItakuCd.MaxLength = 10;
            this.txtItakuCd.Name = "txtItakuCd";
            this.txtItakuCd.Size = new System.Drawing.Size(88, 23);
            this.txtItakuCd.TabIndex = 2;
            this.txtItakuCd.Text = "0";
            this.txtItakuCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtItakuCd_Validating);
            // 
            // lblItakuCd
            // 
            this.lblItakuCd.BackColor = System.Drawing.Color.Silver;
            this.lblItakuCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblItakuCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblItakuCd.Location = new System.Drawing.Point(14, 64);
            this.lblItakuCd.Name = "lblItakuCd";
            this.lblItakuCd.Size = new System.Drawing.Size(120, 23);
            this.lblItakuCd.TabIndex = 949;
            this.lblItakuCd.Text = "委 託 コ ー ド";
            this.lblItakuCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShitenCd
            // 
            this.txtShitenCd.AutoSizeFromLength = true;
            this.txtShitenCd.DisplayLength = null;
            this.txtShitenCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShitenCd.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtShitenCd.Location = new System.Drawing.Point(134, 41);
            this.txtShitenCd.MaxLength = 4;
            this.txtShitenCd.Name = "txtShitenCd";
            this.txtShitenCd.Size = new System.Drawing.Size(50, 23);
            this.txtShitenCd.TabIndex = 1;
            this.txtShitenCd.Text = "0";
            this.txtShitenCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShitenCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtShitenCd_Validating);
            // 
            // lblGinkoNm
            // 
            this.lblGinkoNm.BackColor = System.Drawing.Color.Silver;
            this.lblGinkoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGinkoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGinkoNm.Location = new System.Drawing.Point(184, 18);
            this.lblGinkoNm.Name = "lblGinkoNm";
            this.lblGinkoNm.Size = new System.Drawing.Size(148, 23);
            this.lblGinkoNm.TabIndex = 946;
            this.lblGinkoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtGinkoCd
            // 
            this.txtGinkoCd.AutoSizeFromLength = true;
            this.txtGinkoCd.DisplayLength = null;
            this.txtGinkoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGinkoCd.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtGinkoCd.Location = new System.Drawing.Point(134, 18);
            this.txtGinkoCd.MaxLength = 4;
            this.txtGinkoCd.Name = "txtGinkoCd";
            this.txtGinkoCd.Size = new System.Drawing.Size(50, 23);
            this.txtGinkoCd.TabIndex = 0;
            this.txtGinkoCd.Text = "0";
            this.txtGinkoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGinkoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtGinkoCd_Validating);
            // 
            // lblShitenCd
            // 
            this.lblShitenCd.BackColor = System.Drawing.Color.Silver;
            this.lblShitenCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShitenCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShitenCd.Location = new System.Drawing.Point(14, 41);
            this.lblShitenCd.Name = "lblShitenCd";
            this.lblShitenCd.Size = new System.Drawing.Size(120, 23);
            this.lblShitenCd.TabIndex = 945;
            this.lblShitenCd.Text = "支 店 コ ー ド";
            this.lblShitenCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblGinkoCd
            // 
            this.lblGinkoCd.BackColor = System.Drawing.Color.Silver;
            this.lblGinkoCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGinkoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGinkoCd.Location = new System.Drawing.Point(14, 18);
            this.lblGinkoCd.Name = "lblGinkoCd";
            this.lblGinkoCd.Size = new System.Drawing.Size(120, 23);
            this.lblGinkoCd.TabIndex = 944;
            this.lblGinkoCd.Text = "銀 行 コ ー ド";
            this.lblGinkoCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShumokuCd
            // 
            this.lblShumokuCd.BackColor = System.Drawing.Color.Silver;
            this.lblShumokuCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShumokuCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShumokuCd.Location = new System.Drawing.Point(222, 64);
            this.lblShumokuCd.Name = "lblShumokuCd";
            this.lblShumokuCd.Size = new System.Drawing.Size(24, 23);
            this.lblShumokuCd.TabIndex = 950;
            this.lblShumokuCd.Text = "／";
            this.lblShumokuCd.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblShitenNm
            // 
            this.lblShitenNm.BackColor = System.Drawing.Color.Silver;
            this.lblShitenNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShitenNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShitenNm.Location = new System.Drawing.Point(184, 41);
            this.lblShitenNm.Name = "lblShitenNm";
            this.lblShitenNm.Size = new System.Drawing.Size(148, 23);
            this.lblShitenNm.TabIndex = 947;
            this.lblShitenNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // grpShotokuzei
            // 
            this.grpShotokuzei.Controls.Add(this.lblShotokuzeiKeisanHohoNm);
            this.grpShotokuzei.Controls.Add(this.txtShotokuzeiKeisanHoho);
            this.grpShotokuzei.Controls.Add(this.lblShotokuzeiKeisanHoho);
            this.grpShotokuzei.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.grpShotokuzei.Location = new System.Drawing.Point(392, 161);
            this.grpShotokuzei.Name = "grpShotokuzei";
            this.grpShotokuzei.Size = new System.Drawing.Size(347, 47);
            this.grpShotokuzei.TabIndex = 4;
            this.grpShotokuzei.TabStop = false;
            this.grpShotokuzei.Text = "所得税";
            // 
            // lblShotokuzeiKeisanHohoNm
            // 
            this.lblShotokuzeiKeisanHohoNm.BackColor = System.Drawing.Color.Silver;
            this.lblShotokuzeiKeisanHohoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShotokuzeiKeisanHohoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShotokuzeiKeisanHohoNm.Location = new System.Drawing.Point(150, 16);
            this.lblShotokuzeiKeisanHohoNm.Name = "lblShotokuzeiKeisanHohoNm";
            this.lblShotokuzeiKeisanHohoNm.Size = new System.Drawing.Size(183, 23);
            this.lblShotokuzeiKeisanHohoNm.TabIndex = 947;
            this.lblShotokuzeiKeisanHohoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShotokuzeiKeisanHoho
            // 
            this.txtShotokuzeiKeisanHoho.AutoSizeFromLength = true;
            this.txtShotokuzeiKeisanHoho.DisplayLength = null;
            this.txtShotokuzeiKeisanHoho.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShotokuzeiKeisanHoho.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtShotokuzeiKeisanHoho.Location = new System.Drawing.Point(132, 16);
            this.txtShotokuzeiKeisanHoho.MaxLength = 1;
            this.txtShotokuzeiKeisanHoho.Name = "txtShotokuzeiKeisanHoho";
            this.txtShotokuzeiKeisanHoho.Size = new System.Drawing.Size(18, 23);
            this.txtShotokuzeiKeisanHoho.TabIndex = 0;
            this.txtShotokuzeiKeisanHoho.Text = "0";
            this.txtShotokuzeiKeisanHoho.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtShotokuzeiKeisanHoho.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LastControl_KeyDown);
            this.txtShotokuzeiKeisanHoho.Validating += new System.ComponentModel.CancelEventHandler(this.txtShotokuzeiKeisanHoho_Validating);
            // 
            // lblShotokuzeiKeisanHoho
            // 
            this.lblShotokuzeiKeisanHoho.BackColor = System.Drawing.Color.Silver;
            this.lblShotokuzeiKeisanHoho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShotokuzeiKeisanHoho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShotokuzeiKeisanHoho.Location = new System.Drawing.Point(14, 16);
            this.lblShotokuzeiKeisanHoho.Name = "lblShotokuzeiKeisanHoho";
            this.lblShotokuzeiKeisanHoho.Size = new System.Drawing.Size(120, 23);
            this.lblShotokuzeiKeisanHoho.TabIndex = 946;
            this.lblShotokuzeiKeisanHoho.Text = "計　算　方　法";
            this.lblShotokuzeiKeisanHoho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // KYCM1011
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.grpShotokuzei);
            this.Controls.Add(this.grpGinkoJoho);
            this.Controls.Add(this.grpKoseinenkin);
            this.Controls.Add(this.grpKenkohoken);
            this.Controls.Add(this.grpKoyohoken);
            this.Name = "KYCM1011";
            this.Text = "会社情報の登録";
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.grpKoyohoken, 0);
            this.Controls.SetChildIndex(this.grpKenkohoken, 0);
            this.Controls.SetChildIndex(this.grpKoseinenkin, 0);
            this.Controls.SetChildIndex(this.grpGinkoJoho, 0);
            this.Controls.SetChildIndex(this.grpShotokuzei, 0);
            this.pnlDebug.ResumeLayout(false);
            this.grpKoyohoken.ResumeLayout(false);
            this.grpKoyohoken.PerformLayout();
            this.grpKenkohoken.ResumeLayout(false);
            this.grpKenkohoken.PerformLayout();
            this.grpKoseinenkin.ResumeLayout(false);
            this.grpKoseinenkin.PerformLayout();
            this.grpGinkoJoho.ResumeLayout(false);
            this.grpGinkoJoho.PerformLayout();
            this.grpShotokuzei.ResumeLayout(false);
            this.grpShotokuzei.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpKoyohoken;
        private common.controls.FsiTextBox txtKoyoHokenHihokenshaFtnKs;
        private System.Windows.Forms.Label lblKoyoHokenHihokenshaFtnKs;
        private common.controls.FsiTextBox txtKoyoHokenHihokenshaFtnRt;
        private System.Windows.Forms.Label lblKoyoHokenHihokenshaFtnRt;
        private System.Windows.Forms.Label lblKoyoHokenGyoshuNm;
        private common.controls.FsiTextBox txtKoyoHokenGyoshu;
        private System.Windows.Forms.Label lblKoyoHokenGyoshu;
        private System.Windows.Forms.Label lblKoyoHokenKeisanHohoNm;
        private common.controls.FsiTextBox txtKoyoHokenKeisanHoho;
        private System.Windows.Forms.Label lblKoyoHokenKeisanHoho;
        private System.Windows.Forms.GroupBox grpKenkohoken;
        private common.controls.FsiTextBox txtKenkoHokenJigyoshoBango;
        private System.Windows.Forms.Label lblKenkoHokenJigyoshoBango;
        private common.controls.FsiTextBox txtKenkoHokenJigyoshoSeiriKg;
        private System.Windows.Forms.Label lblKenkoHokenJigyoshoSeiriKg;
        private common.controls.FsiTextBox txtKnkhknHihokenshaFtnKsKig;
        private System.Windows.Forms.Label lblKnkhknHihokenshaFtnKsKig;
        private common.controls.FsiTextBox txtKnkhknHihokenshaFtnRtKig;
        private System.Windows.Forms.Label lblKnkhknHihokenshaFtnRtKig;
        private common.controls.FsiTextBox txtKenkoHokenHihokenshaFtnKs;
        private System.Windows.Forms.Label lblKenkoHokenHihokenshaFtnKs;
        private common.controls.FsiTextBox txtKenkoHokenHihokenshaFtnRt;
        private System.Windows.Forms.Label lblKenkoHokenHihokenshaFtnRt;
        private System.Windows.Forms.GroupBox grpKoseinenkin;
        private common.controls.FsiTextBox txtKoseiNenkinJigyoshoBango;
        private System.Windows.Forms.Label lblKoseiNenkinJigyoshoBango;
        private common.controls.FsiTextBox txtKoseiNenkinJigyoshoSeiriKg;
        private System.Windows.Forms.Label lblKoseiNenkinJigyoshoSeiriKg;
        private common.controls.FsiTextBox txtKoseiNenkinHihokenshaFtnKs;
        private System.Windows.Forms.Label lblKoseiNenkinHihokenshaFtnKs;
        private common.controls.FsiTextBox txtKoseiNenkinHihokenshaFtnRt;
        private System.Windows.Forms.Label lblKoseiNenkinHihokenshaFtnRt;
        private System.Windows.Forms.GroupBox grpGinkoJoho;
        private common.controls.FsiTextBox txtShumokuCd;
        private common.controls.FsiTextBox txtItakuCd;
        private System.Windows.Forms.Label lblItakuCd;
        private common.controls.FsiTextBox txtShitenCd;
        private System.Windows.Forms.Label lblGinkoNm;
        private common.controls.FsiTextBox txtGinkoCd;
        private System.Windows.Forms.Label lblShitenCd;
        private System.Windows.Forms.Label lblGinkoCd;
        private System.Windows.Forms.Label lblShumokuCd;
        private System.Windows.Forms.Label lblShitenNm;
        private System.Windows.Forms.GroupBox grpShotokuzei;
        private System.Windows.Forms.Label lblShotokuzeiKeisanHohoNm;
        private common.controls.FsiTextBox txtShotokuzeiKeisanHoho;
        private System.Windows.Forms.Label lblShotokuzeiKeisanHoho;

    }
}