﻿using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;
using jp.co.fsi.common.controls;
using System.Reflection;
using System;

namespace jp.co.fsi.ky.kycm1011
{
    /// <summary>
    /// 会社情報の登録(KYCM1011)
    /// </summary>
    public partial class KYCM1011 : BasePgForm
    {

        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理用変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KYCM1011()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)

        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            if (!DataLoad())
            {
                // 不正な起動として閉じる
                Msg.Error("会社情報が登録されていません。" + "\r\n" + "＜会社コード＝1＞");
                this.Close();
            }

            // 初期フォーカス
            this.txtKoyoHokenKeisanHoho.Focus();
            this.txtKoyoHokenKeisanHoho.SelectAll();

            // Enter処理を無効化
            this._dtFlg = false;

        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // コード項目に
            // フォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtKoyoHokenKeisanHoho":
                case "txtKoyoHokenGyoshu":
                case "txtGinkoCd":
                case "txtShitenCd":
                case "txtShotokuzeiKeisanHoho":
                    this.btnF1.Enabled = true;
                    break;
                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        public override void PressF1()
        {
            Assembly asm;
            Type t;

            switch (this.ActiveCtlNm)
            {
                case "txtKoyoHokenKeisanHoho":
                    #region 雇用保険計算方法検索
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("CMCM1041.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "TB_KY_F_KOYO_HOKEN_KEISAN_HOHO";       // 起動パラメータ
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.txtKoyoHokenKeisanHoho.Text = result[0];
                                this.lblKoyoHokenKeisanHohoNm.Text = result[1];
                            }
                        }
                    }
                    #endregion
                    break;
                case "txtKoyoHokenGyoshu":
                    #region 雇用保険業種検索
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("CMCM1041.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "TB_KY_F_KOYO_HOKEN_GYOSHU";       // 起動パラメータ
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.txtKoyoHokenGyoshu.Text = result[0];
                                this.lblKoyoHokenGyoshuNm.Text = result[1];
                            }
                        }
                    }
                    #endregion
                    break;
                case "txtGinkoCd":
                    #region 銀行検索
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("KYCM1051.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.ky.kycm1051.KYCM1051");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";                     // 検索ダイアログ起動パラメータ
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.txtGinkoCd.Text = result[0];
                                this.lblGinkoNm.Text = result[1];
                                // 従属フィールドクリア
                                this.txtShitenCd.Text = "0";
                                this.lblShitenNm.Text = "";
                            }
                        }
                    }
                    #endregion
                    break;
                case "txtShitenCd":
                    #region 支店検索
                    // 銀行コード入力済の場合に実行
                    if (!ValChk.IsEmpty(this.txtGinkoCd.Text))
                    {
                        // アセンブリのロード
                        asm = System.Reflection.Assembly.LoadFrom("KYCM1061.exe");
                        // フォーム作成
                        t = asm.GetType("jp.co.fsi.ky.kycm1061.KYCM1061");
                        if (t != null)
                        {
                            Object obj = System.Activator.CreateInstance(t);
                            if (obj != null)
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.Par1 = "1";                     // 検索ダイアログ起動パラメータ
                                frm.InData = this.txtGinkoCd.Text;  // (string)銀行コード
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.txtShitenCd.Text = result[0];
                                    this.lblShitenNm.Text = result[1];
                                }
                            }
                        }
                    }
                    #endregion
                    break;
                case "txtShotokuzeiKeisanHoho":
                    #region 所得税計算方法検索
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("CMCM1041.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "TB_KY_F_SHOTOKUZEI_KEISAN_HOHO";       // 起動パラメータ
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.txtShotokuzeiKeisanHoho.Text = result[0];
                                this.lblShotokuzeiKeisanHohoNm.Text = result[1];
                            }
                        }
                    }
                    #endregion
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        public override void PressF6()
        {
            // Enter処理を無効化
            this._dtFlg = false;

            // 確認メッセージを表示
            string msg = ("更新しますか？");
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // 入力値をバインドパラメータとしてセットする
            ArrayList alParamsKyKaishaJoho = SetKyKaishaJohoParams();

            try
            {
                // トランザクション開始
                this.Dba.BeginTransaction();

                // データ更新
                // CM会社情報
                this.Dba.Update("TB_KY_KAISHA_JOHO",
                    (DbParamCollection)alParamsKyKaishaJoho[1],
                    "KAISHA_CD = @KAISHA_CD AND NENDO = @NENDO",
                    (DbParamCollection)alParamsKyKaishaJoho[0]);

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            //this.Close();
            base.PressEsc();
        }
        
        #endregion

        #region イベント(入力検査)

        /// <summary>
        /// 雇用保険計算方法の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private void txtKoyoHokenKeisanHoho_Validating(object sender, CancelEventArgs e)
        {
            // 入力値補正
            this.txtKoyoHokenKeisanHoho.Text = Util.ToString(Util.ToDecimal(this.txtKoyoHokenKeisanHoho.Text));

            // 数字のみの入力を許可・未入力はNG
            if (!IsValidKubun("7", this.txtKoyoHokenKeisanHoho))
            {
                e.Cancel = true;
                this.txtKoyoHokenKeisanHoho.SelectAll();
                // Enter処理を無効化
                this._dtFlg = false;
            }

            // 従属コントロール
            this.lblKoyoHokenKeisanHohoNm.Text = this.Dba.GetKyuyoKbnNm("7", this.txtKoyoHokenKeisanHoho.Text);
            // Enter処理を有効化
            this._dtFlg = true;
        }

        /// <summary>
        /// 雇用保険業種の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private void txtKoyoHokenGyoshu_Validating(object sender, CancelEventArgs e)
        {
            // 入力値補正
            this.txtKoyoHokenGyoshu.Text = Util.ToString(Util.ToDecimal(this.txtKoyoHokenGyoshu.Text));

            // 数字のみの入力を許可・未入力はNG
            if (!IsValidKubun("6", this.txtKoyoHokenGyoshu))
            {
                e.Cancel = true;
                this.txtKoyoHokenGyoshu.SelectAll();
                // Enter処理を無効化
                this._dtFlg = false;
            }

            // 従属コントロール
            this.lblKoyoHokenGyoshuNm.Text = this.Dba.GetKyuyoKbnNm("6", this.txtKoyoHokenGyoshu.Text);
            // Enter処理を有効化
            this._dtFlg = true;
        }

        /// <summary>
        /// 雇用保険被保険者負担率の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private void txtKoyoHokenHihokenshaFtnRt_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidDec(this.txtKoyoHokenHihokenshaFtnRt))
            {
                e.Cancel = true;
                this.txtKoyoHokenHihokenshaFtnRt.SelectAll();
                // Enter処理を無効化
                this._dtFlg = false;
            }
            // Enter処理を有効化
            this._dtFlg = true;
        }

        /// <summary>
        /// 雇用保険被保険者負担率基数の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private void txtKoyoHokenHihokenshaFtnKs_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidInt(this.txtKoyoHokenHihokenshaFtnKs))
            {
                e.Cancel = true;
                this.txtKoyoHokenHihokenshaFtnKs.SelectAll();
                // Enter処理を無効化
                this._dtFlg = false;
            }
            // Enter処理を有効化
            this._dtFlg = true;
        }

        /// <summary>
        /// 健康保険被保険者負担率の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private void txtKenkoHokenHihokenshaFtnRt_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidDec(this.txtKenkoHokenHihokenshaFtnRt))
            {
                e.Cancel = true;
                this.txtKenkoHokenHihokenshaFtnRt.SelectAll();
                // Enter処理を無効化
                this._dtFlg = false;
            }
            // Enter処理を有効化
            this._dtFlg = true;
        }

        /// <summary>
        /// 健康保険被保険者負担率基数の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private void txtKenkoHokenHihokenshaFtnKs_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidInt(this.txtKenkoHokenHihokenshaFtnKs))
            {
                e.Cancel = true;
                this.txtKenkoHokenHihokenshaFtnKs.SelectAll();
                // Enter処理を無効化
                this._dtFlg = false;
            }
            // Enter処理を有効化
            this._dtFlg = true;
        }

        /// <summary>
        /// 健康保険被保険者負担率・介護の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private void txtKnkhknHihokenshaFtnRtKig_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidDec(this.txtKnkhknHihokenshaFtnRtKig))
            {
                e.Cancel = true;
                this.txtKnkhknHihokenshaFtnRtKig.SelectAll();
                // Enter処理を無効化
                this._dtFlg = false;
            }
            // Enter処理を有効化
            this._dtFlg = true;
        }

        /// <summary>
        /// 健康保険被保険者負担率基数・介護の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private void txtKnkhknHihokenshaFtnKsKig_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidInt(this.txtKnkhknHihokenshaFtnKsKig))
            {
                e.Cancel = true;
                this.txtKnkhknHihokenshaFtnKsKig.SelectAll();
                // Enter処理を無効化
                this._dtFlg = false;
            }
            // Enter処理を有効化
            this._dtFlg = true;
        }

        /// <summary>
        /// 健康保険事業者整理記号の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private void txtKenkoHokenJigyoshoSeiriKg_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidText(this.txtKenkoHokenJigyoshoSeiriKg))
            {
                e.Cancel = true;
                this.txtKenkoHokenJigyoshoSeiriKg.SelectAll();
                // Enter処理を無効化
                this._dtFlg = false;
            }
            // Enter処理を有効化
            this._dtFlg = true;
        }

        /// <summary>
        /// 健康保険事業者番号の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private void txtKenkoHokenJigyoshoBango_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidText(this.txtKenkoHokenJigyoshoBango))
            {
                e.Cancel = true;
                this.txtKenkoHokenJigyoshoBango.SelectAll();
                // Enter処理を無効化
                this._dtFlg = false;
            }
            // Enter処理を有効化
            this._dtFlg = true;
        }

        /// <summary>
        /// 厚生年金被保険者負担率の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private void txtKoseiNenkinHihokenshaFtnRt_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidDec(this.txtKoseiNenkinHihokenshaFtnRt))
            {
                e.Cancel = true;
                this.txtKoseiNenkinHihokenshaFtnRt.SelectAll();
                // Enter処理を無効化
                this._dtFlg = false;
            }
            // Enter処理を有効化
            this._dtFlg = true;
        }

        /// <summary>
        /// 厚生年金被保険者負担率基数の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private void txtKoseiNenkinHihokenshaFtnKs_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidInt(this.txtKoseiNenkinHihokenshaFtnKs))
            {
                e.Cancel = true;
                this.txtKoseiNenkinHihokenshaFtnKs.SelectAll();
                // Enter処理を無効化
                this._dtFlg = false;
            }
            // Enter処理を有効化
            this._dtFlg = true;
        }

        /// <summary>
        /// 厚生年金事業者整理記号の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private void txtKoseiNenkinJigyoshoSeiriKg_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidText(this.txtKoseiNenkinJigyoshoSeiriKg))
            {
                e.Cancel = true;
                this.txtKoseiNenkinJigyoshoSeiriKg.SelectAll();
                // Enter処理を無効化
                this._dtFlg = false;
            }
            // Enter処理を有効化
            this._dtFlg = true;
        }

        /// <summary>
        /// 厚生年金事業者番号の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private void txtKoseiNenkinJigyoshoBango_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidText(this.txtKoseiNenkinJigyoshoBango))
            {
                e.Cancel = true;
                this.txtKoseiNenkinJigyoshoBango.SelectAll();
                // Enter処理を無効化
                this._dtFlg = false;
            }
            // Enter処理を有効化
            this._dtFlg = true;
        }

        /// <summary>
        /// 銀行コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private void txtGinkoCd_Validating(object sender, CancelEventArgs e)
        {
            // 入力値補正
            this.txtGinkoCd.Text = Util.ToString(Util.ToDecimal(this.txtGinkoCd.Text));

            // 数字のみの入力を許可・未入力はNG
            if (!IsValidGinkoCd(this.txtGinkoCd))
            {
                e.Cancel = true;
                this.txtGinkoCd.SelectAll();
                // Enter処理を無効化
                this._dtFlg = false;
            }

            // 従属コントロール
            if (this.txtGinkoCd.Modified || this.txtGinkoCd.Text == "0")
            {
                this.lblGinkoNm.Text = this.Dba.GetName(this.UInfo, "TB_KY_GINKO", " ", Util.ToString(this.txtGinkoCd.Text));
                this.txtShitenCd.Text = "0";
                this.lblShitenNm.Text = "";

                this.txtGinkoCd.Modified = false;
            }
            // Enter処理を有効化
            this._dtFlg = true;
        }

        /// <summary>
        /// 支店コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private void txtShitenCd_Validating(object sender, CancelEventArgs e)
        {
            // 入力値補正
            this.txtShitenCd.Text = Util.ToString(Util.ToDecimal(this.txtShitenCd.Text));

            // 数字のみの入力を許可・未入力はNG
            if (!IsValidShitenCd(this.txtGinkoCd, this.txtShitenCd))
            {
                e.Cancel = true;
                this.txtShitenCd.SelectAll();
                // Enter処理を無効化
                this._dtFlg = false;
            }

            // 従属コントロール
            if (this.txtShitenCd.Modified || this.txtShitenCd.Text == "0")
            {
                this.lblShitenNm.Text = this.Dba.GetGinkoShitenNm(
                    Util.ToString(this.txtGinkoCd.Text), Util.ToString(this.txtShitenCd.Text));

                this.txtShitenCd.Modified = false;
            }
            // Enter処理を有効化
            this._dtFlg = true;
        }

        /// <summary>
        /// 委託コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private void txtItakuCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidText(this.txtItakuCd))
            {
                e.Cancel = true;
                this.txtItakuCd.SelectAll();
                // Enter処理を無効化
                this._dtFlg = false;
            }
            // Enter処理を有効化
            this._dtFlg = true;
        }

        /// <summary>
        /// 種目コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private void txtShumokuCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidText(this.txtShumokuCd))
            {
                e.Cancel = true;
                this.txtShumokuCd.SelectAll();
                // Enter処理を無効化
                this._dtFlg = false;
            }
            // Enter処理を有効化
            this._dtFlg = true;
        }

        /// <summary>
        /// 所得税計算方法の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private void txtShotokuzeiKeisanHoho_Validating(object sender, CancelEventArgs e)
        {
            // 入力値補正
            this.txtShotokuzeiKeisanHoho.Text = Util.ToString(Util.ToDecimal(this.txtShotokuzeiKeisanHoho.Text));

            // 数字のみの入力を許可・未入力はNG
            if (!IsValidKubun("8", this.txtShotokuzeiKeisanHoho))
            {
                e.Cancel = true;
                this.txtShotokuzeiKeisanHoho.SelectAll();
                // Enter処理を無効化
                this._dtFlg = false;
            }

            // 名称セット
            this.lblShotokuzeiKeisanHohoNm.Text = this.Dba.GetKyuyoKbnNm("8", this.txtShotokuzeiKeisanHoho.Text);
            // Enter処理を有効化
            this._dtFlg = true;
        }


        /// <summary>
        /// 画面の最後のコントールでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LastControl_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.btnF6.Enabled && this.Flg)
            {
                this.PressF6();
            }
        }
        #endregion

        #region privateメソッド

        /// <summary>
        /// 編集データ読込
        /// </summary>
        private bool DataLoad()
        {
            bool ret = false;

            // VI社員情報データ参照
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, 1);
            dpc.SetParam("@NENDO", SqlDbType.Decimal, 4, 0);
            DataTable dt = this.Dba.GetDataTableFromSqlWithParams(GetKaishaJohoSql(), dpc);
            if (dt.Rows.Count == 1)
            {
                ret = true;

                DataRow dr = dt.Rows[0];
                this.txtKoyoHokenKeisanHoho.Text = Util.ToString(dr["KOYO_HOKEN_KEISAN_HOHO"]);
                this.lblKoyoHokenKeisanHohoNm.Text = Util.ToString(dr["KOYO_HOKEN_KEISAN_HOHO_NM"]);
                this.txtKoyoHokenGyoshu.Text = Util.ToString(dr["KOYO_HOKEN_GYOSHU"]);
                this.lblKoyoHokenGyoshuNm.Text = Util.ToString(dr["KOYO_HOKEN_GYOSHU_NM"]);
                this.txtKoyoHokenHihokenshaFtnRt.Text = Util.FormatNum(dr["KOYO_HOKEN_HIHOKENSHA_FTN_RT"], 2);
                this.txtKoyoHokenHihokenshaFtnKs.Text = Util.ToString(dr["KOYO_HOKEN_HIHOKENSHA_FTN_KS"]);
                this.txtKenkoHokenHihokenshaFtnRt.Text = Util.FormatNum(dr["KENKO_HOKEN_HIHOKENSHA_FTN_RT"], 2);
                this.txtKenkoHokenHihokenshaFtnKs.Text = Util.ToString(dr["KENKO_HOKEN_HIHOKENSHA_FTN_KS"]);
                this.txtKnkhknHihokenshaFtnRtKig.Text = Util.FormatNum(dr["KNKHKN_HIHOKENSHA_FTN_RT_KIG"], 2);
                this.txtKnkhknHihokenshaFtnKsKig.Text = Util.ToString(dr["KNKHKN_HIHOKENSHA_FTN_KS_KIG"]);
                this.txtKenkoHokenJigyoshoSeiriKg.Text = Util.ToString(dr["KENKO_HOKEN_JIGYOSHO_SEIRI_KG"]);
                this.txtKenkoHokenJigyoshoBango.Text = Util.ToString(dr["KENKO_HOKEN_JIGYOSHO_BANGO"]);
                this.txtKoseiNenkinHihokenshaFtnRt.Text = Util.FormatNum(dr["KOSEI_NENKIN_HIHOKENSHA_FTN_RT"], 2);
                this.txtKoseiNenkinHihokenshaFtnKs.Text = Util.ToString(dr["KOSEI_NENKIN_HIHOKENSHA_FTN_KS"]);
                this.txtKoseiNenkinJigyoshoSeiriKg.Text = Util.ToString(dr["KOSEI_NENKIN_JIGYOSHO_SEIRI_KG"]);
                this.txtKoseiNenkinJigyoshoBango.Text = Util.ToString(dr["KOSEI_NENKIN_JIGYOSHO_BANGO"]);
                this.txtGinkoCd.Text = Util.ToString(dr["GINKO_CD"]);
                this.lblGinkoNm.Text = Util.ToString(dr["GINKO_NM"]);
                this.txtShitenCd.Text = Util.ToString(dr["SHITEN_CD"]);
                this.lblShitenNm.Text = Util.ToString(dr["SHITEN_NM"]);
                this.txtItakuCd.Text = Util.ToString(dr["ITAKU_CD"]);
                this.txtShumokuCd.Text = Util.ToString(dr["SHUMOKU_CD"]);
                this.txtShotokuzeiKeisanHoho.Text = Util.ToString(dr["SHOTOKUZEI_KEISAN_HOHO"]);
                this.lblShotokuzeiKeisanHohoNm.Text = Util.ToString(dr["SHOTOKUZEI_KEISAN_HOHO_NM"]);
            }

            return ret;
        }

        /// <summary>
        /// 給与区分名称DataTableの取得
        /// </summary>
        /// <param name="shubetsuCd">種別コード</param>
        /// <returns>区分(コード値)</returns>
        private DataTable GetDtKubunNm(int shubetsuCd)
        {
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@SHUBETSU_CD", SqlDbType.Int, shubetsuCd);
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT");
            sql.Append(" KUBUN");
            sql.Append(",NM");
            sql.Append(" FROM");
            sql.Append(" VI_KY_KUBUN_NM");
            sql.Append(" WHERE");
            sql.Append(" SHUBETSU_CD = @SHUBETSU_CD");
            sql.Append(" ORDER BY");
            sql.Append(" KUBUN ASC");
            return this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
        }

        /// <summary>
        /// VI_KY_KAISHA_JOHO 取得SQL
        /// </summary>
        /// <returns></returns>
        private string GetKaishaJohoSql()
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT");
            sql.Append(" KAISHA_CD");
            sql.Append(",KAISHA_NM");
            sql.Append(",NENDO");
            sql.Append(",SHUGYO_NISSU");
            sql.Append(",SHUGYO_JIKAN");
            sql.Append(",FUTSU_ZANGYO_WARIMASHI_RITSU");
            sql.Append(",SHINYA_ZANGYO_WARIMASHI_RITSU");
            sql.Append(",KYUSHUTSU_FUTSU_ZNG_WRMS_RT");
            sql.Append(",KYUSHUTSU_SHINYA_ZNG_WRMS_RT");
            sql.Append(",HOTEI_FUTSU_ZNG_WRMS_RT");
            sql.Append(",HOTEI_SHINYA_ZNG_WRMS_RT");
            sql.Append(",SONOTA_ZANGYO_WARIMASHI_RITSU");
            sql.Append(",KEKKIN_KOJO_WARIMASHI_RITSU");
            sql.Append(",CHISO_KOJO_WARIMASHI_RITSU");
            sql.Append(",ZANGYO_TEATE_HASU_SHORI");
            sql.Append(",KEKKIN_KOJO_HASU_SHORI");
            sql.Append(",CHISO_KOJO_HASU_SHORI");
            sql.Append(",SHOTOKUZEI_KEISAN_HOHO");
            sql.Append(",SHOTOKUZEI_KEISAN_HOHO_NM");
            sql.Append(",KOYO_HOKEN_KEISAN_HOHO");
            sql.Append(",KOYO_HOKEN_KEISAN_HOHO_NM");
            sql.Append(",KOYO_HOKEN_GYOSHU");
            sql.Append(",KOYO_HOKEN_GYOSHU_NM");
            sql.Append(",KOYO_HOKEN_HIHOKENSHA_FTN_RT");
            sql.Append(",KOYO_HOKEN_HIHOKENSHA_FTN_KS");
            sql.Append(",KENKO_HOKEN_JIGYOSHO_SEIRI_KG");
            sql.Append(",KENKO_HOKEN_JIGYOSHO_BANGO");
            sql.Append(",KENKO_HOKEN_HIHOKENSHA_FTN_RT");
            sql.Append(",KENKO_HOKEN_HIHOKENSHA_FTN_KS");
            sql.Append(",KOSEI_NENKIN_JIGYOSHO_SEIRI_KG");
            sql.Append(",KOSEI_NENKIN_JIGYOSHO_BANGO");
            sql.Append(",KOSEI_NENKIN_HIHOKENSHA_FTN_RT");
            sql.Append(",KOSEI_NENKIN_HIHOKENSHA_FTN_KS");
            sql.Append(",GINKO_CD");
            sql.Append(",GINKO_NM");
            sql.Append(",SHITEN_CD");
            sql.Append(",SHITEN_NM");
            sql.Append(",YOKIN_SHUMOKU");
            sql.Append(",KOZA_BANGO");
            sql.Append(",KOZA_MEIGININ_KANJI");
            sql.Append(",KOZA_MEIGININ_KANA");
            sql.Append(",ITAKU_CD");
            sql.Append(",SHUMOKU_CD");
            sql.Append(",KNKHKN_HIHOKENSHA_FTN_RT_KIG");
            sql.Append(",KNKHKN_HIHOKENSHA_FTN_KS_KIG");
            sql.Append(" FROM");
            sql.Append(" VI_KY_KAISHA_JOHO");
            sql.Append(" WHERE");
            sql.Append(" KAISHA_CD = @KAISHA_CD");
            sql.Append(" AND NENDO = @NENDO");
            return sql.ToString();

        }

        /// <summary>
        /// TB_KY_KAISHA_JOHOに更新するためのパラメータ設定をします。
        /// </summary>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// 更新処理：DbParamCollection*2(Where句,Set句)
        /// </returns>
        private ArrayList SetKyKaishaJohoParams()
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            // WHERE句パラメータ
            DbParamCollection whereParam = new DbParamCollection();
            whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
            whereParam.SetParam("@NENDO", SqlDbType.Decimal, 4, 0);
            alParams.Add(whereParam);

            // 基本情報
            updParam.SetParam("@KOYO_HOKEN_KEISAN_HOHO", SqlDbType.Decimal, 1, 
                this.txtKoyoHokenKeisanHoho.Text);
            updParam.SetParam("@KOYO_HOKEN_GYOSHU", SqlDbType.Decimal, 1, 
                this.txtKoyoHokenGyoshu.Text);
            updParam.SetParam("@KOYO_HOKEN_HIHOKENSHA_FTN_RT", SqlDbType.Decimal, 5, 2,
                Util.ToDecimal(this.txtKoyoHokenHihokenshaFtnRt.Text));
            updParam.SetParam("@KOYO_HOKEN_HIHOKENSHA_FTN_KS", SqlDbType.Decimal, 4, 0,
                Util.ToDecimal(this.txtKoyoHokenHihokenshaFtnKs.Text));
            updParam.SetParam("@KENKO_HOKEN_HIHOKENSHA_FTN_RT", SqlDbType.Decimal, 5, 2, 
                Util.ToDecimal(this.txtKenkoHokenHihokenshaFtnRt.Text));
            updParam.SetParam("@KENKO_HOKEN_HIHOKENSHA_FTN_KS", SqlDbType.Decimal, 4, 0,
                Util.ToDecimal(this.txtKenkoHokenHihokenshaFtnKs.Text));
            updParam.SetParam("@KNKHKN_HIHOKENSHA_FTN_RT_KIG", SqlDbType.Decimal, 5, 2,
                Util.ToDecimal(this.txtKnkhknHihokenshaFtnRtKig.Text));
            updParam.SetParam("@KNKHKN_HIHOKENSHA_FTN_KS_KIG", SqlDbType.Decimal, 4, 0,
                Util.ToDecimal(this.txtKnkhknHihokenshaFtnKsKig.Text));
            updParam.SetParam("@KENKO_HOKEN_JIGYOSHO_SEIRI_KG", SqlDbType.VarChar, 20, 
                Util.ToString(this.txtKenkoHokenJigyoshoSeiriKg.Text));
            updParam.SetParam("@KENKO_HOKEN_JIGYOSHO_BANGO", SqlDbType.VarChar, 15, 
                Util.ToString(this.txtKenkoHokenJigyoshoBango.Text));
            updParam.SetParam("@KOSEI_NENKIN_HIHOKENSHA_FTN_RT", SqlDbType.Decimal, 5, 2,
                Util.ToDecimal(this.txtKoseiNenkinHihokenshaFtnRt.Text));
            updParam.SetParam("@KOSEI_NENKIN_HIHOKENSHA_FTN_KS", SqlDbType.Decimal, 4, 0,
                Util.ToDecimal(this.txtKoseiNenkinHihokenshaFtnKs.Text));
            updParam.SetParam("@KOSEI_NENKIN_JIGYOSHO_SEIRI_KG", SqlDbType.VarChar, 20, 
                Util.ToString(this.txtKoseiNenkinJigyoshoSeiriKg.Text));
            updParam.SetParam("@KOSEI_NENKIN_JIGYOSHO_BANGO", SqlDbType.VarChar, 15, 
                Util.ToString(this.txtKoseiNenkinJigyoshoBango.Text));
            updParam.SetParam("@GINKO_CD", SqlDbType.Decimal, 4, Util.ToDecimal(this.txtGinkoCd.Text));
            updParam.SetParam("@SHITEN_CD", SqlDbType.Decimal, 4, Util.ToDecimal(this.txtShitenCd.Text));
            updParam.SetParam("@ITAKU_CD", SqlDbType.VarChar, 10, Util.ToString(this.txtItakuCd.Text));
            updParam.SetParam("@SHUMOKU_CD", SqlDbType.VarChar, 10, Util.ToString(this.txtShumokuCd.Text));
            updParam.SetParam("@SHOTOKUZEI_KEISAN_HOHO", SqlDbType.Decimal, 1, 
                Util.ToDecimal(this.txtShotokuzeiKeisanHoho.Text));
            // 更新日
            updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWDATE");

            alParams.Add(updParam);

            return alParams;
        }

        #endregion

        #region privateメソッド(入力検査)

        /// <summary>
        /// 区分の入力チェック
        /// </summary>
        /// <param name="shubetsuCd">種別コード</param>
        /// <param name="textBox">入力テキストボックス</param>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidKubun(string shubetsuCd, FsiTextBox textBox)
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(textBox.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 存在しないコードを入力されたらエラー
            if (textBox.Text != "0")
            {
                string name = this.Dba.GetKyuyoKbnNm(shubetsuCd, textBox.Text);
                if (ValChk.IsEmpty(name))
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 銀行コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidGinkoCd(FsiTextBox textBox)
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(textBox.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 存在しないコードを入力されたらエラー
            if (textBox.Text != "0")
            {
                string name = this.Dba.GetName(this.UInfo, "TB_KY_GINKO", " ", textBox.Text);
                if (ValChk.IsEmpty(name))
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 支店コードの入力チェック
        /// </summary>
        /// <param name="ginkoCd">銀行コード</param>
        /// <param name="shitenCd">支店コード</param>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShitenCd(FsiTextBox ginkoCd, FsiTextBox shitenCd)
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(shitenCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 存在しないコードを入力されたらエラー
            if (shitenCd.Text != "0")
            {
                string name = this.Dba.GetGinkoShitenNm(ginkoCd.Text, shitenCd.Text);
                if (ValChk.IsEmpty(name))
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 文字フィールドの入力チェック
        /// </summary>
        /// <param name="textBox"></param>
        /// <returns></returns>
        private bool IsValidText(FsiTextBox textBox)
        {
            // 入力サイズ
            if (!ValChk.IsWithinLength(textBox.Text, textBox.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 整数値フィールドの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidInt(FsiTextBox textBox)
        {
            // 数値のみ入力可能
            if (!ValChk.IsNumber(textBox.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 入力サイズ値(書式なし数値で判定)
            if (!ValChk.IsWithinLength(Util.ToDecimal(textBox.Text), textBox.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 実数値フィールドの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidDec(FsiTextBox textBox)
        {
            // Decimal値チェック
            if (!ValChk.IsDecNumWithinLength(textBox.Text, textBox.MaxLength - 3, 2, true))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }


        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 雇用保険計算方法の入力チェック
            if (!IsValidKubun("7", this.txtKoyoHokenKeisanHoho))
            {
                this.txtKoyoHokenKeisanHoho.SelectAll();
                return false;
            }

            // 雇用保険業種の入力チェック
            if (!IsValidKubun("6", this.txtKoyoHokenGyoshu))
            {
                this.txtKoyoHokenGyoshu.SelectAll();
                return false;
            }

            // 雇用保険被保険者負担率の入力チェック
            if (!IsValidDec(this.txtKoyoHokenHihokenshaFtnRt))
            {
                this.txtKoyoHokenHihokenshaFtnRt.SelectAll();
                return false;
            }

            // 雇用保険被保険者負担率基数の入力チェック
            if (!IsValidInt(this.txtKoyoHokenHihokenshaFtnKs))
            {
                this.txtKoyoHokenHihokenshaFtnKs.SelectAll();
                return false;
            }

            // 健康保険被保険者負担率の入力チェック
            if (!IsValidDec(this.txtKenkoHokenHihokenshaFtnRt))
            {
                this.txtKenkoHokenHihokenshaFtnRt.SelectAll();
                return false;
            }

            // 健康保険被保険者負担率基数の入力チェック
            if (!IsValidInt(this.txtKenkoHokenHihokenshaFtnKs))
            {
                this.txtKenkoHokenHihokenshaFtnKs.SelectAll();
                return false;
            }

            // 健康保険被保険者負担率・介護の入力チェック
            if (!IsValidDec(this.txtKnkhknHihokenshaFtnRtKig))
            {
                this.txtKnkhknHihokenshaFtnRtKig.SelectAll();
                return false;
            }

            // 健康保険被保険者負担率基数・介護の入力チェック
            if (!IsValidInt(this.txtKnkhknHihokenshaFtnKsKig))
            {
                this.txtKnkhknHihokenshaFtnKsKig.SelectAll();
                return false;
            }

            // 健康保険事業者整理記号の入力チェック
            if (!IsValidText(this.txtKenkoHokenJigyoshoSeiriKg))
            {
                this.txtKenkoHokenJigyoshoSeiriKg.SelectAll();
                return false;
            }

            // 健康保険事業者番号の入力チェック
            if (!IsValidText(this.txtKenkoHokenJigyoshoBango))
            {
                this.txtKenkoHokenJigyoshoBango.SelectAll();
                return false;
            }

            // 厚生年金被保険者負担率の入力チェック
            if (!IsValidDec(this.txtKoseiNenkinHihokenshaFtnRt))
            {
                this.txtKoseiNenkinHihokenshaFtnRt.SelectAll();
                return false;
            }

            // 厚生年金被保険者負担率基数の入力チェック
            if (!IsValidInt(this.txtKoseiNenkinHihokenshaFtnKs))
            {
                this.txtKoseiNenkinHihokenshaFtnKs.SelectAll();
                return false;
            }

            // 厚生年金事業者整理記号の入力チェック
            if (!IsValidText(this.txtKoseiNenkinJigyoshoSeiriKg))
            {
                this.txtKoseiNenkinJigyoshoSeiriKg.SelectAll();
                return false;
            }

            // 厚生年金事業者番号の入力チェック
            if (!IsValidText(this.txtKoseiNenkinJigyoshoBango))
            {
                this.txtKoseiNenkinJigyoshoBango.SelectAll();
                return false;
            }

            // 銀行コードの入力チェック
            if (!IsValidGinkoCd(this.txtGinkoCd))
            {
                this.txtGinkoCd.SelectAll();
                return false;
            }

            // 支店コードの入力チェック
            if (!IsValidShitenCd(this.txtGinkoCd, this.txtShitenCd))
            {
                this.txtShitenCd.SelectAll();
                return false;
            }

            // 委託コードの入力チェック
            if (!IsValidText(this.txtItakuCd))
            {
                this.txtItakuCd.SelectAll();
                return false;
            }

            // 種目コードの入力チェック
            if (!IsValidText(this.txtShumokuCd))
            {
                this.txtShumokuCd.SelectAll();
                return false;
            }

            // 所得税計算方法の入力チェック
            if (!IsValidKubun("8", this.txtShotokuzeiKeisanHoho))
            {
                this.txtShotokuzeiKeisanHoho.SelectAll();
                return false;
            }

            return true;
        }

        #endregion

    }
}
