﻿namespace jp.co.fsi.ky.kyse1011
{
    partial class KYSE1012
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlSyain = new jp.co.fsi.common.FsiPanel();
            this.lblKyuyokeitai = new System.Windows.Forms.Label();
            this.lblShyozoku = new System.Windows.Forms.Label();
            this.lblShainNm = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtShainCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlKintai = new jp.co.fsi.common.FsiPanel();
            this.lblKintai20 = new System.Windows.Forms.Label();
            this.lblKintai19 = new System.Windows.Forms.Label();
            this.lblKintai18 = new System.Windows.Forms.Label();
            this.lblKintai17 = new System.Windows.Forms.Label();
            this.lblKintai16 = new System.Windows.Forms.Label();
            this.lblKintai15 = new System.Windows.Forms.Label();
            this.lblKintai14 = new System.Windows.Forms.Label();
            this.lblKintai13 = new System.Windows.Forms.Label();
            this.lblKintai12 = new System.Windows.Forms.Label();
            this.txtKintai20 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKintai19 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKintai18 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKintai17 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKintai16 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKintai15 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKintai14 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKintai13 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKintai12 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKintai10 = new System.Windows.Forms.Label();
            this.lblKintai9 = new System.Windows.Forms.Label();
            this.lblKintai8 = new System.Windows.Forms.Label();
            this.lblKintai7 = new System.Windows.Forms.Label();
            this.lblKintai6 = new System.Windows.Forms.Label();
            this.lblKintai5 = new System.Windows.Forms.Label();
            this.lblKintai4 = new System.Windows.Forms.Label();
            this.lblKintai3 = new System.Windows.Forms.Label();
            this.txtKintai11 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKintai11 = new System.Windows.Forms.Label();
            this.txtKintai1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKintai = new System.Windows.Forms.Label();
            this.lblKintai2 = new System.Windows.Forms.Label();
            this.txtKintai10 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKintai9 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKintai8 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKintai7 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKintai6 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKintai5 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKintai4 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKintai3 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKintai2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKintai1 = new System.Windows.Forms.Label();
            this.pnlShikyu = new jp.co.fsi.common.FsiPanel();
            this.lblShikyu20 = new System.Windows.Forms.Label();
            this.lblShikyu19 = new System.Windows.Forms.Label();
            this.lblShikyu18 = new System.Windows.Forms.Label();
            this.lblShikyu17 = new System.Windows.Forms.Label();
            this.lblShikyu16 = new System.Windows.Forms.Label();
            this.lblShikyu15 = new System.Windows.Forms.Label();
            this.lblShikyu14 = new System.Windows.Forms.Label();
            this.lblShikyu13 = new System.Windows.Forms.Label();
            this.lblShikyu12 = new System.Windows.Forms.Label();
            this.txtShikyu20 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtShikyu19 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtShikyu18 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtShikyu17 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtShikyu16 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtShikyu15 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtShikyu14 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtShikyu13 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtShikyu12 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShikyu10 = new System.Windows.Forms.Label();
            this.lblShikyu9 = new System.Windows.Forms.Label();
            this.lblShikyu8 = new System.Windows.Forms.Label();
            this.lblShikyu7 = new System.Windows.Forms.Label();
            this.lblShikyu6 = new System.Windows.Forms.Label();
            this.lblShikyu5 = new System.Windows.Forms.Label();
            this.lblShikyu4 = new System.Windows.Forms.Label();
            this.lblShikyu3 = new System.Windows.Forms.Label();
            this.txtShikyu11 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShikyu11 = new System.Windows.Forms.Label();
            this.txtShikyu1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShikyu = new System.Windows.Forms.Label();
            this.lblShikyu2 = new System.Windows.Forms.Label();
            this.txtShikyu10 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtShikyu9 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtShikyu8 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtShikyu7 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtShikyu6 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtShikyu5 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtShikyu4 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtShikyu3 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtShikyu2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShikyu1 = new System.Windows.Forms.Label();
            this.pnlKojo = new jp.co.fsi.common.FsiPanel();
            this.lblKojo20 = new System.Windows.Forms.Label();
            this.lblKojo19 = new System.Windows.Forms.Label();
            this.lblKojo18 = new System.Windows.Forms.Label();
            this.lblKojo17 = new System.Windows.Forms.Label();
            this.lblKojo16 = new System.Windows.Forms.Label();
            this.lblKojo15 = new System.Windows.Forms.Label();
            this.lblKojo14 = new System.Windows.Forms.Label();
            this.lblKojo13 = new System.Windows.Forms.Label();
            this.lblKojo12 = new System.Windows.Forms.Label();
            this.txtKojo20 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKojo19 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKojo18 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKojo17 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKojo16 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKojo15 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKojo14 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKojo13 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKojo12 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKojo10 = new System.Windows.Forms.Label();
            this.lblKojo9 = new System.Windows.Forms.Label();
            this.lblKojo8 = new System.Windows.Forms.Label();
            this.lblKojo7 = new System.Windows.Forms.Label();
            this.lblKojo6 = new System.Windows.Forms.Label();
            this.lblKojo5 = new System.Windows.Forms.Label();
            this.lblKojo4 = new System.Windows.Forms.Label();
            this.lblKojo3 = new System.Windows.Forms.Label();
            this.txtKojo11 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKojo11 = new System.Windows.Forms.Label();
            this.txtKojo1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKojo = new System.Windows.Forms.Label();
            this.lblKojo2 = new System.Windows.Forms.Label();
            this.txtKojo10 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKojo9 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKojo8 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKojo7 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKojo6 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKojo5 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKojo4 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKojo3 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKojo2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKojo1 = new System.Windows.Forms.Label();
            this.pnlShikyuInfo = new jp.co.fsi.common.FsiPanel();
            this.grpShikyubi = new System.Windows.Forms.GroupBox();
            this.lblDayShikyubi = new System.Windows.Forms.Label();
            this.lblMonthShikyubi = new System.Windows.Forms.Label();
            this.lblGengoYearShikyubi = new System.Windows.Forms.Label();
            this.lblShikyubi = new System.Windows.Forms.Label();
            this.lblGengoShikyubi = new System.Windows.Forms.Label();
            this.grpNengetsu = new System.Windows.Forms.GroupBox();
            this.lblMonthNengetsu = new System.Windows.Forms.Label();
            this.lblGengoYearNengetsu = new System.Windows.Forms.Label();
            this.lblNengetsu = new System.Windows.Forms.Label();
            this.lblGengoNengetsu = new System.Windows.Forms.Label();
            this.lblMode = new System.Windows.Forms.Label();
            this.pnlGokei = new jp.co.fsi.common.FsiPanel();
            this.lblGokei10 = new System.Windows.Forms.Label();
            this.lblGokei9 = new System.Windows.Forms.Label();
            this.lblGokei8 = new System.Windows.Forms.Label();
            this.lblGokei7 = new System.Windows.Forms.Label();
            this.lblGokei6 = new System.Windows.Forms.Label();
            this.lblGokei5 = new System.Windows.Forms.Label();
            this.lblGokei4 = new System.Windows.Forms.Label();
            this.lblGokei3 = new System.Windows.Forms.Label();
            this.txtGokei1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGokei = new System.Windows.Forms.Label();
            this.lblGokei2 = new System.Windows.Forms.Label();
            this.txtGokei10 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtGokei9 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtGokei8 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtGokei7 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtGokei6 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtGokei5 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtGokei4 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtGokei3 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtGokei2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGokei1 = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.pnlSyain.SuspendLayout();
            this.pnlKintai.SuspendLayout();
            this.pnlShikyu.SuspendLayout();
            this.pnlKojo.SuspendLayout();
            this.pnlShikyuInfo.SuspendLayout();
            this.grpShikyubi.SuspendLayout();
            this.grpNengetsu.SuspendLayout();
            this.pnlGokei.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Text = "賞与明細入力";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            // 
            // pnlSyain
            // 
            this.pnlSyain.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pnlSyain.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlSyain.Controls.Add(this.lblKyuyokeitai);
            this.pnlSyain.Controls.Add(this.lblShyozoku);
            this.pnlSyain.Controls.Add(this.lblShainNm);
            this.pnlSyain.Controls.Add(this.label4);
            this.pnlSyain.Controls.Add(this.label3);
            this.pnlSyain.Controls.Add(this.label2);
            this.pnlSyain.Controls.Add(this.txtShainCd);
            this.pnlSyain.Controls.Add(this.label1);
            this.pnlSyain.Location = new System.Drawing.Point(17, 47);
            this.pnlSyain.Name = "pnlSyain";
            this.pnlSyain.Size = new System.Drawing.Size(444, 54);
            this.pnlSyain.TabIndex = 1;
            // 
            // lblKyuyokeitai
            // 
            this.lblKyuyokeitai.BackColor = System.Drawing.Color.Silver;
            this.lblKyuyokeitai.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKyuyokeitai.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKyuyokeitai.Location = new System.Drawing.Point(362, 25);
            this.lblKyuyokeitai.Name = "lblKyuyokeitai";
            this.lblKyuyokeitai.Size = new System.Drawing.Size(72, 23);
            this.lblKyuyokeitai.TabIndex = 16;
            this.lblKyuyokeitai.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShyozoku
            // 
            this.lblShyozoku.BackColor = System.Drawing.Color.Silver;
            this.lblShyozoku.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShyozoku.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShyozoku.Location = new System.Drawing.Point(251, 25);
            this.lblShyozoku.Name = "lblShyozoku";
            this.lblShyozoku.Size = new System.Drawing.Size(111, 23);
            this.lblShyozoku.TabIndex = 15;
            this.lblShyozoku.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShainNm
            // 
            this.lblShainNm.BackColor = System.Drawing.Color.Silver;
            this.lblShainNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShainNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShainNm.Location = new System.Drawing.Point(81, 25);
            this.lblShainNm.Name = "lblShainNm";
            this.lblShainNm.Size = new System.Drawing.Size(170, 23);
            this.lblShainNm.TabIndex = 14;
            this.lblShainNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Silver;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.Location = new System.Drawing.Point(362, 6);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 17);
            this.label4.TabIndex = 10;
            this.label4.Text = "給与形態";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Silver;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(251, 6);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(111, 17);
            this.label3.TabIndex = 9;
            this.label3.Text = "所属";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Silver;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(81, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(170, 17);
            this.label2.TabIndex = 8;
            this.label2.Text = "氏　　名";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtShainCd
            // 
            this.txtShainCd.AutoSizeFromLength = false;
            this.txtShainCd.DisplayLength = null;
            this.txtShainCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShainCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtShainCd.Location = new System.Drawing.Point(5, 25);
            this.txtShainCd.MaxLength = 6;
            this.txtShainCd.Name = "txtShainCd";
            this.txtShainCd.Size = new System.Drawing.Size(76, 23);
            this.txtShainCd.TabIndex = 7;
            this.txtShainCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShainCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtShainCd_Validating);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Silver;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(5, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 17);
            this.label1.TabIndex = 6;
            this.label1.Text = "社員ｺｰﾄﾞ";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlKintai
            // 
            this.pnlKintai.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlKintai.Controls.Add(this.lblKintai20);
            this.pnlKintai.Controls.Add(this.lblKintai19);
            this.pnlKintai.Controls.Add(this.lblKintai18);
            this.pnlKintai.Controls.Add(this.lblKintai17);
            this.pnlKintai.Controls.Add(this.lblKintai16);
            this.pnlKintai.Controls.Add(this.lblKintai15);
            this.pnlKintai.Controls.Add(this.lblKintai14);
            this.pnlKintai.Controls.Add(this.lblKintai13);
            this.pnlKintai.Controls.Add(this.lblKintai12);
            this.pnlKintai.Controls.Add(this.txtKintai20);
            this.pnlKintai.Controls.Add(this.txtKintai19);
            this.pnlKintai.Controls.Add(this.txtKintai18);
            this.pnlKintai.Controls.Add(this.txtKintai17);
            this.pnlKintai.Controls.Add(this.txtKintai16);
            this.pnlKintai.Controls.Add(this.txtKintai15);
            this.pnlKintai.Controls.Add(this.txtKintai14);
            this.pnlKintai.Controls.Add(this.txtKintai13);
            this.pnlKintai.Controls.Add(this.txtKintai12);
            this.pnlKintai.Controls.Add(this.lblKintai10);
            this.pnlKintai.Controls.Add(this.lblKintai9);
            this.pnlKintai.Controls.Add(this.lblKintai8);
            this.pnlKintai.Controls.Add(this.lblKintai7);
            this.pnlKintai.Controls.Add(this.lblKintai6);
            this.pnlKintai.Controls.Add(this.lblKintai5);
            this.pnlKintai.Controls.Add(this.lblKintai4);
            this.pnlKintai.Controls.Add(this.lblKintai3);
            this.pnlKintai.Controls.Add(this.txtKintai11);
            this.pnlKintai.Controls.Add(this.lblKintai11);
            this.pnlKintai.Controls.Add(this.txtKintai1);
            this.pnlKintai.Controls.Add(this.lblKintai);
            this.pnlKintai.Controls.Add(this.lblKintai2);
            this.pnlKintai.Controls.Add(this.txtKintai10);
            this.pnlKintai.Controls.Add(this.txtKintai9);
            this.pnlKintai.Controls.Add(this.txtKintai8);
            this.pnlKintai.Controls.Add(this.txtKintai7);
            this.pnlKintai.Controls.Add(this.txtKintai6);
            this.pnlKintai.Controls.Add(this.txtKintai5);
            this.pnlKintai.Controls.Add(this.txtKintai4);
            this.pnlKintai.Controls.Add(this.txtKintai3);
            this.pnlKintai.Controls.Add(this.txtKintai2);
            this.pnlKintai.Controls.Add(this.lblKintai1);
            this.pnlKintai.Location = new System.Drawing.Point(17, 108);
            this.pnlKintai.Name = "pnlKintai";
            this.pnlKintai.Size = new System.Drawing.Size(789, 116);
            this.pnlKintai.TabIndex = 4;
            // 
            // lblKintai20
            // 
            this.lblKintai20.AutoEllipsis = true;
            this.lblKintai20.BackColor = System.Drawing.Color.Silver;
            this.lblKintai20.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKintai20.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKintai20.ForeColor = System.Drawing.Color.White;
            this.lblKintai20.Location = new System.Drawing.Point(706, 59);
            this.lblKintai20.Margin = new System.Windows.Forms.Padding(0);
            this.lblKintai20.Name = "lblKintai20";
            this.lblKintai20.Size = new System.Drawing.Size(75, 33);
            this.lblKintai20.TabIndex = 57;
            this.lblKintai20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblKintai19
            // 
            this.lblKintai19.AutoEllipsis = true;
            this.lblKintai19.BackColor = System.Drawing.Color.Silver;
            this.lblKintai19.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKintai19.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKintai19.ForeColor = System.Drawing.Color.White;
            this.lblKintai19.Location = new System.Drawing.Point(631, 59);
            this.lblKintai19.Margin = new System.Windows.Forms.Padding(0);
            this.lblKintai19.Name = "lblKintai19";
            this.lblKintai19.Size = new System.Drawing.Size(75, 33);
            this.lblKintai19.TabIndex = 56;
            this.lblKintai19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblKintai18
            // 
            this.lblKintai18.AutoEllipsis = true;
            this.lblKintai18.BackColor = System.Drawing.Color.Silver;
            this.lblKintai18.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKintai18.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKintai18.ForeColor = System.Drawing.Color.White;
            this.lblKintai18.Location = new System.Drawing.Point(556, 59);
            this.lblKintai18.Margin = new System.Windows.Forms.Padding(0);
            this.lblKintai18.Name = "lblKintai18";
            this.lblKintai18.Size = new System.Drawing.Size(75, 33);
            this.lblKintai18.TabIndex = 55;
            this.lblKintai18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblKintai17
            // 
            this.lblKintai17.AutoEllipsis = true;
            this.lblKintai17.BackColor = System.Drawing.Color.Silver;
            this.lblKintai17.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKintai17.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKintai17.ForeColor = System.Drawing.Color.White;
            this.lblKintai17.Location = new System.Drawing.Point(481, 59);
            this.lblKintai17.Margin = new System.Windows.Forms.Padding(0);
            this.lblKintai17.Name = "lblKintai17";
            this.lblKintai17.Size = new System.Drawing.Size(75, 33);
            this.lblKintai17.TabIndex = 54;
            this.lblKintai17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblKintai16
            // 
            this.lblKintai16.AutoEllipsis = true;
            this.lblKintai16.BackColor = System.Drawing.Color.Silver;
            this.lblKintai16.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKintai16.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKintai16.ForeColor = System.Drawing.Color.White;
            this.lblKintai16.Location = new System.Drawing.Point(406, 59);
            this.lblKintai16.Margin = new System.Windows.Forms.Padding(0);
            this.lblKintai16.Name = "lblKintai16";
            this.lblKintai16.Size = new System.Drawing.Size(75, 33);
            this.lblKintai16.TabIndex = 53;
            this.lblKintai16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblKintai15
            // 
            this.lblKintai15.AutoEllipsis = true;
            this.lblKintai15.BackColor = System.Drawing.Color.Silver;
            this.lblKintai15.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKintai15.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKintai15.ForeColor = System.Drawing.Color.White;
            this.lblKintai15.Location = new System.Drawing.Point(331, 59);
            this.lblKintai15.Margin = new System.Windows.Forms.Padding(0);
            this.lblKintai15.Name = "lblKintai15";
            this.lblKintai15.Size = new System.Drawing.Size(75, 33);
            this.lblKintai15.TabIndex = 52;
            this.lblKintai15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblKintai14
            // 
            this.lblKintai14.AutoEllipsis = true;
            this.lblKintai14.BackColor = System.Drawing.Color.Silver;
            this.lblKintai14.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKintai14.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKintai14.ForeColor = System.Drawing.Color.White;
            this.lblKintai14.Location = new System.Drawing.Point(256, 59);
            this.lblKintai14.Margin = new System.Windows.Forms.Padding(0);
            this.lblKintai14.Name = "lblKintai14";
            this.lblKintai14.Size = new System.Drawing.Size(75, 33);
            this.lblKintai14.TabIndex = 51;
            this.lblKintai14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblKintai13
            // 
            this.lblKintai13.AutoEllipsis = true;
            this.lblKintai13.BackColor = System.Drawing.Color.Silver;
            this.lblKintai13.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKintai13.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKintai13.ForeColor = System.Drawing.Color.White;
            this.lblKintai13.Location = new System.Drawing.Point(181, 59);
            this.lblKintai13.Margin = new System.Windows.Forms.Padding(0);
            this.lblKintai13.Name = "lblKintai13";
            this.lblKintai13.Size = new System.Drawing.Size(75, 33);
            this.lblKintai13.TabIndex = 50;
            this.lblKintai13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblKintai12
            // 
            this.lblKintai12.AutoEllipsis = true;
            this.lblKintai12.BackColor = System.Drawing.Color.Silver;
            this.lblKintai12.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKintai12.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKintai12.ForeColor = System.Drawing.Color.White;
            this.lblKintai12.Location = new System.Drawing.Point(106, 59);
            this.lblKintai12.Margin = new System.Windows.Forms.Padding(0);
            this.lblKintai12.Name = "lblKintai12";
            this.lblKintai12.Size = new System.Drawing.Size(75, 33);
            this.lblKintai12.TabIndex = 49;
            this.lblKintai12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtKintai20
            // 
            this.txtKintai20.AutoSizeFromLength = false;
            this.txtKintai20.DisplayLength = null;
            this.txtKintai20.Enabled = false;
            this.txtKintai20.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKintai20.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKintai20.Location = new System.Drawing.Point(706, 92);
            this.txtKintai20.MaxLength = 6;
            this.txtKintai20.Name = "txtKintai20";
            this.txtKintai20.Size = new System.Drawing.Size(75, 20);
            this.txtKintai20.TabIndex = 48;
            this.txtKintai20.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKintai20.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtKintai20.Validating += new System.ComponentModel.CancelEventHandler(this.txtKintai_Validating);
            // 
            // txtKintai19
            // 
            this.txtKintai19.AutoSizeFromLength = false;
            this.txtKintai19.DisplayLength = null;
            this.txtKintai19.Enabled = false;
            this.txtKintai19.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKintai19.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKintai19.Location = new System.Drawing.Point(631, 92);
            this.txtKintai19.MaxLength = 6;
            this.txtKintai19.Name = "txtKintai19";
            this.txtKintai19.Size = new System.Drawing.Size(75, 20);
            this.txtKintai19.TabIndex = 47;
            this.txtKintai19.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKintai19.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtKintai19.Validating += new System.ComponentModel.CancelEventHandler(this.txtKintai_Validating);
            // 
            // txtKintai18
            // 
            this.txtKintai18.AutoSizeFromLength = false;
            this.txtKintai18.DisplayLength = null;
            this.txtKintai18.Enabled = false;
            this.txtKintai18.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKintai18.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKintai18.Location = new System.Drawing.Point(556, 92);
            this.txtKintai18.MaxLength = 6;
            this.txtKintai18.Name = "txtKintai18";
            this.txtKintai18.Size = new System.Drawing.Size(75, 20);
            this.txtKintai18.TabIndex = 46;
            this.txtKintai18.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKintai18.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtKintai18.Validating += new System.ComponentModel.CancelEventHandler(this.txtKintai_Validating);
            // 
            // txtKintai17
            // 
            this.txtKintai17.AutoSizeFromLength = false;
            this.txtKintai17.DisplayLength = null;
            this.txtKintai17.Enabled = false;
            this.txtKintai17.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKintai17.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKintai17.Location = new System.Drawing.Point(481, 92);
            this.txtKintai17.MaxLength = 6;
            this.txtKintai17.Name = "txtKintai17";
            this.txtKintai17.Size = new System.Drawing.Size(75, 20);
            this.txtKintai17.TabIndex = 45;
            this.txtKintai17.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKintai17.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtKintai17.Validating += new System.ComponentModel.CancelEventHandler(this.txtKintai_Validating);
            // 
            // txtKintai16
            // 
            this.txtKintai16.AutoSizeFromLength = false;
            this.txtKintai16.DisplayLength = null;
            this.txtKintai16.Enabled = false;
            this.txtKintai16.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKintai16.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKintai16.Location = new System.Drawing.Point(406, 92);
            this.txtKintai16.MaxLength = 6;
            this.txtKintai16.Name = "txtKintai16";
            this.txtKintai16.Size = new System.Drawing.Size(75, 20);
            this.txtKintai16.TabIndex = 44;
            this.txtKintai16.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKintai16.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtKintai16.Validating += new System.ComponentModel.CancelEventHandler(this.txtKintai_Validating);
            // 
            // txtKintai15
            // 
            this.txtKintai15.AutoSizeFromLength = false;
            this.txtKintai15.DisplayLength = null;
            this.txtKintai15.Enabled = false;
            this.txtKintai15.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKintai15.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKintai15.Location = new System.Drawing.Point(331, 92);
            this.txtKintai15.MaxLength = 6;
            this.txtKintai15.Name = "txtKintai15";
            this.txtKintai15.Size = new System.Drawing.Size(75, 20);
            this.txtKintai15.TabIndex = 43;
            this.txtKintai15.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKintai15.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtKintai15.Validating += new System.ComponentModel.CancelEventHandler(this.txtKintai_Validating);
            // 
            // txtKintai14
            // 
            this.txtKintai14.AutoSizeFromLength = false;
            this.txtKintai14.DisplayLength = null;
            this.txtKintai14.Enabled = false;
            this.txtKintai14.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKintai14.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKintai14.Location = new System.Drawing.Point(256, 92);
            this.txtKintai14.MaxLength = 6;
            this.txtKintai14.Name = "txtKintai14";
            this.txtKintai14.Size = new System.Drawing.Size(75, 20);
            this.txtKintai14.TabIndex = 42;
            this.txtKintai14.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKintai14.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtKintai14.Validating += new System.ComponentModel.CancelEventHandler(this.txtKintai_Validating);
            // 
            // txtKintai13
            // 
            this.txtKintai13.AutoSizeFromLength = false;
            this.txtKintai13.DisplayLength = null;
            this.txtKintai13.Enabled = false;
            this.txtKintai13.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKintai13.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKintai13.Location = new System.Drawing.Point(181, 92);
            this.txtKintai13.MaxLength = 6;
            this.txtKintai13.Name = "txtKintai13";
            this.txtKintai13.Size = new System.Drawing.Size(75, 20);
            this.txtKintai13.TabIndex = 41;
            this.txtKintai13.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKintai13.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtKintai13.Validating += new System.ComponentModel.CancelEventHandler(this.txtKintai_Validating);
            // 
            // txtKintai12
            // 
            this.txtKintai12.AutoSizeFromLength = false;
            this.txtKintai12.DisplayLength = null;
            this.txtKintai12.Enabled = false;
            this.txtKintai12.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKintai12.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKintai12.Location = new System.Drawing.Point(106, 92);
            this.txtKintai12.MaxLength = 6;
            this.txtKintai12.Name = "txtKintai12";
            this.txtKintai12.Size = new System.Drawing.Size(75, 20);
            this.txtKintai12.TabIndex = 40;
            this.txtKintai12.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKintai12.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtKintai12.Validating += new System.ComponentModel.CancelEventHandler(this.txtKintai_Validating);
            // 
            // lblKintai10
            // 
            this.lblKintai10.AutoEllipsis = true;
            this.lblKintai10.BackColor = System.Drawing.Color.Silver;
            this.lblKintai10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKintai10.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKintai10.ForeColor = System.Drawing.Color.White;
            this.lblKintai10.Location = new System.Drawing.Point(706, 3);
            this.lblKintai10.Margin = new System.Windows.Forms.Padding(0);
            this.lblKintai10.Name = "lblKintai10";
            this.lblKintai10.Size = new System.Drawing.Size(75, 33);
            this.lblKintai10.TabIndex = 39;
            this.lblKintai10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblKintai9
            // 
            this.lblKintai9.AutoEllipsis = true;
            this.lblKintai9.BackColor = System.Drawing.Color.Silver;
            this.lblKintai9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKintai9.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKintai9.ForeColor = System.Drawing.Color.White;
            this.lblKintai9.Location = new System.Drawing.Point(631, 3);
            this.lblKintai9.Margin = new System.Windows.Forms.Padding(0);
            this.lblKintai9.Name = "lblKintai9";
            this.lblKintai9.Size = new System.Drawing.Size(75, 33);
            this.lblKintai9.TabIndex = 38;
            this.lblKintai9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblKintai8
            // 
            this.lblKintai8.AutoEllipsis = true;
            this.lblKintai8.BackColor = System.Drawing.Color.Silver;
            this.lblKintai8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKintai8.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKintai8.ForeColor = System.Drawing.Color.White;
            this.lblKintai8.Location = new System.Drawing.Point(556, 3);
            this.lblKintai8.Margin = new System.Windows.Forms.Padding(0);
            this.lblKintai8.Name = "lblKintai8";
            this.lblKintai8.Size = new System.Drawing.Size(75, 33);
            this.lblKintai8.TabIndex = 37;
            this.lblKintai8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblKintai7
            // 
            this.lblKintai7.AutoEllipsis = true;
            this.lblKintai7.BackColor = System.Drawing.Color.Silver;
            this.lblKintai7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKintai7.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKintai7.ForeColor = System.Drawing.Color.White;
            this.lblKintai7.Location = new System.Drawing.Point(481, 3);
            this.lblKintai7.Margin = new System.Windows.Forms.Padding(0);
            this.lblKintai7.Name = "lblKintai7";
            this.lblKintai7.Size = new System.Drawing.Size(75, 33);
            this.lblKintai7.TabIndex = 36;
            this.lblKintai7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblKintai6
            // 
            this.lblKintai6.AutoEllipsis = true;
            this.lblKintai6.BackColor = System.Drawing.Color.Silver;
            this.lblKintai6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKintai6.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKintai6.ForeColor = System.Drawing.Color.White;
            this.lblKintai6.Location = new System.Drawing.Point(406, 3);
            this.lblKintai6.Margin = new System.Windows.Forms.Padding(0);
            this.lblKintai6.Name = "lblKintai6";
            this.lblKintai6.Size = new System.Drawing.Size(75, 33);
            this.lblKintai6.TabIndex = 35;
            this.lblKintai6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblKintai5
            // 
            this.lblKintai5.AutoEllipsis = true;
            this.lblKintai5.BackColor = System.Drawing.Color.Silver;
            this.lblKintai5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKintai5.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKintai5.ForeColor = System.Drawing.Color.White;
            this.lblKintai5.Location = new System.Drawing.Point(331, 3);
            this.lblKintai5.Margin = new System.Windows.Forms.Padding(0);
            this.lblKintai5.Name = "lblKintai5";
            this.lblKintai5.Size = new System.Drawing.Size(75, 33);
            this.lblKintai5.TabIndex = 34;
            this.lblKintai5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblKintai4
            // 
            this.lblKintai4.AutoEllipsis = true;
            this.lblKintai4.BackColor = System.Drawing.Color.Silver;
            this.lblKintai4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKintai4.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKintai4.ForeColor = System.Drawing.Color.White;
            this.lblKintai4.Location = new System.Drawing.Point(256, 3);
            this.lblKintai4.Margin = new System.Windows.Forms.Padding(0);
            this.lblKintai4.Name = "lblKintai4";
            this.lblKintai4.Size = new System.Drawing.Size(75, 33);
            this.lblKintai4.TabIndex = 33;
            this.lblKintai4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblKintai3
            // 
            this.lblKintai3.AutoEllipsis = true;
            this.lblKintai3.BackColor = System.Drawing.Color.Silver;
            this.lblKintai3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKintai3.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKintai3.ForeColor = System.Drawing.Color.White;
            this.lblKintai3.Location = new System.Drawing.Point(181, 3);
            this.lblKintai3.Margin = new System.Windows.Forms.Padding(0);
            this.lblKintai3.Name = "lblKintai3";
            this.lblKintai3.Size = new System.Drawing.Size(75, 33);
            this.lblKintai3.TabIndex = 32;
            this.lblKintai3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtKintai11
            // 
            this.txtKintai11.AutoSizeFromLength = false;
            this.txtKintai11.DisplayLength = null;
            this.txtKintai11.Enabled = false;
            this.txtKintai11.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKintai11.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKintai11.Location = new System.Drawing.Point(31, 92);
            this.txtKintai11.MaxLength = 6;
            this.txtKintai11.Name = "txtKintai11";
            this.txtKintai11.Size = new System.Drawing.Size(75, 20);
            this.txtKintai11.TabIndex = 31;
            this.txtKintai11.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKintai11.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtKintai11.Validating += new System.ComponentModel.CancelEventHandler(this.txtKintai_Validating);
            // 
            // lblKintai11
            // 
            this.lblKintai11.AutoEllipsis = true;
            this.lblKintai11.BackColor = System.Drawing.Color.Silver;
            this.lblKintai11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKintai11.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKintai11.ForeColor = System.Drawing.Color.White;
            this.lblKintai11.Location = new System.Drawing.Point(31, 59);
            this.lblKintai11.Margin = new System.Windows.Forms.Padding(0);
            this.lblKintai11.Name = "lblKintai11";
            this.lblKintai11.Size = new System.Drawing.Size(75, 33);
            this.lblKintai11.TabIndex = 30;
            this.lblKintai11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtKintai1
            // 
            this.txtKintai1.AutoSizeFromLength = false;
            this.txtKintai1.DisplayLength = null;
            this.txtKintai1.Enabled = false;
            this.txtKintai1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKintai1.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKintai1.Location = new System.Drawing.Point(31, 36);
            this.txtKintai1.MaxLength = 6;
            this.txtKintai1.Name = "txtKintai1";
            this.txtKintai1.Size = new System.Drawing.Size(75, 20);
            this.txtKintai1.TabIndex = 10;
            this.txtKintai1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKintai1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtKintai1.Validating += new System.ComponentModel.CancelEventHandler(this.txtKintai_Validating);
            // 
            // lblKintai
            // 
            this.lblKintai.AutoEllipsis = true;
            this.lblKintai.BackColor = System.Drawing.Color.Silver;
            this.lblKintai.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblKintai.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKintai.Location = new System.Drawing.Point(5, 3);
            this.lblKintai.Margin = new System.Windows.Forms.Padding(0);
            this.lblKintai.Name = "lblKintai";
            this.lblKintai.Size = new System.Drawing.Size(23, 108);
            this.lblKintai.TabIndex = 7;
            this.lblKintai.Text = "勤　怠";
            this.lblKintai.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblKintai2
            // 
            this.lblKintai2.AutoEllipsis = true;
            this.lblKintai2.BackColor = System.Drawing.Color.Silver;
            this.lblKintai2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKintai2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKintai2.ForeColor = System.Drawing.Color.White;
            this.lblKintai2.Location = new System.Drawing.Point(106, 3);
            this.lblKintai2.Margin = new System.Windows.Forms.Padding(0);
            this.lblKintai2.Name = "lblKintai2";
            this.lblKintai2.Size = new System.Drawing.Size(75, 33);
            this.lblKintai2.TabIndex = 27;
            this.lblKintai2.Text = "１２３４５６";
            this.lblKintai2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtKintai10
            // 
            this.txtKintai10.AutoSizeFromLength = false;
            this.txtKintai10.DisplayLength = null;
            this.txtKintai10.Enabled = false;
            this.txtKintai10.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKintai10.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKintai10.Location = new System.Drawing.Point(706, 36);
            this.txtKintai10.MaxLength = 6;
            this.txtKintai10.Name = "txtKintai10";
            this.txtKintai10.Size = new System.Drawing.Size(75, 20);
            this.txtKintai10.TabIndex = 26;
            this.txtKintai10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKintai10.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtKintai10.Validating += new System.ComponentModel.CancelEventHandler(this.txtKintai_Validating);
            // 
            // txtKintai9
            // 
            this.txtKintai9.AutoSizeFromLength = false;
            this.txtKintai9.DisplayLength = null;
            this.txtKintai9.Enabled = false;
            this.txtKintai9.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKintai9.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKintai9.Location = new System.Drawing.Point(631, 36);
            this.txtKintai9.MaxLength = 6;
            this.txtKintai9.Name = "txtKintai9";
            this.txtKintai9.Size = new System.Drawing.Size(75, 20);
            this.txtKintai9.TabIndex = 25;
            this.txtKintai9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKintai9.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtKintai9.Validating += new System.ComponentModel.CancelEventHandler(this.txtKintai_Validating);
            // 
            // txtKintai8
            // 
            this.txtKintai8.AutoSizeFromLength = false;
            this.txtKintai8.BackColor = System.Drawing.Color.White;
            this.txtKintai8.DisplayLength = null;
            this.txtKintai8.Enabled = false;
            this.txtKintai8.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKintai8.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKintai8.Location = new System.Drawing.Point(556, 36);
            this.txtKintai8.MaxLength = 6;
            this.txtKintai8.Name = "txtKintai8";
            this.txtKintai8.Size = new System.Drawing.Size(75, 20);
            this.txtKintai8.TabIndex = 24;
            this.txtKintai8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKintai8.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtKintai8.Validating += new System.ComponentModel.CancelEventHandler(this.txtKintai_Validating);
            // 
            // txtKintai7
            // 
            this.txtKintai7.AutoSizeFromLength = false;
            this.txtKintai7.DisplayLength = null;
            this.txtKintai7.Enabled = false;
            this.txtKintai7.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKintai7.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKintai7.Location = new System.Drawing.Point(481, 36);
            this.txtKintai7.MaxLength = 6;
            this.txtKintai7.Name = "txtKintai7";
            this.txtKintai7.Size = new System.Drawing.Size(75, 20);
            this.txtKintai7.TabIndex = 23;
            this.txtKintai7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKintai7.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtKintai7.Validating += new System.ComponentModel.CancelEventHandler(this.txtKintai_Validating);
            // 
            // txtKintai6
            // 
            this.txtKintai6.AutoSizeFromLength = false;
            this.txtKintai6.DisplayLength = null;
            this.txtKintai6.Enabled = false;
            this.txtKintai6.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKintai6.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKintai6.Location = new System.Drawing.Point(406, 36);
            this.txtKintai6.MaxLength = 6;
            this.txtKintai6.Name = "txtKintai6";
            this.txtKintai6.Size = new System.Drawing.Size(75, 20);
            this.txtKintai6.TabIndex = 22;
            this.txtKintai6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKintai6.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtKintai6.Validating += new System.ComponentModel.CancelEventHandler(this.txtKintai_Validating);
            // 
            // txtKintai5
            // 
            this.txtKintai5.AutoSizeFromLength = false;
            this.txtKintai5.DisplayLength = null;
            this.txtKintai5.Enabled = false;
            this.txtKintai5.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKintai5.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKintai5.Location = new System.Drawing.Point(331, 36);
            this.txtKintai5.MaxLength = 6;
            this.txtKintai5.Name = "txtKintai5";
            this.txtKintai5.Size = new System.Drawing.Size(75, 20);
            this.txtKintai5.TabIndex = 21;
            this.txtKintai5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKintai5.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtKintai5.Validating += new System.ComponentModel.CancelEventHandler(this.txtKintai_Validating);
            // 
            // txtKintai4
            // 
            this.txtKintai4.AutoSizeFromLength = false;
            this.txtKintai4.DisplayLength = null;
            this.txtKintai4.Enabled = false;
            this.txtKintai4.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKintai4.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKintai4.Location = new System.Drawing.Point(256, 36);
            this.txtKintai4.MaxLength = 6;
            this.txtKintai4.Name = "txtKintai4";
            this.txtKintai4.Size = new System.Drawing.Size(75, 20);
            this.txtKintai4.TabIndex = 20;
            this.txtKintai4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKintai4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtKintai4.Validating += new System.ComponentModel.CancelEventHandler(this.txtKintai_Validating);
            // 
            // txtKintai3
            // 
            this.txtKintai3.AutoSizeFromLength = false;
            this.txtKintai3.DisplayLength = null;
            this.txtKintai3.Enabled = false;
            this.txtKintai3.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKintai3.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKintai3.Location = new System.Drawing.Point(181, 36);
            this.txtKintai3.MaxLength = 6;
            this.txtKintai3.Name = "txtKintai3";
            this.txtKintai3.Size = new System.Drawing.Size(75, 20);
            this.txtKintai3.TabIndex = 19;
            this.txtKintai3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKintai3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtKintai3.Validating += new System.ComponentModel.CancelEventHandler(this.txtKintai_Validating);
            // 
            // txtKintai2
            // 
            this.txtKintai2.AutoSizeFromLength = false;
            this.txtKintai2.DisplayLength = null;
            this.txtKintai2.Enabled = false;
            this.txtKintai2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKintai2.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKintai2.Location = new System.Drawing.Point(106, 36);
            this.txtKintai2.MaxLength = 6;
            this.txtKintai2.Name = "txtKintai2";
            this.txtKintai2.Size = new System.Drawing.Size(75, 20);
            this.txtKintai2.TabIndex = 18;
            this.txtKintai2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKintai2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtKintai2.Validating += new System.ComponentModel.CancelEventHandler(this.txtKintai_Validating);
            // 
            // lblKintai1
            // 
            this.lblKintai1.AutoEllipsis = true;
            this.lblKintai1.BackColor = System.Drawing.Color.Silver;
            this.lblKintai1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKintai1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKintai1.ForeColor = System.Drawing.Color.White;
            this.lblKintai1.Location = new System.Drawing.Point(31, 3);
            this.lblKintai1.Margin = new System.Windows.Forms.Padding(0);
            this.lblKintai1.Name = "lblKintai1";
            this.lblKintai1.Size = new System.Drawing.Size(75, 33);
            this.lblKintai1.TabIndex = 6;
            this.lblKintai1.Text = "基本給";
            this.lblKintai1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlShikyu
            // 
            this.pnlShikyu.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlShikyu.Controls.Add(this.lblShikyu20);
            this.pnlShikyu.Controls.Add(this.lblShikyu19);
            this.pnlShikyu.Controls.Add(this.lblShikyu18);
            this.pnlShikyu.Controls.Add(this.lblShikyu17);
            this.pnlShikyu.Controls.Add(this.lblShikyu16);
            this.pnlShikyu.Controls.Add(this.lblShikyu15);
            this.pnlShikyu.Controls.Add(this.lblShikyu14);
            this.pnlShikyu.Controls.Add(this.lblShikyu13);
            this.pnlShikyu.Controls.Add(this.lblShikyu12);
            this.pnlShikyu.Controls.Add(this.txtShikyu20);
            this.pnlShikyu.Controls.Add(this.txtShikyu19);
            this.pnlShikyu.Controls.Add(this.txtShikyu18);
            this.pnlShikyu.Controls.Add(this.txtShikyu17);
            this.pnlShikyu.Controls.Add(this.txtShikyu16);
            this.pnlShikyu.Controls.Add(this.txtShikyu15);
            this.pnlShikyu.Controls.Add(this.txtShikyu14);
            this.pnlShikyu.Controls.Add(this.txtShikyu13);
            this.pnlShikyu.Controls.Add(this.txtShikyu12);
            this.pnlShikyu.Controls.Add(this.lblShikyu10);
            this.pnlShikyu.Controls.Add(this.lblShikyu9);
            this.pnlShikyu.Controls.Add(this.lblShikyu8);
            this.pnlShikyu.Controls.Add(this.lblShikyu7);
            this.pnlShikyu.Controls.Add(this.lblShikyu6);
            this.pnlShikyu.Controls.Add(this.lblShikyu5);
            this.pnlShikyu.Controls.Add(this.lblShikyu4);
            this.pnlShikyu.Controls.Add(this.lblShikyu3);
            this.pnlShikyu.Controls.Add(this.txtShikyu11);
            this.pnlShikyu.Controls.Add(this.lblShikyu11);
            this.pnlShikyu.Controls.Add(this.txtShikyu1);
            this.pnlShikyu.Controls.Add(this.lblShikyu);
            this.pnlShikyu.Controls.Add(this.lblShikyu2);
            this.pnlShikyu.Controls.Add(this.txtShikyu10);
            this.pnlShikyu.Controls.Add(this.txtShikyu9);
            this.pnlShikyu.Controls.Add(this.txtShikyu8);
            this.pnlShikyu.Controls.Add(this.txtShikyu7);
            this.pnlShikyu.Controls.Add(this.txtShikyu6);
            this.pnlShikyu.Controls.Add(this.txtShikyu5);
            this.pnlShikyu.Controls.Add(this.txtShikyu4);
            this.pnlShikyu.Controls.Add(this.txtShikyu3);
            this.pnlShikyu.Controls.Add(this.txtShikyu2);
            this.pnlShikyu.Controls.Add(this.lblShikyu1);
            this.pnlShikyu.Location = new System.Drawing.Point(17, 228);
            this.pnlShikyu.Name = "pnlShikyu";
            this.pnlShikyu.Size = new System.Drawing.Size(789, 116);
            this.pnlShikyu.TabIndex = 5;
            // 
            // lblShikyu20
            // 
            this.lblShikyu20.AutoEllipsis = true;
            this.lblShikyu20.BackColor = System.Drawing.Color.Silver;
            this.lblShikyu20.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShikyu20.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShikyu20.ForeColor = System.Drawing.Color.White;
            this.lblShikyu20.Location = new System.Drawing.Point(706, 59);
            this.lblShikyu20.Margin = new System.Windows.Forms.Padding(0);
            this.lblShikyu20.Name = "lblShikyu20";
            this.lblShikyu20.Size = new System.Drawing.Size(75, 33);
            this.lblShikyu20.TabIndex = 57;
            this.lblShikyu20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblShikyu19
            // 
            this.lblShikyu19.AutoEllipsis = true;
            this.lblShikyu19.BackColor = System.Drawing.Color.Silver;
            this.lblShikyu19.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShikyu19.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShikyu19.ForeColor = System.Drawing.Color.White;
            this.lblShikyu19.Location = new System.Drawing.Point(631, 59);
            this.lblShikyu19.Margin = new System.Windows.Forms.Padding(0);
            this.lblShikyu19.Name = "lblShikyu19";
            this.lblShikyu19.Size = new System.Drawing.Size(75, 33);
            this.lblShikyu19.TabIndex = 56;
            this.lblShikyu19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblShikyu18
            // 
            this.lblShikyu18.AutoEllipsis = true;
            this.lblShikyu18.BackColor = System.Drawing.Color.Silver;
            this.lblShikyu18.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShikyu18.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShikyu18.ForeColor = System.Drawing.Color.White;
            this.lblShikyu18.Location = new System.Drawing.Point(556, 59);
            this.lblShikyu18.Margin = new System.Windows.Forms.Padding(0);
            this.lblShikyu18.Name = "lblShikyu18";
            this.lblShikyu18.Size = new System.Drawing.Size(75, 33);
            this.lblShikyu18.TabIndex = 55;
            this.lblShikyu18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblShikyu17
            // 
            this.lblShikyu17.AutoEllipsis = true;
            this.lblShikyu17.BackColor = System.Drawing.Color.Silver;
            this.lblShikyu17.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShikyu17.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShikyu17.ForeColor = System.Drawing.Color.White;
            this.lblShikyu17.Location = new System.Drawing.Point(481, 59);
            this.lblShikyu17.Margin = new System.Windows.Forms.Padding(0);
            this.lblShikyu17.Name = "lblShikyu17";
            this.lblShikyu17.Size = new System.Drawing.Size(75, 33);
            this.lblShikyu17.TabIndex = 54;
            this.lblShikyu17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblShikyu16
            // 
            this.lblShikyu16.AutoEllipsis = true;
            this.lblShikyu16.BackColor = System.Drawing.Color.Silver;
            this.lblShikyu16.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShikyu16.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShikyu16.ForeColor = System.Drawing.Color.White;
            this.lblShikyu16.Location = new System.Drawing.Point(406, 59);
            this.lblShikyu16.Margin = new System.Windows.Forms.Padding(0);
            this.lblShikyu16.Name = "lblShikyu16";
            this.lblShikyu16.Size = new System.Drawing.Size(75, 33);
            this.lblShikyu16.TabIndex = 53;
            this.lblShikyu16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblShikyu15
            // 
            this.lblShikyu15.AutoEllipsis = true;
            this.lblShikyu15.BackColor = System.Drawing.Color.Silver;
            this.lblShikyu15.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShikyu15.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShikyu15.ForeColor = System.Drawing.Color.White;
            this.lblShikyu15.Location = new System.Drawing.Point(331, 59);
            this.lblShikyu15.Margin = new System.Windows.Forms.Padding(0);
            this.lblShikyu15.Name = "lblShikyu15";
            this.lblShikyu15.Size = new System.Drawing.Size(75, 33);
            this.lblShikyu15.TabIndex = 52;
            this.lblShikyu15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblShikyu14
            // 
            this.lblShikyu14.AutoEllipsis = true;
            this.lblShikyu14.BackColor = System.Drawing.Color.Silver;
            this.lblShikyu14.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShikyu14.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShikyu14.ForeColor = System.Drawing.Color.White;
            this.lblShikyu14.Location = new System.Drawing.Point(256, 59);
            this.lblShikyu14.Margin = new System.Windows.Forms.Padding(0);
            this.lblShikyu14.Name = "lblShikyu14";
            this.lblShikyu14.Size = new System.Drawing.Size(75, 33);
            this.lblShikyu14.TabIndex = 51;
            this.lblShikyu14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblShikyu13
            // 
            this.lblShikyu13.AutoEllipsis = true;
            this.lblShikyu13.BackColor = System.Drawing.Color.Silver;
            this.lblShikyu13.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShikyu13.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShikyu13.ForeColor = System.Drawing.Color.White;
            this.lblShikyu13.Location = new System.Drawing.Point(181, 59);
            this.lblShikyu13.Margin = new System.Windows.Forms.Padding(0);
            this.lblShikyu13.Name = "lblShikyu13";
            this.lblShikyu13.Size = new System.Drawing.Size(75, 33);
            this.lblShikyu13.TabIndex = 50;
            this.lblShikyu13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblShikyu12
            // 
            this.lblShikyu12.AutoEllipsis = true;
            this.lblShikyu12.BackColor = System.Drawing.Color.Silver;
            this.lblShikyu12.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShikyu12.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShikyu12.ForeColor = System.Drawing.Color.White;
            this.lblShikyu12.Location = new System.Drawing.Point(106, 59);
            this.lblShikyu12.Margin = new System.Windows.Forms.Padding(0);
            this.lblShikyu12.Name = "lblShikyu12";
            this.lblShikyu12.Size = new System.Drawing.Size(75, 33);
            this.lblShikyu12.TabIndex = 49;
            this.lblShikyu12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtShikyu20
            // 
            this.txtShikyu20.AutoSizeFromLength = false;
            this.txtShikyu20.DisplayLength = null;
            this.txtShikyu20.Enabled = false;
            this.txtShikyu20.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShikyu20.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtShikyu20.Location = new System.Drawing.Point(706, 92);
            this.txtShikyu20.MaxLength = 10;
            this.txtShikyu20.Name = "txtShikyu20";
            this.txtShikyu20.Size = new System.Drawing.Size(75, 20);
            this.txtShikyu20.TabIndex = 48;
            this.txtShikyu20.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShikyu20.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtShikyu20.Validating += new System.ComponentModel.CancelEventHandler(this.txtShikyu_Validating);
            // 
            // txtShikyu19
            // 
            this.txtShikyu19.AutoSizeFromLength = false;
            this.txtShikyu19.DisplayLength = null;
            this.txtShikyu19.Enabled = false;
            this.txtShikyu19.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShikyu19.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtShikyu19.Location = new System.Drawing.Point(631, 92);
            this.txtShikyu19.MaxLength = 10;
            this.txtShikyu19.Name = "txtShikyu19";
            this.txtShikyu19.Size = new System.Drawing.Size(75, 20);
            this.txtShikyu19.TabIndex = 47;
            this.txtShikyu19.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShikyu19.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtShikyu19.Validating += new System.ComponentModel.CancelEventHandler(this.txtShikyu_Validating);
            // 
            // txtShikyu18
            // 
            this.txtShikyu18.AutoSizeFromLength = false;
            this.txtShikyu18.DisplayLength = null;
            this.txtShikyu18.Enabled = false;
            this.txtShikyu18.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShikyu18.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtShikyu18.Location = new System.Drawing.Point(556, 92);
            this.txtShikyu18.MaxLength = 10;
            this.txtShikyu18.Name = "txtShikyu18";
            this.txtShikyu18.Size = new System.Drawing.Size(75, 20);
            this.txtShikyu18.TabIndex = 46;
            this.txtShikyu18.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShikyu18.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtShikyu18.Validating += new System.ComponentModel.CancelEventHandler(this.txtShikyu_Validating);
            // 
            // txtShikyu17
            // 
            this.txtShikyu17.AutoSizeFromLength = false;
            this.txtShikyu17.DisplayLength = null;
            this.txtShikyu17.Enabled = false;
            this.txtShikyu17.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShikyu17.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtShikyu17.Location = new System.Drawing.Point(481, 92);
            this.txtShikyu17.MaxLength = 10;
            this.txtShikyu17.Name = "txtShikyu17";
            this.txtShikyu17.Size = new System.Drawing.Size(75, 20);
            this.txtShikyu17.TabIndex = 45;
            this.txtShikyu17.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShikyu17.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtShikyu17.Validating += new System.ComponentModel.CancelEventHandler(this.txtShikyu_Validating);
            // 
            // txtShikyu16
            // 
            this.txtShikyu16.AutoSizeFromLength = false;
            this.txtShikyu16.DisplayLength = null;
            this.txtShikyu16.Enabled = false;
            this.txtShikyu16.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShikyu16.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtShikyu16.Location = new System.Drawing.Point(406, 92);
            this.txtShikyu16.MaxLength = 10;
            this.txtShikyu16.Name = "txtShikyu16";
            this.txtShikyu16.Size = new System.Drawing.Size(75, 20);
            this.txtShikyu16.TabIndex = 44;
            this.txtShikyu16.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShikyu16.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtShikyu16.Validating += new System.ComponentModel.CancelEventHandler(this.txtShikyu_Validating);
            // 
            // txtShikyu15
            // 
            this.txtShikyu15.AutoSizeFromLength = false;
            this.txtShikyu15.DisplayLength = null;
            this.txtShikyu15.Enabled = false;
            this.txtShikyu15.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShikyu15.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtShikyu15.Location = new System.Drawing.Point(331, 92);
            this.txtShikyu15.MaxLength = 10;
            this.txtShikyu15.Name = "txtShikyu15";
            this.txtShikyu15.Size = new System.Drawing.Size(75, 20);
            this.txtShikyu15.TabIndex = 43;
            this.txtShikyu15.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShikyu15.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtShikyu15.Validating += new System.ComponentModel.CancelEventHandler(this.txtShikyu_Validating);
            // 
            // txtShikyu14
            // 
            this.txtShikyu14.AutoSizeFromLength = false;
            this.txtShikyu14.DisplayLength = null;
            this.txtShikyu14.Enabled = false;
            this.txtShikyu14.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShikyu14.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtShikyu14.Location = new System.Drawing.Point(256, 92);
            this.txtShikyu14.MaxLength = 10;
            this.txtShikyu14.Name = "txtShikyu14";
            this.txtShikyu14.Size = new System.Drawing.Size(75, 20);
            this.txtShikyu14.TabIndex = 42;
            this.txtShikyu14.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShikyu14.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtShikyu14.Validating += new System.ComponentModel.CancelEventHandler(this.txtShikyu_Validating);
            // 
            // txtShikyu13
            // 
            this.txtShikyu13.AutoSizeFromLength = false;
            this.txtShikyu13.DisplayLength = null;
            this.txtShikyu13.Enabled = false;
            this.txtShikyu13.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShikyu13.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtShikyu13.Location = new System.Drawing.Point(181, 92);
            this.txtShikyu13.MaxLength = 10;
            this.txtShikyu13.Name = "txtShikyu13";
            this.txtShikyu13.Size = new System.Drawing.Size(75, 20);
            this.txtShikyu13.TabIndex = 41;
            this.txtShikyu13.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShikyu13.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtShikyu13.Validating += new System.ComponentModel.CancelEventHandler(this.txtShikyu_Validating);
            // 
            // txtShikyu12
            // 
            this.txtShikyu12.AutoSizeFromLength = false;
            this.txtShikyu12.DisplayLength = null;
            this.txtShikyu12.Enabled = false;
            this.txtShikyu12.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShikyu12.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtShikyu12.Location = new System.Drawing.Point(106, 92);
            this.txtShikyu12.MaxLength = 10;
            this.txtShikyu12.Name = "txtShikyu12";
            this.txtShikyu12.Size = new System.Drawing.Size(75, 20);
            this.txtShikyu12.TabIndex = 40;
            this.txtShikyu12.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShikyu12.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtShikyu12.Validating += new System.ComponentModel.CancelEventHandler(this.txtShikyu_Validating);
            // 
            // lblShikyu10
            // 
            this.lblShikyu10.AutoEllipsis = true;
            this.lblShikyu10.BackColor = System.Drawing.Color.Silver;
            this.lblShikyu10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShikyu10.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShikyu10.ForeColor = System.Drawing.Color.White;
            this.lblShikyu10.Location = new System.Drawing.Point(706, 3);
            this.lblShikyu10.Margin = new System.Windows.Forms.Padding(0);
            this.lblShikyu10.Name = "lblShikyu10";
            this.lblShikyu10.Size = new System.Drawing.Size(75, 33);
            this.lblShikyu10.TabIndex = 39;
            this.lblShikyu10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblShikyu9
            // 
            this.lblShikyu9.AutoEllipsis = true;
            this.lblShikyu9.BackColor = System.Drawing.Color.Silver;
            this.lblShikyu9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShikyu9.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShikyu9.ForeColor = System.Drawing.Color.White;
            this.lblShikyu9.Location = new System.Drawing.Point(631, 3);
            this.lblShikyu9.Margin = new System.Windows.Forms.Padding(0);
            this.lblShikyu9.Name = "lblShikyu9";
            this.lblShikyu9.Size = new System.Drawing.Size(75, 33);
            this.lblShikyu9.TabIndex = 38;
            this.lblShikyu9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblShikyu8
            // 
            this.lblShikyu8.AutoEllipsis = true;
            this.lblShikyu8.BackColor = System.Drawing.Color.Silver;
            this.lblShikyu8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShikyu8.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShikyu8.ForeColor = System.Drawing.Color.White;
            this.lblShikyu8.Location = new System.Drawing.Point(556, 3);
            this.lblShikyu8.Margin = new System.Windows.Forms.Padding(0);
            this.lblShikyu8.Name = "lblShikyu8";
            this.lblShikyu8.Size = new System.Drawing.Size(75, 33);
            this.lblShikyu8.TabIndex = 37;
            this.lblShikyu8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblShikyu7
            // 
            this.lblShikyu7.AutoEllipsis = true;
            this.lblShikyu7.BackColor = System.Drawing.Color.Silver;
            this.lblShikyu7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShikyu7.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShikyu7.ForeColor = System.Drawing.Color.White;
            this.lblShikyu7.Location = new System.Drawing.Point(481, 3);
            this.lblShikyu7.Margin = new System.Windows.Forms.Padding(0);
            this.lblShikyu7.Name = "lblShikyu7";
            this.lblShikyu7.Size = new System.Drawing.Size(75, 33);
            this.lblShikyu7.TabIndex = 36;
            this.lblShikyu7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblShikyu6
            // 
            this.lblShikyu6.AutoEllipsis = true;
            this.lblShikyu6.BackColor = System.Drawing.Color.Silver;
            this.lblShikyu6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShikyu6.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShikyu6.ForeColor = System.Drawing.Color.White;
            this.lblShikyu6.Location = new System.Drawing.Point(406, 3);
            this.lblShikyu6.Margin = new System.Windows.Forms.Padding(0);
            this.lblShikyu6.Name = "lblShikyu6";
            this.lblShikyu6.Size = new System.Drawing.Size(75, 33);
            this.lblShikyu6.TabIndex = 35;
            this.lblShikyu6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblShikyu5
            // 
            this.lblShikyu5.AutoEllipsis = true;
            this.lblShikyu5.BackColor = System.Drawing.Color.Silver;
            this.lblShikyu5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShikyu5.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShikyu5.ForeColor = System.Drawing.Color.White;
            this.lblShikyu5.Location = new System.Drawing.Point(331, 3);
            this.lblShikyu5.Margin = new System.Windows.Forms.Padding(0);
            this.lblShikyu5.Name = "lblShikyu5";
            this.lblShikyu5.Size = new System.Drawing.Size(75, 33);
            this.lblShikyu5.TabIndex = 34;
            this.lblShikyu5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblShikyu4
            // 
            this.lblShikyu4.AutoEllipsis = true;
            this.lblShikyu4.BackColor = System.Drawing.Color.Silver;
            this.lblShikyu4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShikyu4.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShikyu4.ForeColor = System.Drawing.Color.White;
            this.lblShikyu4.Location = new System.Drawing.Point(256, 3);
            this.lblShikyu4.Margin = new System.Windows.Forms.Padding(0);
            this.lblShikyu4.Name = "lblShikyu4";
            this.lblShikyu4.Size = new System.Drawing.Size(75, 33);
            this.lblShikyu4.TabIndex = 33;
            this.lblShikyu4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblShikyu3
            // 
            this.lblShikyu3.AutoEllipsis = true;
            this.lblShikyu3.BackColor = System.Drawing.Color.Silver;
            this.lblShikyu3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShikyu3.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShikyu3.ForeColor = System.Drawing.Color.White;
            this.lblShikyu3.Location = new System.Drawing.Point(181, 3);
            this.lblShikyu3.Margin = new System.Windows.Forms.Padding(0);
            this.lblShikyu3.Name = "lblShikyu3";
            this.lblShikyu3.Size = new System.Drawing.Size(75, 33);
            this.lblShikyu3.TabIndex = 32;
            this.lblShikyu3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtShikyu11
            // 
            this.txtShikyu11.AutoSizeFromLength = false;
            this.txtShikyu11.DisplayLength = null;
            this.txtShikyu11.Enabled = false;
            this.txtShikyu11.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShikyu11.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtShikyu11.Location = new System.Drawing.Point(31, 92);
            this.txtShikyu11.MaxLength = 10;
            this.txtShikyu11.Name = "txtShikyu11";
            this.txtShikyu11.Size = new System.Drawing.Size(75, 20);
            this.txtShikyu11.TabIndex = 31;
            this.txtShikyu11.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShikyu11.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtShikyu11.Validating += new System.ComponentModel.CancelEventHandler(this.txtShikyu_Validating);
            // 
            // lblShikyu11
            // 
            this.lblShikyu11.AutoEllipsis = true;
            this.lblShikyu11.BackColor = System.Drawing.Color.Silver;
            this.lblShikyu11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShikyu11.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShikyu11.ForeColor = System.Drawing.Color.White;
            this.lblShikyu11.Location = new System.Drawing.Point(31, 59);
            this.lblShikyu11.Margin = new System.Windows.Forms.Padding(0);
            this.lblShikyu11.Name = "lblShikyu11";
            this.lblShikyu11.Size = new System.Drawing.Size(75, 33);
            this.lblShikyu11.TabIndex = 30;
            this.lblShikyu11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtShikyu1
            // 
            this.txtShikyu1.AutoSizeFromLength = false;
            this.txtShikyu1.DisplayLength = null;
            this.txtShikyu1.Enabled = false;
            this.txtShikyu1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShikyu1.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtShikyu1.Location = new System.Drawing.Point(31, 36);
            this.txtShikyu1.MaxLength = 10;
            this.txtShikyu1.Name = "txtShikyu1";
            this.txtShikyu1.Size = new System.Drawing.Size(75, 20);
            this.txtShikyu1.TabIndex = 0;
            this.txtShikyu1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShikyu1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtShikyu1.Validating += new System.ComponentModel.CancelEventHandler(this.txtShikyu_Validating);
            // 
            // lblShikyu
            // 
            this.lblShikyu.AutoEllipsis = true;
            this.lblShikyu.BackColor = System.Drawing.Color.Silver;
            this.lblShikyu.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblShikyu.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShikyu.Location = new System.Drawing.Point(5, 3);
            this.lblShikyu.Margin = new System.Windows.Forms.Padding(0);
            this.lblShikyu.Name = "lblShikyu";
            this.lblShikyu.Size = new System.Drawing.Size(23, 108);
            this.lblShikyu.TabIndex = 28;
            this.lblShikyu.Text = "支　給";
            this.lblShikyu.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblShikyu2
            // 
            this.lblShikyu2.AutoEllipsis = true;
            this.lblShikyu2.BackColor = System.Drawing.Color.Silver;
            this.lblShikyu2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShikyu2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShikyu2.ForeColor = System.Drawing.Color.White;
            this.lblShikyu2.Location = new System.Drawing.Point(106, 3);
            this.lblShikyu2.Margin = new System.Windows.Forms.Padding(0);
            this.lblShikyu2.Name = "lblShikyu2";
            this.lblShikyu2.Size = new System.Drawing.Size(75, 33);
            this.lblShikyu2.TabIndex = 27;
            this.lblShikyu2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtShikyu10
            // 
            this.txtShikyu10.AutoSizeFromLength = false;
            this.txtShikyu10.DisplayLength = null;
            this.txtShikyu10.Enabled = false;
            this.txtShikyu10.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShikyu10.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtShikyu10.Location = new System.Drawing.Point(706, 36);
            this.txtShikyu10.MaxLength = 10;
            this.txtShikyu10.Name = "txtShikyu10";
            this.txtShikyu10.Size = new System.Drawing.Size(75, 20);
            this.txtShikyu10.TabIndex = 26;
            this.txtShikyu10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShikyu10.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtShikyu10.Validating += new System.ComponentModel.CancelEventHandler(this.txtShikyu_Validating);
            // 
            // txtShikyu9
            // 
            this.txtShikyu9.AutoSizeFromLength = false;
            this.txtShikyu9.DisplayLength = null;
            this.txtShikyu9.Enabled = false;
            this.txtShikyu9.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShikyu9.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtShikyu9.Location = new System.Drawing.Point(631, 36);
            this.txtShikyu9.MaxLength = 10;
            this.txtShikyu9.Name = "txtShikyu9";
            this.txtShikyu9.Size = new System.Drawing.Size(75, 20);
            this.txtShikyu9.TabIndex = 25;
            this.txtShikyu9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShikyu9.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtShikyu9.Validating += new System.ComponentModel.CancelEventHandler(this.txtShikyu_Validating);
            // 
            // txtShikyu8
            // 
            this.txtShikyu8.AutoSizeFromLength = false;
            this.txtShikyu8.DisplayLength = null;
            this.txtShikyu8.Enabled = false;
            this.txtShikyu8.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShikyu8.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtShikyu8.Location = new System.Drawing.Point(556, 36);
            this.txtShikyu8.MaxLength = 10;
            this.txtShikyu8.Name = "txtShikyu8";
            this.txtShikyu8.Size = new System.Drawing.Size(75, 20);
            this.txtShikyu8.TabIndex = 24;
            this.txtShikyu8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShikyu8.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtShikyu8.Validating += new System.ComponentModel.CancelEventHandler(this.txtShikyu_Validating);
            // 
            // txtShikyu7
            // 
            this.txtShikyu7.AutoSizeFromLength = false;
            this.txtShikyu7.DisplayLength = null;
            this.txtShikyu7.Enabled = false;
            this.txtShikyu7.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShikyu7.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtShikyu7.Location = new System.Drawing.Point(481, 36);
            this.txtShikyu7.MaxLength = 10;
            this.txtShikyu7.Name = "txtShikyu7";
            this.txtShikyu7.Size = new System.Drawing.Size(75, 20);
            this.txtShikyu7.TabIndex = 23;
            this.txtShikyu7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShikyu7.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtShikyu7.Validating += new System.ComponentModel.CancelEventHandler(this.txtShikyu_Validating);
            // 
            // txtShikyu6
            // 
            this.txtShikyu6.AutoSizeFromLength = false;
            this.txtShikyu6.DisplayLength = null;
            this.txtShikyu6.Enabled = false;
            this.txtShikyu6.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShikyu6.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtShikyu6.Location = new System.Drawing.Point(406, 36);
            this.txtShikyu6.MaxLength = 10;
            this.txtShikyu6.Name = "txtShikyu6";
            this.txtShikyu6.Size = new System.Drawing.Size(75, 20);
            this.txtShikyu6.TabIndex = 22;
            this.txtShikyu6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShikyu6.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtShikyu6.Validating += new System.ComponentModel.CancelEventHandler(this.txtShikyu_Validating);
            // 
            // txtShikyu5
            // 
            this.txtShikyu5.AutoSizeFromLength = false;
            this.txtShikyu5.DisplayLength = null;
            this.txtShikyu5.Enabled = false;
            this.txtShikyu5.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShikyu5.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtShikyu5.Location = new System.Drawing.Point(331, 36);
            this.txtShikyu5.MaxLength = 10;
            this.txtShikyu5.Name = "txtShikyu5";
            this.txtShikyu5.Size = new System.Drawing.Size(75, 20);
            this.txtShikyu5.TabIndex = 21;
            this.txtShikyu5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShikyu5.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtShikyu5.Validating += new System.ComponentModel.CancelEventHandler(this.txtShikyu_Validating);
            // 
            // txtShikyu4
            // 
            this.txtShikyu4.AutoSizeFromLength = false;
            this.txtShikyu4.DisplayLength = null;
            this.txtShikyu4.Enabled = false;
            this.txtShikyu4.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShikyu4.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtShikyu4.Location = new System.Drawing.Point(256, 36);
            this.txtShikyu4.MaxLength = 10;
            this.txtShikyu4.Name = "txtShikyu4";
            this.txtShikyu4.Size = new System.Drawing.Size(75, 20);
            this.txtShikyu4.TabIndex = 20;
            this.txtShikyu4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShikyu4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtShikyu4.Validating += new System.ComponentModel.CancelEventHandler(this.txtShikyu_Validating);
            // 
            // txtShikyu3
            // 
            this.txtShikyu3.AutoSizeFromLength = false;
            this.txtShikyu3.DisplayLength = null;
            this.txtShikyu3.Enabled = false;
            this.txtShikyu3.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShikyu3.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtShikyu3.Location = new System.Drawing.Point(181, 36);
            this.txtShikyu3.MaxLength = 10;
            this.txtShikyu3.Name = "txtShikyu3";
            this.txtShikyu3.Size = new System.Drawing.Size(75, 20);
            this.txtShikyu3.TabIndex = 19;
            this.txtShikyu3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShikyu3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtShikyu3.Validating += new System.ComponentModel.CancelEventHandler(this.txtShikyu_Validating);
            // 
            // txtShikyu2
            // 
            this.txtShikyu2.AutoSizeFromLength = false;
            this.txtShikyu2.DisplayLength = null;
            this.txtShikyu2.Enabled = false;
            this.txtShikyu2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShikyu2.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtShikyu2.Location = new System.Drawing.Point(106, 36);
            this.txtShikyu2.MaxLength = 10;
            this.txtShikyu2.Name = "txtShikyu2";
            this.txtShikyu2.Size = new System.Drawing.Size(75, 20);
            this.txtShikyu2.TabIndex = 18;
            this.txtShikyu2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShikyu2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtShikyu2.Validating += new System.ComponentModel.CancelEventHandler(this.txtShikyu_Validating);
            // 
            // lblShikyu1
            // 
            this.lblShikyu1.AutoEllipsis = true;
            this.lblShikyu1.BackColor = System.Drawing.Color.Silver;
            this.lblShikyu1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShikyu1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShikyu1.ForeColor = System.Drawing.Color.White;
            this.lblShikyu1.Location = new System.Drawing.Point(31, 3);
            this.lblShikyu1.Margin = new System.Windows.Forms.Padding(0);
            this.lblShikyu1.Name = "lblShikyu1";
            this.lblShikyu1.Size = new System.Drawing.Size(75, 33);
            this.lblShikyu1.TabIndex = 6;
            this.lblShikyu1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlKojo
            // 
            this.pnlKojo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlKojo.Controls.Add(this.lblKojo20);
            this.pnlKojo.Controls.Add(this.lblKojo19);
            this.pnlKojo.Controls.Add(this.lblKojo18);
            this.pnlKojo.Controls.Add(this.lblKojo17);
            this.pnlKojo.Controls.Add(this.lblKojo16);
            this.pnlKojo.Controls.Add(this.lblKojo15);
            this.pnlKojo.Controls.Add(this.lblKojo14);
            this.pnlKojo.Controls.Add(this.lblKojo13);
            this.pnlKojo.Controls.Add(this.lblKojo12);
            this.pnlKojo.Controls.Add(this.txtKojo20);
            this.pnlKojo.Controls.Add(this.txtKojo19);
            this.pnlKojo.Controls.Add(this.txtKojo18);
            this.pnlKojo.Controls.Add(this.txtKojo17);
            this.pnlKojo.Controls.Add(this.txtKojo16);
            this.pnlKojo.Controls.Add(this.txtKojo15);
            this.pnlKojo.Controls.Add(this.txtKojo14);
            this.pnlKojo.Controls.Add(this.txtKojo13);
            this.pnlKojo.Controls.Add(this.txtKojo12);
            this.pnlKojo.Controls.Add(this.lblKojo10);
            this.pnlKojo.Controls.Add(this.lblKojo9);
            this.pnlKojo.Controls.Add(this.lblKojo8);
            this.pnlKojo.Controls.Add(this.lblKojo7);
            this.pnlKojo.Controls.Add(this.lblKojo6);
            this.pnlKojo.Controls.Add(this.lblKojo5);
            this.pnlKojo.Controls.Add(this.lblKojo4);
            this.pnlKojo.Controls.Add(this.lblKojo3);
            this.pnlKojo.Controls.Add(this.txtKojo11);
            this.pnlKojo.Controls.Add(this.lblKojo11);
            this.pnlKojo.Controls.Add(this.txtKojo1);
            this.pnlKojo.Controls.Add(this.lblKojo);
            this.pnlKojo.Controls.Add(this.lblKojo2);
            this.pnlKojo.Controls.Add(this.txtKojo10);
            this.pnlKojo.Controls.Add(this.txtKojo9);
            this.pnlKojo.Controls.Add(this.txtKojo8);
            this.pnlKojo.Controls.Add(this.txtKojo7);
            this.pnlKojo.Controls.Add(this.txtKojo6);
            this.pnlKojo.Controls.Add(this.txtKojo5);
            this.pnlKojo.Controls.Add(this.txtKojo4);
            this.pnlKojo.Controls.Add(this.txtKojo3);
            this.pnlKojo.Controls.Add(this.txtKojo2);
            this.pnlKojo.Controls.Add(this.lblKojo1);
            this.pnlKojo.Location = new System.Drawing.Point(17, 348);
            this.pnlKojo.Name = "pnlKojo";
            this.pnlKojo.Size = new System.Drawing.Size(789, 116);
            this.pnlKojo.TabIndex = 6;
            // 
            // lblKojo20
            // 
            this.lblKojo20.AutoEllipsis = true;
            this.lblKojo20.BackColor = System.Drawing.Color.Silver;
            this.lblKojo20.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKojo20.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKojo20.ForeColor = System.Drawing.Color.White;
            this.lblKojo20.Location = new System.Drawing.Point(706, 59);
            this.lblKojo20.Margin = new System.Windows.Forms.Padding(0);
            this.lblKojo20.Name = "lblKojo20";
            this.lblKojo20.Size = new System.Drawing.Size(75, 33);
            this.lblKojo20.TabIndex = 57;
            this.lblKojo20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblKojo19
            // 
            this.lblKojo19.AutoEllipsis = true;
            this.lblKojo19.BackColor = System.Drawing.Color.Silver;
            this.lblKojo19.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKojo19.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKojo19.ForeColor = System.Drawing.Color.White;
            this.lblKojo19.Location = new System.Drawing.Point(631, 59);
            this.lblKojo19.Margin = new System.Windows.Forms.Padding(0);
            this.lblKojo19.Name = "lblKojo19";
            this.lblKojo19.Size = new System.Drawing.Size(75, 33);
            this.lblKojo19.TabIndex = 56;
            this.lblKojo19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblKojo18
            // 
            this.lblKojo18.AutoEllipsis = true;
            this.lblKojo18.BackColor = System.Drawing.Color.Silver;
            this.lblKojo18.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKojo18.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKojo18.ForeColor = System.Drawing.Color.White;
            this.lblKojo18.Location = new System.Drawing.Point(556, 59);
            this.lblKojo18.Margin = new System.Windows.Forms.Padding(0);
            this.lblKojo18.Name = "lblKojo18";
            this.lblKojo18.Size = new System.Drawing.Size(75, 33);
            this.lblKojo18.TabIndex = 55;
            this.lblKojo18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblKojo17
            // 
            this.lblKojo17.AutoEllipsis = true;
            this.lblKojo17.BackColor = System.Drawing.Color.Silver;
            this.lblKojo17.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKojo17.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKojo17.ForeColor = System.Drawing.Color.White;
            this.lblKojo17.Location = new System.Drawing.Point(481, 59);
            this.lblKojo17.Margin = new System.Windows.Forms.Padding(0);
            this.lblKojo17.Name = "lblKojo17";
            this.lblKojo17.Size = new System.Drawing.Size(75, 33);
            this.lblKojo17.TabIndex = 54;
            this.lblKojo17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblKojo16
            // 
            this.lblKojo16.AutoEllipsis = true;
            this.lblKojo16.BackColor = System.Drawing.Color.Silver;
            this.lblKojo16.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKojo16.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKojo16.ForeColor = System.Drawing.Color.White;
            this.lblKojo16.Location = new System.Drawing.Point(406, 59);
            this.lblKojo16.Margin = new System.Windows.Forms.Padding(0);
            this.lblKojo16.Name = "lblKojo16";
            this.lblKojo16.Size = new System.Drawing.Size(75, 33);
            this.lblKojo16.TabIndex = 53;
            this.lblKojo16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblKojo15
            // 
            this.lblKojo15.AutoEllipsis = true;
            this.lblKojo15.BackColor = System.Drawing.Color.Silver;
            this.lblKojo15.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKojo15.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKojo15.ForeColor = System.Drawing.Color.White;
            this.lblKojo15.Location = new System.Drawing.Point(331, 59);
            this.lblKojo15.Margin = new System.Windows.Forms.Padding(0);
            this.lblKojo15.Name = "lblKojo15";
            this.lblKojo15.Size = new System.Drawing.Size(75, 33);
            this.lblKojo15.TabIndex = 52;
            this.lblKojo15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblKojo14
            // 
            this.lblKojo14.AutoEllipsis = true;
            this.lblKojo14.BackColor = System.Drawing.Color.Silver;
            this.lblKojo14.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKojo14.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKojo14.ForeColor = System.Drawing.Color.White;
            this.lblKojo14.Location = new System.Drawing.Point(256, 59);
            this.lblKojo14.Margin = new System.Windows.Forms.Padding(0);
            this.lblKojo14.Name = "lblKojo14";
            this.lblKojo14.Size = new System.Drawing.Size(75, 33);
            this.lblKojo14.TabIndex = 51;
            this.lblKojo14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblKojo13
            // 
            this.lblKojo13.AutoEllipsis = true;
            this.lblKojo13.BackColor = System.Drawing.Color.Silver;
            this.lblKojo13.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKojo13.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKojo13.ForeColor = System.Drawing.Color.White;
            this.lblKojo13.Location = new System.Drawing.Point(181, 59);
            this.lblKojo13.Margin = new System.Windows.Forms.Padding(0);
            this.lblKojo13.Name = "lblKojo13";
            this.lblKojo13.Size = new System.Drawing.Size(75, 33);
            this.lblKojo13.TabIndex = 50;
            this.lblKojo13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblKojo12
            // 
            this.lblKojo12.AutoEllipsis = true;
            this.lblKojo12.BackColor = System.Drawing.Color.Silver;
            this.lblKojo12.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKojo12.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKojo12.ForeColor = System.Drawing.Color.White;
            this.lblKojo12.Location = new System.Drawing.Point(106, 59);
            this.lblKojo12.Margin = new System.Windows.Forms.Padding(0);
            this.lblKojo12.Name = "lblKojo12";
            this.lblKojo12.Size = new System.Drawing.Size(75, 33);
            this.lblKojo12.TabIndex = 49;
            this.lblKojo12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtKojo20
            // 
            this.txtKojo20.AutoSizeFromLength = false;
            this.txtKojo20.DisplayLength = null;
            this.txtKojo20.Enabled = false;
            this.txtKojo20.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKojo20.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKojo20.Location = new System.Drawing.Point(706, 92);
            this.txtKojo20.MaxLength = 10;
            this.txtKojo20.Name = "txtKojo20";
            this.txtKojo20.Size = new System.Drawing.Size(75, 20);
            this.txtKojo20.TabIndex = 48;
            this.txtKojo20.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKojo20.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtKojo20.Validating += new System.ComponentModel.CancelEventHandler(this.txtKojo_Validating);
            // 
            // txtKojo19
            // 
            this.txtKojo19.AutoSizeFromLength = false;
            this.txtKojo19.DisplayLength = null;
            this.txtKojo19.Enabled = false;
            this.txtKojo19.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKojo19.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKojo19.Location = new System.Drawing.Point(631, 92);
            this.txtKojo19.MaxLength = 10;
            this.txtKojo19.Name = "txtKojo19";
            this.txtKojo19.Size = new System.Drawing.Size(75, 20);
            this.txtKojo19.TabIndex = 47;
            this.txtKojo19.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKojo19.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtKojo19.Validating += new System.ComponentModel.CancelEventHandler(this.txtKojo_Validating);
            // 
            // txtKojo18
            // 
            this.txtKojo18.AutoSizeFromLength = false;
            this.txtKojo18.DisplayLength = null;
            this.txtKojo18.Enabled = false;
            this.txtKojo18.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKojo18.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKojo18.Location = new System.Drawing.Point(556, 92);
            this.txtKojo18.MaxLength = 10;
            this.txtKojo18.Name = "txtKojo18";
            this.txtKojo18.Size = new System.Drawing.Size(75, 20);
            this.txtKojo18.TabIndex = 46;
            this.txtKojo18.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKojo18.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtKojo18.Validating += new System.ComponentModel.CancelEventHandler(this.txtKojo_Validating);
            // 
            // txtKojo17
            // 
            this.txtKojo17.AutoSizeFromLength = false;
            this.txtKojo17.DisplayLength = null;
            this.txtKojo17.Enabled = false;
            this.txtKojo17.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKojo17.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKojo17.Location = new System.Drawing.Point(481, 92);
            this.txtKojo17.MaxLength = 10;
            this.txtKojo17.Name = "txtKojo17";
            this.txtKojo17.Size = new System.Drawing.Size(75, 20);
            this.txtKojo17.TabIndex = 45;
            this.txtKojo17.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKojo17.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtKojo17.Validating += new System.ComponentModel.CancelEventHandler(this.txtKojo_Validating);
            // 
            // txtKojo16
            // 
            this.txtKojo16.AutoSizeFromLength = false;
            this.txtKojo16.DisplayLength = null;
            this.txtKojo16.Enabled = false;
            this.txtKojo16.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKojo16.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKojo16.Location = new System.Drawing.Point(406, 92);
            this.txtKojo16.MaxLength = 10;
            this.txtKojo16.Name = "txtKojo16";
            this.txtKojo16.Size = new System.Drawing.Size(75, 20);
            this.txtKojo16.TabIndex = 44;
            this.txtKojo16.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKojo16.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtKojo16.Validating += new System.ComponentModel.CancelEventHandler(this.txtKojo_Validating);
            // 
            // txtKojo15
            // 
            this.txtKojo15.AutoSizeFromLength = false;
            this.txtKojo15.DisplayLength = null;
            this.txtKojo15.Enabled = false;
            this.txtKojo15.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKojo15.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKojo15.Location = new System.Drawing.Point(331, 92);
            this.txtKojo15.MaxLength = 10;
            this.txtKojo15.Name = "txtKojo15";
            this.txtKojo15.Size = new System.Drawing.Size(75, 20);
            this.txtKojo15.TabIndex = 43;
            this.txtKojo15.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKojo15.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtKojo15.Validating += new System.ComponentModel.CancelEventHandler(this.txtKojo_Validating);
            // 
            // txtKojo14
            // 
            this.txtKojo14.AutoSizeFromLength = false;
            this.txtKojo14.DisplayLength = null;
            this.txtKojo14.Enabled = false;
            this.txtKojo14.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKojo14.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKojo14.Location = new System.Drawing.Point(256, 92);
            this.txtKojo14.MaxLength = 10;
            this.txtKojo14.Name = "txtKojo14";
            this.txtKojo14.Size = new System.Drawing.Size(75, 20);
            this.txtKojo14.TabIndex = 42;
            this.txtKojo14.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKojo14.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtKojo14.Validating += new System.ComponentModel.CancelEventHandler(this.txtKojo_Validating);
            // 
            // txtKojo13
            // 
            this.txtKojo13.AutoSizeFromLength = false;
            this.txtKojo13.DisplayLength = null;
            this.txtKojo13.Enabled = false;
            this.txtKojo13.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKojo13.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKojo13.Location = new System.Drawing.Point(181, 92);
            this.txtKojo13.MaxLength = 10;
            this.txtKojo13.Name = "txtKojo13";
            this.txtKojo13.Size = new System.Drawing.Size(75, 20);
            this.txtKojo13.TabIndex = 41;
            this.txtKojo13.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKojo13.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtKojo13.Validating += new System.ComponentModel.CancelEventHandler(this.txtKojo_Validating);
            // 
            // txtKojo12
            // 
            this.txtKojo12.AutoSizeFromLength = false;
            this.txtKojo12.DisplayLength = null;
            this.txtKojo12.Enabled = false;
            this.txtKojo12.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKojo12.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKojo12.Location = new System.Drawing.Point(106, 92);
            this.txtKojo12.MaxLength = 10;
            this.txtKojo12.Name = "txtKojo12";
            this.txtKojo12.Size = new System.Drawing.Size(75, 20);
            this.txtKojo12.TabIndex = 40;
            this.txtKojo12.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKojo12.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtKojo12.Validating += new System.ComponentModel.CancelEventHandler(this.txtKojo_Validating);
            // 
            // lblKojo10
            // 
            this.lblKojo10.AutoEllipsis = true;
            this.lblKojo10.BackColor = System.Drawing.Color.Silver;
            this.lblKojo10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKojo10.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKojo10.ForeColor = System.Drawing.Color.White;
            this.lblKojo10.Location = new System.Drawing.Point(706, 3);
            this.lblKojo10.Margin = new System.Windows.Forms.Padding(0);
            this.lblKojo10.Name = "lblKojo10";
            this.lblKojo10.Size = new System.Drawing.Size(75, 33);
            this.lblKojo10.TabIndex = 39;
            this.lblKojo10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblKojo9
            // 
            this.lblKojo9.AutoEllipsis = true;
            this.lblKojo9.BackColor = System.Drawing.Color.Silver;
            this.lblKojo9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKojo9.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKojo9.ForeColor = System.Drawing.Color.White;
            this.lblKojo9.Location = new System.Drawing.Point(631, 3);
            this.lblKojo9.Margin = new System.Windows.Forms.Padding(0);
            this.lblKojo9.Name = "lblKojo9";
            this.lblKojo9.Size = new System.Drawing.Size(75, 33);
            this.lblKojo9.TabIndex = 38;
            this.lblKojo9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblKojo8
            // 
            this.lblKojo8.AutoEllipsis = true;
            this.lblKojo8.BackColor = System.Drawing.Color.Silver;
            this.lblKojo8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKojo8.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKojo8.ForeColor = System.Drawing.Color.White;
            this.lblKojo8.Location = new System.Drawing.Point(556, 3);
            this.lblKojo8.Margin = new System.Windows.Forms.Padding(0);
            this.lblKojo8.Name = "lblKojo8";
            this.lblKojo8.Size = new System.Drawing.Size(75, 33);
            this.lblKojo8.TabIndex = 37;
            this.lblKojo8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblKojo7
            // 
            this.lblKojo7.AutoEllipsis = true;
            this.lblKojo7.BackColor = System.Drawing.Color.Silver;
            this.lblKojo7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKojo7.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKojo7.ForeColor = System.Drawing.Color.White;
            this.lblKojo7.Location = new System.Drawing.Point(481, 3);
            this.lblKojo7.Margin = new System.Windows.Forms.Padding(0);
            this.lblKojo7.Name = "lblKojo7";
            this.lblKojo7.Size = new System.Drawing.Size(75, 33);
            this.lblKojo7.TabIndex = 36;
            this.lblKojo7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblKojo6
            // 
            this.lblKojo6.AutoEllipsis = true;
            this.lblKojo6.BackColor = System.Drawing.Color.Silver;
            this.lblKojo6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKojo6.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKojo6.ForeColor = System.Drawing.Color.White;
            this.lblKojo6.Location = new System.Drawing.Point(406, 3);
            this.lblKojo6.Margin = new System.Windows.Forms.Padding(0);
            this.lblKojo6.Name = "lblKojo6";
            this.lblKojo6.Size = new System.Drawing.Size(75, 33);
            this.lblKojo6.TabIndex = 35;
            this.lblKojo6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblKojo5
            // 
            this.lblKojo5.AutoEllipsis = true;
            this.lblKojo5.BackColor = System.Drawing.Color.Silver;
            this.lblKojo5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKojo5.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKojo5.ForeColor = System.Drawing.Color.White;
            this.lblKojo5.Location = new System.Drawing.Point(331, 3);
            this.lblKojo5.Margin = new System.Windows.Forms.Padding(0);
            this.lblKojo5.Name = "lblKojo5";
            this.lblKojo5.Size = new System.Drawing.Size(75, 33);
            this.lblKojo5.TabIndex = 34;
            this.lblKojo5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblKojo4
            // 
            this.lblKojo4.AutoEllipsis = true;
            this.lblKojo4.BackColor = System.Drawing.Color.Silver;
            this.lblKojo4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKojo4.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKojo4.ForeColor = System.Drawing.Color.White;
            this.lblKojo4.Location = new System.Drawing.Point(256, 3);
            this.lblKojo4.Margin = new System.Windows.Forms.Padding(0);
            this.lblKojo4.Name = "lblKojo4";
            this.lblKojo4.Size = new System.Drawing.Size(75, 33);
            this.lblKojo4.TabIndex = 33;
            this.lblKojo4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblKojo3
            // 
            this.lblKojo3.AutoEllipsis = true;
            this.lblKojo3.BackColor = System.Drawing.Color.Silver;
            this.lblKojo3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKojo3.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKojo3.ForeColor = System.Drawing.Color.White;
            this.lblKojo3.Location = new System.Drawing.Point(181, 3);
            this.lblKojo3.Margin = new System.Windows.Forms.Padding(0);
            this.lblKojo3.Name = "lblKojo3";
            this.lblKojo3.Size = new System.Drawing.Size(75, 33);
            this.lblKojo3.TabIndex = 32;
            this.lblKojo3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtKojo11
            // 
            this.txtKojo11.AutoSizeFromLength = false;
            this.txtKojo11.DisplayLength = null;
            this.txtKojo11.Enabled = false;
            this.txtKojo11.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKojo11.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKojo11.Location = new System.Drawing.Point(31, 92);
            this.txtKojo11.MaxLength = 10;
            this.txtKojo11.Name = "txtKojo11";
            this.txtKojo11.Size = new System.Drawing.Size(75, 20);
            this.txtKojo11.TabIndex = 31;
            this.txtKojo11.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKojo11.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtKojo11.Validating += new System.ComponentModel.CancelEventHandler(this.txtKojo_Validating);
            // 
            // lblKojo11
            // 
            this.lblKojo11.AutoEllipsis = true;
            this.lblKojo11.BackColor = System.Drawing.Color.Silver;
            this.lblKojo11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKojo11.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKojo11.ForeColor = System.Drawing.Color.White;
            this.lblKojo11.Location = new System.Drawing.Point(31, 59);
            this.lblKojo11.Margin = new System.Windows.Forms.Padding(0);
            this.lblKojo11.Name = "lblKojo11";
            this.lblKojo11.Size = new System.Drawing.Size(75, 33);
            this.lblKojo11.TabIndex = 30;
            this.lblKojo11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtKojo1
            // 
            this.txtKojo1.AutoSizeFromLength = false;
            this.txtKojo1.DisplayLength = null;
            this.txtKojo1.Enabled = false;
            this.txtKojo1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKojo1.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKojo1.Location = new System.Drawing.Point(31, 36);
            this.txtKojo1.MaxLength = 10;
            this.txtKojo1.Name = "txtKojo1";
            this.txtKojo1.Size = new System.Drawing.Size(75, 20);
            this.txtKojo1.TabIndex = 1;
            this.txtKojo1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKojo1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtKojo1.Validating += new System.ComponentModel.CancelEventHandler(this.txtKojo_Validating);
            // 
            // lblKojo
            // 
            this.lblKojo.AutoEllipsis = true;
            this.lblKojo.BackColor = System.Drawing.Color.Silver;
            this.lblKojo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblKojo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKojo.Location = new System.Drawing.Point(5, 3);
            this.lblKojo.Margin = new System.Windows.Forms.Padding(0);
            this.lblKojo.Name = "lblKojo";
            this.lblKojo.Size = new System.Drawing.Size(23, 108);
            this.lblKojo.TabIndex = 28;
            this.lblKojo.Text = "控　除";
            this.lblKojo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblKojo2
            // 
            this.lblKojo2.AutoEllipsis = true;
            this.lblKojo2.BackColor = System.Drawing.Color.Silver;
            this.lblKojo2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKojo2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKojo2.ForeColor = System.Drawing.Color.White;
            this.lblKojo2.Location = new System.Drawing.Point(106, 3);
            this.lblKojo2.Margin = new System.Windows.Forms.Padding(0);
            this.lblKojo2.Name = "lblKojo2";
            this.lblKojo2.Size = new System.Drawing.Size(75, 33);
            this.lblKojo2.TabIndex = 27;
            this.lblKojo2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtKojo10
            // 
            this.txtKojo10.AutoSizeFromLength = false;
            this.txtKojo10.DisplayLength = null;
            this.txtKojo10.Enabled = false;
            this.txtKojo10.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKojo10.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKojo10.Location = new System.Drawing.Point(706, 36);
            this.txtKojo10.MaxLength = 10;
            this.txtKojo10.Name = "txtKojo10";
            this.txtKojo10.Size = new System.Drawing.Size(75, 20);
            this.txtKojo10.TabIndex = 26;
            this.txtKojo10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKojo10.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtKojo10.Validating += new System.ComponentModel.CancelEventHandler(this.txtKojo_Validating);
            // 
            // txtKojo9
            // 
            this.txtKojo9.AutoSizeFromLength = false;
            this.txtKojo9.DisplayLength = null;
            this.txtKojo9.Enabled = false;
            this.txtKojo9.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKojo9.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKojo9.Location = new System.Drawing.Point(631, 36);
            this.txtKojo9.MaxLength = 10;
            this.txtKojo9.Name = "txtKojo9";
            this.txtKojo9.Size = new System.Drawing.Size(75, 20);
            this.txtKojo9.TabIndex = 25;
            this.txtKojo9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKojo9.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtKojo9.Validating += new System.ComponentModel.CancelEventHandler(this.txtKojo_Validating);
            // 
            // txtKojo8
            // 
            this.txtKojo8.AutoSizeFromLength = false;
            this.txtKojo8.DisplayLength = null;
            this.txtKojo8.Enabled = false;
            this.txtKojo8.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKojo8.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKojo8.Location = new System.Drawing.Point(556, 36);
            this.txtKojo8.MaxLength = 10;
            this.txtKojo8.Name = "txtKojo8";
            this.txtKojo8.Size = new System.Drawing.Size(75, 20);
            this.txtKojo8.TabIndex = 24;
            this.txtKojo8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKojo8.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtKojo8.Validating += new System.ComponentModel.CancelEventHandler(this.txtKojo_Validating);
            // 
            // txtKojo7
            // 
            this.txtKojo7.AutoSizeFromLength = false;
            this.txtKojo7.DisplayLength = null;
            this.txtKojo7.Enabled = false;
            this.txtKojo7.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKojo7.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKojo7.Location = new System.Drawing.Point(481, 36);
            this.txtKojo7.MaxLength = 10;
            this.txtKojo7.Name = "txtKojo7";
            this.txtKojo7.Size = new System.Drawing.Size(75, 20);
            this.txtKojo7.TabIndex = 23;
            this.txtKojo7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKojo7.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtKojo7.Validating += new System.ComponentModel.CancelEventHandler(this.txtKojo_Validating);
            // 
            // txtKojo6
            // 
            this.txtKojo6.AutoSizeFromLength = false;
            this.txtKojo6.DisplayLength = null;
            this.txtKojo6.Enabled = false;
            this.txtKojo6.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKojo6.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKojo6.Location = new System.Drawing.Point(406, 36);
            this.txtKojo6.MaxLength = 10;
            this.txtKojo6.Name = "txtKojo6";
            this.txtKojo6.Size = new System.Drawing.Size(75, 20);
            this.txtKojo6.TabIndex = 22;
            this.txtKojo6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKojo6.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtKojo6.Validating += new System.ComponentModel.CancelEventHandler(this.txtKojo_Validating);
            // 
            // txtKojo5
            // 
            this.txtKojo5.AutoSizeFromLength = false;
            this.txtKojo5.DisplayLength = null;
            this.txtKojo5.Enabled = false;
            this.txtKojo5.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKojo5.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKojo5.Location = new System.Drawing.Point(331, 36);
            this.txtKojo5.MaxLength = 10;
            this.txtKojo5.Name = "txtKojo5";
            this.txtKojo5.Size = new System.Drawing.Size(75, 20);
            this.txtKojo5.TabIndex = 21;
            this.txtKojo5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKojo5.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtKojo5.Validating += new System.ComponentModel.CancelEventHandler(this.txtKojo_Validating);
            // 
            // txtKojo4
            // 
            this.txtKojo4.AutoSizeFromLength = false;
            this.txtKojo4.DisplayLength = null;
            this.txtKojo4.Enabled = false;
            this.txtKojo4.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKojo4.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKojo4.Location = new System.Drawing.Point(256, 36);
            this.txtKojo4.MaxLength = 10;
            this.txtKojo4.Name = "txtKojo4";
            this.txtKojo4.Size = new System.Drawing.Size(75, 20);
            this.txtKojo4.TabIndex = 20;
            this.txtKojo4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKojo4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtKojo4.Validating += new System.ComponentModel.CancelEventHandler(this.txtKojo_Validating);
            // 
            // txtKojo3
            // 
            this.txtKojo3.AutoSizeFromLength = false;
            this.txtKojo3.DisplayLength = null;
            this.txtKojo3.Enabled = false;
            this.txtKojo3.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKojo3.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKojo3.Location = new System.Drawing.Point(181, 36);
            this.txtKojo3.MaxLength = 10;
            this.txtKojo3.Name = "txtKojo3";
            this.txtKojo3.Size = new System.Drawing.Size(75, 20);
            this.txtKojo3.TabIndex = 19;
            this.txtKojo3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKojo3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtKojo3.Validating += new System.ComponentModel.CancelEventHandler(this.txtKojo_Validating);
            // 
            // txtKojo2
            // 
            this.txtKojo2.AutoSizeFromLength = false;
            this.txtKojo2.DisplayLength = null;
            this.txtKojo2.Enabled = false;
            this.txtKojo2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKojo2.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKojo2.Location = new System.Drawing.Point(106, 36);
            this.txtKojo2.MaxLength = 10;
            this.txtKojo2.Name = "txtKojo2";
            this.txtKojo2.Size = new System.Drawing.Size(75, 20);
            this.txtKojo2.TabIndex = 18;
            this.txtKojo2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKojo2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtKojo2.Validating += new System.ComponentModel.CancelEventHandler(this.txtKojo_Validating);
            // 
            // lblKojo1
            // 
            this.lblKojo1.AutoEllipsis = true;
            this.lblKojo1.BackColor = System.Drawing.Color.Silver;
            this.lblKojo1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKojo1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKojo1.ForeColor = System.Drawing.Color.White;
            this.lblKojo1.Location = new System.Drawing.Point(31, 3);
            this.lblKojo1.Margin = new System.Windows.Forms.Padding(0);
            this.lblKojo1.Name = "lblKojo1";
            this.lblKojo1.Size = new System.Drawing.Size(75, 33);
            this.lblKojo1.TabIndex = 6;
            this.lblKojo1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlShikyuInfo
            // 
            this.pnlShikyuInfo.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pnlShikyuInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlShikyuInfo.Controls.Add(this.grpShikyubi);
            this.pnlShikyuInfo.Controls.Add(this.grpNengetsu);
            this.pnlShikyuInfo.Location = new System.Drawing.Point(473, 47);
            this.pnlShikyuInfo.Name = "pnlShikyuInfo";
            this.pnlShikyuInfo.Size = new System.Drawing.Size(300, 54);
            this.pnlShikyuInfo.TabIndex = 3;
            // 
            // grpShikyubi
            // 
            this.grpShikyubi.BackColor = System.Drawing.Color.Silver;
            this.grpShikyubi.Controls.Add(this.lblDayShikyubi);
            this.grpShikyubi.Controls.Add(this.lblMonthShikyubi);
            this.grpShikyubi.Controls.Add(this.lblGengoYearShikyubi);
            this.grpShikyubi.Controls.Add(this.lblShikyubi);
            this.grpShikyubi.Controls.Add(this.lblGengoShikyubi);
            this.grpShikyubi.Font = new System.Drawing.Font("ＭＳ ゴシック", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.grpShikyubi.Location = new System.Drawing.Point(128, -4);
            this.grpShikyubi.Name = "grpShikyubi";
            this.grpShikyubi.Size = new System.Drawing.Size(164, 52);
            this.grpShikyubi.TabIndex = 1;
            this.grpShikyubi.TabStop = false;
            // 
            // lblDayShikyubi
            // 
            this.lblDayShikyubi.BackColor = System.Drawing.Color.White;
            this.lblDayShikyubi.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDayShikyubi.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDayShikyubi.ForeColor = System.Drawing.Color.Silver;
            this.lblDayShikyubi.Location = new System.Drawing.Point(124, 28);
            this.lblDayShikyubi.Name = "lblDayShikyubi";
            this.lblDayShikyubi.Size = new System.Drawing.Size(34, 20);
            this.lblDayShikyubi.TabIndex = 10;
            this.lblDayShikyubi.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMonthShikyubi
            // 
            this.lblMonthShikyubi.BackColor = System.Drawing.Color.White;
            this.lblMonthShikyubi.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMonthShikyubi.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMonthShikyubi.ForeColor = System.Drawing.Color.Silver;
            this.lblMonthShikyubi.Location = new System.Drawing.Point(84, 28);
            this.lblMonthShikyubi.Name = "lblMonthShikyubi";
            this.lblMonthShikyubi.Size = new System.Drawing.Size(34, 20);
            this.lblMonthShikyubi.TabIndex = 9;
            this.lblMonthShikyubi.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblGengoYearShikyubi
            // 
            this.lblGengoYearShikyubi.BackColor = System.Drawing.Color.White;
            this.lblGengoYearShikyubi.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengoYearShikyubi.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengoYearShikyubi.ForeColor = System.Drawing.Color.Silver;
            this.lblGengoYearShikyubi.Location = new System.Drawing.Point(44, 28);
            this.lblGengoYearShikyubi.Name = "lblGengoYearShikyubi";
            this.lblGengoYearShikyubi.Size = new System.Drawing.Size(34, 20);
            this.lblGengoYearShikyubi.TabIndex = 8;
            this.lblGengoYearShikyubi.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblShikyubi
            // 
            this.lblShikyubi.BackColor = System.Drawing.Color.Silver;
            this.lblShikyubi.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShikyubi.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShikyubi.Location = new System.Drawing.Point(4, 10);
            this.lblShikyubi.Name = "lblShikyubi";
            this.lblShikyubi.Size = new System.Drawing.Size(155, 16);
            this.lblShikyubi.TabIndex = 5;
            this.lblShikyubi.Text = "支　給　日";
            this.lblShikyubi.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblGengoShikyubi
            // 
            this.lblGengoShikyubi.BackColor = System.Drawing.Color.Silver;
            this.lblGengoShikyubi.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengoShikyubi.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengoShikyubi.Location = new System.Drawing.Point(4, 28);
            this.lblGengoShikyubi.Name = "lblGengoShikyubi";
            this.lblGengoShikyubi.Size = new System.Drawing.Size(40, 20);
            this.lblGengoShikyubi.TabIndex = 0;
            this.lblGengoShikyubi.Text = "平成";
            this.lblGengoShikyubi.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // grpNengetsu
            // 
            this.grpNengetsu.BackColor = System.Drawing.Color.Silver;
            this.grpNengetsu.Controls.Add(this.lblMonthNengetsu);
            this.grpNengetsu.Controls.Add(this.lblGengoYearNengetsu);
            this.grpNengetsu.Controls.Add(this.lblNengetsu);
            this.grpNengetsu.Controls.Add(this.lblGengoNengetsu);
            this.grpNengetsu.Font = new System.Drawing.Font("ＭＳ ゴシック", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.grpNengetsu.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.grpNengetsu.Location = new System.Drawing.Point(4, -4);
            this.grpNengetsu.Name = "grpNengetsu";
            this.grpNengetsu.Size = new System.Drawing.Size(122, 52);
            this.grpNengetsu.TabIndex = 0;
            this.grpNengetsu.TabStop = false;
            // 
            // lblMonthNengetsu
            // 
            this.lblMonthNengetsu.BackColor = System.Drawing.Color.White;
            this.lblMonthNengetsu.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMonthNengetsu.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMonthNengetsu.ForeColor = System.Drawing.Color.Silver;
            this.lblMonthNengetsu.Location = new System.Drawing.Point(84, 28);
            this.lblMonthNengetsu.Name = "lblMonthNengetsu";
            this.lblMonthNengetsu.Size = new System.Drawing.Size(34, 20);
            this.lblMonthNengetsu.TabIndex = 7;
            this.lblMonthNengetsu.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblGengoYearNengetsu
            // 
            this.lblGengoYearNengetsu.BackColor = System.Drawing.Color.White;
            this.lblGengoYearNengetsu.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengoYearNengetsu.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengoYearNengetsu.ForeColor = System.Drawing.Color.Silver;
            this.lblGengoYearNengetsu.Location = new System.Drawing.Point(44, 28);
            this.lblGengoYearNengetsu.Name = "lblGengoYearNengetsu";
            this.lblGengoYearNengetsu.Size = new System.Drawing.Size(34, 20);
            this.lblGengoYearNengetsu.TabIndex = 6;
            this.lblGengoYearNengetsu.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblNengetsu
            // 
            this.lblNengetsu.BackColor = System.Drawing.Color.Silver;
            this.lblNengetsu.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblNengetsu.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblNengetsu.Location = new System.Drawing.Point(4, 10);
            this.lblNengetsu.Name = "lblNengetsu";
            this.lblNengetsu.Size = new System.Drawing.Size(114, 16);
            this.lblNengetsu.TabIndex = 5;
            this.lblNengetsu.Text = "年　月";
            this.lblNengetsu.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblGengoNengetsu
            // 
            this.lblGengoNengetsu.BackColor = System.Drawing.Color.Silver;
            this.lblGengoNengetsu.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengoNengetsu.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengoNengetsu.Location = new System.Drawing.Point(4, 28);
            this.lblGengoNengetsu.Name = "lblGengoNengetsu";
            this.lblGengoNengetsu.Size = new System.Drawing.Size(40, 20);
            this.lblGengoNengetsu.TabIndex = 0;
            this.lblGengoNengetsu.Text = "平成";
            this.lblGengoNengetsu.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMode
            // 
            this.lblMode.BackColor = System.Drawing.Color.White;
            this.lblMode.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMode.Location = new System.Drawing.Point(780, 65);
            this.lblMode.Name = "lblMode";
            this.lblMode.Size = new System.Drawing.Size(26, 36);
            this.lblMode.TabIndex = 909;
            this.lblMode.Text = "登録";
            // 
            // pnlGokei
            // 
            this.pnlGokei.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlGokei.Controls.Add(this.lblGokei10);
            this.pnlGokei.Controls.Add(this.lblGokei9);
            this.pnlGokei.Controls.Add(this.lblGokei8);
            this.pnlGokei.Controls.Add(this.lblGokei7);
            this.pnlGokei.Controls.Add(this.lblGokei6);
            this.pnlGokei.Controls.Add(this.lblGokei5);
            this.pnlGokei.Controls.Add(this.lblGokei4);
            this.pnlGokei.Controls.Add(this.lblGokei3);
            this.pnlGokei.Controls.Add(this.txtGokei1);
            this.pnlGokei.Controls.Add(this.lblGokei);
            this.pnlGokei.Controls.Add(this.lblGokei2);
            this.pnlGokei.Controls.Add(this.txtGokei10);
            this.pnlGokei.Controls.Add(this.txtGokei9);
            this.pnlGokei.Controls.Add(this.txtGokei8);
            this.pnlGokei.Controls.Add(this.txtGokei7);
            this.pnlGokei.Controls.Add(this.txtGokei6);
            this.pnlGokei.Controls.Add(this.txtGokei5);
            this.pnlGokei.Controls.Add(this.txtGokei4);
            this.pnlGokei.Controls.Add(this.txtGokei3);
            this.pnlGokei.Controls.Add(this.txtGokei2);
            this.pnlGokei.Controls.Add(this.lblGokei1);
            this.pnlGokei.Location = new System.Drawing.Point(17, 467);
            this.pnlGokei.Name = "pnlGokei";
            this.pnlGokei.Size = new System.Drawing.Size(789, 60);
            this.pnlGokei.TabIndex = 7;
            // 
            // lblGokei10
            // 
            this.lblGokei10.AutoEllipsis = true;
            this.lblGokei10.BackColor = System.Drawing.Color.Silver;
            this.lblGokei10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGokei10.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGokei10.ForeColor = System.Drawing.Color.White;
            this.lblGokei10.Location = new System.Drawing.Point(706, 3);
            this.lblGokei10.Margin = new System.Windows.Forms.Padding(0);
            this.lblGokei10.Name = "lblGokei10";
            this.lblGokei10.Size = new System.Drawing.Size(75, 33);
            this.lblGokei10.TabIndex = 39;
            this.lblGokei10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblGokei9
            // 
            this.lblGokei9.AutoEllipsis = true;
            this.lblGokei9.BackColor = System.Drawing.Color.Silver;
            this.lblGokei9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGokei9.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGokei9.ForeColor = System.Drawing.Color.White;
            this.lblGokei9.Location = new System.Drawing.Point(631, 3);
            this.lblGokei9.Margin = new System.Windows.Forms.Padding(0);
            this.lblGokei9.Name = "lblGokei9";
            this.lblGokei9.Size = new System.Drawing.Size(75, 33);
            this.lblGokei9.TabIndex = 38;
            this.lblGokei9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblGokei8
            // 
            this.lblGokei8.AutoEllipsis = true;
            this.lblGokei8.BackColor = System.Drawing.Color.Silver;
            this.lblGokei8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGokei8.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGokei8.ForeColor = System.Drawing.Color.White;
            this.lblGokei8.Location = new System.Drawing.Point(556, 3);
            this.lblGokei8.Margin = new System.Windows.Forms.Padding(0);
            this.lblGokei8.Name = "lblGokei8";
            this.lblGokei8.Size = new System.Drawing.Size(75, 33);
            this.lblGokei8.TabIndex = 37;
            this.lblGokei8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblGokei7
            // 
            this.lblGokei7.AutoEllipsis = true;
            this.lblGokei7.BackColor = System.Drawing.Color.Silver;
            this.lblGokei7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGokei7.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGokei7.ForeColor = System.Drawing.Color.White;
            this.lblGokei7.Location = new System.Drawing.Point(481, 3);
            this.lblGokei7.Margin = new System.Windows.Forms.Padding(0);
            this.lblGokei7.Name = "lblGokei7";
            this.lblGokei7.Size = new System.Drawing.Size(75, 33);
            this.lblGokei7.TabIndex = 36;
            this.lblGokei7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblGokei6
            // 
            this.lblGokei6.AutoEllipsis = true;
            this.lblGokei6.BackColor = System.Drawing.Color.Silver;
            this.lblGokei6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGokei6.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGokei6.ForeColor = System.Drawing.Color.White;
            this.lblGokei6.Location = new System.Drawing.Point(406, 3);
            this.lblGokei6.Margin = new System.Windows.Forms.Padding(0);
            this.lblGokei6.Name = "lblGokei6";
            this.lblGokei6.Size = new System.Drawing.Size(75, 33);
            this.lblGokei6.TabIndex = 35;
            this.lblGokei6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblGokei5
            // 
            this.lblGokei5.AutoEllipsis = true;
            this.lblGokei5.BackColor = System.Drawing.Color.Silver;
            this.lblGokei5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGokei5.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGokei5.ForeColor = System.Drawing.Color.White;
            this.lblGokei5.Location = new System.Drawing.Point(331, 3);
            this.lblGokei5.Margin = new System.Windows.Forms.Padding(0);
            this.lblGokei5.Name = "lblGokei5";
            this.lblGokei5.Size = new System.Drawing.Size(75, 33);
            this.lblGokei5.TabIndex = 34;
            this.lblGokei5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblGokei4
            // 
            this.lblGokei4.AutoEllipsis = true;
            this.lblGokei4.BackColor = System.Drawing.Color.Silver;
            this.lblGokei4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGokei4.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGokei4.ForeColor = System.Drawing.Color.White;
            this.lblGokei4.Location = new System.Drawing.Point(256, 3);
            this.lblGokei4.Margin = new System.Windows.Forms.Padding(0);
            this.lblGokei4.Name = "lblGokei4";
            this.lblGokei4.Size = new System.Drawing.Size(75, 33);
            this.lblGokei4.TabIndex = 33;
            this.lblGokei4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblGokei3
            // 
            this.lblGokei3.AutoEllipsis = true;
            this.lblGokei3.BackColor = System.Drawing.Color.Silver;
            this.lblGokei3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGokei3.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGokei3.ForeColor = System.Drawing.Color.White;
            this.lblGokei3.Location = new System.Drawing.Point(181, 3);
            this.lblGokei3.Margin = new System.Windows.Forms.Padding(0);
            this.lblGokei3.Name = "lblGokei3";
            this.lblGokei3.Size = new System.Drawing.Size(75, 33);
            this.lblGokei3.TabIndex = 32;
            this.lblGokei3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtGokei1
            // 
            this.txtGokei1.AutoSizeFromLength = false;
            this.txtGokei1.DisplayLength = null;
            this.txtGokei1.Enabled = false;
            this.txtGokei1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGokei1.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtGokei1.Location = new System.Drawing.Point(31, 36);
            this.txtGokei1.MaxLength = 10;
            this.txtGokei1.Name = "txtGokei1";
            this.txtGokei1.Size = new System.Drawing.Size(75, 20);
            this.txtGokei1.TabIndex = 2;
            this.txtGokei1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtGokei1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblGokei
            // 
            this.lblGokei.AutoEllipsis = true;
            this.lblGokei.BackColor = System.Drawing.Color.Silver;
            this.lblGokei.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblGokei.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGokei.Location = new System.Drawing.Point(5, 3);
            this.lblGokei.Margin = new System.Windows.Forms.Padding(0);
            this.lblGokei.Name = "lblGokei";
            this.lblGokei.Size = new System.Drawing.Size(23, 52);
            this.lblGokei.TabIndex = 28;
            this.lblGokei.Text = "合　計";
            this.lblGokei.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblGokei2
            // 
            this.lblGokei2.AutoEllipsis = true;
            this.lblGokei2.BackColor = System.Drawing.Color.Silver;
            this.lblGokei2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGokei2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGokei2.ForeColor = System.Drawing.Color.White;
            this.lblGokei2.Location = new System.Drawing.Point(106, 3);
            this.lblGokei2.Margin = new System.Windows.Forms.Padding(0);
            this.lblGokei2.Name = "lblGokei2";
            this.lblGokei2.Size = new System.Drawing.Size(75, 33);
            this.lblGokei2.TabIndex = 27;
            this.lblGokei2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtGokei10
            // 
            this.txtGokei10.AutoSizeFromLength = false;
            this.txtGokei10.DisplayLength = null;
            this.txtGokei10.Enabled = false;
            this.txtGokei10.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGokei10.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtGokei10.Location = new System.Drawing.Point(706, 36);
            this.txtGokei10.MaxLength = 10;
            this.txtGokei10.Name = "txtGokei10";
            this.txtGokei10.Size = new System.Drawing.Size(75, 20);
            this.txtGokei10.TabIndex = 26;
            this.txtGokei10.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtGokei10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtGokei9
            // 
            this.txtGokei9.AutoSizeFromLength = false;
            this.txtGokei9.DisplayLength = null;
            this.txtGokei9.Enabled = false;
            this.txtGokei9.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGokei9.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtGokei9.Location = new System.Drawing.Point(631, 36);
            this.txtGokei9.MaxLength = 10;
            this.txtGokei9.Name = "txtGokei9";
            this.txtGokei9.Size = new System.Drawing.Size(75, 20);
            this.txtGokei9.TabIndex = 25;
            this.txtGokei9.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtGokei9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtGokei8
            // 
            this.txtGokei8.AutoSizeFromLength = false;
            this.txtGokei8.DisplayLength = null;
            this.txtGokei8.Enabled = false;
            this.txtGokei8.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGokei8.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtGokei8.Location = new System.Drawing.Point(556, 36);
            this.txtGokei8.MaxLength = 10;
            this.txtGokei8.Name = "txtGokei8";
            this.txtGokei8.Size = new System.Drawing.Size(75, 20);
            this.txtGokei8.TabIndex = 24;
            this.txtGokei8.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtGokei8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtGokei7
            // 
            this.txtGokei7.AutoSizeFromLength = false;
            this.txtGokei7.DisplayLength = null;
            this.txtGokei7.Enabled = false;
            this.txtGokei7.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGokei7.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtGokei7.Location = new System.Drawing.Point(481, 36);
            this.txtGokei7.MaxLength = 10;
            this.txtGokei7.Name = "txtGokei7";
            this.txtGokei7.Size = new System.Drawing.Size(75, 20);
            this.txtGokei7.TabIndex = 23;
            this.txtGokei7.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtGokei7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtGokei6
            // 
            this.txtGokei6.AutoSizeFromLength = false;
            this.txtGokei6.DisplayLength = null;
            this.txtGokei6.Enabled = false;
            this.txtGokei6.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGokei6.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtGokei6.Location = new System.Drawing.Point(406, 36);
            this.txtGokei6.MaxLength = 10;
            this.txtGokei6.Name = "txtGokei6";
            this.txtGokei6.Size = new System.Drawing.Size(75, 20);
            this.txtGokei6.TabIndex = 22;
            this.txtGokei6.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtGokei6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtGokei5
            // 
            this.txtGokei5.AutoSizeFromLength = false;
            this.txtGokei5.DisplayLength = null;
            this.txtGokei5.Enabled = false;
            this.txtGokei5.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGokei5.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtGokei5.Location = new System.Drawing.Point(331, 36);
            this.txtGokei5.MaxLength = 10;
            this.txtGokei5.Name = "txtGokei5";
            this.txtGokei5.Size = new System.Drawing.Size(75, 20);
            this.txtGokei5.TabIndex = 21;
            this.txtGokei5.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtGokei5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtGokei4
            // 
            this.txtGokei4.AutoSizeFromLength = false;
            this.txtGokei4.DisplayLength = null;
            this.txtGokei4.Enabled = false;
            this.txtGokei4.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGokei4.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtGokei4.Location = new System.Drawing.Point(256, 36);
            this.txtGokei4.MaxLength = 10;
            this.txtGokei4.Name = "txtGokei4";
            this.txtGokei4.Size = new System.Drawing.Size(75, 20);
            this.txtGokei4.TabIndex = 20;
            this.txtGokei4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtGokei4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtGokei3
            // 
            this.txtGokei3.AutoSizeFromLength = false;
            this.txtGokei3.DisplayLength = null;
            this.txtGokei3.Enabled = false;
            this.txtGokei3.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGokei3.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtGokei3.Location = new System.Drawing.Point(181, 36);
            this.txtGokei3.MaxLength = 10;
            this.txtGokei3.Name = "txtGokei3";
            this.txtGokei3.Size = new System.Drawing.Size(75, 20);
            this.txtGokei3.TabIndex = 19;
            this.txtGokei3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtGokei3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtGokei2
            // 
            this.txtGokei2.AutoSizeFromLength = false;
            this.txtGokei2.DisplayLength = null;
            this.txtGokei2.Enabled = false;
            this.txtGokei2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGokei2.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtGokei2.Location = new System.Drawing.Point(106, 36);
            this.txtGokei2.MaxLength = 10;
            this.txtGokei2.Name = "txtGokei2";
            this.txtGokei2.Size = new System.Drawing.Size(75, 20);
            this.txtGokei2.TabIndex = 18;
            this.txtGokei2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox_KeyDown);
            this.txtGokei2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblGokei1
            // 
            this.lblGokei1.AutoEllipsis = true;
            this.lblGokei1.BackColor = System.Drawing.Color.Silver;
            this.lblGokei1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGokei1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGokei1.ForeColor = System.Drawing.Color.White;
            this.lblGokei1.Location = new System.Drawing.Point(31, 3);
            this.lblGokei1.Margin = new System.Windows.Forms.Padding(0);
            this.lblGokei1.Name = "lblGokei1";
            this.lblGokei1.Size = new System.Drawing.Size(75, 33);
            this.lblGokei1.TabIndex = 6;
            this.lblGokei1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // KYSE1012
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.pnlGokei);
            this.Controls.Add(this.lblMode);
            this.Controls.Add(this.pnlKojo);
            this.Controls.Add(this.pnlShikyu);
            this.Controls.Add(this.pnlKintai);
            this.Controls.Add(this.pnlSyain);
            this.Controls.Add(this.pnlShikyuInfo);
            this.DoubleBuffered = true;
            this.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.Name = "KYSE1012";
            this.Text = "賞与明細入力";
            this.Controls.SetChildIndex(this.pnlShikyuInfo, 0);
            this.Controls.SetChildIndex(this.pnlSyain, 0);
            this.Controls.SetChildIndex(this.pnlKintai, 0);
            this.Controls.SetChildIndex(this.pnlShikyu, 0);
            this.Controls.SetChildIndex(this.pnlKojo, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.lblMode, 0);
            this.Controls.SetChildIndex(this.pnlGokei, 0);
            this.pnlDebug.ResumeLayout(false);
            this.pnlSyain.ResumeLayout(false);
            this.pnlSyain.PerformLayout();
            this.pnlKintai.ResumeLayout(false);
            this.pnlKintai.PerformLayout();
            this.pnlShikyu.ResumeLayout(false);
            this.pnlShikyu.PerformLayout();
            this.pnlKojo.ResumeLayout(false);
            this.pnlKojo.PerformLayout();
            this.pnlShikyuInfo.ResumeLayout(false);
            this.grpShikyubi.ResumeLayout(false);
            this.grpNengetsu.ResumeLayout(false);
            this.pnlGokei.ResumeLayout(false);
            this.pnlGokei.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private jp.co.fsi.common.FsiPanel pnlSyain;
        private System.Windows.Forms.Label label2;
        private common.controls.FsiTextBox txtShainCd;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private jp.co.fsi.common.FsiPanel pnlKintai;
        private common.controls.FsiTextBox txtKintai10;
        private common.controls.FsiTextBox txtKintai9;
        private common.controls.FsiTextBox txtKintai8;
        private common.controls.FsiTextBox txtKintai7;
        private common.controls.FsiTextBox txtKintai6;
        private common.controls.FsiTextBox txtKintai5;
        private common.controls.FsiTextBox txtKintai4;
        private common.controls.FsiTextBox txtKintai3;
        private common.controls.FsiTextBox txtKintai2;
        private System.Windows.Forms.Label lblKintai1;
        private System.Windows.Forms.Label lblKintai2;
        private System.Windows.Forms.Label lblKintai;
        private common.controls.FsiTextBox txtKintai11;
        private System.Windows.Forms.Label lblKintai11;
        private common.controls.FsiTextBox txtKintai1;
        private System.Windows.Forms.Label lblKintai20;
        private System.Windows.Forms.Label lblKintai19;
        private System.Windows.Forms.Label lblKintai18;
        private System.Windows.Forms.Label lblKintai17;
        private System.Windows.Forms.Label lblKintai16;
        private System.Windows.Forms.Label lblKintai15;
        private System.Windows.Forms.Label lblKintai14;
        private System.Windows.Forms.Label lblKintai13;
        private System.Windows.Forms.Label lblKintai12;
        private common.controls.FsiTextBox txtKintai20;
        private common.controls.FsiTextBox txtKintai19;
        private common.controls.FsiTextBox txtKintai18;
        private common.controls.FsiTextBox txtKintai17;
        private common.controls.FsiTextBox txtKintai16;
        private common.controls.FsiTextBox txtKintai15;
        private common.controls.FsiTextBox txtKintai14;
        private common.controls.FsiTextBox txtKintai13;
        private common.controls.FsiTextBox txtKintai12;
        private System.Windows.Forms.Label lblKintai10;
        private System.Windows.Forms.Label lblKintai9;
        private System.Windows.Forms.Label lblKintai8;
        private System.Windows.Forms.Label lblKintai7;
        private System.Windows.Forms.Label lblKintai6;
        private System.Windows.Forms.Label lblKintai5;
        private System.Windows.Forms.Label lblKintai4;
        private System.Windows.Forms.Label lblKintai3;
        private jp.co.fsi.common.FsiPanel pnlShikyu;
        private System.Windows.Forms.Label lblShikyu20;
        private System.Windows.Forms.Label lblShikyu19;
        private System.Windows.Forms.Label lblShikyu18;
        private System.Windows.Forms.Label lblShikyu17;
        private System.Windows.Forms.Label lblShikyu16;
        private System.Windows.Forms.Label lblShikyu15;
        private System.Windows.Forms.Label lblShikyu14;
        private System.Windows.Forms.Label lblShikyu13;
        private System.Windows.Forms.Label lblShikyu12;
        private common.controls.FsiTextBox txtShikyu20;
        private common.controls.FsiTextBox txtShikyu19;
        private common.controls.FsiTextBox txtShikyu18;
        private common.controls.FsiTextBox txtShikyu17;
        private common.controls.FsiTextBox txtShikyu16;
        private common.controls.FsiTextBox txtShikyu15;
        private common.controls.FsiTextBox txtShikyu14;
        private common.controls.FsiTextBox txtShikyu13;
        private common.controls.FsiTextBox txtShikyu12;
        private System.Windows.Forms.Label lblShikyu10;
        private System.Windows.Forms.Label lblShikyu9;
        private System.Windows.Forms.Label lblShikyu8;
        private System.Windows.Forms.Label lblShikyu7;
        private System.Windows.Forms.Label lblShikyu6;
        private System.Windows.Forms.Label lblShikyu5;
        private System.Windows.Forms.Label lblShikyu4;
        private System.Windows.Forms.Label lblShikyu3;
        private common.controls.FsiTextBox txtShikyu11;
        private System.Windows.Forms.Label lblShikyu11;
        private common.controls.FsiTextBox txtShikyu1;
        private System.Windows.Forms.Label lblShikyu;
        private System.Windows.Forms.Label lblShikyu2;
        private common.controls.FsiTextBox txtShikyu10;
        private common.controls.FsiTextBox txtShikyu9;
        private common.controls.FsiTextBox txtShikyu8;
        private common.controls.FsiTextBox txtShikyu7;
        private common.controls.FsiTextBox txtShikyu6;
        private common.controls.FsiTextBox txtShikyu5;
        private common.controls.FsiTextBox txtShikyu4;
        private common.controls.FsiTextBox txtShikyu3;
        private common.controls.FsiTextBox txtShikyu2;
        private System.Windows.Forms.Label lblShikyu1;
        private jp.co.fsi.common.FsiPanel pnlKojo;
        private System.Windows.Forms.Label lblKojo20;
        private System.Windows.Forms.Label lblKojo19;
        private System.Windows.Forms.Label lblKojo18;
        private System.Windows.Forms.Label lblKojo17;
        private System.Windows.Forms.Label lblKojo16;
        private System.Windows.Forms.Label lblKojo15;
        private System.Windows.Forms.Label lblKojo14;
        private System.Windows.Forms.Label lblKojo13;
        private System.Windows.Forms.Label lblKojo12;
        private common.controls.FsiTextBox txtKojo20;
        private common.controls.FsiTextBox txtKojo19;
        private common.controls.FsiTextBox txtKojo18;
        private common.controls.FsiTextBox txtKojo17;
        private common.controls.FsiTextBox txtKojo16;
        private common.controls.FsiTextBox txtKojo15;
        private common.controls.FsiTextBox txtKojo14;
        private common.controls.FsiTextBox txtKojo13;
        private common.controls.FsiTextBox txtKojo12;
        private System.Windows.Forms.Label lblKojo10;
        private System.Windows.Forms.Label lblKojo9;
        private System.Windows.Forms.Label lblKojo8;
        private System.Windows.Forms.Label lblKojo7;
        private System.Windows.Forms.Label lblKojo6;
        private System.Windows.Forms.Label lblKojo5;
        private System.Windows.Forms.Label lblKojo4;
        private System.Windows.Forms.Label lblKojo3;
        private common.controls.FsiTextBox txtKojo11;
        private System.Windows.Forms.Label lblKojo11;
        private common.controls.FsiTextBox txtKojo1;
        private System.Windows.Forms.Label lblKojo;
        private System.Windows.Forms.Label lblKojo2;
        private common.controls.FsiTextBox txtKojo10;
        private common.controls.FsiTextBox txtKojo9;
        private common.controls.FsiTextBox txtKojo8;
        private common.controls.FsiTextBox txtKojo7;
        private common.controls.FsiTextBox txtKojo6;
        private common.controls.FsiTextBox txtKojo5;
        private common.controls.FsiTextBox txtKojo4;
        private common.controls.FsiTextBox txtKojo3;
        private common.controls.FsiTextBox txtKojo2;
        private System.Windows.Forms.Label lblKojo1;
        private jp.co.fsi.common.FsiPanel pnlShikyuInfo;
        private System.Windows.Forms.GroupBox grpShikyubi;
        private System.Windows.Forms.Label lblShikyubi;
        private System.Windows.Forms.Label lblGengoShikyubi;
        private System.Windows.Forms.GroupBox grpNengetsu;
        private System.Windows.Forms.Label lblNengetsu;
        private System.Windows.Forms.Label lblGengoNengetsu;
        private System.Windows.Forms.Label lblMode;
        private jp.co.fsi.common.FsiPanel pnlGokei;
        private System.Windows.Forms.Label lblGokei10;
        private System.Windows.Forms.Label lblGokei9;
        private System.Windows.Forms.Label lblGokei8;
        private System.Windows.Forms.Label lblGokei7;
        private System.Windows.Forms.Label lblGokei6;
        private System.Windows.Forms.Label lblGokei5;
        private System.Windows.Forms.Label lblGokei4;
        private System.Windows.Forms.Label lblGokei3;
        private common.controls.FsiTextBox txtGokei1;
        private System.Windows.Forms.Label lblGokei;
        private System.Windows.Forms.Label lblGokei2;
        private common.controls.FsiTextBox txtGokei10;
        private common.controls.FsiTextBox txtGokei9;
        private common.controls.FsiTextBox txtGokei8;
        private common.controls.FsiTextBox txtGokei7;
        private common.controls.FsiTextBox txtGokei6;
        private common.controls.FsiTextBox txtGokei5;
        private common.controls.FsiTextBox txtGokei4;
        private common.controls.FsiTextBox txtGokei3;
        private common.controls.FsiTextBox txtGokei2;
        private System.Windows.Forms.Label lblGokei1;
        private System.Windows.Forms.Label lblKyuyokeitai;
        private System.Windows.Forms.Label lblShyozoku;
        private System.Windows.Forms.Label lblShainNm;
        private System.Windows.Forms.Label lblDayShikyubi;
        private System.Windows.Forms.Label lblMonthShikyubi;
        private System.Windows.Forms.Label lblGengoYearShikyubi;
        private System.Windows.Forms.Label lblMonthNengetsu;
        private System.Windows.Forms.Label lblGengoYearNengetsu;

    }
}