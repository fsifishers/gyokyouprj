﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.ky.kyse1011
{
    /// <summary>
    /// 賞与明細入力(KYSE1011)
    /// </summary>
    public partial class KYSE1011 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(日付変更ダイアログ)
        /// </summary>
        private const string MODE_CD_SRC = "1";
        #endregion

        #region 変数(private)
        private DateTime _nengetsu;
        private DateTime _shikyubi;
        private int _shoyoCalcKikan;
        private bool _enterAll;
        private bool _calculate;
        private bool _isPressF6;
        #endregion

        #region プロパティ
        /// <summary>
        /// 支給月
        /// </summary>
        public DateTime Nengetsu
        {
            get
            {
                return  _nengetsu;
            }
            set
            {
                _nengetsu = Util.ToDate(value);
            } 
        }

        /// <summary>
        /// 支給日
        /// </summary>
        public DateTime Shikyubi
        {
            get
            {
                return _shikyubi;
            }
            set
            {
                _shikyubi = Util.ToDate(value);
            }
        }

        /// <summary>
        /// 賞与計算期間
        /// </summary>
        public int ShoyoCalcKikan
        {
            get
            {
                return _shoyoCalcKikan;
            }
            set
            {
                _shoyoCalcKikan = Util.ToInt(value);
            }
        }

        /// <summary>
        /// 全て入力判定
        /// </summary>
        public bool EnterAll 
        {
            get
            {
                return _enterAll;
            }
            set
            {
                _enterAll = value;
            }
        }

        /// <summary>
        /// 更新時に計算判定
        /// </summary>
        public bool Calculate 
        {
            get
            {
                return _calculate;
            }
            set
            {
                _calculate = value;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KYSE1011()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                // Par1が"1"の場合、日付変更ダイアログとしての挙動をする
                // サイズを縮める
                this.Size = new Size(600, 500);
                // EscapeとF6のみ表示
                this.btnEsc.Location = this.btnF1.Location;
                this.btnF1.Visible = false;
                this.btnF2.Visible = false;
                this.btnF3.Visible = false;
                this.btnF4.Visible = false;
                this.btnF5.Visible = false;
                this.btnF6.Location = this.btnF2.Location;
                this.btnF7.Visible = false;
                this.btnF8.Visible = false;
                this.btnF9.Visible = false;
                this.btnF10.Visible = false;
                this.btnF11.Visible = false;
                this.btnF12.Visible = false;
                this.ShowFButton = true;
            }
            else
            {
                // 支給月・支給日の初期値取得
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                string sql = "SELECT MAX(NENGETSU) AS MAX_NENGETSU";
                sql += ",(SELECT MAX(SY.SHIKYUBI) FROM TB_KY_SHOYO_MEISAI_KINTAI AS SY";
                sql += " WHERE SY.KAISHA_CD = KAISHA_CD AND SY.NENGETSU =";
                sql += " (SELECT MAX(NENGETSU) FROM TB_KY_SHOYO_MEISAI_KINTAI)) AS MAX_SHIKYUBI";
                sql += " FROM TB_KY_SHOYO_MEISAI_KINTAI WHERE KAISHA_CD = @KAISHA_CD";
                DataTable dtShikyuInfo =
                    this.Dba.GetDataTableFromSqlWithParams(sql, dpc);
                if (dtShikyuInfo.Rows.Count == 0 || ValChk.IsEmpty(dtShikyuInfo.Rows[0]["MAX_NENGETSU"]))
                {
                    // 該当支給データ存在しない場合はシステム日付をセット
                    this.Nengetsu = DateTime.Now;
                    this.Shikyubi = DateTime.Now;
                }
                else
                { 
                    // 最新の支給月・支給日を取得
                    DataRow drShikyuInfo = dtShikyuInfo.Rows[0];
                    this.Nengetsu = Util.ToDate(drShikyuInfo["MAX_NENGETSU"]);
                    this.Shikyubi = Util.ToDate(drShikyuInfo["MAX_SHIKYUBI"]);
                }

                // 全て入力判定初期値(OFF)
                this.EnterAll = false;
                // 更新時に計算判定初期値(ON)
                this.Calculate = true;
            }

            // 各コントロールに値セット
            // 支給年月
            SetJpDateNengetsu(Util.ConvJpDate(this.Nengetsu, this.Dba));
            // 支給日
            SetJpDateShikyubi(Util.ConvJpDate(this.Shikyubi, this.Dba));
            // 賞与計算期間
            this.txtShoyoCalcKikan.Text = "6";
            // 全て入力チェックボックス
            chkEnterAll.Checked = this.EnterAll;
            // 更新時に計算チェックボックス
            chkCalculate.Checked = this.Calculate;

            // 支給月年にフォーカス
            this.txtGengoYearNengetsu.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtGengoYearNengetsu.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 支給月元号年、支給日元号年に
            // フォーカス時のみF1を有効にする
            // TODO:賞与計算期間
            switch (this.ActiveCtlNm)
            {
                case "txtGengoYearNengetsu":
                    this.btnF1.Enabled = true;
                    break;
                case "txtMonthNengetsu":
                case "txtGengoYearShikyubi":
                    this.btnF1.Enabled = true;
                    break;
                case "txtMonthShikyubi":
                case "txtDayShikyubi":
                case "txtShoyoCalcKikan":
                case "chkEnterAll":
                case "chkCalculate":
                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                // Par1が"1"の場合、日付変更ダイアログとしての処理結果を返す
                this.DialogResult = DialogResult.Cancel;
            }
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        public override void PressF1()
        {
            //アクティブコントロールごとの処理
            switch (this.ActiveCtlNm)
            {
                case "txtGengoYearNengetsu":
                case "txtGengoYearShikyubi":
                    // アセンブリのロード
                    System.Reflection.Assembly asm = System.Reflection.Assembly.LoadFrom("CMCM1021.exe");
                    // フォーム作成
                    Type t = asm.GetType("jp.co.fsi.com.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            if (this.ActiveCtlNm == "txtGengoYearNengetsu")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.InData = this.lblGengoNengetsu.Text;
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.lblGengoNengetsu.Text = result[1];

                                    // 存在しない日付の場合、補正して存在する日付に戻す
                                    SetJpDateNengetsu(Util.FixJpDate(this.lblGengoNengetsu.Text,
                                                            this.txtGengoYearNengetsu.Text,
                                                            this.txtMonthNengetsu.Text,
                                                            "1",
                                                            this.Dba));
                                }
                            }
                            else if (this.ActiveCtlNm == "txtGengoYearShikyubi")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.InData = this.lblGengoShikyubi.Text;
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.lblGengoShikyubi.Text = result[1];

                                    // 存在しない日付の場合、補正して存在する日付に戻す
                                    SetJpDateShikyubi(Util.FixJpDate(this.lblGengoShikyubi.Text, 
                                                        this.txtGengoYearShikyubi.Text,
                                                        this.txtMonthShikyubi.Text, 
                                                        this.txtDayShikyubi.Text, 
                                                        this.Dba));

                                }
                            }
                        }

                    }
                    break;
            }
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        public override void PressF6()
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                // Par1が"1"の場合、日付変更ダイアログとしての処理結果を返却する
                this.DialogResult = DialogResult.OK;

                KYSE1012 frm;
                frm = (KYSE1012)this.Owner;
                frm.Nengetsu = Util.ConvAdDate(this.lblGengoNengetsu.Text, 
                                    this.txtGengoYearNengetsu.Text,
                                    this.txtMonthNengetsu.Text,
                                    "1", 
                                    this.Dba);
                frm.Shikyubi = Util.ConvAdDate(this.lblGengoShikyubi.Text, 
                                    this.txtGengoYearShikyubi.Text,
                                    this.txtMonthShikyubi.Text, 
                                    this.txtDayShikyubi.Text, 
                                    this.Dba);
                // TODO:賞与計算期間
                frm.EnterAll = this.chkEnterAll.Checked;
                frm.Calculate = this.chkCalculate.Checked;

            }
            else
            {
                // 不明動作の回避用
                // F6キーで明細入力を実行して呼出しフォーム終了するとtxtGengoYearNengetsu_Validatingが発生する。
                // 単体実行では影響ないがMENUから実行時に例外となる
                _isPressF6 = true;

                // 賞与明細入力画面の起動
                EditKyuuyoMeisai();
                this.Close();
            }
        }
        #endregion

        #region イベント
        /// <summary>
        /// 支給月(年)値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGengoYearNengetsu_Validating(object sender, CancelEventArgs e)
        {
            // 不明動作の回避用
            if (_isPressF6) return;

            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtGengoYearNengetsu.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtGengoYearNengetsu.SelectAll();
                e.Cancel = true;
                return;
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDateNengetsu(this.FixJpDate(this.lblGengoNengetsu.Text, this.txtGengoYearNengetsu.Text,
                this.txtMonthNengetsu.Text, "1", this.Dba));
        }

        /// <summary>
        /// 支給月(月)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonthNengetsu_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtMonthNengetsu.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtMonthNengetsu.SelectAll();
                e.Cancel = true;
                return;
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDateNengetsu(this.FixJpDate(this.lblGengoNengetsu.Text, this.txtGengoYearNengetsu.Text,
                this.txtMonthNengetsu.Text, "1", this.Dba));
        }

        /// <summary>
        /// 支給日(年)値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGengoYearShikyubi_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtGengoYearShikyubi.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtGengoYearShikyubi.SelectAll();
                e.Cancel = true;
                return;
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDateShikyubi(this.FixJpDate(this.lblGengoShikyubi.Text, this.txtGengoYearShikyubi.Text,
                this.txtMonthShikyubi.Text, this.txtDayShikyubi.Text, this.Dba));
        }

        /// <summary>
        /// 支給日(月)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonthShikyubi_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtMonthShikyubi.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtMonthShikyubi.SelectAll();
                e.Cancel = true;
                return;
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpDateShikyubi(this.FixJpDate(this.lblGengoShikyubi.Text, this.txtGengoYearShikyubi.Text,
                this.txtMonthShikyubi.Text, this.txtDayShikyubi.Text, this.Dba));
        }

        /// <summary>
        /// 支給日(日)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDayShikyubi_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtDayShikyubi.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtDayShikyubi.SelectAll();
                e.Cancel = true;
                return;
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            this.SetJpDateShikyubi(this.FixJpDate(this.lblGengoShikyubi.Text, this.txtGengoYearShikyubi.Text,
                this.txtMonthShikyubi.Text, this.txtDayShikyubi.Text, this.Dba));
        }

        /// <summary>
        /// 賞与計算期間の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShoyoCalcKikan_Validating(object sender, CancelEventArgs e)
        {
            // 未入力の場合、エラーメッセージを表示し、フォーカスを移動しない
            if (ValChk.IsEmpty(this.txtShoyoCalcKikan.Text))
            {
                Msg.Notice("賞与計算期間が未入力です。");
                this.txtShoyoCalcKikan.SelectAll();
                e.Cancel = true;
                return;
            }

            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtShoyoCalcKikan.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtShoyoCalcKikan.SelectAll();
                e.Cancel = true;
                return;
            }

            // 6,12いずれの入力でもない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!this.txtShoyoCalcKikan.Text.Equals("6") && !this.txtShoyoCalcKikan.Text.Equals("12"))
            {
                Msg.Notice("賞与計算期間の入力に誤りがあります。");
                this.txtShoyoCalcKikan.SelectAll();
                e.Cancel = true;
                return;
            }
        }

        private void chkCalculate_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.PressF6();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 和暦日付を正しい日付に補正します。(FixJpDate へ　和暦年・月・日の各値基本補正)
        /// </summary>
        /// <param name="gengo">元号(漢字名称。「平成」「昭和」など)</param>
        /// <param name="jpYear">年(和暦)</param>
        /// <param name="month">月</param>
        /// <param name="day">日</param>
        /// <param name="dba">データアクセスオブジェクト</param>
        /// <returns>
        /// 和暦にフォーマットされた日付
        /// 0:元号
        /// 1:元号を示す記号
        /// 2:年
        /// 3:月
        /// 4:日
        /// 5:全部一纏めにした日付
        /// </returns>
        private string[] FixJpDate(string gengo, string jpYear, string month, string day, DbAccess dba)
        {
            string fixJpYear = jpYear;
            string fixMonth = month;
            string fixDay = day;

            // 年値基本補正
            if (Util.ToInt(jpYear) < 1) fixJpYear = "0";
            // 月値基本補正
            if (Util.ToInt(month) < 1) fixMonth = "1";
            if (Util.ToInt(month) > 12) fixMonth = "12";
            // 日値基本補正
            if (Util.ToInt(day) < 1) fixDay = "1";
            DateTime getsumatsu = Util.ConvAdDate(gengo, fixJpYear, fixMonth, "1", dba);
            getsumatsu = getsumatsu.AddMonths(1).AddDays(-1);                           // 月末日
            if (Util.ToInt(fixDay) > getsumatsu.Day) fixDay = Util.ToString(getsumatsu.Day);

            // まず西暦に変換
            DateTime datAdDate = Util.ConvAdDate(gengo, fixJpYear, fixMonth, fixDay, dba);

            // それを和暦に変換して返却
            return Util.ConvJpDate(datAdDate, dba);
        }

        /// <summary>
        /// 配列に格納された支給日和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDateShikyubi(string[] arrJpDate)
        {
            this.lblGengoShikyubi.Text = arrJpDate[0];
            this.txtGengoYearShikyubi.Text = arrJpDate[2];
            this.txtMonthShikyubi.Text = arrJpDate[3];
            this.txtDayShikyubi.Text = arrJpDate[4];
        }

        /// <summary>
        /// 配列に格納された支給月和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDateNengetsu(string[] arrJpDate)
        {
            this.lblGengoNengetsu.Text = arrJpDate[0];
            this.txtGengoYearNengetsu.Text = arrJpDate[2];
            this.txtMonthNengetsu.Text = arrJpDate[3];
        }

        /// <summary>
        /// 賞与明細入力編集画面を実行する
        /// </summary>
        private void EditKyuuyoMeisai()
        {
            KYSE1012 frm;

            // 賞与明細入力編集画面を起動
            frm = new KYSE1012();
            frm.Nengetsu = Util.ConvAdDate(this.lblGengoNengetsu.Text, 
                                        this.txtGengoYearNengetsu.Text,
                                        this.txtMonthNengetsu.Text, 
                                        "1", 
                                        this.Dba);
            frm.Shikyubi = Util.ConvAdDate(this.lblGengoShikyubi.Text, 
                                        this.txtGengoYearShikyubi.Text,
                                        this.txtMonthShikyubi.Text, 
                                        this.txtDayShikyubi.Text, 
                                        this.Dba);
            frm.ShoyoCalcKikan = Util.ToInt(this.txtShoyoCalcKikan.Text);
            frm.EnterAll = this.chkEnterAll.Checked;
            frm.Calculate = this.chkCalculate.Checked;
            this.Hide();
            frm.ShowDialog();
            this.Close();
        }
        #endregion

    }
}
