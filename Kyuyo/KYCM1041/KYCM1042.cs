﻿using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.ky.kycm1041
{
    /// <summary>
    /// 部課の登録(KYCM1042)
    /// </summary>
    public partial class KYCM1042 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(新規)
        /// </summary>
        private const string MODE_NEW = "1";

        /// <summary>
        /// モード(編集)
        /// </summary>
        private const string MODE_EDIT = "2";
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KYCM1042()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="par1">引数1</param>
        public KYCM1042(string par1)
            : base(par1)
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // タイトルは非表示
            this.lblTitle.Visible = false;

            // 引数：Par1／モード(1:新規、2:変更)、InData：部門コード、部課コード
            string[] code = (string[])this.InData;
            
            // 部門情報表示
            this.lblBumonCd.Text = code[0];
            this.lblBumonNm.Text = this.Dba.GetName(this.UInfo, "TB_KY_BUMON", " ", code[0]);

            // 初期表示
            if (MODE_NEW.Equals(this.Par1))
            {
                // 新規モード
                InitDispOnNew();
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 編集モード
                InitDispOnEdit();
            }
            else
            {
                // 不正な起動として閉じる
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }

            // ボタン表示
            this.ShowFButton = true;

            // ボタンの表示非表示を設定
            this.btnF6.Location = this.btnF3.Location;
            this.btnF3.Location = this.btnF2.Location;
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF2.Visible = false;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F3キー押下時処理
        /// </summary>
        public override void PressF3()
        {
            // 新規モードの場合は処理させない
            if (this.Par1.Equals(MODE_NEW)) return;

            // 確認メッセージを表示
            string msg = "削除しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // 削除用パラメータ
            string[] code = (string[])this.InData;
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
            dpc.SetParam("@BUMON_CD", SqlDbType.Decimal, 4, code[0]);
            dpc.SetParam("@BUKA_CD", SqlDbType.Decimal, 4, code[1]);

            try
            {
                // トランザクション開始
                this.Dba.BeginTransaction();

                if (MODE_EDIT.Equals(this.Par1))
                {
                    // データ削除
                    // 給与部課マスタ
                    this.Dba.Delete("TB_KY_BUKA", 
                        "KAISHA_CD = @KAISHA_CD AND BUMON_CD = @BUMON_CD AND BUKA_CD = @BUKA_CD", dpc);
                }

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        
        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        public override void PressF6()
        {
            // 確認メッセージを表示
            string msg = (MODE_NEW.Equals(this.Par1) ? "登録" : "更新") + "しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // 入力値をバインドパラメータとしてセットする
            ArrayList alParamsKyBuka = SetKyBukaParams();

            try
            {
                // トランザクション開始
                this.Dba.BeginTransaction();

                if (MODE_NEW.Equals(this.Par1))
                {
                    // データ登録
                    // 給与部課マスタ
                    this.Dba.Insert("TB_KY_BUKA", (DbParamCollection)alParamsKyBuka[0]);
                }
                else if (MODE_EDIT.Equals(this.Par1))
                {
                    // データ更新
                    // 給与部課マスタ
                    this.Dba.Update("TB_KY_BUKA",
                        (DbParamCollection)alParamsKyBuka[1],
                        "KAISHA_CD = @KAISHA_CD AND BUMON_CD = @BUMON_CD AND BUKA_CD = @BUKA_CD",
                        (DbParamCollection)alParamsKyBuka[0]);
                }

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 部課コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBukaCd_Validating(object sender, CancelEventArgs e)
        {
            // 入力値補正
            this.txtBukaCd.Text = Util.ToString(Util.ToDecimal(this.txtBukaCd.Text));

            if (!IsValidBukaCd())
            {
                e.Cancel = true;
                this.txtBukaCd.SelectAll();
            }
            
        }

        /// <summary>
        /// 部課名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBukaNm_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidBukaNm())
            {
                e.Cancel = true;
                this.txtBukaNm.SelectAll();
            }
        }

        /// <summary>
        /// カナ名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBukaKanaNm_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidBukaKanaNm())
            {
                e.Cancel = true;
                this.txtBukaKanaNm.SelectAll();
            }
        }

        /// <summary>
        /// 画面の最後のコントールでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LastControl_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.btnF6.Enabled)
            {
                this.PressF6();
            }
        }
        #endregion

        #region privateメソッド

        /// <summary>
        /// 新規モードの初期表示
        /// </summary>
        private void InitDispOnNew()
        {
            // 初期値
            this.txtBukaCd.Text="0";

            // 追加・変更状態表示
            this.lblMODE.Text = "【追加】";

            // 部課コードに初期フォーカス
            this.ActiveControl = this.txtBukaCd;
            this.txtBukaCd.Focus();
            this.txtBukaCd.SelectAll();
        }

        /// <summary>
        /// 編集モードの初期表示
        /// </summary>
        private void InitDispOnEdit()
        {
            string[] code = (string[])this.InData;

            // 現在DBに登録されている値、入力制御を実装
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
            dpc.SetParam("@BUMON_CD", SqlDbType.Decimal, 4, code[0]);
            dpc.SetParam("@BUKA_CD", SqlDbType.Decimal, 4, code[1]);
            string cols = "BUKA_CD, BUKA_NM, BUKA_KANA_NM";
            string from = "TB_KY_BUKA";
            string where = "KAISHA_CD = @KAISHA_CD AND BUMON_CD = @BUMON_CD AND BUKA_CD = @BUKA_CD";
            DataTable dt = this.Dba.GetDataTableByConditionWithParams(cols, from, where, dpc);
            if (dt.Rows.Count == 0)
            {
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }

            // 取得した内容を表示
            DataRow drDispData = dt.Rows[0];
            this.txtBukaCd.Text = Util.ToString(drDispData["BUKA_CD"]);
            this.txtBukaNm.Text = Util.ToString(drDispData["BUKA_NM"]);
            this.txtBukaKanaNm.Text = Util.ToString(drDispData["BUKA_KANA_NM"]);

            // 追加・変更状態表示
            this.lblMODE.Text = "【変更】";

            // 部課コードは入力不可
            this.lblBukaCd.Enabled = false;
            this.txtBukaCd.Enabled = false;
        }

        /// <summary>
        /// 部課コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidBukaCd()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtBukaCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 既に存在するコードを入力した場合はエラーとする
            if (this.txtBukaCd.Text != "0")
            {
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
                dpc.SetParam("@BUMON_CD", SqlDbType.Decimal, 4, this.lblBumonCd.Text);
                dpc.SetParam("@BUKA_CD", SqlDbType.Decimal, 4, this.txtBukaCd.Text);
                string cols = "BUMON_CD";
                string from = "TB_KY_BUKA";
                string where = "KAISHA_CD = @KAISHA_CD AND BUMON_CD = @BUMON_CD AND BUKA_CD = @BUKA_CD";
                DataTable dt = this.Dba.GetDataTableByConditionWithParams(cols, from, where, dpc);
                if (dt.Rows.Count > 0)
                {
                    Msg.Error("既に存在する部課コードと重複しています。");
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 部課名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidBukaNm()
        {
            // 指定バイト数を超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtBukaNm.Text, this.txtBukaNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// カナ名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidBukaKanaNm()
        {
            // 指定バイト数を超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtBukaKanaNm.Text, this.txtBukaKanaNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 必須項目のチェック
            if (this.txtBukaCd.Text == "0")
            {
                Msg.Notice("ゼロは使用できません。");
                this.txtBukaCd.Focus();
                return false;
            }

            if (MODE_NEW.Equals(this.Par1))
            {
                // 部課コードのチェック
                if (!IsValidBukaCd())
                {
                    this.txtBukaCd.Focus();
                    return false;
                }
            }

            // 部課名のチェック
            if (!IsValidBukaNm())
            {
                this.txtBukaNm.Focus();
                return false;
            }

            // カナ名のチェック
            if (!IsValidBukaNm())
            {
                this.txtBukaNm.Focus();
                return false;
            }

            return true;
        }

        /// <summary>
        /// TB_KY_BUMONに更新するためのパラメータ設定をします。
        /// </summary>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// 更新処理：DbParamCollection*2(Where句,Set句)
        /// </returns>
        private ArrayList SetKyBukaParams()
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            if (MODE_NEW.Equals(this.Par1))
            {
                // 会社コード
                updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
                // 部門コード
                updParam.SetParam("@BUMON_CD", SqlDbType.Decimal, 4, this.lblBumonCd.Text);
                // 部課コード
                updParam.SetParam("@BUKA_CD", SqlDbType.Decimal, 4, this.txtBukaCd.Text);
                // 登録日
                updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWDATE");
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // WHERE句のパラメータに設定
                DbParamCollection whereParam = new DbParamCollection();
                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
                whereParam.SetParam("@BUMON_CD", SqlDbType.Decimal, 4, this.lblBumonCd.Text);
                whereParam.SetParam("@BUKA_CD", SqlDbType.Decimal, 4, this.txtBukaCd.Text);
                alParams.Add(whereParam);
            }

            // 部課名
            updParam.SetParam("@BUKA_NM", SqlDbType.VarChar, 30, this.txtBukaNm.Text);
            // カナ名
            updParam.SetParam("@BUKA_KANA_NM", SqlDbType.VarChar, 30, this.txtBukaKanaNm.Text);
            // 更新日
            updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWDATE");

            alParams.Add(updParam);

            return alParams;
        }

        #endregion

    }
}
