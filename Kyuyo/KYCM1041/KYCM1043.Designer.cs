﻿namespace jp.co.fsi.ky.kycm1041
{
    partial class KYCM1043
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblBumonCdFr = new System.Windows.Forms.Label();
            this.lblCodeBet1 = new System.Windows.Forms.Label();
            this.txtBumonCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtBumonCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblBumonCdTo = new System.Windows.Forms.Label();
            this.gbxCondition = new System.Windows.Forms.GroupBox();
            this.pnlDebug.SuspendLayout();
            this.gbxCondition.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(306, 23);
            this.lblTitle.Text = "部課の印刷";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(12, 45);
            this.pnlDebug.Size = new System.Drawing.Size(541, 100);
            // 
            // lblBumonCdFr
            // 
            this.lblBumonCdFr.BackColor = System.Drawing.Color.Silver;
            this.lblBumonCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBumonCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBumonCdFr.Location = new System.Drawing.Point(77, 32);
            this.lblBumonCdFr.Name = "lblBumonCdFr";
            this.lblBumonCdFr.Size = new System.Drawing.Size(180, 19);
            this.lblBumonCdFr.TabIndex = 910;
            this.lblBumonCdFr.Text = "先  頭";
            this.lblBumonCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCodeBet1
            // 
            this.lblCodeBet1.AutoSize = true;
            this.lblCodeBet1.Location = new System.Drawing.Point(263, 36);
            this.lblCodeBet1.Name = "lblCodeBet1";
            this.lblCodeBet1.Size = new System.Drawing.Size(20, 13);
            this.lblCodeBet1.TabIndex = 911;
            this.lblCodeBet1.Text = "～";
            // 
            // txtBumonCdFr
            // 
            this.txtBumonCdFr.AutoSizeFromLength = false;
            this.txtBumonCdFr.DisplayLength = null;
            this.txtBumonCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtBumonCdFr.Location = new System.Drawing.Point(23, 32);
            this.txtBumonCdFr.MaxLength = 4;
            this.txtBumonCdFr.Name = "txtBumonCdFr";
            this.txtBumonCdFr.Size = new System.Drawing.Size(48, 20);
            this.txtBumonCdFr.TabIndex = 907;
            this.txtBumonCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBumonCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtBumonCdFr_Validating);
            // 
            // txtBumonCdTo
            // 
            this.txtBumonCdTo.AutoSizeFromLength = false;
            this.txtBumonCdTo.DisplayLength = null;
            this.txtBumonCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtBumonCdTo.Location = new System.Drawing.Point(286, 32);
            this.txtBumonCdTo.MaxLength = 4;
            this.txtBumonCdTo.Name = "txtBumonCdTo";
            this.txtBumonCdTo.Size = new System.Drawing.Size(48, 20);
            this.txtBumonCdTo.TabIndex = 908;
            this.txtBumonCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBumonCdTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LastControl_KeyDown);
            this.txtBumonCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtBumonCdTo_Validating);
            // 
            // lblBumonCdTo
            // 
            this.lblBumonCdTo.BackColor = System.Drawing.Color.Silver;
            this.lblBumonCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBumonCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBumonCdTo.Location = new System.Drawing.Point(340, 32);
            this.lblBumonCdTo.Name = "lblBumonCdTo";
            this.lblBumonCdTo.Size = new System.Drawing.Size(180, 19);
            this.lblBumonCdTo.TabIndex = 909;
            this.lblBumonCdTo.Text = "最　後";
            this.lblBumonCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gbxCondition
            // 
            this.gbxCondition.Controls.Add(this.lblBumonCdFr);
            this.gbxCondition.Controls.Add(this.lblCodeBet1);
            this.gbxCondition.Controls.Add(this.txtBumonCdFr);
            this.gbxCondition.Controls.Add(this.txtBumonCdTo);
            this.gbxCondition.Controls.Add(this.lblBumonCdTo);
            this.gbxCondition.Font = new System.Drawing.Font("MS UI Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxCondition.Location = new System.Drawing.Point(12, 12);
            this.gbxCondition.Name = "gbxCondition";
            this.gbxCondition.Size = new System.Drawing.Size(536, 76);
            this.gbxCondition.TabIndex = 912;
            this.gbxCondition.TabStop = false;
            this.gbxCondition.Text = "部門コード範囲";
            // 
            // KYCM1043
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(558, 148);
            this.Controls.Add(this.gbxCondition);
            this.Name = "KYCM1043";
            this.Text = "部課の印刷";
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.gbxCondition, 0);
            this.pnlDebug.ResumeLayout(false);
            this.gbxCondition.ResumeLayout(false);
            this.gbxCondition.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblBumonCdFr;
        private System.Windows.Forms.Label lblCodeBet1;
        private System.Windows.Forms.Label lblBumonCdTo;
        private System.Windows.Forms.GroupBox gbxCondition;
        private common.controls.FsiTextBox txtBumonCdFr;
        private common.controls.FsiTextBox txtBumonCdTo;
    }
}