﻿using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;
using System.Reflection;
using System;

namespace jp.co.fsi.ky.kycm1041
{
    /// <summary>
    /// 部課の登録(KYCM1041)
    /// </summary>
    public partial class KYCM1041 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(コード検索)
        /// </summary>
        private const string MODE_CD_SRC = "1";
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KYCM1041()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.InitForm();は呼び出さなくて構いません。
        /// また、このメソッド内の処理を外出しでこのクラス内にメソッド化するのは構いませんが、
        /// 原則、独自で起動時のイベント処理を実装することは禁じます。
        /// </remarks>
        protected override void InitForm()
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                // Par1が"1"の場合、コード検索画面としての挙動をする
                // タイトルは非表示
                this.lblTitle.Visible = false;
                // サイズを縮める
                this.Size = new Size(574, 438);
                // フォームの配置を上へ移動する
                this.lblBumonCd.Location = new System.Drawing.Point(12, 15);
                this.txtBumonCd.Location = new System.Drawing.Point(96, 15);
                this.lblBumonNm.Location = new System.Drawing.Point(141, 15);
                this.lblKanaName.Location = new System.Drawing.Point(276, 15);
                this.txtKanaName.Location = new System.Drawing.Point(360, 15);
                this.dgvList.Location = new System.Drawing.Point(12, 39);
                // EscapeとF1のみ表示
                this.btnEsc.Location = this.btnF1.Location;
                this.btnF1.Location = this.btnF2.Location;
                this.btnF2.Visible = false;
                this.btnF3.Visible = false;
                this.btnF4.Visible = false;
                this.btnF5.Visible = false;
                this.btnF6.Visible = false;
                this.btnF7.Visible = false;
                this.btnF8.Visible = false;
                this.btnF9.Visible = false;
                this.btnF10.Visible = false;
                this.btnF11.Visible = false;
                this.btnF12.Visible = false;
                this.ShowFButton = true;

                // InData：部門コード
                this.txtBumonCd.Text = (string)this.InData;
                if (ValChk.IsEmpty(this.InData))
                {
                    this.lblBumonNm.Text = "0";
                }
                else
                {
                    this.lblBumonNm.Text = this.lblBumonNm.Text =
                        this.Dba.GetName(this.UInfo, "TB_KY_BUMON", " ", this.txtBumonCd.Text);
                }
                this.txtBumonCd.Enabled = false;
            }
            else
            {
                this.txtBumonCd.Text = "0";
                this.lblBumonNm.Text = "";
            }

            // データ表示
            SearchData(true);

            // カナ名のIMEモードを指定
            this.txtKanaName.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;

            // 部門名にフォーカスを当てる
            this.txtBumonCd.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // F1表示を検索に統一するので個別制御を停止
            //// F1表示内容
            //switch (this.ActiveControl.Name)
            //{
            //    case "txtBumonCd":
            //        this.btnF1.Text = "F1\r\n\r\n検索";
            //        break;
            //    default:
            //        this.btnF1.Text = "F1\r\n\r\nカナ名";
            //        break;
            //}
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                // Par1が"1"の場合、ダイアログとしての処理結果を返却する
                this.DialogResult = DialogResult.Cancel;
            }
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        public override void PressF1()
        {
            switch (this.ActiveCtlNm)
            {
                // コントロール別処理
                case "txtBumonCd":
                    #region 部門検索
                    // アセンブリのロード
                    Assembly asm = System.Reflection.Assembly.LoadFrom("KYCM1031.exe");
                    // フォーム作成
                    Type t = asm.GetType("jp.co.fsi.ky.kycm1031.KYCM1031");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            if (this.ActiveCtlNm == "txtBumonCd")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.Par1 = "1";
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.txtBumonCd.Text = result[0];
                                    this.lblBumonNm.Text = result[1];
                                }
                            }
                        }
                    }
                    #endregion
                    break;
                default:
                    // カナ名にフォーカスを戻す
                    this.txtKanaName.Focus();
                    this.txtKanaName.SelectAll();
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // メンテ機能で立ち上げている場合のみ部課登録画面を立ち上げる
            //if (ValChk.IsEmpty(this.Par1))
            if (ValChk.IsEmpty(this.Par1) && this.txtBumonCd.Text != "0")
            {
                // 部課登録画面の起動
                string[] code = new string[2];
                code[0] = this.txtBumonCd.Text;
                code[1] = string.Empty;
                EditBuka(code);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF5();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF5()
        {
            // 摘要の印刷画面を立ち上げる
            KYCM1043 frmKYCM1043 = new KYCM1043();
            frmKYCM1043.ShowDialog(this);
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            // 設定画面の起動
            // MEMO:原則としてここで渡す帳票IDの設定はReport.csvに保持していることが前提ですが、
            // 保持していない場合は、設定画面での保存(F6)時に新規に設定が保持されます。
            PrintSettingForm psForm = new PrintSettingForm(new string[1] { "KYCM1041R" });
            psForm.ShowDialog();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 部門コード検証時の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBumonCd_Validating(object sender, CancelEventArgs e)
        {
            // 入力値補正
            this.txtBumonCd.Text = Util.ToString(Util.ToDecimal(this.txtBumonCd.Text));

            // 数字のみの入力を許可
            if (!IsValidBumonCd())
            {
                e.Cancel = true;
                this.lblBumonNm.Text = "";
                this.txtBumonCd.SelectAll();
                this.txtBumonCd.Focus();
                return;
            }

            // 従属コントロール
            if (this.txtBumonCd.Modified)
            {
                if (this.txtBumonCd.Text == "0")
                {
                    this.lblBumonNm.Text = "";
                }
                else
                {
                    this.lblBumonNm.Text = this.Dba.GetName(this.UInfo, "TB_KY_BUMON", " ", Util.ToString(this.txtBumonCd.Text));
                }
                this.txtBumonCd.Modified = false;
            }
        }

        /// <summary>
        /// カナ名検証時の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKanaName_Validating(object sender, CancelEventArgs e)
        {
            // 入力された情報を元に検索する
            SearchData(false);
        }

        /// <summary>
        /// グリッドでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (MODE_CD_SRC.Equals(this.Par1))
                {
                    ReturnVal();
                }
                else
                {
                    string[] code = new string[2];
                    code[0] = this.txtBumonCd.Text;
                    code[1] = Util.ToString(this.dgvList.SelectedRows[0].Cells["部課コード"].Value);
                    EditBuka(code);
                    e.Handled = true;
                }
            }
        }

        /// <summary>
        /// グリッドのセルダブルクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                ReturnVal();
            }
            else
            {
                string[] code = new string[2];
                code[0] = this.txtBumonCd.Text;
                code[1] = Util.ToString(this.dgvList.SelectedRows[0].Cells["部課コード"].Value);
                EditBuka(code);
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// データを検索する
        /// </summary>
        private void SearchData(bool initial)
        {
            // 部課マスタからデータを取得して表示
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
            dpc.SetParam("BUMON_CD", SqlDbType.Decimal, 4, Util.ToDecimal(this.txtBumonCd.Text));

            string where = "KAISHA_CD = @KAISHA_CD"
                            + " AND BUMON_CD = @BUMON_CD";
            // カナ名が入力されている場合
            if (!ValChk.IsEmpty(this.txtKanaName.Text))
            {
                where += " AND BUKA_KANA_NM LIKE @BUKA_KANA_NM";
                dpc.SetParam("@BUKA_KANA_NM", SqlDbType.VarChar, 30 + 2, "%" + this.txtKanaName.Text + "%");
            }
            string cols = "BUKA_CD AS 部課コード, BUKA_NM AS 部課名, BUKA_KANA_NM AS 部課カナ名";
            string from = "TB_KY_BUKA";
            string order = "BUKA_CD";

            DataTable dt = this.Dba.GetDataTableByConditionWithParams(cols, from, where, order, dpc);

            // 初期処理以外の場合、該当データがなければエラーメッセージを表示
            if (!initial)
            {
                if (dt.Rows.Count == 0)
                {
                    Msg.Info("該当データがありません。");

                    dt.Rows.Add(dt.NewRow());
                }
            }
            this.dgvList.DataSource = dt;

            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvList.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // フォントを設定する
            this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F, FontStyle.Regular);
            this.dgvList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.dgvList.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F);

            // 列幅を設定する
            this.dgvList.Columns[0].Width = 110;
            this.dgvList.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvList.Columns[1].Width = 200;
            this.dgvList.Columns[2].Width = 200;
        }

        /// <summary>
        /// 部課を追加編集する
        /// </summary>
        /// <param name="code">部課コード(空：新規登録、以外：編集)</param>
        private void EditBuka(string[] code)
        {
            KYCM1042 frm;

            if (ValChk.IsEmpty(code[1]))
            {
                // 新規登録モードで登録画面を起動
                frm = new KYCM1042("1");
                frm.InData = code;
            }
            else
            {
                // 編集モードで登録画面を起動
                frm = new KYCM1042("2");
                frm.InData = code;
            }

            DialogResult result = frm.ShowDialog(this);

            if (result == DialogResult.OK)
            {
                // データを再検索する
                SearchData(false);
                // 元々選択していたデータを選択
                for (int i = 0; i < this.dgvList.Rows.Count; i++)
                {
                    if (code[1].Equals(Util.ToString(this.dgvList.Rows[i].Cells["部課コード"].Value)))
                    {
                        this.dgvList.Rows[i].Selected = true;
                        break;
                    }
                }
                // Gridに再度フォーカスをセット
                this.ActiveControl = this.dgvList;
                this.dgvList.Focus();
            }
        }

        /// <summary>
        /// 呼び出し元に戻り値を返す
        /// </summary>
        private void ReturnVal()
        {
            this.OutData = new string[3] { 
                Util.ToString(this.dgvList.SelectedRows[0].Cells["部課コード"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["部課名"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["部課カナ名"].Value)
            };
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// 部門コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidBumonCd()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(Util.ToString(this.txtBumonCd.Text)))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 存在しないコードを入力されたらエラー
            if (this.txtBumonCd.Text != "0")
            {
                string name = this.Dba.GetName(this.UInfo, "TB_KY_BUMON", " ", Util.ToString(this.txtBumonCd.Text));
                if (ValChk.IsEmpty(name))
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }
            }
            else if (this.txtBumonCd.Text == "0" || this.txtBumonCd.Text == "")
            {
                this.lblBumonNm.Text = "";
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }
        #endregion
    }
}
