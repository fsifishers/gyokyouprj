﻿namespace jp.co.fsi.ky.kycm1041
{
    partial class KYCM1042
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtBukaCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblBukaCd = new System.Windows.Forms.Label();
            this.lblMODE = new System.Windows.Forms.Label();
            this.txtBukaKanaNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblBukaKanaNm = new System.Windows.Forms.Label();
            this.txtBukaNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblBukaNm = new System.Windows.Forms.Label();
            this.lblBumon = new System.Windows.Forms.Label();
            this.lblBumonCd = new System.Windows.Forms.Label();
            this.lblBumonNm = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(373, 23);
            this.lblTitle.Text = "部課の登録";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(5, 114);
            this.pnlDebug.Size = new System.Drawing.Size(406, 100);
            // 
            // txtBukaCd
            // 
            this.txtBukaCd.AutoSizeFromLength = true;
            this.txtBukaCd.DisplayLength = null;
            this.txtBukaCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtBukaCd.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtBukaCd.Location = new System.Drawing.Point(116, 13);
            this.txtBukaCd.MaxLength = 4;
            this.txtBukaCd.Name = "txtBukaCd";
            this.txtBukaCd.Size = new System.Drawing.Size(64, 23);
            this.txtBukaCd.TabIndex = 0;
            this.txtBukaCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBukaCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtBukaCd_Validating);
            // 
            // lblBukaCd
            // 
            this.lblBukaCd.BackColor = System.Drawing.Color.Silver;
            this.lblBukaCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBukaCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBukaCd.Location = new System.Drawing.Point(12, 13);
            this.lblBukaCd.Name = "lblBukaCd";
            this.lblBukaCd.Size = new System.Drawing.Size(115, 23);
            this.lblBukaCd.TabIndex = 1;
            this.lblBukaCd.Text = "部課コード";
            this.lblBukaCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMODE
            // 
            this.lblMODE.BackColor = System.Drawing.Color.White;
            this.lblMODE.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMODE.Location = new System.Drawing.Point(315, 13);
            this.lblMODE.Name = "lblMODE";
            this.lblMODE.Size = new System.Drawing.Size(78, 23);
            this.lblMODE.TabIndex = 902;
            this.lblMODE.Text = "【編集】";
            this.lblMODE.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtBukaKanaNm
            // 
            this.txtBukaKanaNm.AutoSizeFromLength = false;
            this.txtBukaKanaNm.DisplayLength = null;
            this.txtBukaKanaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtBukaKanaNm.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtBukaKanaNm.Location = new System.Drawing.Point(124, 129);
            this.txtBukaKanaNm.MaxLength = 30;
            this.txtBukaKanaNm.Name = "txtBukaKanaNm";
            this.txtBukaKanaNm.Size = new System.Drawing.Size(269, 23);
            this.txtBukaKanaNm.TabIndex = 911;
            this.txtBukaKanaNm.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LastControl_KeyDown);
            this.txtBukaKanaNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtBukaKanaNm_Validating);
            // 
            // lblBukaKanaNm
            // 
            this.lblBukaKanaNm.BackColor = System.Drawing.Color.Silver;
            this.lblBukaKanaNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBukaKanaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBukaKanaNm.Location = new System.Drawing.Point(11, 129);
            this.lblBukaKanaNm.Name = "lblBukaKanaNm";
            this.lblBukaKanaNm.Size = new System.Drawing.Size(115, 23);
            this.lblBukaKanaNm.TabIndex = 912;
            this.lblBukaKanaNm.Text = "カナ名";
            this.lblBukaKanaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtBukaNm
            // 
            this.txtBukaNm.AutoSizeFromLength = false;
            this.txtBukaNm.DisplayLength = null;
            this.txtBukaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtBukaNm.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtBukaNm.Location = new System.Drawing.Point(124, 91);
            this.txtBukaNm.MaxLength = 30;
            this.txtBukaNm.Name = "txtBukaNm";
            this.txtBukaNm.Size = new System.Drawing.Size(269, 23);
            this.txtBukaNm.TabIndex = 909;
            this.txtBukaNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtBukaNm_Validating);
            // 
            // lblBukaNm
            // 
            this.lblBukaNm.BackColor = System.Drawing.Color.Silver;
            this.lblBukaNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBukaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBukaNm.Location = new System.Drawing.Point(12, 91);
            this.lblBukaNm.Name = "lblBukaNm";
            this.lblBukaNm.Size = new System.Drawing.Size(115, 23);
            this.lblBukaNm.TabIndex = 910;
            this.lblBukaNm.Text = "部課名";
            this.lblBukaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblBumon
            // 
            this.lblBumon.BackColor = System.Drawing.Color.LightGray;
            this.lblBumon.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBumon.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBumon.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.lblBumon.Location = new System.Drawing.Point(12, 51);
            this.lblBumon.Name = "lblBumon";
            this.lblBumon.Size = new System.Drawing.Size(84, 23);
            this.lblBumon.TabIndex = 915;
            this.lblBumon.Text = "部門";
            this.lblBumon.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblBumonCd
            // 
            this.lblBumonCd.BackColor = System.Drawing.Color.LightGray;
            this.lblBumonCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBumonCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBumonCd.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.lblBumonCd.Location = new System.Drawing.Point(96, 51);
            this.lblBumonCd.Name = "lblBumonCd";
            this.lblBumonCd.Size = new System.Drawing.Size(31, 23);
            this.lblBumonCd.TabIndex = 914;
            this.lblBumonCd.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblBumonNm
            // 
            this.lblBumonNm.BackColor = System.Drawing.Color.LightGray;
            this.lblBumonNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBumonNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBumonNm.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.lblBumonNm.Location = new System.Drawing.Point(124, 51);
            this.lblBumonNm.Name = "lblBumonNm";
            this.lblBumonNm.Size = new System.Drawing.Size(269, 23);
            this.lblBumonNm.TabIndex = 913;
            this.lblBumonNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // KYCM1042
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(398, 217);
            this.Controls.Add(this.lblBumon);
            this.Controls.Add(this.lblBumonCd);
            this.Controls.Add(this.lblBumonNm);
            this.Controls.Add(this.txtBukaKanaNm);
            this.Controls.Add(this.lblBukaKanaNm);
            this.Controls.Add(this.txtBukaNm);
            this.Controls.Add(this.lblBukaNm);
            this.Controls.Add(this.lblMODE);
            this.Controls.Add(this.txtBukaCd);
            this.Controls.Add(this.lblBukaCd);
            this.Name = "KYCM1042";
            this.Text = "部課の登録";
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblBukaCd, 0);
            this.Controls.SetChildIndex(this.txtBukaCd, 0);
            this.Controls.SetChildIndex(this.lblMODE, 0);
            this.Controls.SetChildIndex(this.lblBukaNm, 0);
            this.Controls.SetChildIndex(this.txtBukaNm, 0);
            this.Controls.SetChildIndex(this.lblBukaKanaNm, 0);
            this.Controls.SetChildIndex(this.txtBukaKanaNm, 0);
            this.Controls.SetChildIndex(this.lblBumonNm, 0);
            this.Controls.SetChildIndex(this.lblBumonCd, 0);
            this.Controls.SetChildIndex(this.lblBumon, 0);
            this.pnlDebug.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private jp.co.fsi.common.controls.FsiTextBox txtBukaCd;
        private System.Windows.Forms.Label lblBukaCd;
        private System.Windows.Forms.Label lblMODE;
        private common.controls.FsiTextBox txtBukaKanaNm;
        private System.Windows.Forms.Label lblBukaKanaNm;
        private common.controls.FsiTextBox txtBukaNm;
        private System.Windows.Forms.Label lblBukaNm;
        private System.Windows.Forms.Label lblBumon;
        private System.Windows.Forms.Label lblBumonCd;
        private System.Windows.Forms.Label lblBumonNm;

    }
}