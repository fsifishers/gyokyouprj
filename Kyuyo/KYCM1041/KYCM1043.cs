﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.ky.kycm1041
{
    /// <summary>
    /// 部課の印刷(KYCM1043)
    /// </summary>
    public partial class KYCM1043 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// データ取得用
        /// </summary>
        private const int MAX_LINE_NO = 50;
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KYCM1043()
        {
            InitializeComponent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // タイトルは非表示
            this.lblTitle.Visible = false;

            // ボタンを設定
            this.ShowFButton = true;
            this.btnEsc.Visible = true;
            this.btnF1.Visible = true;
            this.btnF2.Visible = false;
            this.btnF3.Visible = false;
            this.btnF4.Visible = true;
            this.btnF5.Visible = true;
            this.btnF6.Visible = false;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF1.Location = this.btnF2.Location;
            this.btnF5.Location = this.btnF4.Location;
            this.btnF4.Location = this.btnF3.Location;

            // 初期フォーカスを設定
            this.txtBumonCdFr.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            switch (this.ActiveCtlNm)
            {
                // 部門コード(自)・(至)の場合のみ有効にする
                case "txtBumonCdFr":
                case "txtBumonCdTo":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理 検索
        /// </summary>
        public override void PressF1()
        {
            Assembly asm;
            Type t;

            switch (this.ActiveControl.Name)
            {
                // 部門コード(自)・(至)の場合、部門の検索画面を立ち上げる
                case "txtBumonCdFr":
                case "txtBumonCdTo":
                    #region 部門検索
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("KYCM1031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.ky.kycm1031.KYCM1031");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            if (this.ActiveControl.Name == "txtBumonCdFr")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.Par1 = "1";
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.txtBumonCdFr.Text = result[0];
                                    this.lblBumonCdFr.Text = result[1];
                                }
                            }
                            else if (this.ActiveControl.Name == "txtBumonCdTo")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.Par1 = "1";
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.txtBumonCdTo.Text = result[0];
                                    this.lblBumonCdTo.Text = result[1];
                                }
                            }
                        }
                    }
                    #endregion
                    break;   

                default:
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理 
        /// </summary>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfYesNo("実行しますか？") == DialogResult.Yes)
            {
                // プレビュー処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理 
        /// </summary>
        public override void PressF5()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfYesNo("実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }
        #endregion

        #region イベント
        /// <summary>
        /// 部門コード(自)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBumonCdFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidBumonCdFr())
            {
                e.Cancel = true;
                this.txtBumonCdFr.SelectAll();
            }
        }

        /// <summary>
        /// 部門コード(至)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBumonCdTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidBumonCdTo())
            {
                e.Cancel = true;
                this.txtBumonCdTo.SelectAll();
            }
        }

        /// <summary>
        /// 画面の最後のコントールでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LastControl_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.btnF6.Enabled)
            {
                this.PressF4();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 部門コード(自)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidBumonCdFr()
        {
            // 未入力の場合、「先　頭」を表示
            if (ValChk.IsEmpty(this.txtBumonCdFr.Text))
            {
                this.lblBumonCdFr.Text = "先　頭";
            }
            // 数字のみの入力を許可
            else if (!ValChk.IsNumber(this.txtBumonCdFr.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 存在するコードの場合はラベルに名称を表示する
            else
            {
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                dpc.SetParam("@BUMON_CD", SqlDbType.Decimal, 4, this.txtBumonCdFr.Text);
                StringBuilder where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
                where.Append(" AND BUMON_CD = @BUMON_CD");
                DataTable dtBumonDate =
                    this.Dba.GetDataTableByConditionWithParams("BUMON_NM",
                        "TB_KY_BUMON", Util.ToString(where), dpc);
                if (dtBumonDate.Rows.Count > 0)
                {
                    this.lblBumonCdFr.Text = dtBumonDate.Rows[0]["BUMON_NM"].ToString();
                }
                else
                {
                    this.lblBumonCdFr.Text = "";
                }
            }

            return true;
        }

        /// <summary>
        /// 部門コード(至)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidBumonCdTo()
        {
            // 未入力の場合「最　後」を表示
            if (ValChk.IsEmpty(this.txtBumonCdTo.Text))
            {
                this.lblBumonCdTo.Text = "最　後";
            }
            // 数字のみの入力を許可
            else if (!ValChk.IsNumber(this.txtBumonCdTo.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 存在するコードの場合はラベルに名称を表示する
            else
            {
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                dpc.SetParam("@BUMON_CD", SqlDbType.Decimal, 4, this.txtBumonCdTo.Text);
                StringBuilder where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
                where.Append(" AND BUMON_CD = @BUMON_CD");
                DataTable dtBumonDate =
                    this.Dba.GetDataTableByConditionWithParams("BUMON_NM",
                        "TB_KY_BUMON", Util.ToString(where), dpc);
                if (dtBumonDate.Rows.Count > 0)
                {
                    this.lblBumonCdTo.Text = dtBumonDate.Rows[0]["BUMON_NM"].ToString();
                }
                else
                {
                    this.lblBumonCdTo.Text = "";
                }
            }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 部門コード(自)のチェック
            if (!IsValidBumonCdFr())
            {
                this.txtBumonCdFr.Focus();
                return false;
            }

            // 部門コード(至)のチェック
            if (!IsValidBumonCdTo())
            {
                this.txtBumonCdTo.Focus();
                return false;
            }

            return true;
        }

        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview)
        {
            // 現状仮の状態
            try
            {
                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                bool dataFlag = MakeWkData();

                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    StringBuilder cols = new StringBuilder();
                    cols.Append("  ITEM001");
                    cols.Append(" ,ITEM002");
                    cols.Append(" ,ITEM003");
                    cols.Append(" ,ITEM004");
                    cols.Append(" ,ITEM005");
                    cols.Append(" ,ITEM006");

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                    // データの取得
                    DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_KY_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    // 帳票オブジェクトをインスタンス化
                    KYCM1041R rpt = new KYCM1041R(dtOutput);

                    if (isPreview)
                    {
                        // プレビュー画面表示
                        PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                        pFrm.WindowState = FormWindowState.Maximized;
                        pFrm.Show();
                    }
                    else
                    {
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
            }
            finally
            {
                this.Dba.Rollback();
            }
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {
            #region 設定値をセットする
            // 部門コードを取得
            string BumonCdFr;
            string BumonCdTo;
            if (Util.ToString(this.txtBumonCdFr.Text) != "")
            {
                BumonCdFr = this.txtBumonCdFr.Text;
            }
            else
            {
                BumonCdFr = "0";
            }
            if (Util.ToString(this.txtBumonCdTo.Text) != "")
            {
                BumonCdTo = this.txtBumonCdTo.Text;
            }
            else
            {
                BumonCdTo = "9999";
            }
            #endregion

            #region データを取得する
            // 現状仮のSQL。
            StringBuilder Sql = new StringBuilder();
            Sql.Append("SELECT");
            Sql.Append(" A.BUMON_CD,");
            Sql.Append(" B.BUMON_NM,");
            Sql.Append(" B.BUMON_KANA_NM,");
            Sql.Append(" A.BUKA_CD,");
            Sql.Append(" A.BUKA_NM,");
            Sql.Append(" A.BUKA_KANA_NM ");
            Sql.Append("FROM");
            Sql.Append(" TB_KY_BUKA AS A ");
            Sql.Append("LEFT OUTER JOIN");
            Sql.Append(" TB_KY_BUMON AS B ");
            Sql.Append("ON A.KAISHA_CD = B.KAISHA_CD");
            Sql.Append(" AND A.BUMON_CD = B.BUMON_CD ");
            Sql.Append("WHERE");
            Sql.Append(" A.KAISHA_CD = @KAISHA_CD");
            Sql.Append(" AND A.BUMON_CD BETWEEN @BUMON_CD_FR AND @BUMON_CD_TO ");
            Sql.Append("ORDER BY");
            Sql.Append(" A.KAISHA_CD ASC,");
            Sql.Append(" A.BUMON_CD ASC");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd); // 会社コード
            dpc.SetParam("@BUMON_CD_FR", SqlDbType.Decimal, 4, BumonCdFr); // 部門コードFr
            dpc.SetParam("@BUMON_CD_TO", SqlDbType.Decimal, 4, BumonCdTo); // 部門コードTo

            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            #endregion

            int i = 0; // ソート番号用変数
            if (dtMainLoop.Rows.Count == 0)
            {
                Msg.Info("該当データがありません。");
                return false;
            }
            else
            {
                #region 印刷ワークテーブルに登録
                //前回の部門コードを保持する用の変数
                int bumonCdTmp = -1;
                string bumonCd;
                string bumonNm;

                foreach (DataRow dr in dtMainLoop.Rows)
                {
                    if (bumonCdTmp != int.Parse(dr["BUMON_CD"].ToString()))
                    {
                        bumonCd = dr["BUMON_CD"].ToString();
                        bumonNm = dr["BUMON_NM"].ToString();
                        bumonCdTmp = int.Parse(dr["BUMON_CD"].ToString());
                    }
                    else
                    {
                        bumonCd = "";
                        bumonNm = "";
                    }

                    Sql = new StringBuilder();
                    dpc = new DbParamCollection();
                    Sql.Append("INSERT INTO PR_KY_TBL(");
                    Sql.Append(" GUID");
                    Sql.Append(" ,SORT");
                    Sql.Append(" ,ITEM001");
                    Sql.Append(" ,ITEM002");
                    Sql.Append(" ,ITEM003");
                    Sql.Append(" ,ITEM004");
                    Sql.Append(" ,ITEM005");
                    Sql.Append(" ,ITEM006");
                    Sql.Append(") ");
                    Sql.Append("VALUES(");
                    Sql.Append("  @GUID");
                    Sql.Append(" ,@SORT");
                    Sql.Append(" ,@ITEM001");
                    Sql.Append(" ,@ITEM002");
                    Sql.Append(" ,@ITEM003");
                    Sql.Append(" ,@ITEM004");
                    Sql.Append(" ,@ITEM005");
                    Sql.Append(" ,@ITEM006");
                    Sql.Append(") ");
                    // データを設定
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
                    dpc.SetParam("@ITEM001", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                    dpc.SetParam("@ITEM002", SqlDbType.VarChar, 200, bumonCd); // 部門ｺｰﾄﾞ
                    dpc.SetParam("@ITEM003", SqlDbType.VarChar, 200, bumonNm); // 部門名
                    dpc.SetParam("@ITEM004", SqlDbType.VarChar, 200, dr["BUKA_CD"].ToString()); // ｺｰﾄﾞ
                    dpc.SetParam("@ITEM005", SqlDbType.VarChar, 200, dr["BUKA_NM"].ToString()); // 部課名
                    dpc.SetParam("@ITEM006", SqlDbType.VarChar, 200, dr["BUKA_KANA_NM"].ToString()); // 部課カナ名
                    // データを登録
                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                    i++;
                }
                #endregion

                if (MAX_LINE_NO == 0)
                {
                #region 印刷ワークテーブルに空行登録
                // 現在のページの行を計算する
                int Amari = i % MAX_LINE_NO;
                for (int j = Amari; j < MAX_LINE_NO; j++)
                {
                    Sql = new StringBuilder();
                    dpc = new DbParamCollection();
                    Sql.Append("INSERT INTO PR_KY_TBL(");
                    Sql.Append(" GUID");
                    Sql.Append(" ,SORT");
                    Sql.Append(" ,ITEM001");
                    Sql.Append(") ");
                    Sql.Append("VALUES(");
                    Sql.Append("  @GUID");
                    Sql.Append(" ,@SORT");
                    Sql.Append(" ,@ITEM001");
                    Sql.Append(") ");
                    // データを設定
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
                    dpc.SetParam("@ITEM001", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                    // データを登録
                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                    i++;
                }
                    #endregion
                }

                // 印刷ワークテーブルのデータ件数を取得
                dpc = new DbParamCollection();
                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                DataTable tmpdtPR_KY_TBL = this.Dba.GetDataTableByConditionWithParams(
                    "SORT",
                    "PR_KY_TBL",
                    "GUID = @GUID",
                    dpc);

                bool dataFlag;
                if (tmpdtPR_KY_TBL.Rows.Count > 0)
                {
                    dataFlag = true;
                }
                else
                {
                    dataFlag = false;
                }

                return dataFlag;
            }
        }
        #endregion

    }
}
