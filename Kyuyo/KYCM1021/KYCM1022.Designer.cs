﻿namespace jp.co.fsi.ky.kycm1021
{
    partial class KYCM1022
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtShainCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShainCd = new System.Windows.Forms.Label();
            this.lblMODE = new System.Windows.Forms.Label();
            this.lblShainNmDisp = new System.Windows.Forms.Label();
            this.tabPages = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.pnlPage1 = new jp.co.fsi.common.FsiPanel();
            this.label99 = new System.Windows.Forms.Label();
            this.txtZenBikou = new jp.co.fsi.common.controls.FsiTextBox();
            this.txthyoji = new jp.co.fsi.common.controls.FsiTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.FsiTextBox1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtMyNumber = new jp.co.fsi.common.controls.FsiTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlTaishokuNengappi = new jp.co.fsi.common.FsiPanel();
            this.lblDayTaishokuNengappi = new System.Windows.Forms.Label();
            this.txtDayTaishokuNengappi = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMonthTaishokuNengappi = new System.Windows.Forms.Label();
            this.lblGengoYearTaishokuNengappi = new System.Windows.Forms.Label();
            this.txtMonthTaishokuNengappi = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGengoTaishokuNengappi = new System.Windows.Forms.Label();
            this.txtGengoYearTaishokuNengappi = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblTaishokuNengappi = new System.Windows.Forms.Label();
            this.pnlNyushaNengappi = new jp.co.fsi.common.FsiPanel();
            this.lblDayNyushaNengappi = new System.Windows.Forms.Label();
            this.txtDayNyushaNengappi = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMonthNyushaNengappi = new System.Windows.Forms.Label();
            this.lblGengoYearNyushaNengappi = new System.Windows.Forms.Label();
            this.txtMonthNyushaNengappi = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGengoNyushaNengappi = new System.Windows.Forms.Label();
            this.txtGengoYearNyushaNengappi = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblNyushaNengappi = new System.Windows.Forms.Label();
            this.lblKyuyoKeitaiNm = new System.Windows.Forms.Label();
            this.txtKyuyoKeitai = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKyuyoKeitai = new System.Windows.Forms.Label();
            this.lblYakushokuNm = new System.Windows.Forms.Label();
            this.txtYakushokuCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblYakushokuCd = new System.Windows.Forms.Label();
            this.lblBukaNm = new System.Windows.Forms.Label();
            this.txtBukaCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblBukaCd = new System.Windows.Forms.Label();
            this.lblBumonNm = new System.Windows.Forms.Label();
            this.txtBumonCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblBumonCd = new System.Windows.Forms.Label();
            this.txtDenwaBango = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtJusho2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtJusho1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblYubinBango1 = new System.Windows.Forms.Label();
            this.lblDenwaBango = new System.Windows.Forms.Label();
            this.lblJusho2 = new System.Windows.Forms.Label();
            this.lblJusho1 = new System.Windows.Forms.Label();
            this.lblSeibetsuNm = new System.Windows.Forms.Label();
            this.pnlSeinengappi = new jp.co.fsi.common.FsiPanel();
            this.lblDaySeinengappi = new System.Windows.Forms.Label();
            this.txtDaySeinengappi = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMonthSeinengappi = new System.Windows.Forms.Label();
            this.lblGengoYearSeinengappi = new System.Windows.Forms.Label();
            this.txtMonthSeinengappi = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGengoSeinengappi = new System.Windows.Forms.Label();
            this.txtGengoYearSeinengappi = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtYubinBango2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblYubinBango = new System.Windows.Forms.Label();
            this.txtSeibetsu = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblSeinengappi = new System.Windows.Forms.Label();
            this.txtYubinBango1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblSeibetsu = new System.Windows.Forms.Label();
            this.txtShainKanaNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShainKanaNm = new System.Windows.Forms.Label();
            this.txtShainNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShainNm = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.panel3 = new jp.co.fsi.common.FsiPanel();
            this.txtKoseiNenkinHokenryo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKoseiNenkinShoyoKeisanKbn = new System.Windows.Forms.Label();
            this.txtJuminzei7gatsubunIko = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblJuminzei7gatsubunIko = new System.Windows.Forms.Label();
            this.txtJuminzei6gatsubun = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblJuminzeiShichosonNm = new System.Windows.Forms.Label();
            this.txtJuminzeiShichosonCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKoyoHokenKeisanKubunNm = new System.Windows.Forms.Label();
            this.txtKoyoHokenKeisanKubun = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKoseiNenkinHyojunHoshuGtgkNm = new System.Windows.Forms.Label();
            this.lblKenkoHokenHyojunHoshuGtgkNm = new System.Windows.Forms.Label();
            this.lblKoseiNenkinShoyoKeisanKbnNm = new System.Windows.Forms.Label();
            this.KaigoHokenKubunNm = new System.Windows.Forms.Label();
            this.txtKaigoHokenKubun = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKaigoHokenKubun = new System.Windows.Forms.Label();
            this.lblKenkoHokenShoyoKeisanKubunNm = new System.Windows.Forms.Label();
            this.txtKenkoHokenShoyoKeisanKubun = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKenkoHokenShoyoKeisanKubun = new System.Windows.Forms.Label();
            this.lblKoseiNenkin = new System.Windows.Forms.Label();
            this.lblKoyohoken = new System.Windows.Forms.Label();
            this.lblKenkohoken = new System.Windows.Forms.Label();
            this.lblJuminzei6gatsubun = new System.Windows.Forms.Label();
            this.lblJuminzeiShichosonCd = new System.Windows.Forms.Label();
            this.lblKoyoHokenKeisanKubun = new System.Windows.Forms.Label();
            this.txtKoseiNenkinShoyoKeisanKbn = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKoseiNenkinHyojunHoshuGtgk = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKoseiNenkinBango = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKoseiNenkinHokenryo = new System.Windows.Forms.Label();
            this.lblKoseiNenkinHyojunHoshuGtgk = new System.Windows.Forms.Label();
            this.lblKoseiNenkinBango = new System.Windows.Forms.Label();
            this.txtKenkoHokenryo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKenkoHokenryo = new System.Windows.Forms.Label();
            this.txtKenkoHokenHyojunHoshuGtgk = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKenkoHokenHyojunHoshuGtgk = new System.Windows.Forms.Label();
            this.txtKenkoHokenshoBango = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKenkoHokenshoBango = new System.Windows.Forms.Label();
            this.lblJuminzei = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.panel7 = new jp.co.fsi.common.FsiPanel();
            this.label93 = new System.Windows.Forms.Label();
            this.txtHiJukyoShinzoku = new jp.co.fsi.common.controls.FsiTextBox();
            this.label94 = new System.Windows.Forms.Label();
            this.lblFuyoHaigushaJukyo = new System.Windows.Forms.Label();
            this.txtFuyoHaigushaNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblFuyoHaigushaNm = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.txtFuyoHaigushaJukyo = new jp.co.fsi.common.controls.FsiTextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.txtFuyoHaigushaMyNumberDm = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtFuyoHaigushaMyNumber = new jp.co.fsi.common.controls.FsiTextBox();
            this.label102 = new System.Windows.Forms.Label();
            this.pnlFuyoHaigushaSeinengappi = new jp.co.fsi.common.FsiPanel();
            this.lblFuyoHaigushaDaySeinengappi = new System.Windows.Forms.Label();
            this.txtFuyoHaigushaDaySeinengappi = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblFuyoHaigushaMonthSeinengappi = new System.Windows.Forms.Label();
            this.lblFuyoHaigushaGengoYearSeinengappi = new System.Windows.Forms.Label();
            this.txtFuyoHaigushaMonthSeinengappi = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblFuyoHaigushaGengoSeinengappi = new System.Windows.Forms.Label();
            this.txtFuyoHaigushaGengoYearSeinengappi = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblFuyoHaigushaSeinengappi = new System.Windows.Forms.Label();
            this.txtFuyoHaigushaKanaNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblFuyoHaigushaKanaNm = new System.Windows.Forms.Label();
            this.label89 = new System.Windows.Forms.Label();
            this.lblShogaishaTokubetsuNm = new System.Windows.Forms.Label();
            this.txtShogaishaTokubetsu = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShogaishaTokubetsu = new System.Windows.Forms.Label();
            this.lblShogaisha = new System.Windows.Forms.Label();
            this.lblFuyo = new System.Windows.Forms.Label();
            this.txtKyuyoShubetsu = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtTekiyo2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtTekiyo1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKyuyoShubetsu = new System.Windows.Forms.Label();
            this.lblTekiyo2 = new System.Windows.Forms.Label();
            this.lblTekiyo1 = new System.Windows.Forms.Label();
            this.Sai16MimanNm = new System.Windows.Forms.Label();
            this.txtSai16Miman = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblSai16Miman = new System.Windows.Forms.Label();
            this.lblHaigushaKubunNm = new System.Windows.Forms.Label();
            this.txtHaigushaKubun = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblHaigushaKubun = new System.Windows.Forms.Label();
            this.lblGaikokujinNm = new System.Windows.Forms.Label();
            this.txtGaikokujin = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGaikokujin = new System.Windows.Forms.Label();
            this.lblSaigaishaNm = new System.Windows.Forms.Label();
            this.txtSaigaisha = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblSaigaisha = new System.Windows.Forms.Label();
            this.lblShiboTaishokuNm = new System.Windows.Forms.Label();
            this.txtShiboTaishoku = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShiboTaishoku = new System.Windows.Forms.Label();
            this.lblMiseinenshaNm = new System.Windows.Forms.Label();
            this.txtMiseinensha = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMiseinensha = new System.Windows.Forms.Label();
            this.lblHonninKubun3Nm = new System.Windows.Forms.Label();
            this.txtHonninKubun3 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblHonninKubun3 = new System.Windows.Forms.Label();
            this.lblHonninKubun2Nm = new System.Windows.Forms.Label();
            this.txtHonninKubun2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblHonninKubun2 = new System.Windows.Forms.Label();
            this.ShogaishaIppanNm = new System.Windows.Forms.Label();
            this.txtShogaishaIppan = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShogaishaIppan = new System.Windows.Forms.Label();
            this.lblFuyoDokyoTokushoIppanNm = new System.Windows.Forms.Label();
            this.txtFuyoDokyoTokushoIppan = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblFuyoDokyoTokushoIppan = new System.Windows.Forms.Label();
            this.lblFuyoRojin = new System.Windows.Forms.Label();
            this.lblNenmatsuChoseiNm = new System.Windows.Forms.Label();
            this.lblZeigakuHyoNm = new System.Windows.Forms.Label();
            this.txtNenmatsuChosei = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblFuyoRojinDokyoRoshintoNm = new System.Windows.Forms.Label();
            this.txtFuyoRojinDokyoRoshinto = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblFuyoRojinDokyoRoshinto = new System.Windows.Forms.Label();
            this.lblFuyoRojinDokyoRoshintoIgaiNm = new System.Windows.Forms.Label();
            this.txtFuyoRojinDokyoRoshintoIgai = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblFuyoRojinDokyoRoshintoIgai = new System.Windows.Forms.Label();
            this.lblFuyoTokuteiNm = new System.Windows.Forms.Label();
            this.txtFuyoTokutei = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblFuyoTokutei = new System.Windows.Forms.Label();
            this.lblFuyoIppanNm = new System.Windows.Forms.Label();
            this.txtFuyoIppan = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblFuyoIppan = new System.Windows.Forms.Label();
            this.lblHonninKubun1Nm = new System.Windows.Forms.Label();
            this.txtHonninKubun1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblHonninKubun1 = new System.Windows.Forms.Label();
            this.lblNenmatsuChosei = new System.Windows.Forms.Label();
            this.txtZeigakuHyo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblZeigakuHyo = new System.Windows.Forms.Label();
            this.lblGensenchoshuhyo = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.panel11 = new jp.co.fsi.common.FsiPanel();
            this.lblKojoFootD1 = new System.Windows.Forms.Label();
            this.lblShikyuFootD1 = new System.Windows.Forms.Label();
            this.lblKojoGokei = new System.Windows.Forms.Label();
            this.lblKojoGokeiFoot = new System.Windows.Forms.Label();
            this.lblKojo = new System.Windows.Forms.Label();
            this.lblShikyu = new System.Windows.Forms.Label();
            this.lblShikyuGokei = new System.Windows.Forms.Label();
            this.lblShikyuFoot = new System.Windows.Forms.Label();
            this.dgvShikyu = new System.Windows.Forms.DataGridView();
            this.dgvKojo = new System.Windows.Forms.DataGridView();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.panel15 = new jp.co.fsi.common.FsiPanel();
            this.lblKyuyoYokinShumoku1Nm = new System.Windows.Forms.Label();
            this.txtKyuyoYokinShumoku1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKyuyoShitenNm1 = new System.Windows.Forms.Label();
            this.txtKyuyoShitenCd1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKyuyoGinkoNm1 = new System.Windows.Forms.Label();
            this.txtKyuyoGinkoCd1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKyuyoKeiyakushaBango1 = new System.Windows.Forms.Label();
            this.lblKyuyoKozaMeigininKana1 = new System.Windows.Forms.Label();
            this.lblKyuyoKozaMeigininKanji1 = new System.Windows.Forms.Label();
            this.lblKyuyoKozaBango1 = new System.Windows.Forms.Label();
            this.lblKyuyoYokinShumoku1 = new System.Windows.Forms.Label();
            this.lblKyuyoShitenCd1 = new System.Windows.Forms.Label();
            this.lblKyuyoGinkoCd1 = new System.Windows.Forms.Label();
            this.txtKyuyoShikyuKingaku1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKyuyoShikyuKingaku1 = new System.Windows.Forms.Label();
            this.txtKyuyoShikyuRitsu1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKyuyoShikyuRitsu1 = new System.Windows.Forms.Label();
            this.lblKyuyoShikyuHoho1Nm = new System.Windows.Forms.Label();
            this.txtKyuyoShikyuHoho1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKyuyoShikyuHoho1 = new System.Windows.Forms.Label();
            this.txtKyuyoKozaBango1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKyuyoKozaMeigininKana1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKyuyoKozaMeigininKanji1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKyuyoKeiyakushaBango1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKyuyoYokinShumoku2Nm = new System.Windows.Forms.Label();
            this.txtKyuyoYokinShumoku2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKyuyoShitenNm2 = new System.Windows.Forms.Label();
            this.txtKyuyoShitenCd2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKyuyoGinkoNm2 = new System.Windows.Forms.Label();
            this.txtKyuyoGinkoCd2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKyuyoKeiyakushaBango2 = new System.Windows.Forms.Label();
            this.lblKyuyoKozaMeigininKana2 = new System.Windows.Forms.Label();
            this.lblKyuyoKozaMeigininKanji2 = new System.Windows.Forms.Label();
            this.lblKyuyoKozaBango2 = new System.Windows.Forms.Label();
            this.lblKyuyoYokinShumoku2 = new System.Windows.Forms.Label();
            this.lblKyuyoShitenCd2 = new System.Windows.Forms.Label();
            this.lblShikyuHoho2 = new System.Windows.Forms.Label();
            this.lblShikyuHoho1 = new System.Windows.Forms.Label();
            this.lblKyuyoGinkoCd2 = new System.Windows.Forms.Label();
            this.txtKyuyoShikyuKingaku2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKyuyoShikyuKingaku2 = new System.Windows.Forms.Label();
            this.txtKyuyoShikyuRitsu2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKyuyoShikyuRitsu2 = new System.Windows.Forms.Label();
            this.lblKyuyoShikyuHoho2Nm = new System.Windows.Forms.Label();
            this.txtKyuyoShikyuHoho2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKyuyoShikyuHoho2 = new System.Windows.Forms.Label();
            this.txtKyuyoKozaBango2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKyuyoKozaMeigininKana2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKyuyoKozaMeigininKanji2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKyuyoKeiyakushaBango2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.panel1 = new jp.co.fsi.common.FsiPanel();
            this.txtIkouNo = new jp.co.fsi.common.controls.FsiTextBox();
            this.label90 = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.txtFuyoNm4 = new jp.co.fsi.common.controls.FsiTextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.txtFuyoJukyo4 = new jp.co.fsi.common.controls.FsiTextBox();
            this.button6 = new System.Windows.Forms.Button();
            this.txtFuyoMyNumberDm4 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtFuyoMyNumber4 = new jp.co.fsi.common.controls.FsiTextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.panel6 = new jp.co.fsi.common.FsiPanel();
            this.label39 = new System.Windows.Forms.Label();
            this.txtFuyoDaySeinengappi4 = new jp.co.fsi.common.controls.FsiTextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.txtFuyoMonthSeinengappi4 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblFuyoGengoSeinengappi4 = new System.Windows.Forms.Label();
            this.txtFuyoGengoYearSeinengappi4 = new jp.co.fsi.common.controls.FsiTextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.txtFuyoKanaNm4 = new jp.co.fsi.common.controls.FsiTextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.txtFuyoNm3 = new jp.co.fsi.common.controls.FsiTextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.txtFuyoJukyo3 = new jp.co.fsi.common.controls.FsiTextBox();
            this.button5 = new System.Windows.Forms.Button();
            this.txtFuyoMyNumberDm3 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtFuyoMyNumber3 = new jp.co.fsi.common.controls.FsiTextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.panel5 = new jp.co.fsi.common.FsiPanel();
            this.label29 = new System.Windows.Forms.Label();
            this.txtFuyoDaySeinengappi3 = new jp.co.fsi.common.controls.FsiTextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.txtFuyoMonthSeinengappi3 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblFuyoGengoSeinengappi3 = new System.Windows.Forms.Label();
            this.txtFuyoGengoYearSeinengappi3 = new jp.co.fsi.common.controls.FsiTextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.txtFuyoKanaNm3 = new jp.co.fsi.common.controls.FsiTextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtFuyoNm2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.txtFuyoJukyo2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.button4 = new System.Windows.Forms.Button();
            this.txtFuyoMyNumberDm2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtFuyoMyNumber2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.panel4 = new jp.co.fsi.common.FsiPanel();
            this.label19 = new System.Windows.Forms.Label();
            this.txtFuyoDaySeinengappi2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.txtFuyoMonthSeinengappi2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblFuyoGengoSeinengappi2 = new System.Windows.Forms.Label();
            this.txtFuyoGengoYearSeinengappi2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtFuyoKanaNm2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtFuyoNm1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtFuyoJukyo1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.txtFuyoMyNumberDm1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtFuyoMyNumber1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.panel2 = new jp.co.fsi.common.FsiPanel();
            this.label9 = new System.Windows.Forms.Label();
            this.txtFuyoDaySeinengappi1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtFuyoMonthSeinengappi1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblFuyoGengoSeinengappi1 = new System.Windows.Forms.Label();
            this.txtFuyoGengoYearSeinengappi1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtFuyoKanaNm1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.panel8 = new jp.co.fsi.common.FsiPanel();
            this.panel9 = new jp.co.fsi.common.FsiPanel();
            this.txtIkouMimanNo = new jp.co.fsi.common.controls.FsiTextBox();
            this.label85 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.txtFuyoMimanNm4 = new jp.co.fsi.common.controls.FsiTextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.txtFuyoMimanJukyo4 = new jp.co.fsi.common.controls.FsiTextBox();
            this.button10 = new System.Windows.Forms.Button();
            this.txtFuyoMimanMyNumberDm4 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtFuyoMimanMyNumber4 = new jp.co.fsi.common.controls.FsiTextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.panel10 = new jp.co.fsi.common.FsiPanel();
            this.label49 = new System.Windows.Forms.Label();
            this.txtFuyoMimanDaySeinengappi4 = new jp.co.fsi.common.controls.FsiTextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.txtFuyoMimanMonthSeinengappi4 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblFuyoMimanGengoSeinengappi4 = new System.Windows.Forms.Label();
            this.txtFuyoMimanGengoYearSeinengappi4 = new jp.co.fsi.common.controls.FsiTextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.txtFuyoMimanKanaNm4 = new jp.co.fsi.common.controls.FsiTextBox();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.txtFuyoMimanNm3 = new jp.co.fsi.common.controls.FsiTextBox();
            this.label57 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.txtFuyoMimanJukyo3 = new jp.co.fsi.common.controls.FsiTextBox();
            this.button9 = new System.Windows.Forms.Button();
            this.txtFuyoMimanMyNumberDm3 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtFuyoMimanMyNumber3 = new jp.co.fsi.common.controls.FsiTextBox();
            this.label59 = new System.Windows.Forms.Label();
            this.panel12 = new jp.co.fsi.common.FsiPanel();
            this.label60 = new System.Windows.Forms.Label();
            this.txtFuyoMimanDaySeinengappi3 = new jp.co.fsi.common.controls.FsiTextBox();
            this.label61 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.txtFuyoMimanMonthSeinengappi3 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblFuyoMimanGengoSeinengappi3 = new System.Windows.Forms.Label();
            this.txtFuyoMimanGengoYearSeinengappi3 = new jp.co.fsi.common.controls.FsiTextBox();
            this.label64 = new System.Windows.Forms.Label();
            this.txtFuyoMimanKanaNm3 = new jp.co.fsi.common.controls.FsiTextBox();
            this.label65 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.txtFuyoMimanNm2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.label67 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.txtFuyoMimanJukyo2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.button8 = new System.Windows.Forms.Button();
            this.txtFuyoMimanMyNumberDm2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtFuyoMimanMyNumber2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.label70 = new System.Windows.Forms.Label();
            this.panel13 = new jp.co.fsi.common.FsiPanel();
            this.label71 = new System.Windows.Forms.Label();
            this.txtFuyoMimanDaySeinengappi2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.label72 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.txtFuyoMimanMonthSeinengappi2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblFuyoMimanGengoSeinengappi2 = new System.Windows.Forms.Label();
            this.txtFuyoMimanGengoYearSeinengappi2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.label75 = new System.Windows.Forms.Label();
            this.txtFuyoMimanKanaNm2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.label76 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.txtFuyoMimanNm1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.label79 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.txtFuyoMimanJukyo1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.button7 = new System.Windows.Forms.Button();
            this.txtFuyoMimanMyNumberDm1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtFuyoMimanMyNumber1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.label81 = new System.Windows.Forms.Label();
            this.panel16 = new jp.co.fsi.common.FsiPanel();
            this.label82 = new System.Windows.Forms.Label();
            this.txtFuyoMimanDaySeinengappi1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.label83 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.txtFuyoMimanMonthSeinengappi1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblFuyoMimanGengoSeinengappi1 = new System.Windows.Forms.Label();
            this.txtFuyoMimanGengoYearSeinengappi1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.label86 = new System.Windows.Forms.Label();
            this.txtFuyoMimanKanaNm1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.label87 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.tabPages.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.pnlPage1.SuspendLayout();
            this.pnlTaishokuNengappi.SuspendLayout();
            this.pnlNyushaNengappi.SuspendLayout();
            this.pnlSeinengappi.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.panel7.SuspendLayout();
            this.pnlFuyoHaigushaSeinengappi.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.panel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvShikyu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvKojo)).BeginInit();
            this.tabPage5.SuspendLayout();
            this.panel15.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tabPage7.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel16.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(725, 23);
            this.lblTitle.Text = "社員の登録";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(5, 445);
            this.pnlDebug.Size = new System.Drawing.Size(758, 95);
            // 
            // txtShainCd
            // 
            this.txtShainCd.AutoSizeFromLength = true;
            this.txtShainCd.DisplayLength = null;
            this.txtShainCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShainCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtShainCd.Location = new System.Drawing.Point(128, 14);
            this.txtShainCd.MaxLength = 6;
            this.txtShainCd.Name = "txtShainCd";
            this.txtShainCd.Size = new System.Drawing.Size(48, 23);
            this.txtShainCd.TabIndex = 0;
            this.txtShainCd.TabStop = false;
            this.txtShainCd.Text = "0";
            this.txtShainCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShainCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtShainCd_Validating);
            // 
            // lblShainCd
            // 
            this.lblShainCd.BackColor = System.Drawing.Color.Silver;
            this.lblShainCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShainCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShainCd.Location = new System.Drawing.Point(12, 13);
            this.lblShainCd.Name = "lblShainCd";
            this.lblShainCd.Size = new System.Drawing.Size(115, 25);
            this.lblShainCd.TabIndex = 1;
            this.lblShainCd.Text = "社員コード";
            this.lblShainCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMODE
            // 
            this.lblMODE.BackColor = System.Drawing.Color.White;
            this.lblMODE.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMODE.Location = new System.Drawing.Point(663, 16);
            this.lblMODE.Name = "lblMODE";
            this.lblMODE.Size = new System.Drawing.Size(78, 20);
            this.lblMODE.TabIndex = 902;
            this.lblMODE.Text = "【変更】";
            this.lblMODE.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblShainNmDisp
            // 
            this.lblShainNmDisp.BackColor = System.Drawing.Color.Silver;
            this.lblShainNmDisp.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShainNmDisp.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShainNmDisp.Location = new System.Drawing.Point(177, 13);
            this.lblShainNmDisp.Name = "lblShainNmDisp";
            this.lblShainNmDisp.Size = new System.Drawing.Size(211, 25);
            this.lblShainNmDisp.TabIndex = 903;
            this.lblShainNmDisp.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tabPages
            // 
            this.tabPages.Controls.Add(this.tabPage1);
            this.tabPages.Controls.Add(this.tabPage2);
            this.tabPages.Controls.Add(this.tabPage3);
            this.tabPages.Controls.Add(this.tabPage4);
            this.tabPages.Controls.Add(this.tabPage5);
            this.tabPages.Controls.Add(this.tabPage6);
            this.tabPages.Controls.Add(this.tabPage7);
            this.tabPages.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tabPages.ItemSize = new System.Drawing.Size(68, 25);
            this.tabPages.Location = new System.Drawing.Point(12, 51);
            this.tabPages.Name = "tabPages";
            this.tabPages.SelectedIndex = 0;
            this.tabPages.Size = new System.Drawing.Size(729, 428);
            this.tabPages.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.pnlPage1);
            this.tabPage1.Location = new System.Drawing.Point(4, 29);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(721, 395);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "F8 基本情報";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // pnlPage1
            // 
            this.pnlPage1.BackColor = System.Drawing.Color.PeachPuff;
            this.pnlPage1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlPage1.Controls.Add(this.label99);
            this.pnlPage1.Controls.Add(this.txtZenBikou);
            this.pnlPage1.Controls.Add(this.txthyoji);
            this.pnlPage1.Controls.Add(this.label4);
            this.pnlPage1.Controls.Add(this.label5);
            this.pnlPage1.Controls.Add(this.button1);
            this.pnlPage1.Controls.Add(this.FsiTextBox1);
            this.pnlPage1.Controls.Add(this.txtMyNumber);
            this.pnlPage1.Controls.Add(this.label1);
            this.pnlPage1.Controls.Add(this.pnlTaishokuNengappi);
            this.pnlPage1.Controls.Add(this.lblTaishokuNengappi);
            this.pnlPage1.Controls.Add(this.pnlNyushaNengappi);
            this.pnlPage1.Controls.Add(this.lblNyushaNengappi);
            this.pnlPage1.Controls.Add(this.lblKyuyoKeitaiNm);
            this.pnlPage1.Controls.Add(this.txtKyuyoKeitai);
            this.pnlPage1.Controls.Add(this.lblKyuyoKeitai);
            this.pnlPage1.Controls.Add(this.lblYakushokuNm);
            this.pnlPage1.Controls.Add(this.txtYakushokuCd);
            this.pnlPage1.Controls.Add(this.lblYakushokuCd);
            this.pnlPage1.Controls.Add(this.lblBukaNm);
            this.pnlPage1.Controls.Add(this.txtBukaCd);
            this.pnlPage1.Controls.Add(this.lblBukaCd);
            this.pnlPage1.Controls.Add(this.lblBumonNm);
            this.pnlPage1.Controls.Add(this.txtBumonCd);
            this.pnlPage1.Controls.Add(this.lblBumonCd);
            this.pnlPage1.Controls.Add(this.txtDenwaBango);
            this.pnlPage1.Controls.Add(this.txtJusho2);
            this.pnlPage1.Controls.Add(this.txtJusho1);
            this.pnlPage1.Controls.Add(this.lblYubinBango1);
            this.pnlPage1.Controls.Add(this.lblDenwaBango);
            this.pnlPage1.Controls.Add(this.lblJusho2);
            this.pnlPage1.Controls.Add(this.lblJusho1);
            this.pnlPage1.Controls.Add(this.lblSeibetsuNm);
            this.pnlPage1.Controls.Add(this.pnlSeinengappi);
            this.pnlPage1.Controls.Add(this.txtYubinBango2);
            this.pnlPage1.Controls.Add(this.lblYubinBango);
            this.pnlPage1.Controls.Add(this.txtSeibetsu);
            this.pnlPage1.Controls.Add(this.lblSeinengappi);
            this.pnlPage1.Controls.Add(this.txtYubinBango1);
            this.pnlPage1.Controls.Add(this.lblSeibetsu);
            this.pnlPage1.Controls.Add(this.txtShainKanaNm);
            this.pnlPage1.Controls.Add(this.lblShainKanaNm);
            this.pnlPage1.Controls.Add(this.txtShainNm);
            this.pnlPage1.Controls.Add(this.lblShainNm);
            this.pnlPage1.Location = new System.Drawing.Point(20, 18);
            this.pnlPage1.Name = "pnlPage1";
            this.pnlPage1.Size = new System.Drawing.Size(680, 359);
            this.pnlPage1.TabIndex = 5;
            // 
            // label99
            // 
            this.label99.BackColor = System.Drawing.Color.Transparent;
            this.label99.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label99.Location = new System.Drawing.Point(341, 216);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(224, 25);
            this.label99.TabIndex = 1050;
            this.label99.Text = "※前職支払者の住所、又は、名称";
            this.label99.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtZenBikou
            // 
            this.txtZenBikou.AutoSizeFromLength = false;
            this.txtZenBikou.DisplayLength = null;
            this.txtZenBikou.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtZenBikou.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtZenBikou.Location = new System.Drawing.Point(344, 244);
            this.txtZenBikou.MaxLength = 50;
            this.txtZenBikou.Name = "txtZenBikou";
            this.txtZenBikou.Size = new System.Drawing.Size(317, 23);
            this.txtZenBikou.TabIndex = 1048;
            this.txtZenBikou.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LastControl_KeyDown);
            this.txtZenBikou.Validating += new System.ComponentModel.CancelEventHandler(this.txtZenBikou_Validating);
            // 
            // txthyoji
            // 
            this.txthyoji.AutoSizeFromLength = false;
            this.txthyoji.BackColor = System.Drawing.Color.White;
            this.txthyoji.DisplayLength = null;
            this.txthyoji.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txthyoji.Location = new System.Drawing.Point(114, 269);
            this.txthyoji.MaxLength = 1;
            this.txthyoji.Name = "txthyoji";
            this.txthyoji.Size = new System.Drawing.Size(23, 20);
            this.txthyoji.TabIndex = 969;
            this.txthyoji.Text = "0";
            this.txthyoji.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txthyoji.Validating += new System.ComponentModel.CancelEventHandler(this.txthyoji_Validating);
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Silver;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.Location = new System.Drawing.Point(145, 267);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(188, 25);
            this.label4.TabIndex = 968;
            this.label4.Text = "0:表示する 1:表示しない ";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Silver;
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label5.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label5.Location = new System.Drawing.Point(14, 267);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(129, 25);
            this.label5.TabIndex = 967;
            this.label5.Text = "一覧表示";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(234, 244);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 45;
            this.button1.Text = "公開";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // FsiTextBox1
            // 
            this.FsiTextBox1.AutoSizeFromLength = false;
            this.FsiTextBox1.DisplayLength = null;
            this.FsiTextBox1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.FsiTextBox1.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.FsiTextBox1.Location = new System.Drawing.Point(114, 243);
            this.FsiTextBox1.MaxLength = 15;
            this.FsiTextBox1.Name = "FsiTextBox1";
            this.FsiTextBox1.ReadOnly = true;
            this.FsiTextBox1.Size = new System.Drawing.Size(114, 23);
            this.FsiTextBox1.TabIndex = 44;
            this.FsiTextBox1.Text = "************";
            this.FsiTextBox1.Visible = false;
            // 
            // txtMyNumber
            // 
            this.txtMyNumber.AutoSizeFromLength = false;
            this.txtMyNumber.DisplayLength = null;
            this.txtMyNumber.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMyNumber.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtMyNumber.Location = new System.Drawing.Point(114, 243);
            this.txtMyNumber.MaxLength = 12;
            this.txtMyNumber.Name = "txtMyNumber";
            this.txtMyNumber.ReadOnly = true;
            this.txtMyNumber.Size = new System.Drawing.Size(114, 23);
            this.txtMyNumber.TabIndex = 42;
            this.txtMyNumber.Validating += new System.ComponentModel.CancelEventHandler(this.txtMyNumber_Validating);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Silver;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(14, 243);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 23);
            this.label1.TabIndex = 43;
            this.label1.Text = "マイナンバー";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlTaishokuNengappi
            // 
            this.pnlTaishokuNengappi.BackColor = System.Drawing.Color.Silver;
            this.pnlTaishokuNengappi.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlTaishokuNengappi.Controls.Add(this.lblDayTaishokuNengappi);
            this.pnlTaishokuNengappi.Controls.Add(this.txtDayTaishokuNengappi);
            this.pnlTaishokuNengappi.Controls.Add(this.lblMonthTaishokuNengappi);
            this.pnlTaishokuNengappi.Controls.Add(this.lblGengoYearTaishokuNengappi);
            this.pnlTaishokuNengappi.Controls.Add(this.txtMonthTaishokuNengappi);
            this.pnlTaishokuNengappi.Controls.Add(this.lblGengoTaishokuNengappi);
            this.pnlTaishokuNengappi.Controls.Add(this.txtGengoYearTaishokuNengappi);
            this.pnlTaishokuNengappi.Location = new System.Drawing.Point(444, 161);
            this.pnlTaishokuNengappi.Name = "pnlTaishokuNengappi";
            this.pnlTaishokuNengappi.Size = new System.Drawing.Size(219, 25);
            this.pnlTaishokuNengappi.TabIndex = 14;
            // 
            // lblDayTaishokuNengappi
            // 
            this.lblDayTaishokuNengappi.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDayTaishokuNengappi.Location = new System.Drawing.Point(164, 0);
            this.lblDayTaishokuNengappi.Name = "lblDayTaishokuNengappi";
            this.lblDayTaishokuNengappi.Size = new System.Drawing.Size(21, 23);
            this.lblDayTaishokuNengappi.TabIndex = 14;
            this.lblDayTaishokuNengappi.Text = "日";
            this.lblDayTaishokuNengappi.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtDayTaishokuNengappi
            // 
            this.txtDayTaishokuNengappi.AutoSizeFromLength = false;
            this.txtDayTaishokuNengappi.DisplayLength = null;
            this.txtDayTaishokuNengappi.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDayTaishokuNengappi.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtDayTaishokuNengappi.Location = new System.Drawing.Point(139, 0);
            this.txtDayTaishokuNengappi.MaxLength = 2;
            this.txtDayTaishokuNengappi.Name = "txtDayTaishokuNengappi";
            this.txtDayTaishokuNengappi.Size = new System.Drawing.Size(25, 23);
            this.txtDayTaishokuNengappi.TabIndex = 13;
            this.txtDayTaishokuNengappi.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDayTaishokuNengappi.Validating += new System.ComponentModel.CancelEventHandler(this.txtDayTaishokuNengappi_Validating);
            // 
            // lblMonthTaishokuNengappi
            // 
            this.lblMonthTaishokuNengappi.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMonthTaishokuNengappi.Location = new System.Drawing.Point(118, 0);
            this.lblMonthTaishokuNengappi.Name = "lblMonthTaishokuNengappi";
            this.lblMonthTaishokuNengappi.Size = new System.Drawing.Size(21, 23);
            this.lblMonthTaishokuNengappi.TabIndex = 12;
            this.lblMonthTaishokuNengappi.Text = "月";
            this.lblMonthTaishokuNengappi.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblGengoYearTaishokuNengappi
            // 
            this.lblGengoYearTaishokuNengappi.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengoYearTaishokuNengappi.Location = new System.Drawing.Point(72, 0);
            this.lblGengoYearTaishokuNengappi.Name = "lblGengoYearTaishokuNengappi";
            this.lblGengoYearTaishokuNengappi.Size = new System.Drawing.Size(21, 23);
            this.lblGengoYearTaishokuNengappi.TabIndex = 10;
            this.lblGengoYearTaishokuNengappi.Text = "年";
            this.lblGengoYearTaishokuNengappi.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtMonthTaishokuNengappi
            // 
            this.txtMonthTaishokuNengappi.AutoSizeFromLength = false;
            this.txtMonthTaishokuNengappi.DisplayLength = null;
            this.txtMonthTaishokuNengappi.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMonthTaishokuNengappi.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtMonthTaishokuNengappi.Location = new System.Drawing.Point(93, 0);
            this.txtMonthTaishokuNengappi.MaxLength = 2;
            this.txtMonthTaishokuNengappi.Name = "txtMonthTaishokuNengappi";
            this.txtMonthTaishokuNengappi.Size = new System.Drawing.Size(25, 23);
            this.txtMonthTaishokuNengappi.TabIndex = 11;
            this.txtMonthTaishokuNengappi.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonthTaishokuNengappi.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthTaishokuNengappi_Validating);
            // 
            // lblGengoTaishokuNengappi
            // 
            this.lblGengoTaishokuNengappi.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengoTaishokuNengappi.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengoTaishokuNengappi.Location = new System.Drawing.Point(0, 0);
            this.lblGengoTaishokuNengappi.Name = "lblGengoTaishokuNengappi";
            this.lblGengoTaishokuNengappi.Size = new System.Drawing.Size(47, 23);
            this.lblGengoTaishokuNengappi.TabIndex = 8;
            this.lblGengoTaishokuNengappi.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtGengoYearTaishokuNengappi
            // 
            this.txtGengoYearTaishokuNengappi.AutoSizeFromLength = false;
            this.txtGengoYearTaishokuNengappi.DisplayLength = null;
            this.txtGengoYearTaishokuNengappi.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGengoYearTaishokuNengappi.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtGengoYearTaishokuNengappi.Location = new System.Drawing.Point(47, 0);
            this.txtGengoYearTaishokuNengappi.MaxLength = 2;
            this.txtGengoYearTaishokuNengappi.Name = "txtGengoYearTaishokuNengappi";
            this.txtGengoYearTaishokuNengappi.Size = new System.Drawing.Size(25, 23);
            this.txtGengoYearTaishokuNengappi.TabIndex = 9;
            this.txtGengoYearTaishokuNengappi.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGengoYearTaishokuNengappi.Validating += new System.ComponentModel.CancelEventHandler(this.txtGengoYearTaishokuNengappi_Validating);
            // 
            // lblTaishokuNengappi
            // 
            this.lblTaishokuNengappi.BackColor = System.Drawing.Color.Silver;
            this.lblTaishokuNengappi.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTaishokuNengappi.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTaishokuNengappi.Location = new System.Drawing.Point(344, 161);
            this.lblTaishokuNengappi.Name = "lblTaishokuNengappi";
            this.lblTaishokuNengappi.Size = new System.Drawing.Size(100, 25);
            this.lblTaishokuNengappi.TabIndex = 41;
            this.lblTaishokuNengappi.Text = "退職年月日";
            this.lblTaishokuNengappi.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlNyushaNengappi
            // 
            this.pnlNyushaNengappi.BackColor = System.Drawing.Color.Silver;
            this.pnlNyushaNengappi.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlNyushaNengappi.Controls.Add(this.lblDayNyushaNengappi);
            this.pnlNyushaNengappi.Controls.Add(this.txtDayNyushaNengappi);
            this.pnlNyushaNengappi.Controls.Add(this.lblMonthNyushaNengappi);
            this.pnlNyushaNengappi.Controls.Add(this.lblGengoYearNyushaNengappi);
            this.pnlNyushaNengappi.Controls.Add(this.txtMonthNyushaNengappi);
            this.pnlNyushaNengappi.Controls.Add(this.lblGengoNyushaNengappi);
            this.pnlNyushaNengappi.Controls.Add(this.txtGengoYearNyushaNengappi);
            this.pnlNyushaNengappi.Location = new System.Drawing.Point(444, 136);
            this.pnlNyushaNengappi.Name = "pnlNyushaNengappi";
            this.pnlNyushaNengappi.Size = new System.Drawing.Size(219, 25);
            this.pnlNyushaNengappi.TabIndex = 13;
            // 
            // lblDayNyushaNengappi
            // 
            this.lblDayNyushaNengappi.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDayNyushaNengappi.Location = new System.Drawing.Point(164, 0);
            this.lblDayNyushaNengappi.Name = "lblDayNyushaNengappi";
            this.lblDayNyushaNengappi.Size = new System.Drawing.Size(21, 23);
            this.lblDayNyushaNengappi.TabIndex = 14;
            this.lblDayNyushaNengappi.Text = "日";
            this.lblDayNyushaNengappi.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtDayNyushaNengappi
            // 
            this.txtDayNyushaNengappi.AutoSizeFromLength = false;
            this.txtDayNyushaNengappi.DisplayLength = null;
            this.txtDayNyushaNengappi.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDayNyushaNengappi.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtDayNyushaNengappi.Location = new System.Drawing.Point(139, 0);
            this.txtDayNyushaNengappi.MaxLength = 2;
            this.txtDayNyushaNengappi.Name = "txtDayNyushaNengappi";
            this.txtDayNyushaNengappi.Size = new System.Drawing.Size(25, 23);
            this.txtDayNyushaNengappi.TabIndex = 13;
            this.txtDayNyushaNengappi.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDayNyushaNengappi.Validating += new System.ComponentModel.CancelEventHandler(this.txtDayNyushaNengappi_Validating);
            // 
            // lblMonthNyushaNengappi
            // 
            this.lblMonthNyushaNengappi.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMonthNyushaNengappi.Location = new System.Drawing.Point(118, 0);
            this.lblMonthNyushaNengappi.Name = "lblMonthNyushaNengappi";
            this.lblMonthNyushaNengappi.Size = new System.Drawing.Size(21, 23);
            this.lblMonthNyushaNengappi.TabIndex = 12;
            this.lblMonthNyushaNengappi.Text = "月";
            this.lblMonthNyushaNengappi.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblGengoYearNyushaNengappi
            // 
            this.lblGengoYearNyushaNengappi.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengoYearNyushaNengappi.Location = new System.Drawing.Point(72, 0);
            this.lblGengoYearNyushaNengappi.Name = "lblGengoYearNyushaNengappi";
            this.lblGengoYearNyushaNengappi.Size = new System.Drawing.Size(21, 23);
            this.lblGengoYearNyushaNengappi.TabIndex = 10;
            this.lblGengoYearNyushaNengappi.Text = "年";
            this.lblGengoYearNyushaNengappi.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtMonthNyushaNengappi
            // 
            this.txtMonthNyushaNengappi.AutoSizeFromLength = false;
            this.txtMonthNyushaNengappi.DisplayLength = null;
            this.txtMonthNyushaNengappi.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMonthNyushaNengappi.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtMonthNyushaNengappi.Location = new System.Drawing.Point(93, 0);
            this.txtMonthNyushaNengappi.MaxLength = 2;
            this.txtMonthNyushaNengappi.Name = "txtMonthNyushaNengappi";
            this.txtMonthNyushaNengappi.Size = new System.Drawing.Size(25, 23);
            this.txtMonthNyushaNengappi.TabIndex = 11;
            this.txtMonthNyushaNengappi.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonthNyushaNengappi.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthNyushaNengappi_Validating);
            // 
            // lblGengoNyushaNengappi
            // 
            this.lblGengoNyushaNengappi.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengoNyushaNengappi.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengoNyushaNengappi.Location = new System.Drawing.Point(0, 0);
            this.lblGengoNyushaNengappi.Name = "lblGengoNyushaNengappi";
            this.lblGengoNyushaNengappi.Size = new System.Drawing.Size(47, 23);
            this.lblGengoNyushaNengappi.TabIndex = 8;
            this.lblGengoNyushaNengappi.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtGengoYearNyushaNengappi
            // 
            this.txtGengoYearNyushaNengappi.AutoSizeFromLength = false;
            this.txtGengoYearNyushaNengappi.DisplayLength = null;
            this.txtGengoYearNyushaNengappi.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGengoYearNyushaNengappi.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtGengoYearNyushaNengappi.Location = new System.Drawing.Point(47, 0);
            this.txtGengoYearNyushaNengappi.MaxLength = 2;
            this.txtGengoYearNyushaNengappi.Name = "txtGengoYearNyushaNengappi";
            this.txtGengoYearNyushaNengappi.Size = new System.Drawing.Size(25, 23);
            this.txtGengoYearNyushaNengappi.TabIndex = 0;
            this.txtGengoYearNyushaNengappi.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGengoYearNyushaNengappi.Validating += new System.ComponentModel.CancelEventHandler(this.txtGengoYearNyushaNengappi_Validating);
            // 
            // lblNyushaNengappi
            // 
            this.lblNyushaNengappi.BackColor = System.Drawing.Color.Silver;
            this.lblNyushaNengappi.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblNyushaNengappi.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblNyushaNengappi.Location = new System.Drawing.Point(344, 136);
            this.lblNyushaNengappi.Name = "lblNyushaNengappi";
            this.lblNyushaNengappi.Size = new System.Drawing.Size(100, 25);
            this.lblNyushaNengappi.TabIndex = 39;
            this.lblNyushaNengappi.Text = "入社年月日";
            this.lblNyushaNengappi.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKyuyoKeitaiNm
            // 
            this.lblKyuyoKeitaiNm.BackColor = System.Drawing.Color.Silver;
            this.lblKyuyoKeitaiNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKyuyoKeitaiNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKyuyoKeitaiNm.Location = new System.Drawing.Point(494, 99);
            this.lblKyuyoKeitaiNm.Name = "lblKyuyoKeitaiNm";
            this.lblKyuyoKeitaiNm.Size = new System.Drawing.Size(169, 23);
            this.lblKyuyoKeitaiNm.TabIndex = 38;
            this.lblKyuyoKeitaiNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKyuyoKeitai
            // 
            this.txtKyuyoKeitai.AutoSizeFromLength = true;
            this.txtKyuyoKeitai.DisplayLength = null;
            this.txtKyuyoKeitai.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKyuyoKeitai.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKyuyoKeitai.Location = new System.Drawing.Point(444, 99);
            this.txtKyuyoKeitai.MaxLength = 1;
            this.txtKyuyoKeitai.Name = "txtKyuyoKeitai";
            this.txtKyuyoKeitai.Size = new System.Drawing.Size(50, 23);
            this.txtKyuyoKeitai.TabIndex = 12;
            this.txtKyuyoKeitai.Text = "0";
            this.txtKyuyoKeitai.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKyuyoKeitai.Validating += new System.ComponentModel.CancelEventHandler(this.txtKyuyoKeitai_Validating);
            // 
            // lblKyuyoKeitai
            // 
            this.lblKyuyoKeitai.BackColor = System.Drawing.Color.Silver;
            this.lblKyuyoKeitai.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKyuyoKeitai.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKyuyoKeitai.Location = new System.Drawing.Point(344, 99);
            this.lblKyuyoKeitai.Name = "lblKyuyoKeitai";
            this.lblKyuyoKeitai.Size = new System.Drawing.Size(100, 23);
            this.lblKyuyoKeitai.TabIndex = 36;
            this.lblKyuyoKeitai.Text = "給与形態";
            this.lblKyuyoKeitai.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblYakushokuNm
            // 
            this.lblYakushokuNm.BackColor = System.Drawing.Color.Silver;
            this.lblYakushokuNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblYakushokuNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYakushokuNm.Location = new System.Drawing.Point(494, 76);
            this.lblYakushokuNm.Name = "lblYakushokuNm";
            this.lblYakushokuNm.Size = new System.Drawing.Size(169, 23);
            this.lblYakushokuNm.TabIndex = 35;
            this.lblYakushokuNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtYakushokuCd
            // 
            this.txtYakushokuCd.AutoSizeFromLength = true;
            this.txtYakushokuCd.DisplayLength = null;
            this.txtYakushokuCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtYakushokuCd.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtYakushokuCd.Location = new System.Drawing.Point(444, 76);
            this.txtYakushokuCd.MaxLength = 4;
            this.txtYakushokuCd.Name = "txtYakushokuCd";
            this.txtYakushokuCd.Size = new System.Drawing.Size(50, 23);
            this.txtYakushokuCd.TabIndex = 11;
            this.txtYakushokuCd.Text = "0";
            this.txtYakushokuCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtYakushokuCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtYakushokuCd_Validating);
            // 
            // lblYakushokuCd
            // 
            this.lblYakushokuCd.BackColor = System.Drawing.Color.Silver;
            this.lblYakushokuCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblYakushokuCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYakushokuCd.Location = new System.Drawing.Point(344, 76);
            this.lblYakushokuCd.Name = "lblYakushokuCd";
            this.lblYakushokuCd.Size = new System.Drawing.Size(100, 23);
            this.lblYakushokuCd.TabIndex = 33;
            this.lblYakushokuCd.Text = "役　　職";
            this.lblYakushokuCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblBukaNm
            // 
            this.lblBukaNm.BackColor = System.Drawing.Color.Silver;
            this.lblBukaNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBukaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBukaNm.Location = new System.Drawing.Point(494, 53);
            this.lblBukaNm.Name = "lblBukaNm";
            this.lblBukaNm.Size = new System.Drawing.Size(169, 23);
            this.lblBukaNm.TabIndex = 32;
            this.lblBukaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtBukaCd
            // 
            this.txtBukaCd.AutoSizeFromLength = true;
            this.txtBukaCd.DisplayLength = null;
            this.txtBukaCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtBukaCd.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtBukaCd.Location = new System.Drawing.Point(444, 53);
            this.txtBukaCd.MaxLength = 4;
            this.txtBukaCd.Name = "txtBukaCd";
            this.txtBukaCd.Size = new System.Drawing.Size(50, 23);
            this.txtBukaCd.TabIndex = 10;
            this.txtBukaCd.Text = "0";
            this.txtBukaCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBukaCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtBukaCd_Validating);
            // 
            // lblBukaCd
            // 
            this.lblBukaCd.BackColor = System.Drawing.Color.Silver;
            this.lblBukaCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBukaCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBukaCd.Location = new System.Drawing.Point(344, 53);
            this.lblBukaCd.Name = "lblBukaCd";
            this.lblBukaCd.Size = new System.Drawing.Size(100, 23);
            this.lblBukaCd.TabIndex = 30;
            this.lblBukaCd.Text = "部　　課";
            this.lblBukaCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblBumonNm
            // 
            this.lblBumonNm.BackColor = System.Drawing.Color.Silver;
            this.lblBumonNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBumonNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBumonNm.Location = new System.Drawing.Point(494, 30);
            this.lblBumonNm.Name = "lblBumonNm";
            this.lblBumonNm.Size = new System.Drawing.Size(169, 23);
            this.lblBumonNm.TabIndex = 29;
            this.lblBumonNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtBumonCd
            // 
            this.txtBumonCd.AutoSizeFromLength = true;
            this.txtBumonCd.DisplayLength = null;
            this.txtBumonCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtBumonCd.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtBumonCd.Location = new System.Drawing.Point(444, 30);
            this.txtBumonCd.MaxLength = 4;
            this.txtBumonCd.Name = "txtBumonCd";
            this.txtBumonCd.Size = new System.Drawing.Size(50, 23);
            this.txtBumonCd.TabIndex = 9;
            this.txtBumonCd.Text = "0";
            this.txtBumonCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBumonCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtBumonCd_Validating);
            // 
            // lblBumonCd
            // 
            this.lblBumonCd.BackColor = System.Drawing.Color.Silver;
            this.lblBumonCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBumonCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBumonCd.Location = new System.Drawing.Point(344, 30);
            this.lblBumonCd.Name = "lblBumonCd";
            this.lblBumonCd.Size = new System.Drawing.Size(100, 23);
            this.lblBumonCd.TabIndex = 27;
            this.lblBumonCd.Text = "部　　門";
            this.lblBumonCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDenwaBango
            // 
            this.txtDenwaBango.AutoSizeFromLength = false;
            this.txtDenwaBango.DisplayLength = null;
            this.txtDenwaBango.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDenwaBango.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtDenwaBango.Location = new System.Drawing.Point(114, 220);
            this.txtDenwaBango.MaxLength = 15;
            this.txtDenwaBango.Name = "txtDenwaBango";
            this.txtDenwaBango.Size = new System.Drawing.Size(114, 23);
            this.txtDenwaBango.TabIndex = 8;
            this.txtDenwaBango.Validating += new System.ComponentModel.CancelEventHandler(this.txtDenwaBango_Validating);
            // 
            // txtJusho2
            // 
            this.txtJusho2.AutoSizeFromLength = false;
            this.txtJusho2.DisplayLength = null;
            this.txtJusho2.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtJusho2.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtJusho2.Location = new System.Drawing.Point(114, 196);
            this.txtJusho2.MaxLength = 40;
            this.txtJusho2.Name = "txtJusho2";
            this.txtJusho2.Size = new System.Drawing.Size(219, 23);
            this.txtJusho2.TabIndex = 7;
            this.txtJusho2.Validating += new System.ComponentModel.CancelEventHandler(this.txtJusho2_Validating);
            // 
            // txtJusho1
            // 
            this.txtJusho1.AutoSizeFromLength = false;
            this.txtJusho1.DisplayLength = null;
            this.txtJusho1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtJusho1.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtJusho1.Location = new System.Drawing.Point(114, 173);
            this.txtJusho1.MaxLength = 40;
            this.txtJusho1.Name = "txtJusho1";
            this.txtJusho1.Size = new System.Drawing.Size(219, 23);
            this.txtJusho1.TabIndex = 6;
            this.txtJusho1.Validating += new System.ComponentModel.CancelEventHandler(this.txtJusho1_Validating);
            // 
            // lblYubinBango1
            // 
            this.lblYubinBango1.BackColor = System.Drawing.Color.Silver;
            this.lblYubinBango1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblYubinBango1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYubinBango1.Location = new System.Drawing.Point(150, 150);
            this.lblYubinBango1.Name = "lblYubinBango1";
            this.lblYubinBango1.Size = new System.Drawing.Size(20, 23);
            this.lblYubinBango1.TabIndex = 23;
            this.lblYubinBango1.Text = "-";
            this.lblYubinBango1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDenwaBango
            // 
            this.lblDenwaBango.BackColor = System.Drawing.Color.Silver;
            this.lblDenwaBango.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDenwaBango.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDenwaBango.Location = new System.Drawing.Point(14, 220);
            this.lblDenwaBango.Name = "lblDenwaBango";
            this.lblDenwaBango.Size = new System.Drawing.Size(100, 23);
            this.lblDenwaBango.TabIndex = 21;
            this.lblDenwaBango.Text = "電話番号";
            this.lblDenwaBango.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblJusho2
            // 
            this.lblJusho2.BackColor = System.Drawing.Color.Silver;
            this.lblJusho2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblJusho2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJusho2.Location = new System.Drawing.Point(14, 196);
            this.lblJusho2.Name = "lblJusho2";
            this.lblJusho2.Size = new System.Drawing.Size(100, 23);
            this.lblJusho2.TabIndex = 20;
            this.lblJusho2.Text = "住 所 ２";
            this.lblJusho2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblJusho1
            // 
            this.lblJusho1.BackColor = System.Drawing.Color.Silver;
            this.lblJusho1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblJusho1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJusho1.Location = new System.Drawing.Point(14, 173);
            this.lblJusho1.Name = "lblJusho1";
            this.lblJusho1.Size = new System.Drawing.Size(100, 23);
            this.lblJusho1.TabIndex = 19;
            this.lblJusho1.Text = "住 所 １";
            this.lblJusho1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSeibetsuNm
            // 
            this.lblSeibetsuNm.BackColor = System.Drawing.Color.Silver;
            this.lblSeibetsuNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSeibetsuNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSeibetsuNm.Location = new System.Drawing.Point(132, 76);
            this.lblSeibetsuNm.Name = "lblSeibetsuNm";
            this.lblSeibetsuNm.Size = new System.Drawing.Size(100, 23);
            this.lblSeibetsuNm.TabIndex = 2;
            this.lblSeibetsuNm.Text = "1：男　2:女";
            this.lblSeibetsuNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlSeinengappi
            // 
            this.pnlSeinengappi.BackColor = System.Drawing.Color.Silver;
            this.pnlSeinengappi.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlSeinengappi.Controls.Add(this.lblDaySeinengappi);
            this.pnlSeinengappi.Controls.Add(this.txtDaySeinengappi);
            this.pnlSeinengappi.Controls.Add(this.lblMonthSeinengappi);
            this.pnlSeinengappi.Controls.Add(this.lblGengoYearSeinengappi);
            this.pnlSeinengappi.Controls.Add(this.txtMonthSeinengappi);
            this.pnlSeinengappi.Controls.Add(this.lblGengoSeinengappi);
            this.pnlSeinengappi.Controls.Add(this.txtGengoYearSeinengappi);
            this.pnlSeinengappi.Location = new System.Drawing.Point(114, 99);
            this.pnlSeinengappi.Name = "pnlSeinengappi";
            this.pnlSeinengappi.Size = new System.Drawing.Size(219, 25);
            this.pnlSeinengappi.TabIndex = 3;
            // 
            // lblDaySeinengappi
            // 
            this.lblDaySeinengappi.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDaySeinengappi.Location = new System.Drawing.Point(164, 0);
            this.lblDaySeinengappi.Name = "lblDaySeinengappi";
            this.lblDaySeinengappi.Size = new System.Drawing.Size(21, 23);
            this.lblDaySeinengappi.TabIndex = 14;
            this.lblDaySeinengappi.Text = "日";
            this.lblDaySeinengappi.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtDaySeinengappi
            // 
            this.txtDaySeinengappi.AutoSizeFromLength = false;
            this.txtDaySeinengappi.DisplayLength = null;
            this.txtDaySeinengappi.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDaySeinengappi.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtDaySeinengappi.Location = new System.Drawing.Point(139, 0);
            this.txtDaySeinengappi.MaxLength = 2;
            this.txtDaySeinengappi.Name = "txtDaySeinengappi";
            this.txtDaySeinengappi.Size = new System.Drawing.Size(25, 23);
            this.txtDaySeinengappi.TabIndex = 2;
            this.txtDaySeinengappi.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDaySeinengappi.Validating += new System.ComponentModel.CancelEventHandler(this.txtDaySeinengappi_Validating);
            // 
            // lblMonthSeinengappi
            // 
            this.lblMonthSeinengappi.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMonthSeinengappi.Location = new System.Drawing.Point(118, 0);
            this.lblMonthSeinengappi.Name = "lblMonthSeinengappi";
            this.lblMonthSeinengappi.Size = new System.Drawing.Size(21, 23);
            this.lblMonthSeinengappi.TabIndex = 12;
            this.lblMonthSeinengappi.Text = "月";
            this.lblMonthSeinengappi.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblGengoYearSeinengappi
            // 
            this.lblGengoYearSeinengappi.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengoYearSeinengappi.Location = new System.Drawing.Point(72, 0);
            this.lblGengoYearSeinengappi.Name = "lblGengoYearSeinengappi";
            this.lblGengoYearSeinengappi.Size = new System.Drawing.Size(21, 23);
            this.lblGengoYearSeinengappi.TabIndex = 2;
            this.lblGengoYearSeinengappi.Text = "年";
            this.lblGengoYearSeinengappi.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtMonthSeinengappi
            // 
            this.txtMonthSeinengappi.AutoSizeFromLength = false;
            this.txtMonthSeinengappi.DisplayLength = null;
            this.txtMonthSeinengappi.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMonthSeinengappi.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtMonthSeinengappi.Location = new System.Drawing.Point(93, 0);
            this.txtMonthSeinengappi.MaxLength = 2;
            this.txtMonthSeinengappi.Name = "txtMonthSeinengappi";
            this.txtMonthSeinengappi.Size = new System.Drawing.Size(25, 23);
            this.txtMonthSeinengappi.TabIndex = 1;
            this.txtMonthSeinengappi.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonthSeinengappi.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthSeinengappi_Validating);
            // 
            // lblGengoSeinengappi
            // 
            this.lblGengoSeinengappi.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengoSeinengappi.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengoSeinengappi.Location = new System.Drawing.Point(0, 0);
            this.lblGengoSeinengappi.Name = "lblGengoSeinengappi";
            this.lblGengoSeinengappi.Size = new System.Drawing.Size(47, 23);
            this.lblGengoSeinengappi.TabIndex = 0;
            this.lblGengoSeinengappi.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtGengoYearSeinengappi
            // 
            this.txtGengoYearSeinengappi.AutoSizeFromLength = false;
            this.txtGengoYearSeinengappi.DisplayLength = null;
            this.txtGengoYearSeinengappi.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGengoYearSeinengappi.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtGengoYearSeinengappi.Location = new System.Drawing.Point(47, 0);
            this.txtGengoYearSeinengappi.MaxLength = 2;
            this.txtGengoYearSeinengappi.Name = "txtGengoYearSeinengappi";
            this.txtGengoYearSeinengappi.Size = new System.Drawing.Size(25, 23);
            this.txtGengoYearSeinengappi.TabIndex = 0;
            this.txtGengoYearSeinengappi.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGengoYearSeinengappi.Validating += new System.ComponentModel.CancelEventHandler(this.txtGengoYearSeinengappi_Validating);
            // 
            // txtYubinBango2
            // 
            this.txtYubinBango2.AutoSizeFromLength = true;
            this.txtYubinBango2.DisplayLength = null;
            this.txtYubinBango2.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtYubinBango2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtYubinBango2.Location = new System.Drawing.Point(170, 150);
            this.txtYubinBango2.MaxLength = 4;
            this.txtYubinBango2.Name = "txtYubinBango2";
            this.txtYubinBango2.Size = new System.Drawing.Size(47, 23);
            this.txtYubinBango2.TabIndex = 5;
            this.txtYubinBango2.Validating += new System.ComponentModel.CancelEventHandler(this.txtYubinBango2_Validating);
            // 
            // lblYubinBango
            // 
            this.lblYubinBango.BackColor = System.Drawing.Color.Silver;
            this.lblYubinBango.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblYubinBango.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYubinBango.Location = new System.Drawing.Point(14, 150);
            this.lblYubinBango.Name = "lblYubinBango";
            this.lblYubinBango.Size = new System.Drawing.Size(100, 23);
            this.lblYubinBango.TabIndex = 14;
            this.lblYubinBango.Text = "郵便番号";
            this.lblYubinBango.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSeibetsu
            // 
            this.txtSeibetsu.AutoSizeFromLength = true;
            this.txtSeibetsu.DisplayLength = null;
            this.txtSeibetsu.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSeibetsu.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtSeibetsu.Location = new System.Drawing.Point(114, 76);
            this.txtSeibetsu.MaxLength = 1;
            this.txtSeibetsu.Name = "txtSeibetsu";
            this.txtSeibetsu.Size = new System.Drawing.Size(18, 23);
            this.txtSeibetsu.TabIndex = 2;
            this.txtSeibetsu.Text = "0";
            this.txtSeibetsu.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtSeibetsu.Validating += new System.ComponentModel.CancelEventHandler(this.txtSeibetsu_Validating);
            // 
            // lblSeinengappi
            // 
            this.lblSeinengappi.BackColor = System.Drawing.Color.Silver;
            this.lblSeinengappi.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSeinengappi.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSeinengappi.Location = new System.Drawing.Point(14, 99);
            this.lblSeinengappi.Name = "lblSeinengappi";
            this.lblSeinengappi.Size = new System.Drawing.Size(100, 25);
            this.lblSeinengappi.TabIndex = 12;
            this.lblSeinengappi.Text = "生年月日";
            this.lblSeinengappi.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtYubinBango1
            // 
            this.txtYubinBango1.AutoSizeFromLength = false;
            this.txtYubinBango1.DisplayLength = null;
            this.txtYubinBango1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtYubinBango1.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtYubinBango1.Location = new System.Drawing.Point(114, 150);
            this.txtYubinBango1.MaxLength = 3;
            this.txtYubinBango1.Name = "txtYubinBango1";
            this.txtYubinBango1.Size = new System.Drawing.Size(36, 23);
            this.txtYubinBango1.TabIndex = 4;
            this.txtYubinBango1.Validating += new System.ComponentModel.CancelEventHandler(this.txtYubinBango1_Validating);
            // 
            // lblSeibetsu
            // 
            this.lblSeibetsu.BackColor = System.Drawing.Color.Silver;
            this.lblSeibetsu.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSeibetsu.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSeibetsu.Location = new System.Drawing.Point(14, 76);
            this.lblSeibetsu.Name = "lblSeibetsu";
            this.lblSeibetsu.Size = new System.Drawing.Size(100, 23);
            this.lblSeibetsu.TabIndex = 8;
            this.lblSeibetsu.Text = "性　　別";
            this.lblSeibetsu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShainKanaNm
            // 
            this.txtShainKanaNm.AutoSizeFromLength = false;
            this.txtShainKanaNm.DisplayLength = null;
            this.txtShainKanaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShainKanaNm.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtShainKanaNm.Location = new System.Drawing.Point(114, 53);
            this.txtShainKanaNm.MaxLength = 30;
            this.txtShainKanaNm.Name = "txtShainKanaNm";
            this.txtShainKanaNm.Size = new System.Drawing.Size(219, 23);
            this.txtShainKanaNm.TabIndex = 1;
            this.txtShainKanaNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtShainKanaNm_Validating);
            // 
            // lblShainKanaNm
            // 
            this.lblShainKanaNm.BackColor = System.Drawing.Color.Silver;
            this.lblShainKanaNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShainKanaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShainKanaNm.Location = new System.Drawing.Point(14, 53);
            this.lblShainKanaNm.Name = "lblShainKanaNm";
            this.lblShainKanaNm.Size = new System.Drawing.Size(100, 23);
            this.lblShainKanaNm.TabIndex = 2;
            this.lblShainKanaNm.Text = "フリガナ";
            this.lblShainKanaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShainNm
            // 
            this.txtShainNm.AutoSizeFromLength = false;
            this.txtShainNm.DisplayLength = null;
            this.txtShainNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShainNm.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtShainNm.Location = new System.Drawing.Point(114, 30);
            this.txtShainNm.MaxLength = 30;
            this.txtShainNm.Name = "txtShainNm";
            this.txtShainNm.Size = new System.Drawing.Size(219, 23);
            this.txtShainNm.TabIndex = 0;
            this.txtShainNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtShainNm_Validating);
            // 
            // lblShainNm
            // 
            this.lblShainNm.BackColor = System.Drawing.Color.Silver;
            this.lblShainNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShainNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShainNm.Location = new System.Drawing.Point(14, 30);
            this.lblShainNm.Name = "lblShainNm";
            this.lblShainNm.Size = new System.Drawing.Size(100, 23);
            this.lblShainNm.TabIndex = 0;
            this.lblShainNm.Text = "氏　　名";
            this.lblShainNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.panel3);
            this.tabPage2.Location = new System.Drawing.Point(4, 29);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(721, 395);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "F9 社会保険／住民税";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.PeachPuff;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.txtKoseiNenkinHokenryo);
            this.panel3.Controls.Add(this.lblKoseiNenkinShoyoKeisanKbn);
            this.panel3.Controls.Add(this.txtJuminzei7gatsubunIko);
            this.panel3.Controls.Add(this.lblJuminzei7gatsubunIko);
            this.panel3.Controls.Add(this.txtJuminzei6gatsubun);
            this.panel3.Controls.Add(this.lblJuminzeiShichosonNm);
            this.panel3.Controls.Add(this.txtJuminzeiShichosonCd);
            this.panel3.Controls.Add(this.lblKoyoHokenKeisanKubunNm);
            this.panel3.Controls.Add(this.txtKoyoHokenKeisanKubun);
            this.panel3.Controls.Add(this.lblKoseiNenkinHyojunHoshuGtgkNm);
            this.panel3.Controls.Add(this.lblKenkoHokenHyojunHoshuGtgkNm);
            this.panel3.Controls.Add(this.lblKoseiNenkinShoyoKeisanKbnNm);
            this.panel3.Controls.Add(this.KaigoHokenKubunNm);
            this.panel3.Controls.Add(this.txtKaigoHokenKubun);
            this.panel3.Controls.Add(this.lblKaigoHokenKubun);
            this.panel3.Controls.Add(this.lblKenkoHokenShoyoKeisanKubunNm);
            this.panel3.Controls.Add(this.txtKenkoHokenShoyoKeisanKubun);
            this.panel3.Controls.Add(this.lblKenkoHokenShoyoKeisanKubun);
            this.panel3.Controls.Add(this.lblKoseiNenkin);
            this.panel3.Controls.Add(this.lblKoyohoken);
            this.panel3.Controls.Add(this.lblKenkohoken);
            this.panel3.Controls.Add(this.lblJuminzei6gatsubun);
            this.panel3.Controls.Add(this.lblJuminzeiShichosonCd);
            this.panel3.Controls.Add(this.lblKoyoHokenKeisanKubun);
            this.panel3.Controls.Add(this.txtKoseiNenkinShoyoKeisanKbn);
            this.panel3.Controls.Add(this.txtKoseiNenkinHyojunHoshuGtgk);
            this.panel3.Controls.Add(this.txtKoseiNenkinBango);
            this.panel3.Controls.Add(this.lblKoseiNenkinHokenryo);
            this.panel3.Controls.Add(this.lblKoseiNenkinHyojunHoshuGtgk);
            this.panel3.Controls.Add(this.lblKoseiNenkinBango);
            this.panel3.Controls.Add(this.txtKenkoHokenryo);
            this.panel3.Controls.Add(this.lblKenkoHokenryo);
            this.panel3.Controls.Add(this.txtKenkoHokenHyojunHoshuGtgk);
            this.panel3.Controls.Add(this.lblKenkoHokenHyojunHoshuGtgk);
            this.panel3.Controls.Add(this.txtKenkoHokenshoBango);
            this.panel3.Controls.Add(this.lblKenkoHokenshoBango);
            this.panel3.Controls.Add(this.lblJuminzei);
            this.panel3.Location = new System.Drawing.Point(20, 18);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(680, 359);
            this.panel3.TabIndex = 0;
            // 
            // txtKoseiNenkinHokenryo
            // 
            this.txtKoseiNenkinHokenryo.AcceptsReturn = true;
            this.txtKoseiNenkinHokenryo.AutoSizeFromLength = false;
            this.txtKoseiNenkinHokenryo.DisplayLength = null;
            this.txtKoseiNenkinHokenryo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKoseiNenkinHokenryo.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKoseiNenkinHokenryo.Location = new System.Drawing.Point(114, 219);
            this.txtKoseiNenkinHokenryo.MaxLength = 9;
            this.txtKoseiNenkinHokenryo.Name = "txtKoseiNenkinHokenryo";
            this.txtKoseiNenkinHokenryo.Size = new System.Drawing.Size(116, 23);
            this.txtKoseiNenkinHokenryo.TabIndex = 7;
            this.txtKoseiNenkinHokenryo.Text = "0";
            this.txtKoseiNenkinHokenryo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKoseiNenkinHokenryo.Validating += new System.ComponentModel.CancelEventHandler(this.txtKoseiNenkinHokenryo_Validating);
            // 
            // lblKoseiNenkinShoyoKeisanKbn
            // 
            this.lblKoseiNenkinShoyoKeisanKbn.BackColor = System.Drawing.Color.Silver;
            this.lblKoseiNenkinShoyoKeisanKbn.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKoseiNenkinShoyoKeisanKbn.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKoseiNenkinShoyoKeisanKbn.Location = new System.Drawing.Point(14, 242);
            this.lblKoseiNenkinShoyoKeisanKbn.Name = "lblKoseiNenkinShoyoKeisanKbn";
            this.lblKoseiNenkinShoyoKeisanKbn.Size = new System.Drawing.Size(100, 23);
            this.lblKoseiNenkinShoyoKeisanKbn.TabIndex = 61;
            this.lblKoseiNenkinShoyoKeisanKbn.Text = "賞与特別計算";
            this.lblKoseiNenkinShoyoKeisanKbn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtJuminzei7gatsubunIko
            // 
            this.txtJuminzei7gatsubunIko.AutoSizeFromLength = false;
            this.txtJuminzei7gatsubunIko.DisplayLength = null;
            this.txtJuminzei7gatsubunIko.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtJuminzei7gatsubunIko.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtJuminzei7gatsubunIko.Location = new System.Drawing.Point(444, 217);
            this.txtJuminzei7gatsubunIko.MaxLength = 9;
            this.txtJuminzei7gatsubunIko.Name = "txtJuminzei7gatsubunIko";
            this.txtJuminzei7gatsubunIko.Size = new System.Drawing.Size(116, 23);
            this.txtJuminzei7gatsubunIko.TabIndex = 12;
            this.txtJuminzei7gatsubunIko.Text = "0";
            this.txtJuminzei7gatsubunIko.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtJuminzei7gatsubunIko.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LastControl_KeyDown);
            this.txtJuminzei7gatsubunIko.Validating += new System.ComponentModel.CancelEventHandler(this.txtJuminzei7gatsubunIko_Validating);
            // 
            // lblJuminzei7gatsubunIko
            // 
            this.lblJuminzei7gatsubunIko.BackColor = System.Drawing.Color.Silver;
            this.lblJuminzei7gatsubunIko.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblJuminzei7gatsubunIko.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJuminzei7gatsubunIko.Location = new System.Drawing.Point(344, 217);
            this.lblJuminzei7gatsubunIko.Name = "lblJuminzei7gatsubunIko";
            this.lblJuminzei7gatsubunIko.Size = new System.Drawing.Size(100, 23);
            this.lblJuminzei7gatsubunIko.TabIndex = 60;
            this.lblJuminzei7gatsubunIko.Text = "７月分以降";
            this.lblJuminzei7gatsubunIko.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtJuminzei6gatsubun
            // 
            this.txtJuminzei6gatsubun.AutoSizeFromLength = false;
            this.txtJuminzei6gatsubun.DisplayLength = null;
            this.txtJuminzei6gatsubun.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtJuminzei6gatsubun.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtJuminzei6gatsubun.Location = new System.Drawing.Point(444, 194);
            this.txtJuminzei6gatsubun.MaxLength = 9;
            this.txtJuminzei6gatsubun.Name = "txtJuminzei6gatsubun";
            this.txtJuminzei6gatsubun.Size = new System.Drawing.Size(116, 23);
            this.txtJuminzei6gatsubun.TabIndex = 11;
            this.txtJuminzei6gatsubun.Text = "0";
            this.txtJuminzei6gatsubun.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtJuminzei6gatsubun.Validating += new System.ComponentModel.CancelEventHandler(this.txtJuminzei6gatsubun_Validating);
            // 
            // lblJuminzeiShichosonNm
            // 
            this.lblJuminzeiShichosonNm.BackColor = System.Drawing.Color.Silver;
            this.lblJuminzeiShichosonNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblJuminzeiShichosonNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJuminzeiShichosonNm.Location = new System.Drawing.Point(494, 171);
            this.lblJuminzeiShichosonNm.Name = "lblJuminzeiShichosonNm";
            this.lblJuminzeiShichosonNm.Size = new System.Drawing.Size(169, 23);
            this.lblJuminzeiShichosonNm.TabIndex = 58;
            this.lblJuminzeiShichosonNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtJuminzeiShichosonCd
            // 
            this.txtJuminzeiShichosonCd.AutoSizeFromLength = true;
            this.txtJuminzeiShichosonCd.DisplayLength = null;
            this.txtJuminzeiShichosonCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtJuminzeiShichosonCd.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtJuminzeiShichosonCd.Location = new System.Drawing.Point(444, 171);
            this.txtJuminzeiShichosonCd.MaxLength = 6;
            this.txtJuminzeiShichosonCd.Name = "txtJuminzeiShichosonCd";
            this.txtJuminzeiShichosonCd.Size = new System.Drawing.Size(48, 23);
            this.txtJuminzeiShichosonCd.TabIndex = 10;
            this.txtJuminzeiShichosonCd.Text = "0";
            this.txtJuminzeiShichosonCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtJuminzeiShichosonCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtJuminzeiShichosonCd_Validating);
            // 
            // lblKoyoHokenKeisanKubunNm
            // 
            this.lblKoyoHokenKeisanKubunNm.BackColor = System.Drawing.Color.Silver;
            this.lblKoyoHokenKeisanKubunNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKoyoHokenKeisanKubunNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKoyoHokenKeisanKubunNm.Location = new System.Drawing.Point(462, 30);
            this.lblKoyoHokenKeisanKubunNm.Name = "lblKoyoHokenKeisanKubunNm";
            this.lblKoyoHokenKeisanKubunNm.Size = new System.Drawing.Size(133, 23);
            this.lblKoyoHokenKeisanKubunNm.TabIndex = 56;
            this.lblKoyoHokenKeisanKubunNm.Text = "0:しない　1:する";
            this.lblKoyoHokenKeisanKubunNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKoyoHokenKeisanKubun
            // 
            this.txtKoyoHokenKeisanKubun.AutoSizeFromLength = true;
            this.txtKoyoHokenKeisanKubun.DisplayLength = null;
            this.txtKoyoHokenKeisanKubun.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKoyoHokenKeisanKubun.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtKoyoHokenKeisanKubun.Location = new System.Drawing.Point(444, 30);
            this.txtKoyoHokenKeisanKubun.MaxLength = 1;
            this.txtKoyoHokenKeisanKubun.Name = "txtKoyoHokenKeisanKubun";
            this.txtKoyoHokenKeisanKubun.Size = new System.Drawing.Size(18, 23);
            this.txtKoyoHokenKeisanKubun.TabIndex = 9;
            this.txtKoyoHokenKeisanKubun.Text = "0";
            this.txtKoyoHokenKeisanKubun.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtKoyoHokenKeisanKubun.Validating += new System.ComponentModel.CancelEventHandler(this.txtKoyoHokenKeisanKubun_Validating);
            // 
            // lblKoseiNenkinHyojunHoshuGtgkNm
            // 
            this.lblKoseiNenkinHyojunHoshuGtgkNm.BackColor = System.Drawing.Color.Silver;
            this.lblKoseiNenkinHyojunHoshuGtgkNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKoseiNenkinHyojunHoshuGtgkNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKoseiNenkinHyojunHoshuGtgkNm.Location = new System.Drawing.Point(184, 196);
            this.lblKoseiNenkinHyojunHoshuGtgkNm.Name = "lblKoseiNenkinHyojunHoshuGtgkNm";
            this.lblKoseiNenkinHyojunHoshuGtgkNm.Size = new System.Drawing.Size(46, 23);
            this.lblKoseiNenkinHyojunHoshuGtgkNm.TabIndex = 54;
            this.lblKoseiNenkinHyojunHoshuGtgkNm.Text = "千円";
            this.lblKoseiNenkinHyojunHoshuGtgkNm.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblKenkoHokenHyojunHoshuGtgkNm
            // 
            this.lblKenkoHokenHyojunHoshuGtgkNm.BackColor = System.Drawing.Color.Silver;
            this.lblKenkoHokenHyojunHoshuGtgkNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKenkoHokenHyojunHoshuGtgkNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKenkoHokenHyojunHoshuGtgkNm.Location = new System.Drawing.Point(184, 53);
            this.lblKenkoHokenHyojunHoshuGtgkNm.Name = "lblKenkoHokenHyojunHoshuGtgkNm";
            this.lblKenkoHokenHyojunHoshuGtgkNm.Size = new System.Drawing.Size(46, 23);
            this.lblKenkoHokenHyojunHoshuGtgkNm.TabIndex = 53;
            this.lblKenkoHokenHyojunHoshuGtgkNm.Text = "千円";
            this.lblKenkoHokenHyojunHoshuGtgkNm.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblKoseiNenkinShoyoKeisanKbnNm
            // 
            this.lblKoseiNenkinShoyoKeisanKbnNm.BackColor = System.Drawing.Color.Silver;
            this.lblKoseiNenkinShoyoKeisanKbnNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKoseiNenkinShoyoKeisanKbnNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKoseiNenkinShoyoKeisanKbnNm.Location = new System.Drawing.Point(132, 242);
            this.lblKoseiNenkinShoyoKeisanKbnNm.Name = "lblKoseiNenkinShoyoKeisanKbnNm";
            this.lblKoseiNenkinShoyoKeisanKbnNm.Size = new System.Drawing.Size(133, 23);
            this.lblKoseiNenkinShoyoKeisanKbnNm.TabIndex = 52;
            this.lblKoseiNenkinShoyoKeisanKbnNm.Text = "0:しない　1:する";
            this.lblKoseiNenkinShoyoKeisanKbnNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // KaigoHokenKubunNm
            // 
            this.KaigoHokenKubunNm.BackColor = System.Drawing.Color.Silver;
            this.KaigoHokenKubunNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.KaigoHokenKubunNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.KaigoHokenKubunNm.Location = new System.Drawing.Point(132, 122);
            this.KaigoHokenKubunNm.Name = "KaigoHokenKubunNm";
            this.KaigoHokenKubunNm.Size = new System.Drawing.Size(133, 23);
            this.KaigoHokenKubunNm.TabIndex = 50;
            this.KaigoHokenKubunNm.Text = "0:しない　1:する";
            this.KaigoHokenKubunNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKaigoHokenKubun
            // 
            this.txtKaigoHokenKubun.AutoSizeFromLength = true;
            this.txtKaigoHokenKubun.DisplayLength = null;
            this.txtKaigoHokenKubun.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKaigoHokenKubun.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtKaigoHokenKubun.Location = new System.Drawing.Point(114, 122);
            this.txtKaigoHokenKubun.MaxLength = 1;
            this.txtKaigoHokenKubun.Name = "txtKaigoHokenKubun";
            this.txtKaigoHokenKubun.Size = new System.Drawing.Size(18, 23);
            this.txtKaigoHokenKubun.TabIndex = 4;
            this.txtKaigoHokenKubun.Text = "0";
            this.txtKaigoHokenKubun.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtKaigoHokenKubun.Validating += new System.ComponentModel.CancelEventHandler(this.txtKaigoHokenKubun_Validating);
            // 
            // lblKaigoHokenKubun
            // 
            this.lblKaigoHokenKubun.BackColor = System.Drawing.Color.Silver;
            this.lblKaigoHokenKubun.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKaigoHokenKubun.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKaigoHokenKubun.Location = new System.Drawing.Point(14, 122);
            this.lblKaigoHokenKubun.Name = "lblKaigoHokenKubun";
            this.lblKaigoHokenKubun.Size = new System.Drawing.Size(100, 23);
            this.lblKaigoHokenKubun.TabIndex = 48;
            this.lblKaigoHokenKubun.Text = "介護保険該当";
            this.lblKaigoHokenKubun.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKenkoHokenShoyoKeisanKubunNm
            // 
            this.lblKenkoHokenShoyoKeisanKubunNm.BackColor = System.Drawing.Color.Silver;
            this.lblKenkoHokenShoyoKeisanKubunNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKenkoHokenShoyoKeisanKubunNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKenkoHokenShoyoKeisanKubunNm.Location = new System.Drawing.Point(132, 99);
            this.lblKenkoHokenShoyoKeisanKubunNm.Name = "lblKenkoHokenShoyoKeisanKubunNm";
            this.lblKenkoHokenShoyoKeisanKubunNm.Size = new System.Drawing.Size(133, 23);
            this.lblKenkoHokenShoyoKeisanKubunNm.TabIndex = 47;
            this.lblKenkoHokenShoyoKeisanKubunNm.Text = "0:しない　1:する";
            this.lblKenkoHokenShoyoKeisanKubunNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKenkoHokenShoyoKeisanKubun
            // 
            this.txtKenkoHokenShoyoKeisanKubun.AutoSizeFromLength = true;
            this.txtKenkoHokenShoyoKeisanKubun.DisplayLength = null;
            this.txtKenkoHokenShoyoKeisanKubun.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKenkoHokenShoyoKeisanKubun.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtKenkoHokenShoyoKeisanKubun.Location = new System.Drawing.Point(114, 99);
            this.txtKenkoHokenShoyoKeisanKubun.MaxLength = 1;
            this.txtKenkoHokenShoyoKeisanKubun.Name = "txtKenkoHokenShoyoKeisanKubun";
            this.txtKenkoHokenShoyoKeisanKubun.Size = new System.Drawing.Size(18, 23);
            this.txtKenkoHokenShoyoKeisanKubun.TabIndex = 3;
            this.txtKenkoHokenShoyoKeisanKubun.Text = "0";
            this.txtKenkoHokenShoyoKeisanKubun.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtKenkoHokenShoyoKeisanKubun.Validating += new System.ComponentModel.CancelEventHandler(this.txtKenkoHokenShoyoKeisanKubun_Validating);
            // 
            // lblKenkoHokenShoyoKeisanKubun
            // 
            this.lblKenkoHokenShoyoKeisanKubun.BackColor = System.Drawing.Color.Silver;
            this.lblKenkoHokenShoyoKeisanKubun.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKenkoHokenShoyoKeisanKubun.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKenkoHokenShoyoKeisanKubun.Location = new System.Drawing.Point(14, 99);
            this.lblKenkoHokenShoyoKeisanKubun.Name = "lblKenkoHokenShoyoKeisanKubun";
            this.lblKenkoHokenShoyoKeisanKubun.Size = new System.Drawing.Size(100, 23);
            this.lblKenkoHokenShoyoKeisanKubun.TabIndex = 45;
            this.lblKenkoHokenShoyoKeisanKubun.Text = "賞与特別計算";
            this.lblKenkoHokenShoyoKeisanKubun.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKoseiNenkin
            // 
            this.lblKoseiNenkin.BackColor = System.Drawing.Color.Transparent;
            this.lblKoseiNenkin.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKoseiNenkin.Location = new System.Drawing.Point(11, 150);
            this.lblKoseiNenkin.Name = "lblKoseiNenkin";
            this.lblKoseiNenkin.Size = new System.Drawing.Size(136, 23);
            this.lblKoseiNenkin.TabIndex = 44;
            this.lblKoseiNenkin.Text = "【厚生年金】";
            this.lblKoseiNenkin.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKoyohoken
            // 
            this.lblKoyohoken.BackColor = System.Drawing.Color.Transparent;
            this.lblKoyohoken.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKoyohoken.Location = new System.Drawing.Point(341, 7);
            this.lblKoyohoken.Name = "lblKoyohoken";
            this.lblKoyohoken.Size = new System.Drawing.Size(136, 23);
            this.lblKoyohoken.TabIndex = 43;
            this.lblKoyohoken.Text = "【雇用保険】";
            this.lblKoyohoken.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKenkohoken
            // 
            this.lblKenkohoken.BackColor = System.Drawing.Color.Transparent;
            this.lblKenkohoken.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKenkohoken.Location = new System.Drawing.Point(11, 7);
            this.lblKenkohoken.Name = "lblKenkohoken";
            this.lblKenkohoken.Size = new System.Drawing.Size(136, 23);
            this.lblKenkohoken.TabIndex = 42;
            this.lblKenkohoken.Text = "【健康保険】";
            this.lblKenkohoken.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblJuminzei6gatsubun
            // 
            this.lblJuminzei6gatsubun.BackColor = System.Drawing.Color.Silver;
            this.lblJuminzei6gatsubun.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblJuminzei6gatsubun.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJuminzei6gatsubun.Location = new System.Drawing.Point(344, 194);
            this.lblJuminzei6gatsubun.Name = "lblJuminzei6gatsubun";
            this.lblJuminzei6gatsubun.Size = new System.Drawing.Size(100, 23);
            this.lblJuminzei6gatsubun.TabIndex = 41;
            this.lblJuminzei6gatsubun.Text = "６　月　分";
            this.lblJuminzei6gatsubun.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblJuminzeiShichosonCd
            // 
            this.lblJuminzeiShichosonCd.BackColor = System.Drawing.Color.Silver;
            this.lblJuminzeiShichosonCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblJuminzeiShichosonCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJuminzeiShichosonCd.Location = new System.Drawing.Point(344, 171);
            this.lblJuminzeiShichosonCd.Name = "lblJuminzeiShichosonCd";
            this.lblJuminzeiShichosonCd.Size = new System.Drawing.Size(100, 23);
            this.lblJuminzeiShichosonCd.TabIndex = 39;
            this.lblJuminzeiShichosonCd.Text = "市町村コード";
            this.lblJuminzeiShichosonCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKoyoHokenKeisanKubun
            // 
            this.lblKoyoHokenKeisanKubun.BackColor = System.Drawing.Color.Silver;
            this.lblKoyoHokenKeisanKubun.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKoyoHokenKeisanKubun.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKoyoHokenKeisanKubun.Location = new System.Drawing.Point(344, 30);
            this.lblKoyoHokenKeisanKubun.Name = "lblKoyoHokenKeisanKubun";
            this.lblKoyoHokenKeisanKubun.Size = new System.Drawing.Size(100, 23);
            this.lblKoyoHokenKeisanKubun.TabIndex = 27;
            this.lblKoyoHokenKeisanKubun.Text = "保険料計算";
            this.lblKoyoHokenKeisanKubun.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKoseiNenkinShoyoKeisanKbn
            // 
            this.txtKoseiNenkinShoyoKeisanKbn.AutoSizeFromLength = false;
            this.txtKoseiNenkinShoyoKeisanKbn.DisplayLength = null;
            this.txtKoseiNenkinShoyoKeisanKbn.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKoseiNenkinShoyoKeisanKbn.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKoseiNenkinShoyoKeisanKbn.Location = new System.Drawing.Point(114, 242);
            this.txtKoseiNenkinShoyoKeisanKbn.MaxLength = 15;
            this.txtKoseiNenkinShoyoKeisanKbn.Name = "txtKoseiNenkinShoyoKeisanKbn";
            this.txtKoseiNenkinShoyoKeisanKbn.Size = new System.Drawing.Size(18, 23);
            this.txtKoseiNenkinShoyoKeisanKbn.TabIndex = 8;
            this.txtKoseiNenkinShoyoKeisanKbn.Text = "0";
            this.txtKoseiNenkinShoyoKeisanKbn.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtKoseiNenkinShoyoKeisanKbn.Validating += new System.ComponentModel.CancelEventHandler(this.txtKoseiNenkinShoyoKeisanKbn_Validating);
            // 
            // txtKoseiNenkinHyojunHoshuGtgk
            // 
            this.txtKoseiNenkinHyojunHoshuGtgk.AutoSizeFromLength = false;
            this.txtKoseiNenkinHyojunHoshuGtgk.DisplayLength = null;
            this.txtKoseiNenkinHyojunHoshuGtgk.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKoseiNenkinHyojunHoshuGtgk.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKoseiNenkinHyojunHoshuGtgk.Location = new System.Drawing.Point(114, 196);
            this.txtKoseiNenkinHyojunHoshuGtgk.MaxLength = 5;
            this.txtKoseiNenkinHyojunHoshuGtgk.Name = "txtKoseiNenkinHyojunHoshuGtgk";
            this.txtKoseiNenkinHyojunHoshuGtgk.Size = new System.Drawing.Size(70, 23);
            this.txtKoseiNenkinHyojunHoshuGtgk.TabIndex = 6;
            this.txtKoseiNenkinHyojunHoshuGtgk.Text = "0";
            this.txtKoseiNenkinHyojunHoshuGtgk.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKoseiNenkinHyojunHoshuGtgk.Validating += new System.ComponentModel.CancelEventHandler(this.txtKoseiNenkinHyojunHoshuGtgk_Validating);
            // 
            // txtKoseiNenkinBango
            // 
            this.txtKoseiNenkinBango.AutoSizeFromLength = false;
            this.txtKoseiNenkinBango.DisplayLength = null;
            this.txtKoseiNenkinBango.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKoseiNenkinBango.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKoseiNenkinBango.Location = new System.Drawing.Point(114, 173);
            this.txtKoseiNenkinBango.MaxLength = 10;
            this.txtKoseiNenkinBango.Name = "txtKoseiNenkinBango";
            this.txtKoseiNenkinBango.Size = new System.Drawing.Size(116, 23);
            this.txtKoseiNenkinBango.TabIndex = 5;
            this.txtKoseiNenkinBango.Validating += new System.ComponentModel.CancelEventHandler(this.txtKoseiNenkinBango_Validating);
            // 
            // lblKoseiNenkinHokenryo
            // 
            this.lblKoseiNenkinHokenryo.BackColor = System.Drawing.Color.Silver;
            this.lblKoseiNenkinHokenryo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKoseiNenkinHokenryo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKoseiNenkinHokenryo.Location = new System.Drawing.Point(14, 219);
            this.lblKoseiNenkinHokenryo.Name = "lblKoseiNenkinHokenryo";
            this.lblKoseiNenkinHokenryo.Size = new System.Drawing.Size(100, 23);
            this.lblKoseiNenkinHokenryo.TabIndex = 16;
            this.lblKoseiNenkinHokenryo.Text = "保 険 料";
            this.lblKoseiNenkinHokenryo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKoseiNenkinHyojunHoshuGtgk
            // 
            this.lblKoseiNenkinHyojunHoshuGtgk.BackColor = System.Drawing.Color.Silver;
            this.lblKoseiNenkinHyojunHoshuGtgk.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKoseiNenkinHyojunHoshuGtgk.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKoseiNenkinHyojunHoshuGtgk.Location = new System.Drawing.Point(14, 196);
            this.lblKoseiNenkinHyojunHoshuGtgk.Name = "lblKoseiNenkinHyojunHoshuGtgk";
            this.lblKoseiNenkinHyojunHoshuGtgk.Size = new System.Drawing.Size(100, 23);
            this.lblKoseiNenkinHyojunHoshuGtgk.TabIndex = 20;
            this.lblKoseiNenkinHyojunHoshuGtgk.Text = "標準報酬月額";
            this.lblKoseiNenkinHyojunHoshuGtgk.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKoseiNenkinBango
            // 
            this.lblKoseiNenkinBango.BackColor = System.Drawing.Color.Silver;
            this.lblKoseiNenkinBango.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKoseiNenkinBango.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKoseiNenkinBango.Location = new System.Drawing.Point(14, 173);
            this.lblKoseiNenkinBango.Name = "lblKoseiNenkinBango";
            this.lblKoseiNenkinBango.Size = new System.Drawing.Size(100, 23);
            this.lblKoseiNenkinBango.TabIndex = 19;
            this.lblKoseiNenkinBango.Text = "年金番号";
            this.lblKoseiNenkinBango.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKenkoHokenryo
            // 
            this.txtKenkoHokenryo.AutoSizeFromLength = true;
            this.txtKenkoHokenryo.DisplayLength = null;
            this.txtKenkoHokenryo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKenkoHokenryo.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtKenkoHokenryo.Location = new System.Drawing.Point(114, 76);
            this.txtKenkoHokenryo.MaxLength = 9;
            this.txtKenkoHokenryo.Name = "txtKenkoHokenryo";
            this.txtKenkoHokenryo.Size = new System.Drawing.Size(69, 23);
            this.txtKenkoHokenryo.TabIndex = 2;
            this.txtKenkoHokenryo.Text = "0";
            this.txtKenkoHokenryo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKenkoHokenryo.Validating += new System.ComponentModel.CancelEventHandler(this.txtKenkoHokenryo_Validating);
            // 
            // lblKenkoHokenryo
            // 
            this.lblKenkoHokenryo.BackColor = System.Drawing.Color.Silver;
            this.lblKenkoHokenryo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKenkoHokenryo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKenkoHokenryo.Location = new System.Drawing.Point(14, 76);
            this.lblKenkoHokenryo.Name = "lblKenkoHokenryo";
            this.lblKenkoHokenryo.Size = new System.Drawing.Size(100, 23);
            this.lblKenkoHokenryo.TabIndex = 8;
            this.lblKenkoHokenryo.Text = "保 険 料";
            this.lblKenkoHokenryo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKenkoHokenHyojunHoshuGtgk
            // 
            this.txtKenkoHokenHyojunHoshuGtgk.AutoSizeFromLength = false;
            this.txtKenkoHokenHyojunHoshuGtgk.DisplayLength = null;
            this.txtKenkoHokenHyojunHoshuGtgk.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKenkoHokenHyojunHoshuGtgk.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKenkoHokenHyojunHoshuGtgk.Location = new System.Drawing.Point(114, 53);
            this.txtKenkoHokenHyojunHoshuGtgk.MaxLength = 5;
            this.txtKenkoHokenHyojunHoshuGtgk.Name = "txtKenkoHokenHyojunHoshuGtgk";
            this.txtKenkoHokenHyojunHoshuGtgk.Size = new System.Drawing.Size(70, 23);
            this.txtKenkoHokenHyojunHoshuGtgk.TabIndex = 1;
            this.txtKenkoHokenHyojunHoshuGtgk.Text = "0";
            this.txtKenkoHokenHyojunHoshuGtgk.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKenkoHokenHyojunHoshuGtgk.Validating += new System.ComponentModel.CancelEventHandler(this.txtKenkoHokenHyojunHoshuGtgk_Validating);
            // 
            // lblKenkoHokenHyojunHoshuGtgk
            // 
            this.lblKenkoHokenHyojunHoshuGtgk.BackColor = System.Drawing.Color.Silver;
            this.lblKenkoHokenHyojunHoshuGtgk.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKenkoHokenHyojunHoshuGtgk.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKenkoHokenHyojunHoshuGtgk.Location = new System.Drawing.Point(14, 53);
            this.lblKenkoHokenHyojunHoshuGtgk.Name = "lblKenkoHokenHyojunHoshuGtgk";
            this.lblKenkoHokenHyojunHoshuGtgk.Size = new System.Drawing.Size(100, 23);
            this.lblKenkoHokenHyojunHoshuGtgk.TabIndex = 2;
            this.lblKenkoHokenHyojunHoshuGtgk.Text = "標準報酬月額";
            this.lblKenkoHokenHyojunHoshuGtgk.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKenkoHokenshoBango
            // 
            this.txtKenkoHokenshoBango.AutoSizeFromLength = false;
            this.txtKenkoHokenshoBango.DisplayLength = null;
            this.txtKenkoHokenshoBango.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKenkoHokenshoBango.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKenkoHokenshoBango.Location = new System.Drawing.Point(114, 30);
            this.txtKenkoHokenshoBango.MaxLength = 10;
            this.txtKenkoHokenshoBango.Name = "txtKenkoHokenshoBango";
            this.txtKenkoHokenshoBango.Size = new System.Drawing.Size(116, 23);
            this.txtKenkoHokenshoBango.TabIndex = 0;
            this.txtKenkoHokenshoBango.Validating += new System.ComponentModel.CancelEventHandler(this.txtKenkoHokenshoBango_Validating);
            // 
            // lblKenkoHokenshoBango
            // 
            this.lblKenkoHokenshoBango.BackColor = System.Drawing.Color.Silver;
            this.lblKenkoHokenshoBango.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKenkoHokenshoBango.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKenkoHokenshoBango.Location = new System.Drawing.Point(14, 30);
            this.lblKenkoHokenshoBango.Name = "lblKenkoHokenshoBango";
            this.lblKenkoHokenshoBango.Size = new System.Drawing.Size(100, 23);
            this.lblKenkoHokenshoBango.TabIndex = 0;
            this.lblKenkoHokenshoBango.Text = "保険証番号";
            this.lblKenkoHokenshoBango.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblJuminzei
            // 
            this.lblJuminzei.BackColor = System.Drawing.Color.Transparent;
            this.lblJuminzei.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJuminzei.Location = new System.Drawing.Point(341, 150);
            this.lblJuminzei.Name = "lblJuminzei";
            this.lblJuminzei.Size = new System.Drawing.Size(136, 23);
            this.lblJuminzei.TabIndex = 51;
            this.lblJuminzei.Text = "【住民税】";
            this.lblJuminzei.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.panel7);
            this.tabPage3.Location = new System.Drawing.Point(4, 29);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(721, 395);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "F10 所得税";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.PeachPuff;
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel7.Controls.Add(this.label93);
            this.panel7.Controls.Add(this.txtHiJukyoShinzoku);
            this.panel7.Controls.Add(this.label94);
            this.panel7.Controls.Add(this.lblFuyoHaigushaJukyo);
            this.panel7.Controls.Add(this.txtFuyoHaigushaNm);
            this.panel7.Controls.Add(this.lblFuyoHaigushaNm);
            this.panel7.Controls.Add(this.label77);
            this.panel7.Controls.Add(this.txtFuyoHaigushaJukyo);
            this.panel7.Controls.Add(this.button2);
            this.panel7.Controls.Add(this.txtFuyoHaigushaMyNumberDm);
            this.panel7.Controls.Add(this.txtFuyoHaigushaMyNumber);
            this.panel7.Controls.Add(this.label102);
            this.panel7.Controls.Add(this.pnlFuyoHaigushaSeinengappi);
            this.panel7.Controls.Add(this.lblFuyoHaigushaSeinengappi);
            this.panel7.Controls.Add(this.txtFuyoHaigushaKanaNm);
            this.panel7.Controls.Add(this.lblFuyoHaigushaKanaNm);
            this.panel7.Controls.Add(this.label89);
            this.panel7.Controls.Add(this.lblShogaishaTokubetsuNm);
            this.panel7.Controls.Add(this.txtShogaishaTokubetsu);
            this.panel7.Controls.Add(this.lblShogaishaTokubetsu);
            this.panel7.Controls.Add(this.lblShogaisha);
            this.panel7.Controls.Add(this.lblFuyo);
            this.panel7.Controls.Add(this.txtKyuyoShubetsu);
            this.panel7.Controls.Add(this.txtTekiyo2);
            this.panel7.Controls.Add(this.txtTekiyo1);
            this.panel7.Controls.Add(this.lblKyuyoShubetsu);
            this.panel7.Controls.Add(this.lblTekiyo2);
            this.panel7.Controls.Add(this.lblTekiyo1);
            this.panel7.Controls.Add(this.Sai16MimanNm);
            this.panel7.Controls.Add(this.txtSai16Miman);
            this.panel7.Controls.Add(this.lblSai16Miman);
            this.panel7.Controls.Add(this.lblHaigushaKubunNm);
            this.panel7.Controls.Add(this.txtHaigushaKubun);
            this.panel7.Controls.Add(this.lblHaigushaKubun);
            this.panel7.Controls.Add(this.lblGaikokujinNm);
            this.panel7.Controls.Add(this.txtGaikokujin);
            this.panel7.Controls.Add(this.lblGaikokujin);
            this.panel7.Controls.Add(this.lblSaigaishaNm);
            this.panel7.Controls.Add(this.txtSaigaisha);
            this.panel7.Controls.Add(this.lblSaigaisha);
            this.panel7.Controls.Add(this.lblShiboTaishokuNm);
            this.panel7.Controls.Add(this.txtShiboTaishoku);
            this.panel7.Controls.Add(this.lblShiboTaishoku);
            this.panel7.Controls.Add(this.lblMiseinenshaNm);
            this.panel7.Controls.Add(this.txtMiseinensha);
            this.panel7.Controls.Add(this.lblMiseinensha);
            this.panel7.Controls.Add(this.lblHonninKubun3Nm);
            this.panel7.Controls.Add(this.txtHonninKubun3);
            this.panel7.Controls.Add(this.lblHonninKubun3);
            this.panel7.Controls.Add(this.lblHonninKubun2Nm);
            this.panel7.Controls.Add(this.txtHonninKubun2);
            this.panel7.Controls.Add(this.lblHonninKubun2);
            this.panel7.Controls.Add(this.ShogaishaIppanNm);
            this.panel7.Controls.Add(this.txtShogaishaIppan);
            this.panel7.Controls.Add(this.lblShogaishaIppan);
            this.panel7.Controls.Add(this.lblFuyoDokyoTokushoIppanNm);
            this.panel7.Controls.Add(this.txtFuyoDokyoTokushoIppan);
            this.panel7.Controls.Add(this.lblFuyoDokyoTokushoIppan);
            this.panel7.Controls.Add(this.lblFuyoRojin);
            this.panel7.Controls.Add(this.lblNenmatsuChoseiNm);
            this.panel7.Controls.Add(this.lblZeigakuHyoNm);
            this.panel7.Controls.Add(this.txtNenmatsuChosei);
            this.panel7.Controls.Add(this.lblFuyoRojinDokyoRoshintoNm);
            this.panel7.Controls.Add(this.txtFuyoRojinDokyoRoshinto);
            this.panel7.Controls.Add(this.lblFuyoRojinDokyoRoshinto);
            this.panel7.Controls.Add(this.lblFuyoRojinDokyoRoshintoIgaiNm);
            this.panel7.Controls.Add(this.txtFuyoRojinDokyoRoshintoIgai);
            this.panel7.Controls.Add(this.lblFuyoRojinDokyoRoshintoIgai);
            this.panel7.Controls.Add(this.lblFuyoTokuteiNm);
            this.panel7.Controls.Add(this.txtFuyoTokutei);
            this.panel7.Controls.Add(this.lblFuyoTokutei);
            this.panel7.Controls.Add(this.lblFuyoIppanNm);
            this.panel7.Controls.Add(this.txtFuyoIppan);
            this.panel7.Controls.Add(this.lblFuyoIppan);
            this.panel7.Controls.Add(this.lblHonninKubun1Nm);
            this.panel7.Controls.Add(this.txtHonninKubun1);
            this.panel7.Controls.Add(this.lblHonninKubun1);
            this.panel7.Controls.Add(this.lblNenmatsuChosei);
            this.panel7.Controls.Add(this.txtZeigakuHyo);
            this.panel7.Controls.Add(this.lblZeigakuHyo);
            this.panel7.Controls.Add(this.lblGensenchoshuhyo);
            this.panel7.Location = new System.Drawing.Point(20, 18);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(680, 359);
            this.panel7.TabIndex = 5;
            // 
            // label93
            // 
            this.label93.BackColor = System.Drawing.Color.Silver;
            this.label93.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label93.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label93.Location = new System.Drawing.Point(441, 145);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(34, 23);
            this.label93.TabIndex = 1002;
            this.label93.Text = "人";
            this.label93.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtHiJukyoShinzoku
            // 
            this.txtHiJukyoShinzoku.AutoSizeFromLength = true;
            this.txtHiJukyoShinzoku.DisplayLength = null;
            this.txtHiJukyoShinzoku.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtHiJukyoShinzoku.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtHiJukyoShinzoku.Location = new System.Drawing.Point(409, 145);
            this.txtHiJukyoShinzoku.MaxLength = 2;
            this.txtHiJukyoShinzoku.Name = "txtHiJukyoShinzoku";
            this.txtHiJukyoShinzoku.Size = new System.Drawing.Size(32, 23);
            this.txtHiJukyoShinzoku.TabIndex = 1000;
            this.txtHiJukyoShinzoku.Text = "0";
            this.txtHiJukyoShinzoku.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label94
            // 
            this.label94.BackColor = System.Drawing.Color.Silver;
            this.label94.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label94.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label94.Location = new System.Drawing.Point(274, 145);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(135, 23);
            this.label94.TabIndex = 1001;
            this.label94.Text = "非住居者親族数";
            this.label94.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFuyoHaigushaJukyo
            // 
            this.lblFuyoHaigushaJukyo.BackColor = System.Drawing.Color.Silver;
            this.lblFuyoHaigushaJukyo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFuyoHaigushaJukyo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFuyoHaigushaJukyo.Location = new System.Drawing.Point(381, 243);
            this.lblFuyoHaigushaJukyo.Name = "lblFuyoHaigushaJukyo";
            this.lblFuyoHaigushaJukyo.Size = new System.Drawing.Size(68, 23);
            this.lblFuyoHaigushaJukyo.TabIndex = 998;
            this.lblFuyoHaigushaJukyo.Text = "住　　居";
            this.lblFuyoHaigushaJukyo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFuyoHaigushaNm
            // 
            this.txtFuyoHaigushaNm.AutoSizeFromLength = false;
            this.txtFuyoHaigushaNm.DisplayLength = null;
            this.txtFuyoHaigushaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoHaigushaNm.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtFuyoHaigushaNm.Location = new System.Drawing.Point(448, 196);
            this.txtFuyoHaigushaNm.MaxLength = 30;
            this.txtFuyoHaigushaNm.Name = "txtFuyoHaigushaNm";
            this.txtFuyoHaigushaNm.Size = new System.Drawing.Size(219, 23);
            this.txtFuyoHaigushaNm.TabIndex = 986;
            // 
            // lblFuyoHaigushaNm
            // 
            this.lblFuyoHaigushaNm.BackColor = System.Drawing.Color.Silver;
            this.lblFuyoHaigushaNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFuyoHaigushaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFuyoHaigushaNm.Location = new System.Drawing.Point(381, 197);
            this.lblFuyoHaigushaNm.Name = "lblFuyoHaigushaNm";
            this.lblFuyoHaigushaNm.Size = new System.Drawing.Size(68, 23);
            this.lblFuyoHaigushaNm.TabIndex = 987;
            this.lblFuyoHaigushaNm.Text = "氏　　名";
            this.lblFuyoHaigushaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label77
            // 
            this.label77.BackColor = System.Drawing.Color.Silver;
            this.label77.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label77.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label77.Location = new System.Drawing.Point(466, 243);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(155, 23);
            this.label77.TabIndex = 996;
            this.label77.Text = "0:住居者　1:非住居者";
            this.label77.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFuyoHaigushaJukyo
            // 
            this.txtFuyoHaigushaJukyo.AutoSizeFromLength = true;
            this.txtFuyoHaigushaJukyo.DisplayLength = null;
            this.txtFuyoHaigushaJukyo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoHaigushaJukyo.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtFuyoHaigushaJukyo.Location = new System.Drawing.Point(448, 243);
            this.txtFuyoHaigushaJukyo.MaxLength = 1;
            this.txtFuyoHaigushaJukyo.Name = "txtFuyoHaigushaJukyo";
            this.txtFuyoHaigushaJukyo.Size = new System.Drawing.Size(18, 23);
            this.txtFuyoHaigushaJukyo.TabIndex = 997;
            this.txtFuyoHaigushaJukyo.Text = "0";
            this.txtFuyoHaigushaJukyo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(595, 291);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 995;
            this.button2.Text = "公開";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Visible = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // txtFuyoHaigushaMyNumberDm
            // 
            this.txtFuyoHaigushaMyNumberDm.AutoSizeFromLength = false;
            this.txtFuyoHaigushaMyNumberDm.DisplayLength = null;
            this.txtFuyoHaigushaMyNumberDm.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoHaigushaMyNumberDm.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtFuyoHaigushaMyNumberDm.Location = new System.Drawing.Point(481, 291);
            this.txtFuyoHaigushaMyNumberDm.MaxLength = 15;
            this.txtFuyoHaigushaMyNumberDm.Name = "txtFuyoHaigushaMyNumberDm";
            this.txtFuyoHaigushaMyNumberDm.ReadOnly = true;
            this.txtFuyoHaigushaMyNumberDm.Size = new System.Drawing.Size(114, 23);
            this.txtFuyoHaigushaMyNumberDm.TabIndex = 994;
            this.txtFuyoHaigushaMyNumberDm.Text = "************";
            // 
            // txtFuyoHaigushaMyNumber
            // 
            this.txtFuyoHaigushaMyNumber.AutoSizeFromLength = false;
            this.txtFuyoHaigushaMyNumber.DisplayLength = null;
            this.txtFuyoHaigushaMyNumber.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoHaigushaMyNumber.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtFuyoHaigushaMyNumber.Location = new System.Drawing.Point(481, 291);
            this.txtFuyoHaigushaMyNumber.MaxLength = 12;
            this.txtFuyoHaigushaMyNumber.Name = "txtFuyoHaigushaMyNumber";
            this.txtFuyoHaigushaMyNumber.ReadOnly = true;
            this.txtFuyoHaigushaMyNumber.Size = new System.Drawing.Size(114, 23);
            this.txtFuyoHaigushaMyNumber.TabIndex = 992;
            // 
            // label102
            // 
            this.label102.BackColor = System.Drawing.Color.Silver;
            this.label102.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label102.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label102.Location = new System.Drawing.Point(381, 291);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(100, 23);
            this.label102.TabIndex = 993;
            this.label102.Text = "マイナンバー";
            this.label102.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlFuyoHaigushaSeinengappi
            // 
            this.pnlFuyoHaigushaSeinengappi.BackColor = System.Drawing.Color.Silver;
            this.pnlFuyoHaigushaSeinengappi.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlFuyoHaigushaSeinengappi.Controls.Add(this.lblFuyoHaigushaDaySeinengappi);
            this.pnlFuyoHaigushaSeinengappi.Controls.Add(this.txtFuyoHaigushaDaySeinengappi);
            this.pnlFuyoHaigushaSeinengappi.Controls.Add(this.lblFuyoHaigushaMonthSeinengappi);
            this.pnlFuyoHaigushaSeinengappi.Controls.Add(this.lblFuyoHaigushaGengoYearSeinengappi);
            this.pnlFuyoHaigushaSeinengappi.Controls.Add(this.txtFuyoHaigushaMonthSeinengappi);
            this.pnlFuyoHaigushaSeinengappi.Controls.Add(this.lblFuyoHaigushaGengoSeinengappi);
            this.pnlFuyoHaigushaSeinengappi.Controls.Add(this.txtFuyoHaigushaGengoYearSeinengappi);
            this.pnlFuyoHaigushaSeinengappi.Location = new System.Drawing.Point(448, 266);
            this.pnlFuyoHaigushaSeinengappi.Name = "pnlFuyoHaigushaSeinengappi";
            this.pnlFuyoHaigushaSeinengappi.Size = new System.Drawing.Size(219, 25);
            this.pnlFuyoHaigushaSeinengappi.TabIndex = 990;
            // 
            // lblFuyoHaigushaDaySeinengappi
            // 
            this.lblFuyoHaigushaDaySeinengappi.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFuyoHaigushaDaySeinengappi.Location = new System.Drawing.Point(164, 0);
            this.lblFuyoHaigushaDaySeinengappi.Name = "lblFuyoHaigushaDaySeinengappi";
            this.lblFuyoHaigushaDaySeinengappi.Size = new System.Drawing.Size(21, 23);
            this.lblFuyoHaigushaDaySeinengappi.TabIndex = 14;
            this.lblFuyoHaigushaDaySeinengappi.Text = "日";
            this.lblFuyoHaigushaDaySeinengappi.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtFuyoHaigushaDaySeinengappi
            // 
            this.txtFuyoHaigushaDaySeinengappi.AutoSizeFromLength = false;
            this.txtFuyoHaigushaDaySeinengappi.DisplayLength = null;
            this.txtFuyoHaigushaDaySeinengappi.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoHaigushaDaySeinengappi.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtFuyoHaigushaDaySeinengappi.Location = new System.Drawing.Point(139, 0);
            this.txtFuyoHaigushaDaySeinengappi.MaxLength = 2;
            this.txtFuyoHaigushaDaySeinengappi.Name = "txtFuyoHaigushaDaySeinengappi";
            this.txtFuyoHaigushaDaySeinengappi.Size = new System.Drawing.Size(25, 23);
            this.txtFuyoHaigushaDaySeinengappi.TabIndex = 2;
            this.txtFuyoHaigushaDaySeinengappi.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFuyoHaigushaDaySeinengappi.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LastControl_KeyDown);
            // 
            // lblFuyoHaigushaMonthSeinengappi
            // 
            this.lblFuyoHaigushaMonthSeinengappi.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFuyoHaigushaMonthSeinengappi.Location = new System.Drawing.Point(118, 0);
            this.lblFuyoHaigushaMonthSeinengappi.Name = "lblFuyoHaigushaMonthSeinengappi";
            this.lblFuyoHaigushaMonthSeinengappi.Size = new System.Drawing.Size(21, 23);
            this.lblFuyoHaigushaMonthSeinengappi.TabIndex = 12;
            this.lblFuyoHaigushaMonthSeinengappi.Text = "月";
            this.lblFuyoHaigushaMonthSeinengappi.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblFuyoHaigushaGengoYearSeinengappi
            // 
            this.lblFuyoHaigushaGengoYearSeinengappi.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFuyoHaigushaGengoYearSeinengappi.Location = new System.Drawing.Point(72, 0);
            this.lblFuyoHaigushaGengoYearSeinengappi.Name = "lblFuyoHaigushaGengoYearSeinengappi";
            this.lblFuyoHaigushaGengoYearSeinengappi.Size = new System.Drawing.Size(21, 23);
            this.lblFuyoHaigushaGengoYearSeinengappi.TabIndex = 2;
            this.lblFuyoHaigushaGengoYearSeinengappi.Text = "年";
            this.lblFuyoHaigushaGengoYearSeinengappi.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtFuyoHaigushaMonthSeinengappi
            // 
            this.txtFuyoHaigushaMonthSeinengappi.AutoSizeFromLength = false;
            this.txtFuyoHaigushaMonthSeinengappi.DisplayLength = null;
            this.txtFuyoHaigushaMonthSeinengappi.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoHaigushaMonthSeinengappi.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtFuyoHaigushaMonthSeinengappi.Location = new System.Drawing.Point(93, 0);
            this.txtFuyoHaigushaMonthSeinengappi.MaxLength = 2;
            this.txtFuyoHaigushaMonthSeinengappi.Name = "txtFuyoHaigushaMonthSeinengappi";
            this.txtFuyoHaigushaMonthSeinengappi.Size = new System.Drawing.Size(25, 23);
            this.txtFuyoHaigushaMonthSeinengappi.TabIndex = 1;
            this.txtFuyoHaigushaMonthSeinengappi.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblFuyoHaigushaGengoSeinengappi
            // 
            this.lblFuyoHaigushaGengoSeinengappi.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFuyoHaigushaGengoSeinengappi.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFuyoHaigushaGengoSeinengappi.Location = new System.Drawing.Point(0, 0);
            this.lblFuyoHaigushaGengoSeinengappi.Name = "lblFuyoHaigushaGengoSeinengappi";
            this.lblFuyoHaigushaGengoSeinengappi.Size = new System.Drawing.Size(47, 23);
            this.lblFuyoHaigushaGengoSeinengappi.TabIndex = 0;
            this.lblFuyoHaigushaGengoSeinengappi.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtFuyoHaigushaGengoYearSeinengappi
            // 
            this.txtFuyoHaigushaGengoYearSeinengappi.AutoSizeFromLength = false;
            this.txtFuyoHaigushaGengoYearSeinengappi.DisplayLength = null;
            this.txtFuyoHaigushaGengoYearSeinengappi.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoHaigushaGengoYearSeinengappi.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtFuyoHaigushaGengoYearSeinengappi.Location = new System.Drawing.Point(47, 0);
            this.txtFuyoHaigushaGengoYearSeinengappi.MaxLength = 2;
            this.txtFuyoHaigushaGengoYearSeinengappi.Name = "txtFuyoHaigushaGengoYearSeinengappi";
            this.txtFuyoHaigushaGengoYearSeinengappi.Size = new System.Drawing.Size(25, 23);
            this.txtFuyoHaigushaGengoYearSeinengappi.TabIndex = 0;
            this.txtFuyoHaigushaGengoYearSeinengappi.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblFuyoHaigushaSeinengappi
            // 
            this.lblFuyoHaigushaSeinengappi.BackColor = System.Drawing.Color.Silver;
            this.lblFuyoHaigushaSeinengappi.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFuyoHaigushaSeinengappi.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFuyoHaigushaSeinengappi.Location = new System.Drawing.Point(381, 266);
            this.lblFuyoHaigushaSeinengappi.Name = "lblFuyoHaigushaSeinengappi";
            this.lblFuyoHaigushaSeinengappi.Size = new System.Drawing.Size(68, 25);
            this.lblFuyoHaigushaSeinengappi.TabIndex = 991;
            this.lblFuyoHaigushaSeinengappi.Text = "生年月日";
            this.lblFuyoHaigushaSeinengappi.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFuyoHaigushaKanaNm
            // 
            this.txtFuyoHaigushaKanaNm.AutoSizeFromLength = false;
            this.txtFuyoHaigushaKanaNm.DisplayLength = null;
            this.txtFuyoHaigushaKanaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoHaigushaKanaNm.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtFuyoHaigushaKanaNm.Location = new System.Drawing.Point(448, 219);
            this.txtFuyoHaigushaKanaNm.MaxLength = 30;
            this.txtFuyoHaigushaKanaNm.Name = "txtFuyoHaigushaKanaNm";
            this.txtFuyoHaigushaKanaNm.Size = new System.Drawing.Size(219, 23);
            this.txtFuyoHaigushaKanaNm.TabIndex = 988;
            // 
            // lblFuyoHaigushaKanaNm
            // 
            this.lblFuyoHaigushaKanaNm.BackColor = System.Drawing.Color.Silver;
            this.lblFuyoHaigushaKanaNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFuyoHaigushaKanaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFuyoHaigushaKanaNm.Location = new System.Drawing.Point(381, 220);
            this.lblFuyoHaigushaKanaNm.Name = "lblFuyoHaigushaKanaNm";
            this.lblFuyoHaigushaKanaNm.Size = new System.Drawing.Size(68, 23);
            this.lblFuyoHaigushaKanaNm.TabIndex = 989;
            this.lblFuyoHaigushaKanaNm.Text = "フリガナ";
            this.lblFuyoHaigushaKanaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label89
            // 
            this.label89.BackColor = System.Drawing.Color.PeachPuff;
            this.label89.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label89.Location = new System.Drawing.Point(380, 174);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(177, 23);
            this.label89.TabIndex = 999;
            this.label89.Text = "【控除対象配偶者】";
            this.label89.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShogaishaTokubetsuNm
            // 
            this.lblShogaishaTokubetsuNm.BackColor = System.Drawing.Color.Silver;
            this.lblShogaishaTokubetsuNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShogaishaTokubetsuNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShogaishaTokubetsuNm.Location = new System.Drawing.Point(625, 76);
            this.lblShogaishaTokubetsuNm.Name = "lblShogaishaTokubetsuNm";
            this.lblShogaishaTokubetsuNm.Size = new System.Drawing.Size(34, 23);
            this.lblShogaishaTokubetsuNm.TabIndex = 98;
            this.lblShogaishaTokubetsuNm.Text = "人";
            this.lblShogaishaTokubetsuNm.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtShogaishaTokubetsu
            // 
            this.txtShogaishaTokubetsu.AutoSizeFromLength = true;
            this.txtShogaishaTokubetsu.DisplayLength = null;
            this.txtShogaishaTokubetsu.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShogaishaTokubetsu.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtShogaishaTokubetsu.Location = new System.Drawing.Point(593, 76);
            this.txtShogaishaTokubetsu.MaxLength = 2;
            this.txtShogaishaTokubetsu.Name = "txtShogaishaTokubetsu";
            this.txtShogaishaTokubetsu.Size = new System.Drawing.Size(32, 23);
            this.txtShogaishaTokubetsu.TabIndex = 17;
            this.txtShogaishaTokubetsu.Text = "0";
            this.txtShogaishaTokubetsu.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShogaishaTokubetsu.Validating += new System.ComponentModel.CancelEventHandler(this.txtShogaishaTokubetsu_Validating);
            // 
            // lblShogaishaTokubetsu
            // 
            this.lblShogaishaTokubetsu.BackColor = System.Drawing.Color.Silver;
            this.lblShogaishaTokubetsu.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShogaishaTokubetsu.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShogaishaTokubetsu.Location = new System.Drawing.Point(483, 76);
            this.lblShogaishaTokubetsu.Name = "lblShogaishaTokubetsu";
            this.lblShogaishaTokubetsu.Size = new System.Drawing.Size(110, 23);
            this.lblShogaishaTokubetsu.TabIndex = 96;
            this.lblShogaishaTokubetsu.Text = "特　　　　別";
            this.lblShogaishaTokubetsu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShogaisha
            // 
            this.lblShogaisha.BackColor = System.Drawing.Color.Transparent;
            this.lblShogaisha.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShogaisha.Location = new System.Drawing.Point(480, 7);
            this.lblShogaisha.Name = "lblShogaisha";
            this.lblShogaisha.Size = new System.Drawing.Size(136, 23);
            this.lblShogaisha.TabIndex = 94;
            this.lblShogaisha.Text = "【障害者】";
            this.lblShogaisha.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFuyo
            // 
            this.lblFuyo.BackColor = System.Drawing.Color.Transparent;
            this.lblFuyo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFuyo.Location = new System.Drawing.Point(273, 7);
            this.lblFuyo.Name = "lblFuyo";
            this.lblFuyo.Size = new System.Drawing.Size(136, 23);
            this.lblFuyo.TabIndex = 93;
            this.lblFuyo.Text = "【扶養親族】";
            this.lblFuyo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKyuyoShubetsu
            // 
            this.txtKyuyoShubetsu.AutoSizeFromLength = false;
            this.txtKyuyoShubetsu.DisplayLength = null;
            this.txtKyuyoShubetsu.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKyuyoShubetsu.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtKyuyoShubetsu.Location = new System.Drawing.Point(93, 328);
            this.txtKyuyoShubetsu.MaxLength = 20;
            this.txtKyuyoShubetsu.Name = "txtKyuyoShubetsu";
            this.txtKyuyoShubetsu.Size = new System.Drawing.Size(173, 23);
            this.txtKyuyoShubetsu.TabIndex = 20;
            this.txtKyuyoShubetsu.Validating += new System.ComponentModel.CancelEventHandler(this.txtKyuyoShubetsu_Validating);
            // 
            // txtTekiyo2
            // 
            this.txtTekiyo2.AutoSizeFromLength = false;
            this.txtTekiyo2.DisplayLength = null;
            this.txtTekiyo2.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTekiyo2.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtTekiyo2.Location = new System.Drawing.Point(93, 305);
            this.txtTekiyo2.MaxLength = 50;
            this.txtTekiyo2.Name = "txtTekiyo2";
            this.txtTekiyo2.Size = new System.Drawing.Size(282, 23);
            this.txtTekiyo2.TabIndex = 19;
            this.txtTekiyo2.Validating += new System.ComponentModel.CancelEventHandler(this.txtTekiyo2_Validating);
            // 
            // txtTekiyo1
            // 
            this.txtTekiyo1.AutoSizeFromLength = false;
            this.txtTekiyo1.DisplayLength = null;
            this.txtTekiyo1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTekiyo1.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtTekiyo1.Location = new System.Drawing.Point(93, 282);
            this.txtTekiyo1.MaxLength = 50;
            this.txtTekiyo1.Name = "txtTekiyo1";
            this.txtTekiyo1.Size = new System.Drawing.Size(282, 23);
            this.txtTekiyo1.TabIndex = 18;
            this.txtTekiyo1.Validating += new System.ComponentModel.CancelEventHandler(this.txtTekiyo1_Validating);
            // 
            // lblKyuyoShubetsu
            // 
            this.lblKyuyoShubetsu.BackColor = System.Drawing.Color.Silver;
            this.lblKyuyoShubetsu.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKyuyoShubetsu.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKyuyoShubetsu.Location = new System.Drawing.Point(14, 329);
            this.lblKyuyoShubetsu.Name = "lblKyuyoShubetsu";
            this.lblKyuyoShubetsu.Size = new System.Drawing.Size(79, 23);
            this.lblKyuyoShubetsu.TabIndex = 92;
            this.lblKyuyoShubetsu.Text = "種　別";
            this.lblKyuyoShubetsu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTekiyo2
            // 
            this.lblTekiyo2.BackColor = System.Drawing.Color.Silver;
            this.lblTekiyo2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTekiyo2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTekiyo2.Location = new System.Drawing.Point(14, 306);
            this.lblTekiyo2.Name = "lblTekiyo2";
            this.lblTekiyo2.Size = new System.Drawing.Size(79, 23);
            this.lblTekiyo2.TabIndex = 91;
            this.lblTekiyo2.Text = "摘要②";
            this.lblTekiyo2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTekiyo1
            // 
            this.lblTekiyo1.BackColor = System.Drawing.Color.Silver;
            this.lblTekiyo1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTekiyo1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTekiyo1.Location = new System.Drawing.Point(14, 283);
            this.lblTekiyo1.Name = "lblTekiyo1";
            this.lblTekiyo1.Size = new System.Drawing.Size(79, 23);
            this.lblTekiyo1.TabIndex = 90;
            this.lblTekiyo1.Text = "摘要①";
            this.lblTekiyo1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Sai16MimanNm
            // 
            this.Sai16MimanNm.BackColor = System.Drawing.Color.Silver;
            this.Sai16MimanNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Sai16MimanNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Sai16MimanNm.Location = new System.Drawing.Point(441, 122);
            this.Sai16MimanNm.Name = "Sai16MimanNm";
            this.Sai16MimanNm.Size = new System.Drawing.Size(34, 23);
            this.Sai16MimanNm.TabIndex = 86;
            this.Sai16MimanNm.Text = "人";
            this.Sai16MimanNm.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtSai16Miman
            // 
            this.txtSai16Miman.AutoSizeFromLength = true;
            this.txtSai16Miman.DisplayLength = null;
            this.txtSai16Miman.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSai16Miman.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtSai16Miman.Location = new System.Drawing.Point(409, 122);
            this.txtSai16Miman.MaxLength = 2;
            this.txtSai16Miman.Name = "txtSai16Miman";
            this.txtSai16Miman.Size = new System.Drawing.Size(32, 23);
            this.txtSai16Miman.TabIndex = 14;
            this.txtSai16Miman.Text = "0";
            this.txtSai16Miman.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSai16Miman.Validating += new System.ComponentModel.CancelEventHandler(this.txtSai16Miman_Validating);
            // 
            // lblSai16Miman
            // 
            this.lblSai16Miman.BackColor = System.Drawing.Color.Silver;
            this.lblSai16Miman.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSai16Miman.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSai16Miman.Location = new System.Drawing.Point(274, 122);
            this.lblSai16Miman.Name = "lblSai16Miman";
            this.lblSai16Miman.Size = new System.Drawing.Size(135, 23);
            this.lblSai16Miman.TabIndex = 84;
            this.lblSai16Miman.Text = "１６歳未満";
            this.lblSai16Miman.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblHaigushaKubunNm
            // 
            this.lblHaigushaKubunNm.BackColor = System.Drawing.Color.Silver;
            this.lblHaigushaKubunNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblHaigushaKubunNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblHaigushaKubunNm.Location = new System.Drawing.Point(132, 229);
            this.lblHaigushaKubunNm.Name = "lblHaigushaKubunNm";
            this.lblHaigushaKubunNm.Size = new System.Drawing.Size(133, 23);
            this.lblHaigushaKubunNm.TabIndex = 80;
            this.lblHaigushaKubunNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtHaigushaKubun
            // 
            this.txtHaigushaKubun.AutoSizeFromLength = true;
            this.txtHaigushaKubun.DisplayLength = null;
            this.txtHaigushaKubun.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtHaigushaKubun.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtHaigushaKubun.Location = new System.Drawing.Point(114, 229);
            this.txtHaigushaKubun.MaxLength = 1;
            this.txtHaigushaKubun.Name = "txtHaigushaKubun";
            this.txtHaigushaKubun.Size = new System.Drawing.Size(18, 23);
            this.txtHaigushaKubun.TabIndex = 9;
            this.txtHaigushaKubun.Text = "0";
            this.txtHaigushaKubun.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtHaigushaKubun.Validating += new System.ComponentModel.CancelEventHandler(this.txtHaigushaKubun_Validating);
            // 
            // lblHaigushaKubun
            // 
            this.lblHaigushaKubun.BackColor = System.Drawing.Color.Silver;
            this.lblHaigushaKubun.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblHaigushaKubun.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblHaigushaKubun.Location = new System.Drawing.Point(14, 229);
            this.lblHaigushaKubun.Name = "lblHaigushaKubun";
            this.lblHaigushaKubun.Size = new System.Drawing.Size(100, 23);
            this.lblHaigushaKubun.TabIndex = 78;
            this.lblHaigushaKubun.Text = "配偶者区分";
            this.lblHaigushaKubun.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblGaikokujinNm
            // 
            this.lblGaikokujinNm.BackColor = System.Drawing.Color.Silver;
            this.lblGaikokujinNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGaikokujinNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGaikokujinNm.Location = new System.Drawing.Point(132, 201);
            this.lblGaikokujinNm.Name = "lblGaikokujinNm";
            this.lblGaikokujinNm.Size = new System.Drawing.Size(133, 23);
            this.lblGaikokujinNm.TabIndex = 74;
            this.lblGaikokujinNm.Text = "0:対象外　1:対象";
            this.lblGaikokujinNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtGaikokujin
            // 
            this.txtGaikokujin.AutoSizeFromLength = true;
            this.txtGaikokujin.DisplayLength = null;
            this.txtGaikokujin.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGaikokujin.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtGaikokujin.Location = new System.Drawing.Point(114, 201);
            this.txtGaikokujin.MaxLength = 1;
            this.txtGaikokujin.Name = "txtGaikokujin";
            this.txtGaikokujin.Size = new System.Drawing.Size(18, 23);
            this.txtGaikokujin.TabIndex = 8;
            this.txtGaikokujin.Text = "0";
            this.txtGaikokujin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtGaikokujin.Validating += new System.ComponentModel.CancelEventHandler(this.txtGaikokujin_Validating);
            // 
            // lblGaikokujin
            // 
            this.lblGaikokujin.BackColor = System.Drawing.Color.Silver;
            this.lblGaikokujin.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGaikokujin.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGaikokujin.Location = new System.Drawing.Point(14, 201);
            this.lblGaikokujin.Name = "lblGaikokujin";
            this.lblGaikokujin.Size = new System.Drawing.Size(100, 23);
            this.lblGaikokujin.TabIndex = 72;
            this.lblGaikokujin.Text = "外　国　人";
            this.lblGaikokujin.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSaigaishaNm
            // 
            this.lblSaigaishaNm.BackColor = System.Drawing.Color.Silver;
            this.lblSaigaishaNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSaigaishaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSaigaishaNm.Location = new System.Drawing.Point(132, 178);
            this.lblSaigaishaNm.Name = "lblSaigaishaNm";
            this.lblSaigaishaNm.Size = new System.Drawing.Size(133, 23);
            this.lblSaigaishaNm.TabIndex = 71;
            this.lblSaigaishaNm.Text = "0:対象外　1:対象";
            this.lblSaigaishaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSaigaisha
            // 
            this.txtSaigaisha.AutoSizeFromLength = true;
            this.txtSaigaisha.DisplayLength = null;
            this.txtSaigaisha.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSaigaisha.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtSaigaisha.Location = new System.Drawing.Point(114, 178);
            this.txtSaigaisha.MaxLength = 1;
            this.txtSaigaisha.Name = "txtSaigaisha";
            this.txtSaigaisha.Size = new System.Drawing.Size(18, 23);
            this.txtSaigaisha.TabIndex = 7;
            this.txtSaigaisha.Text = "0";
            this.txtSaigaisha.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtSaigaisha.Validating += new System.ComponentModel.CancelEventHandler(this.txtSaigaisha_Validating);
            // 
            // lblSaigaisha
            // 
            this.lblSaigaisha.BackColor = System.Drawing.Color.Silver;
            this.lblSaigaisha.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSaigaisha.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSaigaisha.Location = new System.Drawing.Point(14, 178);
            this.lblSaigaisha.Name = "lblSaigaisha";
            this.lblSaigaisha.Size = new System.Drawing.Size(100, 23);
            this.lblSaigaisha.TabIndex = 69;
            this.lblSaigaisha.Text = "災　害　者";
            this.lblSaigaisha.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShiboTaishokuNm
            // 
            this.lblShiboTaishokuNm.BackColor = System.Drawing.Color.Silver;
            this.lblShiboTaishokuNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShiboTaishokuNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShiboTaishokuNm.Location = new System.Drawing.Point(132, 155);
            this.lblShiboTaishokuNm.Name = "lblShiboTaishokuNm";
            this.lblShiboTaishokuNm.Size = new System.Drawing.Size(133, 23);
            this.lblShiboTaishokuNm.TabIndex = 68;
            this.lblShiboTaishokuNm.Text = "0:対象外　1:対象";
            this.lblShiboTaishokuNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShiboTaishoku
            // 
            this.txtShiboTaishoku.AutoSizeFromLength = true;
            this.txtShiboTaishoku.DisplayLength = null;
            this.txtShiboTaishoku.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShiboTaishoku.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtShiboTaishoku.Location = new System.Drawing.Point(114, 155);
            this.txtShiboTaishoku.MaxLength = 1;
            this.txtShiboTaishoku.Name = "txtShiboTaishoku";
            this.txtShiboTaishoku.Size = new System.Drawing.Size(18, 23);
            this.txtShiboTaishoku.TabIndex = 6;
            this.txtShiboTaishoku.Text = "0";
            this.txtShiboTaishoku.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtShiboTaishoku.Validating += new System.ComponentModel.CancelEventHandler(this.txtShiboTaishoku_Validating);
            // 
            // lblShiboTaishoku
            // 
            this.lblShiboTaishoku.BackColor = System.Drawing.Color.Silver;
            this.lblShiboTaishoku.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShiboTaishoku.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShiboTaishoku.Location = new System.Drawing.Point(14, 155);
            this.lblShiboTaishoku.Name = "lblShiboTaishoku";
            this.lblShiboTaishoku.Size = new System.Drawing.Size(100, 23);
            this.lblShiboTaishoku.TabIndex = 66;
            this.lblShiboTaishoku.Text = "死 亡 退 職";
            this.lblShiboTaishoku.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMiseinenshaNm
            // 
            this.lblMiseinenshaNm.BackColor = System.Drawing.Color.Silver;
            this.lblMiseinenshaNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMiseinenshaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMiseinenshaNm.Location = new System.Drawing.Point(132, 132);
            this.lblMiseinenshaNm.Name = "lblMiseinenshaNm";
            this.lblMiseinenshaNm.Size = new System.Drawing.Size(133, 23);
            this.lblMiseinenshaNm.TabIndex = 65;
            this.lblMiseinenshaNm.Text = "0:対象外　1:対象";
            this.lblMiseinenshaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtMiseinensha
            // 
            this.txtMiseinensha.AutoSizeFromLength = true;
            this.txtMiseinensha.DisplayLength = null;
            this.txtMiseinensha.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMiseinensha.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtMiseinensha.Location = new System.Drawing.Point(114, 132);
            this.txtMiseinensha.MaxLength = 1;
            this.txtMiseinensha.Name = "txtMiseinensha";
            this.txtMiseinensha.Size = new System.Drawing.Size(18, 23);
            this.txtMiseinensha.TabIndex = 5;
            this.txtMiseinensha.Text = "0";
            this.txtMiseinensha.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtMiseinensha.Validating += new System.ComponentModel.CancelEventHandler(this.txtMiseinensha_Validating);
            // 
            // lblMiseinensha
            // 
            this.lblMiseinensha.BackColor = System.Drawing.Color.Silver;
            this.lblMiseinensha.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMiseinensha.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMiseinensha.Location = new System.Drawing.Point(14, 132);
            this.lblMiseinensha.Name = "lblMiseinensha";
            this.lblMiseinensha.Size = new System.Drawing.Size(100, 23);
            this.lblMiseinensha.TabIndex = 63;
            this.lblMiseinensha.Text = "未 成 年 者";
            this.lblMiseinensha.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblHonninKubun3Nm
            // 
            this.lblHonninKubun3Nm.BackColor = System.Drawing.Color.Silver;
            this.lblHonninKubun3Nm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblHonninKubun3Nm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblHonninKubun3Nm.Location = new System.Drawing.Point(132, 109);
            this.lblHonninKubun3Nm.Name = "lblHonninKubun3Nm";
            this.lblHonninKubun3Nm.Size = new System.Drawing.Size(133, 23);
            this.lblHonninKubun3Nm.TabIndex = 62;
            this.lblHonninKubun3Nm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtHonninKubun3
            // 
            this.txtHonninKubun3.AutoSizeFromLength = true;
            this.txtHonninKubun3.DisplayLength = null;
            this.txtHonninKubun3.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtHonninKubun3.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtHonninKubun3.Location = new System.Drawing.Point(114, 109);
            this.txtHonninKubun3.MaxLength = 1;
            this.txtHonninKubun3.Name = "txtHonninKubun3";
            this.txtHonninKubun3.Size = new System.Drawing.Size(18, 23);
            this.txtHonninKubun3.TabIndex = 4;
            this.txtHonninKubun3.Text = "0";
            this.txtHonninKubun3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtHonninKubun3.Validating += new System.ComponentModel.CancelEventHandler(this.txtHonninKubun3_Validating);
            // 
            // lblHonninKubun3
            // 
            this.lblHonninKubun3.BackColor = System.Drawing.Color.Silver;
            this.lblHonninKubun3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblHonninKubun3.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblHonninKubun3.Location = new System.Drawing.Point(14, 109);
            this.lblHonninKubun3.Name = "lblHonninKubun3";
            this.lblHonninKubun3.Size = new System.Drawing.Size(100, 23);
            this.lblHonninKubun3.TabIndex = 60;
            this.lblHonninKubun3.Text = "本人区分③";
            this.lblHonninKubun3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblHonninKubun2Nm
            // 
            this.lblHonninKubun2Nm.BackColor = System.Drawing.Color.Silver;
            this.lblHonninKubun2Nm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblHonninKubun2Nm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblHonninKubun2Nm.Location = new System.Drawing.Point(132, 86);
            this.lblHonninKubun2Nm.Name = "lblHonninKubun2Nm";
            this.lblHonninKubun2Nm.Size = new System.Drawing.Size(133, 23);
            this.lblHonninKubun2Nm.TabIndex = 59;
            this.lblHonninKubun2Nm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtHonninKubun2
            // 
            this.txtHonninKubun2.AutoSizeFromLength = true;
            this.txtHonninKubun2.DisplayLength = null;
            this.txtHonninKubun2.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtHonninKubun2.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtHonninKubun2.Location = new System.Drawing.Point(114, 86);
            this.txtHonninKubun2.MaxLength = 1;
            this.txtHonninKubun2.Name = "txtHonninKubun2";
            this.txtHonninKubun2.Size = new System.Drawing.Size(18, 23);
            this.txtHonninKubun2.TabIndex = 3;
            this.txtHonninKubun2.Text = "0";
            this.txtHonninKubun2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtHonninKubun2.Validating += new System.ComponentModel.CancelEventHandler(this.txtHonninKubun2_Validating);
            // 
            // lblHonninKubun2
            // 
            this.lblHonninKubun2.BackColor = System.Drawing.Color.Silver;
            this.lblHonninKubun2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblHonninKubun2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblHonninKubun2.Location = new System.Drawing.Point(14, 86);
            this.lblHonninKubun2.Name = "lblHonninKubun2";
            this.lblHonninKubun2.Size = new System.Drawing.Size(100, 23);
            this.lblHonninKubun2.TabIndex = 57;
            this.lblHonninKubun2.Text = "本人区分②";
            this.lblHonninKubun2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ShogaishaIppanNm
            // 
            this.ShogaishaIppanNm.BackColor = System.Drawing.Color.Silver;
            this.ShogaishaIppanNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.ShogaishaIppanNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ShogaishaIppanNm.Location = new System.Drawing.Point(625, 53);
            this.ShogaishaIppanNm.Name = "ShogaishaIppanNm";
            this.ShogaishaIppanNm.Size = new System.Drawing.Size(34, 23);
            this.ShogaishaIppanNm.TabIndex = 56;
            this.ShogaishaIppanNm.Text = "人";
            this.ShogaishaIppanNm.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtShogaishaIppan
            // 
            this.txtShogaishaIppan.AutoSizeFromLength = true;
            this.txtShogaishaIppan.DisplayLength = null;
            this.txtShogaishaIppan.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShogaishaIppan.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtShogaishaIppan.Location = new System.Drawing.Point(593, 53);
            this.txtShogaishaIppan.MaxLength = 2;
            this.txtShogaishaIppan.Name = "txtShogaishaIppan";
            this.txtShogaishaIppan.Size = new System.Drawing.Size(32, 23);
            this.txtShogaishaIppan.TabIndex = 16;
            this.txtShogaishaIppan.Text = "0";
            this.txtShogaishaIppan.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShogaishaIppan.Validating += new System.ComponentModel.CancelEventHandler(this.txtShogaishaIppan_Validating);
            // 
            // lblShogaishaIppan
            // 
            this.lblShogaishaIppan.BackColor = System.Drawing.Color.Silver;
            this.lblShogaishaIppan.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShogaishaIppan.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShogaishaIppan.Location = new System.Drawing.Point(483, 53);
            this.lblShogaishaIppan.Name = "lblShogaishaIppan";
            this.lblShogaishaIppan.Size = new System.Drawing.Size(110, 23);
            this.lblShogaishaIppan.TabIndex = 54;
            this.lblShogaishaIppan.Text = "一　　　　般";
            this.lblShogaishaIppan.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFuyoDokyoTokushoIppanNm
            // 
            this.lblFuyoDokyoTokushoIppanNm.BackColor = System.Drawing.Color.Silver;
            this.lblFuyoDokyoTokushoIppanNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFuyoDokyoTokushoIppanNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFuyoDokyoTokushoIppanNm.Location = new System.Drawing.Point(625, 30);
            this.lblFuyoDokyoTokushoIppanNm.Name = "lblFuyoDokyoTokushoIppanNm";
            this.lblFuyoDokyoTokushoIppanNm.Size = new System.Drawing.Size(34, 23);
            this.lblFuyoDokyoTokushoIppanNm.TabIndex = 53;
            this.lblFuyoDokyoTokushoIppanNm.Text = "人";
            this.lblFuyoDokyoTokushoIppanNm.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtFuyoDokyoTokushoIppan
            // 
            this.txtFuyoDokyoTokushoIppan.AutoSizeFromLength = true;
            this.txtFuyoDokyoTokushoIppan.DisplayLength = null;
            this.txtFuyoDokyoTokushoIppan.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoDokyoTokushoIppan.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtFuyoDokyoTokushoIppan.Location = new System.Drawing.Point(593, 30);
            this.txtFuyoDokyoTokushoIppan.MaxLength = 2;
            this.txtFuyoDokyoTokushoIppan.Name = "txtFuyoDokyoTokushoIppan";
            this.txtFuyoDokyoTokushoIppan.Size = new System.Drawing.Size(32, 23);
            this.txtFuyoDokyoTokushoIppan.TabIndex = 15;
            this.txtFuyoDokyoTokushoIppan.Text = "0";
            this.txtFuyoDokyoTokushoIppan.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFuyoDokyoTokushoIppan.Validating += new System.ComponentModel.CancelEventHandler(this.txtFuyoDokyoTokushoIppan_Validating);
            // 
            // lblFuyoDokyoTokushoIppan
            // 
            this.lblFuyoDokyoTokushoIppan.BackColor = System.Drawing.Color.Silver;
            this.lblFuyoDokyoTokushoIppan.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFuyoDokyoTokushoIppan.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFuyoDokyoTokushoIppan.Location = new System.Drawing.Point(483, 30);
            this.lblFuyoDokyoTokushoIppan.Name = "lblFuyoDokyoTokushoIppan";
            this.lblFuyoDokyoTokushoIppan.Size = new System.Drawing.Size(110, 23);
            this.lblFuyoDokyoTokushoIppan.TabIndex = 51;
            this.lblFuyoDokyoTokushoIppan.Text = "同居特別障害者";
            this.lblFuyoDokyoTokushoIppan.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFuyoRojin
            // 
            this.lblFuyoRojin.BackColor = System.Drawing.Color.Silver;
            this.lblFuyoRojin.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFuyoRojin.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFuyoRojin.Location = new System.Drawing.Point(274, 76);
            this.lblFuyoRojin.Name = "lblFuyoRojin";
            this.lblFuyoRojin.Size = new System.Drawing.Size(25, 46);
            this.lblFuyoRojin.TabIndex = 50;
            this.lblFuyoRojin.Text = "老人";
            this.lblFuyoRojin.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblNenmatsuChoseiNm
            // 
            this.lblNenmatsuChoseiNm.BackColor = System.Drawing.Color.Silver;
            this.lblNenmatsuChoseiNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblNenmatsuChoseiNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblNenmatsuChoseiNm.Location = new System.Drawing.Point(132, 36);
            this.lblNenmatsuChoseiNm.Name = "lblNenmatsuChoseiNm";
            this.lblNenmatsuChoseiNm.Size = new System.Drawing.Size(133, 23);
            this.lblNenmatsuChoseiNm.TabIndex = 49;
            this.lblNenmatsuChoseiNm.Text = "0:しない　1:する";
            this.lblNenmatsuChoseiNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblZeigakuHyoNm
            // 
            this.lblZeigakuHyoNm.BackColor = System.Drawing.Color.Silver;
            this.lblZeigakuHyoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblZeigakuHyoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblZeigakuHyoNm.Location = new System.Drawing.Point(132, 13);
            this.lblZeigakuHyoNm.Name = "lblZeigakuHyoNm";
            this.lblZeigakuHyoNm.Size = new System.Drawing.Size(133, 23);
            this.lblZeigakuHyoNm.TabIndex = 48;
            this.lblZeigakuHyoNm.Text = "0:なし 1:甲 2:乙";
            this.lblZeigakuHyoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtNenmatsuChosei
            // 
            this.txtNenmatsuChosei.AutoSizeFromLength = false;
            this.txtNenmatsuChosei.DisplayLength = null;
            this.txtNenmatsuChosei.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtNenmatsuChosei.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtNenmatsuChosei.Location = new System.Drawing.Point(114, 36);
            this.txtNenmatsuChosei.MaxLength = 30;
            this.txtNenmatsuChosei.Name = "txtNenmatsuChosei";
            this.txtNenmatsuChosei.Size = new System.Drawing.Size(18, 23);
            this.txtNenmatsuChosei.TabIndex = 1;
            this.txtNenmatsuChosei.Text = "0";
            this.txtNenmatsuChosei.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtNenmatsuChosei.Validating += new System.ComponentModel.CancelEventHandler(this.txtNenmatsuChosei_Validating);
            // 
            // lblFuyoRojinDokyoRoshintoNm
            // 
            this.lblFuyoRojinDokyoRoshintoNm.BackColor = System.Drawing.Color.Silver;
            this.lblFuyoRojinDokyoRoshintoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFuyoRojinDokyoRoshintoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFuyoRojinDokyoRoshintoNm.Location = new System.Drawing.Point(441, 99);
            this.lblFuyoRojinDokyoRoshintoNm.Name = "lblFuyoRojinDokyoRoshintoNm";
            this.lblFuyoRojinDokyoRoshintoNm.Size = new System.Drawing.Size(34, 23);
            this.lblFuyoRojinDokyoRoshintoNm.TabIndex = 38;
            this.lblFuyoRojinDokyoRoshintoNm.Text = "人";
            this.lblFuyoRojinDokyoRoshintoNm.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtFuyoRojinDokyoRoshinto
            // 
            this.txtFuyoRojinDokyoRoshinto.AutoSizeFromLength = true;
            this.txtFuyoRojinDokyoRoshinto.DisplayLength = null;
            this.txtFuyoRojinDokyoRoshinto.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoRojinDokyoRoshinto.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtFuyoRojinDokyoRoshinto.Location = new System.Drawing.Point(409, 99);
            this.txtFuyoRojinDokyoRoshinto.MaxLength = 2;
            this.txtFuyoRojinDokyoRoshinto.Name = "txtFuyoRojinDokyoRoshinto";
            this.txtFuyoRojinDokyoRoshinto.Size = new System.Drawing.Size(32, 23);
            this.txtFuyoRojinDokyoRoshinto.TabIndex = 13;
            this.txtFuyoRojinDokyoRoshinto.Text = "0";
            this.txtFuyoRojinDokyoRoshinto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFuyoRojinDokyoRoshinto.Validating += new System.ComponentModel.CancelEventHandler(this.txtFuyoRojinDokyoRoshinto_Validating);
            // 
            // lblFuyoRojinDokyoRoshinto
            // 
            this.lblFuyoRojinDokyoRoshinto.BackColor = System.Drawing.Color.Silver;
            this.lblFuyoRojinDokyoRoshinto.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFuyoRojinDokyoRoshinto.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFuyoRojinDokyoRoshinto.Location = new System.Drawing.Point(299, 99);
            this.lblFuyoRojinDokyoRoshinto.Name = "lblFuyoRojinDokyoRoshinto";
            this.lblFuyoRojinDokyoRoshinto.Size = new System.Drawing.Size(110, 23);
            this.lblFuyoRojinDokyoRoshinto.TabIndex = 36;
            this.lblFuyoRojinDokyoRoshinto.Text = "同居老親等";
            this.lblFuyoRojinDokyoRoshinto.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFuyoRojinDokyoRoshintoIgaiNm
            // 
            this.lblFuyoRojinDokyoRoshintoIgaiNm.BackColor = System.Drawing.Color.Silver;
            this.lblFuyoRojinDokyoRoshintoIgaiNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFuyoRojinDokyoRoshintoIgaiNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFuyoRojinDokyoRoshintoIgaiNm.Location = new System.Drawing.Point(441, 76);
            this.lblFuyoRojinDokyoRoshintoIgaiNm.Name = "lblFuyoRojinDokyoRoshintoIgaiNm";
            this.lblFuyoRojinDokyoRoshintoIgaiNm.Size = new System.Drawing.Size(34, 23);
            this.lblFuyoRojinDokyoRoshintoIgaiNm.TabIndex = 35;
            this.lblFuyoRojinDokyoRoshintoIgaiNm.Text = "人";
            this.lblFuyoRojinDokyoRoshintoIgaiNm.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtFuyoRojinDokyoRoshintoIgai
            // 
            this.txtFuyoRojinDokyoRoshintoIgai.AutoSizeFromLength = true;
            this.txtFuyoRojinDokyoRoshintoIgai.DisplayLength = null;
            this.txtFuyoRojinDokyoRoshintoIgai.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoRojinDokyoRoshintoIgai.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtFuyoRojinDokyoRoshintoIgai.Location = new System.Drawing.Point(409, 76);
            this.txtFuyoRojinDokyoRoshintoIgai.MaxLength = 2;
            this.txtFuyoRojinDokyoRoshintoIgai.Name = "txtFuyoRojinDokyoRoshintoIgai";
            this.txtFuyoRojinDokyoRoshintoIgai.Size = new System.Drawing.Size(32, 23);
            this.txtFuyoRojinDokyoRoshintoIgai.TabIndex = 12;
            this.txtFuyoRojinDokyoRoshintoIgai.Text = "0";
            this.txtFuyoRojinDokyoRoshintoIgai.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFuyoRojinDokyoRoshintoIgai.Validating += new System.ComponentModel.CancelEventHandler(this.txtFuyoRojinDokyoRoshintoIgai_Validating);
            // 
            // lblFuyoRojinDokyoRoshintoIgai
            // 
            this.lblFuyoRojinDokyoRoshintoIgai.BackColor = System.Drawing.Color.Silver;
            this.lblFuyoRojinDokyoRoshintoIgai.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFuyoRojinDokyoRoshintoIgai.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFuyoRojinDokyoRoshintoIgai.Location = new System.Drawing.Point(299, 76);
            this.lblFuyoRojinDokyoRoshintoIgai.Name = "lblFuyoRojinDokyoRoshintoIgai";
            this.lblFuyoRojinDokyoRoshintoIgai.Size = new System.Drawing.Size(110, 23);
            this.lblFuyoRojinDokyoRoshintoIgai.TabIndex = 33;
            this.lblFuyoRojinDokyoRoshintoIgai.Text = "同居老親等以外";
            this.lblFuyoRojinDokyoRoshintoIgai.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFuyoTokuteiNm
            // 
            this.lblFuyoTokuteiNm.BackColor = System.Drawing.Color.Silver;
            this.lblFuyoTokuteiNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFuyoTokuteiNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFuyoTokuteiNm.Location = new System.Drawing.Point(441, 53);
            this.lblFuyoTokuteiNm.Name = "lblFuyoTokuteiNm";
            this.lblFuyoTokuteiNm.Size = new System.Drawing.Size(34, 23);
            this.lblFuyoTokuteiNm.TabIndex = 32;
            this.lblFuyoTokuteiNm.Text = "人";
            this.lblFuyoTokuteiNm.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtFuyoTokutei
            // 
            this.txtFuyoTokutei.AutoSizeFromLength = true;
            this.txtFuyoTokutei.DisplayLength = null;
            this.txtFuyoTokutei.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoTokutei.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtFuyoTokutei.Location = new System.Drawing.Point(409, 53);
            this.txtFuyoTokutei.MaxLength = 2;
            this.txtFuyoTokutei.Name = "txtFuyoTokutei";
            this.txtFuyoTokutei.Size = new System.Drawing.Size(32, 23);
            this.txtFuyoTokutei.TabIndex = 11;
            this.txtFuyoTokutei.Text = "0";
            this.txtFuyoTokutei.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFuyoTokutei.Validating += new System.ComponentModel.CancelEventHandler(this.txtFuyoTokutei_Validating);
            // 
            // lblFuyoTokutei
            // 
            this.lblFuyoTokutei.BackColor = System.Drawing.Color.Silver;
            this.lblFuyoTokutei.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFuyoTokutei.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFuyoTokutei.Location = new System.Drawing.Point(274, 53);
            this.lblFuyoTokutei.Name = "lblFuyoTokutei";
            this.lblFuyoTokutei.Size = new System.Drawing.Size(135, 23);
            this.lblFuyoTokutei.TabIndex = 30;
            this.lblFuyoTokutei.Text = "特　　　定";
            this.lblFuyoTokutei.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFuyoIppanNm
            // 
            this.lblFuyoIppanNm.BackColor = System.Drawing.Color.Silver;
            this.lblFuyoIppanNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFuyoIppanNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFuyoIppanNm.Location = new System.Drawing.Point(441, 30);
            this.lblFuyoIppanNm.Name = "lblFuyoIppanNm";
            this.lblFuyoIppanNm.Size = new System.Drawing.Size(34, 23);
            this.lblFuyoIppanNm.TabIndex = 29;
            this.lblFuyoIppanNm.Text = "人";
            this.lblFuyoIppanNm.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtFuyoIppan
            // 
            this.txtFuyoIppan.AutoSizeFromLength = true;
            this.txtFuyoIppan.DisplayLength = null;
            this.txtFuyoIppan.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoIppan.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtFuyoIppan.Location = new System.Drawing.Point(409, 30);
            this.txtFuyoIppan.MaxLength = 2;
            this.txtFuyoIppan.Name = "txtFuyoIppan";
            this.txtFuyoIppan.Size = new System.Drawing.Size(32, 23);
            this.txtFuyoIppan.TabIndex = 10;
            this.txtFuyoIppan.Text = "0";
            this.txtFuyoIppan.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFuyoIppan.Validating += new System.ComponentModel.CancelEventHandler(this.txtFuyoIppan_Validating);
            // 
            // lblFuyoIppan
            // 
            this.lblFuyoIppan.BackColor = System.Drawing.Color.Silver;
            this.lblFuyoIppan.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFuyoIppan.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFuyoIppan.Location = new System.Drawing.Point(274, 30);
            this.lblFuyoIppan.Name = "lblFuyoIppan";
            this.lblFuyoIppan.Size = new System.Drawing.Size(135, 23);
            this.lblFuyoIppan.TabIndex = 27;
            this.lblFuyoIppan.Text = "控除対象扶養親族等";
            this.lblFuyoIppan.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblHonninKubun1Nm
            // 
            this.lblHonninKubun1Nm.BackColor = System.Drawing.Color.Silver;
            this.lblHonninKubun1Nm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblHonninKubun1Nm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblHonninKubun1Nm.Location = new System.Drawing.Point(132, 63);
            this.lblHonninKubun1Nm.Name = "lblHonninKubun1Nm";
            this.lblHonninKubun1Nm.Size = new System.Drawing.Size(133, 23);
            this.lblHonninKubun1Nm.TabIndex = 18;
            this.lblHonninKubun1Nm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtHonninKubun1
            // 
            this.txtHonninKubun1.AutoSizeFromLength = true;
            this.txtHonninKubun1.DisplayLength = null;
            this.txtHonninKubun1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtHonninKubun1.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtHonninKubun1.Location = new System.Drawing.Point(114, 63);
            this.txtHonninKubun1.MaxLength = 1;
            this.txtHonninKubun1.Name = "txtHonninKubun1";
            this.txtHonninKubun1.Size = new System.Drawing.Size(18, 23);
            this.txtHonninKubun1.TabIndex = 2;
            this.txtHonninKubun1.Text = "0";
            this.txtHonninKubun1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtHonninKubun1.Validating += new System.ComponentModel.CancelEventHandler(this.txtHonninKubun1_Validating);
            // 
            // lblHonninKubun1
            // 
            this.lblHonninKubun1.BackColor = System.Drawing.Color.Silver;
            this.lblHonninKubun1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblHonninKubun1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblHonninKubun1.Location = new System.Drawing.Point(14, 63);
            this.lblHonninKubun1.Name = "lblHonninKubun1";
            this.lblHonninKubun1.Size = new System.Drawing.Size(100, 23);
            this.lblHonninKubun1.TabIndex = 8;
            this.lblHonninKubun1.Text = "本人区分①";
            this.lblHonninKubun1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblNenmatsuChosei
            // 
            this.lblNenmatsuChosei.BackColor = System.Drawing.Color.Silver;
            this.lblNenmatsuChosei.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblNenmatsuChosei.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblNenmatsuChosei.Location = new System.Drawing.Point(14, 36);
            this.lblNenmatsuChosei.Name = "lblNenmatsuChosei";
            this.lblNenmatsuChosei.Size = new System.Drawing.Size(100, 23);
            this.lblNenmatsuChosei.TabIndex = 2;
            this.lblNenmatsuChosei.Text = "年 末 調 整";
            this.lblNenmatsuChosei.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtZeigakuHyo
            // 
            this.txtZeigakuHyo.AutoSizeFromLength = false;
            this.txtZeigakuHyo.DisplayLength = null;
            this.txtZeigakuHyo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtZeigakuHyo.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtZeigakuHyo.Location = new System.Drawing.Point(114, 13);
            this.txtZeigakuHyo.MaxLength = 1;
            this.txtZeigakuHyo.Name = "txtZeigakuHyo";
            this.txtZeigakuHyo.Size = new System.Drawing.Size(18, 23);
            this.txtZeigakuHyo.TabIndex = 0;
            this.txtZeigakuHyo.Text = "0";
            this.txtZeigakuHyo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtZeigakuHyo.Validating += new System.ComponentModel.CancelEventHandler(this.txtZeigakuHyo_Validating);
            // 
            // lblZeigakuHyo
            // 
            this.lblZeigakuHyo.BackColor = System.Drawing.Color.Silver;
            this.lblZeigakuHyo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblZeigakuHyo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblZeigakuHyo.Location = new System.Drawing.Point(14, 13);
            this.lblZeigakuHyo.Name = "lblZeigakuHyo";
            this.lblZeigakuHyo.Size = new System.Drawing.Size(100, 23);
            this.lblZeigakuHyo.TabIndex = 0;
            this.lblZeigakuHyo.Text = "税　額　票";
            this.lblZeigakuHyo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblGensenchoshuhyo
            // 
            this.lblGensenchoshuhyo.BackColor = System.Drawing.Color.Transparent;
            this.lblGensenchoshuhyo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGensenchoshuhyo.Location = new System.Drawing.Point(15, 259);
            this.lblGensenchoshuhyo.Name = "lblGensenchoshuhyo";
            this.lblGensenchoshuhyo.Size = new System.Drawing.Size(136, 23);
            this.lblGensenchoshuhyo.TabIndex = 95;
            this.lblGensenchoshuhyo.Text = "【源泉徴収票】";
            this.lblGensenchoshuhyo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.panel11);
            this.tabPage4.Location = new System.Drawing.Point(4, 29);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(721, 395);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "F11 諸手当";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.PeachPuff;
            this.panel11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel11.Controls.Add(this.lblKojoFootD1);
            this.panel11.Controls.Add(this.lblShikyuFootD1);
            this.panel11.Controls.Add(this.lblKojoGokei);
            this.panel11.Controls.Add(this.lblKojoGokeiFoot);
            this.panel11.Controls.Add(this.lblKojo);
            this.panel11.Controls.Add(this.lblShikyu);
            this.panel11.Controls.Add(this.lblShikyuGokei);
            this.panel11.Controls.Add(this.lblShikyuFoot);
            this.panel11.Controls.Add(this.dgvShikyu);
            this.panel11.Controls.Add(this.dgvKojo);
            this.panel11.Location = new System.Drawing.Point(20, 18);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(680, 359);
            this.panel11.TabIndex = 5;
            // 
            // lblKojoFootD1
            // 
            this.lblKojoFootD1.BackColor = System.Drawing.Color.Silver;
            this.lblKojoFootD1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKojoFootD1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKojoFootD1.Location = new System.Drawing.Point(624, 314);
            this.lblKojoFootD1.Name = "lblKojoFootD1";
            this.lblKojoFootD1.Size = new System.Drawing.Size(39, 23);
            this.lblKojoFootD1.TabIndex = 50;
            this.lblKojoFootD1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblShikyuFootD1
            // 
            this.lblShikyuFootD1.BackColor = System.Drawing.Color.Silver;
            this.lblShikyuFootD1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShikyuFootD1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShikyuFootD1.Location = new System.Drawing.Point(295, 314);
            this.lblShikyuFootD1.Name = "lblShikyuFootD1";
            this.lblShikyuFootD1.Size = new System.Drawing.Size(38, 23);
            this.lblShikyuFootD1.TabIndex = 49;
            this.lblShikyuFootD1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblKojoGokei
            // 
            this.lblKojoGokei.BackColor = System.Drawing.Color.Silver;
            this.lblKojoGokei.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKojoGokei.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKojoGokei.Location = new System.Drawing.Point(524, 314);
            this.lblKojoGokei.Name = "lblKojoGokei";
            this.lblKojoGokei.Size = new System.Drawing.Size(100, 23);
            this.lblKojoGokei.TabIndex = 48;
            this.lblKojoGokei.Text = "1,234,567";
            this.lblKojoGokei.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblKojoGokeiFoot
            // 
            this.lblKojoGokeiFoot.BackColor = System.Drawing.Color.Silver;
            this.lblKojoGokeiFoot.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKojoGokeiFoot.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKojoGokeiFoot.Location = new System.Drawing.Point(344, 314);
            this.lblKojoGokeiFoot.Name = "lblKojoGokeiFoot";
            this.lblKojoGokeiFoot.Size = new System.Drawing.Size(180, 23);
            this.lblKojoGokeiFoot.TabIndex = 47;
            this.lblKojoGokeiFoot.Text = "合　　計";
            this.lblKojoGokeiFoot.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblKojo
            // 
            this.lblKojo.BackColor = System.Drawing.Color.Transparent;
            this.lblKojo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKojo.Location = new System.Drawing.Point(341, 7);
            this.lblKojo.Name = "lblKojo";
            this.lblKojo.Size = new System.Drawing.Size(136, 23);
            this.lblKojo.TabIndex = 44;
            this.lblKojo.Text = "【控除項目】";
            this.lblKojo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShikyu
            // 
            this.lblShikyu.BackColor = System.Drawing.Color.Transparent;
            this.lblShikyu.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShikyu.Location = new System.Drawing.Point(12, 7);
            this.lblShikyu.Name = "lblShikyu";
            this.lblShikyu.Size = new System.Drawing.Size(136, 23);
            this.lblShikyu.TabIndex = 43;
            this.lblShikyu.Text = "【支給項目】";
            this.lblShikyu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShikyuGokei
            // 
            this.lblShikyuGokei.BackColor = System.Drawing.Color.Silver;
            this.lblShikyuGokei.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShikyuGokei.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShikyuGokei.Location = new System.Drawing.Point(195, 314);
            this.lblShikyuGokei.Name = "lblShikyuGokei";
            this.lblShikyuGokei.Size = new System.Drawing.Size(100, 23);
            this.lblShikyuGokei.TabIndex = 29;
            this.lblShikyuGokei.Text = "1,234,567";
            this.lblShikyuGokei.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblShikyuFoot
            // 
            this.lblShikyuFoot.BackColor = System.Drawing.Color.Silver;
            this.lblShikyuFoot.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShikyuFoot.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShikyuFoot.Location = new System.Drawing.Point(15, 314);
            this.lblShikyuFoot.Name = "lblShikyuFoot";
            this.lblShikyuFoot.Size = new System.Drawing.Size(180, 23);
            this.lblShikyuFoot.TabIndex = 27;
            this.lblShikyuFoot.Text = "合　　計";
            this.lblShikyuFoot.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dgvShikyu
            // 
            this.dgvShikyu.AllowUserToAddRows = false;
            this.dgvShikyu.AllowUserToDeleteRows = false;
            this.dgvShikyu.AllowUserToResizeColumns = false;
            this.dgvShikyu.AllowUserToResizeRows = false;
            this.dgvShikyu.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvShikyu.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.dgvShikyu.Location = new System.Drawing.Point(14, 30);
            this.dgvShikyu.MultiSelect = false;
            this.dgvShikyu.Name = "dgvShikyu";
            this.dgvShikyu.ReadOnly = true;
            this.dgvShikyu.RowHeadersVisible = false;
            this.dgvShikyu.RowTemplate.Height = 21;
            this.dgvShikyu.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvShikyu.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvShikyu.Size = new System.Drawing.Size(319, 281);
            this.dgvShikyu.TabIndex = 0;
            this.dgvShikyu.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dgvShikyu_CellValidating);
            this.dgvShikyu.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgv_EditingControlShowing);
            this.dgvShikyu.Enter += new System.EventHandler(this.dgvShikyu_Enter);
            this.dgvShikyu.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dgv_KeyPress);
            // 
            // dgvKojo
            // 
            this.dgvKojo.AllowUserToAddRows = false;
            this.dgvKojo.AllowUserToDeleteRows = false;
            this.dgvKojo.AllowUserToResizeColumns = false;
            this.dgvKojo.AllowUserToResizeRows = false;
            this.dgvKojo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvKojo.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.dgvKojo.Location = new System.Drawing.Point(344, 30);
            this.dgvKojo.MultiSelect = false;
            this.dgvKojo.Name = "dgvKojo";
            this.dgvKojo.ReadOnly = true;
            this.dgvKojo.RowHeadersVisible = false;
            this.dgvKojo.RowTemplate.Height = 21;
            this.dgvKojo.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvKojo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvKojo.Size = new System.Drawing.Size(319, 281);
            this.dgvKojo.TabIndex = 1;
            this.dgvKojo.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dgvKojo_CellValidating);
            this.dgvKojo.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgv_EditingControlShowing);
            this.dgvKojo.Enter += new System.EventHandler(this.dgvKojo_Enter);
            this.dgvKojo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dgv_KeyPress);
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.panel15);
            this.tabPage5.Location = new System.Drawing.Point(4, 29);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(721, 395);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "F12 振込銀行";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.Color.PeachPuff;
            this.panel15.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel15.Controls.Add(this.lblKyuyoYokinShumoku1Nm);
            this.panel15.Controls.Add(this.txtKyuyoYokinShumoku1);
            this.panel15.Controls.Add(this.lblKyuyoShitenNm1);
            this.panel15.Controls.Add(this.txtKyuyoShitenCd1);
            this.panel15.Controls.Add(this.lblKyuyoGinkoNm1);
            this.panel15.Controls.Add(this.txtKyuyoGinkoCd1);
            this.panel15.Controls.Add(this.lblKyuyoKeiyakushaBango1);
            this.panel15.Controls.Add(this.lblKyuyoKozaMeigininKana1);
            this.panel15.Controls.Add(this.lblKyuyoKozaMeigininKanji1);
            this.panel15.Controls.Add(this.lblKyuyoKozaBango1);
            this.panel15.Controls.Add(this.lblKyuyoYokinShumoku1);
            this.panel15.Controls.Add(this.lblKyuyoShitenCd1);
            this.panel15.Controls.Add(this.lblKyuyoGinkoCd1);
            this.panel15.Controls.Add(this.txtKyuyoShikyuKingaku1);
            this.panel15.Controls.Add(this.lblKyuyoShikyuKingaku1);
            this.panel15.Controls.Add(this.txtKyuyoShikyuRitsu1);
            this.panel15.Controls.Add(this.lblKyuyoShikyuRitsu1);
            this.panel15.Controls.Add(this.lblKyuyoShikyuHoho1Nm);
            this.panel15.Controls.Add(this.txtKyuyoShikyuHoho1);
            this.panel15.Controls.Add(this.lblKyuyoShikyuHoho1);
            this.panel15.Controls.Add(this.txtKyuyoKozaBango1);
            this.panel15.Controls.Add(this.txtKyuyoKozaMeigininKana1);
            this.panel15.Controls.Add(this.txtKyuyoKozaMeigininKanji1);
            this.panel15.Controls.Add(this.txtKyuyoKeiyakushaBango1);
            this.panel15.Controls.Add(this.lblKyuyoYokinShumoku2Nm);
            this.panel15.Controls.Add(this.txtKyuyoYokinShumoku2);
            this.panel15.Controls.Add(this.lblKyuyoShitenNm2);
            this.panel15.Controls.Add(this.txtKyuyoShitenCd2);
            this.panel15.Controls.Add(this.lblKyuyoGinkoNm2);
            this.panel15.Controls.Add(this.txtKyuyoGinkoCd2);
            this.panel15.Controls.Add(this.lblKyuyoKeiyakushaBango2);
            this.panel15.Controls.Add(this.lblKyuyoKozaMeigininKana2);
            this.panel15.Controls.Add(this.lblKyuyoKozaMeigininKanji2);
            this.panel15.Controls.Add(this.lblKyuyoKozaBango2);
            this.panel15.Controls.Add(this.lblKyuyoYokinShumoku2);
            this.panel15.Controls.Add(this.lblKyuyoShitenCd2);
            this.panel15.Controls.Add(this.lblShikyuHoho2);
            this.panel15.Controls.Add(this.lblShikyuHoho1);
            this.panel15.Controls.Add(this.lblKyuyoGinkoCd2);
            this.panel15.Controls.Add(this.txtKyuyoShikyuKingaku2);
            this.panel15.Controls.Add(this.lblKyuyoShikyuKingaku2);
            this.panel15.Controls.Add(this.txtKyuyoShikyuRitsu2);
            this.panel15.Controls.Add(this.lblKyuyoShikyuRitsu2);
            this.panel15.Controls.Add(this.lblKyuyoShikyuHoho2Nm);
            this.panel15.Controls.Add(this.txtKyuyoShikyuHoho2);
            this.panel15.Controls.Add(this.lblKyuyoShikyuHoho2);
            this.panel15.Controls.Add(this.txtKyuyoKozaBango2);
            this.panel15.Controls.Add(this.txtKyuyoKozaMeigininKana2);
            this.panel15.Controls.Add(this.txtKyuyoKozaMeigininKanji2);
            this.panel15.Controls.Add(this.txtKyuyoKeiyakushaBango2);
            this.panel15.Location = new System.Drawing.Point(20, 18);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(680, 359);
            this.panel15.TabIndex = 5;
            // 
            // lblKyuyoYokinShumoku1Nm
            // 
            this.lblKyuyoYokinShumoku1Nm.BackColor = System.Drawing.Color.Silver;
            this.lblKyuyoYokinShumoku1Nm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKyuyoYokinShumoku1Nm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKyuyoYokinShumoku1Nm.Location = new System.Drawing.Point(164, 168);
            this.lblKyuyoYokinShumoku1Nm.Name = "lblKyuyoYokinShumoku1Nm";
            this.lblKyuyoYokinShumoku1Nm.Size = new System.Drawing.Size(169, 23);
            this.lblKyuyoYokinShumoku1Nm.TabIndex = 82;
            this.lblKyuyoYokinShumoku1Nm.Text = "1:普通　2:当座";
            this.lblKyuyoYokinShumoku1Nm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKyuyoYokinShumoku1
            // 
            this.txtKyuyoYokinShumoku1.AutoSizeFromLength = true;
            this.txtKyuyoYokinShumoku1.DisplayLength = null;
            this.txtKyuyoYokinShumoku1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKyuyoYokinShumoku1.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtKyuyoYokinShumoku1.Location = new System.Drawing.Point(114, 168);
            this.txtKyuyoYokinShumoku1.MaxLength = 1;
            this.txtKyuyoYokinShumoku1.Name = "txtKyuyoYokinShumoku1";
            this.txtKyuyoYokinShumoku1.Size = new System.Drawing.Size(50, 23);
            this.txtKyuyoYokinShumoku1.TabIndex = 5;
            this.txtKyuyoYokinShumoku1.Text = "0";
            this.txtKyuyoYokinShumoku1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKyuyoYokinShumoku1.Validating += new System.ComponentModel.CancelEventHandler(this.txtKyuyoYokinShumoku1_Validating);
            // 
            // lblKyuyoShitenNm1
            // 
            this.lblKyuyoShitenNm1.BackColor = System.Drawing.Color.Silver;
            this.lblKyuyoShitenNm1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKyuyoShitenNm1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKyuyoShitenNm1.Location = new System.Drawing.Point(164, 145);
            this.lblKyuyoShitenNm1.Name = "lblKyuyoShitenNm1";
            this.lblKyuyoShitenNm1.Size = new System.Drawing.Size(169, 23);
            this.lblKyuyoShitenNm1.TabIndex = 80;
            this.lblKyuyoShitenNm1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKyuyoShitenCd1
            // 
            this.txtKyuyoShitenCd1.AutoSizeFromLength = true;
            this.txtKyuyoShitenCd1.DisplayLength = null;
            this.txtKyuyoShitenCd1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKyuyoShitenCd1.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKyuyoShitenCd1.Location = new System.Drawing.Point(114, 145);
            this.txtKyuyoShitenCd1.MaxLength = 4;
            this.txtKyuyoShitenCd1.Name = "txtKyuyoShitenCd1";
            this.txtKyuyoShitenCd1.Size = new System.Drawing.Size(50, 23);
            this.txtKyuyoShitenCd1.TabIndex = 4;
            this.txtKyuyoShitenCd1.Text = "0";
            this.txtKyuyoShitenCd1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKyuyoShitenCd1.Validating += new System.ComponentModel.CancelEventHandler(this.txtKyuyoShitenCd1_Validating);
            // 
            // lblKyuyoGinkoNm1
            // 
            this.lblKyuyoGinkoNm1.BackColor = System.Drawing.Color.Silver;
            this.lblKyuyoGinkoNm1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKyuyoGinkoNm1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKyuyoGinkoNm1.Location = new System.Drawing.Point(164, 122);
            this.lblKyuyoGinkoNm1.Name = "lblKyuyoGinkoNm1";
            this.lblKyuyoGinkoNm1.Size = new System.Drawing.Size(169, 23);
            this.lblKyuyoGinkoNm1.TabIndex = 78;
            this.lblKyuyoGinkoNm1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKyuyoGinkoCd1
            // 
            this.txtKyuyoGinkoCd1.AutoSizeFromLength = true;
            this.txtKyuyoGinkoCd1.DisplayLength = null;
            this.txtKyuyoGinkoCd1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKyuyoGinkoCd1.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKyuyoGinkoCd1.Location = new System.Drawing.Point(114, 122);
            this.txtKyuyoGinkoCd1.MaxLength = 3;
            this.txtKyuyoGinkoCd1.Name = "txtKyuyoGinkoCd1";
            this.txtKyuyoGinkoCd1.Size = new System.Drawing.Size(50, 23);
            this.txtKyuyoGinkoCd1.TabIndex = 3;
            this.txtKyuyoGinkoCd1.Text = "0";
            this.txtKyuyoGinkoCd1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKyuyoGinkoCd1.Validating += new System.ComponentModel.CancelEventHandler(this.txtKyuyoGinkoCd1_Validating);
            // 
            // lblKyuyoKeiyakushaBango1
            // 
            this.lblKyuyoKeiyakushaBango1.BackColor = System.Drawing.Color.Silver;
            this.lblKyuyoKeiyakushaBango1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKyuyoKeiyakushaBango1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKyuyoKeiyakushaBango1.Location = new System.Drawing.Point(14, 260);
            this.lblKyuyoKeiyakushaBango1.Name = "lblKyuyoKeiyakushaBango1";
            this.lblKyuyoKeiyakushaBango1.Size = new System.Drawing.Size(100, 23);
            this.lblKyuyoKeiyakushaBango1.TabIndex = 76;
            this.lblKyuyoKeiyakushaBango1.Text = "契約者番号";
            this.lblKyuyoKeiyakushaBango1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKyuyoKozaMeigininKana1
            // 
            this.lblKyuyoKozaMeigininKana1.BackColor = System.Drawing.Color.Silver;
            this.lblKyuyoKozaMeigininKana1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKyuyoKozaMeigininKana1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKyuyoKozaMeigininKana1.Location = new System.Drawing.Point(14, 237);
            this.lblKyuyoKozaMeigininKana1.Name = "lblKyuyoKozaMeigininKana1";
            this.lblKyuyoKozaMeigininKana1.Size = new System.Drawing.Size(100, 23);
            this.lblKyuyoKozaMeigininKana1.TabIndex = 75;
            this.lblKyuyoKozaMeigininKana1.Text = "フリガナ";
            this.lblKyuyoKozaMeigininKana1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKyuyoKozaMeigininKanji1
            // 
            this.lblKyuyoKozaMeigininKanji1.BackColor = System.Drawing.Color.Silver;
            this.lblKyuyoKozaMeigininKanji1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKyuyoKozaMeigininKanji1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKyuyoKozaMeigininKanji1.Location = new System.Drawing.Point(14, 214);
            this.lblKyuyoKozaMeigininKanji1.Name = "lblKyuyoKozaMeigininKanji1";
            this.lblKyuyoKozaMeigininKanji1.Size = new System.Drawing.Size(100, 23);
            this.lblKyuyoKozaMeigininKanji1.TabIndex = 74;
            this.lblKyuyoKozaMeigininKanji1.Text = "口座名義人";
            this.lblKyuyoKozaMeigininKanji1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKyuyoKozaBango1
            // 
            this.lblKyuyoKozaBango1.BackColor = System.Drawing.Color.Silver;
            this.lblKyuyoKozaBango1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKyuyoKozaBango1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKyuyoKozaBango1.Location = new System.Drawing.Point(14, 191);
            this.lblKyuyoKozaBango1.Name = "lblKyuyoKozaBango1";
            this.lblKyuyoKozaBango1.Size = new System.Drawing.Size(100, 23);
            this.lblKyuyoKozaBango1.TabIndex = 73;
            this.lblKyuyoKozaBango1.Text = "口座番号";
            this.lblKyuyoKozaBango1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKyuyoYokinShumoku1
            // 
            this.lblKyuyoYokinShumoku1.BackColor = System.Drawing.Color.Silver;
            this.lblKyuyoYokinShumoku1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKyuyoYokinShumoku1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKyuyoYokinShumoku1.Location = new System.Drawing.Point(14, 168);
            this.lblKyuyoYokinShumoku1.Name = "lblKyuyoYokinShumoku1";
            this.lblKyuyoYokinShumoku1.Size = new System.Drawing.Size(100, 23);
            this.lblKyuyoYokinShumoku1.TabIndex = 72;
            this.lblKyuyoYokinShumoku1.Text = "種　　目";
            this.lblKyuyoYokinShumoku1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKyuyoShitenCd1
            // 
            this.lblKyuyoShitenCd1.BackColor = System.Drawing.Color.Silver;
            this.lblKyuyoShitenCd1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKyuyoShitenCd1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKyuyoShitenCd1.Location = new System.Drawing.Point(14, 145);
            this.lblKyuyoShitenCd1.Name = "lblKyuyoShitenCd1";
            this.lblKyuyoShitenCd1.Size = new System.Drawing.Size(100, 23);
            this.lblKyuyoShitenCd1.TabIndex = 71;
            this.lblKyuyoShitenCd1.Text = "支店コード";
            this.lblKyuyoShitenCd1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKyuyoGinkoCd1
            // 
            this.lblKyuyoGinkoCd1.BackColor = System.Drawing.Color.Silver;
            this.lblKyuyoGinkoCd1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKyuyoGinkoCd1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKyuyoGinkoCd1.Location = new System.Drawing.Point(14, 122);
            this.lblKyuyoGinkoCd1.Name = "lblKyuyoGinkoCd1";
            this.lblKyuyoGinkoCd1.Size = new System.Drawing.Size(100, 23);
            this.lblKyuyoGinkoCd1.TabIndex = 70;
            this.lblKyuyoGinkoCd1.Text = "銀行コード";
            this.lblKyuyoGinkoCd1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKyuyoShikyuKingaku1
            // 
            this.txtKyuyoShikyuKingaku1.AutoSizeFromLength = true;
            this.txtKyuyoShikyuKingaku1.DisplayLength = null;
            this.txtKyuyoShikyuKingaku1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKyuyoShikyuKingaku1.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKyuyoShikyuKingaku1.Location = new System.Drawing.Point(114, 76);
            this.txtKyuyoShikyuKingaku1.MaxLength = 9;
            this.txtKyuyoShikyuKingaku1.Name = "txtKyuyoShikyuKingaku1";
            this.txtKyuyoShikyuKingaku1.Size = new System.Drawing.Size(81, 23);
            this.txtKyuyoShikyuKingaku1.TabIndex = 2;
            this.txtKyuyoShikyuKingaku1.Text = "0";
            this.txtKyuyoShikyuKingaku1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKyuyoShikyuKingaku1.Validating += new System.ComponentModel.CancelEventHandler(this.txtKyuyoShikyuKingaku1_Validating);
            // 
            // lblKyuyoShikyuKingaku1
            // 
            this.lblKyuyoShikyuKingaku1.BackColor = System.Drawing.Color.Silver;
            this.lblKyuyoShikyuKingaku1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKyuyoShikyuKingaku1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKyuyoShikyuKingaku1.Location = new System.Drawing.Point(14, 76);
            this.lblKyuyoShikyuKingaku1.Name = "lblKyuyoShikyuKingaku1";
            this.lblKyuyoShikyuKingaku1.Size = new System.Drawing.Size(100, 23);
            this.lblKyuyoShikyuKingaku1.TabIndex = 68;
            this.lblKyuyoShikyuKingaku1.Text = "支給金額";
            this.lblKyuyoShikyuKingaku1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKyuyoShikyuRitsu1
            // 
            this.txtKyuyoShikyuRitsu1.AutoSizeFromLength = true;
            this.txtKyuyoShikyuRitsu1.DisplayLength = null;
            this.txtKyuyoShikyuRitsu1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKyuyoShikyuRitsu1.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKyuyoShikyuRitsu1.Location = new System.Drawing.Point(114, 53);
            this.txtKyuyoShikyuRitsu1.MaxLength = 3;
            this.txtKyuyoShikyuRitsu1.Name = "txtKyuyoShikyuRitsu1";
            this.txtKyuyoShikyuRitsu1.Size = new System.Drawing.Size(50, 23);
            this.txtKyuyoShikyuRitsu1.TabIndex = 1;
            this.txtKyuyoShikyuRitsu1.Text = "0";
            this.txtKyuyoShikyuRitsu1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKyuyoShikyuRitsu1.Validating += new System.ComponentModel.CancelEventHandler(this.txtKyuyoShikyuRitsu1_Validating);
            // 
            // lblKyuyoShikyuRitsu1
            // 
            this.lblKyuyoShikyuRitsu1.BackColor = System.Drawing.Color.Silver;
            this.lblKyuyoShikyuRitsu1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKyuyoShikyuRitsu1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKyuyoShikyuRitsu1.Location = new System.Drawing.Point(14, 53);
            this.lblKyuyoShikyuRitsu1.Name = "lblKyuyoShikyuRitsu1";
            this.lblKyuyoShikyuRitsu1.Size = new System.Drawing.Size(100, 23);
            this.lblKyuyoShikyuRitsu1.TabIndex = 66;
            this.lblKyuyoShikyuRitsu1.Text = "支給率(％)";
            this.lblKyuyoShikyuRitsu1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKyuyoShikyuHoho1Nm
            // 
            this.lblKyuyoShikyuHoho1Nm.BackColor = System.Drawing.Color.Silver;
            this.lblKyuyoShikyuHoho1Nm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKyuyoShikyuHoho1Nm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKyuyoShikyuHoho1Nm.Location = new System.Drawing.Point(164, 30);
            this.lblKyuyoShikyuHoho1Nm.Name = "lblKyuyoShikyuHoho1Nm";
            this.lblKyuyoShikyuHoho1Nm.Size = new System.Drawing.Size(169, 23);
            this.lblKyuyoShikyuHoho1Nm.TabIndex = 65;
            this.lblKyuyoShikyuHoho1Nm.Text = "1:現金　2:振込";
            this.lblKyuyoShikyuHoho1Nm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKyuyoShikyuHoho1
            // 
            this.txtKyuyoShikyuHoho1.AutoSizeFromLength = true;
            this.txtKyuyoShikyuHoho1.DisplayLength = null;
            this.txtKyuyoShikyuHoho1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKyuyoShikyuHoho1.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKyuyoShikyuHoho1.Location = new System.Drawing.Point(114, 30);
            this.txtKyuyoShikyuHoho1.MaxLength = 1;
            this.txtKyuyoShikyuHoho1.Name = "txtKyuyoShikyuHoho1";
            this.txtKyuyoShikyuHoho1.Size = new System.Drawing.Size(50, 23);
            this.txtKyuyoShikyuHoho1.TabIndex = 0;
            this.txtKyuyoShikyuHoho1.Text = "0";
            this.txtKyuyoShikyuHoho1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKyuyoShikyuHoho1.Validating += new System.ComponentModel.CancelEventHandler(this.txtKyuyoShikyuHoho1_Validating);
            // 
            // lblKyuyoShikyuHoho1
            // 
            this.lblKyuyoShikyuHoho1.BackColor = System.Drawing.Color.Silver;
            this.lblKyuyoShikyuHoho1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKyuyoShikyuHoho1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKyuyoShikyuHoho1.Location = new System.Drawing.Point(14, 30);
            this.lblKyuyoShikyuHoho1.Name = "lblKyuyoShikyuHoho1";
            this.lblKyuyoShikyuHoho1.Size = new System.Drawing.Size(100, 23);
            this.lblKyuyoShikyuHoho1.TabIndex = 63;
            this.lblKyuyoShikyuHoho1.Text = "支給方法";
            this.lblKyuyoShikyuHoho1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKyuyoKozaBango1
            // 
            this.txtKyuyoKozaBango1.AutoSizeFromLength = false;
            this.txtKyuyoKozaBango1.DisplayLength = null;
            this.txtKyuyoKozaBango1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKyuyoKozaBango1.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKyuyoKozaBango1.Location = new System.Drawing.Point(114, 191);
            this.txtKyuyoKozaBango1.MaxLength = 8;
            this.txtKyuyoKozaBango1.Name = "txtKyuyoKozaBango1";
            this.txtKyuyoKozaBango1.Size = new System.Drawing.Size(81, 23);
            this.txtKyuyoKozaBango1.TabIndex = 6;
            this.txtKyuyoKozaBango1.Validating += new System.ComponentModel.CancelEventHandler(this.txtKyuyoKozaBango1_Validating);
            // 
            // txtKyuyoKozaMeigininKana1
            // 
            this.txtKyuyoKozaMeigininKana1.AutoSizeFromLength = false;
            this.txtKyuyoKozaMeigininKana1.DisplayLength = null;
            this.txtKyuyoKozaMeigininKana1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKyuyoKozaMeigininKana1.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtKyuyoKozaMeigininKana1.Location = new System.Drawing.Point(114, 237);
            this.txtKyuyoKozaMeigininKana1.MaxLength = 30;
            this.txtKyuyoKozaMeigininKana1.Name = "txtKyuyoKozaMeigininKana1";
            this.txtKyuyoKozaMeigininKana1.Size = new System.Drawing.Size(219, 23);
            this.txtKyuyoKozaMeigininKana1.TabIndex = 8;
            this.txtKyuyoKozaMeigininKana1.Validating += new System.ComponentModel.CancelEventHandler(this.txtKyuyoKozaMeigininKana1_Validating);
            // 
            // txtKyuyoKozaMeigininKanji1
            // 
            this.txtKyuyoKozaMeigininKanji1.AutoSizeFromLength = false;
            this.txtKyuyoKozaMeigininKanji1.DisplayLength = null;
            this.txtKyuyoKozaMeigininKanji1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKyuyoKozaMeigininKanji1.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtKyuyoKozaMeigininKanji1.Location = new System.Drawing.Point(114, 214);
            this.txtKyuyoKozaMeigininKanji1.MaxLength = 30;
            this.txtKyuyoKozaMeigininKanji1.Name = "txtKyuyoKozaMeigininKanji1";
            this.txtKyuyoKozaMeigininKanji1.Size = new System.Drawing.Size(219, 23);
            this.txtKyuyoKozaMeigininKanji1.TabIndex = 7;
            this.txtKyuyoKozaMeigininKanji1.Validating += new System.ComponentModel.CancelEventHandler(this.txtKyuyoKozaMeigininKanji1_Validating);
            // 
            // txtKyuyoKeiyakushaBango1
            // 
            this.txtKyuyoKeiyakushaBango1.AutoSizeFromLength = false;
            this.txtKyuyoKeiyakushaBango1.DisplayLength = null;
            this.txtKyuyoKeiyakushaBango1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKyuyoKeiyakushaBango1.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKyuyoKeiyakushaBango1.Location = new System.Drawing.Point(114, 260);
            this.txtKyuyoKeiyakushaBango1.MaxLength = 15;
            this.txtKyuyoKeiyakushaBango1.Name = "txtKyuyoKeiyakushaBango1";
            this.txtKyuyoKeiyakushaBango1.Size = new System.Drawing.Size(219, 23);
            this.txtKyuyoKeiyakushaBango1.TabIndex = 9;
            this.txtKyuyoKeiyakushaBango1.Validating += new System.ComponentModel.CancelEventHandler(this.txtKyuyoKeiyakushaBango1_Validating);
            // 
            // lblKyuyoYokinShumoku2Nm
            // 
            this.lblKyuyoYokinShumoku2Nm.BackColor = System.Drawing.Color.Silver;
            this.lblKyuyoYokinShumoku2Nm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKyuyoYokinShumoku2Nm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKyuyoYokinShumoku2Nm.Location = new System.Drawing.Point(494, 168);
            this.lblKyuyoYokinShumoku2Nm.Name = "lblKyuyoYokinShumoku2Nm";
            this.lblKyuyoYokinShumoku2Nm.Size = new System.Drawing.Size(169, 23);
            this.lblKyuyoYokinShumoku2Nm.TabIndex = 58;
            this.lblKyuyoYokinShumoku2Nm.Text = "1:普通　2:当座";
            this.lblKyuyoYokinShumoku2Nm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKyuyoYokinShumoku2
            // 
            this.txtKyuyoYokinShumoku2.AutoSizeFromLength = true;
            this.txtKyuyoYokinShumoku2.DisplayLength = null;
            this.txtKyuyoYokinShumoku2.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKyuyoYokinShumoku2.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKyuyoYokinShumoku2.Location = new System.Drawing.Point(444, 168);
            this.txtKyuyoYokinShumoku2.MaxLength = 1;
            this.txtKyuyoYokinShumoku2.Name = "txtKyuyoYokinShumoku2";
            this.txtKyuyoYokinShumoku2.Size = new System.Drawing.Size(50, 23);
            this.txtKyuyoYokinShumoku2.TabIndex = 15;
            this.txtKyuyoYokinShumoku2.Text = "0";
            this.txtKyuyoYokinShumoku2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKyuyoYokinShumoku2.Validating += new System.ComponentModel.CancelEventHandler(this.txtKyuyoYokinShumoku2_Validating);
            // 
            // lblKyuyoShitenNm2
            // 
            this.lblKyuyoShitenNm2.BackColor = System.Drawing.Color.Silver;
            this.lblKyuyoShitenNm2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKyuyoShitenNm2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKyuyoShitenNm2.Location = new System.Drawing.Point(494, 145);
            this.lblKyuyoShitenNm2.Name = "lblKyuyoShitenNm2";
            this.lblKyuyoShitenNm2.Size = new System.Drawing.Size(169, 23);
            this.lblKyuyoShitenNm2.TabIndex = 56;
            this.lblKyuyoShitenNm2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKyuyoShitenCd2
            // 
            this.txtKyuyoShitenCd2.AutoSizeFromLength = true;
            this.txtKyuyoShitenCd2.DisplayLength = null;
            this.txtKyuyoShitenCd2.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKyuyoShitenCd2.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKyuyoShitenCd2.Location = new System.Drawing.Point(444, 145);
            this.txtKyuyoShitenCd2.MaxLength = 4;
            this.txtKyuyoShitenCd2.Name = "txtKyuyoShitenCd2";
            this.txtKyuyoShitenCd2.Size = new System.Drawing.Size(50, 23);
            this.txtKyuyoShitenCd2.TabIndex = 14;
            this.txtKyuyoShitenCd2.Text = "0";
            this.txtKyuyoShitenCd2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKyuyoShitenCd2.Validating += new System.ComponentModel.CancelEventHandler(this.txtKyuyoShitenCd2_Validating);
            // 
            // lblKyuyoGinkoNm2
            // 
            this.lblKyuyoGinkoNm2.BackColor = System.Drawing.Color.Silver;
            this.lblKyuyoGinkoNm2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKyuyoGinkoNm2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKyuyoGinkoNm2.Location = new System.Drawing.Point(494, 122);
            this.lblKyuyoGinkoNm2.Name = "lblKyuyoGinkoNm2";
            this.lblKyuyoGinkoNm2.Size = new System.Drawing.Size(169, 23);
            this.lblKyuyoGinkoNm2.TabIndex = 54;
            this.lblKyuyoGinkoNm2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKyuyoGinkoCd2
            // 
            this.txtKyuyoGinkoCd2.AutoSizeFromLength = true;
            this.txtKyuyoGinkoCd2.DisplayLength = null;
            this.txtKyuyoGinkoCd2.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKyuyoGinkoCd2.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKyuyoGinkoCd2.Location = new System.Drawing.Point(444, 122);
            this.txtKyuyoGinkoCd2.MaxLength = 3;
            this.txtKyuyoGinkoCd2.Name = "txtKyuyoGinkoCd2";
            this.txtKyuyoGinkoCd2.Size = new System.Drawing.Size(50, 23);
            this.txtKyuyoGinkoCd2.TabIndex = 13;
            this.txtKyuyoGinkoCd2.Text = "0";
            this.txtKyuyoGinkoCd2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKyuyoGinkoCd2.Validating += new System.ComponentModel.CancelEventHandler(this.txtKyuyoGinkoCd2_Validating);
            // 
            // lblKyuyoKeiyakushaBango2
            // 
            this.lblKyuyoKeiyakushaBango2.BackColor = System.Drawing.Color.Silver;
            this.lblKyuyoKeiyakushaBango2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKyuyoKeiyakushaBango2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKyuyoKeiyakushaBango2.Location = new System.Drawing.Point(344, 260);
            this.lblKyuyoKeiyakushaBango2.Name = "lblKyuyoKeiyakushaBango2";
            this.lblKyuyoKeiyakushaBango2.Size = new System.Drawing.Size(100, 23);
            this.lblKyuyoKeiyakushaBango2.TabIndex = 52;
            this.lblKyuyoKeiyakushaBango2.Text = "契約者番号";
            this.lblKyuyoKeiyakushaBango2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKyuyoKozaMeigininKana2
            // 
            this.lblKyuyoKozaMeigininKana2.BackColor = System.Drawing.Color.Silver;
            this.lblKyuyoKozaMeigininKana2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKyuyoKozaMeigininKana2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKyuyoKozaMeigininKana2.Location = new System.Drawing.Point(344, 237);
            this.lblKyuyoKozaMeigininKana2.Name = "lblKyuyoKozaMeigininKana2";
            this.lblKyuyoKozaMeigininKana2.Size = new System.Drawing.Size(100, 23);
            this.lblKyuyoKozaMeigininKana2.TabIndex = 50;
            this.lblKyuyoKozaMeigininKana2.Text = "フリガナ";
            this.lblKyuyoKozaMeigininKana2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKyuyoKozaMeigininKanji2
            // 
            this.lblKyuyoKozaMeigininKanji2.BackColor = System.Drawing.Color.Silver;
            this.lblKyuyoKozaMeigininKanji2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKyuyoKozaMeigininKanji2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKyuyoKozaMeigininKanji2.Location = new System.Drawing.Point(344, 214);
            this.lblKyuyoKozaMeigininKanji2.Name = "lblKyuyoKozaMeigininKanji2";
            this.lblKyuyoKozaMeigininKanji2.Size = new System.Drawing.Size(100, 23);
            this.lblKyuyoKozaMeigininKanji2.TabIndex = 49;
            this.lblKyuyoKozaMeigininKanji2.Text = "口座名義人";
            this.lblKyuyoKozaMeigininKanji2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKyuyoKozaBango2
            // 
            this.lblKyuyoKozaBango2.BackColor = System.Drawing.Color.Silver;
            this.lblKyuyoKozaBango2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKyuyoKozaBango2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKyuyoKozaBango2.Location = new System.Drawing.Point(344, 191);
            this.lblKyuyoKozaBango2.Name = "lblKyuyoKozaBango2";
            this.lblKyuyoKozaBango2.Size = new System.Drawing.Size(100, 23);
            this.lblKyuyoKozaBango2.TabIndex = 48;
            this.lblKyuyoKozaBango2.Text = "口座番号";
            this.lblKyuyoKozaBango2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKyuyoYokinShumoku2
            // 
            this.lblKyuyoYokinShumoku2.BackColor = System.Drawing.Color.Silver;
            this.lblKyuyoYokinShumoku2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKyuyoYokinShumoku2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKyuyoYokinShumoku2.Location = new System.Drawing.Point(344, 168);
            this.lblKyuyoYokinShumoku2.Name = "lblKyuyoYokinShumoku2";
            this.lblKyuyoYokinShumoku2.Size = new System.Drawing.Size(100, 23);
            this.lblKyuyoYokinShumoku2.TabIndex = 47;
            this.lblKyuyoYokinShumoku2.Text = "種　　目";
            this.lblKyuyoYokinShumoku2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKyuyoShitenCd2
            // 
            this.lblKyuyoShitenCd2.BackColor = System.Drawing.Color.Silver;
            this.lblKyuyoShitenCd2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKyuyoShitenCd2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKyuyoShitenCd2.Location = new System.Drawing.Point(344, 145);
            this.lblKyuyoShitenCd2.Name = "lblKyuyoShitenCd2";
            this.lblKyuyoShitenCd2.Size = new System.Drawing.Size(100, 23);
            this.lblKyuyoShitenCd2.TabIndex = 46;
            this.lblKyuyoShitenCd2.Text = "支店コード";
            this.lblKyuyoShitenCd2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShikyuHoho2
            // 
            this.lblShikyuHoho2.BackColor = System.Drawing.Color.Transparent;
            this.lblShikyuHoho2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShikyuHoho2.Location = new System.Drawing.Point(341, 7);
            this.lblShikyuHoho2.Name = "lblShikyuHoho2";
            this.lblShikyuHoho2.Size = new System.Drawing.Size(136, 23);
            this.lblShikyuHoho2.TabIndex = 45;
            this.lblShikyuHoho2.Text = "【支給方法２】";
            this.lblShikyuHoho2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShikyuHoho1
            // 
            this.lblShikyuHoho1.BackColor = System.Drawing.Color.Transparent;
            this.lblShikyuHoho1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShikyuHoho1.Location = new System.Drawing.Point(11, 7);
            this.lblShikyuHoho1.Name = "lblShikyuHoho1";
            this.lblShikyuHoho1.Size = new System.Drawing.Size(136, 23);
            this.lblShikyuHoho1.TabIndex = 44;
            this.lblShikyuHoho1.Text = "【支給方法１】";
            this.lblShikyuHoho1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKyuyoGinkoCd2
            // 
            this.lblKyuyoGinkoCd2.BackColor = System.Drawing.Color.Silver;
            this.lblKyuyoGinkoCd2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKyuyoGinkoCd2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKyuyoGinkoCd2.Location = new System.Drawing.Point(344, 122);
            this.lblKyuyoGinkoCd2.Name = "lblKyuyoGinkoCd2";
            this.lblKyuyoGinkoCd2.Size = new System.Drawing.Size(100, 23);
            this.lblKyuyoGinkoCd2.TabIndex = 36;
            this.lblKyuyoGinkoCd2.Text = "銀行コード";
            this.lblKyuyoGinkoCd2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKyuyoShikyuKingaku2
            // 
            this.txtKyuyoShikyuKingaku2.AutoSizeFromLength = true;
            this.txtKyuyoShikyuKingaku2.DisplayLength = null;
            this.txtKyuyoShikyuKingaku2.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKyuyoShikyuKingaku2.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKyuyoShikyuKingaku2.Location = new System.Drawing.Point(444, 76);
            this.txtKyuyoShikyuKingaku2.MaxLength = 9;
            this.txtKyuyoShikyuKingaku2.Name = "txtKyuyoShikyuKingaku2";
            this.txtKyuyoShikyuKingaku2.Size = new System.Drawing.Size(81, 23);
            this.txtKyuyoShikyuKingaku2.TabIndex = 12;
            this.txtKyuyoShikyuKingaku2.Text = "0";
            this.txtKyuyoShikyuKingaku2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKyuyoShikyuKingaku2.Validating += new System.ComponentModel.CancelEventHandler(this.txtKyuyoShikyuKingaku2_Validating);
            // 
            // lblKyuyoShikyuKingaku2
            // 
            this.lblKyuyoShikyuKingaku2.BackColor = System.Drawing.Color.Silver;
            this.lblKyuyoShikyuKingaku2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKyuyoShikyuKingaku2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKyuyoShikyuKingaku2.Location = new System.Drawing.Point(344, 76);
            this.lblKyuyoShikyuKingaku2.Name = "lblKyuyoShikyuKingaku2";
            this.lblKyuyoShikyuKingaku2.Size = new System.Drawing.Size(100, 23);
            this.lblKyuyoShikyuKingaku2.TabIndex = 33;
            this.lblKyuyoShikyuKingaku2.Text = "支給金額";
            this.lblKyuyoShikyuKingaku2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKyuyoShikyuRitsu2
            // 
            this.txtKyuyoShikyuRitsu2.AutoSizeFromLength = true;
            this.txtKyuyoShikyuRitsu2.DisplayLength = null;
            this.txtKyuyoShikyuRitsu2.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKyuyoShikyuRitsu2.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKyuyoShikyuRitsu2.Location = new System.Drawing.Point(444, 53);
            this.txtKyuyoShikyuRitsu2.MaxLength = 3;
            this.txtKyuyoShikyuRitsu2.Name = "txtKyuyoShikyuRitsu2";
            this.txtKyuyoShikyuRitsu2.Size = new System.Drawing.Size(50, 23);
            this.txtKyuyoShikyuRitsu2.TabIndex = 11;
            this.txtKyuyoShikyuRitsu2.Text = "0";
            this.txtKyuyoShikyuRitsu2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKyuyoShikyuRitsu2.Validating += new System.ComponentModel.CancelEventHandler(this.txtKyuyoShikyuRitsu2_Validating);
            // 
            // lblKyuyoShikyuRitsu2
            // 
            this.lblKyuyoShikyuRitsu2.BackColor = System.Drawing.Color.Silver;
            this.lblKyuyoShikyuRitsu2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKyuyoShikyuRitsu2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKyuyoShikyuRitsu2.Location = new System.Drawing.Point(344, 53);
            this.lblKyuyoShikyuRitsu2.Name = "lblKyuyoShikyuRitsu2";
            this.lblKyuyoShikyuRitsu2.Size = new System.Drawing.Size(100, 23);
            this.lblKyuyoShikyuRitsu2.TabIndex = 30;
            this.lblKyuyoShikyuRitsu2.Text = "支給率(％)";
            this.lblKyuyoShikyuRitsu2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKyuyoShikyuHoho2Nm
            // 
            this.lblKyuyoShikyuHoho2Nm.BackColor = System.Drawing.Color.Silver;
            this.lblKyuyoShikyuHoho2Nm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKyuyoShikyuHoho2Nm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKyuyoShikyuHoho2Nm.Location = new System.Drawing.Point(494, 30);
            this.lblKyuyoShikyuHoho2Nm.Name = "lblKyuyoShikyuHoho2Nm";
            this.lblKyuyoShikyuHoho2Nm.Size = new System.Drawing.Size(169, 23);
            this.lblKyuyoShikyuHoho2Nm.TabIndex = 29;
            this.lblKyuyoShikyuHoho2Nm.Text = "1:現金　2:振込";
            this.lblKyuyoShikyuHoho2Nm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKyuyoShikyuHoho2
            // 
            this.txtKyuyoShikyuHoho2.AutoSizeFromLength = true;
            this.txtKyuyoShikyuHoho2.DisplayLength = null;
            this.txtKyuyoShikyuHoho2.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKyuyoShikyuHoho2.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKyuyoShikyuHoho2.Location = new System.Drawing.Point(444, 30);
            this.txtKyuyoShikyuHoho2.MaxLength = 1;
            this.txtKyuyoShikyuHoho2.Name = "txtKyuyoShikyuHoho2";
            this.txtKyuyoShikyuHoho2.Size = new System.Drawing.Size(50, 23);
            this.txtKyuyoShikyuHoho2.TabIndex = 10;
            this.txtKyuyoShikyuHoho2.Text = "0";
            this.txtKyuyoShikyuHoho2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKyuyoShikyuHoho2.Validating += new System.ComponentModel.CancelEventHandler(this.txtKyuyoShikyuHoho2_Validating);
            // 
            // lblKyuyoShikyuHoho2
            // 
            this.lblKyuyoShikyuHoho2.BackColor = System.Drawing.Color.Silver;
            this.lblKyuyoShikyuHoho2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKyuyoShikyuHoho2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKyuyoShikyuHoho2.Location = new System.Drawing.Point(344, 30);
            this.lblKyuyoShikyuHoho2.Name = "lblKyuyoShikyuHoho2";
            this.lblKyuyoShikyuHoho2.Size = new System.Drawing.Size(100, 23);
            this.lblKyuyoShikyuHoho2.TabIndex = 27;
            this.lblKyuyoShikyuHoho2.Text = "支給方法";
            this.lblKyuyoShikyuHoho2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKyuyoKozaBango2
            // 
            this.txtKyuyoKozaBango2.AutoSizeFromLength = false;
            this.txtKyuyoKozaBango2.DisplayLength = null;
            this.txtKyuyoKozaBango2.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKyuyoKozaBango2.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKyuyoKozaBango2.Location = new System.Drawing.Point(444, 191);
            this.txtKyuyoKozaBango2.MaxLength = 8;
            this.txtKyuyoKozaBango2.Name = "txtKyuyoKozaBango2";
            this.txtKyuyoKozaBango2.Size = new System.Drawing.Size(81, 23);
            this.txtKyuyoKozaBango2.TabIndex = 16;
            this.txtKyuyoKozaBango2.Validating += new System.ComponentModel.CancelEventHandler(this.txtKyuyoKozaBango2_Validating);
            // 
            // txtKyuyoKozaMeigininKana2
            // 
            this.txtKyuyoKozaMeigininKana2.AutoSizeFromLength = false;
            this.txtKyuyoKozaMeigininKana2.DisplayLength = null;
            this.txtKyuyoKozaMeigininKana2.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKyuyoKozaMeigininKana2.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtKyuyoKozaMeigininKana2.Location = new System.Drawing.Point(444, 237);
            this.txtKyuyoKozaMeigininKana2.MaxLength = 30;
            this.txtKyuyoKozaMeigininKana2.Name = "txtKyuyoKozaMeigininKana2";
            this.txtKyuyoKozaMeigininKana2.Size = new System.Drawing.Size(219, 23);
            this.txtKyuyoKozaMeigininKana2.TabIndex = 18;
            this.txtKyuyoKozaMeigininKana2.Validating += new System.ComponentModel.CancelEventHandler(this.txtKyuyoKozaMeigininKana2_Validating);
            // 
            // txtKyuyoKozaMeigininKanji2
            // 
            this.txtKyuyoKozaMeigininKanji2.AutoSizeFromLength = false;
            this.txtKyuyoKozaMeigininKanji2.DisplayLength = null;
            this.txtKyuyoKozaMeigininKanji2.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKyuyoKozaMeigininKanji2.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtKyuyoKozaMeigininKanji2.Location = new System.Drawing.Point(444, 214);
            this.txtKyuyoKozaMeigininKanji2.MaxLength = 30;
            this.txtKyuyoKozaMeigininKanji2.Name = "txtKyuyoKozaMeigininKanji2";
            this.txtKyuyoKozaMeigininKanji2.Size = new System.Drawing.Size(219, 23);
            this.txtKyuyoKozaMeigininKanji2.TabIndex = 17;
            this.txtKyuyoKozaMeigininKanji2.Validating += new System.ComponentModel.CancelEventHandler(this.txtKyuyoKozaMeigininKanji2_Validating);
            // 
            // txtKyuyoKeiyakushaBango2
            // 
            this.txtKyuyoKeiyakushaBango2.AutoSizeFromLength = false;
            this.txtKyuyoKeiyakushaBango2.DisplayLength = null;
            this.txtKyuyoKeiyakushaBango2.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKyuyoKeiyakushaBango2.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKyuyoKeiyakushaBango2.Location = new System.Drawing.Point(444, 260);
            this.txtKyuyoKeiyakushaBango2.MaxLength = 15;
            this.txtKyuyoKeiyakushaBango2.Name = "txtKyuyoKeiyakushaBango2";
            this.txtKyuyoKeiyakushaBango2.Size = new System.Drawing.Size(219, 23);
            this.txtKyuyoKeiyakushaBango2.TabIndex = 19;
            this.txtKyuyoKeiyakushaBango2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LastControl_KeyDown);
            this.txtKyuyoKeiyakushaBango2.Validating += new System.ComponentModel.CancelEventHandler(this.txtKyuyoKeiyakushaBango2_Validating);
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.panel1);
            this.tabPage6.Location = new System.Drawing.Point(4, 29);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(721, 395);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "F4 世帯情報";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.PeachPuff;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.txtIkouNo);
            this.panel1.Controls.Add(this.label90);
            this.panel1.Controls.Add(this.label91);
            this.panel1.Controls.Add(this.label42);
            this.panel1.Controls.Add(this.label32);
            this.panel1.Controls.Add(this.label22);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.label35);
            this.panel1.Controls.Add(this.txtFuyoNm4);
            this.panel1.Controls.Add(this.label36);
            this.panel1.Controls.Add(this.label37);
            this.panel1.Controls.Add(this.txtFuyoJukyo4);
            this.panel1.Controls.Add(this.button6);
            this.panel1.Controls.Add(this.txtFuyoMyNumberDm4);
            this.panel1.Controls.Add(this.txtFuyoMyNumber4);
            this.panel1.Controls.Add(this.label38);
            this.panel1.Controls.Add(this.panel6);
            this.panel1.Controls.Add(this.label43);
            this.panel1.Controls.Add(this.txtFuyoKanaNm4);
            this.panel1.Controls.Add(this.label44);
            this.panel1.Controls.Add(this.label25);
            this.panel1.Controls.Add(this.txtFuyoNm3);
            this.panel1.Controls.Add(this.label26);
            this.panel1.Controls.Add(this.label27);
            this.panel1.Controls.Add(this.txtFuyoJukyo3);
            this.panel1.Controls.Add(this.button5);
            this.panel1.Controls.Add(this.txtFuyoMyNumberDm3);
            this.panel1.Controls.Add(this.txtFuyoMyNumber3);
            this.panel1.Controls.Add(this.label28);
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Controls.Add(this.label33);
            this.panel1.Controls.Add(this.txtFuyoKanaNm3);
            this.panel1.Controls.Add(this.label34);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.txtFuyoNm2);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.txtFuyoJukyo2);
            this.panel1.Controls.Add(this.button4);
            this.panel1.Controls.Add(this.txtFuyoMyNumberDm2);
            this.panel1.Controls.Add(this.txtFuyoMyNumber2);
            this.panel1.Controls.Add(this.label18);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.label23);
            this.panel1.Controls.Add(this.txtFuyoKanaNm2);
            this.panel1.Controls.Add(this.label24);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.txtFuyoNm1);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.txtFuyoJukyo1);
            this.panel1.Controls.Add(this.button3);
            this.panel1.Controls.Add(this.txtFuyoMyNumberDm1);
            this.panel1.Controls.Add(this.txtFuyoMyNumber1);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.txtFuyoKanaNm1);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Location = new System.Drawing.Point(20, 18);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(680, 359);
            this.panel1.TabIndex = 8;
            // 
            // txtIkouNo
            // 
            this.txtIkouNo.AutoSizeFromLength = false;
            this.txtIkouNo.DisplayLength = null;
            this.txtIkouNo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtIkouNo.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtIkouNo.Location = new System.Drawing.Point(91, 329);
            this.txtIkouNo.MaxLength = 50;
            this.txtIkouNo.Name = "txtIkouNo";
            this.txtIkouNo.Size = new System.Drawing.Size(456, 23);
            this.txtIkouNo.TabIndex = 1041;
            this.txtIkouNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LastControl_KeyDown);
            this.txtIkouNo.Validating += new System.ComponentModel.CancelEventHandler(this.txtIkouNo_Validating);
            // 
            // label90
            // 
            this.label90.BackColor = System.Drawing.Color.Silver;
            this.label90.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label90.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label90.Location = new System.Drawing.Point(12, 330);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(79, 23);
            this.label90.TabIndex = 1043;
            this.label90.Text = "摘要";
            this.label90.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label91
            // 
            this.label91.BackColor = System.Drawing.Color.Transparent;
            this.label91.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label91.Location = new System.Drawing.Point(13, 306);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(186, 23);
            this.label91.TabIndex = 1045;
            this.label91.Text = "【5人目以降の個人番号】";
            this.label91.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label42
            // 
            this.label42.BackColor = System.Drawing.Color.Transparent;
            this.label42.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label42.Location = new System.Drawing.Point(334, 163);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(16, 21);
            this.label42.TabIndex = 1040;
            this.label42.Text = "4";
            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label32
            // 
            this.label32.BackColor = System.Drawing.Color.Transparent;
            this.label32.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label32.Location = new System.Drawing.Point(334, 23);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(16, 23);
            this.label32.TabIndex = 1039;
            this.label32.Text = "3";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label22
            // 
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label22.Location = new System.Drawing.Point(3, 163);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(16, 21);
            this.label22.TabIndex = 1038;
            this.label22.Text = "2";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label12
            // 
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label12.Location = new System.Drawing.Point(3, 23);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(16, 23);
            this.label12.TabIndex = 1037;
            this.label12.Text = "1";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label35
            // 
            this.label35.BackColor = System.Drawing.Color.Silver;
            this.label35.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label35.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label35.Location = new System.Drawing.Point(342, 231);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(68, 23);
            this.label35.TabIndex = 1036;
            this.label35.Text = "住　　居";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFuyoNm4
            // 
            this.txtFuyoNm4.AutoSizeFromLength = false;
            this.txtFuyoNm4.DisplayLength = null;
            this.txtFuyoNm4.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoNm4.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtFuyoNm4.Location = new System.Drawing.Point(409, 184);
            this.txtFuyoNm4.MaxLength = 30;
            this.txtFuyoNm4.Name = "txtFuyoNm4";
            this.txtFuyoNm4.Size = new System.Drawing.Size(219, 23);
            this.txtFuyoNm4.TabIndex = 1024;
            // 
            // label36
            // 
            this.label36.BackColor = System.Drawing.Color.Silver;
            this.label36.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label36.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label36.Location = new System.Drawing.Point(342, 184);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(68, 23);
            this.label36.TabIndex = 1025;
            this.label36.Text = "氏　　名";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label37
            // 
            this.label37.BackColor = System.Drawing.Color.Silver;
            this.label37.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label37.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label37.Location = new System.Drawing.Point(427, 230);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(155, 23);
            this.label37.TabIndex = 1034;
            this.label37.Text = "0:住居者　1:非住居者";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFuyoJukyo4
            // 
            this.txtFuyoJukyo4.AutoSizeFromLength = true;
            this.txtFuyoJukyo4.DisplayLength = null;
            this.txtFuyoJukyo4.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoJukyo4.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtFuyoJukyo4.Location = new System.Drawing.Point(409, 230);
            this.txtFuyoJukyo4.MaxLength = 1;
            this.txtFuyoJukyo4.Name = "txtFuyoJukyo4";
            this.txtFuyoJukyo4.Size = new System.Drawing.Size(18, 23);
            this.txtFuyoJukyo4.TabIndex = 1035;
            this.txtFuyoJukyo4.Text = "0";
            this.txtFuyoJukyo4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(557, 278);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 23);
            this.button6.TabIndex = 1033;
            this.button6.Text = "公開";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // txtFuyoMyNumberDm4
            // 
            this.txtFuyoMyNumberDm4.AutoSizeFromLength = false;
            this.txtFuyoMyNumberDm4.DisplayLength = null;
            this.txtFuyoMyNumberDm4.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoMyNumberDm4.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtFuyoMyNumberDm4.Location = new System.Drawing.Point(442, 278);
            this.txtFuyoMyNumberDm4.MaxLength = 15;
            this.txtFuyoMyNumberDm4.Name = "txtFuyoMyNumberDm4";
            this.txtFuyoMyNumberDm4.ReadOnly = true;
            this.txtFuyoMyNumberDm4.Size = new System.Drawing.Size(114, 23);
            this.txtFuyoMyNumberDm4.TabIndex = 1032;
            this.txtFuyoMyNumberDm4.Text = "************";
            // 
            // txtFuyoMyNumber4
            // 
            this.txtFuyoMyNumber4.AutoSizeFromLength = false;
            this.txtFuyoMyNumber4.DisplayLength = null;
            this.txtFuyoMyNumber4.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoMyNumber4.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtFuyoMyNumber4.Location = new System.Drawing.Point(442, 278);
            this.txtFuyoMyNumber4.MaxLength = 12;
            this.txtFuyoMyNumber4.Name = "txtFuyoMyNumber4";
            this.txtFuyoMyNumber4.ReadOnly = true;
            this.txtFuyoMyNumber4.Size = new System.Drawing.Size(114, 23);
            this.txtFuyoMyNumber4.TabIndex = 1030;
            // 
            // label38
            // 
            this.label38.BackColor = System.Drawing.Color.Silver;
            this.label38.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label38.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label38.Location = new System.Drawing.Point(342, 278);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(100, 23);
            this.label38.TabIndex = 1031;
            this.label38.Text = "マイナンバー";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.Silver;
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel6.Controls.Add(this.label39);
            this.panel6.Controls.Add(this.txtFuyoDaySeinengappi4);
            this.panel6.Controls.Add(this.label40);
            this.panel6.Controls.Add(this.label41);
            this.panel6.Controls.Add(this.txtFuyoMonthSeinengappi4);
            this.panel6.Controls.Add(this.lblFuyoGengoSeinengappi4);
            this.panel6.Controls.Add(this.txtFuyoGengoYearSeinengappi4);
            this.panel6.Location = new System.Drawing.Point(409, 253);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(219, 25);
            this.panel6.TabIndex = 1028;
            // 
            // label39
            // 
            this.label39.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label39.Location = new System.Drawing.Point(164, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(21, 23);
            this.label39.TabIndex = 14;
            this.label39.Text = "日";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtFuyoDaySeinengappi4
            // 
            this.txtFuyoDaySeinengappi4.AutoSizeFromLength = false;
            this.txtFuyoDaySeinengappi4.DisplayLength = null;
            this.txtFuyoDaySeinengappi4.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoDaySeinengappi4.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtFuyoDaySeinengappi4.Location = new System.Drawing.Point(139, 0);
            this.txtFuyoDaySeinengappi4.MaxLength = 2;
            this.txtFuyoDaySeinengappi4.Name = "txtFuyoDaySeinengappi4";
            this.txtFuyoDaySeinengappi4.Size = new System.Drawing.Size(25, 23);
            this.txtFuyoDaySeinengappi4.TabIndex = 2;
            this.txtFuyoDaySeinengappi4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label40
            // 
            this.label40.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label40.Location = new System.Drawing.Point(118, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(21, 23);
            this.label40.TabIndex = 12;
            this.label40.Text = "月";
            this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label41
            // 
            this.label41.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label41.Location = new System.Drawing.Point(72, 0);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(21, 23);
            this.label41.TabIndex = 2;
            this.label41.Text = "年";
            this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtFuyoMonthSeinengappi4
            // 
            this.txtFuyoMonthSeinengappi4.AutoSizeFromLength = false;
            this.txtFuyoMonthSeinengappi4.DisplayLength = null;
            this.txtFuyoMonthSeinengappi4.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoMonthSeinengappi4.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtFuyoMonthSeinengappi4.Location = new System.Drawing.Point(93, 0);
            this.txtFuyoMonthSeinengappi4.MaxLength = 2;
            this.txtFuyoMonthSeinengappi4.Name = "txtFuyoMonthSeinengappi4";
            this.txtFuyoMonthSeinengappi4.Size = new System.Drawing.Size(25, 23);
            this.txtFuyoMonthSeinengappi4.TabIndex = 1;
            this.txtFuyoMonthSeinengappi4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblFuyoGengoSeinengappi4
            // 
            this.lblFuyoGengoSeinengappi4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFuyoGengoSeinengappi4.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFuyoGengoSeinengappi4.Location = new System.Drawing.Point(0, 0);
            this.lblFuyoGengoSeinengappi4.Name = "lblFuyoGengoSeinengappi4";
            this.lblFuyoGengoSeinengappi4.Size = new System.Drawing.Size(47, 23);
            this.lblFuyoGengoSeinengappi4.TabIndex = 0;
            this.lblFuyoGengoSeinengappi4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtFuyoGengoYearSeinengappi4
            // 
            this.txtFuyoGengoYearSeinengappi4.AutoSizeFromLength = false;
            this.txtFuyoGengoYearSeinengappi4.DisplayLength = null;
            this.txtFuyoGengoYearSeinengappi4.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoGengoYearSeinengappi4.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtFuyoGengoYearSeinengappi4.Location = new System.Drawing.Point(47, 0);
            this.txtFuyoGengoYearSeinengappi4.MaxLength = 2;
            this.txtFuyoGengoYearSeinengappi4.Name = "txtFuyoGengoYearSeinengappi4";
            this.txtFuyoGengoYearSeinengappi4.Size = new System.Drawing.Size(25, 23);
            this.txtFuyoGengoYearSeinengappi4.TabIndex = 0;
            this.txtFuyoGengoYearSeinengappi4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label43
            // 
            this.label43.BackColor = System.Drawing.Color.Silver;
            this.label43.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label43.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label43.Location = new System.Drawing.Point(342, 253);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(68, 25);
            this.label43.TabIndex = 1029;
            this.label43.Text = "生年月日";
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFuyoKanaNm4
            // 
            this.txtFuyoKanaNm4.AutoSizeFromLength = false;
            this.txtFuyoKanaNm4.DisplayLength = null;
            this.txtFuyoKanaNm4.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoKanaNm4.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtFuyoKanaNm4.Location = new System.Drawing.Point(409, 207);
            this.txtFuyoKanaNm4.MaxLength = 30;
            this.txtFuyoKanaNm4.Name = "txtFuyoKanaNm4";
            this.txtFuyoKanaNm4.Size = new System.Drawing.Size(219, 23);
            this.txtFuyoKanaNm4.TabIndex = 1026;
            // 
            // label44
            // 
            this.label44.BackColor = System.Drawing.Color.Silver;
            this.label44.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label44.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label44.Location = new System.Drawing.Point(342, 207);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(68, 23);
            this.label44.TabIndex = 1027;
            this.label44.Text = "フリガナ";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label25
            // 
            this.label25.BackColor = System.Drawing.Color.Silver;
            this.label25.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label25.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label25.Location = new System.Drawing.Point(342, 93);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(68, 23);
            this.label25.TabIndex = 1023;
            this.label25.Text = "住　　居";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFuyoNm3
            // 
            this.txtFuyoNm3.AutoSizeFromLength = false;
            this.txtFuyoNm3.DisplayLength = null;
            this.txtFuyoNm3.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoNm3.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtFuyoNm3.Location = new System.Drawing.Point(409, 46);
            this.txtFuyoNm3.MaxLength = 30;
            this.txtFuyoNm3.Name = "txtFuyoNm3";
            this.txtFuyoNm3.Size = new System.Drawing.Size(219, 23);
            this.txtFuyoNm3.TabIndex = 1011;
            // 
            // label26
            // 
            this.label26.BackColor = System.Drawing.Color.Silver;
            this.label26.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label26.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label26.Location = new System.Drawing.Point(342, 46);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(68, 23);
            this.label26.TabIndex = 1012;
            this.label26.Text = "氏　　名";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label27
            // 
            this.label27.BackColor = System.Drawing.Color.Silver;
            this.label27.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label27.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label27.Location = new System.Drawing.Point(427, 92);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(155, 23);
            this.label27.TabIndex = 1021;
            this.label27.Text = "0:住居者　1:非住居者";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFuyoJukyo3
            // 
            this.txtFuyoJukyo3.AutoSizeFromLength = true;
            this.txtFuyoJukyo3.DisplayLength = null;
            this.txtFuyoJukyo3.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoJukyo3.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtFuyoJukyo3.Location = new System.Drawing.Point(409, 92);
            this.txtFuyoJukyo3.MaxLength = 1;
            this.txtFuyoJukyo3.Name = "txtFuyoJukyo3";
            this.txtFuyoJukyo3.Size = new System.Drawing.Size(18, 23);
            this.txtFuyoJukyo3.TabIndex = 1022;
            this.txtFuyoJukyo3.Text = "0";
            this.txtFuyoJukyo3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(557, 140);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 1020;
            this.button5.Text = "公開";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // txtFuyoMyNumberDm3
            // 
            this.txtFuyoMyNumberDm3.AutoSizeFromLength = false;
            this.txtFuyoMyNumberDm3.DisplayLength = null;
            this.txtFuyoMyNumberDm3.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoMyNumberDm3.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtFuyoMyNumberDm3.Location = new System.Drawing.Point(442, 140);
            this.txtFuyoMyNumberDm3.MaxLength = 15;
            this.txtFuyoMyNumberDm3.Name = "txtFuyoMyNumberDm3";
            this.txtFuyoMyNumberDm3.ReadOnly = true;
            this.txtFuyoMyNumberDm3.Size = new System.Drawing.Size(114, 23);
            this.txtFuyoMyNumberDm3.TabIndex = 1019;
            this.txtFuyoMyNumberDm3.Text = "************";
            // 
            // txtFuyoMyNumber3
            // 
            this.txtFuyoMyNumber3.AutoSizeFromLength = false;
            this.txtFuyoMyNumber3.DisplayLength = null;
            this.txtFuyoMyNumber3.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoMyNumber3.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtFuyoMyNumber3.Location = new System.Drawing.Point(442, 140);
            this.txtFuyoMyNumber3.MaxLength = 12;
            this.txtFuyoMyNumber3.Name = "txtFuyoMyNumber3";
            this.txtFuyoMyNumber3.ReadOnly = true;
            this.txtFuyoMyNumber3.Size = new System.Drawing.Size(114, 23);
            this.txtFuyoMyNumber3.TabIndex = 1017;
            // 
            // label28
            // 
            this.label28.BackColor = System.Drawing.Color.Silver;
            this.label28.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label28.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label28.Location = new System.Drawing.Point(342, 140);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(100, 23);
            this.label28.TabIndex = 1018;
            this.label28.Text = "マイナンバー";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Silver;
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel5.Controls.Add(this.label29);
            this.panel5.Controls.Add(this.txtFuyoDaySeinengappi3);
            this.panel5.Controls.Add(this.label30);
            this.panel5.Controls.Add(this.label31);
            this.panel5.Controls.Add(this.txtFuyoMonthSeinengappi3);
            this.panel5.Controls.Add(this.lblFuyoGengoSeinengappi3);
            this.panel5.Controls.Add(this.txtFuyoGengoYearSeinengappi3);
            this.panel5.Location = new System.Drawing.Point(409, 115);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(219, 25);
            this.panel5.TabIndex = 1015;
            // 
            // label29
            // 
            this.label29.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label29.Location = new System.Drawing.Point(164, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(21, 23);
            this.label29.TabIndex = 14;
            this.label29.Text = "日";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtFuyoDaySeinengappi3
            // 
            this.txtFuyoDaySeinengappi3.AutoSizeFromLength = false;
            this.txtFuyoDaySeinengappi3.DisplayLength = null;
            this.txtFuyoDaySeinengappi3.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoDaySeinengappi3.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtFuyoDaySeinengappi3.Location = new System.Drawing.Point(139, 0);
            this.txtFuyoDaySeinengappi3.MaxLength = 2;
            this.txtFuyoDaySeinengappi3.Name = "txtFuyoDaySeinengappi3";
            this.txtFuyoDaySeinengappi3.Size = new System.Drawing.Size(25, 23);
            this.txtFuyoDaySeinengappi3.TabIndex = 2;
            this.txtFuyoDaySeinengappi3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label30
            // 
            this.label30.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label30.Location = new System.Drawing.Point(118, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(21, 23);
            this.label30.TabIndex = 12;
            this.label30.Text = "月";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label31
            // 
            this.label31.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label31.Location = new System.Drawing.Point(72, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(21, 23);
            this.label31.TabIndex = 2;
            this.label31.Text = "年";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtFuyoMonthSeinengappi3
            // 
            this.txtFuyoMonthSeinengappi3.AutoSizeFromLength = false;
            this.txtFuyoMonthSeinengappi3.DisplayLength = null;
            this.txtFuyoMonthSeinengappi3.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoMonthSeinengappi3.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtFuyoMonthSeinengappi3.Location = new System.Drawing.Point(93, 0);
            this.txtFuyoMonthSeinengappi3.MaxLength = 2;
            this.txtFuyoMonthSeinengappi3.Name = "txtFuyoMonthSeinengappi3";
            this.txtFuyoMonthSeinengappi3.Size = new System.Drawing.Size(25, 23);
            this.txtFuyoMonthSeinengappi3.TabIndex = 1;
            this.txtFuyoMonthSeinengappi3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblFuyoGengoSeinengappi3
            // 
            this.lblFuyoGengoSeinengappi3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFuyoGengoSeinengappi3.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFuyoGengoSeinengappi3.Location = new System.Drawing.Point(0, 0);
            this.lblFuyoGengoSeinengappi3.Name = "lblFuyoGengoSeinengappi3";
            this.lblFuyoGengoSeinengappi3.Size = new System.Drawing.Size(47, 23);
            this.lblFuyoGengoSeinengappi3.TabIndex = 0;
            this.lblFuyoGengoSeinengappi3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtFuyoGengoYearSeinengappi3
            // 
            this.txtFuyoGengoYearSeinengappi3.AutoSizeFromLength = false;
            this.txtFuyoGengoYearSeinengappi3.DisplayLength = null;
            this.txtFuyoGengoYearSeinengappi3.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoGengoYearSeinengappi3.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtFuyoGengoYearSeinengappi3.Location = new System.Drawing.Point(47, 0);
            this.txtFuyoGengoYearSeinengappi3.MaxLength = 2;
            this.txtFuyoGengoYearSeinengappi3.Name = "txtFuyoGengoYearSeinengappi3";
            this.txtFuyoGengoYearSeinengappi3.Size = new System.Drawing.Size(25, 23);
            this.txtFuyoGengoYearSeinengappi3.TabIndex = 0;
            this.txtFuyoGengoYearSeinengappi3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label33
            // 
            this.label33.BackColor = System.Drawing.Color.Silver;
            this.label33.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label33.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label33.Location = new System.Drawing.Point(342, 115);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(68, 25);
            this.label33.TabIndex = 1016;
            this.label33.Text = "生年月日";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFuyoKanaNm3
            // 
            this.txtFuyoKanaNm3.AutoSizeFromLength = false;
            this.txtFuyoKanaNm3.DisplayLength = null;
            this.txtFuyoKanaNm3.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoKanaNm3.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtFuyoKanaNm3.Location = new System.Drawing.Point(409, 69);
            this.txtFuyoKanaNm3.MaxLength = 30;
            this.txtFuyoKanaNm3.Name = "txtFuyoKanaNm3";
            this.txtFuyoKanaNm3.Size = new System.Drawing.Size(219, 23);
            this.txtFuyoKanaNm3.TabIndex = 1013;
            // 
            // label34
            // 
            this.label34.BackColor = System.Drawing.Color.Silver;
            this.label34.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label34.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label34.Location = new System.Drawing.Point(342, 69);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(68, 23);
            this.label34.TabIndex = 1014;
            this.label34.Text = "フリガナ";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label15
            // 
            this.label15.BackColor = System.Drawing.Color.Silver;
            this.label15.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label15.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label15.Location = new System.Drawing.Point(12, 231);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(68, 23);
            this.label15.TabIndex = 1010;
            this.label15.Text = "住　　居";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFuyoNm2
            // 
            this.txtFuyoNm2.AutoSizeFromLength = false;
            this.txtFuyoNm2.DisplayLength = null;
            this.txtFuyoNm2.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoNm2.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtFuyoNm2.Location = new System.Drawing.Point(79, 184);
            this.txtFuyoNm2.MaxLength = 30;
            this.txtFuyoNm2.Name = "txtFuyoNm2";
            this.txtFuyoNm2.Size = new System.Drawing.Size(219, 23);
            this.txtFuyoNm2.TabIndex = 998;
            // 
            // label16
            // 
            this.label16.BackColor = System.Drawing.Color.Silver;
            this.label16.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label16.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label16.Location = new System.Drawing.Point(12, 184);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(68, 23);
            this.label16.TabIndex = 999;
            this.label16.Text = "氏　　名";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label17
            // 
            this.label17.BackColor = System.Drawing.Color.Silver;
            this.label17.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label17.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label17.Location = new System.Drawing.Point(97, 230);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(155, 23);
            this.label17.TabIndex = 1008;
            this.label17.Text = "0:住居者　1:非住居者";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFuyoJukyo2
            // 
            this.txtFuyoJukyo2.AutoSizeFromLength = true;
            this.txtFuyoJukyo2.DisplayLength = null;
            this.txtFuyoJukyo2.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoJukyo2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtFuyoJukyo2.Location = new System.Drawing.Point(79, 230);
            this.txtFuyoJukyo2.MaxLength = 1;
            this.txtFuyoJukyo2.Name = "txtFuyoJukyo2";
            this.txtFuyoJukyo2.Size = new System.Drawing.Size(18, 23);
            this.txtFuyoJukyo2.TabIndex = 1009;
            this.txtFuyoJukyo2.Text = "0";
            this.txtFuyoJukyo2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(227, 278);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 1007;
            this.button4.Text = "公開";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // txtFuyoMyNumberDm2
            // 
            this.txtFuyoMyNumberDm2.AutoSizeFromLength = false;
            this.txtFuyoMyNumberDm2.DisplayLength = null;
            this.txtFuyoMyNumberDm2.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoMyNumberDm2.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtFuyoMyNumberDm2.Location = new System.Drawing.Point(112, 278);
            this.txtFuyoMyNumberDm2.MaxLength = 15;
            this.txtFuyoMyNumberDm2.Name = "txtFuyoMyNumberDm2";
            this.txtFuyoMyNumberDm2.ReadOnly = true;
            this.txtFuyoMyNumberDm2.Size = new System.Drawing.Size(114, 23);
            this.txtFuyoMyNumberDm2.TabIndex = 1006;
            this.txtFuyoMyNumberDm2.Text = "************";
            // 
            // txtFuyoMyNumber2
            // 
            this.txtFuyoMyNumber2.AutoSizeFromLength = false;
            this.txtFuyoMyNumber2.DisplayLength = null;
            this.txtFuyoMyNumber2.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoMyNumber2.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtFuyoMyNumber2.Location = new System.Drawing.Point(112, 278);
            this.txtFuyoMyNumber2.MaxLength = 12;
            this.txtFuyoMyNumber2.Name = "txtFuyoMyNumber2";
            this.txtFuyoMyNumber2.ReadOnly = true;
            this.txtFuyoMyNumber2.Size = new System.Drawing.Size(114, 23);
            this.txtFuyoMyNumber2.TabIndex = 1004;
            // 
            // label18
            // 
            this.label18.BackColor = System.Drawing.Color.Silver;
            this.label18.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label18.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label18.Location = new System.Drawing.Point(12, 278);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(100, 23);
            this.label18.TabIndex = 1005;
            this.label18.Text = "マイナンバー";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Silver;
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel4.Controls.Add(this.label19);
            this.panel4.Controls.Add(this.txtFuyoDaySeinengappi2);
            this.panel4.Controls.Add(this.label20);
            this.panel4.Controls.Add(this.label21);
            this.panel4.Controls.Add(this.txtFuyoMonthSeinengappi2);
            this.panel4.Controls.Add(this.lblFuyoGengoSeinengappi2);
            this.panel4.Controls.Add(this.txtFuyoGengoYearSeinengappi2);
            this.panel4.Location = new System.Drawing.Point(79, 253);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(219, 25);
            this.panel4.TabIndex = 1002;
            // 
            // label19
            // 
            this.label19.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label19.Location = new System.Drawing.Point(164, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(21, 23);
            this.label19.TabIndex = 14;
            this.label19.Text = "日";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtFuyoDaySeinengappi2
            // 
            this.txtFuyoDaySeinengappi2.AutoSizeFromLength = false;
            this.txtFuyoDaySeinengappi2.DisplayLength = null;
            this.txtFuyoDaySeinengappi2.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoDaySeinengappi2.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtFuyoDaySeinengappi2.Location = new System.Drawing.Point(139, 0);
            this.txtFuyoDaySeinengappi2.MaxLength = 2;
            this.txtFuyoDaySeinengappi2.Name = "txtFuyoDaySeinengappi2";
            this.txtFuyoDaySeinengappi2.Size = new System.Drawing.Size(25, 23);
            this.txtFuyoDaySeinengappi2.TabIndex = 2;
            this.txtFuyoDaySeinengappi2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label20
            // 
            this.label20.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label20.Location = new System.Drawing.Point(118, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(21, 23);
            this.label20.TabIndex = 12;
            this.label20.Text = "月";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label21
            // 
            this.label21.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label21.Location = new System.Drawing.Point(72, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(21, 23);
            this.label21.TabIndex = 2;
            this.label21.Text = "年";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtFuyoMonthSeinengappi2
            // 
            this.txtFuyoMonthSeinengappi2.AutoSizeFromLength = false;
            this.txtFuyoMonthSeinengappi2.DisplayLength = null;
            this.txtFuyoMonthSeinengappi2.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoMonthSeinengappi2.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtFuyoMonthSeinengappi2.Location = new System.Drawing.Point(93, 0);
            this.txtFuyoMonthSeinengappi2.MaxLength = 2;
            this.txtFuyoMonthSeinengappi2.Name = "txtFuyoMonthSeinengappi2";
            this.txtFuyoMonthSeinengappi2.Size = new System.Drawing.Size(25, 23);
            this.txtFuyoMonthSeinengappi2.TabIndex = 1;
            this.txtFuyoMonthSeinengappi2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblFuyoGengoSeinengappi2
            // 
            this.lblFuyoGengoSeinengappi2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFuyoGengoSeinengappi2.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFuyoGengoSeinengappi2.Location = new System.Drawing.Point(0, 0);
            this.lblFuyoGengoSeinengappi2.Name = "lblFuyoGengoSeinengappi2";
            this.lblFuyoGengoSeinengappi2.Size = new System.Drawing.Size(47, 23);
            this.lblFuyoGengoSeinengappi2.TabIndex = 0;
            this.lblFuyoGengoSeinengappi2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtFuyoGengoYearSeinengappi2
            // 
            this.txtFuyoGengoYearSeinengappi2.AutoSizeFromLength = false;
            this.txtFuyoGengoYearSeinengappi2.DisplayLength = null;
            this.txtFuyoGengoYearSeinengappi2.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoGengoYearSeinengappi2.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtFuyoGengoYearSeinengappi2.Location = new System.Drawing.Point(47, 0);
            this.txtFuyoGengoYearSeinengappi2.MaxLength = 2;
            this.txtFuyoGengoYearSeinengappi2.Name = "txtFuyoGengoYearSeinengappi2";
            this.txtFuyoGengoYearSeinengappi2.Size = new System.Drawing.Size(25, 23);
            this.txtFuyoGengoYearSeinengappi2.TabIndex = 0;
            this.txtFuyoGengoYearSeinengappi2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label23
            // 
            this.label23.BackColor = System.Drawing.Color.Silver;
            this.label23.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label23.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label23.Location = new System.Drawing.Point(12, 253);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(68, 25);
            this.label23.TabIndex = 1003;
            this.label23.Text = "生年月日";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFuyoKanaNm2
            // 
            this.txtFuyoKanaNm2.AutoSizeFromLength = false;
            this.txtFuyoKanaNm2.DisplayLength = null;
            this.txtFuyoKanaNm2.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoKanaNm2.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtFuyoKanaNm2.Location = new System.Drawing.Point(79, 207);
            this.txtFuyoKanaNm2.MaxLength = 30;
            this.txtFuyoKanaNm2.Name = "txtFuyoKanaNm2";
            this.txtFuyoKanaNm2.Size = new System.Drawing.Size(219, 23);
            this.txtFuyoKanaNm2.TabIndex = 1000;
            // 
            // label24
            // 
            this.label24.BackColor = System.Drawing.Color.Silver;
            this.label24.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label24.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label24.Location = new System.Drawing.Point(12, 207);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(68, 23);
            this.label24.TabIndex = 1001;
            this.label24.Text = "フリガナ";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Silver;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(12, 93);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 23);
            this.label2.TabIndex = 997;
            this.label2.Text = "住　　居";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFuyoNm1
            // 
            this.txtFuyoNm1.AutoSizeFromLength = false;
            this.txtFuyoNm1.DisplayLength = null;
            this.txtFuyoNm1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoNm1.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtFuyoNm1.Location = new System.Drawing.Point(79, 46);
            this.txtFuyoNm1.MaxLength = 30;
            this.txtFuyoNm1.Name = "txtFuyoNm1";
            this.txtFuyoNm1.Size = new System.Drawing.Size(219, 23);
            this.txtFuyoNm1.TabIndex = 985;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.Silver;
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label6.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label6.Location = new System.Drawing.Point(12, 46);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(68, 23);
            this.label6.TabIndex = 986;
            this.label6.Text = "氏　　名";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.Silver;
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label7.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label7.Location = new System.Drawing.Point(97, 92);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(155, 23);
            this.label7.TabIndex = 995;
            this.label7.Text = "0:住居者　1:非住居者";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFuyoJukyo1
            // 
            this.txtFuyoJukyo1.AutoSizeFromLength = true;
            this.txtFuyoJukyo1.DisplayLength = null;
            this.txtFuyoJukyo1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoJukyo1.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtFuyoJukyo1.Location = new System.Drawing.Point(79, 92);
            this.txtFuyoJukyo1.MaxLength = 1;
            this.txtFuyoJukyo1.Name = "txtFuyoJukyo1";
            this.txtFuyoJukyo1.Size = new System.Drawing.Size(18, 23);
            this.txtFuyoJukyo1.TabIndex = 996;
            this.txtFuyoJukyo1.Text = "0";
            this.txtFuyoJukyo1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(227, 140);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 994;
            this.button3.Text = "公開";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // txtFuyoMyNumberDm1
            // 
            this.txtFuyoMyNumberDm1.AutoSizeFromLength = false;
            this.txtFuyoMyNumberDm1.DisplayLength = null;
            this.txtFuyoMyNumberDm1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoMyNumberDm1.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtFuyoMyNumberDm1.Location = new System.Drawing.Point(113, 140);
            this.txtFuyoMyNumberDm1.MaxLength = 15;
            this.txtFuyoMyNumberDm1.Name = "txtFuyoMyNumberDm1";
            this.txtFuyoMyNumberDm1.ReadOnly = true;
            this.txtFuyoMyNumberDm1.Size = new System.Drawing.Size(114, 23);
            this.txtFuyoMyNumberDm1.TabIndex = 993;
            this.txtFuyoMyNumberDm1.Text = "************";
            // 
            // txtFuyoMyNumber1
            // 
            this.txtFuyoMyNumber1.AutoSizeFromLength = false;
            this.txtFuyoMyNumber1.DisplayLength = null;
            this.txtFuyoMyNumber1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoMyNumber1.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtFuyoMyNumber1.Location = new System.Drawing.Point(112, 140);
            this.txtFuyoMyNumber1.MaxLength = 12;
            this.txtFuyoMyNumber1.Name = "txtFuyoMyNumber1";
            this.txtFuyoMyNumber1.ReadOnly = true;
            this.txtFuyoMyNumber1.Size = new System.Drawing.Size(114, 23);
            this.txtFuyoMyNumber1.TabIndex = 991;
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.Silver;
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label8.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label8.Location = new System.Drawing.Point(12, 140);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(100, 23);
            this.label8.TabIndex = 992;
            this.label8.Text = "マイナンバー";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Silver;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.txtFuyoDaySeinengappi1);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.txtFuyoMonthSeinengappi1);
            this.panel2.Controls.Add(this.lblFuyoGengoSeinengappi1);
            this.panel2.Controls.Add(this.txtFuyoGengoYearSeinengappi1);
            this.panel2.Location = new System.Drawing.Point(79, 115);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(219, 25);
            this.panel2.TabIndex = 989;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label9.Location = new System.Drawing.Point(164, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(21, 23);
            this.label9.TabIndex = 14;
            this.label9.Text = "日";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtFuyoDaySeinengappi1
            // 
            this.txtFuyoDaySeinengappi1.AutoSizeFromLength = false;
            this.txtFuyoDaySeinengappi1.DisplayLength = null;
            this.txtFuyoDaySeinengappi1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoDaySeinengappi1.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtFuyoDaySeinengappi1.Location = new System.Drawing.Point(139, 0);
            this.txtFuyoDaySeinengappi1.MaxLength = 2;
            this.txtFuyoDaySeinengappi1.Name = "txtFuyoDaySeinengappi1";
            this.txtFuyoDaySeinengappi1.Size = new System.Drawing.Size(25, 23);
            this.txtFuyoDaySeinengappi1.TabIndex = 2;
            this.txtFuyoDaySeinengappi1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label10.Location = new System.Drawing.Point(118, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(21, 23);
            this.label10.TabIndex = 12;
            this.label10.Text = "月";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label11.Location = new System.Drawing.Point(72, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(21, 23);
            this.label11.TabIndex = 2;
            this.label11.Text = "年";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtFuyoMonthSeinengappi1
            // 
            this.txtFuyoMonthSeinengappi1.AutoSizeFromLength = false;
            this.txtFuyoMonthSeinengappi1.DisplayLength = null;
            this.txtFuyoMonthSeinengappi1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoMonthSeinengappi1.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtFuyoMonthSeinengappi1.Location = new System.Drawing.Point(93, 0);
            this.txtFuyoMonthSeinengappi1.MaxLength = 2;
            this.txtFuyoMonthSeinengappi1.Name = "txtFuyoMonthSeinengappi1";
            this.txtFuyoMonthSeinengappi1.Size = new System.Drawing.Size(25, 23);
            this.txtFuyoMonthSeinengappi1.TabIndex = 1;
            this.txtFuyoMonthSeinengappi1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblFuyoGengoSeinengappi1
            // 
            this.lblFuyoGengoSeinengappi1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFuyoGengoSeinengappi1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFuyoGengoSeinengappi1.Location = new System.Drawing.Point(0, 0);
            this.lblFuyoGengoSeinengappi1.Name = "lblFuyoGengoSeinengappi1";
            this.lblFuyoGengoSeinengappi1.Size = new System.Drawing.Size(47, 23);
            this.lblFuyoGengoSeinengappi1.TabIndex = 0;
            this.lblFuyoGengoSeinengappi1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtFuyoGengoYearSeinengappi1
            // 
            this.txtFuyoGengoYearSeinengappi1.AutoSizeFromLength = false;
            this.txtFuyoGengoYearSeinengappi1.DisplayLength = null;
            this.txtFuyoGengoYearSeinengappi1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoGengoYearSeinengappi1.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtFuyoGengoYearSeinengappi1.Location = new System.Drawing.Point(47, 0);
            this.txtFuyoGengoYearSeinengappi1.MaxLength = 2;
            this.txtFuyoGengoYearSeinengappi1.Name = "txtFuyoGengoYearSeinengappi1";
            this.txtFuyoGengoYearSeinengappi1.Size = new System.Drawing.Size(25, 23);
            this.txtFuyoGengoYearSeinengappi1.TabIndex = 0;
            this.txtFuyoGengoYearSeinengappi1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label13
            // 
            this.label13.BackColor = System.Drawing.Color.Silver;
            this.label13.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label13.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label13.Location = new System.Drawing.Point(12, 115);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(68, 25);
            this.label13.TabIndex = 990;
            this.label13.Text = "生年月日";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFuyoKanaNm1
            // 
            this.txtFuyoKanaNm1.AutoSizeFromLength = false;
            this.txtFuyoKanaNm1.DisplayLength = null;
            this.txtFuyoKanaNm1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoKanaNm1.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtFuyoKanaNm1.Location = new System.Drawing.Point(79, 69);
            this.txtFuyoKanaNm1.MaxLength = 30;
            this.txtFuyoKanaNm1.Name = "txtFuyoKanaNm1";
            this.txtFuyoKanaNm1.Size = new System.Drawing.Size(219, 23);
            this.txtFuyoKanaNm1.TabIndex = 987;
            // 
            // label14
            // 
            this.label14.BackColor = System.Drawing.Color.Silver;
            this.label14.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label14.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label14.Location = new System.Drawing.Point(12, 69);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(68, 23);
            this.label14.TabIndex = 988;
            this.label14.Text = "フリガナ";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(9, 17);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(177, 23);
            this.label3.TabIndex = 97;
            this.label3.Text = "【控除対象扶養親族】";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.panel8);
            this.tabPage7.Location = new System.Drawing.Point(4, 29);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(721, 395);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "F5 世帯情報2";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.Transparent;
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel8.Controls.Add(this.panel9);
            this.panel8.Location = new System.Drawing.Point(20, 18);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(680, 359);
            this.panel8.TabIndex = 7;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.PeachPuff;
            this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel9.Controls.Add(this.txtIkouMimanNo);
            this.panel9.Controls.Add(this.label85);
            this.panel9.Controls.Add(this.label92);
            this.panel9.Controls.Add(this.label52);
            this.panel9.Controls.Add(this.label63);
            this.panel9.Controls.Add(this.label69);
            this.panel9.Controls.Add(this.label74);
            this.panel9.Controls.Add(this.label45);
            this.panel9.Controls.Add(this.txtFuyoMimanNm4);
            this.panel9.Controls.Add(this.label46);
            this.panel9.Controls.Add(this.label47);
            this.panel9.Controls.Add(this.txtFuyoMimanJukyo4);
            this.panel9.Controls.Add(this.button10);
            this.panel9.Controls.Add(this.txtFuyoMimanMyNumberDm4);
            this.panel9.Controls.Add(this.txtFuyoMimanMyNumber4);
            this.panel9.Controls.Add(this.label48);
            this.panel9.Controls.Add(this.panel10);
            this.panel9.Controls.Add(this.label53);
            this.panel9.Controls.Add(this.txtFuyoMimanKanaNm4);
            this.panel9.Controls.Add(this.label55);
            this.panel9.Controls.Add(this.label56);
            this.panel9.Controls.Add(this.txtFuyoMimanNm3);
            this.panel9.Controls.Add(this.label57);
            this.panel9.Controls.Add(this.label58);
            this.panel9.Controls.Add(this.txtFuyoMimanJukyo3);
            this.panel9.Controls.Add(this.button9);
            this.panel9.Controls.Add(this.txtFuyoMimanMyNumberDm3);
            this.panel9.Controls.Add(this.txtFuyoMimanMyNumber3);
            this.panel9.Controls.Add(this.label59);
            this.panel9.Controls.Add(this.panel12);
            this.panel9.Controls.Add(this.label64);
            this.panel9.Controls.Add(this.txtFuyoMimanKanaNm3);
            this.panel9.Controls.Add(this.label65);
            this.panel9.Controls.Add(this.label66);
            this.panel9.Controls.Add(this.txtFuyoMimanNm2);
            this.panel9.Controls.Add(this.label67);
            this.panel9.Controls.Add(this.label68);
            this.panel9.Controls.Add(this.txtFuyoMimanJukyo2);
            this.panel9.Controls.Add(this.button8);
            this.panel9.Controls.Add(this.txtFuyoMimanMyNumberDm2);
            this.panel9.Controls.Add(this.txtFuyoMimanMyNumber2);
            this.panel9.Controls.Add(this.label70);
            this.panel9.Controls.Add(this.panel13);
            this.panel9.Controls.Add(this.label75);
            this.panel9.Controls.Add(this.txtFuyoMimanKanaNm2);
            this.panel9.Controls.Add(this.label76);
            this.panel9.Controls.Add(this.label78);
            this.panel9.Controls.Add(this.txtFuyoMimanNm1);
            this.panel9.Controls.Add(this.label79);
            this.panel9.Controls.Add(this.label80);
            this.panel9.Controls.Add(this.txtFuyoMimanJukyo1);
            this.panel9.Controls.Add(this.button7);
            this.panel9.Controls.Add(this.txtFuyoMimanMyNumberDm1);
            this.panel9.Controls.Add(this.txtFuyoMimanMyNumber1);
            this.panel9.Controls.Add(this.label81);
            this.panel9.Controls.Add(this.panel16);
            this.panel9.Controls.Add(this.label86);
            this.panel9.Controls.Add(this.txtFuyoMimanKanaNm1);
            this.panel9.Controls.Add(this.label87);
            this.panel9.Controls.Add(this.label88);
            this.panel9.Location = new System.Drawing.Point(-2, -2);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(680, 359);
            this.panel9.TabIndex = 98;
            // 
            // txtIkouMimanNo
            // 
            this.txtIkouMimanNo.AutoSizeFromLength = false;
            this.txtIkouMimanNo.DisplayLength = null;
            this.txtIkouMimanNo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtIkouMimanNo.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtIkouMimanNo.Location = new System.Drawing.Point(91, 329);
            this.txtIkouMimanNo.MaxLength = 50;
            this.txtIkouMimanNo.Name = "txtIkouMimanNo";
            this.txtIkouMimanNo.Size = new System.Drawing.Size(456, 23);
            this.txtIkouMimanNo.TabIndex = 1046;
            this.txtIkouMimanNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LastControl_KeyDown);
            this.txtIkouMimanNo.Validating += new System.ComponentModel.CancelEventHandler(this.txtIkouMimanNo_Validating);
            // 
            // label85
            // 
            this.label85.BackColor = System.Drawing.Color.Silver;
            this.label85.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label85.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label85.Location = new System.Drawing.Point(12, 330);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(79, 23);
            this.label85.TabIndex = 1047;
            this.label85.Text = "摘要";
            this.label85.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label92
            // 
            this.label92.BackColor = System.Drawing.Color.Transparent;
            this.label92.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label92.Location = new System.Drawing.Point(13, 306);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(186, 23);
            this.label92.TabIndex = 1048;
            this.label92.Text = "【5人目以降の個人番号】";
            this.label92.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label52
            // 
            this.label52.BackColor = System.Drawing.Color.Transparent;
            this.label52.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label52.Location = new System.Drawing.Point(334, 163);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(16, 21);
            this.label52.TabIndex = 1044;
            this.label52.Text = "4";
            this.label52.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label63
            // 
            this.label63.BackColor = System.Drawing.Color.Transparent;
            this.label63.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label63.Location = new System.Drawing.Point(334, 23);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(16, 23);
            this.label63.TabIndex = 1043;
            this.label63.Text = "3";
            this.label63.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label69
            // 
            this.label69.BackColor = System.Drawing.Color.Transparent;
            this.label69.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label69.Location = new System.Drawing.Point(3, 163);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(16, 21);
            this.label69.TabIndex = 1042;
            this.label69.Text = "2";
            this.label69.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label74
            // 
            this.label74.BackColor = System.Drawing.Color.Transparent;
            this.label74.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label74.Location = new System.Drawing.Point(3, 23);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(16, 23);
            this.label74.TabIndex = 1041;
            this.label74.Text = "1";
            this.label74.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label45
            // 
            this.label45.BackColor = System.Drawing.Color.Silver;
            this.label45.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label45.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label45.Location = new System.Drawing.Point(342, 231);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(68, 23);
            this.label45.TabIndex = 1036;
            this.label45.Text = "住　　居";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFuyoMimanNm4
            // 
            this.txtFuyoMimanNm4.AutoSizeFromLength = false;
            this.txtFuyoMimanNm4.DisplayLength = null;
            this.txtFuyoMimanNm4.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoMimanNm4.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtFuyoMimanNm4.Location = new System.Drawing.Point(409, 184);
            this.txtFuyoMimanNm4.MaxLength = 30;
            this.txtFuyoMimanNm4.Name = "txtFuyoMimanNm4";
            this.txtFuyoMimanNm4.Size = new System.Drawing.Size(219, 23);
            this.txtFuyoMimanNm4.TabIndex = 1024;
            // 
            // label46
            // 
            this.label46.BackColor = System.Drawing.Color.Silver;
            this.label46.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label46.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label46.Location = new System.Drawing.Point(342, 184);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(68, 23);
            this.label46.TabIndex = 1025;
            this.label46.Text = "氏　　名";
            this.label46.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label47
            // 
            this.label47.BackColor = System.Drawing.Color.Silver;
            this.label47.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label47.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label47.Location = new System.Drawing.Point(427, 230);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(155, 23);
            this.label47.TabIndex = 1034;
            this.label47.Text = "0:住居者　1:非住居者";
            this.label47.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFuyoMimanJukyo4
            // 
            this.txtFuyoMimanJukyo4.AutoSizeFromLength = true;
            this.txtFuyoMimanJukyo4.DisplayLength = null;
            this.txtFuyoMimanJukyo4.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoMimanJukyo4.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtFuyoMimanJukyo4.Location = new System.Drawing.Point(409, 230);
            this.txtFuyoMimanJukyo4.MaxLength = 1;
            this.txtFuyoMimanJukyo4.Name = "txtFuyoMimanJukyo4";
            this.txtFuyoMimanJukyo4.Size = new System.Drawing.Size(18, 23);
            this.txtFuyoMimanJukyo4.TabIndex = 1035;
            this.txtFuyoMimanJukyo4.Text = "0";
            this.txtFuyoMimanJukyo4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(557, 278);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(75, 23);
            this.button10.TabIndex = 1033;
            this.button10.Text = "公開";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // txtFuyoMimanMyNumberDm4
            // 
            this.txtFuyoMimanMyNumberDm4.AutoSizeFromLength = false;
            this.txtFuyoMimanMyNumberDm4.DisplayLength = null;
            this.txtFuyoMimanMyNumberDm4.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoMimanMyNumberDm4.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtFuyoMimanMyNumberDm4.Location = new System.Drawing.Point(442, 278);
            this.txtFuyoMimanMyNumberDm4.MaxLength = 15;
            this.txtFuyoMimanMyNumberDm4.Name = "txtFuyoMimanMyNumberDm4";
            this.txtFuyoMimanMyNumberDm4.ReadOnly = true;
            this.txtFuyoMimanMyNumberDm4.Size = new System.Drawing.Size(114, 23);
            this.txtFuyoMimanMyNumberDm4.TabIndex = 1032;
            this.txtFuyoMimanMyNumberDm4.Text = "************";
            // 
            // txtFuyoMimanMyNumber4
            // 
            this.txtFuyoMimanMyNumber4.AutoSizeFromLength = false;
            this.txtFuyoMimanMyNumber4.DisplayLength = null;
            this.txtFuyoMimanMyNumber4.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoMimanMyNumber4.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtFuyoMimanMyNumber4.Location = new System.Drawing.Point(442, 278);
            this.txtFuyoMimanMyNumber4.MaxLength = 12;
            this.txtFuyoMimanMyNumber4.Name = "txtFuyoMimanMyNumber4";
            this.txtFuyoMimanMyNumber4.ReadOnly = true;
            this.txtFuyoMimanMyNumber4.Size = new System.Drawing.Size(114, 23);
            this.txtFuyoMimanMyNumber4.TabIndex = 1030;
            // 
            // label48
            // 
            this.label48.BackColor = System.Drawing.Color.Silver;
            this.label48.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label48.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label48.Location = new System.Drawing.Point(342, 278);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(100, 23);
            this.label48.TabIndex = 1031;
            this.label48.Text = "マイナンバー";
            this.label48.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.Silver;
            this.panel10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel10.Controls.Add(this.label49);
            this.panel10.Controls.Add(this.txtFuyoMimanDaySeinengappi4);
            this.panel10.Controls.Add(this.label50);
            this.panel10.Controls.Add(this.label51);
            this.panel10.Controls.Add(this.txtFuyoMimanMonthSeinengappi4);
            this.panel10.Controls.Add(this.lblFuyoMimanGengoSeinengappi4);
            this.panel10.Controls.Add(this.txtFuyoMimanGengoYearSeinengappi4);
            this.panel10.Location = new System.Drawing.Point(409, 253);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(219, 25);
            this.panel10.TabIndex = 1028;
            // 
            // label49
            // 
            this.label49.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label49.Location = new System.Drawing.Point(164, 0);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(21, 23);
            this.label49.TabIndex = 14;
            this.label49.Text = "日";
            this.label49.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtFuyoMimanDaySeinengappi4
            // 
            this.txtFuyoMimanDaySeinengappi4.AutoSizeFromLength = false;
            this.txtFuyoMimanDaySeinengappi4.DisplayLength = null;
            this.txtFuyoMimanDaySeinengappi4.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoMimanDaySeinengappi4.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtFuyoMimanDaySeinengappi4.Location = new System.Drawing.Point(139, 0);
            this.txtFuyoMimanDaySeinengappi4.MaxLength = 2;
            this.txtFuyoMimanDaySeinengappi4.Name = "txtFuyoMimanDaySeinengappi4";
            this.txtFuyoMimanDaySeinengappi4.Size = new System.Drawing.Size(25, 23);
            this.txtFuyoMimanDaySeinengappi4.TabIndex = 2;
            this.txtFuyoMimanDaySeinengappi4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label50
            // 
            this.label50.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label50.Location = new System.Drawing.Point(118, 0);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(21, 23);
            this.label50.TabIndex = 12;
            this.label50.Text = "月";
            this.label50.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label51
            // 
            this.label51.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label51.Location = new System.Drawing.Point(72, 0);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(21, 23);
            this.label51.TabIndex = 2;
            this.label51.Text = "年";
            this.label51.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtFuyoMimanMonthSeinengappi4
            // 
            this.txtFuyoMimanMonthSeinengappi4.AutoSizeFromLength = false;
            this.txtFuyoMimanMonthSeinengappi4.DisplayLength = null;
            this.txtFuyoMimanMonthSeinengappi4.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoMimanMonthSeinengappi4.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtFuyoMimanMonthSeinengappi4.Location = new System.Drawing.Point(93, 0);
            this.txtFuyoMimanMonthSeinengappi4.MaxLength = 2;
            this.txtFuyoMimanMonthSeinengappi4.Name = "txtFuyoMimanMonthSeinengappi4";
            this.txtFuyoMimanMonthSeinengappi4.Size = new System.Drawing.Size(25, 23);
            this.txtFuyoMimanMonthSeinengappi4.TabIndex = 1;
            this.txtFuyoMimanMonthSeinengappi4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblFuyoMimanGengoSeinengappi4
            // 
            this.lblFuyoMimanGengoSeinengappi4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFuyoMimanGengoSeinengappi4.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFuyoMimanGengoSeinengappi4.Location = new System.Drawing.Point(0, 0);
            this.lblFuyoMimanGengoSeinengappi4.Name = "lblFuyoMimanGengoSeinengappi4";
            this.lblFuyoMimanGengoSeinengappi4.Size = new System.Drawing.Size(47, 23);
            this.lblFuyoMimanGengoSeinengappi4.TabIndex = 0;
            this.lblFuyoMimanGengoSeinengappi4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtFuyoMimanGengoYearSeinengappi4
            // 
            this.txtFuyoMimanGengoYearSeinengappi4.AutoSizeFromLength = false;
            this.txtFuyoMimanGengoYearSeinengappi4.DisplayLength = null;
            this.txtFuyoMimanGengoYearSeinengappi4.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoMimanGengoYearSeinengappi4.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtFuyoMimanGengoYearSeinengappi4.Location = new System.Drawing.Point(47, 0);
            this.txtFuyoMimanGengoYearSeinengappi4.MaxLength = 2;
            this.txtFuyoMimanGengoYearSeinengappi4.Name = "txtFuyoMimanGengoYearSeinengappi4";
            this.txtFuyoMimanGengoYearSeinengappi4.Size = new System.Drawing.Size(25, 23);
            this.txtFuyoMimanGengoYearSeinengappi4.TabIndex = 0;
            this.txtFuyoMimanGengoYearSeinengappi4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label53
            // 
            this.label53.BackColor = System.Drawing.Color.Silver;
            this.label53.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label53.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label53.Location = new System.Drawing.Point(342, 253);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(68, 25);
            this.label53.TabIndex = 1029;
            this.label53.Text = "生年月日";
            this.label53.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFuyoMimanKanaNm4
            // 
            this.txtFuyoMimanKanaNm4.AutoSizeFromLength = false;
            this.txtFuyoMimanKanaNm4.DisplayLength = null;
            this.txtFuyoMimanKanaNm4.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoMimanKanaNm4.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtFuyoMimanKanaNm4.Location = new System.Drawing.Point(409, 207);
            this.txtFuyoMimanKanaNm4.MaxLength = 30;
            this.txtFuyoMimanKanaNm4.Name = "txtFuyoMimanKanaNm4";
            this.txtFuyoMimanKanaNm4.Size = new System.Drawing.Size(219, 23);
            this.txtFuyoMimanKanaNm4.TabIndex = 1026;
            // 
            // label55
            // 
            this.label55.BackColor = System.Drawing.Color.Silver;
            this.label55.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label55.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label55.Location = new System.Drawing.Point(342, 207);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(68, 23);
            this.label55.TabIndex = 1027;
            this.label55.Text = "フリガナ";
            this.label55.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label56
            // 
            this.label56.BackColor = System.Drawing.Color.Silver;
            this.label56.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label56.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label56.Location = new System.Drawing.Point(342, 93);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(68, 23);
            this.label56.TabIndex = 1023;
            this.label56.Text = "住　　居";
            this.label56.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFuyoMimanNm3
            // 
            this.txtFuyoMimanNm3.AutoSizeFromLength = false;
            this.txtFuyoMimanNm3.DisplayLength = null;
            this.txtFuyoMimanNm3.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoMimanNm3.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtFuyoMimanNm3.Location = new System.Drawing.Point(409, 46);
            this.txtFuyoMimanNm3.MaxLength = 30;
            this.txtFuyoMimanNm3.Name = "txtFuyoMimanNm3";
            this.txtFuyoMimanNm3.Size = new System.Drawing.Size(219, 23);
            this.txtFuyoMimanNm3.TabIndex = 1011;
            // 
            // label57
            // 
            this.label57.BackColor = System.Drawing.Color.Silver;
            this.label57.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label57.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label57.Location = new System.Drawing.Point(342, 46);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(68, 23);
            this.label57.TabIndex = 1012;
            this.label57.Text = "氏　　名";
            this.label57.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label58
            // 
            this.label58.BackColor = System.Drawing.Color.Silver;
            this.label58.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label58.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label58.Location = new System.Drawing.Point(427, 92);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(155, 23);
            this.label58.TabIndex = 1021;
            this.label58.Text = "0:住居者　1:非住居者";
            this.label58.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFuyoMimanJukyo3
            // 
            this.txtFuyoMimanJukyo3.AutoSizeFromLength = true;
            this.txtFuyoMimanJukyo3.DisplayLength = null;
            this.txtFuyoMimanJukyo3.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoMimanJukyo3.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtFuyoMimanJukyo3.Location = new System.Drawing.Point(409, 92);
            this.txtFuyoMimanJukyo3.MaxLength = 1;
            this.txtFuyoMimanJukyo3.Name = "txtFuyoMimanJukyo3";
            this.txtFuyoMimanJukyo3.Size = new System.Drawing.Size(18, 23);
            this.txtFuyoMimanJukyo3.TabIndex = 1022;
            this.txtFuyoMimanJukyo3.Text = "0";
            this.txtFuyoMimanJukyo3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(557, 140);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(75, 23);
            this.button9.TabIndex = 1020;
            this.button9.Text = "公開";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // txtFuyoMimanMyNumberDm3
            // 
            this.txtFuyoMimanMyNumberDm3.AutoSizeFromLength = false;
            this.txtFuyoMimanMyNumberDm3.DisplayLength = null;
            this.txtFuyoMimanMyNumberDm3.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoMimanMyNumberDm3.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtFuyoMimanMyNumberDm3.Location = new System.Drawing.Point(442, 140);
            this.txtFuyoMimanMyNumberDm3.MaxLength = 15;
            this.txtFuyoMimanMyNumberDm3.Name = "txtFuyoMimanMyNumberDm3";
            this.txtFuyoMimanMyNumberDm3.ReadOnly = true;
            this.txtFuyoMimanMyNumberDm3.Size = new System.Drawing.Size(114, 23);
            this.txtFuyoMimanMyNumberDm3.TabIndex = 1019;
            this.txtFuyoMimanMyNumberDm3.Text = "************";
            // 
            // txtFuyoMimanMyNumber3
            // 
            this.txtFuyoMimanMyNumber3.AutoSizeFromLength = false;
            this.txtFuyoMimanMyNumber3.DisplayLength = null;
            this.txtFuyoMimanMyNumber3.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoMimanMyNumber3.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtFuyoMimanMyNumber3.Location = new System.Drawing.Point(442, 140);
            this.txtFuyoMimanMyNumber3.MaxLength = 12;
            this.txtFuyoMimanMyNumber3.Name = "txtFuyoMimanMyNumber3";
            this.txtFuyoMimanMyNumber3.ReadOnly = true;
            this.txtFuyoMimanMyNumber3.Size = new System.Drawing.Size(114, 23);
            this.txtFuyoMimanMyNumber3.TabIndex = 1017;
            // 
            // label59
            // 
            this.label59.BackColor = System.Drawing.Color.Silver;
            this.label59.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label59.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label59.Location = new System.Drawing.Point(342, 140);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(100, 23);
            this.label59.TabIndex = 1018;
            this.label59.Text = "マイナンバー";
            this.label59.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.Silver;
            this.panel12.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel12.Controls.Add(this.label60);
            this.panel12.Controls.Add(this.txtFuyoMimanDaySeinengappi3);
            this.panel12.Controls.Add(this.label61);
            this.panel12.Controls.Add(this.label62);
            this.panel12.Controls.Add(this.txtFuyoMimanMonthSeinengappi3);
            this.panel12.Controls.Add(this.lblFuyoMimanGengoSeinengappi3);
            this.panel12.Controls.Add(this.txtFuyoMimanGengoYearSeinengappi3);
            this.panel12.Location = new System.Drawing.Point(409, 115);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(219, 25);
            this.panel12.TabIndex = 1015;
            // 
            // label60
            // 
            this.label60.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label60.Location = new System.Drawing.Point(164, 0);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(21, 23);
            this.label60.TabIndex = 14;
            this.label60.Text = "日";
            this.label60.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtFuyoMimanDaySeinengappi3
            // 
            this.txtFuyoMimanDaySeinengappi3.AutoSizeFromLength = false;
            this.txtFuyoMimanDaySeinengappi3.DisplayLength = null;
            this.txtFuyoMimanDaySeinengappi3.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoMimanDaySeinengappi3.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtFuyoMimanDaySeinengappi3.Location = new System.Drawing.Point(139, 0);
            this.txtFuyoMimanDaySeinengappi3.MaxLength = 2;
            this.txtFuyoMimanDaySeinengappi3.Name = "txtFuyoMimanDaySeinengappi3";
            this.txtFuyoMimanDaySeinengappi3.Size = new System.Drawing.Size(25, 23);
            this.txtFuyoMimanDaySeinengappi3.TabIndex = 2;
            this.txtFuyoMimanDaySeinengappi3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label61
            // 
            this.label61.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label61.Location = new System.Drawing.Point(118, 0);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(21, 23);
            this.label61.TabIndex = 12;
            this.label61.Text = "月";
            this.label61.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label62
            // 
            this.label62.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label62.Location = new System.Drawing.Point(72, 0);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(21, 23);
            this.label62.TabIndex = 2;
            this.label62.Text = "年";
            this.label62.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtFuyoMimanMonthSeinengappi3
            // 
            this.txtFuyoMimanMonthSeinengappi3.AutoSizeFromLength = false;
            this.txtFuyoMimanMonthSeinengappi3.DisplayLength = null;
            this.txtFuyoMimanMonthSeinengappi3.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoMimanMonthSeinengappi3.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtFuyoMimanMonthSeinengappi3.Location = new System.Drawing.Point(93, 0);
            this.txtFuyoMimanMonthSeinengappi3.MaxLength = 2;
            this.txtFuyoMimanMonthSeinengappi3.Name = "txtFuyoMimanMonthSeinengappi3";
            this.txtFuyoMimanMonthSeinengappi3.Size = new System.Drawing.Size(25, 23);
            this.txtFuyoMimanMonthSeinengappi3.TabIndex = 1;
            this.txtFuyoMimanMonthSeinengappi3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblFuyoMimanGengoSeinengappi3
            // 
            this.lblFuyoMimanGengoSeinengappi3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFuyoMimanGengoSeinengappi3.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFuyoMimanGengoSeinengappi3.Location = new System.Drawing.Point(0, 0);
            this.lblFuyoMimanGengoSeinengappi3.Name = "lblFuyoMimanGengoSeinengappi3";
            this.lblFuyoMimanGengoSeinengappi3.Size = new System.Drawing.Size(47, 23);
            this.lblFuyoMimanGengoSeinengappi3.TabIndex = 0;
            this.lblFuyoMimanGengoSeinengappi3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtFuyoMimanGengoYearSeinengappi3
            // 
            this.txtFuyoMimanGengoYearSeinengappi3.AutoSizeFromLength = false;
            this.txtFuyoMimanGengoYearSeinengappi3.DisplayLength = null;
            this.txtFuyoMimanGengoYearSeinengappi3.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoMimanGengoYearSeinengappi3.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtFuyoMimanGengoYearSeinengappi3.Location = new System.Drawing.Point(47, 0);
            this.txtFuyoMimanGengoYearSeinengappi3.MaxLength = 2;
            this.txtFuyoMimanGengoYearSeinengappi3.Name = "txtFuyoMimanGengoYearSeinengappi3";
            this.txtFuyoMimanGengoYearSeinengappi3.Size = new System.Drawing.Size(25, 23);
            this.txtFuyoMimanGengoYearSeinengappi3.TabIndex = 0;
            this.txtFuyoMimanGengoYearSeinengappi3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label64
            // 
            this.label64.BackColor = System.Drawing.Color.Silver;
            this.label64.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label64.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label64.Location = new System.Drawing.Point(342, 115);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(68, 25);
            this.label64.TabIndex = 1016;
            this.label64.Text = "生年月日";
            this.label64.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFuyoMimanKanaNm3
            // 
            this.txtFuyoMimanKanaNm3.AutoSizeFromLength = false;
            this.txtFuyoMimanKanaNm3.DisplayLength = null;
            this.txtFuyoMimanKanaNm3.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoMimanKanaNm3.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtFuyoMimanKanaNm3.Location = new System.Drawing.Point(409, 69);
            this.txtFuyoMimanKanaNm3.MaxLength = 30;
            this.txtFuyoMimanKanaNm3.Name = "txtFuyoMimanKanaNm3";
            this.txtFuyoMimanKanaNm3.Size = new System.Drawing.Size(219, 23);
            this.txtFuyoMimanKanaNm3.TabIndex = 1013;
            // 
            // label65
            // 
            this.label65.BackColor = System.Drawing.Color.Silver;
            this.label65.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label65.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label65.Location = new System.Drawing.Point(342, 69);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(68, 23);
            this.label65.TabIndex = 1014;
            this.label65.Text = "フリガナ";
            this.label65.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label66
            // 
            this.label66.BackColor = System.Drawing.Color.Silver;
            this.label66.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label66.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label66.Location = new System.Drawing.Point(12, 231);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(68, 23);
            this.label66.TabIndex = 1010;
            this.label66.Text = "住　　居";
            this.label66.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFuyoMimanNm2
            // 
            this.txtFuyoMimanNm2.AutoSizeFromLength = false;
            this.txtFuyoMimanNm2.DisplayLength = null;
            this.txtFuyoMimanNm2.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoMimanNm2.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtFuyoMimanNm2.Location = new System.Drawing.Point(79, 184);
            this.txtFuyoMimanNm2.MaxLength = 30;
            this.txtFuyoMimanNm2.Name = "txtFuyoMimanNm2";
            this.txtFuyoMimanNm2.Size = new System.Drawing.Size(219, 23);
            this.txtFuyoMimanNm2.TabIndex = 998;
            // 
            // label67
            // 
            this.label67.BackColor = System.Drawing.Color.Silver;
            this.label67.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label67.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label67.Location = new System.Drawing.Point(12, 184);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(68, 23);
            this.label67.TabIndex = 999;
            this.label67.Text = "氏　　名";
            this.label67.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label68
            // 
            this.label68.BackColor = System.Drawing.Color.Silver;
            this.label68.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label68.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label68.Location = new System.Drawing.Point(97, 230);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(155, 23);
            this.label68.TabIndex = 1008;
            this.label68.Text = "0:住居者　1:非住居者";
            this.label68.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFuyoMimanJukyo2
            // 
            this.txtFuyoMimanJukyo2.AutoSizeFromLength = true;
            this.txtFuyoMimanJukyo2.DisplayLength = null;
            this.txtFuyoMimanJukyo2.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoMimanJukyo2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtFuyoMimanJukyo2.Location = new System.Drawing.Point(79, 230);
            this.txtFuyoMimanJukyo2.MaxLength = 1;
            this.txtFuyoMimanJukyo2.Name = "txtFuyoMimanJukyo2";
            this.txtFuyoMimanJukyo2.Size = new System.Drawing.Size(18, 23);
            this.txtFuyoMimanJukyo2.TabIndex = 1009;
            this.txtFuyoMimanJukyo2.Text = "0";
            this.txtFuyoMimanJukyo2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(227, 278);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(75, 23);
            this.button8.TabIndex = 1007;
            this.button8.Text = "公開";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // txtFuyoMimanMyNumberDm2
            // 
            this.txtFuyoMimanMyNumberDm2.AutoSizeFromLength = false;
            this.txtFuyoMimanMyNumberDm2.DisplayLength = null;
            this.txtFuyoMimanMyNumberDm2.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoMimanMyNumberDm2.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtFuyoMimanMyNumberDm2.Location = new System.Drawing.Point(112, 278);
            this.txtFuyoMimanMyNumberDm2.MaxLength = 15;
            this.txtFuyoMimanMyNumberDm2.Name = "txtFuyoMimanMyNumberDm2";
            this.txtFuyoMimanMyNumberDm2.ReadOnly = true;
            this.txtFuyoMimanMyNumberDm2.Size = new System.Drawing.Size(114, 23);
            this.txtFuyoMimanMyNumberDm2.TabIndex = 1006;
            this.txtFuyoMimanMyNumberDm2.Text = "************";
            // 
            // txtFuyoMimanMyNumber2
            // 
            this.txtFuyoMimanMyNumber2.AutoSizeFromLength = false;
            this.txtFuyoMimanMyNumber2.DisplayLength = null;
            this.txtFuyoMimanMyNumber2.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoMimanMyNumber2.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtFuyoMimanMyNumber2.Location = new System.Drawing.Point(112, 278);
            this.txtFuyoMimanMyNumber2.MaxLength = 12;
            this.txtFuyoMimanMyNumber2.Name = "txtFuyoMimanMyNumber2";
            this.txtFuyoMimanMyNumber2.ReadOnly = true;
            this.txtFuyoMimanMyNumber2.Size = new System.Drawing.Size(114, 23);
            this.txtFuyoMimanMyNumber2.TabIndex = 1004;
            // 
            // label70
            // 
            this.label70.BackColor = System.Drawing.Color.Silver;
            this.label70.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label70.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label70.Location = new System.Drawing.Point(12, 278);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(100, 23);
            this.label70.TabIndex = 1005;
            this.label70.Text = "マイナンバー";
            this.label70.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.Silver;
            this.panel13.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel13.Controls.Add(this.label71);
            this.panel13.Controls.Add(this.txtFuyoMimanDaySeinengappi2);
            this.panel13.Controls.Add(this.label72);
            this.panel13.Controls.Add(this.label73);
            this.panel13.Controls.Add(this.txtFuyoMimanMonthSeinengappi2);
            this.panel13.Controls.Add(this.lblFuyoMimanGengoSeinengappi2);
            this.panel13.Controls.Add(this.txtFuyoMimanGengoYearSeinengappi2);
            this.panel13.Location = new System.Drawing.Point(79, 253);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(219, 25);
            this.panel13.TabIndex = 1002;
            // 
            // label71
            // 
            this.label71.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label71.Location = new System.Drawing.Point(164, 0);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(21, 23);
            this.label71.TabIndex = 14;
            this.label71.Text = "日";
            this.label71.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtFuyoMimanDaySeinengappi2
            // 
            this.txtFuyoMimanDaySeinengappi2.AutoSizeFromLength = false;
            this.txtFuyoMimanDaySeinengappi2.DisplayLength = null;
            this.txtFuyoMimanDaySeinengappi2.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoMimanDaySeinengappi2.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtFuyoMimanDaySeinengappi2.Location = new System.Drawing.Point(139, 0);
            this.txtFuyoMimanDaySeinengappi2.MaxLength = 2;
            this.txtFuyoMimanDaySeinengappi2.Name = "txtFuyoMimanDaySeinengappi2";
            this.txtFuyoMimanDaySeinengappi2.Size = new System.Drawing.Size(25, 23);
            this.txtFuyoMimanDaySeinengappi2.TabIndex = 2;
            this.txtFuyoMimanDaySeinengappi2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label72
            // 
            this.label72.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label72.Location = new System.Drawing.Point(118, 0);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(21, 23);
            this.label72.TabIndex = 12;
            this.label72.Text = "月";
            this.label72.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label73
            // 
            this.label73.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label73.Location = new System.Drawing.Point(72, 0);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(21, 23);
            this.label73.TabIndex = 2;
            this.label73.Text = "年";
            this.label73.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtFuyoMimanMonthSeinengappi2
            // 
            this.txtFuyoMimanMonthSeinengappi2.AutoSizeFromLength = false;
            this.txtFuyoMimanMonthSeinengappi2.DisplayLength = null;
            this.txtFuyoMimanMonthSeinengappi2.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoMimanMonthSeinengappi2.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtFuyoMimanMonthSeinengappi2.Location = new System.Drawing.Point(93, 0);
            this.txtFuyoMimanMonthSeinengappi2.MaxLength = 2;
            this.txtFuyoMimanMonthSeinengappi2.Name = "txtFuyoMimanMonthSeinengappi2";
            this.txtFuyoMimanMonthSeinengappi2.Size = new System.Drawing.Size(25, 23);
            this.txtFuyoMimanMonthSeinengappi2.TabIndex = 1;
            this.txtFuyoMimanMonthSeinengappi2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblFuyoMimanGengoSeinengappi2
            // 
            this.lblFuyoMimanGengoSeinengappi2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFuyoMimanGengoSeinengappi2.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFuyoMimanGengoSeinengappi2.Location = new System.Drawing.Point(0, 0);
            this.lblFuyoMimanGengoSeinengappi2.Name = "lblFuyoMimanGengoSeinengappi2";
            this.lblFuyoMimanGengoSeinengappi2.Size = new System.Drawing.Size(47, 23);
            this.lblFuyoMimanGengoSeinengappi2.TabIndex = 0;
            this.lblFuyoMimanGengoSeinengappi2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtFuyoMimanGengoYearSeinengappi2
            // 
            this.txtFuyoMimanGengoYearSeinengappi2.AutoSizeFromLength = false;
            this.txtFuyoMimanGengoYearSeinengappi2.DisplayLength = null;
            this.txtFuyoMimanGengoYearSeinengappi2.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoMimanGengoYearSeinengappi2.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtFuyoMimanGengoYearSeinengappi2.Location = new System.Drawing.Point(47, 0);
            this.txtFuyoMimanGengoYearSeinengappi2.MaxLength = 2;
            this.txtFuyoMimanGengoYearSeinengappi2.Name = "txtFuyoMimanGengoYearSeinengappi2";
            this.txtFuyoMimanGengoYearSeinengappi2.Size = new System.Drawing.Size(25, 23);
            this.txtFuyoMimanGengoYearSeinengappi2.TabIndex = 0;
            this.txtFuyoMimanGengoYearSeinengappi2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label75
            // 
            this.label75.BackColor = System.Drawing.Color.Silver;
            this.label75.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label75.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label75.Location = new System.Drawing.Point(12, 253);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(68, 25);
            this.label75.TabIndex = 1003;
            this.label75.Text = "生年月日";
            this.label75.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFuyoMimanKanaNm2
            // 
            this.txtFuyoMimanKanaNm2.AutoSizeFromLength = false;
            this.txtFuyoMimanKanaNm2.DisplayLength = null;
            this.txtFuyoMimanKanaNm2.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoMimanKanaNm2.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtFuyoMimanKanaNm2.Location = new System.Drawing.Point(79, 207);
            this.txtFuyoMimanKanaNm2.MaxLength = 30;
            this.txtFuyoMimanKanaNm2.Name = "txtFuyoMimanKanaNm2";
            this.txtFuyoMimanKanaNm2.Size = new System.Drawing.Size(219, 23);
            this.txtFuyoMimanKanaNm2.TabIndex = 1000;
            // 
            // label76
            // 
            this.label76.BackColor = System.Drawing.Color.Silver;
            this.label76.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label76.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label76.Location = new System.Drawing.Point(12, 207);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(68, 23);
            this.label76.TabIndex = 1001;
            this.label76.Text = "フリガナ";
            this.label76.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label78
            // 
            this.label78.BackColor = System.Drawing.Color.Silver;
            this.label78.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label78.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label78.Location = new System.Drawing.Point(12, 93);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(68, 23);
            this.label78.TabIndex = 997;
            this.label78.Text = "住　　居";
            this.label78.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFuyoMimanNm1
            // 
            this.txtFuyoMimanNm1.AutoSizeFromLength = false;
            this.txtFuyoMimanNm1.DisplayLength = null;
            this.txtFuyoMimanNm1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoMimanNm1.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtFuyoMimanNm1.Location = new System.Drawing.Point(79, 46);
            this.txtFuyoMimanNm1.MaxLength = 30;
            this.txtFuyoMimanNm1.Name = "txtFuyoMimanNm1";
            this.txtFuyoMimanNm1.Size = new System.Drawing.Size(219, 23);
            this.txtFuyoMimanNm1.TabIndex = 985;
            // 
            // label79
            // 
            this.label79.BackColor = System.Drawing.Color.Silver;
            this.label79.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label79.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label79.Location = new System.Drawing.Point(12, 46);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(68, 23);
            this.label79.TabIndex = 986;
            this.label79.Text = "氏　　名";
            this.label79.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label80
            // 
            this.label80.BackColor = System.Drawing.Color.Silver;
            this.label80.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label80.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label80.Location = new System.Drawing.Point(97, 92);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(155, 23);
            this.label80.TabIndex = 995;
            this.label80.Text = "0:住居者　1:非住居者";
            this.label80.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFuyoMimanJukyo1
            // 
            this.txtFuyoMimanJukyo1.AutoSizeFromLength = true;
            this.txtFuyoMimanJukyo1.DisplayLength = null;
            this.txtFuyoMimanJukyo1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoMimanJukyo1.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtFuyoMimanJukyo1.Location = new System.Drawing.Point(79, 92);
            this.txtFuyoMimanJukyo1.MaxLength = 1;
            this.txtFuyoMimanJukyo1.Name = "txtFuyoMimanJukyo1";
            this.txtFuyoMimanJukyo1.Size = new System.Drawing.Size(18, 23);
            this.txtFuyoMimanJukyo1.TabIndex = 996;
            this.txtFuyoMimanJukyo1.Text = "0";
            this.txtFuyoMimanJukyo1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(227, 140);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(75, 23);
            this.button7.TabIndex = 994;
            this.button7.Text = "公開";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // txtFuyoMimanMyNumberDm1
            // 
            this.txtFuyoMimanMyNumberDm1.AutoSizeFromLength = false;
            this.txtFuyoMimanMyNumberDm1.DisplayLength = null;
            this.txtFuyoMimanMyNumberDm1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoMimanMyNumberDm1.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtFuyoMimanMyNumberDm1.Location = new System.Drawing.Point(113, 140);
            this.txtFuyoMimanMyNumberDm1.MaxLength = 15;
            this.txtFuyoMimanMyNumberDm1.Name = "txtFuyoMimanMyNumberDm1";
            this.txtFuyoMimanMyNumberDm1.ReadOnly = true;
            this.txtFuyoMimanMyNumberDm1.Size = new System.Drawing.Size(114, 23);
            this.txtFuyoMimanMyNumberDm1.TabIndex = 993;
            this.txtFuyoMimanMyNumberDm1.Text = "************";
            // 
            // txtFuyoMimanMyNumber1
            // 
            this.txtFuyoMimanMyNumber1.AutoSizeFromLength = false;
            this.txtFuyoMimanMyNumber1.DisplayLength = null;
            this.txtFuyoMimanMyNumber1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoMimanMyNumber1.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtFuyoMimanMyNumber1.Location = new System.Drawing.Point(112, 140);
            this.txtFuyoMimanMyNumber1.MaxLength = 12;
            this.txtFuyoMimanMyNumber1.Name = "txtFuyoMimanMyNumber1";
            this.txtFuyoMimanMyNumber1.ReadOnly = true;
            this.txtFuyoMimanMyNumber1.Size = new System.Drawing.Size(114, 23);
            this.txtFuyoMimanMyNumber1.TabIndex = 991;
            // 
            // label81
            // 
            this.label81.BackColor = System.Drawing.Color.Silver;
            this.label81.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label81.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label81.Location = new System.Drawing.Point(12, 140);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(100, 23);
            this.label81.TabIndex = 992;
            this.label81.Text = "マイナンバー";
            this.label81.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.Color.Silver;
            this.panel16.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel16.Controls.Add(this.label82);
            this.panel16.Controls.Add(this.txtFuyoMimanDaySeinengappi1);
            this.panel16.Controls.Add(this.label83);
            this.panel16.Controls.Add(this.label84);
            this.panel16.Controls.Add(this.txtFuyoMimanMonthSeinengappi1);
            this.panel16.Controls.Add(this.lblFuyoMimanGengoSeinengappi1);
            this.panel16.Controls.Add(this.txtFuyoMimanGengoYearSeinengappi1);
            this.panel16.Location = new System.Drawing.Point(79, 115);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(219, 25);
            this.panel16.TabIndex = 989;
            // 
            // label82
            // 
            this.label82.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label82.Location = new System.Drawing.Point(164, 0);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(21, 23);
            this.label82.TabIndex = 14;
            this.label82.Text = "日";
            this.label82.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtFuyoMimanDaySeinengappi1
            // 
            this.txtFuyoMimanDaySeinengappi1.AutoSizeFromLength = false;
            this.txtFuyoMimanDaySeinengappi1.DisplayLength = null;
            this.txtFuyoMimanDaySeinengappi1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoMimanDaySeinengappi1.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtFuyoMimanDaySeinengappi1.Location = new System.Drawing.Point(139, 0);
            this.txtFuyoMimanDaySeinengappi1.MaxLength = 2;
            this.txtFuyoMimanDaySeinengappi1.Name = "txtFuyoMimanDaySeinengappi1";
            this.txtFuyoMimanDaySeinengappi1.Size = new System.Drawing.Size(25, 23);
            this.txtFuyoMimanDaySeinengappi1.TabIndex = 2;
            this.txtFuyoMimanDaySeinengappi1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label83
            // 
            this.label83.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label83.Location = new System.Drawing.Point(118, 0);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(21, 23);
            this.label83.TabIndex = 12;
            this.label83.Text = "月";
            this.label83.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label84
            // 
            this.label84.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label84.Location = new System.Drawing.Point(72, 0);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(21, 23);
            this.label84.TabIndex = 2;
            this.label84.Text = "年";
            this.label84.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtFuyoMimanMonthSeinengappi1
            // 
            this.txtFuyoMimanMonthSeinengappi1.AutoSizeFromLength = false;
            this.txtFuyoMimanMonthSeinengappi1.DisplayLength = null;
            this.txtFuyoMimanMonthSeinengappi1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoMimanMonthSeinengappi1.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtFuyoMimanMonthSeinengappi1.Location = new System.Drawing.Point(93, 0);
            this.txtFuyoMimanMonthSeinengappi1.MaxLength = 2;
            this.txtFuyoMimanMonthSeinengappi1.Name = "txtFuyoMimanMonthSeinengappi1";
            this.txtFuyoMimanMonthSeinengappi1.Size = new System.Drawing.Size(25, 23);
            this.txtFuyoMimanMonthSeinengappi1.TabIndex = 1;
            this.txtFuyoMimanMonthSeinengappi1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblFuyoMimanGengoSeinengappi1
            // 
            this.lblFuyoMimanGengoSeinengappi1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFuyoMimanGengoSeinengappi1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFuyoMimanGengoSeinengappi1.Location = new System.Drawing.Point(0, 0);
            this.lblFuyoMimanGengoSeinengappi1.Name = "lblFuyoMimanGengoSeinengappi1";
            this.lblFuyoMimanGengoSeinengappi1.Size = new System.Drawing.Size(47, 23);
            this.lblFuyoMimanGengoSeinengappi1.TabIndex = 0;
            this.lblFuyoMimanGengoSeinengappi1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtFuyoMimanGengoYearSeinengappi1
            // 
            this.txtFuyoMimanGengoYearSeinengappi1.AutoSizeFromLength = false;
            this.txtFuyoMimanGengoYearSeinengappi1.DisplayLength = null;
            this.txtFuyoMimanGengoYearSeinengappi1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoMimanGengoYearSeinengappi1.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtFuyoMimanGengoYearSeinengappi1.Location = new System.Drawing.Point(47, 0);
            this.txtFuyoMimanGengoYearSeinengappi1.MaxLength = 2;
            this.txtFuyoMimanGengoYearSeinengappi1.Name = "txtFuyoMimanGengoYearSeinengappi1";
            this.txtFuyoMimanGengoYearSeinengappi1.Size = new System.Drawing.Size(25, 23);
            this.txtFuyoMimanGengoYearSeinengappi1.TabIndex = 0;
            this.txtFuyoMimanGengoYearSeinengappi1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label86
            // 
            this.label86.BackColor = System.Drawing.Color.Silver;
            this.label86.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label86.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label86.Location = new System.Drawing.Point(12, 115);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(68, 25);
            this.label86.TabIndex = 990;
            this.label86.Text = "生年月日";
            this.label86.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFuyoMimanKanaNm1
            // 
            this.txtFuyoMimanKanaNm1.AutoSizeFromLength = false;
            this.txtFuyoMimanKanaNm1.DisplayLength = null;
            this.txtFuyoMimanKanaNm1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFuyoMimanKanaNm1.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtFuyoMimanKanaNm1.Location = new System.Drawing.Point(79, 69);
            this.txtFuyoMimanKanaNm1.MaxLength = 30;
            this.txtFuyoMimanKanaNm1.Name = "txtFuyoMimanKanaNm1";
            this.txtFuyoMimanKanaNm1.Size = new System.Drawing.Size(219, 23);
            this.txtFuyoMimanKanaNm1.TabIndex = 987;
            // 
            // label87
            // 
            this.label87.BackColor = System.Drawing.Color.Silver;
            this.label87.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label87.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label87.Location = new System.Drawing.Point(12, 69);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(68, 23);
            this.label87.TabIndex = 988;
            this.label87.Text = "フリガナ";
            this.label87.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label88
            // 
            this.label88.BackColor = System.Drawing.Color.Transparent;
            this.label88.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label88.Location = new System.Drawing.Point(9, 16);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(177, 23);
            this.label88.TabIndex = 97;
            this.label88.Text = "【16歳未満の扶養親族】";
            this.label88.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // KYCM1022
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(750, 543);
            this.Controls.Add(this.tabPages);
            this.Controls.Add(this.lblShainNmDisp);
            this.Controls.Add(this.txtShainCd);
            this.Controls.Add(this.lblShainCd);
            this.Controls.Add(this.lblMODE);
            this.Name = "KYCM1022";
            this.Text = "社員の登録";
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblMODE, 0);
            this.Controls.SetChildIndex(this.lblShainCd, 0);
            this.Controls.SetChildIndex(this.txtShainCd, 0);
            this.Controls.SetChildIndex(this.lblShainNmDisp, 0);
            this.Controls.SetChildIndex(this.tabPages, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.pnlDebug.ResumeLayout(false);
            this.tabPages.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.pnlPage1.ResumeLayout(false);
            this.pnlPage1.PerformLayout();
            this.pnlTaishokuNengappi.ResumeLayout(false);
            this.pnlTaishokuNengappi.PerformLayout();
            this.pnlNyushaNengappi.ResumeLayout(false);
            this.pnlNyushaNengappi.PerformLayout();
            this.pnlSeinengappi.ResumeLayout(false);
            this.pnlSeinengappi.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.pnlFuyoHaigushaSeinengappi.ResumeLayout(false);
            this.pnlFuyoHaigushaSeinengappi.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvShikyu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvKojo)).EndInit();
            this.tabPage5.ResumeLayout(false);
            this.panel15.ResumeLayout(false);
            this.panel15.PerformLayout();
            this.tabPage6.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.tabPage7.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.panel16.ResumeLayout(false);
            this.panel16.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private jp.co.fsi.common.controls.FsiTextBox txtShainCd;
        private System.Windows.Forms.Label lblShainCd;
        private System.Windows.Forms.Label lblMODE;
        private System.Windows.Forms.Label lblShainNmDisp;
        private System.Windows.Forms.TabControl tabPages;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private jp.co.fsi.common.FsiPanel pnlPage1;
        private jp.co.fsi.common.FsiPanel pnlTaishokuNengappi;
        private System.Windows.Forms.Label lblDayTaishokuNengappi;
        private common.controls.FsiTextBox txtDayTaishokuNengappi;
        private System.Windows.Forms.Label lblMonthTaishokuNengappi;
        private System.Windows.Forms.Label lblGengoYearTaishokuNengappi;
        private common.controls.FsiTextBox txtMonthTaishokuNengappi;
        private System.Windows.Forms.Label lblGengoTaishokuNengappi;
        private common.controls.FsiTextBox txtGengoYearTaishokuNengappi;
        private System.Windows.Forms.Label lblTaishokuNengappi;
        private jp.co.fsi.common.FsiPanel pnlNyushaNengappi;
        private System.Windows.Forms.Label lblDayNyushaNengappi;
        private common.controls.FsiTextBox txtDayNyushaNengappi;
        private System.Windows.Forms.Label lblMonthNyushaNengappi;
        private System.Windows.Forms.Label lblGengoYearNyushaNengappi;
        private common.controls.FsiTextBox txtMonthNyushaNengappi;
        private System.Windows.Forms.Label lblGengoNyushaNengappi;
        private common.controls.FsiTextBox txtGengoYearNyushaNengappi;
        private System.Windows.Forms.Label lblNyushaNengappi;
        private System.Windows.Forms.Label lblKyuyoKeitaiNm;
        private common.controls.FsiTextBox txtKyuyoKeitai;
        private System.Windows.Forms.Label lblKyuyoKeitai;
        private System.Windows.Forms.Label lblYakushokuNm;
        private common.controls.FsiTextBox txtYakushokuCd;
        private System.Windows.Forms.Label lblYakushokuCd;
        private System.Windows.Forms.Label lblBukaNm;
        private common.controls.FsiTextBox txtBukaCd;
        private System.Windows.Forms.Label lblBukaCd;
        private System.Windows.Forms.Label lblBumonNm;
        private common.controls.FsiTextBox txtBumonCd;
        private System.Windows.Forms.Label lblBumonCd;
        private common.controls.FsiTextBox txtDenwaBango;
        private common.controls.FsiTextBox txtJusho2;
        private common.controls.FsiTextBox txtJusho1;
        private System.Windows.Forms.Label lblYubinBango1;
        private System.Windows.Forms.Label lblDenwaBango;
        private System.Windows.Forms.Label lblJusho2;
        private System.Windows.Forms.Label lblJusho1;
        private System.Windows.Forms.Label lblSeibetsuNm;
        private jp.co.fsi.common.FsiPanel pnlSeinengappi;
        private System.Windows.Forms.Label lblDaySeinengappi;
        private common.controls.FsiTextBox txtDaySeinengappi;
        private System.Windows.Forms.Label lblMonthSeinengappi;
        private System.Windows.Forms.Label lblGengoYearSeinengappi;
        private common.controls.FsiTextBox txtMonthSeinengappi;
        private System.Windows.Forms.Label lblGengoSeinengappi;
        private common.controls.FsiTextBox txtGengoYearSeinengappi;
        private common.controls.FsiTextBox txtYubinBango2;
        private System.Windows.Forms.Label lblYubinBango;
        private common.controls.FsiTextBox txtSeibetsu;
        private System.Windows.Forms.Label lblSeinengappi;
        private common.controls.FsiTextBox txtYubinBango1;
        private System.Windows.Forms.Label lblSeibetsu;
        private common.controls.FsiTextBox txtShainKanaNm;
        private System.Windows.Forms.Label lblShainKanaNm;
        private common.controls.FsiTextBox txtShainNm;
        private System.Windows.Forms.Label lblShainNm;
        private jp.co.fsi.common.FsiPanel panel3;
        private System.Windows.Forms.Label lblJuminzei;
        private System.Windows.Forms.Label KaigoHokenKubunNm;
        private common.controls.FsiTextBox txtKaigoHokenKubun;
        private System.Windows.Forms.Label lblKaigoHokenKubun;
        private System.Windows.Forms.Label lblKenkoHokenShoyoKeisanKubunNm;
        private common.controls.FsiTextBox txtKenkoHokenShoyoKeisanKubun;
        private System.Windows.Forms.Label lblKenkoHokenShoyoKeisanKubun;
        private System.Windows.Forms.Label lblKoseiNenkin;
        private System.Windows.Forms.Label lblKoyohoken;
        private System.Windows.Forms.Label lblKenkohoken;
        private System.Windows.Forms.Label lblJuminzei6gatsubun;
        private System.Windows.Forms.Label lblJuminzeiShichosonCd;
        private System.Windows.Forms.Label lblKoyoHokenKeisanKubun;
        private common.controls.FsiTextBox txtKoseiNenkinShoyoKeisanKbn;
        private common.controls.FsiTextBox txtKoseiNenkinHyojunHoshuGtgk;
        private common.controls.FsiTextBox txtKoseiNenkinBango;
        private System.Windows.Forms.Label lblKoseiNenkinHokenryo;
        private System.Windows.Forms.Label lblKoseiNenkinHyojunHoshuGtgk;
        private System.Windows.Forms.Label lblKoseiNenkinBango;
        private common.controls.FsiTextBox txtKenkoHokenryo;
        private System.Windows.Forms.Label lblKenkoHokenryo;
        private common.controls.FsiTextBox txtKenkoHokenHyojunHoshuGtgk;
        private System.Windows.Forms.Label lblKenkoHokenHyojunHoshuGtgk;
        private common.controls.FsiTextBox txtKenkoHokenshoBango;
        private System.Windows.Forms.Label lblKenkoHokenshoBango;
        private jp.co.fsi.common.FsiPanel panel7;
        private System.Windows.Forms.Label lblFuyoRojinDokyoRoshintoNm;
        private common.controls.FsiTextBox txtFuyoRojinDokyoRoshinto;
        private System.Windows.Forms.Label lblFuyoRojinDokyoRoshinto;
        private System.Windows.Forms.Label lblFuyoRojinDokyoRoshintoIgaiNm;
        private common.controls.FsiTextBox txtFuyoRojinDokyoRoshintoIgai;
        private System.Windows.Forms.Label lblFuyoRojinDokyoRoshintoIgai;
        private System.Windows.Forms.Label lblFuyoTokuteiNm;
        private common.controls.FsiTextBox txtFuyoTokutei;
        private System.Windows.Forms.Label lblFuyoTokutei;
        private System.Windows.Forms.Label lblFuyoIppanNm;
        private common.controls.FsiTextBox txtFuyoIppan;
        private System.Windows.Forms.Label lblFuyoIppan;
        private System.Windows.Forms.Label lblHonninKubun1Nm;
        private common.controls.FsiTextBox txtHonninKubun1;
        private System.Windows.Forms.Label lblHonninKubun1;
        private System.Windows.Forms.Label lblNenmatsuChosei;
        private common.controls.FsiTextBox txtZeigakuHyo;
        private System.Windows.Forms.Label lblZeigakuHyo;
        private jp.co.fsi.common.FsiPanel panel11;
        private System.Windows.Forms.Label lblShikyuGokei;
        private System.Windows.Forms.Label lblShikyuFoot;
        private jp.co.fsi.common.FsiPanel panel15;
        private System.Windows.Forms.Label lblKyuyoGinkoCd2;
        private common.controls.FsiTextBox txtKyuyoShikyuKingaku2;
        private System.Windows.Forms.Label lblKyuyoShikyuKingaku2;
        private common.controls.FsiTextBox txtKyuyoShikyuRitsu2;
        private System.Windows.Forms.Label lblKyuyoShikyuRitsu2;
        private System.Windows.Forms.Label lblKyuyoShikyuHoho2Nm;
        private common.controls.FsiTextBox txtKyuyoShikyuHoho2;
        private System.Windows.Forms.Label lblKyuyoShikyuHoho2;
        private common.controls.FsiTextBox txtKyuyoKozaBango2;
        private common.controls.FsiTextBox txtKyuyoKozaMeigininKana2;
        private common.controls.FsiTextBox txtKyuyoKozaMeigininKanji2;
        private common.controls.FsiTextBox txtKyuyoKeiyakushaBango2;
        private System.Windows.Forms.Label lblKoseiNenkinShoyoKeisanKbnNm;
        private System.Windows.Forms.Label lblKoyoHokenKeisanKubunNm;
        private common.controls.FsiTextBox txtKoyoHokenKeisanKubun;
        private System.Windows.Forms.Label lblKoseiNenkinHyojunHoshuGtgkNm;
        private System.Windows.Forms.Label lblKenkoHokenHyojunHoshuGtgkNm;
        private common.controls.FsiTextBox txtJuminzei7gatsubunIko;
        private System.Windows.Forms.Label lblJuminzei7gatsubunIko;
        private common.controls.FsiTextBox txtJuminzei6gatsubun;
        private System.Windows.Forms.Label lblJuminzeiShichosonNm;
        private common.controls.FsiTextBox txtJuminzeiShichosonCd;
        private System.Windows.Forms.Label lblShogaishaTokubetsuNm;
        private common.controls.FsiTextBox txtShogaishaTokubetsu;
        private System.Windows.Forms.Label lblShogaishaTokubetsu;
        private System.Windows.Forms.Label lblShogaisha;
        private System.Windows.Forms.Label lblFuyo;
        private common.controls.FsiTextBox txtKyuyoShubetsu;
        private common.controls.FsiTextBox txtTekiyo2;
        private common.controls.FsiTextBox txtTekiyo1;
        private System.Windows.Forms.Label lblKyuyoShubetsu;
        private System.Windows.Forms.Label lblTekiyo2;
        private System.Windows.Forms.Label lblTekiyo1;
        private System.Windows.Forms.Label Sai16MimanNm;
        private common.controls.FsiTextBox txtSai16Miman;
        private System.Windows.Forms.Label lblSai16Miman;
        private System.Windows.Forms.Label lblHaigushaKubunNm;
        private common.controls.FsiTextBox txtHaigushaKubun;
        private System.Windows.Forms.Label lblHaigushaKubun;
        private System.Windows.Forms.Label lblGaikokujinNm;
        private common.controls.FsiTextBox txtGaikokujin;
        private System.Windows.Forms.Label lblGaikokujin;
        private System.Windows.Forms.Label lblSaigaishaNm;
        private common.controls.FsiTextBox txtSaigaisha;
        private System.Windows.Forms.Label lblSaigaisha;
        private System.Windows.Forms.Label lblShiboTaishokuNm;
        private common.controls.FsiTextBox txtShiboTaishoku;
        private System.Windows.Forms.Label lblShiboTaishoku;
        private System.Windows.Forms.Label lblMiseinenshaNm;
        private common.controls.FsiTextBox txtMiseinensha;
        private System.Windows.Forms.Label lblMiseinensha;
        private System.Windows.Forms.Label lblHonninKubun3Nm;
        private common.controls.FsiTextBox txtHonninKubun3;
        private System.Windows.Forms.Label lblHonninKubun3;
        private System.Windows.Forms.Label lblHonninKubun2Nm;
        private common.controls.FsiTextBox txtHonninKubun2;
        private System.Windows.Forms.Label lblHonninKubun2;
        private System.Windows.Forms.Label ShogaishaIppanNm;
        private common.controls.FsiTextBox txtShogaishaIppan;
        private System.Windows.Forms.Label lblShogaishaIppan;
        private System.Windows.Forms.Label lblFuyoDokyoTokushoIppanNm;
        private common.controls.FsiTextBox txtFuyoDokyoTokushoIppan;
        private System.Windows.Forms.Label lblFuyoDokyoTokushoIppan;
        private System.Windows.Forms.Label lblFuyoRojin;
        private System.Windows.Forms.Label lblNenmatsuChoseiNm;
        private System.Windows.Forms.Label lblZeigakuHyoNm;
        private common.controls.FsiTextBox txtNenmatsuChosei;
        private System.Windows.Forms.Label lblGensenchoshuhyo;
        private System.Windows.Forms.Label lblKojo;
        private System.Windows.Forms.Label lblShikyu;
        private System.Windows.Forms.Label lblKojoGokei;
        private System.Windows.Forms.Label lblKojoGokeiFoot;
        private System.Windows.Forms.DataGridView dgvShikyu;
        private System.Windows.Forms.DataGridView dgvKojo;
        private System.Windows.Forms.Label lblKyuyoYokinShumoku1Nm;
        private common.controls.FsiTextBox txtKyuyoYokinShumoku1;
        private System.Windows.Forms.Label lblKyuyoShitenNm1;
        private common.controls.FsiTextBox txtKyuyoShitenCd1;
        private System.Windows.Forms.Label lblKyuyoGinkoNm1;
        private common.controls.FsiTextBox txtKyuyoGinkoCd1;
        private System.Windows.Forms.Label lblKyuyoKeiyakushaBango1;
        private System.Windows.Forms.Label lblKyuyoKozaMeigininKana1;
        private System.Windows.Forms.Label lblKyuyoKozaMeigininKanji1;
        private System.Windows.Forms.Label lblKyuyoKozaBango1;
        private System.Windows.Forms.Label lblKyuyoYokinShumoku1;
        private System.Windows.Forms.Label lblKyuyoShitenCd1;
        private System.Windows.Forms.Label lblKyuyoGinkoCd1;
        private common.controls.FsiTextBox txtKyuyoShikyuKingaku1;
        private System.Windows.Forms.Label lblKyuyoShikyuKingaku1;
        private common.controls.FsiTextBox txtKyuyoShikyuRitsu1;
        private System.Windows.Forms.Label lblKyuyoShikyuRitsu1;
        private System.Windows.Forms.Label lblKyuyoShikyuHoho1Nm;
        private common.controls.FsiTextBox txtKyuyoShikyuHoho1;
        private System.Windows.Forms.Label lblKyuyoShikyuHoho1;
        private common.controls.FsiTextBox txtKyuyoKozaBango1;
        private common.controls.FsiTextBox txtKyuyoKozaMeigininKana1;
        private common.controls.FsiTextBox txtKyuyoKozaMeigininKanji1;
        private common.controls.FsiTextBox txtKyuyoKeiyakushaBango1;
        private System.Windows.Forms.Label lblKyuyoYokinShumoku2Nm;
        private common.controls.FsiTextBox txtKyuyoYokinShumoku2;
        private System.Windows.Forms.Label lblKyuyoShitenNm2;
        private common.controls.FsiTextBox txtKyuyoShitenCd2;
        private System.Windows.Forms.Label lblKyuyoGinkoNm2;
        private common.controls.FsiTextBox txtKyuyoGinkoCd2;
        private System.Windows.Forms.Label lblKyuyoKeiyakushaBango2;
        private System.Windows.Forms.Label lblKyuyoKozaMeigininKana2;
        private System.Windows.Forms.Label lblKyuyoKozaMeigininKanji2;
        private System.Windows.Forms.Label lblKyuyoKozaBango2;
        private System.Windows.Forms.Label lblKyuyoYokinShumoku2;
        private System.Windows.Forms.Label lblKyuyoShitenCd2;
        private System.Windows.Forms.Label lblShikyuHoho2;
        private System.Windows.Forms.Label lblShikyuHoho1;
        private common.controls.FsiTextBox txtKoseiNenkinHokenryo;
        private System.Windows.Forms.Label lblKoseiNenkinShoyoKeisanKbn;
        private System.Windows.Forms.Label lblKojoFootD1;
        private System.Windows.Forms.Label lblShikyuFootD1;
        private common.controls.FsiTextBox txtMyNumber;
        private System.Windows.Forms.Label label1;
        private common.controls.FsiTextBox FsiTextBox1;
        private System.Windows.Forms.Button button1;
        private common.controls.FsiTextBox txthyoji;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.TabPage tabPage7;
        private jp.co.fsi.common.FsiPanel panel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label35;
        private common.controls.FsiTextBox txtFuyoNm4;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private common.controls.FsiTextBox txtFuyoJukyo4;
        private System.Windows.Forms.Button button6;
        private common.controls.FsiTextBox txtFuyoMyNumberDm4;
        private common.controls.FsiTextBox txtFuyoMyNumber4;
        private System.Windows.Forms.Label label38;
        private jp.co.fsi.common.FsiPanel panel6;
        private System.Windows.Forms.Label label39;
        private common.controls.FsiTextBox txtFuyoDaySeinengappi4;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private common.controls.FsiTextBox txtFuyoMonthSeinengappi4;
        private System.Windows.Forms.Label lblFuyoGengoSeinengappi4;
        private common.controls.FsiTextBox txtFuyoGengoYearSeinengappi4;
        private System.Windows.Forms.Label label43;
        private common.controls.FsiTextBox txtFuyoKanaNm4;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label25;
        private common.controls.FsiTextBox txtFuyoNm3;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private common.controls.FsiTextBox txtFuyoJukyo3;
        private System.Windows.Forms.Button button5;
        private common.controls.FsiTextBox txtFuyoMyNumberDm3;
        private common.controls.FsiTextBox txtFuyoMyNumber3;
        private System.Windows.Forms.Label label28;
        private jp.co.fsi.common.FsiPanel panel5;
        private System.Windows.Forms.Label label29;
        private common.controls.FsiTextBox txtFuyoDaySeinengappi3;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private common.controls.FsiTextBox txtFuyoMonthSeinengappi3;
        private System.Windows.Forms.Label lblFuyoGengoSeinengappi3;
        private common.controls.FsiTextBox txtFuyoGengoYearSeinengappi3;
        private System.Windows.Forms.Label label33;
        private common.controls.FsiTextBox txtFuyoKanaNm3;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label15;
        private common.controls.FsiTextBox txtFuyoNm2;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private common.controls.FsiTextBox txtFuyoJukyo2;
        private System.Windows.Forms.Button button4;
        private common.controls.FsiTextBox txtFuyoMyNumberDm2;
        private common.controls.FsiTextBox txtFuyoMyNumber2;
        private System.Windows.Forms.Label label18;
        private jp.co.fsi.common.FsiPanel panel4;
        private System.Windows.Forms.Label label19;
        private common.controls.FsiTextBox txtFuyoDaySeinengappi2;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private common.controls.FsiTextBox txtFuyoMonthSeinengappi2;
        private System.Windows.Forms.Label lblFuyoGengoSeinengappi2;
        private common.controls.FsiTextBox txtFuyoGengoYearSeinengappi2;
        private System.Windows.Forms.Label label23;
        private common.controls.FsiTextBox txtFuyoKanaNm2;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label2;
        private common.controls.FsiTextBox txtFuyoNm1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private common.controls.FsiTextBox txtFuyoJukyo1;
        private System.Windows.Forms.Button button3;
        private common.controls.FsiTextBox txtFuyoMyNumberDm1;
        private common.controls.FsiTextBox txtFuyoMyNumber1;
        private System.Windows.Forms.Label label8;
        private jp.co.fsi.common.FsiPanel panel2;
        private System.Windows.Forms.Label label9;
        private common.controls.FsiTextBox txtFuyoDaySeinengappi1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private common.controls.FsiTextBox txtFuyoMonthSeinengappi1;
        private System.Windows.Forms.Label lblFuyoGengoSeinengappi1;
        private common.controls.FsiTextBox txtFuyoGengoYearSeinengappi1;
        private System.Windows.Forms.Label label13;
        private common.controls.FsiTextBox txtFuyoKanaNm1;
        private System.Windows.Forms.Label label14;
        private jp.co.fsi.common.FsiPanel panel8;
        private jp.co.fsi.common.FsiPanel panel9;
        private System.Windows.Forms.Label label45;
        private common.controls.FsiTextBox txtFuyoMimanNm4;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private common.controls.FsiTextBox txtFuyoMimanJukyo4;
        private System.Windows.Forms.Button button10;
        private common.controls.FsiTextBox txtFuyoMimanMyNumberDm4;
        private common.controls.FsiTextBox txtFuyoMimanMyNumber4;
        private System.Windows.Forms.Label label48;
        private jp.co.fsi.common.FsiPanel panel10;
        private System.Windows.Forms.Label label49;
        private common.controls.FsiTextBox txtFuyoMimanDaySeinengappi4;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private common.controls.FsiTextBox txtFuyoMimanMonthSeinengappi4;
        private System.Windows.Forms.Label lblFuyoMimanGengoSeinengappi4;
        private common.controls.FsiTextBox txtFuyoMimanGengoYearSeinengappi4;
        private System.Windows.Forms.Label label53;
        private common.controls.FsiTextBox txtFuyoMimanKanaNm4;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label56;
        private common.controls.FsiTextBox txtFuyoMimanNm3;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label58;
        private common.controls.FsiTextBox txtFuyoMimanJukyo3;
        private System.Windows.Forms.Button button9;
        private common.controls.FsiTextBox txtFuyoMimanMyNumberDm3;
        private common.controls.FsiTextBox txtFuyoMimanMyNumber3;
        private System.Windows.Forms.Label label59;
        private jp.co.fsi.common.FsiPanel panel12;
        private System.Windows.Forms.Label label60;
        private common.controls.FsiTextBox txtFuyoMimanDaySeinengappi3;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label62;
        private common.controls.FsiTextBox txtFuyoMimanMonthSeinengappi3;
        private System.Windows.Forms.Label lblFuyoMimanGengoSeinengappi3;
        private common.controls.FsiTextBox txtFuyoMimanGengoYearSeinengappi3;
        private System.Windows.Forms.Label label64;
        private common.controls.FsiTextBox txtFuyoMimanKanaNm3;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label66;
        private common.controls.FsiTextBox txtFuyoMimanNm2;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label68;
        private common.controls.FsiTextBox txtFuyoMimanJukyo2;
        private System.Windows.Forms.Button button8;
        private common.controls.FsiTextBox txtFuyoMimanMyNumberDm2;
        private common.controls.FsiTextBox txtFuyoMimanMyNumber2;
        private System.Windows.Forms.Label label70;
        private jp.co.fsi.common.FsiPanel panel13;
        private System.Windows.Forms.Label label71;
        private common.controls.FsiTextBox txtFuyoMimanDaySeinengappi2;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label73;
        private common.controls.FsiTextBox txtFuyoMimanMonthSeinengappi2;
        private System.Windows.Forms.Label lblFuyoMimanGengoSeinengappi2;
        private common.controls.FsiTextBox txtFuyoMimanGengoYearSeinengappi2;
        private System.Windows.Forms.Label label75;
        private common.controls.FsiTextBox txtFuyoMimanKanaNm2;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label label78;
        private common.controls.FsiTextBox txtFuyoMimanNm1;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label80;
        private common.controls.FsiTextBox txtFuyoMimanJukyo1;
        private System.Windows.Forms.Button button7;
        private common.controls.FsiTextBox txtFuyoMimanMyNumberDm1;
        private common.controls.FsiTextBox txtFuyoMimanMyNumber1;
        private System.Windows.Forms.Label label81;
        private jp.co.fsi.common.FsiPanel panel16;
        private System.Windows.Forms.Label label82;
        private common.controls.FsiTextBox txtFuyoMimanDaySeinengappi1;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Label label84;
        private common.controls.FsiTextBox txtFuyoMimanMonthSeinengappi1;
        private System.Windows.Forms.Label lblFuyoMimanGengoSeinengappi1;
        private common.controls.FsiTextBox txtFuyoMimanGengoYearSeinengappi1;
        private System.Windows.Forms.Label label86;
        private common.controls.FsiTextBox txtFuyoMimanKanaNm1;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.Label lblFuyoHaigushaJukyo;
        private common.controls.FsiTextBox txtFuyoHaigushaNm;
        private System.Windows.Forms.Label lblFuyoHaigushaNm;
        private System.Windows.Forms.Label label77;
        private common.controls.FsiTextBox txtFuyoHaigushaJukyo;
        private System.Windows.Forms.Button button2;
        private common.controls.FsiTextBox txtFuyoHaigushaMyNumberDm;
        private common.controls.FsiTextBox txtFuyoHaigushaMyNumber;
        private System.Windows.Forms.Label label102;
        private jp.co.fsi.common.FsiPanel pnlFuyoHaigushaSeinengappi;
        private System.Windows.Forms.Label lblFuyoHaigushaDaySeinengappi;
        private common.controls.FsiTextBox txtFuyoHaigushaDaySeinengappi;
        private System.Windows.Forms.Label lblFuyoHaigushaMonthSeinengappi;
        private System.Windows.Forms.Label lblFuyoHaigushaGengoYearSeinengappi;
        private common.controls.FsiTextBox txtFuyoHaigushaMonthSeinengappi;
        private System.Windows.Forms.Label lblFuyoHaigushaGengoSeinengappi;
        private common.controls.FsiTextBox txtFuyoHaigushaGengoYearSeinengappi;
        private System.Windows.Forms.Label lblFuyoHaigushaSeinengappi;
        private common.controls.FsiTextBox txtFuyoHaigushaKanaNm;
        private System.Windows.Forms.Label lblFuyoHaigushaKanaNm;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label74;
        private common.controls.FsiTextBox txtIkouNo;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.Label label91;
        private common.controls.FsiTextBox txtIkouMimanNo;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Label label92;
        private common.controls.FsiTextBox txtZenBikou;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.Label label93;
        private common.controls.FsiTextBox txtHiJukyoShinzoku;
        private System.Windows.Forms.Label label94;
    }
}