﻿namespace jp.co.fsi.ky.kycm1021
{
    partial class KYCM1021
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblKanaName = new System.Windows.Forms.Label();
            this.txtKanaName = new jp.co.fsi.common.controls.FsiTextBox();
            this.dgvList = new System.Windows.Forms.DataGridView();
            this.txtShainCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShainCd = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rdoAll = new System.Windows.Forms.RadioButton();
            this.rdoTaishoku = new System.Windows.Forms.RadioButton();
            this.rdoZaishoku = new System.Windows.Forms.RadioButton();
            this.pnlDebug.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(768, 23);
            this.lblTitle.Text = "社員の登録";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Size = new System.Drawing.Size(801, 100);
            // 
            // lblKanaName
            // 
            this.lblKanaName.BackColor = System.Drawing.Color.Silver;
            this.lblKanaName.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKanaName.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblKanaName.Location = new System.Drawing.Point(164, 49);
            this.lblKanaName.Name = "lblKanaName";
            this.lblKanaName.Size = new System.Drawing.Size(85, 23);
            this.lblKanaName.TabIndex = 2;
            this.lblKanaName.Text = "カ　ナ　名";
            this.lblKanaName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKanaName
            // 
            this.txtKanaName.AutoSizeFromLength = false;
            this.txtKanaName.DisplayLength = null;
            this.txtKanaName.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKanaName.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtKanaName.Location = new System.Drawing.Point(249, 49);
            this.txtKanaName.MaxLength = 30;
            this.txtKanaName.Name = "txtKanaName";
            this.txtKanaName.Size = new System.Drawing.Size(180, 23);
            this.txtKanaName.TabIndex = 3;
            this.txtKanaName.Validating += new System.ComponentModel.CancelEventHandler(this.txtKanaName_Validating);
            // 
            // dgvList
            // 
            this.dgvList.AllowUserToAddRows = false;
            this.dgvList.AllowUserToDeleteRows = false;
            this.dgvList.AllowUserToResizeColumns = false;
            this.dgvList.AllowUserToResizeRows = false;
            this.dgvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvList.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.dgvList.Location = new System.Drawing.Point(17, 153);
            this.dgvList.MultiSelect = false;
            this.dgvList.Name = "dgvList";
            this.dgvList.ReadOnly = true;
            this.dgvList.RowHeadersVisible = false;
            this.dgvList.RowTemplate.Height = 21;
            this.dgvList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvList.Size = new System.Drawing.Size(530, 296);
            this.dgvList.TabIndex = 0;
            this.dgvList.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvList_CellDoubleClick);
            this.dgvList.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvList_KeyDown);
            // 
            // txtShainCd
            // 
            this.txtShainCd.AutoSizeFromLength = false;
            this.txtShainCd.DisplayLength = null;
            this.txtShainCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShainCd.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtShainCd.Location = new System.Drawing.Point(102, 49);
            this.txtShainCd.MaxLength = 6;
            this.txtShainCd.Name = "txtShainCd";
            this.txtShainCd.Size = new System.Drawing.Size(53, 23);
            this.txtShainCd.TabIndex = 1;
            this.txtShainCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtShainCd_Validating);
            // 
            // lblShainCd
            // 
            this.lblShainCd.BackColor = System.Drawing.Color.Silver;
            this.lblShainCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShainCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblShainCd.Location = new System.Drawing.Point(17, 49);
            this.lblShainCd.Name = "lblShainCd";
            this.lblShainCd.Size = new System.Drawing.Size(85, 23);
            this.lblShainCd.TabIndex = 0;
            this.lblShainCd.Text = "社員コード";
            this.lblShainCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rdoAll);
            this.groupBox1.Controls.Add(this.rdoTaishoku);
            this.groupBox1.Controls.Add(this.rdoZaishoku);
            this.groupBox1.Location = new System.Drawing.Point(435, 42);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(112, 98);
            this.groupBox1.TabIndex = 904;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "表示区分";
            // 
            // rdoAll
            // 
            this.rdoAll.AutoSize = true;
            this.rdoAll.Location = new System.Drawing.Point(27, 67);
            this.rdoAll.Name = "rdoAll";
            this.rdoAll.Size = new System.Drawing.Size(44, 16);
            this.rdoAll.TabIndex = 2;
            this.rdoAll.Text = "全て";
            this.rdoAll.UseVisualStyleBackColor = true;
            this.rdoAll.Click += new System.EventHandler(this.ckboxAllHyoji_CheckedChanged);
            // 
            // rdoTaishoku
            // 
            this.rdoTaishoku.AutoSize = true;
            this.rdoTaishoku.Location = new System.Drawing.Point(27, 45);
            this.rdoTaishoku.Name = "rdoTaishoku";
            this.rdoTaishoku.Size = new System.Drawing.Size(59, 16);
            this.rdoTaishoku.TabIndex = 1;
            this.rdoTaishoku.Text = "退職者";
            this.rdoTaishoku.UseVisualStyleBackColor = true;
            this.rdoTaishoku.Click += new System.EventHandler(this.ckboxAllHyoji_CheckedChanged);
            // 
            // rdoZaishoku
            // 
            this.rdoZaishoku.AutoSize = true;
            this.rdoZaishoku.Checked = true;
            this.rdoZaishoku.Location = new System.Drawing.Point(27, 23);
            this.rdoZaishoku.Name = "rdoZaishoku";
            this.rdoZaishoku.Size = new System.Drawing.Size(59, 16);
            this.rdoZaishoku.TabIndex = 0;
            this.rdoZaishoku.TabStop = true;
            this.rdoZaishoku.Text = "在職者";
            this.rdoZaishoku.UseVisualStyleBackColor = true;
            this.rdoZaishoku.Click += new System.EventHandler(this.ckboxAllHyoji_CheckedChanged);
            // 
            // KYCM1021
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(944, 638);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.txtShainCd);
            this.Controls.Add(this.lblShainCd);
            this.Controls.Add(this.dgvList);
            this.Controls.Add(this.txtKanaName);
            this.Controls.Add(this.lblKanaName);
            this.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.Name = "KYCM1021";
            this.Par1 = "";
            this.Text = "社員の登録";
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.lblKanaName, 0);
            this.Controls.SetChildIndex(this.txtKanaName, 0);
            this.Controls.SetChildIndex(this.dgvList, 0);
            this.Controls.SetChildIndex(this.lblShainCd, 0);
            this.Controls.SetChildIndex(this.txtShainCd, 0);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.pnlDebug.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblKanaName;
        private jp.co.fsi.common.controls.FsiTextBox txtKanaName;
        private System.Windows.Forms.DataGridView dgvList;
        private common.controls.FsiTextBox txtShainCd;
        private System.Windows.Forms.Label lblShainCd;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rdoZaishoku;
        private System.Windows.Forms.RadioButton rdoAll;
        private System.Windows.Forms.RadioButton rdoTaishoku;
    }
}