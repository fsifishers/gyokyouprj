﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;
using jp.co.fsi.common.controls;
using System.Reflection;
using System.Security.Cryptography;
using jp.co.fsi.common.mynumber;

namespace jp.co.fsi.ky.kycm1021
{
    /// <summary>
    /// 社員の登録(KYCM1022)
    /// </summary>
    public partial class KYCM1022 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(新規)
        /// </summary>
        private const string MODE_NEW = "1";

        /// <summary>
        /// モード(編集)
        /// </summary>
        private const string MODE_EDIT = "2";

        //パスワードに使用する文字
        private static readonly string passwordChars = "0123456789abcdefghijklmnopqrstuvwxyz";
        #endregion

        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理用変数
        /// </summary>
        private bool _numberFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._numberFlg;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KYCM1022()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="par1">引数1</param>
        public KYCM1022(string par1)
            : base(par1)
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // タイトルは非表示
            this.lblTitle.Visible = false;

            // 諸手当グリッド初期化
            InitTeateGridView();

            // 引数：Par1／モード(1:新規、2:変更)、InData：社員コード
            if (MODE_NEW.Equals(this.Par1))
            {
                // 新規モードの初期表示
                InitDispOnNew();
                this.btnF3.Enabled = false;
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 編集モードの初期表示
                InitDispOnEdit();
            }
            else
            {
                // 不正な起動として閉じる
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }
            this.ShowFButton = true;

            // ボタンの表示非表示を設定
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF1.Location = this.btnF2.Location;
            this.btnF6.Location = this.btnF4.Location;
            this.btnF1.Visible = true;
            this.btnF2.Visible = false;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 元号年とコード項目に
            // フォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtGengoYearSeinengappi":
                case "txtGengoYearNyushaNengappi":
                case "txtGengoYearTaishokuNengappi":
                case "txtFuyoHaigushaGengoYearSeinengappi":
                case "txtFuyoGengoYearSeinengappi1":
                case "txtFuyoGengoYearSeinengappi2":
                case "txtFuyoGengoYearSeinengappi3":
                case "txtFuyoGengoYearSeinengappi4":
                case "txtFuyoMimanGengoYearSeinengappi1":
                case "txtFuyoMimanGengoYearSeinengappi2":
                case "txtFuyoMimanGengoYearSeinengappi3":
                case "txtFuyoMimanGengoYearSeinengappi4":
                case "txtBumonCd":
                case "txtBukaCd":
                case "txtYakushokuCd":
                case "txtKyuyoKeitai":
                case "txtJuminzeiShichosonCd":
                case "txtHonninKubun1":
                case "txtHonninKubun2":
                case "txtHonninKubun3":
                case "txtHaigushaKubun":
                case "txtKyuyoGinkoCd1":
                case "txtKyuyoGinkoCd2":
                case "txtKyuyoShitenCd1":
                case "txtKyuyoShitenCd2":
                    this.btnF1.Enabled = true;
                    break;
                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        public override void PressF1()
        {
            Assembly asm;
            Type t;

            // 元号年とコード項目に
            // フォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtGengoYearSeinengappi":
                case "txtGengoYearNyushaNengappi":
                case "txtGengoYearTaishokuNengappi":
                case "txtFuyoHaigushaGengoYearSeinengappi":
                case "txtFuyoGengoYearSeinengappi1":
                case "txtFuyoGengoYearSeinengappi2":
                case "txtFuyoGengoYearSeinengappi3":
                case "txtFuyoGengoYearSeinengappi4":
                case "txtFuyoMimanGengoYearSeinengappi1":
                case "txtFuyoMimanGengoYearSeinengappi2":
                case "txtFuyoMimanGengoYearSeinengappi3":
                case "txtFuyoMimanGengoYearSeinengappi4":
                    #region 元号検索
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            #region 扶養情報以外
                            if (this.ActiveCtlNm == "txtGengoYearSeinengappi")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.InData = this.lblGengoSeinengappi.Text;
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.lblGengoSeinengappi.Text = result[1];

                                    // 和暦年未入力時の初期値
                                    if (ValChk.IsEmpty(this.txtGengoYearSeinengappi.Text))
                                    {
                                        string[] date = Util.ConvJpDate(DateTime.Today, this.Dba);
                                        this.txtGengoYearSeinengappi.Text = date[2];
                                    }

                                    FixDateControls(
                                        this.lblGengoSeinengappi,
                                        this.txtGengoYearSeinengappi,
                                        this.txtMonthSeinengappi,
                                        this.txtDaySeinengappi);
                                }
                            }
                            else if (this.ActiveCtlNm == "txtGengoYearNyushaNengappi")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.InData = this.lblGengoNyushaNengappi.Text;
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.lblGengoNyushaNengappi.Text = result[1];

                                    // 和暦年未入力時の初期値
                                    if (ValChk.IsEmpty(this.txtGengoYearNyushaNengappi.Text))
                                    {
                                        string[] date = Util.ConvJpDate(DateTime.Today, this.Dba);
                                        this.txtGengoYearNyushaNengappi.Text = date[2];
                                    }

                                    FixDateControls(
                                        this.lblGengoNyushaNengappi,
                                        this.txtGengoYearNyushaNengappi,
                                        this.txtMonthNyushaNengappi,
                                        this.txtDayNyushaNengappi);
                                }
                            }
                            else if (this.ActiveCtlNm == "txtGengoYearTaishokuNengappi")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.InData = this.lblGengoTaishokuNengappi.Text;
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.lblGengoTaishokuNengappi.Text = result[1];

                                    // 和暦年未入力時の初期値
                                    if (ValChk.IsEmpty(this.txtGengoYearTaishokuNengappi.Text))
                                    {
                                        string[] date = Util.ConvJpDate(DateTime.Today, this.Dba);
                                        this.txtGengoYearTaishokuNengappi.Text = date[2];
                                    }

                                    FixDateControls(
                                        this.lblGengoTaishokuNengappi,
                                        this.txtGengoYearTaishokuNengappi,
                                        this.txtMonthTaishokuNengappi,
                                        this.txtDayTaishokuNengappi);
                                }
                            }
                            #endregion

                            #region 配偶者
                            else if (this.ActiveCtlNm == "txtFuyoHaigushaGengoYearSeinengappi")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.InData = this.lblFuyoHaigushaGengoSeinengappi.Text;
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.lblFuyoHaigushaGengoSeinengappi.Text = result[1];

                                    // 和暦年未入力時の初期値
                                    if (ValChk.IsEmpty(this.txtFuyoHaigushaGengoYearSeinengappi.Text))
                                    {
                                        string[] date = Util.ConvJpDate(DateTime.Today, this.Dba);
                                        this.txtFuyoHaigushaGengoYearSeinengappi.Text = date[2];
                                    }

                                    FixDateControls(
                                        this.lblFuyoHaigushaGengoSeinengappi,
                                        this.txtFuyoHaigushaGengoYearSeinengappi,
                                        this.txtFuyoHaigushaMonthSeinengappi,
                                        this.txtFuyoHaigushaDaySeinengappi);
                                }
                            }
                            #endregion

                            #region 控除対象扶養親族
                            else if (this.ActiveCtlNm == "txtFuyoGengoYearSeinengappi1")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.InData = this.lblFuyoGengoSeinengappi1.Text;
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.lblFuyoGengoSeinengappi1.Text = result[1];

                                    // 和暦年未入力時の初期値
                                    if (ValChk.IsEmpty(this.txtFuyoGengoYearSeinengappi1.Text))
                                    {
                                        string[] date = Util.ConvJpDate(DateTime.Today, this.Dba);
                                        this.txtFuyoGengoYearSeinengappi1.Text = date[2];
                                    }

                                    FixDateControls(
                                        this.lblFuyoGengoSeinengappi1,
                                        this.txtFuyoGengoYearSeinengappi1,
                                        this.txtFuyoMonthSeinengappi1,
                                        this.txtFuyoDaySeinengappi1);
                                }
                            }
                            else if (this.ActiveCtlNm == "txtFuyoGengoYearSeinengappi2")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.InData = this.lblFuyoGengoSeinengappi2.Text;
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.lblFuyoGengoSeinengappi2.Text = result[1];

                                    // 和暦年未入力時の初期値
                                    if (ValChk.IsEmpty(this.txtFuyoGengoYearSeinengappi2.Text))
                                    {
                                        string[] date = Util.ConvJpDate(DateTime.Today, this.Dba);
                                        this.txtFuyoGengoYearSeinengappi2.Text = date[2];
                                    }

                                    FixDateControls(
                                        this.lblFuyoGengoSeinengappi2,
                                        this.txtFuyoGengoYearSeinengappi2,
                                        this.txtFuyoMonthSeinengappi2,
                                        this.txtFuyoDaySeinengappi2);
                                }
                            }
                            else if (this.ActiveCtlNm == "txtFuyoGengoYearSeinengappi3")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.InData = this.lblFuyoGengoSeinengappi3.Text;
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.lblFuyoGengoSeinengappi3.Text = result[1];

                                    // 和暦年未入力時の初期値
                                    if (ValChk.IsEmpty(this.txtFuyoGengoYearSeinengappi3.Text))
                                    {
                                        string[] date = Util.ConvJpDate(DateTime.Today, this.Dba);
                                        this.txtFuyoGengoYearSeinengappi3.Text = date[2];
                                    }

                                    FixDateControls(
                                        this.lblFuyoGengoSeinengappi3,
                                        this.txtFuyoGengoYearSeinengappi3,
                                        this.txtFuyoMonthSeinengappi3,
                                        this.txtFuyoDaySeinengappi3);
                                }
                            }
                            else if (this.ActiveCtlNm == "txtFuyoGengoYearSeinengappi4")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.InData = this.lblFuyoGengoSeinengappi4.Text;
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.lblFuyoGengoSeinengappi4.Text = result[1];

                                    // 和暦年未入力時の初期値
                                    if (ValChk.IsEmpty(this.txtFuyoGengoYearSeinengappi4.Text))
                                    {
                                        string[] date = Util.ConvJpDate(DateTime.Today, this.Dba);
                                        this.txtFuyoGengoYearSeinengappi4.Text = date[2];
                                    }

                                    FixDateControls(
                                        this.lblFuyoGengoSeinengappi4,
                                        this.txtFuyoGengoYearSeinengappi4,
                                        this.txtFuyoMonthSeinengappi4,
                                        this.txtFuyoDaySeinengappi4);
                                }
                            }
                            #endregion

                            #region 16歳未満の扶養親族
                            else if (this.ActiveCtlNm == "txtFuyoMimanGengoYearSeinengappi1")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.InData = this.lblFuyoMimanGengoSeinengappi1.Text;
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.lblFuyoGengoSeinengappi1.Text = result[1];

                                    // 和暦年未入力時の初期値
                                    if (ValChk.IsEmpty(this.txtFuyoMimanGengoYearSeinengappi1.Text))
                                    {
                                        string[] date = Util.ConvJpDate(DateTime.Today, this.Dba);
                                        this.txtFuyoMimanGengoYearSeinengappi1.Text = date[2];
                                    }

                                    FixDateControls(
                                        this.lblFuyoMimanGengoSeinengappi1,
                                        this.txtFuyoMimanGengoYearSeinengappi1,
                                        this.txtFuyoMimanMonthSeinengappi1,
                                        this.txtFuyoMimanDaySeinengappi1);
                                }
                            }
                            else if (this.ActiveCtlNm == "txtFuyoMimanGengoYearSeinengappi2")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.InData = this.lblFuyoMimanGengoSeinengappi2.Text;
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.lblFuyoGengoSeinengappi2.Text = result[1];

                                    // 和暦年未入力時の初期値
                                    if (ValChk.IsEmpty(this.txtFuyoMimanGengoYearSeinengappi2.Text))
                                    {
                                        string[] date = Util.ConvJpDate(DateTime.Today, this.Dba);
                                        this.txtFuyoMimanGengoYearSeinengappi2.Text = date[2];
                                    }

                                    FixDateControls(
                                        this.lblFuyoMimanGengoSeinengappi2,
                                        this.txtFuyoMimanGengoYearSeinengappi2,
                                        this.txtFuyoMimanMonthSeinengappi2,
                                        this.txtFuyoMimanDaySeinengappi2);
                                }
                            }
                            else if (this.ActiveCtlNm == "txtFuyoMimanGengoYearSeinengappi3")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.InData = this.lblFuyoMimanGengoSeinengappi3.Text;
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.lblFuyoGengoSeinengappi3.Text = result[1];

                                    // 和暦年未入力時の初期値
                                    if (ValChk.IsEmpty(this.txtFuyoMimanGengoYearSeinengappi3.Text))
                                    {
                                        string[] date = Util.ConvJpDate(DateTime.Today, this.Dba);
                                        this.txtFuyoMimanGengoYearSeinengappi3.Text = date[2];
                                    }

                                    FixDateControls(
                                        this.lblFuyoMimanGengoSeinengappi3,
                                        this.txtFuyoMimanGengoYearSeinengappi3,
                                        this.txtFuyoMimanMonthSeinengappi3,
                                        this.txtFuyoMimanDaySeinengappi3);
                                }
                            }
                            else if (this.ActiveCtlNm == "txtFuyoMimanGengoYearSeinengappi4")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.InData = this.lblFuyoMimanGengoSeinengappi4.Text;
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.lblFuyoGengoSeinengappi4.Text = result[1];

                                    // 和暦年未入力時の初期値
                                    if (ValChk.IsEmpty(this.txtFuyoMimanGengoYearSeinengappi4.Text))
                                    {
                                        string[] date = Util.ConvJpDate(DateTime.Today, this.Dba);
                                        this.txtFuyoMimanGengoYearSeinengappi4.Text = date[2];
                                    }

                                    FixDateControls(
                                        this.lblFuyoMimanGengoSeinengappi4,
                                        this.txtFuyoMimanGengoYearSeinengappi4,
                                        this.txtFuyoMimanMonthSeinengappi4,
                                        this.txtFuyoMimanDaySeinengappi4);
                                }
                            }
                            #endregion
                        }
                    }
                    #endregion
                    break;
                case "txtBumonCd":
                    #region 部門検索
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("KYCM1031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.ky.kycm1031.KYCM1031");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.txtBumonCd.Text = result[0];
                                this.lblBumonNm.Text = result[1];
                                this.txtBukaCd.Text = "";
                                this.lblBukaNm.Text = "";
                            }
                        }
                    }
                    #endregion
                    break;
                case "txtBukaCd":
                    #region 部課検索
                    // 部門コード入力済の場合に実行
                    if (!ValChk.IsEmpty(this.txtBumonCd.Text))
                    {
                        // アセンブリのロード
                        asm = System.Reflection.Assembly.LoadFrom("KYCM1041.exe");
                        // フォーム作成
                        t = asm.GetType("jp.co.fsi.ky.kycm1041.KYCM1041");
                        if (t != null)
                        {
                            Object obj = System.Activator.CreateInstance(t);
                            if (obj != null)
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.Par1 = "1";                     // 検索ダイアログ起動パラメータ
                                frm.InData = this.txtBumonCd.Text;  // (string)部門コード
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.txtBukaCd.Text = result[0];
                                    this.lblBukaNm.Text = result[1];
                                }
                            }
                        }
                    }
                    #endregion
                    break;
                case "txtYakushokuCd":
                    #region 役職検索
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("KYCM1071.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.ky.kycm1071.KYCM1071");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";       // 起動パラメータ
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.txtYakushokuCd.Text = result[0];
                                this.lblYakushokuNm.Text = result[1];
                            }
                        }
                    }
                    #endregion
                    break;
                case "txtKyuyoKeitai":
                    #region 給与形態検索
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("CMCM1041.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "TB_KY_F_KYUYO_KEITAI";       // 起動パラメータ
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.txtKyuyoKeitai.Text = result[0];
                                this.lblKyuyoKeitaiNm.Text = result[1];
                            }
                        }
                    }
                    #endregion
                    break;
                case "txtJuminzeiShichosonCd":
                    #region 市町村検索
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("KYCM1081.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.ky.kycm1081.KYCM1081");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.txtJuminzeiShichosonCd.Text = result[0];
                                this.lblJuminzeiShichosonNm.Text = result[1];
                            }
                        }
                    }
                    #endregion
                    break;
                case "txtHonninKubun1":
                    #region 本人区分1検索
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("CMCM1041.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "TB_KY_F_HONNIN_KUBUN1";       // 起動パラメータ
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.txtHonninKubun1.Text = result[0];
                                this.lblHonninKubun1Nm.Text = result[1];
                            }
                        }
                    }
                    #endregion
                    break;
                case "txtHonninKubun2":
                    #region 本人区分2検索
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("CMCM1041.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "TB_KY_F_HONNIN_KUBUN2";       // 起動パラメータ
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.txtHonninKubun2.Text = result[0];
                                this.lblHonninKubun2Nm.Text = result[1];
                            }
                        }
                    }
                    #endregion
                    break;
                case "txtHonninKubun3":
                    #region 本人区分3検索
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("CMCM1041.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "TB_KY_F_HONNIN_KUBUN3";       // 起動パラメータ
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.txtHonninKubun3.Text = result[0];
                                this.lblHonninKubun3Nm.Text = result[1];
                            }
                        }
                    }
                    #endregion
                    break;
                case "txtHaigushaKubun":
                    #region 配偶者検索
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("CMCM1041.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "TB_KY_F_HAIGUSHA_KUBUN";       // 起動パラメータ
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.txtHaigushaKubun.Text = result[0];
                                this.lblHaigushaKubunNm.Text = result[1];
                            }
                        }
                    }
                    #endregion
                    break;
                case "txtKyuyoGinkoCd1":
                case "txtKyuyoGinkoCd2":
                    #region 銀行検索
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("KYCM1051.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.ky.kycm1051.KYCM1051");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            if (this.ActiveCtlNm == "txtKyuyoGinkoCd1")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.Par1 = "1";                     // 起動パラメータ
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.txtKyuyoGinkoCd1.Text = result[0];
                                    this.lblKyuyoGinkoNm1.Text = result[1];
                                    this.txtKyuyoShitenCd1.Text = "";
                                    this.lblKyuyoShitenNm1.Text = "";
                                }
                            }
                            else if (this.ActiveCtlNm == "txtKyuyoGinkoCd2")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.Par1 = "1";                     // 起動パラメータ
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.txtKyuyoGinkoCd2.Text = result[0];
                                    this.lblKyuyoGinkoNm2.Text = result[1];
                                    this.txtKyuyoShitenCd2.Text = "";
                                    this.lblKyuyoShitenNm2.Text = "";
                                }
                            }
                        }
                    }
                    #endregion
                    break;
                case "txtKyuyoShitenCd1":
                case "txtKyuyoShitenCd2":
                    #region 支店検索
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("KYCM1061.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.ky.kycm1061.KYCM1061");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            if (this.ActiveCtlNm == "txtKyuyoShitenCd1"
                                && !ValChk.IsEmpty(this.txtKyuyoGinkoCd1.Text))
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.Par1 = "1";                             // 起動パラメータ
                                frm.InData = this.txtKyuyoGinkoCd1.Text;    // (string)銀行コード
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.txtKyuyoShitenCd1.Text = result[0];
                                    this.lblKyuyoShitenNm1.Text = result[1];
                                }
                            }
                            else if (this.ActiveCtlNm == "txtKyuyoShitenCd2"
                                        && !ValChk.IsEmpty(this.txtKyuyoGinkoCd2.Text))
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.Par1 = "1";                             // 起動パラメータ
                                frm.InData = this.txtKyuyoGinkoCd2.Text;    // (string)銀行コード
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.txtKyuyoShitenCd2.Text = result[0];
                                    this.lblKyuyoShitenNm2.Text = result[1];
                                }
                            }
                        }
                    }
                    #endregion
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// F3キー押下時処理
        /// </summary>
        public override void PressF3()
        {
            // 新規モードの場合は処理させない
            if (this.Par1.Equals(MODE_NEW)) return;

            // 確認メッセージを表示
            string msg = "削除しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // 削除用パラメータ
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
            dpc.SetParam("@SHAIN_CD", SqlDbType.Decimal, 6, this.txtShainCd.Text);
            dpc.SetParam("@KUBUN", SqlDbType.Decimal, 2, 1);

            try
            {
                // トランザクション開始
                this.Dba.BeginTransaction();

                if (MODE_EDIT.Equals(this.Par1))
                {
                    // マイナンバー削除
                    this.Dba.Delete("TB_CM_SEALA_NO", "KAISHA_CD = @KAISHA_CD AND SEALA_NO = @SHAIN_CD AND KUBUN = @KUBUN", dpc);
                    this.Dba.Delete("TB_CM_SEALA_NO1", "KAISHA_CD = @KAISHA_CD AND SEALA_NO = @SHAIN_CD AND KUBUN = @KUBUN", dpc);
                    // 給与.社員情報データ削除
                    this.Dba.Delete("TB_KY_SHAIN_JOHO", "KAISHA_CD = @KAISHA_CD AND SHAIN_CD = @SHAIN_CD", dpc);
                }

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            this.tabPages.SelectedTab = this.tabPage6;
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            this.tabPages.SelectedTab = this.tabPage7;
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        public override void PressF6()
        {
            // 確認メッセージを表示
            string msg = (MODE_NEW.Equals(this.Par1) ? "登録" : "更新") + "しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // 入力値をバインドパラメータとしてセットする
            ArrayList alParamsKyShainJoho = SetKyShainJohoParams();

            try
            {
                // トランザクション開始
                this.Dba.BeginTransaction();
                if (MODE_NEW.Equals(this.Par1))
                {
                    // マイナンバー登録
                    Mynumber.InsertNumber(Util.ToString(txtMyNumber.Text), 1, Util.ToInt(txtShainCd.Text), 0, this.Dba);
                    #region 扶養親族用マイナンバー登録処理
                    if (!ValChk.IsEmpty(this.txtFuyoHaigushaNm.Text))
                    {
                        // 削除用パラメータ
                        DbParamCollection dpc = new DbParamCollection();
                        dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
                        dpc.SetParam("@SHAIN_CD", SqlDbType.Decimal, 6, this.txtShainCd.Text);
                        dpc.SetParam("@FUYO_NO", SqlDbType.Decimal, 2, 9);
                        dpc.SetParam("@KUBUN", SqlDbType.Decimal, 2, 1);

                        // マイナンバー削除
                        this.Dba.Delete("TB_CM_SEALA_NO", "KAISHA_CD = @KAISHA_CD AND SEALA_NO = @SHAIN_CD AND FUYO_NO = @FUYO_NO AND KUBUN = @KUBUN", dpc);
                        this.Dba.Delete("TB_CM_SEALA_NO1", "KAISHA_CD = @KAISHA_CD AND SEALA_NO = @SHAIN_CD AND FUYO_NO = @FUYO_NO AND KUBUN = @KUBUN", dpc);

                        Mynumber.InsertNumber(Util.ToString(txtFuyoHaigushaMyNumber.Text), 1, Util.ToInt(txtShainCd.Text), 9, this.Dba);
                    }
                    if (!ValChk.IsEmpty(this.txtFuyoNm1.Text))
                    {
                        // 削除用パラメータ
                        DbParamCollection dpc = new DbParamCollection();
                        dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
                        dpc.SetParam("@SHAIN_CD", SqlDbType.Decimal, 6, this.txtShainCd.Text);
                        dpc.SetParam("@FUYO_NO", SqlDbType.Decimal, 2, 1);
                        dpc.SetParam("@KUBUN", SqlDbType.Decimal, 2, 1);

                        // マイナンバー削除
                        this.Dba.Delete("TB_CM_SEALA_NO", "KAISHA_CD = @KAISHA_CD AND SEALA_NO = @SHAIN_CD AND FUYO_NO = @FUYO_NO AND KUBUN = @KUBUN", dpc);
                        this.Dba.Delete("TB_CM_SEALA_NO1", "KAISHA_CD = @KAISHA_CD AND SEALA_NO = @SHAIN_CD AND FUYO_NO = @FUYO_NO AND KUBUN = @KUBUN", dpc);

                        Mynumber.InsertNumber(Util.ToString(txtFuyoMyNumber1.Text), 1, Util.ToInt(txtShainCd.Text), 1, this.Dba);
                    }
                    if (!ValChk.IsEmpty(this.txtFuyoNm2.Text))
                    {
                        // 削除用パラメータ
                        DbParamCollection dpc = new DbParamCollection();
                        dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
                        dpc.SetParam("@SHAIN_CD", SqlDbType.Decimal, 6, this.txtShainCd.Text);
                        dpc.SetParam("@FUYO_NO", SqlDbType.Decimal, 2, 2);
                        dpc.SetParam("@KUBUN", SqlDbType.Decimal, 2, 1);

                        // マイナンバー削除
                        this.Dba.Delete("TB_CM_SEALA_NO", "KAISHA_CD = @KAISHA_CD AND SEALA_NO = @SHAIN_CD AND FUYO_NO = @FUYO_NO AND KUBUN = @KUBUN", dpc);
                        this.Dba.Delete("TB_CM_SEALA_NO1", "KAISHA_CD = @KAISHA_CD AND SEALA_NO = @SHAIN_CD AND FUYO_NO = @FUYO_NO AND KUBUN = @KUBUN", dpc);

                        Mynumber.InsertNumber(Util.ToString(txtFuyoMyNumber2.Text), 1, Util.ToInt(txtShainCd.Text), 2, this.Dba);
                    }
                    if (!ValChk.IsEmpty(this.txtFuyoNm3.Text))
                    {
                        // 削除用パラメータ
                        DbParamCollection dpc = new DbParamCollection();
                        dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
                        dpc.SetParam("@SHAIN_CD", SqlDbType.Decimal, 6, this.txtShainCd.Text);
                        dpc.SetParam("@FUYO_NO", SqlDbType.Decimal, 2, 3);
                        dpc.SetParam("@KUBUN", SqlDbType.Decimal, 2, 1);

                        // マイナンバー削除
                        this.Dba.Delete("TB_CM_SEALA_NO", "KAISHA_CD = @KAISHA_CD AND SEALA_NO = @SHAIN_CD AND FUYO_NO = @FUYO_NO AND KUBUN = @KUBUN", dpc);
                        this.Dba.Delete("TB_CM_SEALA_NO1", "KAISHA_CD = @KAISHA_CD AND SEALA_NO = @SHAIN_CD AND FUYO_NO = @FUYO_NO AND KUBUN = @KUBUN", dpc);

                        Mynumber.InsertNumber(Util.ToString(txtFuyoMyNumber3.Text), 1, Util.ToInt(txtShainCd.Text), 3, this.Dba);
                    }
                    if (!ValChk.IsEmpty(this.txtFuyoNm4.Text))
                    {
                        // 削除用パラメータ
                        DbParamCollection dpc = new DbParamCollection();
                        dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
                        dpc.SetParam("@SHAIN_CD", SqlDbType.Decimal, 6, this.txtShainCd.Text);
                        dpc.SetParam("@FUYO_NO", SqlDbType.Decimal, 2, 4);
                        dpc.SetParam("@KUBUN", SqlDbType.Decimal, 2, 1);

                        // マイナンバー削除
                        this.Dba.Delete("TB_CM_SEALA_NO", "KAISHA_CD = @KAISHA_CD AND SEALA_NO = @SHAIN_CD AND FUYO_NO = @FUYO_NO AND KUBUN = @KUBUN", dpc);
                        this.Dba.Delete("TB_CM_SEALA_NO1", "KAISHA_CD = @KAISHA_CD AND SEALA_NO = @SHAIN_CD AND FUYO_NO = @FUYO_NO AND KUBUN = @KUBUN", dpc);

                        Mynumber.InsertNumber(Util.ToString(txtFuyoMyNumber4.Text), 1, Util.ToInt(txtShainCd.Text), 4, this.Dba);
                    }
                    if (!ValChk.IsEmpty(this.txtFuyoMimanNm1.Text))
                    {
                        // 削除用パラメータ
                        DbParamCollection dpc = new DbParamCollection();
                        dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
                        dpc.SetParam("@SHAIN_CD", SqlDbType.Decimal, 6, this.txtShainCd.Text);
                        dpc.SetParam("@FUYO_NO", SqlDbType.Decimal, 2, 5);
                        dpc.SetParam("@KUBUN", SqlDbType.Decimal, 2, 1);

                        // マイナンバー削除
                        this.Dba.Delete("TB_CM_SEALA_NO", "KAISHA_CD = @KAISHA_CD AND SEALA_NO = @SHAIN_CD AND FUYO_NO = @FUYO_NO AND KUBUN = @KUBUN", dpc);
                        this.Dba.Delete("TB_CM_SEALA_NO1", "KAISHA_CD = @KAISHA_CD AND SEALA_NO = @SHAIN_CD AND FUYO_NO = @FUYO_NO AND KUBUN = @KUBUN", dpc);

                        Mynumber.InsertNumber(Util.ToString(txtFuyoMimanMyNumber1.Text), 1, Util.ToInt(txtShainCd.Text), 5, this.Dba);
                    }
                    if (!ValChk.IsEmpty(this.txtFuyoMimanNm2.Text))
                    {
                        // 削除用パラメータ
                        DbParamCollection dpc = new DbParamCollection();
                        dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
                        dpc.SetParam("@SHAIN_CD", SqlDbType.Decimal, 6, this.txtShainCd.Text);
                        dpc.SetParam("@FUYO_NO", SqlDbType.Decimal, 2, 6);
                        dpc.SetParam("@KUBUN", SqlDbType.Decimal, 2, 1);

                        // マイナンバー削除
                        this.Dba.Delete("TB_CM_SEALA_NO", "KAISHA_CD = @KAISHA_CD AND SEALA_NO = @SHAIN_CD AND FUYO_NO = @FUYO_NO AND KUBUN = @KUBUN", dpc);
                        this.Dba.Delete("TB_CM_SEALA_NO1", "KAISHA_CD = @KAISHA_CD AND SEALA_NO = @SHAIN_CD AND FUYO_NO = @FUYO_NO AND KUBUN = @KUBUN", dpc);

                        Mynumber.InsertNumber(Util.ToString(txtFuyoMimanMyNumber2.Text), 1, Util.ToInt(txtShainCd.Text), 6, this.Dba);
                    }
                    if (!ValChk.IsEmpty(this.txtFuyoMimanNm3.Text))
                    {
                        // 削除用パラメータ
                        DbParamCollection dpc = new DbParamCollection();
                        dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
                        dpc.SetParam("@SHAIN_CD", SqlDbType.Decimal, 6, this.txtShainCd.Text);
                        dpc.SetParam("@FUYO_NO", SqlDbType.Decimal, 2, 7);
                        dpc.SetParam("@KUBUN", SqlDbType.Decimal, 2, 1);

                        // マイナンバー削除
                        this.Dba.Delete("TB_CM_SEALA_NO", "KAISHA_CD = @KAISHA_CD AND SEALA_NO = @SHAIN_CD AND FUYO_NO = @FUYO_NO AND KUBUN = @KUBUN", dpc);
                        this.Dba.Delete("TB_CM_SEALA_NO1", "KAISHA_CD = @KAISHA_CD AND SEALA_NO = @SHAIN_CD AND FUYO_NO = @FUYO_NO AND KUBUN = @KUBUN", dpc);

                        Mynumber.InsertNumber(Util.ToString(txtFuyoMimanMyNumber3.Text), 1, Util.ToInt(txtShainCd.Text), 7, this.Dba);
                    }
                    if (!ValChk.IsEmpty(this.txtFuyoMimanNm4.Text))
                    {
                        // 削除用パラメータ
                        DbParamCollection dpc = new DbParamCollection();
                        dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
                        dpc.SetParam("@SHAIN_CD", SqlDbType.Decimal, 6, this.txtShainCd.Text);
                        dpc.SetParam("@FUYO_NO", SqlDbType.Decimal, 2, 8);
                        dpc.SetParam("@KUBUN", SqlDbType.Decimal, 2, 1);

                        // マイナンバー削除
                        this.Dba.Delete("TB_CM_SEALA_NO", "KAISHA_CD = @KAISHA_CD AND SEALA_NO = @SHAIN_CD AND FUYO_NO = @FUYO_NO AND KUBUN = @KUBUN", dpc);
                        this.Dba.Delete("TB_CM_SEALA_NO1", "KAISHA_CD = @KAISHA_CD AND SEALA_NO = @SHAIN_CD AND FUYO_NO = @FUYO_NO AND KUBUN = @KUBUN", dpc);

                        Mynumber.InsertNumber(Util.ToString(txtFuyoMimanMyNumber4.Text), 1, Util.ToInt(txtShainCd.Text), 8, this.Dba);
                    }
                    #endregion

                    // 給与.社員情報データ登録
                    this.Dba.Insert("TB_KY_SHAIN_JOHO", (DbParamCollection)alParamsKyShainJoho[0]);
                }
                else if (MODE_EDIT.Equals(this.Par1))
                {
                    // 削除用パラメータ
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
                    dpc.SetParam("@SHAIN_CD", SqlDbType.Decimal, 6, this.txtShainCd.Text);
                    dpc.SetParam("@FUYO_NO", SqlDbType.Decimal, 2, 0);
                    dpc.SetParam("@KUBUN", SqlDbType.Decimal, 2, 1);

                    // マイナンバー削除
                    this.Dba.Delete("TB_CM_SEALA_NO", "KAISHA_CD = @KAISHA_CD AND SEALA_NO = @SHAIN_CD AND FUYO_NO = @FUYO_NO AND KUBUN = @KUBUN", dpc);
                    this.Dba.Delete("TB_CM_SEALA_NO1", "KAISHA_CD = @KAISHA_CD AND SEALA_NO = @SHAIN_CD AND FUYO_NO = @FUYO_NO AND KUBUN = @KUBUN", dpc);

                    #region 扶養親族用マイナンバー削除処理
                    if (this.txtFuyoHaigushaNm.Text != "")
                    {
                        // 削除用パラメータ
                        dpc = new DbParamCollection();
                        dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
                        dpc.SetParam("@SHAIN_CD", SqlDbType.Decimal, 6, this.txtShainCd.Text);
                        dpc.SetParam("@FUYO_NO", SqlDbType.Decimal, 2, 9);
                        dpc.SetParam("@KUBUN", SqlDbType.Decimal, 2, 1);

                        // マイナンバー削除
                        this.Dba.Delete("TB_CM_SEALA_NO", "KAISHA_CD = @KAISHA_CD AND SEALA_NO = @SHAIN_CD AND FUYO_NO = @FUYO_NO AND KUBUN = @KUBUN", dpc);
                        this.Dba.Delete("TB_CM_SEALA_NO1", "KAISHA_CD = @KAISHA_CD AND SEALA_NO = @SHAIN_CD AND FUYO_NO = @FUYO_NO AND KUBUN = @KUBUN", dpc);

                    }
                    if (this.txtFuyoNm1.Text != "")
                    {
                        // 削除用パラメータ
                        dpc = new DbParamCollection();
                        dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
                        dpc.SetParam("@SHAIN_CD", SqlDbType.Decimal, 6, this.txtShainCd.Text);
                        dpc.SetParam("@FUYO_NO", SqlDbType.Decimal, 2, 1);
                        dpc.SetParam("@KUBUN", SqlDbType.Decimal, 2, 1);

                        // マイナンバー削除
                        this.Dba.Delete("TB_CM_SEALA_NO", "KAISHA_CD = @KAISHA_CD AND SEALA_NO = @SHAIN_CD AND FUYO_NO = @FUYO_NO AND KUBUN = @KUBUN", dpc);
                        this.Dba.Delete("TB_CM_SEALA_NO1", "KAISHA_CD = @KAISHA_CD AND SEALA_NO = @SHAIN_CD AND FUYO_NO = @FUYO_NO AND KUBUN = @KUBUN", dpc);

                    }
                    if (this.txtFuyoNm2.Text != "")
                    {
                        // 削除用パラメータ
                        dpc = new DbParamCollection();
                        dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
                        dpc.SetParam("@SHAIN_CD", SqlDbType.Decimal, 6, this.txtShainCd.Text);
                        dpc.SetParam("@FUYO_NO", SqlDbType.Decimal, 2, 2);
                        dpc.SetParam("@KUBUN", SqlDbType.Decimal, 2, 1);

                        // マイナンバー削除
                        this.Dba.Delete("TB_CM_SEALA_NO", "KAISHA_CD = @KAISHA_CD AND SEALA_NO = @SHAIN_CD AND FUYO_NO = @FUYO_NO AND KUBUN = @KUBUN", dpc);
                        this.Dba.Delete("TB_CM_SEALA_NO1", "KAISHA_CD = @KAISHA_CD AND SEALA_NO = @SHAIN_CD AND FUYO_NO = @FUYO_NO AND KUBUN = @KUBUN", dpc);

                    }
                    if (this.txtFuyoNm3.Text != "")
                    {
                        // 削除用パラメータ
                        dpc = new DbParamCollection();
                        dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
                        dpc.SetParam("@SHAIN_CD", SqlDbType.Decimal, 6, this.txtShainCd.Text);
                        dpc.SetParam("@FUYO_NO", SqlDbType.Decimal, 2, 3);
                        dpc.SetParam("@KUBUN", SqlDbType.Decimal, 2, 1);

                        // マイナンバー削除
                        this.Dba.Delete("TB_CM_SEALA_NO", "KAISHA_CD = @KAISHA_CD AND SEALA_NO = @SHAIN_CD AND FUYO_NO = @FUYO_NO AND KUBUN = @KUBUN", dpc);
                        this.Dba.Delete("TB_CM_SEALA_NO1", "KAISHA_CD = @KAISHA_CD AND SEALA_NO = @SHAIN_CD AND FUYO_NO = @FUYO_NO AND KUBUN = @KUBUN", dpc);

                    }
                    if (this.txtFuyoNm4.Text != "")
                    {
                        // 削除用パラメータ
                        dpc = new DbParamCollection();
                        dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
                        dpc.SetParam("@SHAIN_CD", SqlDbType.Decimal, 6, this.txtShainCd.Text);
                        dpc.SetParam("@FUYO_NO", SqlDbType.Decimal, 2, 4);
                        dpc.SetParam("@KUBUN", SqlDbType.Decimal, 2, 1);

                        // マイナンバー削除
                        this.Dba.Delete("TB_CM_SEALA_NO", "KAISHA_CD = @KAISHA_CD AND SEALA_NO = @SHAIN_CD AND FUYO_NO = @FUYO_NO AND KUBUN = @KUBUN", dpc);
                        this.Dba.Delete("TB_CM_SEALA_NO1", "KAISHA_CD = @KAISHA_CD AND SEALA_NO = @SHAIN_CD AND FUYO_NO = @FUYO_NO AND KUBUN = @KUBUN", dpc);

                    }
                    if (this.txtFuyoMimanNm1.Text != "")
                    {
                        // 削除用パラメータ
                        dpc = new DbParamCollection();
                        dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
                        dpc.SetParam("@SHAIN_CD", SqlDbType.Decimal, 6, this.txtShainCd.Text);
                        dpc.SetParam("@FUYO_NO", SqlDbType.Decimal, 2, 5);
                        dpc.SetParam("@KUBUN", SqlDbType.Decimal, 2, 1);

                        // マイナンバー削除
                        this.Dba.Delete("TB_CM_SEALA_NO", "KAISHA_CD = @KAISHA_CD AND SEALA_NO = @SHAIN_CD AND FUYO_NO = @FUYO_NO AND KUBUN = @KUBUN", dpc);
                        this.Dba.Delete("TB_CM_SEALA_NO1", "KAISHA_CD = @KAISHA_CD AND SEALA_NO = @SHAIN_CD AND FUYO_NO = @FUYO_NO AND KUBUN = @KUBUN", dpc);

                    }
                    if (this.txtFuyoMimanNm2.Text != "")
                    {
                        // 削除用パラメータ
                        dpc = new DbParamCollection();
                        dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
                        dpc.SetParam("@SHAIN_CD", SqlDbType.Decimal, 6, this.txtShainCd.Text);
                        dpc.SetParam("@FUYO_NO", SqlDbType.Decimal, 2, 6);
                        dpc.SetParam("@KUBUN", SqlDbType.Decimal, 2, 1);

                        // マイナンバー削除
                        this.Dba.Delete("TB_CM_SEALA_NO", "KAISHA_CD = @KAISHA_CD AND SEALA_NO = @SHAIN_CD AND FUYO_NO = @FUYO_NO AND KUBUN = @KUBUN", dpc);
                        this.Dba.Delete("TB_CM_SEALA_NO1", "KAISHA_CD = @KAISHA_CD AND SEALA_NO = @SHAIN_CD AND FUYO_NO = @FUYO_NO AND KUBUN = @KUBUN", dpc);

                    }
                    if (this.txtFuyoMimanNm3.Text != "")
                    {
                        // 削除用パラメータ
                        dpc = new DbParamCollection();
                        dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
                        dpc.SetParam("@SHAIN_CD", SqlDbType.Decimal, 6, this.txtShainCd.Text);
                        dpc.SetParam("@FUYO_NO", SqlDbType.Decimal, 2, 7);
                        dpc.SetParam("@KUBUN", SqlDbType.Decimal, 2, 1);

                        // マイナンバー削除
                        this.Dba.Delete("TB_CM_SEALA_NO", "KAISHA_CD = @KAISHA_CD AND SEALA_NO = @SHAIN_CD AND FUYO_NO = @FUYO_NO AND KUBUN = @KUBUN", dpc);
                        this.Dba.Delete("TB_CM_SEALA_NO1", "KAISHA_CD = @KAISHA_CD AND SEALA_NO = @SHAIN_CD AND FUYO_NO = @FUYO_NO AND KUBUN = @KUBUN", dpc);

                    }
                    if (this.txtFuyoMimanNm4.Text != "")
                    {
                        // 削除用パラメータ
                        dpc = new DbParamCollection();
                        dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
                        dpc.SetParam("@SHAIN_CD", SqlDbType.Decimal, 6, this.txtShainCd.Text);
                        dpc.SetParam("@FUYO_NO", SqlDbType.Decimal, 2, 8);
                        dpc.SetParam("@KUBUN", SqlDbType.Decimal, 2, 1);

                        // マイナンバー削除
                        this.Dba.Delete("TB_CM_SEALA_NO", "KAISHA_CD = @KAISHA_CD AND SEALA_NO = @SHAIN_CD AND FUYO_NO = @FUYO_NO AND KUBUN = @KUBUN", dpc);
                        this.Dba.Delete("TB_CM_SEALA_NO1", "KAISHA_CD = @KAISHA_CD AND SEALA_NO = @SHAIN_CD AND FUYO_NO = @FUYO_NO AND KUBUN = @KUBUN", dpc);

                    }
                    #endregion

                    // マイナンバー登録// セットするマイナンバー，区分，社員コード，※扶養ナンバー※，Dba
                    Mynumber.InsertNumber(Util.ToString(txtMyNumber.Text), 1, Util.ToInt(txtShainCd.Text), 0, this.Dba);

                    #region 扶養親族用マイナンバー登録処理
                    if (this.txtFuyoHaigushaNm.Text != "")
                    {
                        Mynumber.InsertNumber(Util.ToString(txtFuyoHaigushaMyNumber.Text), 1, Util.ToInt(txtShainCd.Text), 9, this.Dba);
                    }
                    if (this.txtFuyoNm1.Text != "")
                    {
                        Mynumber.InsertNumber(Util.ToString(txtFuyoMyNumber1.Text), 1, Util.ToInt(txtShainCd.Text), 1, this.Dba);
                    }
                    if (this.txtFuyoNm2.Text != "")
                    {
                        Mynumber.InsertNumber(Util.ToString(txtFuyoMyNumber2.Text), 1, Util.ToInt(txtShainCd.Text), 2, this.Dba);
                    }
                    if (this.txtFuyoNm3.Text != "")
                    {
                        Mynumber.InsertNumber(Util.ToString(txtFuyoMyNumber3.Text), 1, Util.ToInt(txtShainCd.Text), 3, this.Dba);
                    }
                    if (this.txtFuyoNm4.Text != "")
                    {
                        Mynumber.InsertNumber(Util.ToString(txtFuyoMyNumber4.Text), 1, Util.ToInt(txtShainCd.Text), 4, this.Dba);
                    }
                    if (this.txtFuyoMimanNm1.Text != "")
                    {
                        Mynumber.InsertNumber(Util.ToString(txtFuyoMimanMyNumber1.Text), 1, Util.ToInt(txtShainCd.Text), 5, this.Dba);
                    }
                    if (this.txtFuyoMimanNm2.Text != "")
                    {
                        Mynumber.InsertNumber(Util.ToString(txtFuyoMimanMyNumber2.Text), 1, Util.ToInt(txtShainCd.Text), 6, this.Dba);
                    }
                    if (this.txtFuyoMimanNm3.Text != "")
                    {
                        Mynumber.InsertNumber(Util.ToString(txtFuyoMimanMyNumber3.Text), 1, Util.ToInt(txtShainCd.Text), 7, this.Dba);
                    }
                    if (this.txtFuyoMimanNm4.Text != "")
                    {
                        Mynumber.InsertNumber(Util.ToString(txtFuyoMimanMyNumber4.Text), 1, Util.ToInt(txtShainCd.Text), 8, this.Dba);
                    }
                    #endregion

                    // 給与.社員情報データ更新
                    this.Dba.Update("TB_KY_SHAIN_JOHO",
                        (DbParamCollection)alParamsKyShainJoho[1],
                        "KAISHA_CD = @KAISHA_CD AND SHAIN_CD = @SHAIN_CD",
                        (DbParamCollection)alParamsKyShainJoho[0]);
                }
                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// F8キー押下時処理
        /// </summary>
        public override void PressF8()
        {
            this.tabPages.SelectedTab = this.tabPage1;
        }

        /// <summary>
        /// F9キー押下時処理
        /// </summary>
        public override void PressF9()
        {
            this.tabPages.SelectedTab = this.tabPage2;
        }

        /// <summary>
        /// F10キー押下時処理
        /// </summary>
        public override void PressF10()
        {
            this.tabPages.SelectedTab = this.tabPage3;
        }

        /// <summary>
        /// F11キー押下時処理
        /// </summary>
        public override void PressF11()
        {
            this.tabPages.SelectedTab = this.tabPage4;
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            this.tabPages.SelectedTab = this.tabPage5;
        }
        #endregion

        #region イベント
        /// <summary>
        /// マイナンバーの公開
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            if(!_numberFlg)
            {
                this.FsiTextBox1.Visible = false;
                this.txtMyNumber.ReadOnly = false;
                _numberFlg = true;
                this.button1.Text = "非公開";
            }
            else
            {
                this.FsiTextBox1.Visible = true;
                this.txtMyNumber.ReadOnly = true;
                _numberFlg = false;
                this.button1.Text = "公開";
            }
        }

        /// <summary>
        /// マイナンバーの公開--扶養配偶者
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            if (!_numberFlg)
            {
                this.txtFuyoHaigushaMyNumberDm.Visible = false;
                this.txtFuyoHaigushaMyNumber.ReadOnly = false;
                _numberFlg = true;
                this.button2.Text = "非公開";
            }
            else
            {
                this.txtFuyoHaigushaMyNumberDm.Visible = true;
                this.txtFuyoHaigushaMyNumber.ReadOnly = true;
                _numberFlg = false;
                this.button2.Text = "公開";
            }
        }

        /// <summary>
        /// マイナンバーの公開--扶養親族1
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            if (!_numberFlg)
            {
                this.txtFuyoMyNumberDm1.Visible = false;
                this.txtFuyoMyNumber1.ReadOnly = false;
                _numberFlg = true;
                this.button3.Text = "非公開";
            }
            else
            {
                this.txtFuyoMyNumberDm1.Visible = true;
                this.txtFuyoMyNumber1.ReadOnly = true;
                _numberFlg = false;
                this.button3.Text = "公開";
            }
        }

        /// <summary>
        /// マイナンバーの公開--扶養親族2
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button4_Click(object sender, EventArgs e)
        {
            if (!_numberFlg)
            {
                this.txtFuyoMyNumberDm2.Visible = false;
                this.txtFuyoMyNumber2.ReadOnly = false;
                _numberFlg = true;
                this.button4.Text = "非公開";
            }
            else
            {
                this.txtFuyoMyNumberDm2.Visible = true;
                this.txtFuyoMyNumber2.ReadOnly = true;
                _numberFlg = false;
                this.button4.Text = "公開";
            }
        }

        /// <summary>
        /// マイナンバーの公開--扶養親族3
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button5_Click(object sender, EventArgs e)
        {
            if (!_numberFlg)
            {
                this.txtFuyoMyNumberDm3.Visible = false;
                this.txtFuyoMyNumber3.ReadOnly = false;
                _numberFlg = true;
                this.button5.Text = "非公開";
            }
            else
            {
                this.txtFuyoMyNumberDm3.Visible = true;
                this.txtFuyoMyNumber3.ReadOnly = true;
                _numberFlg = false;
                this.button5.Text = "公開";
            }
        }

        /// <summary>
        /// マイナンバーの公開--扶養親族4
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button6_Click(object sender, EventArgs e)
        {
            if (!_numberFlg)
            {
                this.txtFuyoMyNumberDm4.Visible = false;
                this.txtFuyoMyNumber4.ReadOnly = false;
                _numberFlg = true;
                this.button6.Text = "非公開";
            }
            else
            {
                this.txtFuyoMyNumberDm4.Visible = true;
                this.txtFuyoMyNumber4.ReadOnly = true;
                _numberFlg = false;
                this.button6.Text = "公開";
            }
        }

        /// <summary>
        /// マイナンバーの公開--扶養親族16歳未満1
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button7_Click(object sender, EventArgs e)
        {
            if (!_numberFlg)
            {
                this.txtFuyoMimanMyNumberDm1.Visible = false;
                this.txtFuyoMimanMyNumber1.ReadOnly = false;
                _numberFlg = true;
                this.button7.Text = "非公開";
            }
            else
            {
                this.txtFuyoMimanMyNumberDm1.Visible = true;
                this.txtFuyoMimanMyNumber1.ReadOnly = true;
                _numberFlg = false;
                this.button7.Text = "公開";
            }
        }

        /// <summary>
        /// マイナンバーの公開--扶養親族16歳未満2
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button8_Click(object sender, EventArgs e)
        {
            if (!_numberFlg)
            {
                this.txtFuyoMimanMyNumberDm2.Visible = false;
                this.txtFuyoMimanMyNumber2.ReadOnly = false;
                _numberFlg = true;
                this.button8.Text = "非公開";
            }
            else
            {
                this.txtFuyoMimanMyNumberDm2.Visible = true;
                this.txtFuyoMimanMyNumber2.ReadOnly = true;
                _numberFlg = false;
                this.button8.Text = "公開";
            }
        }

        /// <summary>
        /// マイナンバーの公開--扶養親族16歳未満3
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button9_Click(object sender, EventArgs e)
        {
            if (!_numberFlg)
            {
                this.txtFuyoMimanMyNumberDm3.Visible = false;
                this.txtFuyoMimanMyNumber3.ReadOnly = false;
                _numberFlg = true;
                this.button9.Text = "非公開";
            }
            else
            {
                this.txtFuyoMimanMyNumberDm3.Visible = true;
                this.txtFuyoMimanMyNumber3.ReadOnly = true;
                _numberFlg = false;
                this.button9.Text = "公開";
            }
        }

        /// <summary>
        /// マイナンバーの公開--扶養親族16歳未満4
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button10_Click(object sender, EventArgs e)
        {
            if (!_numberFlg)
            {
                this.txtFuyoMimanMyNumberDm4.Visible = false;
                this.txtFuyoMimanMyNumber4.ReadOnly = false;
                _numberFlg = true;
                this.button10.Text = "非公開";
            }
            else
            {
                this.txtFuyoMimanMyNumberDm4.Visible = true;
                this.txtFuyoMimanMyNumber4.ReadOnly = true;
                _numberFlg = false;
                this.button10.Text = "公開";
            }
        }

        /// <summary>
        /// 社員コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShainCd_Validating(object sender, CancelEventArgs e)
        {
            // 入力値補正
            this.txtShainCd.Text = Util.ToString(Util.ToDecimal(this.txtShainCd.Text));

            if (!IsValidShainCd())
            {
                e.Cancel = true;
                this.txtShainCd.SelectAll();
                return;
            }

            string name = this.Dba.GetName(this.UInfo, "TB_KY_SHAIN_JOHO", " ", this.txtShainCd.Text);
            if (name == null)
            {
                // OK 入力値を表示
                this.lblShainNmDisp.Text = this.txtShainNm.Text;
            }
        }

        /// <summary>
        /// 社員名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShainNm_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShainNm())
            {
                e.Cancel = true;
                this.txtShainNm.SelectAll();
                return;
            }

            this.lblShainNmDisp.Text = this.txtShainNm.Text;
        }

        /// <summary>
        /// 社員カナ名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShainKanaNm_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShainKanaNm())
            {
                e.Cancel = true;
                this.txtShainKanaNm.SelectAll();
            }
        }

        /// <summary>
        /// 性別の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSeibetsu_Validating(object sender, CancelEventArgs e)
        {
            // 定義外の値を補正
            this.txtSeibetsu.Text = FixDefinedCode(this.txtSeibetsu.Text, 2);
        }

        /// <summary>
        /// 生年月日(年)値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGengoYearSeinengappi_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtGengoYearSeinengappi.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtGengoYearSeinengappi.SelectAll();
                e.Cancel = true;
                return;
            }

            // 日付コントロール補正
            FixDateControls(
                this.lblGengoSeinengappi,
                this.txtGengoYearSeinengappi,
                this.txtMonthSeinengappi,
                this.txtDaySeinengappi);
        }

        /// <summary>
        /// 生年月日(月)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonthSeinengappi_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtMonthSeinengappi.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtMonthSeinengappi.SelectAll();
                e.Cancel = true;
                return;
            }

            // 日付コントロール補正
            FixDateControls(
                this.lblGengoSeinengappi,
                this.txtGengoYearSeinengappi,
                this.txtMonthSeinengappi,
                this.txtDaySeinengappi);
        }

        /// <summary>
        /// 生年月日(日)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDaySeinengappi_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtDaySeinengappi.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtDaySeinengappi.SelectAll();
                e.Cancel = true;
                return;
            }

            // 日付コントロール補正
            FixDateControls(
                this.lblGengoSeinengappi,
                this.txtGengoYearSeinengappi,
                this.txtMonthSeinengappi,
                this.txtDaySeinengappi);
        }

        /// <summary>
        /// 郵便番号1の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtYubinBango1_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidYubinBango1())
            {
                e.Cancel = true;
                this.txtYubinBango1.SelectAll();
            }
        }

        /// <summary>
        /// 郵便番号2の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtYubinBango2_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidYubinBango2())
            {
                e.Cancel = true;
                this.txtYubinBango2.SelectAll();
            }
        }

        /// <summary>
        /// 住所1の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtJusho1_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidJusho1())
            {
                e.Cancel = true;
                this.txtJusho1.SelectAll();
            }
        }

        /// <summary>
        /// 住所2の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtJusho2_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidJusho2())
            {
                e.Cancel = true;
                this.txtJusho2.SelectAll();
            }
        }

        /// <summary>
        /// 電話番号の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDenwaBango_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidDenwaBango())
            {
                e.Cancel = true;
                this.txtDenwaBango.SelectAll();
            }
        }

        /// <summary>
        /// 一覧表示の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txthyoji_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidhyoji())
            {
                e.Cancel = true;
                this.txthyoji.SelectAll();
            }
        }

        /// <summary>
        /// マイナンバーの値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMyNumber_Validating(object sender, CancelEventArgs e)
        {

            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtMyNumber.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtMyNumber.SelectAll();
                e.Cancel = true;
                return;
            }

            if (!IsValidMyNumber())
            {
                e.Cancel = true;
                this.txtMyNumber.SelectAll();
            }
        }

        /// <summary>
        /// 部門コードの値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBumonCd_Validating(object sender, CancelEventArgs e)
        {
            // 入力値補正
            this.txtBumonCd.Text = Util.ToString(Util.ToDecimal(this.txtBumonCd.Text));

            // 数字のみの入力を許可
            if (!IsValidBumonCd())
            {
                e.Cancel = true;
                this.txtBumonCd.SelectAll();
                return;
            }

            if (this.txtBumonCd.Modified || this.txtBumonCd.Text == "0")
            {
                // 名称セット
                this.lblBumonNm.Text = this.Dba.GetName(this.UInfo, "TB_KY_BUMON", " ", Util.ToString(this.txtBumonCd.Text));
                this.txtBukaCd.Text = "0";
                this.lblBukaNm.Text = "";

                this.txtBumonCd.Modified = false;
            }
        }

        /// <summary>
        /// 部課コードの値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBukaCd_Validating(object sender, CancelEventArgs e)
        {
            // 入力値補正
            this.txtBukaCd.Text = Util.ToString(Util.ToDecimal(this.txtBukaCd.Text));

            // 数字のみの入力を許可・未入力はNG
            if (!IsValidBukaCd())
            {
                e.Cancel = true;
                this.txtBukaCd.SelectAll();
                return;
            }

            if (this.txtBukaCd.Modified || this.txtBukaCd.Text == "0")
            {
                // 名称セット
                this.lblBukaNm.Text = this.Dba.GetBukaNm(
                    this.UInfo, Util.ToString(this.txtBumonCd.Text), Util.ToString(this.txtBukaCd.Text));

                this.txtBukaCd.Modified = false;
            }
        }

        /// <summary>
        /// 役職コードの値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtYakushokuCd_Validating(object sender, CancelEventArgs e)
        {
            // 入力値補正
            this.txtYakushokuCd.Text = Util.ToString(Util.ToDecimal(this.txtYakushokuCd.Text));

            // 数字のみの入力を許可・未入力はNG
            if (!IsValidYakushokuCd())
            {
                e.Cancel = true;
                this.txtYakushokuCd.SelectAll();
            }

            if (this.txtYakushokuCd.Modified || this.txtYakushokuCd.Text == "0")
            {
                // 名称セット
                this.lblYakushokuNm.Text =
                    this.Dba.GetName(this.UInfo, "TB_KY_YAKUSHOKU", " ", Util.ToString(this.txtYakushokuCd.Text));

                this.txtYakushokuCd.Modified = false;
            }
        }

        /// <summary>
        /// 給与形態の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKyuyoKeitai_Validating(object sender, CancelEventArgs e)
        {
            // 入力値補正
            this.txtKyuyoKeitai.Text = Util.ToString(Util.ToDecimal(this.txtKyuyoKeitai.Text));

            // 数字のみの入力を許可・未入力はNG
            if (!IsValidKyuyoKeitai())
            {
                e.Cancel = true;
                this.txtKyuyoKeitai.SelectAll();
            }

            if (this.txtKyuyoKeitai.Modified || this.txtKyuyoKeitai.Text == "0")
            {
                // 名称セット
                this.lblKyuyoKeitaiNm.Text =
                    this.Dba.GetKyuyoKbnNm("1", Util.ToString(this.txtKyuyoKeitai.Text));

                this.txtKyuyoKeitai.Modified = false;
            }
        }

        /// <summary>
        /// 入社年月日(年)値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGengoYearNyushaNengappi_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtGengoYearNyushaNengappi.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtGengoYearNyushaNengappi.SelectAll();
                e.Cancel = true;
                return;
            }

            // 日付コントロール補正
            FixDateControls(
                this.lblGengoNyushaNengappi,
                this.txtGengoYearNyushaNengappi,
                this.txtMonthNyushaNengappi,
                this.txtDayNyushaNengappi);
        }

        /// <summary>
        /// 入社年月日(月)値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonthNyushaNengappi_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtMonthNyushaNengappi.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtMonthNyushaNengappi.SelectAll();
                e.Cancel = true;
                return;
            }

            // 日付コントロール補正
            FixDateControls(
                this.lblGengoNyushaNengappi,
                this.txtGengoYearNyushaNengappi,
                this.txtMonthNyushaNengappi,
                this.txtDayNyushaNengappi);
        }

        /// <summary>
        /// 入社年月日(日)値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDayNyushaNengappi_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtDayNyushaNengappi.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtDayNyushaNengappi.SelectAll();
                e.Cancel = true;
                return;
            }

            // 日付フィールド補正
            FixDateControls(
                this.lblGengoNyushaNengappi,
                this.txtGengoYearNyushaNengappi,
                this.txtMonthNyushaNengappi,
                this.txtDayNyushaNengappi);
        }

        /// <summary>
        /// 退職年月日(年)値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGengoYearTaishokuNengappi_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtGengoYearTaishokuNengappi.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtGengoYearTaishokuNengappi.SelectAll();
                e.Cancel = true;
                return;
            }

            // 日付コントロール補正
            FixDateControls(
                this.lblGengoTaishokuNengappi,
                this.txtGengoYearTaishokuNengappi,
                this.txtMonthTaishokuNengappi,
                this.txtDayTaishokuNengappi);
        }

        /// <summary>
        /// 退職年月日(月)値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonthTaishokuNengappi_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtMonthTaishokuNengappi.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtMonthTaishokuNengappi.SelectAll();
                e.Cancel = true;
                return;
            }

            // 日付コントロール補正
            FixDateControls(
                this.lblGengoTaishokuNengappi,
                this.txtGengoYearTaishokuNengappi,
                this.txtMonthTaishokuNengappi,
                this.txtDayTaishokuNengappi);
        }

        /// <summary>
        /// 退職年月日(日)値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDayTaishokuNengappi_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtDayTaishokuNengappi.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtDayTaishokuNengappi.SelectAll();
                e.Cancel = true;
                return;
            }

            // 日付コントロール補正
            FixDateControls(
                this.lblGengoTaishokuNengappi,
                this.txtGengoYearTaishokuNengappi,
                this.txtMonthTaishokuNengappi,
                this.txtDayTaishokuNengappi);
        }

        /// <summary>
        /// 健康保険証番号の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKenkoHokenshoBango_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidKenkoHokenshoBango())
            {
                e.Cancel = true;
                this.txtKenkoHokenshoBango.SelectAll();
            }
        }

        /// <summary>
        /// 健康保険報酬月額の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKenkoHokenHyojunHoshuGtgk_Validating(object sender, CancelEventArgs e)
        {
            // 入力値補正
            this.txtKenkoHokenHyojunHoshuGtgk.Text = Util.ToString(Util.ToDecimal(this.txtKenkoHokenHyojunHoshuGtgk.Text));

            // 金額値入力の検証
            if (!IsValidCurrency(this.txtKenkoHokenHyojunHoshuGtgk))
            {
                e.Cancel = true;
                this.txtKenkoHokenHyojunHoshuGtgk.SelectAll();
            }

            // フォーマット
            this.txtKenkoHokenHyojunHoshuGtgk.Text = Util.FormatNum(this.txtKenkoHokenHyojunHoshuGtgk.Text);
        }

        /// <summary>
        /// 健康保険料の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKenkoHokenryo_Validating(object sender, CancelEventArgs e)
        {
            // 入力値補正
            this.txtKenkoHokenryo.Text = Util.ToString(Util.ToDecimal(this.txtKenkoHokenryo.Text));

            // 金額値入力の検証
            if (!IsValidCurrency(this.txtKenkoHokenryo))
            {
                e.Cancel = true;
                this.txtKenkoHokenryo.SelectAll();
            }

            // フォーマット
            this.txtKenkoHokenryo.Text = Util.FormatNum(this.txtKenkoHokenryo.Text);

        }

        /// <summary>
        /// 健康保険賞与計算区分の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKenkoHokenShoyoKeisanKubun_Validating(object sender, CancelEventArgs e)
        {
            // 定義外の値を補正
            this.txtKenkoHokenShoyoKeisanKubun.Text = FixDefinedCode(this.txtKenkoHokenShoyoKeisanKubun.Text, 1);
        }

        /// <summary>
        /// 介護保険区分の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKaigoHokenKubun_Validating(object sender, CancelEventArgs e)
        {
            // 定義外の値を補正
            this.txtKaigoHokenKubun.Text = FixDefinedCode(this.txtKaigoHokenKubun.Text, 1);
        }

        /// <summary>
        /// 年金番号の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKoseiNenkinBango_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidKoseiNenkinBango())
            {
                e.Cancel = true;
                this.txtKoseiNenkinBango.SelectAll();
            }
        }

        /// <summary>
        /// 厚生年金報酬月額の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKoseiNenkinHyojunHoshuGtgk_Validating(object sender, CancelEventArgs e)
        {
            // 入力値補正
            this.txtKoseiNenkinHyojunHoshuGtgk.Text = Util.ToString(Util.ToDecimal(this.txtKoseiNenkinHyojunHoshuGtgk.Text));

            // 金額値入力の検証
            if (!IsValidCurrency(this.txtKoseiNenkinHyojunHoshuGtgk))
            {
                e.Cancel = true;
                this.txtKoseiNenkinHyojunHoshuGtgk.SelectAll();
            }

            // フォーマット
            this.txtKoseiNenkinHyojunHoshuGtgk.Text = Util.FormatNum(this.txtKoseiNenkinHyojunHoshuGtgk.Text);

        }

        /// <summary>
        /// 厚生年金保険料の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKoseiNenkinHokenryo_Validating(object sender, CancelEventArgs e)
        {
            // 入力値補正
            this.txtKoseiNenkinHokenryo.Text = Util.ToString(Util.ToDecimal(this.txtKoseiNenkinHokenryo.Text));

            // 金額値入力の検証
            if (!IsValidCurrency(this.txtKoseiNenkinHokenryo))
            {
                e.Cancel = true;
                this.txtKoseiNenkinHokenryo.SelectAll();
            }

            // フォーマット
            this.txtKoseiNenkinHokenryo.Text = Util.FormatNum(this.txtKoseiNenkinHokenryo.Text);

        }

        /// <summary>
        /// 厚生年金賞与計算区分の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKoseiNenkinShoyoKeisanKbn_Validating(object sender, CancelEventArgs e)
        {
            // 定義外の値を補正
            this.txtKoseiNenkinShoyoKeisanKbn.Text = FixDefinedCode(this.txtKoseiNenkinShoyoKeisanKbn.Text, 1);
        }

        /// <summary>
        /// 雇用保険計算区分の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKoyoHokenKeisanKubun_Validating(object sender, CancelEventArgs e)
        {
            // 定義外の値を補正
            this.txtKoyoHokenKeisanKubun.Text = FixDefinedCode(this.txtKoyoHokenKeisanKubun.Text, 1);
        }

        /// <summary>
        /// 市町村コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtJuminzeiShichosonCd_Validating(object sender, CancelEventArgs e)
        {
            // 入力値補正
            this.txtJuminzeiShichosonCd.Text = Util.ToString(Util.ToDecimal(this.txtJuminzeiShichosonCd.Text));

            // 数字のみの入力を許可・未入力はNG
            if (!IsValidJuminzeiShichosonCd())
            {
                e.Cancel = true;
                this.txtJuminzeiShichosonCd.SelectAll();
            }

            if (this.txtJuminzeiShichosonCd.Modified || this.txtJuminzeiShichosonCd.Text == "0")
            {
                // 名称セット
                this.lblJuminzeiShichosonNm.Text =
                    this.Dba.GetName(this.UInfo, "TB_KY_SHICHOSON", " ", Util.ToString(this.txtJuminzeiShichosonCd.Text));

                this.txtJuminzeiShichosonCd.Modified = false;
            }
        }

        /// <summary>
        /// 住民税６月分の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtJuminzei6gatsubun_Validating(object sender, CancelEventArgs e)
        {
            // 入力値補正
            this.txtJuminzei6gatsubun.Text = Util.ToString(Util.ToDecimal(this.txtJuminzei6gatsubun.Text));

            // 金額値入力の検証
            if (!IsValidCurrency(this.txtJuminzei6gatsubun))
            {
                e.Cancel = true;
                this.txtJuminzei6gatsubun.SelectAll();
            }

            // フォーマット
            this.txtJuminzei6gatsubun.Text = Util.FormatNum(this.txtJuminzei6gatsubun.Text);
        }

        /// <summary>
        /// 住民税７月以降の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtJuminzei7gatsubunIko_Validating(object sender, CancelEventArgs e)
        {
            // 入力値補正
            this.txtJuminzei7gatsubunIko.Text = Util.ToString(Util.ToDecimal(this.txtJuminzei7gatsubunIko.Text));

            // 金額値入力の検証
            if (!IsValidCurrency(this.txtJuminzei7gatsubunIko))
            {
                e.Cancel = true;
                this.txtJuminzei7gatsubunIko.SelectAll();
            }

            // フォーマット
            this.txtJuminzei7gatsubunIko.Text = Util.FormatNum(this.txtJuminzei7gatsubunIko.Text);
        }

        /// <summary>
        /// 税額表の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtZeigakuHyo_Validating(object sender, CancelEventArgs e)
        {
            // 定義外の値を補正
            this.txtZeigakuHyo.Text = FixDefinedCode(this.txtZeigakuHyo.Text, 2);
        }

        /// <summary>
        /// 年末調整の別の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtNenmatsuChosei_Validating(object sender, CancelEventArgs e)
        {
            // 定義外の値を補正
            this.txtNenmatsuChosei.Text = FixDefinedCode(this.txtNenmatsuChosei.Text, 1);
        }

        /// <summary>
        /// 本人区分①の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtHonninKubun1_Validating(object sender, CancelEventArgs e)
        {
            // 入力値補正
            this.txtHonninKubun1.Text = Util.ToString(Util.ToDecimal(this.txtHonninKubun1.Text));

            // 数字のみの入力を許可・未入力はNG
            if (!IsValidHonninKubun1())
            {
                e.Cancel = true;
                this.txtHonninKubun1.SelectAll();
            }

            if (this.txtHonninKubun1.Modified || this.txtHonninKubun1.Text == "0")
            {
                // 名称セット
                this.lblHonninKubun1Nm.Text =
                    this.Dba.GetKyuyoKbnNm("2", Util.ToString(this.txtHonninKubun1.Text));

                this.txtHonninKubun1.Modified = false;
            }
        }

        /// <summary>
        /// 本人区分②の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtHonninKubun2_Validating(object sender, CancelEventArgs e)
        {
            // 入力値補正
            this.txtHonninKubun2.Text = Util.ToString(Util.ToDecimal(this.txtHonninKubun2.Text));

            // 数字のみの入力を許可・未入力はNG
            if (!IsValidHonninKubun2())
            {
                e.Cancel = true;
                this.txtHonninKubun2.SelectAll();
            }

            if (this.txtHonninKubun2.Modified || this.txtHonninKubun2.Text == "0")
            {
                // 名称セット
                this.lblHonninKubun2Nm.Text =
                    this.Dba.GetKyuyoKbnNm("3", Util.ToString(this.txtHonninKubun2.Text));

                this.txtHonninKubun2.Modified = false;
            }
        }

        /// <summary>
        /// 本人区分③の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtHonninKubun3_Validating(object sender, CancelEventArgs e)
        {
            // 入力値補正
            this.txtHonninKubun3.Text = Util.ToString(Util.ToDecimal(this.txtHonninKubun3.Text));

            // 数字のみの入力を許可・未入力はNG
            if (!IsValidHonninKubun3())
            {
                e.Cancel = true;
                this.txtHonninKubun3.SelectAll();
            }

            if (this.txtHonninKubun3.Modified || this.txtHonninKubun3.Text == "0")
            {
                // 名称セット
                this.lblHonninKubun3Nm.Text =
                    this.Dba.GetKyuyoKbnNm("4", Util.ToString(this.txtHonninKubun3.Text));

                this.txtHonninKubun3.Modified = false;
            }
        }

        /// <summary>
        /// 未成年者の別の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMiseinensha_Validating(object sender, CancelEventArgs e)
        {
            // 定義外の値を補正
            this.txtMiseinensha.Text = FixDefinedCode(this.txtMiseinensha.Text, 1);
        }

        /// <summary>
        /// 死亡退職の別の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShiboTaishoku_Validating(object sender, CancelEventArgs e)
        {
            // 定義外の値を補正
            this.txtShiboTaishoku.Text = FixDefinedCode(this.txtShiboTaishoku.Text, 1);
        }

        /// <summary>
        /// 災害者の別の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSaigaisha_Validating(object sender, CancelEventArgs e)
        {
            // 定義外の値を補正
            this.txtSaigaisha.Text = FixDefinedCode(this.txtSaigaisha.Text, 1);
        }

        /// <summary>
        /// 外国人の別の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGaikokujin_Validating(object sender, CancelEventArgs e)
        {
            // 定義外の値を補正
            this.txtGaikokujin.Text = FixDefinedCode(this.txtGaikokujin.Text, 1);
        }

        /// <summary>
        /// 配偶者区分の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtHaigushaKubun_Validating(object sender, CancelEventArgs e)
        {
            // 入力値補正
            this.txtHaigushaKubun.Text = Util.ToString(Util.ToDecimal(this.txtHaigushaKubun.Text));

            // 数字のみの入力を許可・未入力はNG
            if (!IsValidHaigushaKubun())
            {
                e.Cancel = true;
                this.txtHaigushaKubun.SelectAll();
            }

            if (this.txtHaigushaKubun.Modified || this.txtHaigushaKubun.Text == "0")
            {
                // 名称セット
                this.lblHaigushaKubunNm.Text =
                    this.Dba.GetKyuyoKbnNm("5", Util.ToString(this.txtHaigushaKubun.Text));

                this.txtHaigushaKubun.Modified = false;
            }
        }

        /// <summary>
        /// 控除対象扶養の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFuyoIppan_Validating(object sender, CancelEventArgs e)
        {
            // 入力値補正
            this.txtFuyoIppan.Text = Util.ToString(Util.ToDecimal(this.txtFuyoIppan.Text));

            // 人数値入力の検証
            if (!IsValidNumber(this.txtFuyoIppan))
            {
                e.Cancel = true;
                this.txtFuyoIppan.SelectAll();
            }
        }

        /// <summary>
        /// 特定扶養親族の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFuyoTokutei_Validating(object sender, CancelEventArgs e)
        {
            // 入力値補正
            this.txtFuyoTokutei.Text = Util.ToString(Util.ToDecimal(this.txtFuyoTokutei.Text));

            // 人数値入力の検証
            if (!IsValidNumber(this.txtFuyoTokutei))
            {
                e.Cancel = true;
                this.txtFuyoTokutei.SelectAll();
            }
        }

        /// <summary>
        /// 同居老親等以外の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFuyoRojinDokyoRoshintoIgai_Validating(object sender, CancelEventArgs e)
        {
            // 入力値補正
            this.txtFuyoRojinDokyoRoshintoIgai.Text = Util.ToString(Util.ToDecimal(this.txtFuyoRojinDokyoRoshintoIgai.Text));

            // 人数値入力の検証
            if (!IsValidNumber(this.txtFuyoRojinDokyoRoshintoIgai))
            {
                e.Cancel = true;
                this.txtFuyoRojinDokyoRoshintoIgai.SelectAll();
            }
        }

        /// <summary>
        /// 同居老親等の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFuyoRojinDokyoRoshinto_Validating(object sender, CancelEventArgs e)
        {
            // 入力値補正
            this.txtFuyoRojinDokyoRoshinto.Text = Util.ToString(Util.ToDecimal(this.txtFuyoRojinDokyoRoshinto.Text));

            // 人数値入力の検証
            if (!IsValidNumber(this.txtFuyoRojinDokyoRoshinto))
            {
                e.Cancel = true;
                this.txtFuyoRojinDokyoRoshinto.SelectAll();
            }
        }

        /// <summary>
        /// １６歳未満の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSai16Miman_Validating(object sender, CancelEventArgs e)
        {
            // 入力値補正
            this.txtSai16Miman.Text = Util.ToString(Util.ToDecimal(this.txtSai16Miman.Text));

            // 人数値入力の検証
            if (!IsValidNumber(this.txtSai16Miman))
            {
                e.Cancel = true;
                this.txtSai16Miman.SelectAll();
            }
        }

        /// <summary>
        /// 同居特別障害者の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFuyoDokyoTokushoIppan_Validating(object sender, CancelEventArgs e)
        {
            // 入力値補正
            this.txtFuyoDokyoTokushoIppan.Text = Util.ToString(Util.ToDecimal(this.txtFuyoDokyoTokushoIppan.Text));

            // 人数値入力の検証
            if (!IsValidNumber(this.txtFuyoDokyoTokushoIppan))
            {
                e.Cancel = true;
                this.txtFuyoDokyoTokushoIppan.SelectAll();
            }
        }

        /// <summary>
        /// 一般障害者の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShogaishaIppan_Validating(object sender, CancelEventArgs e)
        {
            // 入力値補正
            this.txtShogaishaIppan.Text = Util.ToString(Util.ToDecimal(this.txtShogaishaIppan.Text));

            // 人数値入力の検証
            if (!IsValidNumber(this.txtShogaishaIppan))
            {
                e.Cancel = true;
                this.txtShogaishaIppan.SelectAll();
            }
        }

        /// <summary>
        /// 特別障害者の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShogaishaTokubetsu_Validating(object sender, CancelEventArgs e)
        {
            // 入力値補正
            this.txtShogaishaTokubetsu.Text = Util.ToString(Util.ToDecimal(this.txtShogaishaTokubetsu.Text));

            // 人数値入力の検証
            if (!IsValidNumber(this.txtShogaishaTokubetsu))
            {
                e.Cancel = true;
                this.txtShogaishaTokubetsu.SelectAll();
            }
        }

        /// <summary>
        /// 源泉徴収票・摘要①の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTekiyo1_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidTekiyo1())
            {
                e.Cancel = true;
                this.txtTekiyo1.SelectAll();
            }
        }

        /// <summary>
        /// 源泉徴収票・摘要②の値チェック処理前職備考欄の入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTekiyo2_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidTekiyo2())
            {
                e.Cancel = true;
                this.txtTekiyo2.SelectAll();
            }
        }

        /// <summary>
        /// 前職備考欄の入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtZenBikou_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidZenshokuBikou())
            {
                e.Cancel = true;
                this.txtZenBikou.SelectAll();
            }
        }

        /// <summary>
        /// 扶養親族5人目以降の個人番号の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtIkouNo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidIkouNo())
            {
                e.Cancel = true;
                this.txtIkouNo.SelectAll();
            }
        }

        /// <summary>
        /// 16歳未満5人目以降の個人番号の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtIkouMimanNo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidMimanIkouNo())
            {
                e.Cancel = true;
                this.txtIkouMimanNo.SelectAll();
            }
        }

        /// <summary>
        /// 源泉徴収票・種別の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKyuyoShubetsu_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidKyuyoShubetsu())
            {
                e.Cancel = true;
                this.txtKyuyoShubetsu.SelectAll();
            }
        }

        /// <summary>
        /// 支給1・支給方法の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKyuyoShikyuHoho1_Validating(object sender, CancelEventArgs e)
        {
            // 定義外の値を補正
            this.txtKyuyoShikyuHoho1.Text = FixDefinedCode(this.txtKyuyoShikyuHoho1.Text, 2);

            // 金額値入力の検証
            if (!IsValidNumber(this.txtKyuyoShikyuHoho1))
            {
                e.Cancel = true;
                this.txtKyuyoShikyuHoho1.SelectAll();
            }
        }

        /// <summary>
        /// 支給1・支給率の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKyuyoShikyuRitsu1_Validating(object sender, CancelEventArgs e)
        {
            // 定義外の値を補正
            this.txtKyuyoShikyuRitsu1.Text = FixDefinedCode(this.txtKyuyoShikyuRitsu1.Text, 100);
            // 入力値補正
            this.txtKyuyoShikyuRitsu1.Text = Util.ToString(Util.ToDecimal(this.txtKyuyoShikyuRitsu1.Text));

            // 金額値入力の検証
            if (!IsValidCurrency(this.txtKyuyoShikyuRitsu1))
            {
                e.Cancel = true;
                this.txtKyuyoShikyuRitsu1.SelectAll();
            }
        }

        /// <summary>
        /// 支給1・支給額の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKyuyoShikyuKingaku1_Validating(object sender, CancelEventArgs e)
        {
            // 入力値補正
            this.txtKyuyoShikyuKingaku1.Text = Util.ToString(Util.ToDecimal(this.txtKyuyoShikyuKingaku1.Text));

            // 金額値入力の検証
            if (!IsValidCurrency(this.txtKyuyoShikyuKingaku1))
            {
                e.Cancel = true;
                this.txtKyuyoShikyuKingaku1.SelectAll();
            }

            // フォーマット
            this.txtKyuyoShikyuKingaku1.Text = Util.FormatNum(this.txtKyuyoShikyuKingaku1.Text);
        }

        /// <summary>
        /// 支給1・銀行コードの値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKyuyoGinkoCd1_Validating(object sender, CancelEventArgs e)
        {
            // 入力値補正
            this.txtKyuyoGinkoCd1.Text = Util.ToString(Util.ToDecimal(this.txtKyuyoGinkoCd1.Text));

            // 数字のみの入力を許可・未入力はNG
            if (!IsValidGinkoCd(this.txtKyuyoGinkoCd1))
            {
                e.Cancel = true;
                this.txtKyuyoGinkoCd1.SelectAll();
            }

            if (this.txtKyuyoGinkoCd1.Modified || this.txtKyuyoGinkoCd1.Text == "0")
            {
                // 名称セット
                this.lblKyuyoGinkoNm1.Text =
                    this.Dba.GetName(this.UInfo, "TB_KY_GINKO", "", Util.ToString(this.txtKyuyoGinkoCd1.Text));
                this.txtKyuyoShitenCd1.Text = "0";
                this.lblKyuyoShitenNm1.Text = "";

                this.txtKyuyoGinkoCd1.Modified = false;
            }
        }

        /// <summary>
        /// 支給1・支店コードの値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKyuyoShitenCd1_Validating(object sender, CancelEventArgs e)
        {
            // 入力値補正
            this.txtKyuyoShitenCd1.Text = Util.ToString(Util.ToDecimal(this.txtKyuyoShitenCd1.Text));

            // 数字のみの入力を許可
            if (!IsValidShitenCd(this.txtKyuyoGinkoCd1, this.txtKyuyoShitenCd1))
            {
                e.Cancel = true;
                this.txtKyuyoShitenCd1.SelectAll();
            }

            if (this.txtKyuyoShitenCd1.Modified || this.txtKyuyoShitenCd1.Text == "0")
            {
                // 名称セット
                this.lblKyuyoShitenNm1.Text = this.Dba.GetGinkoShitenNm(
                    Util.ToString(this.txtKyuyoGinkoCd1.Text), Util.ToString(this.txtKyuyoShitenCd1.Text));

                this.txtKyuyoShitenCd1.Modified = false;
            }
        }

        /// <summary>
        /// 支給1・預金種目の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKyuyoYokinShumoku1_Validating(object sender, CancelEventArgs e)
        {
            // 定義外の値を補正
            this.txtKyuyoYokinShumoku1.Text = FixDefinedCode(this.txtKyuyoYokinShumoku1.Text, 2);

            // 金額値入力の検証
            if (!IsValidNumber(this.txtKyuyoYokinShumoku1))
            {
                e.Cancel = true;
                this.txtKyuyoYokinShumoku1.SelectAll();
            }
        }

        /// <summary>
        /// 支給1・口座番号の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKyuyoKozaBango1_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidKozaBango(this.txtKyuyoKozaBango1))
            {
                e.Cancel = true;
                this.txtKyuyoKozaBango1.SelectAll();
            }
        }

        /// <summary>
        /// 支給1・名義人の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKyuyoKozaMeigininKanji1_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidKozaMeigininKanji(this.txtKyuyoKozaMeigininKanji1))
            {
                e.Cancel = true;
                this.txtKyuyoKozaMeigininKanji1.SelectAll();
            }
        }

        /// <summary>
        /// 支給1・名義人カナの値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKyuyoKozaMeigininKana1_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidKozaMeigininKana(this.txtKyuyoKozaMeigininKana1))
            {
                e.Cancel = true;
                this.txtKyuyoKozaMeigininKana1.SelectAll();
            }
        }

        /// <summary>
        /// 支給1・契約者番号の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKyuyoKeiyakushaBango1_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidKeiyakushaBango(this.txtKyuyoKeiyakushaBango1))
            {
                e.Cancel = true;
                this.txtKyuyoKeiyakushaBango1.SelectAll();
            }
        }

        /// <summary>
        /// 支給2・支給方法の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKyuyoShikyuHoho2_Validating(object sender, CancelEventArgs e)
        {
            // 定義外の値を補正
            this.txtKyuyoShikyuHoho2.Text = FixDefinedCode(this.txtKyuyoShikyuHoho2.Text, 2);

            // 金額値入力の検証
            if (!IsValidNumber(this.txtKyuyoShikyuHoho2))
            {
                e.Cancel = true;
                this.txtKyuyoShikyuHoho2.SelectAll();
            }
        }

        /// <summary>
        /// 支給2・支給率の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKyuyoShikyuRitsu2_Validating(object sender, CancelEventArgs e)
        {
            // 定義外の値を補正
            this.txtKyuyoShikyuRitsu2.Text = FixDefinedCode(this.txtKyuyoShikyuRitsu2.Text, 100);

            // 入力値補正
            this.txtKyuyoShikyuRitsu2.Text = Util.ToString(Util.ToDecimal(this.txtKyuyoShikyuRitsu2.Text));

            // 金額値入力の検証
            if (!IsValidCurrency(this.txtKyuyoShikyuRitsu2))
            {
                e.Cancel = true;
                this.txtKyuyoShikyuRitsu2.SelectAll();
            }
        }

        /// <summary>
        /// 支給2・支給額の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKyuyoShikyuKingaku2_Validating(object sender, CancelEventArgs e)
        {
            // 入力値補正
            this.txtKyuyoShikyuKingaku2.Text = Util.ToString(Util.ToDecimal(this.txtKyuyoShikyuKingaku2.Text));

            // 金額値入力の検証
            if (!IsValidCurrency(this.txtKyuyoShikyuKingaku2))
            {
                e.Cancel = true;
                this.txtKyuyoShikyuKingaku2.SelectAll();
            }

            // フォーマット
            this.txtKyuyoShikyuKingaku2.Text = Util.FormatNum(this.txtKyuyoShikyuKingaku2.Text);
        }

        /// <summary>
        /// 支給2・銀行コードの値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKyuyoGinkoCd2_Validating(object sender, CancelEventArgs e)
        {
            // 入力値補正
            this.txtKyuyoGinkoCd2.Text = Util.ToString(Util.ToDecimal(this.txtKyuyoGinkoCd2.Text));

            // 数字のみの入力を許可・未入力はNG
            if (!IsValidGinkoCd(this.txtKyuyoGinkoCd2))
            {
                e.Cancel = true;
                this.txtKyuyoGinkoCd2.SelectAll();
            }

            if (this.txtKyuyoGinkoCd2.Modified || this.txtKyuyoGinkoCd2.Text == "0")
            {
                // 名称セット
                this.lblKyuyoGinkoNm2.Text =
                    this.Dba.GetName(this.UInfo, "TB_KY_GINKO", " ", Util.ToString(this.txtKyuyoGinkoCd2.Text));
                this.txtKyuyoShitenCd2.Text = "0";
                this.lblKyuyoShitenNm2.Text = "";

                this.txtKyuyoGinkoCd2.Modified = false;
            }
        }

        /// <summary>
        /// 支給2・支店コードの値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKyuyoShitenCd2_Validating(object sender, CancelEventArgs e)
        {
            // 入力値補正
            this.txtKyuyoShitenCd2.Text = Util.ToString(Util.ToDecimal(this.txtKyuyoShitenCd2.Text));

            // 数字のみの入力を許可・未入力はNG
            if (!IsValidShitenCd(this.txtKyuyoGinkoCd2, this.txtKyuyoShitenCd2))
            {
                e.Cancel = true;
                this.txtKyuyoShitenCd2.SelectAll();
            }

            if (this.txtKyuyoShitenCd2.Modified || this.txtKyuyoShitenCd2.Text == "0")
            {
                // 名称セット
                this.lblKyuyoShitenNm2.Text = this.Dba.GetGinkoShitenNm(
                    Util.ToString(this.txtKyuyoGinkoCd2.Text), Util.ToString(this.txtKyuyoShitenCd2.Text));

                this.txtKyuyoShitenCd2.Modified = false;
            }
        }

        /// <summary>
        /// 支給2・預金種目の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKyuyoYokinShumoku2_Validating(object sender, CancelEventArgs e)
        {
            // 定義外の値を補正
            this.txtKyuyoYokinShumoku2.Text = FixDefinedCode(this.txtKyuyoYokinShumoku2.Text, 2);

            // 金額値入力の検証
            if (!IsValidNumber(this.txtKyuyoYokinShumoku2))
            {
                e.Cancel = true;
                this.txtKyuyoYokinShumoku2.SelectAll();
            }
        }

        /// <summary>
        /// 支給2・口座番号の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKyuyoKozaBango2_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidKozaBango(this.txtKyuyoKozaBango2))
            {
                e.Cancel = true;
                this.txtKyuyoKozaBango2.SelectAll();
            }
        }

        /// <summary>
        /// 支給2・名義人の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKyuyoKozaMeigininKanji2_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidKozaMeigininKanji(this.txtKyuyoKozaMeigininKanji2))
            {
                e.Cancel = true;
                this.txtKyuyoKozaMeigininKanji1.SelectAll();
            }
        }

        /// <summary>
        /// 支給2・名義人カナの値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKyuyoKozaMeigininKana2_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidKozaMeigininKana(this.txtKyuyoKozaMeigininKana2))
            {
                e.Cancel = true;
                this.txtKyuyoKozaMeigininKana1.SelectAll();
            }
        }

        /// <summary>
        /// 支給2・契約者番号の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKyuyoKeiyakushaBango2_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidKeiyakushaBango(this.txtKyuyoKeiyakushaBango2))
            {
                e.Cancel = true;
                this.txtKyuyoKeiyakushaBango1.SelectAll();
            }
        }

        /// <summary>
        /// 画面の最後のコントールでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LastControl_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.btnF6.Enabled)
            {
                if (this.tabPages.TabPages.Count > tabPages.SelectedIndex + 1)
                {
                    tabPages.SelectedIndex += 1;
                }
                else
                {
                    this.PressF6();
                }
            }
        }
        #endregion

        #region イベント(DataGridView)

        /// <summary>
        /// グリッド共通KeyPressイベントハンドラ
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < '0' || '9' < e.KeyChar) && e.KeyChar != '\b')
                e.Handled = true;
        }

        /// <summary>
        /// グリッド共通EditingControlShowingイベントハンドラ
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            //表示されているコントロールがDataGridViewTextBoxEditingControlか調べる
            if (e.Control is DataGridViewTextBoxEditingControl)
            {
                DataGridView dgv = (DataGridView)sender;

                //編集のために表示されているコントロールを取得
                DataGridViewTextBoxEditingControl tb =
                    (DataGridViewTextBoxEditingControl)e.Control;

                //イベントハンドラを削除
                tb.KeyPress -=
                    new KeyPressEventHandler(dgv_KeyPress);

                //該当する列か調べる
                if (dgv.CurrentCell.OwningColumn.Name == "TEATE_KOMOKU")
                {
                    //KeyPressイベントハンドラを追加
                    tb.KeyPress +=
                        new KeyPressEventHandler(dgv_KeyPress);
                }
            }
        }

        /// <summary>
        /// 支給項目グリッドセル値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvShikyu_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            switch (dgvShikyu.Columns[e.ColumnIndex].Name)
            {
                case "TEATE_KOMOKU":
                    if (!IsValidTeateCurrency(e.FormattedValue))
                    {
                        e.Cancel = true;
                        return;
                    }
                    break;
                default:
                    // NONE
                    break;
            }

            dgvShikyu.EndEdit();
            CalcTeateTotal();
        }

        /// <summary>
        /// 支給項目グリッドEnter時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvShikyu_Enter(object sender, EventArgs e)
        {
            this.dgvShikyu.Focus();
            this.dgvShikyu.CurrentCell = this.dgvShikyu[2, dgvShikyu.CurrentRow.Index];
        }

        /// <summary>
        /// 控除項目グリッドセル値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvKojo_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            switch (dgvKojo.Columns[e.ColumnIndex].Name)
            {
                case "TEATE_KOMOKU":
                    if (!IsValidTeateCurrency(e.FormattedValue))
                    {
                        e.Cancel = true;
                        return;
                    }
                    break;
                default:
                    // NONE
                    break;
            }
            dgvShikyu.EndEdit();
            CalcTeateTotal();
        }

        /// <summary>
        /// 控除項目グリッドEnter時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvKojo_Enter(object sender, EventArgs e)
        {
            this.dgvKojo.Focus();
            this.dgvKojo.CurrentCell = this.dgvKojo[2, dgvKojo.CurrentRow.Index];
        }

        #endregion

        #region privateメソッド

        /// <summary>
        /// 諸手当グリッド初期化
        /// </summary>
        private void InitTeateGridView()
        {
            DbParamCollection dpc;
            StringBuilder sql;
            DataTable dt;

            // グリッド列スタイル
            DataGridViewCellStyle noStyle = new DataGridViewCellStyle();            // 番号列
            noStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            DataGridViewCellStyle curStyle = new DataGridViewCellStyle();           // 金額列
            curStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            curStyle.Format = "#,###";

            #region 支給項目グリッド

            // グリッドデータソース定義
            DataTable dtShikyu = new DataTable();
            dtShikyu.Columns.Add("KOMOKU_BANGO", Type.GetType("System.Decimal"));   // 項目番号
            dtShikyu.Columns.Add("KOMOKU_NM", Type.GetType("System.String"));       // 項目名
            dtShikyu.Columns.Add("TEATE_KOMOKU", Type.GetType("System.Decimal"));   // 手当項目

            // 社員手当項目設定データ参照
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            sql = new StringBuilder();
            sql.Append("SELECT");
            sql.Append(" KAISHA_CD");
            sql.Append(",KOMOKU_SHUBETSU");
            sql.Append(",KOMOKU_BANGO");
            sql.Append(",KOMOKU_NM");
            sql.Append(",KOMOKU_KUBUN1");
            sql.Append(",KOMOKU_KUBUN2");
            sql.Append(",KOMOKU_KUBUN3");
            sql.Append(",KOMOKU_KUBUN4");
            sql.Append(",KOMOKU_KUBUN5");
            sql.Append(",REGIST_DATE");
            sql.Append(",UPDATE_DATE");
            sql.Append(" FROM");
            sql.Append(" TB_KY_SHAIN_TEATE_KOMOKU_STI");
            sql.Append(" WHERE");
            sql.Append(" KAISHA_CD = @KAISHA_CD");
            sql.Append(" AND KOMOKU_SHUBETSU = 1");
            sql.Append(" ORDER BY");
            sql.Append(" KOMOKU_BANGO ASC");
            dt = this.Dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);
            // データ行追加
            foreach (DataRow dr in dt.Rows)
            {
                DataRow gdr = dtShikyu.NewRow();
                gdr["KOMOKU_BANGO"] = Util.ToDecimal(dr["KOMOKU_BANGO"]);
                gdr["KOMOKU_NM"] = Util.ToString(dr["KOMOKU_NM"]);
                gdr["TEATE_KOMOKU"] = 0;

                dtShikyu.Rows.Add(gdr);
            }

            // グリッドへデータソース設定
            this.dgvShikyu.DataSource = dtShikyu;

            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvShikyu.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // フォントを設定する
            this.dgvShikyu.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F, FontStyle.Regular);
            this.dgvShikyu.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.dgvShikyu.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 12F);
            this.dgvShikyu.AlternatingRowsDefaultCellStyle.BackColor = Color.Aquamarine;

            // 編集設定
            this.dgvShikyu.SelectionMode = DataGridViewSelectionMode.CellSelect;
            this.dgvShikyu.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.dgvShikyu.ReadOnly = false;
            this.dgvShikyu.EditMode = DataGridViewEditMode.EditOnEnter;
            this.dgvShikyu.Columns[0].ReadOnly = true;
            this.dgvShikyu.Columns[1].ReadOnly = true;

            // 列設定
            this.dgvShikyu.Columns[0].HeaderText = "NO";
            this.dgvShikyu.Columns[0].DefaultCellStyle = noStyle;
            this.dgvShikyu.Columns[0].Width = 30;
            this.dgvShikyu.Columns[1].HeaderText = "項目名";
            this.dgvShikyu.Columns[1].Width = 150;
            this.dgvShikyu.Columns[2].HeaderText = "金　　額";
            this.dgvShikyu.Columns[2].DefaultCellStyle = curStyle;
            this.dgvShikyu.Columns[2].Width = 100;

            #endregion

            #region 控除項目グリッド

            // グリッドデータソース定義
            DataTable dtKojo = new DataTable();
            dtKojo.Columns.Add("KOMOKU_BANGO", Type.GetType("System.Decimal"));     // 項目番号
            dtKojo.Columns.Add("KOMOKU_NM", Type.GetType("System.String"));         // 項目名
            dtKojo.Columns.Add("TEATE_KOMOKU", Type.GetType("System.Decimal"));     // 手当項目

            // 社員手当項目設定データ参照
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            sql = new StringBuilder();
            sql.Append("SELECT");
            sql.Append(" KAISHA_CD");
            sql.Append(",KOMOKU_SHUBETSU");
            sql.Append(",KOMOKU_BANGO");
            sql.Append(",KOMOKU_NM");
            sql.Append(",KOMOKU_KUBUN1");
            sql.Append(",KOMOKU_KUBUN2");
            sql.Append(",KOMOKU_KUBUN3");
            sql.Append(",KOMOKU_KUBUN4");
            sql.Append(",KOMOKU_KUBUN5");
            sql.Append(",REGIST_DATE");
            sql.Append(",UPDATE_DATE");
            sql.Append(" FROM");
            sql.Append(" TB_KY_SHAIN_TEATE_KOMOKU_STI");
            sql.Append(" WHERE");
            sql.Append(" KAISHA_CD = @KAISHA_CD");
            sql.Append(" AND KOMOKU_SHUBETSU = 2");
            sql.Append(" ORDER BY");
            sql.Append(" KOMOKU_BANGO ASC");
            dt = this.Dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);
            // データ行追加
            foreach (DataRow dr in dt.Rows)
            {
                DataRow gdr = dtKojo.NewRow();
                gdr["KOMOKU_BANGO"] = Util.ToDecimal(dr["KOMOKU_BANGO"]);
                gdr["KOMOKU_NM"] = Util.ToString(dr["KOMOKU_NM"]);
                gdr["TEATE_KOMOKU"] = 0;

                dtKojo.Rows.Add(gdr);
            }

            // グリッドへデータソース設定
            this.dgvKojo.DataSource = dtKojo;

            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvKojo.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // フォントを設定する
            this.dgvKojo.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F, FontStyle.Regular);
            this.dgvKojo.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.dgvKojo.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 12F);
            this.dgvKojo.AlternatingRowsDefaultCellStyle.BackColor = Color.Aquamarine;

            // 編集設定
            this.dgvKojo.SelectionMode = DataGridViewSelectionMode.CellSelect;
            this.dgvKojo.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.dgvKojo.ReadOnly = false;
            this.dgvKojo.EditMode = DataGridViewEditMode.EditOnEnter;
            this.dgvKojo.Columns[0].ReadOnly = true;
            this.dgvKojo.Columns[1].ReadOnly = true;

            // 列設定
            this.dgvKojo.Columns[0].HeaderText = "NO";
            this.dgvKojo.Columns[0].DefaultCellStyle = noStyle;
            this.dgvKojo.Columns[0].Width = 30;
            this.dgvKojo.Columns[1].HeaderText = "項目名";
            this.dgvKojo.Columns[1].Width = 150;
            this.dgvKojo.Columns[2].HeaderText = "金　　額";
            this.dgvKojo.Columns[2].DefaultCellStyle = curStyle;
            this.dgvKojo.Columns[2].Width = 100;

            #endregion

            CalcTeateTotal();
        }

        /// <summary>
        /// 手当項目の金額合計
        /// </summary>
        private void CalcTeateTotal()
        {
            decimal total;

            // 支給項目グリッド
            total = 0;
            foreach (DataRow dr in ((DataTable)dgvShikyu.DataSource).Rows)
            {
                total += Util.ToDecimal(Util.ToString(dr["TEATE_KOMOKU"]));
            }
            this.lblShikyuGokei.Text = Util.FormatNum(total);
            // 控除項目グリッド
            total = 0;
            foreach (DataRow dr in ((DataTable)dgvKojo.DataSource).Rows)
            {
                total += Util.ToDecimal(Util.ToString(dr["TEATE_KOMOKU"]));
            }
            this.lblKojoGokei.Text = Util.FormatNum(total);
        }

        /// <summary>
        /// 新規モードの初期表示
        /// </summary>
        private void InitDispOnNew()
        {
            DataRow roginUser = GetroginUserdetail();//ログインしている人のユーザ情報取得
            if (Util.ToInt(roginUser["USER_KUBUN"]).Equals(1))// 管理者権限のみ公開ボタンを表示
            {
                this.button1.Visible = true;
                this.button2.Visible = true;
                this.button3.Visible = true;
                this.button4.Visible = true;
                this.button5.Visible = true;
                this.button6.Visible = true;
                this.button7.Visible = true;
                this.button8.Visible = true;
                this.button9.Visible = true;
                this.button10.Visible = true;
                this.txtMyNumber.ReadOnly = false;
            }
            // 追加・変更状態表示
            this.lblMODE.Text = "【追加】";

            // 社員コードに初期フォーカス
            this.ActiveControl = this.txtShainCd;
            this.txtShainCd.Focus();
        }

        /// <summary>
        /// 編集モードの初期表示
        /// </summary>
        private void InitDispOnEdit()
        {
            DataRow roginUser = GetroginUserdetail();//ログインしている人のユーザ情報取得
            if (Util.ToInt(roginUser["USER_KUBUN"]).Equals(1))// 管理者権限のみ公開ボタンを表示
            {
                #region ボタン入力制御
                this.button1.Visible = true;
                this.button2.Visible = true;
                this.button3.Visible = true;
                this.button4.Visible = true;
                this.button5.Visible = true;
                this.button6.Visible = true;
                this.button7.Visible = true;
                this.button8.Visible = true;
                this.button9.Visible = true;
                this.button10.Visible = true;
                this.txtMyNumber.ReadOnly = false;
                this.txtFuyoHaigushaMyNumber.ReadOnly = false;
                this.txtFuyoMyNumber1.ReadOnly = false;
                this.txtFuyoMyNumber2.ReadOnly = false;
                this.txtFuyoMyNumber3.ReadOnly = false;
                this.txtFuyoMyNumber4.ReadOnly = false;
                this.txtFuyoMimanMyNumber1.ReadOnly = false;
                this.txtFuyoMimanMyNumber2.ReadOnly = false;
                this.txtFuyoMimanMyNumber3.ReadOnly = false;
                this.txtFuyoMimanMyNumber4.ReadOnly = false;
                #endregion
            }

            // 社員データ読込
            DataLoad((string)this.InData);
            // マイナンバー登録
            txtMyNumber.Text = Mynumber.GetmyNumber(Util.ToInt(InData), 1, 1, this.Dba);
            #region 扶養親族用マイナンバー取得処理
            txtFuyoHaigushaMyNumber.Text = Mynumber.GetmyNumber(Util.ToInt(InData), 1, 9, this.Dba);
            txtFuyoMyNumber1.Text = Mynumber.GetmyNumber(Util.ToInt(InData), 1, 1, this.Dba);
            txtFuyoMyNumber2.Text = Mynumber.GetmyNumber(Util.ToInt(InData), 1, 2, this.Dba);
            txtFuyoMyNumber3.Text = Mynumber.GetmyNumber(Util.ToInt(InData), 1, 3, this.Dba);
            txtFuyoMyNumber4.Text = Mynumber.GetmyNumber(Util.ToInt(InData), 1, 4, this.Dba);
            txtFuyoMimanMyNumber1.Text = Mynumber.GetmyNumber(Util.ToInt(InData), 1, 5, this.Dba);
            txtFuyoMimanMyNumber2.Text = Mynumber.GetmyNumber(Util.ToInt(InData), 1, 6, this.Dba);
            txtFuyoMimanMyNumber3.Text = Mynumber.GetmyNumber(Util.ToInt(InData), 1, 7, this.Dba);
            txtFuyoMimanMyNumber4.Text = Mynumber.GetmyNumber(Util.ToInt(InData), 1, 8, this.Dba);
            #endregion

            // 追加・変更状態表示
            this.lblMODE.Text = "【変更】";

            #region マイナンバー表示設定
            if (!ValChk.IsEmpty(Util.ToString(txtMyNumber.Text)))
            {
                //マイナンバーを非表示に（※TextBoxをかぶせる）
                this.txtMyNumber.ReadOnly = true;
                this.FsiTextBox1.Visible = true;
            }
            if (!ValChk.IsEmpty(Util.ToString(txtFuyoHaigushaMyNumber.Text)))
            {
                //マイナンバーを非表示に（※TextBoxをかぶせる）
                this.txtFuyoHaigushaMyNumber.ReadOnly = true;
                this.txtFuyoHaigushaMyNumberDm.Visible = true;
            }
            if (!ValChk.IsEmpty(Util.ToString(txtFuyoMyNumber1.Text)))
            {
                //マイナンバーを非表示に（※TextBoxをかぶせる）
                this.txtFuyoMyNumber1.ReadOnly = true;
                this.txtFuyoMyNumberDm1.Visible = true;
            }
            if (!ValChk.IsEmpty(Util.ToString(txtFuyoMyNumber2.Text)))
            {
                //マイナンバーを非表示に（※TextBoxをかぶせる）
                this.txtFuyoMyNumber2.ReadOnly = true;
                this.txtFuyoMyNumberDm2.Visible = true;
            }
            if (!ValChk.IsEmpty(Util.ToString(txtFuyoMyNumber3.Text)))
            {
                //マイナンバーを非表示に（※TextBoxをかぶせる）
                this.txtFuyoMyNumber3.ReadOnly = true;
                this.txtFuyoMyNumberDm3.Visible = true;
            }
            if (!ValChk.IsEmpty(Util.ToString(txtFuyoMyNumber4.Text)))
            {
                //マイナンバーを非表示に（※TextBoxをかぶせる）
                this.txtFuyoMyNumber4.ReadOnly = true;
                this.txtFuyoMyNumberDm4.Visible = true;
            }
            if (!ValChk.IsEmpty(Util.ToString(txtFuyoMimanMyNumber1.Text)))
            {
                //マイナンバーを非表示に（※TextBoxをかぶせる）
                this.txtFuyoMimanMyNumber1.ReadOnly = true;
                this.txtFuyoMimanMyNumberDm1.Visible = true;
            }
            if (!ValChk.IsEmpty(Util.ToString(txtFuyoMimanMyNumber2.Text)))
            {
                //マイナンバーを非表示に（※TextBoxをかぶせる）
                this.txtFuyoMimanMyNumber2.ReadOnly = true;
                this.txtFuyoMimanMyNumberDm2.Visible = true;
            }
            if (!ValChk.IsEmpty(Util.ToString(txtFuyoMimanMyNumber3.Text)))
            {
                //マイナンバーを非表示に（※TextBoxをかぶせる）
                this.txtFuyoMimanMyNumber3.ReadOnly = true;
                this.txtFuyoMimanMyNumberDm3.Visible = true;
            }
            if (!ValChk.IsEmpty(Util.ToString(txtFuyoMimanMyNumber4.Text)))
            {
                //マイナンバーを非表示に（※TextBoxをかぶせる）
                this.txtFuyoMimanMyNumber4.ReadOnly = true;
                this.txtFuyoMimanMyNumberDm4.Visible = true;
            }
            #endregion

            // 社員コードは入力不可
            this.tabPages.Focus();
            this.lblShainCd.Enabled = false;
            this.txtShainCd.Enabled = false;
        }

        /// <summary>
        /// 社員データの読込
        /// </summary>
        /// <param name="shainCd">社員コード</param>
        private void DataLoad(string shainCd)
        {
            // VI社員情報データ参照
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHAIN_CD", SqlDbType.Decimal, 6, shainCd);
            DataTable dt = this.Dba.GetDataTableFromSqlWithParams(GetShainJohoSql(), dpc);
            if (dt.Rows.Count == 1)
            {
                DataRow dr = dt.Rows[0];
                this.txtShainCd.Text = Util.ToString(dr["SHAIN_CD"]);
                this.lblShainNmDisp.Text = Util.ToString(dr["SHAIN_NM"]);
                // 基本情報ページ
                this.txtShainNm.Text = Util.ToString(dr["SHAIN_NM"]);
                this.txtShainKanaNm.Text = Util.ToString(dr["SHAIN_KANA_NM"]);
                this.txtSeibetsu.Text = Util.ToString(dr["SEIBETSU"]);
                if (!ValChk.IsEmpty(dr["SEINENGAPPI"]))
                    SetJpSeinengappi(Util.ConvJpDate(Util.ToDate(dr["SEINENGAPPI"]), this.Dba));
                this.txtYubinBango1.Text = Util.ToString(dr["YUBIN_BANGO1"]);
                this.txtYubinBango2.Text = Util.ToString(dr["YUBIN_BANGO2"]);
                this.txtJusho1.Text = Util.ToString(dr["JUSHO1"]);
                this.txtJusho2.Text = Util.ToString(dr["JUSHO2"]);
                this.txtDenwaBango.Text = Util.ToString(dr["DENWA_BANGO"]);
                this.txthyoji.Text = Util.ToString(dr["HYOJI_FLG"]); // 表示区分
                this.txtBumonCd.Text = Util.ToString(dr["BUMON_CD"]);
                this.lblBumonNm.Text = Util.ToString(dr["BUMON_NM"]);
                this.txtBukaCd.Text = Util.ToString(dr["BUKA_CD"]);
                this.lblBukaNm.Text = Util.ToString(dr["BUKA_NM"]);
                this.txtYakushokuCd.Text = Util.ToString(dr["YAKUSHOKU_CD"]);
                this.lblYakushokuNm.Text = Util.ToString(dr["YAKUSHOKU_NM"]);
                this.txtKyuyoKeitai.Text = Util.ToString(dr["KYUYO_KEITAI"]);
                this.lblKyuyoKeitaiNm.Text = Util.ToString(dr["KYUYO_KEITAI_NM"]);
                if (!ValChk.IsEmpty(dr["NYUSHA_NENGAPPI"]))
                    SetJpNyushaNengappi(Util.ConvJpDate(Util.ToDate(dr["NYUSHA_NENGAPPI"]), this.Dba));
                if (!ValChk.IsEmpty(dr["TAISHOKU_NENGAPPI"]))
                    SetJpTaishokuNengappi(Util.ConvJpDate(Util.ToDate(dr["TAISHOKU_NENGAPPI"]), this.Dba));
                this.txtZenBikou.Text = Util.ToString(dr["ZENSHOKU_BIKOU"]);
                // 社会保険住民税ページ
                this.txtKenkoHokenshoBango.Text = Util.ToString(dr["KENKO_HOKENSHO_BANGO"]);
                this.txtKenkoHokenHyojunHoshuGtgk.Text = Util.FormatNum(dr["KENKO_HOKEN_HYOJUN_HOSHU_GTGK"]);
                this.txtKenkoHokenryo.Text = Util.FormatNum(dr["KENKO_HOKENRYO"]);
                this.txtKenkoHokenShoyoKeisanKubun.Text = Util.ToString(dr["KENKO_HOKEN_SHOYO_KEISAN_KUBUN"]);
                this.txtKaigoHokenKubun.Text = Util.ToString(dr["KAIGO_HOKEN_KUBUN"]);
                this.txtKoseiNenkinBango.Text = Util.ToString(dr["KOSEI_NENKIN_BANGO"]);
                this.txtKoseiNenkinHyojunHoshuGtgk.Text = Util.ToString(dr["KOSEI_NENKIN_HYOJUN_HOSHU_GTGK"]);
                this.txtKoseiNenkinHokenryo.Text = Util.FormatNum(dr["KOSEI_NENKIN_HOKENRYO"]);
                this.txtKoseiNenkinShoyoKeisanKbn.Text = Util.ToString(dr["KOSEI_NENKIN_SHOYO_KEISAN_KBN"]);
                this.txtKoyoHokenKeisanKubun.Text = Util.ToString(dr["KOYO_HOKEN_KEISAN_KUBUN"]);
                this.txtJuminzeiShichosonCd.Text = Util.ToString(dr["JUMINZEI_SHICHOSON_CD"]);
                this.lblJuminzeiShichosonNm.Text = Util.ToString(dr["JUMINZEI_SHICHOSON_NM"]);
                this.txtJuminzei6gatsubun.Text = Util.FormatNum(dr["JUMINZEI_6GATSUBUN"]);
                this.txtJuminzei7gatsubunIko.Text = Util.FormatNum(dr["JUMINZEI_7GATSUBUN_IKO"]);
                // 所得税ページ
                this.txtZeigakuHyo.Text = Util.ToString(dr["ZEIGAKU_HYO"]);
                this.txtNenmatsuChosei.Text = Util.ToString(dr["NENMATSU_CHOSEI"]);
                this.txtHonninKubun1.Text = Util.ToString(dr["HONNIN_KUBUN1"]);
                this.lblHonninKubun1Nm.Text = Util.ToString(dr["HONNIN_KUBUN1_NM"]);
                this.txtHonninKubun2.Text = Util.ToString(dr["HONNIN_KUBUN2"]);
                this.lblHonninKubun2Nm.Text = Util.ToString(dr["HONNIN_KUBUN2_NM"]);
                this.txtHonninKubun3.Text = Util.ToString(dr["HONNIN_KUBUN3"]);
                this.lblHonninKubun3Nm.Text = Util.ToString(dr["HONNIN_KUBUN3_NM"]);
                this.txtMiseinensha.Text = Util.ToString(dr["MISEINENSHA"]);
                this.txtShiboTaishoku.Text = Util.ToString(dr["SHIBO_TAISHOKU"]);
                this.txtSaigaisha.Text = Util.ToString(dr["SAIGAISHA"]);
                this.txtGaikokujin.Text = Util.ToString(dr["GAIKOKUJIN"]);
                // 世帯情報
                this.txtFuyoHaigushaNm.Text = Util.ToString(dr["FUYO_HAIGUSHA_NM"]);
                this.txtFuyoHaigushaKanaNm.Text = Util.ToString(dr["FUYO_HAIGUSHA_KANA_NM"]);
                this.txtFuyoHaigushaJukyo.Text = Util.ToString(dr["FUYO_HAIGUSHA_JUKYO"]);
                if (!ValChk.IsEmpty(dr["FUYO_HAIGUSHA_SEINENGAPPI"]))
                    SetJpHaigushaSeinengappi(Util.ConvJpDate(Util.ToDate(dr["FUYO_HAIGUSHA_SEINENGAPPI"]), this.Dba));

                this.txtFuyoNm1.Text = Util.ToString(dr["FUYO1_NM"]);
                this.txtFuyoKanaNm1.Text = Util.ToString(dr["FUYO1_KANA_NM"]);
                this.txtFuyoJukyo1.Text = Util.ToString(dr["FUYO1_JUKYO"]);
                if (!ValChk.IsEmpty(dr["FUYO1_SEINENGAPPI"]))
                    SetJpShinzokuSeinengappi1(Util.ConvJpDate(Util.ToDate(dr["FUYO1_SEINENGAPPI"]), this.Dba));

                this.txtFuyoNm2.Text = Util.ToString(dr["FUYO2_NM"]);
                this.txtFuyoKanaNm2.Text = Util.ToString(dr["FUYO2_KANA_NM"]);
                this.txtFuyoJukyo2.Text = Util.ToString(dr["FUYO2_JUKYO"]);
                if (!ValChk.IsEmpty(dr["FUYO2_SEINENGAPPI"]))
                    SetJpShinzokuSeinengappi2(Util.ConvJpDate(Util.ToDate(dr["FUYO2_SEINENGAPPI"]), this.Dba));

                this.txtFuyoNm3.Text = Util.ToString(dr["FUYO3_NM"]);
                this.txtFuyoKanaNm3.Text = Util.ToString(dr["FUYO3_KANA_NM"]);
                this.txtFuyoJukyo3.Text = Util.ToString(dr["FUYO3_JUKYO"]);
                if (!ValChk.IsEmpty(dr["FUYO3_SEINENGAPPI"]))
                    SetJpShinzokuSeinengappi3(Util.ConvJpDate(Util.ToDate(dr["FUYO3_SEINENGAPPI"]), this.Dba));

                this.txtFuyoNm4.Text = Util.ToString(dr["FUYO4_NM"]);
                this.txtFuyoKanaNm4.Text = Util.ToString(dr["FUYO4_KANA_NM"]);
                this.txtFuyoJukyo4.Text = Util.ToString(dr["FUYO4_JUKYO"]);
                if (!ValChk.IsEmpty(dr["FUYO4_SEINENGAPPI"]))
                    SetJpShinzokuSeinengappi4(Util.ConvJpDate(Util.ToDate(dr["FUYO4_SEINENGAPPI"]), this.Dba));

                this.txtIkouNo.Text = Util.ToString(dr["FUYO_BIKOU"]);

                this.txtFuyoMimanNm1.Text = Util.ToString(dr["FUYO1_MIMAN_NM"]);
                this.txtFuyoMimanKanaNm1.Text = Util.ToString(dr["FUYO1_MIMAN_KANA_NM"]);
                this.txtFuyoMimanJukyo1.Text = Util.ToString(dr["FUYO1_MIMAN_JUKYO"]);
                if (!ValChk.IsEmpty(dr["FUYO1_MIMAN_SEINENGAPPI"]))
                    SetJpMimanSeinengappi1(Util.ConvJpDate(Util.ToDate(dr["FUYO1_MIMAN_SEINENGAPPI"]), this.Dba));

                this.txtFuyoMimanNm2.Text = Util.ToString(dr["FUYO2_MIMAN_NM"]);
                this.txtFuyoMimanKanaNm2.Text = Util.ToString(dr["FUYO2_MIMAN_KANA_NM"]);
                this.txtFuyoMimanJukyo2.Text = Util.ToString(dr["FUYO2_MIMAN_JUKYO"]);
                if (!ValChk.IsEmpty(dr["FUYO2_MIMAN_SEINENGAPPI"]))
                    SetJpMimanSeinengappi2(Util.ConvJpDate(Util.ToDate(dr["FUYO2_MIMAN_SEINENGAPPI"]), this.Dba));

                this.txtFuyoMimanNm3.Text = Util.ToString(dr["FUYO3_MIMAN_NM"]);
                this.txtFuyoMimanKanaNm3.Text = Util.ToString(dr["FUYO3_MIMAN_KANA_NM"]);
                this.txtFuyoMimanJukyo3.Text = Util.ToString(dr["FUYO3_MIMAN_JUKYO"]);
                if (!ValChk.IsEmpty(dr["FUYO3_MIMAN_SEINENGAPPI"]))
                    SetJpMimanSeinengappi3(Util.ConvJpDate(Util.ToDate(dr["FUYO3_MIMAN_SEINENGAPPI"]), this.Dba));

                this.txtFuyoMimanNm4.Text = Util.ToString(dr["FUYO4_MIMAN_NM"]);
                this.txtFuyoMimanKanaNm4.Text = Util.ToString(dr["FUYO4_MIMAN_KANA_NM"]);
                this.txtFuyoMimanJukyo4.Text = Util.ToString(dr["FUYO4_MIMAN_JUKYO"]);
                if (!ValChk.IsEmpty(dr["FUYO4_MIMAN_SEINENGAPPI"]))
                    SetJpMimanSeinengappi4(Util.ConvJpDate(Util.ToDate(dr["FUYO4_MIMAN_SEINENGAPPI"]), this.Dba));

                this.txtIkouMimanNo.Text = Util.ToString(dr["FUYO_MIMAN_BIKOU"]);

                this.txtHaigushaKubun.Text = Util.ToString(dr["HAIGUSHA_KUBUN"]);
                this.lblHaigushaKubunNm.Text = Util.ToString(dr["HAIGUSHA_KUBUN_NM"]);
                this.txtFuyoIppan.Text = Util.ToString(dr["FUYO_IPPAN"]);
                this.txtFuyoTokutei.Text = Util.ToString(dr["FUYO_TOKUTEI"]);
                this.txtFuyoRojinDokyoRoshintoIgai.Text = Util.ToString(dr["FUYO_ROJIN_DOKYO_ROSHINTO_IGAI"]);
                this.txtFuyoRojinDokyoRoshinto.Text = Util.ToString(dr["FUYO_ROJIN_DOKYO_ROSHINTO"]);
                this.txtSai16Miman.Text = Util.ToString(dr["SAI16_MIMAN"]);
                this.txtHiJukyoShinzoku.Text = Util.ToString(dr["HIJUKYO_SHINZOKU"]);
                this.txtFuyoDokyoTokushoIppan.Text = Util.ToString(dr["FUYO_DOKYO_TOKUSHO_IPPAN"]);
                this.txtShogaishaIppan.Text = Util.ToString(dr["SHOGAISHA_IPPAN"]);
                this.txtShogaishaTokubetsu.Text = Util.ToString(dr["SHOGAISHA_TOKUBETSU"]);
                this.txtTekiyo1.Text = Util.ToString(dr["TEKIYO1"]);
                this.txtTekiyo2.Text = Util.ToString(dr["TEKIYO2"]);
                this.txtKyuyoShubetsu.Text = Util.ToString(dr["KYUYO_SHUBETSU"]);
                // 諸手当ページ
                foreach (DataGridViewRow dgvr in dgvShikyu.Rows)
                {
                    dgvr.Cells["TEATE_KOMOKU"].Value =
                        Util.ToDecimal(dr["TEATE_SHIKYU_KOMOKU" + Util.ToString(dgvr.Cells["KOMOKU_BANGO"].Value)]);
                }
                foreach (DataGridViewRow dgvr in dgvKojo.Rows)
                {
                    dgvr.Cells["TEATE_KOMOKU"].Value =
                        Util.ToDecimal(dr["TEATE_KOJO_KOMOKU" + Util.ToString(dgvr.Cells["KOMOKU_BANGO"].Value)]);
                }
                // 振込銀行ページ
                this.txtKyuyoShikyuHoho1.Text = Util.ToString(dr["KYUYO_SHIKYU_HOHO1"]);
                this.txtKyuyoShikyuRitsu1.Text = Util.FormatNum(dr["KYUYO_SHIKYU_RITSU1"], 0);
                this.txtKyuyoShikyuKingaku1.Text = Util.FormatNum(dr["KYUYO_SHIKYU_KINGAKU1"]);
                this.txtKyuyoGinkoCd1.Text = Util.ToString(dr["KYUYO_GINKO_CD1"]);
                this.lblKyuyoGinkoNm1.Text = Util.ToString(dr["KYUYO_GINKO_NM1"]);
                this.txtKyuyoShitenCd1.Text = Util.ToString(dr["KYUYO_SHITEN_CD1"]);
                this.lblKyuyoShitenNm1.Text = Util.ToString(dr["KYUYO_SHITEN_NM1"]);
                this.txtKyuyoYokinShumoku1.Text = Util.ToString(dr["KYUYO_YOKIN_SHUMOKU1"]);
                this.txtKyuyoKozaBango1.Text = Util.ToString(dr["KYUYO_KOZA_BANGO1"]);
                this.txtKyuyoKozaMeigininKanji1.Text = Util.ToString(dr["KYUYO_KOZA_MEIGININ_KANJI1"]);
                this.txtKyuyoKozaMeigininKana1.Text = Util.ToString(dr["KYUYO_KOZA_MEIGININ_KANA1"]);
                this.txtKyuyoKeiyakushaBango1.Text = Util.ToString(dr["KYUYO_KEIYAKUSHA_BANGO1"]);
                this.txtKyuyoShikyuHoho2.Text = Util.ToString(dr["KYUYO_SHIKYU_HOHO2"]);
                this.txtKyuyoShikyuRitsu2.Text = Util.FormatNum(dr["KYUYO_SHIKYU_RITSU2"], 0);
                this.txtKyuyoShikyuKingaku2.Text = Util.FormatNum(dr["KYUYO_SHIKYU_KINGAKU2"]);
                this.txtKyuyoGinkoCd2.Text = Util.ToString(dr["KYUYO_GINKO_CD2"]);
                this.lblKyuyoGinkoNm2.Text = Util.ToString(dr["KYUYO_GINKO_NM2"]);
                this.txtKyuyoShitenCd2.Text = Util.ToString(dr["KYUYO_SHITEN_CD2"]);
                this.lblKyuyoShitenNm2.Text = Util.ToString(dr["KYUYO_SHITEN_NM2"]);
                this.txtKyuyoYokinShumoku2.Text = Util.ToString(dr["KYUYO_YOKIN_SHUMOKU2"]);
                this.txtKyuyoKozaBango2.Text = Util.ToString(dr["KYUYO_KOZA_BANGO2"]);
                this.txtKyuyoKozaMeigininKanji2.Text = Util.ToString(dr["KYUYO_KOZA_MEIGININ_KANJI2"]);
                this.txtKyuyoKozaMeigininKana2.Text = Util.ToString(dr["KYUYO_KOZA_MEIGININ_KANA2"]);
                this.txtKyuyoKeiyakushaBango2.Text = Util.ToString(dr["KYUYO_KEIYAKUSHA_BANGO2"]);
            }
        }

        /// <summary>
        /// 社員データの照会SQL文を返す
        /// </summary>
        private string GetShainJohoSql()
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT");
            sql.Append(" KAISHA_CD");
            sql.Append(",SHAIN_CD");
            sql.Append(",SHAIN_NM");
            sql.Append(",SHAIN_KANA_NM");
            sql.Append(",SEIBETSU");
            sql.Append(",SEINENGAPPI");
            sql.Append(",YUBIN_BANGO1");
            sql.Append(",YUBIN_BANGO2");
            sql.Append(",JUSHO1");
            sql.Append(",JUSHO2");
            sql.Append(",DENWA_BANGO");
            sql.Append(",HYOJI_FLG");
            sql.Append(",BUMON_CD");
            sql.Append(",BUMON_NM");
            sql.Append(",BUKA_CD");
            sql.Append(",BUKA_NM");
            sql.Append(",YAKUSHOKU_CD");
            sql.Append(",YAKUSHOKU_NM");
            sql.Append(",KYUYO_KEITAI");
            sql.Append(",KYUYO_KEITAI_NM");
            sql.Append(",NYUSHA_NENGAPPI");
            sql.Append(",TAISHOKU_NENGAPPI");
            sql.Append(",ZENSHOKU_BIKOU");
            sql.Append(",KENKO_HOKENSHO_BANGO");
            sql.Append(",KENKO_HOKEN_TOKYU");
            sql.Append(",KENKO_HOKEN_HYOJUN_HOSHU_GTGK");
            sql.Append(",KENKO_HOKENRYO");
            sql.Append(",KENKO_HOKEN_SHOYO_KEISAN_KUBUN");
            sql.Append(",KOSEI_NENKIN_BANGO");
            sql.Append(",KOSEI_NENKIN_TOKYU");
            sql.Append(",KOSEI_NENKIN_HYOJUN_HOSHU_GTGK");
            sql.Append(",KOSEI_NENKIN_HOKENRYO");
            sql.Append(",KOSEI_NENKIN_SHOYO_KEISAN_KBN");
            sql.Append(",KENKO_HOKEN_KANYU_KUBUN");
            sql.Append(",KOSEI_NENKIN_KANYU_KUBUN");
            sql.Append(",KOSEI_NENKIN_SHUBETSU");
            sql.Append(",KOSEI_NENKIN_SHUBETSU_NM");
            sql.Append(",SHAHO_JUZEN_KAITEI_TSUKI");
            sql.Append(",SHAHO_JUZEN_KAITEI_GENIN");
            sql.Append(",KOYO_HOKEN_KEISAN_KUBUN");
            sql.Append(",JUMINZEI_SHICHOSON_CD");
            sql.Append(",JUMINZEI_SHICHOSON_NM");
            sql.Append(",JUMINZEI_6GATSUBUN");
            sql.Append(",JUMINZEI_7GATSUBUN_IKO");
            sql.Append(",ZEIGAKU_HYO");
            sql.Append(",NENMATSU_CHOSEI");
            sql.Append(",HONNIN_KUBUN1");
            sql.Append(",HONNIN_KUBUN1_NM");
            sql.Append(",HONNIN_KUBUN2");
            sql.Append(",HONNIN_KUBUN2_NM");
            sql.Append(",HONNIN_KUBUN3");
            sql.Append(",HONNIN_KUBUN3_NM");
            sql.Append(",HAIGUSHA_KUBUN");
            sql.Append(",HAIGUSHA_KUBUN_NM");
            sql.Append(",FUYO_IPPAN");
            sql.Append(",FUYO_NENSHO");
            sql.Append(",FUYO_TOKUTEI");
            sql.Append(",FUYO_ROJIN_DOKYO_ROSHINTO_IGAI");
            sql.Append(",FUYO_ROJIN_DOKYO_ROSHINTO");
            sql.Append(",FUYO_DOKYO_TOKUSHO_IPPAN");
            sql.Append(",FUYO_DOKYO_TOKUSHO_NENSHO");
            sql.Append(",FUYO_DOKYO_TOKUSHO_TOKUTEI");
            sql.Append(",FUYO_DOKYO_TKS_DOKYO_RSNT_IGAI");
            sql.Append(",FUYO_DOKYO_TKS_DOKYO_RSNT");
            sql.Append(",SHOGAISHA_IPPAN");
            sql.Append(",SHOGAISHA_TOKUBETSU");
            sql.Append(",OTTO_ARI");
            sql.Append(",MISEINENSHA");
            sql.Append(",SHIBO_TAISHOKU");
            sql.Append(",SAIGAISHA");
            sql.Append(",GAIKOKUJIN");

            sql.Append(",FUYO_HAIGUSHA_NM");
            sql.Append(",FUYO_HAIGUSHA_KANA_NM");
            sql.Append(",FUYO_HAIGUSHA_JUKYO");
            sql.Append(",FUYO_HAIGUSHA_SEINENGAPPI");
            sql.Append(",FUYO1_NM");
            sql.Append(",FUYO1_KANA_NM");
            sql.Append(",FUYO1_JUKYO");
            sql.Append(",FUYO1_SEINENGAPPI");
            sql.Append(",FUYO2_NM");
            sql.Append(",FUYO2_KANA_NM");
            sql.Append(",FUYO2_JUKYO");
            sql.Append(",FUYO2_SEINENGAPPI");
            sql.Append(",FUYO3_NM");
            sql.Append(",FUYO3_KANA_NM");
            sql.Append(",FUYO3_JUKYO");
            sql.Append(",FUYO3_SEINENGAPPI");
            sql.Append(",FUYO4_NM");
            sql.Append(",FUYO4_KANA_NM");
            sql.Append(",FUYO4_JUKYO");
            sql.Append(",FUYO4_SEINENGAPPI");
            sql.Append(",FUYO_BIKOU");
            sql.Append(",FUYO1_MIMAN_NM");
            sql.Append(",FUYO1_MIMAN_KANA_NM");
            sql.Append(",FUYO1_MIMAN_JUKYO");
            sql.Append(",FUYO1_MIMAN_SEINENGAPPI");
            sql.Append(",FUYO2_MIMAN_NM");
            sql.Append(",FUYO2_MIMAN_KANA_NM");
            sql.Append(",FUYO2_MIMAN_JUKYO");
            sql.Append(",FUYO2_MIMAN_SEINENGAPPI");
            sql.Append(",FUYO3_MIMAN_NM");
            sql.Append(",FUYO3_MIMAN_KANA_NM");
            sql.Append(",FUYO3_MIMAN_JUKYO");
            sql.Append(",FUYO3_MIMAN_SEINENGAPPI");
            sql.Append(",FUYO4_MIMAN_NM");
            sql.Append(",FUYO4_MIMAN_KANA_NM");
            sql.Append(",FUYO4_MIMAN_JUKYO");
            sql.Append(",FUYO4_MIMAN_SEINENGAPPI");
            sql.Append(",FUYO_MIMAN_BIKOU");

            sql.Append(",TEKIYO1");
            sql.Append(",TEKIYO2");
            sql.Append(",SAI16_MIMAN"); 
            sql.Append(",HIJUKYO_SHINZOKU");
            sql.Append(",KYUYO_SHUBETSU");
            sql.Append(",TEATE_SHIKYU_KOMOKU1");
            sql.Append(",TEATE_SHIKYU_KOMOKU2");
            sql.Append(",TEATE_SHIKYU_KOMOKU3");
            sql.Append(",TEATE_SHIKYU_KOMOKU4");
            sql.Append(",TEATE_SHIKYU_KOMOKU5");
            sql.Append(",TEATE_SHIKYU_KOMOKU6");
            sql.Append(",TEATE_SHIKYU_KOMOKU7");
            sql.Append(",TEATE_SHIKYU_KOMOKU8");
            sql.Append(",TEATE_SHIKYU_KOMOKU9");
            sql.Append(",TEATE_SHIKYU_KOMOKU10");
            sql.Append(",TEATE_SHIKYU_KOMOKU11");
            sql.Append(",TEATE_SHIKYU_KOMOKU12");
            sql.Append(",TEATE_SHIKYU_KOMOKU13");
            sql.Append(",TEATE_SHIKYU_KOMOKU14");
            sql.Append(",TEATE_SHIKYU_KOMOKU15");
            sql.Append(",TEATE_SHIKYU_KOMOKU16");
            sql.Append(",TEATE_SHIKYU_KOMOKU17");
            sql.Append(",TEATE_SHIKYU_KOMOKU18");
            sql.Append(",TEATE_SHIKYU_KOMOKU19");
            sql.Append(",TEATE_SHIKYU_KOMOKU20");
            sql.Append(",TEATE_SHIKYU_KOMOKU21");
            sql.Append(",TEATE_SHIKYU_KOMOKU22");
            sql.Append(",TEATE_SHIKYU_KOMOKU23");
            sql.Append(",TEATE_SHIKYU_KOMOKU24");
            sql.Append(",TEATE_SHIKYU_KOMOKU25");
            sql.Append(",TEATE_SHIKYU_KOMOKU26");
            sql.Append(",TEATE_SHIKYU_KOMOKU27");
            sql.Append(",TEATE_SHIKYU_KOMOKU28");
            sql.Append(",TEATE_SHIKYU_KOMOKU29");
            sql.Append(",TEATE_SHIKYU_KOMOKU30");
            sql.Append(",TEATE_KOJO_KOMOKU1");
            sql.Append(",TEATE_KOJO_KOMOKU2");
            sql.Append(",TEATE_KOJO_KOMOKU3");
            sql.Append(",TEATE_KOJO_KOMOKU4");
            sql.Append(",TEATE_KOJO_KOMOKU5");
            sql.Append(",TEATE_KOJO_KOMOKU6");
            sql.Append(",TEATE_KOJO_KOMOKU7");
            sql.Append(",TEATE_KOJO_KOMOKU8");
            sql.Append(",TEATE_KOJO_KOMOKU9");
            sql.Append(",TEATE_KOJO_KOMOKU10");
            sql.Append(",TEATE_KOJO_KOMOKU11");
            sql.Append(",TEATE_KOJO_KOMOKU12");
            sql.Append(",TEATE_KOJO_KOMOKU13");
            sql.Append(",TEATE_KOJO_KOMOKU14");
            sql.Append(",TEATE_KOJO_KOMOKU15");
            sql.Append(",TEATE_KOJO_KOMOKU16");
            sql.Append(",TEATE_KOJO_KOMOKU17");
            sql.Append(",TEATE_KOJO_KOMOKU18");
            sql.Append(",TEATE_KOJO_KOMOKU19");
            sql.Append(",TEATE_KOJO_KOMOKU20");
            sql.Append(",KYUYO_SHIKYU_HOHO1");
            sql.Append(",KYUYO_SHIKYU_KINGAKU1");
            sql.Append(",KYUYO_SHIKYU_RITSU1");
            sql.Append(",KYUYO_GINKO_CD1");
            sql.Append(",KYUYO_GINKO_NM1");
            sql.Append(",KYUYO_SHITEN_CD1");
            sql.Append(",KYUYO_SHITEN_NM1");
            sql.Append(",KYUYO_YOKIN_SHUMOKU1");
            sql.Append(",KYUYO_KOZA_BANGO1");
            sql.Append(",KYUYO_KOZA_MEIGININ_KANJI1");
            sql.Append(",KYUYO_KOZA_MEIGININ_KANA1");
            sql.Append(",KYUYO_KEIYAKUSHA_BANGO1");
            sql.Append(",KYUYO_SHIKYU_HOHO2");
            sql.Append(",KYUYO_SHIKYU_KINGAKU2");
            sql.Append(",KYUYO_SHIKYU_RITSU2");
            sql.Append(",KYUYO_GINKO_CD2");
            sql.Append(",KYUYO_GINKO_NM2");
            sql.Append(",KYUYO_SHITEN_CD2");
            sql.Append(",KYUYO_SHITEN_NM2");
            sql.Append(",KYUYO_YOKIN_SHUMOKU2");
            sql.Append(",KYUYO_KOZA_BANGO2");
            sql.Append(",KYUYO_KOZA_MEIGININ_KANJI2");
            sql.Append(",KYUYO_KOZA_MEIGININ_KANA2");
            sql.Append(",KYUYO_KEIYAKUSHA_BANGO2");
            sql.Append(",KAIGO_HOKEN_KUBUN");
            sql.Append(" FROM");
            sql.Append(" VI_KY_SHAIN_JOHO");
            sql.Append(" WHERE");
            sql.Append(" KAISHA_CD = @KAISHA_CD");
            sql.Append(" AND SHAIN_CD = @SHAIN_CD");
            return sql.ToString();
        }

        /// <summary>
        /// コード項目の補正
        /// </summary>
        /// <param name="value">入力値</param>
        /// <param name="maxCode">定義上限値</param>
        /// <returns>下限0から上限値までの値</returns>
        private string FixDefinedCode(object value, int maxCode)
        {
            string ret = "0";

            if (value != null)
            {
                if (Util.ToInt(value) > maxCode)
                {
                    ret = Util.ToString(maxCode);
                }
                else
                {
                    ret = Util.ToString(value);
                }
            }

            return ret;
        }

        /// <summary>
        /// 日付入力の補正
        /// </summary>
        /// <param name="gengo">元号ラベル</param>
        /// <param name="gengoYear">和暦年テキストボックス</param>
        /// <param name="month">月テキストボックス</param>
        /// <param name="day">日テキストボックス</param>
        private void FixDateControls(Label gengo, FsiTextBox gengoYear, FsiTextBox month, FsiTextBox day)
        {
            if (ValChk.IsEmpty(gengoYear.Text))
            {
                // 和暦年が未入力の場合、日付入力をクリア
                gengo.Text = "";
                gengoYear.Text = "";
                month.Text = "";
                day.Text = "";
            }
            else
            {
                // 未入力部分は 0年1月1日から補完
                if (ValChk.IsEmpty(gengo.Text)) gengo.Text = "平成";
                //if (ValChk.IsEmpty(gengoYear.Text)) gengoYear.Text = "0";
                if (ValChk.IsEmpty(month.Text) || month.Text == "0") month.Text = "1";
                if (ValChk.IsEmpty(day.Text) || day.Text == "0") day.Text = "1";

                // 月が12以上の場合は12をセット
                if (Util.ToInt(month.Text) > 12) month.Text = "12";

                // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
                DateTime tmpDate = Util.ConvAdDate(gengo.Text, gengoYear.Text, month.Text, "1", this.Dba);
                int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);
                if (Util.ToInt(day.Text) > lastDayInMonth)
                {
                    day.Text = Util.ToString(lastDayInMonth);
                }
            }
        }

        /// <summary>
        /// 配列に格納された生年月日和暦をセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpSeinengappi(string[] arrJpDate)
        {
            this.lblGengoSeinengappi.Text = arrJpDate[0];
            this.txtGengoYearSeinengappi.Text = arrJpDate[2];
            this.txtMonthSeinengappi.Text = arrJpDate[3];
            this.txtDaySeinengappi.Text = arrJpDate[4];
        }

        #region 生年月日和暦変換(扶養親族)
        /// <summary>
        /// 配列に格納された生年月日和暦(控除対象配偶者) 
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpHaigushaSeinengappi(string[] arrJpDate)
        {
            this.lblFuyoHaigushaGengoSeinengappi.Text = arrJpDate[0];
            this.txtFuyoHaigushaGengoYearSeinengappi.Text = arrJpDate[2];
            this.txtFuyoHaigushaMonthSeinengappi.Text = arrJpDate[3];
            this.txtFuyoHaigushaDaySeinengappi.Text = arrJpDate[4];
        }
        /// <summary>
        /// 配列に格納された生年月日和暦(控除対象親族1) 
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpShinzokuSeinengappi1(string[] arrJpDate)
        {
            this.lblFuyoGengoSeinengappi1.Text = arrJpDate[0];
            this.txtFuyoGengoYearSeinengappi1.Text = arrJpDate[2];
            this.txtFuyoMonthSeinengappi1.Text = arrJpDate[3];
            this.txtFuyoDaySeinengappi1.Text = arrJpDate[4];
        }

        /// <summary>
        /// 配列に格納された生年月日和暦(控除対象親族2) 
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpShinzokuSeinengappi2(string[] arrJpDate)
        {
            this.lblFuyoGengoSeinengappi2.Text = arrJpDate[0];
            this.txtFuyoGengoYearSeinengappi2.Text = arrJpDate[2];
            this.txtFuyoMonthSeinengappi2.Text = arrJpDate[3];
            this.txtFuyoDaySeinengappi2.Text = arrJpDate[4];
        }

        /// <summary>
        /// 配列に格納された生年月日和暦(控除対象親族3) 
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpShinzokuSeinengappi3(string[] arrJpDate)
        {
            this.lblFuyoGengoSeinengappi3.Text = arrJpDate[0];
            this.txtFuyoGengoYearSeinengappi3.Text = arrJpDate[2];
            this.txtFuyoMonthSeinengappi3.Text = arrJpDate[3];
            this.txtFuyoDaySeinengappi3.Text = arrJpDate[4];
        }

        /// <summary>
        /// 配列に格納された生年月日和暦(控除対象親族4) 
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpShinzokuSeinengappi4(string[] arrJpDate)
        {
            this.lblFuyoGengoSeinengappi4.Text = arrJpDate[0];
            this.txtFuyoGengoYearSeinengappi4.Text = arrJpDate[2];
            this.txtFuyoMonthSeinengappi4.Text = arrJpDate[3];
            this.txtFuyoDaySeinengappi4.Text = arrJpDate[4];
        }

        /// <summary>
        /// 配列に格納された生年月日和暦(控除対象16歳未満1) 
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpMimanSeinengappi1(string[] arrJpDate)
        {
            this.lblFuyoMimanGengoSeinengappi1.Text = arrJpDate[0];
            this.txtFuyoMimanGengoYearSeinengappi1.Text = arrJpDate[2];
            this.txtFuyoMimanMonthSeinengappi1.Text = arrJpDate[3];
            this.txtFuyoMimanDaySeinengappi1.Text = arrJpDate[4];
        }

        /// <summary>
        /// 配列に格納された生年月日和暦(控除対象16歳未満2) 
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpMimanSeinengappi2(string[] arrJpDate)
        {
            this.lblFuyoMimanGengoSeinengappi2.Text = arrJpDate[0];
            this.txtFuyoMimanGengoYearSeinengappi2.Text = arrJpDate[2];
            this.txtFuyoMimanMonthSeinengappi2.Text = arrJpDate[3];
            this.txtFuyoMimanDaySeinengappi2.Text = arrJpDate[4];
        }

        /// <summary>
        /// 配列に格納された生年月日和暦(控除対象16歳未満3) 
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpMimanSeinengappi3(string[] arrJpDate)
        {
            this.lblFuyoMimanGengoSeinengappi3.Text = arrJpDate[0];
            this.txtFuyoMimanGengoYearSeinengappi3.Text = arrJpDate[2];
            this.txtFuyoMimanMonthSeinengappi3.Text = arrJpDate[3];
            this.txtFuyoMimanDaySeinengappi3.Text = arrJpDate[4];
        }

        /// <summary>
        /// 配列に格納された生年月日和暦(控除対象16歳未満4) 
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpMimanSeinengappi4(string[] arrJpDate)
        {
            this.lblFuyoMimanGengoSeinengappi4.Text = arrJpDate[0];
            this.txtFuyoMimanGengoYearSeinengappi4.Text = arrJpDate[2];
            this.txtFuyoMimanMonthSeinengappi4.Text = arrJpDate[3];
            this.txtFuyoMimanDaySeinengappi4.Text = arrJpDate[4];
        }
        /**/
        #endregion

        /// <summary>
        /// 配列に格納された入社年月日和暦をセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpNyushaNengappi(string[] arrJpDate)
        {
            this.lblGengoNyushaNengappi.Text = arrJpDate[0];
            this.txtGengoYearNyushaNengappi.Text = arrJpDate[2];
            this.txtMonthNyushaNengappi.Text = arrJpDate[3];
            this.txtDayNyushaNengappi.Text = arrJpDate[4];
        }

        /// <summary>
        /// 配列に格納された退職年月日和暦をセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpTaishokuNengappi(string[] arrJpDate)
        {
            this.lblGengoTaishokuNengappi.Text = arrJpDate[0];
            this.txtGengoYearTaishokuNengappi.Text = arrJpDate[2];
            this.txtMonthTaishokuNengappi.Text = arrJpDate[3];
            this.txtDayTaishokuNengappi.Text = arrJpDate[4];
        }

        /// <summary>
        /// TB_KY_SHAIN_JOHOに更新するためのパラメータ設定をします。
        /// </summary>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// 更新処理：DbParamCollection*2(Where句,Set句)
        /// </returns>
        private ArrayList SetKyShainJohoParams()
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            if (MODE_NEW.Equals(this.Par1))
            {
                // 会社コード
                updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
                // 社員コード
                updParam.SetParam("@SHAIN_CD", SqlDbType.Decimal, 6, Util.ToDecimal(this.txtShainCd.Text));
                // 登録日
                updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWDATE");
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // WHERE句パラメータ
                DbParamCollection whereParam = new DbParamCollection();
                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
                whereParam.SetParam("@SHAIN_CD", SqlDbType.Decimal, 6, Util.ToDecimal(this.txtShainCd.Text));

                alParams.Add(whereParam);
            }

            // 基本情報
            updParam.SetParam("@SHAIN_NM", SqlDbType.VarChar, 30, this.txtShainNm.Text);
            updParam.SetParam("@SHAIN_KANA_NM", SqlDbType.VarChar, 30, this.txtShainKanaNm.Text);
            updParam.SetParam("@SEIBETSU", SqlDbType.Decimal, 1, Util.ToInt(this.txtSeibetsu.Text));
            if (ValChk.IsEmpty(this.txtGengoYearSeinengappi.Text))
            {
                updParam.SetParam("@SEINENGAPPI", SqlDbType.DateTime, DBNull.Value);
            }
            else
            {
                updParam.SetParam("@SEINENGAPPI", SqlDbType.DateTime,
                    Util.ConvAdDate(
                        this.lblGengoSeinengappi.Text,
                        this.txtGengoYearSeinengappi.Text,
                        this.txtMonthSeinengappi.Text,
                        this.txtDaySeinengappi.Text, this.Dba));
            }
            updParam.SetParam("@YUBIN_BANGO1", SqlDbType.VarChar, 3, this.txtYubinBango1.Text);
            updParam.SetParam("@YUBIN_BANGO2", SqlDbType.VarChar, 4, this.txtYubinBango2.Text);
            updParam.SetParam("@JUSHO1", SqlDbType.VarChar, 40, this.txtJusho1.Text);
            updParam.SetParam("@JUSHO2", SqlDbType.VarChar, 40, this.txtJusho2.Text);
            updParam.SetParam("@DENWA_BANGO", SqlDbType.VarChar, 15, this.txtDenwaBango.Text);
            updParam.SetParam("@BUMON_CD", SqlDbType.Decimal, 4, Util.ToDecimal(this.txtBumonCd.Text));
            updParam.SetParam("@BUKA_CD", SqlDbType.Decimal, 4, Util.ToDecimal(this.txtBukaCd.Text));
            updParam.SetParam("@YAKUSHOKU_CD", SqlDbType.Decimal, 4, Util.ToDecimal(this.txtYakushokuCd.Text));
            updParam.SetParam("@KYUYO_KEITAI", SqlDbType.Decimal, 1, Util.ToDecimal(this.txtKyuyoKeitai.Text));
            if (ValChk.IsEmpty(this.txtGengoYearNyushaNengappi.Text))
            {
                updParam.SetParam("@NYUSHA_NENGAPPI", SqlDbType.DateTime, DBNull.Value);
            }
            else
            {
                updParam.SetParam("@NYUSHA_NENGAPPI", SqlDbType.DateTime,
                    Util.ConvAdDate(
                        this.lblGengoNyushaNengappi.Text,
                        this.txtGengoYearNyushaNengappi.Text,
                        this.txtMonthNyushaNengappi.Text,
                        this.txtDayNyushaNengappi.Text, this.Dba));
            }
            if (ValChk.IsEmpty(this.txtGengoYearTaishokuNengappi.Text))
            {
                updParam.SetParam("@TAISHOKU_NENGAPPI", SqlDbType.DateTime, DBNull.Value);
            }
            else
            {
                updParam.SetParam("@TAISHOKU_NENGAPPI", SqlDbType.DateTime,
                    Util.ConvAdDate(
                        this.lblGengoTaishokuNengappi.Text,
                        this.txtGengoYearTaishokuNengappi.Text,
                        this.txtMonthTaishokuNengappi.Text,
                        this.txtDayTaishokuNengappi.Text, this.Dba));
            }
            updParam.SetParam("@ZENSHOKU_BIKOU", SqlDbType.VarChar, 50, this.txtZenBikou.Text);
            // 社会保険／住民税
            if (ValChk.IsEmpty(this.txtKenkoHokenshoBango.Text))
            {
                updParam.SetParam("@KENKO_HOKENSHO_BANGO", SqlDbType.Decimal, 10, DBNull.Value);
            }
            else
            {
                updParam.SetParam("@KENKO_HOKENSHO_BANGO", SqlDbType.Decimal, 10, Util.ToDecimal(this.txtKenkoHokenshoBango.Text));
            }
            //updParam.SetParam("@KENKO_HOKEN_TOKYU", SqlDbType.Decimal, 2, 1); //
            updParam.SetParam("@KENKO_HOKEN_HYOJUN_HOSHU_GTGK", SqlDbType.Decimal, 9, Util.ToDecimal(this.txtKenkoHokenHyojunHoshuGtgk.Text));
            updParam.SetParam("@KENKO_HOKENRYO", SqlDbType.Decimal, 9, Util.ToDecimal(this.txtKenkoHokenryo.Text));
            updParam.SetParam("@KENKO_HOKEN_SHOYO_KEISAN_KUBUN", SqlDbType.Decimal, 1, Util.ToDecimal(this.txtKenkoHokenShoyoKeisanKubun.Text));
            if (ValChk.IsEmpty(this.txtKoseiNenkinBango.Text))
            {
                updParam.SetParam("@KOSEI_NENKIN_BANGO", SqlDbType.Decimal, 10, DBNull.Value);
            }
            else
            {
                updParam.SetParam("@KOSEI_NENKIN_BANGO", SqlDbType.Decimal, 10, Util.ToDecimal(this.txtKoseiNenkinBango.Text));
            }
            //updParam.SetParam("@KOSEI_NENKIN_TOKYU", SqlDbType.Decimal, 2, 1); //
            updParam.SetParam("@KOSEI_NENKIN_HYOJUN_HOSHU_GTGK", SqlDbType.Decimal, 9, Util.ToDecimal(this.txtKoseiNenkinHyojunHoshuGtgk.Text));
            updParam.SetParam("@KOSEI_NENKIN_HOKENRYO", SqlDbType.Decimal, 9, Util.ToDecimal(this.txtKoseiNenkinHokenryo.Text));
            updParam.SetParam("@KOSEI_NENKIN_SHOYO_KEISAN_KBN", SqlDbType.Decimal, 1, Util.ToDecimal(this.txtKoseiNenkinShoyoKeisanKbn.Text));
            //updParam.SetParam("@KENKO_HOKEN_KANYU_KUBUN", SqlDbType.Decimal, 2, 1); //
            //updParam.SetParam("@KOSEI_NENKIN_KANYU_KUBUN", SqlDbType.Decimal, 2, 1); //
            //updParam.SetParam("@KOSEI_NENKIN_SHUBETSU", SqlDbType.Decimal, 2, 1); //
            //updParam.SetParam("@SHAHO_JUZEN_KAITEI_TSUKI", SqlDbType.Decimal, 2, 1); //
            //updParam.SetParam("@SHAHO_JUZEN_KAITEI_GENIN", SqlDbType.Decimal, 2, 1); //
            updParam.SetParam("@KOYO_HOKEN_KEISAN_KUBUN", SqlDbType.Decimal, 1, Util.ToDecimal(this.txtKoyoHokenKeisanKubun.Text));
            updParam.SetParam("@JUMINZEI_SHICHOSON_CD", SqlDbType.Decimal, 6, Util.ToDecimal(this.txtJuminzeiShichosonCd.Text));
            updParam.SetParam("@JUMINZEI_6GATSUBUN", SqlDbType.Decimal, 9, Util.ToDecimal(this.txtJuminzei6gatsubun.Text));
            updParam.SetParam("@JUMINZEI_7GATSUBUN_IKO", SqlDbType.Decimal, 9, Util.ToDecimal(this.txtJuminzei7gatsubunIko.Text));
            updParam.SetParam("@KAIGO_HOKEN_KUBUN", SqlDbType.Decimal, 1, Util.ToDecimal(this.txtKaigoHokenKubun.Text));
            // 所得税
            updParam.SetParam("@ZEIGAKU_HYO", SqlDbType.Decimal, 1, Util.ToDecimal(this.txtZeigakuHyo.Text));
            updParam.SetParam("@NENMATSU_CHOSEI", SqlDbType.Decimal, 1, Util.ToDecimal(this.txtNenmatsuChosei.Text));
            updParam.SetParam("@HONNIN_KUBUN1", SqlDbType.Decimal, 1, Util.ToDecimal(this.txtHonninKubun1.Text));
            updParam.SetParam("@HONNIN_KUBUN2", SqlDbType.Decimal, 1, Util.ToDecimal(this.txtHonninKubun2.Text));
            updParam.SetParam("@HONNIN_KUBUN3", SqlDbType.Decimal, 1, Util.ToDecimal(this.txtHonninKubun3.Text));
            updParam.SetParam("@HAIGUSHA_KUBUN", SqlDbType.Decimal, 1, Util.ToDecimal(this.txtHaigushaKubun.Text));
            updParam.SetParam("@FUYO_IPPAN", SqlDbType.Decimal, 2, Util.ToDecimal(this.txtFuyoIppan.Text));
            //updParam.SetParam("@FUYO_NENSHO", SqlDbType.Decimal, 2, 1); //
            updParam.SetParam("@FUYO_TOKUTEI", SqlDbType.Decimal, 2, Util.ToDecimal(this.txtFuyoTokutei.Text));
            updParam.SetParam("@FUYO_ROJIN_DOKYO_ROSHINTO_IGAI", SqlDbType.Decimal, 2, Util.ToDecimal(this.txtFuyoRojinDokyoRoshintoIgai.Text));
            updParam.SetParam("@FUYO_ROJIN_DOKYO_ROSHINTO", SqlDbType.Decimal, 2, Util.ToDecimal(this.txtFuyoRojinDokyoRoshinto.Text));
            updParam.SetParam("@FUYO_DOKYO_TOKUSHO_IPPAN", SqlDbType.Decimal, 2, Util.ToDecimal(this.txtFuyoDokyoTokushoIppan.Text));

            // 扶養配偶者情報
            updParam.SetParam("@FUYO_HAIGUSHA_NM", SqlDbType.VarChar, 30, this.txtFuyoHaigushaNm.Text);
            updParam.SetParam("@FUYO_HAIGUSHA_KANA_NM", SqlDbType.VarChar, 30, this.txtFuyoHaigushaKanaNm.Text);
            updParam.SetParam("@FUYO_HAIGUSHA_JUKYO", SqlDbType.Decimal, 2, Util.ToDecimal(this.txtFuyoHaigushaJukyo.Text));

            if (ValChk.IsEmpty(this.txtFuyoHaigushaGengoYearSeinengappi.Text))
            {
                updParam.SetParam("@FUYO_HAIGUSHA_SEINENGAPPI", SqlDbType.DateTime, DBNull.Value);
            }
            else
            {
                // 生年月日
                updParam.SetParam("@FUYO_HAIGUSHA_SEINENGAPPI", SqlDbType.DateTime,
                        Util.ConvAdDate(
                            this.lblFuyoHaigushaGengoSeinengappi.Text,
                            this.txtFuyoHaigushaGengoYearSeinengappi.Text,
                            this.txtFuyoHaigushaMonthSeinengappi.Text,
                            this.txtFuyoHaigushaDaySeinengappi.Text, this.Dba));
            }

            // 扶養親族情報
            updParam.SetParam("@FUYO1_NM", SqlDbType.VarChar, 30, this.txtFuyoNm1.Text);
            updParam.SetParam("@FUYO1_KANA_NM", SqlDbType.VarChar, 30, this.txtFuyoKanaNm1.Text);
            updParam.SetParam("@FUYO1_JUKYO", SqlDbType.Decimal, 2, Util.ToDecimal(this.txtFuyoJukyo1.Text));

            if (ValChk.IsEmpty(this.txtFuyoGengoYearSeinengappi1.Text))
            {
                updParam.SetParam("@FUYO1_SEINENGAPPI", SqlDbType.DateTime, DBNull.Value);
            }
            else
            {
                // 生年月日
                updParam.SetParam("@FUYO1_SEINENGAPPI", SqlDbType.DateTime,
                        Util.ConvAdDate(
                            this.lblFuyoGengoSeinengappi1.Text,
                            this.txtFuyoGengoYearSeinengappi1.Text,
                            this.txtFuyoMonthSeinengappi1.Text,
                            this.txtFuyoDaySeinengappi1.Text, this.Dba));
            }

            // 扶養親族情報
            updParam.SetParam("@FUYO2_NM", SqlDbType.VarChar, 30, this.txtFuyoNm2.Text);
            updParam.SetParam("@FUYO2_KANA_NM", SqlDbType.VarChar, 30, this.txtFuyoKanaNm2.Text);
            updParam.SetParam("@FUYO2_JUKYO", SqlDbType.Decimal, 2, Util.ToDecimal(this.txtFuyoJukyo2.Text));

            if (ValChk.IsEmpty(this.txtFuyoGengoYearSeinengappi2.Text))
            {
                updParam.SetParam("@FUYO2_SEINENGAPPI", SqlDbType.DateTime, DBNull.Value);
            }
            else
            {
                // 生年月日
                updParam.SetParam("@FUYO2_SEINENGAPPI", SqlDbType.DateTime,
                        Util.ConvAdDate(
                            this.lblFuyoGengoSeinengappi2.Text,
                            this.txtFuyoGengoYearSeinengappi2.Text,
                            this.txtFuyoMonthSeinengappi2.Text,
                            this.txtFuyoDaySeinengappi2.Text, this.Dba));
            }

            // 扶養親族情報
            updParam.SetParam("@FUYO3_NM", SqlDbType.VarChar, 30, this.txtFuyoNm3.Text);
            updParam.SetParam("@FUYO3_KANA_NM", SqlDbType.VarChar, 30, this.txtFuyoKanaNm3.Text);
            updParam.SetParam("@FUYO3_JUKYO", SqlDbType.Decimal, 2, Util.ToDecimal(this.txtFuyoJukyo3.Text));

            if (ValChk.IsEmpty(this.txtFuyoGengoYearSeinengappi3.Text))
            {
                updParam.SetParam("@FUYO3_SEINENGAPPI", SqlDbType.DateTime, DBNull.Value);
            }
            else
            {
                // 生年月日
                updParam.SetParam("@FUYO3_SEINENGAPPI", SqlDbType.DateTime,
                        Util.ConvAdDate(
                            this.lblFuyoGengoSeinengappi3.Text,
                            this.txtFuyoGengoYearSeinengappi3.Text,
                            this.txtFuyoMonthSeinengappi3.Text,
                            this.txtFuyoDaySeinengappi3.Text, this.Dba));
            }

            // 扶養親族情報
            updParam.SetParam("@FUYO4_NM", SqlDbType.VarChar, 30, this.txtFuyoNm4.Text);
            updParam.SetParam("@FUYO4_KANA_NM", SqlDbType.VarChar, 30, this.txtFuyoKanaNm4.Text);
            updParam.SetParam("@FUYO4_JUKYO", SqlDbType.Decimal, 2, Util.ToDecimal(this.txtFuyoJukyo4.Text));

            if (ValChk.IsEmpty(this.txtFuyoGengoYearSeinengappi4.Text))
            {
                updParam.SetParam("@FUYO4_SEINENGAPPI", SqlDbType.DateTime, DBNull.Value);
            }
            else
            {
                // 生年月日
                updParam.SetParam("@FUYO4_SEINENGAPPI", SqlDbType.DateTime,
                        Util.ConvAdDate(
                            this.lblFuyoGengoSeinengappi4.Text,
                            this.txtFuyoGengoYearSeinengappi4.Text,
                            this.txtFuyoMonthSeinengappi4.Text,
                            this.txtFuyoDaySeinengappi4.Text, this.Dba));
            }
            updParam.SetParam("@FUYO_BIKOU", SqlDbType.VarChar, 50, this.txtIkouNo.Text);

            /*　16歳未満の扶養親族入力項目　*/
            updParam.SetParam("@FUYO1_MIMAN_NM", SqlDbType.VarChar, 30, this.txtFuyoMimanNm1.Text);
            updParam.SetParam("@FUYO1_MIMAN_KANA_NM", SqlDbType.VarChar, 30, this.txtFuyoMimanKanaNm1.Text);
            updParam.SetParam("@FUYO1_MIMAN_JUKYO", SqlDbType.Decimal, 2, Util.ToDecimal(this.txtFuyoMimanJukyo1.Text));

            if (ValChk.IsEmpty(this.txtFuyoMimanGengoYearSeinengappi1.Text))
            {
                updParam.SetParam("@FUYO1_MIMAN_SEINENGAPPI", SqlDbType.DateTime, DBNull.Value);
            }
            else
            {
                // 生年月日
                updParam.SetParam("@FUYO1_MIMAN_SEINENGAPPI", SqlDbType.DateTime,
                        Util.ConvAdDate(
                            this.lblFuyoMimanGengoSeinengappi1.Text,
                            this.txtFuyoMimanGengoYearSeinengappi1.Text,
                            this.txtFuyoMimanMonthSeinengappi1.Text,
                            this.txtFuyoMimanDaySeinengappi1.Text, this.Dba));
            }

            updParam.SetParam("@FUYO2_MIMAN_NM", SqlDbType.VarChar, 30, this.txtFuyoMimanNm2.Text);
            updParam.SetParam("@FUYO2_MIMAN_KANA_NM", SqlDbType.VarChar, 30, this.txtFuyoMimanKanaNm2.Text);
            updParam.SetParam("@FUYO2_MIMAN_JUKYO", SqlDbType.Decimal, 2, Util.ToDecimal(this.txtFuyoMimanJukyo2.Text));

            if (ValChk.IsEmpty(this.txtFuyoMimanGengoYearSeinengappi2.Text))
            {
                updParam.SetParam("@FUYO2_MIMAN_SEINENGAPPI", SqlDbType.DateTime, DBNull.Value);
            }
            else
            {
                // 生年月日
                updParam.SetParam("@FUYO2_MIMAN_SEINENGAPPI", SqlDbType.DateTime,
                        Util.ConvAdDate(
                            this.lblFuyoMimanGengoSeinengappi2.Text,
                            this.txtFuyoMimanGengoYearSeinengappi2.Text,
                            this.txtFuyoMimanMonthSeinengappi2.Text,
                            this.txtFuyoMimanDaySeinengappi2.Text, this.Dba));
            }

            updParam.SetParam("@FUYO3_MIMAN_NM", SqlDbType.VarChar, 30, this.txtFuyoMimanNm3.Text);
            updParam.SetParam("@FUYO3_MIMAN_KANA_NM", SqlDbType.VarChar, 30, this.txtFuyoMimanKanaNm3.Text);
            updParam.SetParam("@FUYO3_MIMAN_JUKYO", SqlDbType.Decimal, 2, Util.ToDecimal(this.txtFuyoMimanJukyo3.Text));

            if (ValChk.IsEmpty(this.txtFuyoMimanGengoYearSeinengappi3.Text))
            {
                updParam.SetParam("@FUYO3_MIMAN_SEINENGAPPI", SqlDbType.DateTime, DBNull.Value);
            }
            else
            {
                // 生年月日
                updParam.SetParam("@FUYO3_MIMAN_SEINENGAPPI", SqlDbType.DateTime,
                        Util.ConvAdDate(
                            this.lblFuyoMimanGengoSeinengappi3.Text,
                            this.txtFuyoMimanGengoYearSeinengappi3.Text,
                            this.txtFuyoMimanMonthSeinengappi3.Text,
                            this.txtFuyoMimanDaySeinengappi3.Text, this.Dba));
            }

            updParam.SetParam("@FUYO4_MIMAN_NM", SqlDbType.VarChar, 30, this.txtFuyoMimanNm4.Text);
            updParam.SetParam("@FUYO4_MIMAN_KANA_NM", SqlDbType.VarChar, 30, this.txtFuyoMimanKanaNm4.Text);
            updParam.SetParam("@FUYO4_MIMAN_JUKYO", SqlDbType.Decimal, 2, Util.ToDecimal(this.txtFuyoMimanJukyo4.Text));

            if (ValChk.IsEmpty(this.txtFuyoMimanGengoYearSeinengappi4.Text))
            {
                updParam.SetParam("@FUYO4_MIMAN_SEINENGAPPI", SqlDbType.DateTime, DBNull.Value);
            }
            else
            {
                // 生年月日
                updParam.SetParam("@FUYO4_MIMAN_SEINENGAPPI", SqlDbType.DateTime,
                        Util.ConvAdDate(
                            this.lblFuyoMimanGengoSeinengappi4.Text,
                            this.txtFuyoMimanGengoYearSeinengappi4.Text,
                            this.txtFuyoMimanMonthSeinengappi4.Text,
                            this.txtFuyoMimanDaySeinengappi4.Text, this.Dba));
            }
            updParam.SetParam("@FUYO_MIMAN_BIKOU", SqlDbType.VarChar, 50, this.txtIkouMimanNo.Text);

            //updParam.SetParam("@FUYO_DOKYO_TOKUSHO_NENSHO", SqlDbType.Decimal, 2, 1); //
            //updParam.SetParam("@FUYO_DOKYO_TOKUSHO_TOKUTEI", SqlDbType.Decimal, 2, 1); //
            //updParam.SetParam("@FUYO_DOKYO_TKS_DOKYO_RSNT_IGAI", SqlDbType.Decimal, 2, 1); //
            //updParam.SetParam("@FUYO_DOKYO_TKS_DOKYO_RSNT", SqlDbType.Decimal, 2, 1); //
            updParam.SetParam("@SHOGAISHA_IPPAN", SqlDbType.Decimal, 2, Util.ToDecimal(this.txtShogaishaIppan.Text));
            updParam.SetParam("@SHOGAISHA_TOKUBETSU", SqlDbType.Decimal, 2, Util.ToDecimal(this.txtShogaishaTokubetsu.Text));
            //updParam.SetParam("@OTTO_ARI", SqlDbType.Decimal, 2, 1); //
            updParam.SetParam("@MISEINENSHA", SqlDbType.Decimal, 2, Util.ToDecimal(this.txtMiseinensha.Text));
            updParam.SetParam("@SHIBO_TAISHOKU", SqlDbType.Decimal, 2, Util.ToDecimal(this.txtShiboTaishoku.Text));
            updParam.SetParam("@SAIGAISHA", SqlDbType.Decimal, 2, Util.ToDecimal(this.txtSaigaisha.Text));
            updParam.SetParam("@GAIKOKUJIN", SqlDbType.Decimal, 2, Util.ToDecimal(this.txtGaikokujin.Text));
            updParam.SetParam("@TEKIYO1", SqlDbType.VarChar, 50, this.txtTekiyo1.Text);
            updParam.SetParam("@TEKIYO2", SqlDbType.VarChar, 50, this.txtTekiyo2.Text);
            updParam.SetParam("@KYUYO_SHUBETSU", SqlDbType.VarChar, 20, this.txtKyuyoShubetsu.Text);
            updParam.SetParam("@SAI16_MIMAN", SqlDbType.Decimal, 2, Util.ToDecimal(this.txtSai16Miman.Text));
            updParam.SetParam("@HIJUKYO_SHINZOKU", SqlDbType.Decimal, 2, Util.ToDecimal(this.txtHiJukyoShinzoku.Text));
            updParam.SetParam("@HYOJI_FLG", SqlDbType.Decimal, 1, this.txthyoji.Text);// 表示区分
            // 諸手当
            /// 支給項目
            foreach (DataRow dr in ((DataTable)dgvShikyu.DataSource).Rows)
            {
                updParam.SetParam("@TEATE_SHIKYU_KOMOKU" + Util.ToString(dr["KOMOKU_BANGO"]),
                    SqlDbType.Decimal, 9, Util.ToDecimal(dr["TEATE_KOMOKU"]));

            }
            /// 控除項目
            foreach (DataRow dr in ((DataTable)dgvKojo.DataSource).Rows)
            {
                updParam.SetParam("@TEATE_KOJO_KOMOKU" + Util.ToString(dr["KOMOKU_BANGO"]),
                    SqlDbType.Decimal, 9, Util.ToDecimal(dr["TEATE_KOMOKU"]));
            }
            // 振込銀行
            updParam.SetParam("@KYUYO_SHIKYU_HOHO1", SqlDbType.Decimal, 1, Util.ToDecimal(this.txtKyuyoShikyuHoho1.Text));
            updParam.SetParam("@KYUYO_SHIKYU_KINGAKU1", SqlDbType.Decimal, 9, Util.ToDecimal(this.txtKyuyoShikyuKingaku1.Text));
            updParam.SetParam("@KYUYO_SHIKYU_RITSU1", SqlDbType.Decimal, 4, Util.ToDecimal(this.txtKyuyoShikyuRitsu1.Text));
            updParam.SetParam("@KYUYO_GINKO_CD1", SqlDbType.Decimal, 4, Util.ToDecimal(this.txtKyuyoGinkoCd1.Text));
            updParam.SetParam("@KYUYO_SHITEN_CD1", SqlDbType.Decimal, 4, Util.ToDecimal(this.txtKyuyoShitenCd1.Text));
            updParam.SetParam("@KYUYO_YOKIN_SHUMOKU1", SqlDbType.Decimal, 1, Util.ToDecimal(this.txtKyuyoYokinShumoku1.Text));
            if (ValChk.IsEmpty(this.txtKyuyoKozaBango1.Text))
            {
                updParam.SetParam("@KYUYO_KOZA_BANGO1", SqlDbType.Decimal, 8, DBNull.Value);
            }
            else
            {
                updParam.SetParam("@KYUYO_KOZA_BANGO1", SqlDbType.VarChar, 8, this.txtKyuyoKozaBango1.Text);
            }
            updParam.SetParam("@KYUYO_KOZA_MEIGININ_KANJI1", SqlDbType.VarChar, 30, this.txtKyuyoKozaMeigininKanji1.Text);
            updParam.SetParam("@KYUYO_KOZA_MEIGININ_KANA1", SqlDbType.VarChar, 30, this.txtKyuyoKozaMeigininKana1.Text);
            updParam.SetParam("@KYUYO_KEIYAKUSHA_BANGO1", SqlDbType.VarChar, 15, this.txtKyuyoKeiyakushaBango1.Text);
            updParam.SetParam("@KYUYO_SHIKYU_HOHO2", SqlDbType.Decimal, 1, Util.ToDecimal(this.txtKyuyoShikyuHoho2.Text));
            updParam.SetParam("@KYUYO_SHIKYU_KINGAKU2", SqlDbType.Decimal, 9, Util.ToDecimal(this.txtKyuyoShikyuKingaku2.Text));
            updParam.SetParam("@KYUYO_SHIKYU_RITSU2", SqlDbType.Decimal, 4, Util.ToDecimal(this.txtKyuyoShikyuRitsu2.Text));
            updParam.SetParam("@KYUYO_GINKO_CD2", SqlDbType.Decimal, 4, Util.ToDecimal(this.txtKyuyoGinkoCd2.Text));
            updParam.SetParam("@KYUYO_SHITEN_CD2", SqlDbType.Decimal, 4, Util.ToDecimal(this.txtKyuyoShitenCd2.Text));
            updParam.SetParam("@KYUYO_YOKIN_SHUMOKU2", SqlDbType.Decimal, 1, Util.ToDecimal(this.txtKyuyoYokinShumoku2.Text));
            if (ValChk.IsEmpty(this.txtKyuyoKozaBango2.Text))
            {
                updParam.SetParam("@KYUYO_KOZA_BANGO2", SqlDbType.Decimal, 8, DBNull.Value);
            }
            else
            {
                updParam.SetParam("@KYUYO_KOZA_BANGO2", SqlDbType.VarChar, 8, this.txtKyuyoKozaBango2.Text);
            }
            updParam.SetParam("@KYUYO_KOZA_MEIGININ_KANJI2", SqlDbType.VarChar, 30, this.txtKyuyoKozaMeigininKanji2.Text);
            updParam.SetParam("@KYUYO_KOZA_MEIGININ_KANA2", SqlDbType.VarChar, 30, this.txtKyuyoKozaMeigininKana2.Text);
            updParam.SetParam("@KYUYO_KEIYAKUSHA_BANGO2", SqlDbType.VarChar, 15, this.txtKyuyoKeiyakushaBango2.Text);
            //updParam.SetParam("@SHOYO_SHIKYU_HOHO1", SqlDbType.Decimal, 1, );
            //updParam.SetParam("@SHOYO_SHIKYU_KINGAKU1", SqlDbType.Decimal, 9, );
            //updParam.SetParam("@SHOYO_SHIKYU_RITSU1", SqlDbType.Decimal, 3, );
            //updParam.SetParam("@SHOYO_GINKO_CD1", SqlDbType.Decimal, 4, );
            //updParam.SetParam("@SHOYO_SHITEN_CD1", SqlDbType.Decimal, 4, );
            //updParam.SetParam("@SHOYO_YOKIN_SHUMOKU1", SqlDbType.Decimal, 1, );
            //updParam.SetParam("@SHOYO_KOZA_BANGO1", SqlDbType.VarChar, 8, );
            //updParam.SetParam("@SHOYO_KOZA_MEIGININ_KANJI1", SqlDbType.VarChar, 30, );
            //updParam.SetParam("@SHOYO_KOZA_MEIGININ_KANA1", SqlDbType.VarChar, 30, );
            //updParam.SetParam("@SHOYO_KEIYAKUSHA_BANGO1", SqlDbType.VarChar, 15, );
            //updParam.SetParam("@SHOYO_SHIKYU_HOHO2", SqlDbType.Decimal, 1, );
            //updParam.SetParam("@SHOYO_SHIKYU_KINGAKU2", SqlDbType.Decimal, 9, );
            //updParam.SetParam("@SHOYO_SHIKYU_RITSU2", SqlDbType.Decimal, 3, );
            //updParam.SetParam("@SHOYO_GINKO_CD2", SqlDbType.Decimal, 4, );
            //updParam.SetParam("@SHOYO_SHITEN_CD2", SqlDbType.Decimal, 4, );
            //updParam.SetParam("@SHOYO_YOKIN_SHUMOKU2", SqlDbType.Decimal, 1, );
            //updParam.SetParam("@SHOYO_KOZA_BANGO2", SqlDbType.VarChar, 8, );
            //updParam.SetParam("@SHOYO_KOZA_MEIGININ_KANJI2", SqlDbType.VarChar, 30, );
            //updParam.SetParam("@SHOYO_KOZA_MEIGININ_KANA2", SqlDbType.VarChar, 30, );
            //updParam.SetParam("@SHOYO_KEIYAKUSHA_BANGO2", SqlDbType.Decimal, 15, );
            updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWDATE");

            alParams.Add(updParam);

            return alParams;
        }

        /// <summary>
        /// 今ログインしている人のデータを取得
        /// </summary>
        /// <returns>roginUser</returns>
        private DataRow GetroginUserdetail()
        {
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@TANTOSHA_CD", SqlDbType.Decimal, 4, this.UInfo.UserCd);
            StringBuilder sql = new StringBuilder();
            sql.Append(" SELECT ");
            sql.Append(" * ");
            sql.Append(" FROM ");
            sql.Append(" TB_CM_TANTOSHA ");
            sql.Append(" WHERE ");
            sql.Append(" KAISHA_CD = @KAISHA_CD AND ");
            sql.Append(" TANTOSHA_CD = @TANTOSHA_CD ");
            DataTable dt = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);

            if (dt.Rows.Count == 0)
            {
                Msg.Info("ユーザ情報取得に失敗しました。");
            }
            DataRow dr = dt.Rows[0];
            return dr;
        }
        #endregion

        #region privateメソッド(入力チェック)
        /// <summary>
        /// 社員コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShainCd()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtShainCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 既に存在するコードを入力した場合はエラーとする
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
            dpc.SetParam("@SHAIN_CD", SqlDbType.Decimal, 6, this.txtShainCd.Text);
            StringBuilder where = new StringBuilder();
            where.Append(" KAISHA_CD = @KAISHA_CD");
            where.Append(" AND SHAIN_CD = @SHAIN_CD");
            DataTable dt =
                this.Dba.GetDataTableByConditionWithParams("SHAIN_CD",
                    "TB_KY_SHAIN_JOHO", Util.ToString(where), dpc);
            if (dt.Rows.Count > 0)
            {
                Msg.Error("既に存在する社員コードと重複しています。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 社員名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShainNm()
        {
            return IsValidText(this.txtShainNm);
        }

        /// <summary>
        /// 社員カナ名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShainKanaNm()
        {
            return IsValidText(this.txtShainKanaNm);
        }

        /// <summary>
        /// 郵便番号1の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidYubinBango1()
        {
            return IsValidText(this.txtYubinBango1);
        }

        /// <summary>
        /// 郵便番号2の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidYubinBango2()
        {
            return IsValidText(this.txtYubinBango2);
        }

        /// <summary>
        /// 住所1の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidJusho1()
        {
            return IsValidText(this.txtJusho1);
        }

        /// <summary>
        /// 住所2の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidJusho2()
        {
            return IsValidText(this.txtJusho2);
        }

        /// <summary>
        /// 電話番号の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidDenwaBango()
        {
            return IsValidText(this.txtDenwaBango);
        }

        /// <summary>
        /// 一覧表示の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidhyoji()
        {
            // 未入力は0とする
            if (ValChk.IsEmpty(this.txthyoji.Text))
            {
                this.txthyoji.Text = "0";
            }

            // 0,1のみ入力許可
            if (!ValChk.IsNumber(this.txthyoji.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            int intval = Util.ToInt(this.txthyoji.Text);
            if (intval < 0 || intval > 1)
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// マイナンバーの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidMyNumber()
        {
            return IsValidText(this.txtMyNumber);
        }

        /// <summary>
        /// 部門コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidBumonCd()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(Util.ToString(this.txtBumonCd.Text)))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 空白だった場合0を入れる
            if (this.txtBumonCd.Text == "")
            {
                this.txtBumonCd.Text = "0";
            }
            // 存在しないコードを入力されたらエラー
            else if (this.txtBumonCd.Text != "0")
            {
                string name = this.Dba.GetName(this.UInfo, "TB_KY_BUMON", " ", Util.ToString(this.txtBumonCd.Text));
                if (ValChk.IsEmpty(name))
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 部課コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidBukaCd()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(Util.ToString(this.txtBukaCd.Text)))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 空白だった場合0を入れる
            if (this.txtBukaCd.Text == "")
            {
                this.txtBukaCd.Text = "0";
            }
            // 存在しないコードを入力されたらエラー
            else if (this.txtBukaCd.Text != "0")
            {
                string name = this.Dba.GetBukaNm(
                    this.UInfo, Util.ToString(this.txtBumonCd.Text), Util.ToString(this.txtBukaCd.Text));
                if (ValChk.IsEmpty(name))
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 役職コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidYakushokuCd()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(Util.ToString(this.txtYakushokuCd.Text)))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 空白だった場合0を入れる
            if (this.txtYakushokuCd.Text == "")
            {
                this.txtYakushokuCd.Text = "0";
            }
            // 存在しないコードを入力されたらエラー
            else if (this.txtYakushokuCd.Text != "0")
            {
                string name = this.Dba.GetName(this.UInfo, "TB_KY_YAKUSHOKU", " ", Util.ToString(this.txtYakushokuCd.Text));
                if (ValChk.IsEmpty(name))
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 給与形態の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidKyuyoKeitai()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(Util.ToString(this.txtKyuyoKeitai.Text)))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 空白だった場合0を入れる
            if (this.txtKyuyoKeitai.Text == "")
            {
                this.txtKyuyoKeitai.Text = "0";
            }
            // 存在しないコードを入力されたらエラー
            else if (this.txtKyuyoKeitai.Text != "0")
            {
                string name =
                    this.Dba.GetKyuyoKbnNm("1", Util.ToString(this.txtKyuyoKeitai.Text));
                if (ValChk.IsEmpty(name))
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 保険証番号の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidKenkoHokenshoBango()
        {
            return IsValidNumber(this.txtKenkoHokenshoBango);
        }

        /// <summary>
        /// 年金番号の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidKoseiNenkinBango()
        {
            return IsValidNumber(this.txtKoseiNenkinBango);
        }

        /// <summary>
        /// 市町村コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidJuminzeiShichosonCd()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(Util.ToString(this.txtJuminzeiShichosonCd.Text)))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 空白だった場合0を入れる
            if (this.txtJuminzeiShichosonCd.Text == "")
            {
                this.txtJuminzeiShichosonCd.Text = "0";
            }
            // 存在しないコードを入力されたらエラー
            else if (this.txtJuminzeiShichosonCd.Text != "0")
            {
                string name =
                    this.Dba.GetName(this.UInfo, "TB_KY_SHICHOSON", " ", Util.ToString(this.txtJuminzeiShichosonCd.Text));
                if (ValChk.IsEmpty(name))
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 本人区分①の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidHonninKubun1()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtHonninKubun1.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 空白だった場合0を入れる
            if (this.txtHonninKubun1.Text == "")
            {
                this.txtHonninKubun1.Text = "0";
            }
            // 存在しないコードを入力されたらエラー
            else if (this.txtHonninKubun1.Text != "0")
            {
                string name = this.Dba.GetKyuyoKbnNm("2", Util.ToString(this.txtHonninKubun1.Text));
                if (ValChk.IsEmpty(name))
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 本人区分②の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidHonninKubun2()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtHonninKubun2.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 空白だった場合0を入れる
            if (this.txtHonninKubun2.Text == "")
            {
                this.txtHonninKubun2.Text = "0";
            }
            // 存在しないコードを入力されたらエラー
            else if (this.txtHonninKubun2.Text != "0")
            {
                string name = this.Dba.GetKyuyoKbnNm("3", Util.ToString(this.txtHonninKubun2.Text));
                if (ValChk.IsEmpty(name))
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 本人区分③の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidHonninKubun3()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtHonninKubun3.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 空白だった場合0を入れる
            if (this.txtHonninKubun3.Text == "")
            {
                this.txtHonninKubun3.Text = "0";
            }
            // 存在しないコードを入力されたらエラー
            else if (this.txtHonninKubun3.Text != "0")
            {
                string name = this.Dba.GetKyuyoKbnNm("4", Util.ToString(this.txtHonninKubun3.Text));
                if (ValChk.IsEmpty(name))
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 配偶者区分の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidHaigushaKubun()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtHaigushaKubun.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 空白だった場合0を入れる
            if (this.txtHaigushaKubun.Text == "")
            {
                this.txtHaigushaKubun.Text = "0";
            }
            // 存在しないコードを入力されたらエラー
            else if (this.txtHaigushaKubun.Text != "0")
            {
                string name = this.Dba.GetKyuyoKbnNm("5", Util.ToString(this.txtHaigushaKubun.Text));
                if (ValChk.IsEmpty(name))
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 摘要1の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidTekiyo1()
        {
            return IsValidText(this.txtTekiyo1);
        }

        /// <summary>
        /// 摘要2の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidTekiyo2()
        {
            return IsValidText(this.txtTekiyo2);
        }

        /// <summary>
        /// 扶養親族5人目以降の個人番号の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidIkouNo()
        {
            return IsValidText(this.txtIkouNo);
        }

        /// <summary>
        /// 16歳未満5人目以降の個人番号の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidMimanIkouNo()
        {
            return IsValidText(this.txtIkouMimanNo);
        }

        /// <summary>
        /// 前職備考欄の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidZenshokuBikou()
        {
            return IsValidText(this.txtZenBikou);
        }

        /// <summary>
        /// 給与種別の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidKyuyoShubetsu()
        {
            return IsValidText(this.txtKyuyoShubetsu);
        }

        /// <summary>
        /// 銀行コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidGinkoCd(FsiTextBox textBox)
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(textBox.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 空白だった場合0を入れる
            if (textBox.Text == "")
            {
                textBox.Text = "0";
            }
            // 存在しないコードを入力されたらエラー
            else if (textBox.Text != "0")
            {
                string name = this.Dba.GetName(this.UInfo, "TB_KY_GINKO", "", textBox.Text);
                if (ValChk.IsEmpty(name))
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 支店コードの入力チェック
        /// </summary>
        /// <param name="ginkoCd">銀行コード</param>
        /// <param name="shitenCd">支店コード</param>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShitenCd(FsiTextBox ginkoCd, FsiTextBox shitenCd)
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(shitenCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 空白だった場合0を入れる
            if (shitenCd.Text == "")
            {
                shitenCd.Text = "0";
            }
            // 存在しないコードを入力されたらエラー
            else if (shitenCd.Text != "0")
            {
                string name = this.Dba.GetGinkoShitenNm(ginkoCd.Text, shitenCd.Text);
                if (ValChk.IsEmpty(name))
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 口座番号の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidKozaBango(FsiTextBox textBox)
        {
            return IsValidNumber(textBox);
        }

        /// <summary>
        /// 口座名義人の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidKozaMeigininKanji(FsiTextBox textBox)
        {
            return IsValidText(textBox);
        }

        /// <summary>
        /// 口座名義人カナの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidKozaMeigininKana(FsiTextBox textBox)
        {
            return IsValidText(textBox);
        }

        /// <summary>
        /// 契約者番号の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidKeiyakushaBango(FsiTextBox textBox)
        {
            return IsValidText(textBox);
        }

        /// <summary>
        /// グリッド金額フィールドの入力チェック
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private bool IsValidTeateCurrency(object value)
        {
            // 金額値のみ入力可能
            if (!ValChk.IsNumber(Util.ToDecimal(Util.ToString(value))))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 入力サイズ
            if (!ValChk.IsWithinLength(Util.ToDecimal(Util.ToString(value)), 8))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 文字フィールドの入力チェック
        /// </summary>
        /// <param name="textBox"></param>
        /// <returns></returns>
        private bool IsValidText(FsiTextBox textBox)
        {
            // 入力サイズ
            if (!ValChk.IsWithinLength(textBox.Text, textBox.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 金額フィールドの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidCurrency(FsiTextBox textBox)
        {
            // 金額値のみ入力可能
            if (!ValChk.IsNumber(Util.ToDecimal(textBox.Text)))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 入力サイズ(書式なし数値で判定)
            if (!ValChk.IsWithinLength(Util.ToDecimal(textBox.Text), textBox.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 数値フィールドの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidNumber(FsiTextBox textBox)
        {
            // 数値のみ入力可能
            if (!ValChk.IsNumber(textBox.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 入力サイズ値
            if (!ValChk.IsWithinLength(textBox.Text, textBox.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// グリッド全編集行を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateDataGridViewAll()
        {
            // 支給項目グリッド
            foreach (DataRow dr in ((DataTable)dgvShikyu.DataSource).Rows)
            {
                if (!IsValidTeateCurrency(dr["TEATE_KOMOKU"]))
                {
                    dgvShikyu.Focus();
                    return false;
                }
            }
            // 控除項目グリッド
            foreach (DataRow dr in ((DataTable)dgvKojo.DataSource).Rows)
            {
                if (!IsValidTeateCurrency(dr["TEATE_KOMOKU"]))
                {
                    dgvKojo.Focus();
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 必須項目のチェック
            if (this.txtShainCd.Text == "0")
            {
                Msg.Notice("ゼロは使用できません。");
                this.txtShainCd.Focus();
                return false;
            }
            if (this.txtKyuyoKeitai.Text == "0")
            {
                Msg.Notice("ゼロは使用できません。");
                this.txtKyuyoKeitai.Focus();
                return false;
            }

            if (MODE_NEW.Equals(this.Par1))
            {
                // 社員コードのチェック
                if (!IsValidShainCd())
                {
                    this.txtShainCd.Focus();
                    return false;
                }
            }

            #region 基本情報のチェック

            // 氏名のチェック
            if (!IsValidShainNm())
            {
                this.txtShainNm.Focus();
                return false;
            }
            // カナ名のチェック
            if (!IsValidShainNm())
            {
                this.txtShainKanaNm.Focus();
                return false;
            }
            // 郵便番号のチェック
            if (!IsValidYubinBango1())
            {
                this.txtYubinBango1.Focus();
                return false;
            }
            if (!IsValidYubinBango2())
            {
                this.txtYubinBango2.Focus();
                return false;
            }
            // 住所のチェック
            if (!IsValidJusho1())
            {
                this.txtJusho1.Focus();
                return false;
            }
            if (!IsValidJusho2())
            {
                this.txtJusho2.Focus();
                return false;
            }
            // 電話番号のチェック
            if (!IsValidDenwaBango())
            {
                this.txtDenwaBango.Focus();
                return false;
            }
            // 一覧表示のチェック
            if (!IsValidhyoji())
            {
                this.txthyoji.Focus();
                return false;
            }
            // マイナンバーのチェック
            if (!IsValidMyNumber())
            {
                this.txtMyNumber.Focus();
                return false;
            }
            // 部門コードのチェック
            if (!IsValidBumonCd())
            {
                this.txtBumonCd.Focus();
                return false;
            }
            // 部課コードのチェック
            if (!IsValidBukaCd())
            {
                this.txtBukaCd.Focus();
                return false;
            }
            // 役職コードのチェック
            if (!IsValidYakushokuCd())
            {
                this.txtYakushokuCd.Focus();
                return false;
            }
            // 給与形態コードのチェック
            if (!IsValidKyuyoKeitai())
            {
                this.txtKyuyoKeitai.Focus();
                return false;
            }

            #endregion

            #region 社会保険・住民税のチェック

            // 保険証番号のチェック
            if (!IsValidKenkoHokenshoBango())
            {
                this.txtKenkoHokenshoBango.Focus();
                return false;
            }
            // 健康保険標準報酬月額のチェック
            if (!IsValidCurrency(this.txtKenkoHokenHyojunHoshuGtgk))
            {
                this.txtKenkoHokenHyojunHoshuGtgk.Focus();
                return false;
            }
            // 健康保険料のチェック
            if (!IsValidCurrency(this.txtKenkoHokenryo))
            {
                this.txtKenkoHokenryo.Focus();
                return false;
            }
            // 年金番号のチェック
            if (!IsValidKoseiNenkinBango())
            {
                this.txtKoseiNenkinBango.Focus();
                return false;
            }
            // 厚生年金標準報酬月額のチェック
            if (!IsValidCurrency(this.txtKoseiNenkinHyojunHoshuGtgk))
            {
                this.txtKoseiNenkinHyojunHoshuGtgk.Focus();
                return false;
            }
            // 厚生年金保険料のチェック
            if (!IsValidCurrency(this.txtKoseiNenkinHokenryo))
            {
                this.txtKoseiNenkinHokenryo.Focus();
                return false;
            }
            // 住民税６月分のチェック
            if (!IsValidCurrency(this.txtJuminzei6gatsubun))
            {
                this.txtJuminzei6gatsubun.Focus();
                return false;
            }
            // 住民税７月分以降のチェック
            if (!IsValidCurrency(this.txtJuminzei7gatsubunIko))
            {
                this.txtJuminzei7gatsubunIko.Focus();
                return false;
            }
            #endregion

            #region 所得税のチェック

            // 摘要①のチェック
            if (!IsValidTekiyo1())
            {
                this.txtTekiyo1.Focus();
                return false;
            }
            // 摘要②のチェック
            if (!IsValidTekiyo2())
            {
                this.txtTekiyo2.Focus();
                return false;
            }
            // 給与種別のチェック
            if (!IsValidKyuyoShubetsu())
            {
                this.txtKyuyoShubetsu.Focus();
                return false;
            }

            // 扶養親族備考欄のチェック
            if (!IsValidIkouNo())
            {
                this.txtIkouNo.Focus();
                return false;
            }
            // 16歳未満の備考欄のチェック
            if (!IsValidMimanIkouNo())
            {
                this.txtIkouMimanNo.Focus();
                return false;
            }
            // 前職備考欄のチェック
            if (!IsValidZenshokuBikou())
            {
                this.txtZenBikou.Focus();
                return false;
            }
            #endregion

            #region 諸手当のチェック

            ValidateDataGridViewAll();

            #endregion

            #region 振込銀行のチェック

            // 支給方法１・支給金額のチェック
            if (!IsValidCurrency(this.txtKyuyoShikyuKingaku1))
            {
                this.txtKyuyoShikyuKingaku1.Focus();
                return false;
            }
            // 支給方法１・銀行コードのチェック
            if (!IsValidGinkoCd(this.txtKyuyoGinkoCd1))
            {
                this.txtKyuyoGinkoCd1.Focus();
                return false;
            }
            // 支給方法１・支店コードのチェック
            if (!IsValidShitenCd(this.txtKyuyoGinkoCd1, this.txtKyuyoShitenCd1))
            {
                this.txtKyuyoShitenCd1.Focus();
                return false;
            }
            // 支給方法１・口座番号のチェック
            if (!IsValidKozaBango(this.txtKyuyoKozaBango1))
            {
                this.txtKyuyoKozaBango1.Focus();
                return false;
            }
            // 支給方法１・口座名義人漢字のチェック
            if (!IsValidKozaMeigininKanji(this.txtKyuyoKozaMeigininKanji1))
            {
                this.txtKyuyoKozaMeigininKanji1.Focus();
                return false;
            }
            // 支給方法１・口座名義人カナのチェック
            if (!IsValidKozaMeigininKanji(this.txtKyuyoKozaMeigininKana1))
            {
                this.txtKyuyoKozaMeigininKana1.Focus();
                return false;
            }
            // 支給方法１・契約者番号のチェック
            if (!IsValidKeiyakushaBango(this.txtKyuyoKeiyakushaBango2))
            {
                this.txtKyuyoKeiyakushaBango2.Focus();
                return false;
            }

            // 支給方法２・支給金額のチェック
            if (!IsValidCurrency(this.txtKyuyoShikyuKingaku2))
            {
                this.txtKyuyoShikyuKingaku2.Focus();
                return false;
            }
            // 支給方法２・銀行コードのチェック
            if (!IsValidGinkoCd(this.txtKyuyoGinkoCd2))
            {
                this.txtKyuyoGinkoCd2.Focus();
                return false;
            }
            // 支給方法２・支店コードのチェック
            if (!IsValidShitenCd(this.txtKyuyoGinkoCd2, this.txtKyuyoShitenCd2))
            {
                this.txtKyuyoShitenCd2.Focus();
                return false;
            }
            // 支給方法２・口座番号のチェック
            if (!IsValidKozaBango(this.txtKyuyoKozaBango2))
            {
                this.txtKyuyoKozaBango2.Focus();
                return false;
            }
            // 支給方法２・口座名義人漢字のチェック
            if (!IsValidKozaMeigininKanji(this.txtKyuyoKozaMeigininKanji2))
            {
                this.txtKyuyoKozaMeigininKanji2.Focus();
                return false;
            }
            // 支給方法２・口座名義人カナのチェック
            if (!IsValidKozaMeigininKanji(this.txtKyuyoKozaMeigininKana2))
            {
                this.txtKyuyoKozaMeigininKana2.Focus();
                return false;
            }
            // 支給方法２・契約者番号のチェック
            if (!IsValidKeiyakushaBango(this.txtKyuyoKeiyakushaBango2))
            {
                this.txtKyuyoKeiyakushaBango2.Focus();
                return false;
            }

            #endregion

            return true;
        }
        #endregion
    }
}