﻿using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.ky.kycm1021
{
    /// <summary>
    /// 社員の登録(KYCM1021)
    /// </summary>
    public partial class KYCM1021 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(コード検索)
        /// </summary>
        private const string MODE_CD_SRC = "1";

        /// <summary>
        /// 検索画面用画面タイトル
        /// </summary>
        private const string SEARCH_TITLE = "社員の検索";
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KYCM1021()
        {
            InitializeComponent();
        }
        #endregion

        #region protectedメソッド(継承)

        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                // Par1が"1"の場合、コード検索画面としての挙動をする
                // フォームのキャプションにラベルタイトルのtextを設定する
                this.Text = SEARCH_TITLE;
                // サイズを縮める
                this.Size = new Size(574, 550);
                //タイトルを非表示
                this.lblTitle.Visible = false;
                // フォームの配置を上へ移動する
                this.lblShainCd.Location = new System.Drawing.Point(14, 17);
                this.txtShainCd.Location = new System.Drawing.Point(98, 17);
                this.lblKanaName.Location = new System.Drawing.Point(164, 17);
                this.txtKanaName.Location = new System.Drawing.Point(249, 17);
                //this.ckboxAllHyoji.Location = new System.Drawing.Point(453, 14);
                this.groupBox1.Location = new System.Drawing.Point(435, 10);
                //this.dgvList.Location = new System.Drawing.Point(12, 45);
                this.dgvList.Location = new System.Drawing.Point(12, 118);
                // EscapeとF1のみ表示
                this.btnEsc.Location = this.btnF1.Location;
                this.btnF1.Location = this.btnF2.Location;
                this.btnF2.Visible = false;
                this.btnF3.Visible = false;
                this.btnF4.Visible = false;
                this.btnF5.Visible = false;
                this.btnF6.Visible = false;
                this.btnF7.Visible = false;
                this.btnF8.Visible = false;
                this.btnF9.Visible = false;
                this.btnF10.Visible = false;
                this.btnF11.Visible = false;
                this.btnF12.Visible = false;
                this.ShowFButton = true;
            }

            SearchData(true);

            // 社員一覧にフォーカス
            this.dgvList.Focus();
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                // Par1が"1"の場合、ダイアログとしての処理結果を返却する
                this.DialogResult = DialogResult.Cancel;
            }
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        public override void PressF1()
        {
            // カナ名にフォーカスを戻す
            this.txtKanaName.Focus();
            this.txtKanaName.SelectAll();
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // メンテ機能で立ち上げている場合のみ社員登録画面を立ち上げる
            if (ValChk.IsEmpty(this.Par1))
            {
                // 社員登録画面の起動
                EditShain(string.Empty);
            }
        }
        #endregion

        #region イベント
        /// <summary>
        /// 表示ボタンの変更時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ckboxAllHyoji_CheckedChanged(object sender, System.EventArgs e)
        {
            // 入力された情報を元に検索する
            SearchData(false);
        }

        /// <summary>
        /// 社員コード検証時の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShainCd_Validating(object sender, CancelEventArgs e)
        {
            SearchData(false);
        }

        /// <summary>
        /// カナ名検証時の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKanaName_Validating(object sender, CancelEventArgs e)
        {
            // 入力された情報を元に検索する
            SearchData(false);
        }

        /// <summary>
        /// グリッドでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (MODE_CD_SRC.Equals(this.Par1))
                {
                    ReturnVal();
                }
                else
                {
                    EditShain(Util.ToString(this.dgvList.SelectedRows[0].Cells["社員コード"].Value));
                    e.Handled = true;
                }
            }
        }

        /// <summary>
        /// グリッドのセルダブルクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                ReturnVal();
            }
            else
            {
                EditShain(Util.ToString(this.dgvList.SelectedRows[0].Cells["社員コード"].Value));
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// データを検索する
        /// </summary>
        /// <param name="isInitial">初期化判定</param>
        private void SearchData(bool isInitial)
        {
            // 社員情報マスタからデータを取得して表示
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder where = new StringBuilder("");
            // 会社コード指定
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
            where.Append("KAISHA_CD = @KAISHA_CD");
            if (isInitial)
            {
                //// 初期処理の場合　表示フラグが1の社員は表示しない
                //where.Append(" AND KY.HYOJI_FLG = 0");
                // 初期表示は在職者のみ表示する
                where.Append(" AND KY.TAISHOKU_NENGAPPI IS NULL ");
            }
            else
            {
                // 社員コード条件指定
                if (Util.ToDecimal(Util.ToString(this.txtShainCd.Text)) > 0)
                {
                    dpc.SetParam("@SHAIN_CD", SqlDbType.Decimal, 6, this.txtShainCd.Text);
                    where.Append(" AND SHAIN_CD = @SHAIN_CD");
                }
                // カナ名条件指定
                if (!ValChk.IsEmpty(this.txtKanaName.Text))
                {
                    dpc.SetParam("@SHAIN_KANA_NM", SqlDbType.VarChar, 30, "%" + this.txtKanaName.Text + "%");
                    where.Append(" AND KY.SHAIN_KANA_NM LIKE @SHAIN_KANA_NM");
                }
                //if (this.ckboxAllHyoji.Checked == false)
                //{
                //    where.Append(" AND KY.HYOJI_FLG = 0");
                //}
                if (this.rdoZaishoku.Checked)
                {
                    where.Append(" AND KY.TAISHOKU_NENGAPPI IS NULL ");
                }
                else if (this.rdoTaishoku.Checked)
                {
                    where.Append(" AND KY.TAISHOKU_NENGAPPI IS NOT NULL ");
                }
            }

            string cols = "KY.SHAIN_CD AS 社員コード";
            cols += ", KY.SHAIN_NM AS 社員名";
            cols += ", KY.SHAIN_KANA_NM AS 社員カナ名";
            string from = "TB_KY_SHAIN_JOHO AS KY";

            DataTable dt =
                this.Dba.GetDataTableByConditionWithParams(cols, from,
                    Util.ToString(where), "KY.SHAIN_CD", dpc);

            // 初期処理以外の場合、該当データがなければエラーメッセージを表示
            if (dt.Rows.Count == 0)
            {
                if (!isInitial)
                {
                    Msg.Info("該当データがありません。");
                }

                dt.Rows.Add(dt.NewRow());
            }

            this.dgvList.DataSource = dt;

            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvList.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // フォントを設定する
            this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F, FontStyle.Regular);
            this.dgvList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.dgvList.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F);

            // 列幅を設定する
            this.dgvList.Columns[0].Width = 110;
            this.dgvList.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvList.Columns[1].Width = 200;
            this.dgvList.Columns[2].Width = 200;
        }

        /// <summary>
        /// 社員情報を追加編集する
        /// </summary>
        /// <param name="code">社員コード(空：新規登録、以外：編集)</param>
        private void EditShain(string code)
        {
            KYCM1022 frmKYCM1022;

            if (ValChk.IsEmpty(code))
            {
                // 新規登録モードで登録画面を起動
                frmKYCM1022 = new KYCM1022("1");
            }
            else
            {
                // 編集モードで登録画面を起動
                frmKYCM1022 = new KYCM1022("2");
                frmKYCM1022.InData = code;
            }

            DialogResult result = frmKYCM1022.ShowDialog(this);

            if (result == DialogResult.OK)
            {
                // データを再検索する
                SearchData(false);
                // 元々選択していたデータを選択
                for (int i = 0; i < this.dgvList.Rows.Count; i++)
                {
                    if (code.Equals(Util.ToString(this.dgvList.Rows[i].Cells["社員コード"].Value)))
                    {
                        this.dgvList.Rows[i].Selected = true;
                        break;
                    }
                }
                // Gridに再度フォーカスをセット
                this.ActiveControl = this.dgvList;
                this.dgvList.Focus();
            }
        }

        /// <summary>
        /// 呼び出し元に戻り値を返す
        /// </summary>
        private void ReturnVal()
        {
            this.OutData = new string[3] { 
                Util.ToString(this.dgvList.SelectedRows[0].Cells["社員コード"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["社員名"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["社員カナ名"].Value)
            };
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        #endregion
    }
}
