﻿using System.Data;

using jp.co.fsi.common.report;

namespace jp.co.fsi.ky.kykr1011
{
    /// <summary>
    /// KYKR1011R の概要の説明です。
    /// </summary>
    public partial class KYKR1011R : BaseReport
    {

        public KYKR1011R(DataTable tgtData)
            : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }

        public KYKR1011R(DataTable tgtData, string repID)
            : base(tgtData, repID)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }

    }
}
