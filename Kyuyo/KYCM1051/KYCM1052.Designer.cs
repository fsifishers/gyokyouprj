﻿namespace jp.co.fsi.ky.kycm1051
{
    partial class KYCM1052
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtGinkoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGinkoCd = new System.Windows.Forms.Label();
            this.lblMODE = new System.Windows.Forms.Label();
            this.txtGinkoKanaNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGinkoKanaNm = new System.Windows.Forms.Label();
            this.txtGinkoNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGinkoNm = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(381, 23);
            this.lblTitle.Text = "銀行の登録";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(5, 77);
            this.pnlDebug.Size = new System.Drawing.Size(414, 100);
            // 
            // txtGinkoCd
            // 
            this.txtGinkoCd.AutoSizeFromLength = true;
            this.txtGinkoCd.DisplayLength = null;
            this.txtGinkoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGinkoCd.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtGinkoCd.Location = new System.Drawing.Point(127, 13);
            this.txtGinkoCd.MaxLength = 4;
            this.txtGinkoCd.Name = "txtGinkoCd";
            this.txtGinkoCd.Size = new System.Drawing.Size(64, 23);
            this.txtGinkoCd.TabIndex = 0;
            this.txtGinkoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGinkoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtGinkoCd_Validating);
            // 
            // lblGinkoCd
            // 
            this.lblGinkoCd.BackColor = System.Drawing.Color.Silver;
            this.lblGinkoCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGinkoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGinkoCd.Location = new System.Drawing.Point(12, 13);
            this.lblGinkoCd.Name = "lblGinkoCd";
            this.lblGinkoCd.Size = new System.Drawing.Size(115, 23);
            this.lblGinkoCd.TabIndex = 1;
            this.lblGinkoCd.Text = "銀行コード";
            this.lblGinkoCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMODE
            // 
            this.lblMODE.BackColor = System.Drawing.Color.White;
            this.lblMODE.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMODE.Location = new System.Drawing.Point(318, 13);
            this.lblMODE.Name = "lblMODE";
            this.lblMODE.Size = new System.Drawing.Size(78, 23);
            this.lblMODE.TabIndex = 902;
            this.lblMODE.Text = "【編集】";
            this.lblMODE.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtGinkoKanaNm
            // 
            this.txtGinkoKanaNm.AutoSizeFromLength = false;
            this.txtGinkoKanaNm.DisplayLength = null;
            this.txtGinkoKanaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGinkoKanaNm.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtGinkoKanaNm.Location = new System.Drawing.Point(124, 94);
            this.txtGinkoKanaNm.MaxLength = 30;
            this.txtGinkoKanaNm.Name = "txtGinkoKanaNm";
            this.txtGinkoKanaNm.Size = new System.Drawing.Size(269, 23);
            this.txtGinkoKanaNm.TabIndex = 905;
            this.txtGinkoKanaNm.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LastControl_KeyDown);
            this.txtGinkoKanaNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtGinkoKanaNm_Validating);
            // 
            // lblGinkoKanaNm
            // 
            this.lblGinkoKanaNm.BackColor = System.Drawing.Color.Silver;
            this.lblGinkoKanaNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGinkoKanaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGinkoKanaNm.Location = new System.Drawing.Point(9, 94);
            this.lblGinkoKanaNm.Name = "lblGinkoKanaNm";
            this.lblGinkoKanaNm.Size = new System.Drawing.Size(115, 23);
            this.lblGinkoKanaNm.TabIndex = 906;
            this.lblGinkoKanaNm.Text = "カナ名";
            this.lblGinkoKanaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtGinkoNm
            // 
            this.txtGinkoNm.AutoSizeFromLength = false;
            this.txtGinkoNm.DisplayLength = null;
            this.txtGinkoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGinkoNm.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtGinkoNm.Location = new System.Drawing.Point(125, 54);
            this.txtGinkoNm.MaxLength = 30;
            this.txtGinkoNm.Name = "txtGinkoNm";
            this.txtGinkoNm.Size = new System.Drawing.Size(269, 23);
            this.txtGinkoNm.TabIndex = 903;
            this.txtGinkoNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtGinkoNm_Validating);
            // 
            // lblGinkoNm
            // 
            this.lblGinkoNm.BackColor = System.Drawing.Color.Silver;
            this.lblGinkoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGinkoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGinkoNm.Location = new System.Drawing.Point(10, 54);
            this.lblGinkoNm.Name = "lblGinkoNm";
            this.lblGinkoNm.Size = new System.Drawing.Size(115, 23);
            this.lblGinkoNm.TabIndex = 904;
            this.lblGinkoNm.Text = "銀行名";
            this.lblGinkoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // KYCM1052
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(406, 180);
            this.Controls.Add(this.txtGinkoKanaNm);
            this.Controls.Add(this.lblGinkoKanaNm);
            this.Controls.Add(this.txtGinkoNm);
            this.Controls.Add(this.lblGinkoNm);
            this.Controls.Add(this.lblMODE);
            this.Controls.Add(this.txtGinkoCd);
            this.Controls.Add(this.lblGinkoCd);
            this.Name = "KYCM1052";
            this.Text = "銀行の登録";
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblGinkoCd, 0);
            this.Controls.SetChildIndex(this.txtGinkoCd, 0);
            this.Controls.SetChildIndex(this.lblMODE, 0);
            this.Controls.SetChildIndex(this.lblGinkoNm, 0);
            this.Controls.SetChildIndex(this.txtGinkoNm, 0);
            this.Controls.SetChildIndex(this.lblGinkoKanaNm, 0);
            this.Controls.SetChildIndex(this.txtGinkoKanaNm, 0);
            this.pnlDebug.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private jp.co.fsi.common.controls.FsiTextBox txtGinkoCd;
        private System.Windows.Forms.Label lblGinkoCd;
        private System.Windows.Forms.Label lblMODE;
        private common.controls.FsiTextBox txtGinkoKanaNm;
        private System.Windows.Forms.Label lblGinkoKanaNm;
        private common.controls.FsiTextBox txtGinkoNm;
        private System.Windows.Forms.Label lblGinkoNm;
    }
}