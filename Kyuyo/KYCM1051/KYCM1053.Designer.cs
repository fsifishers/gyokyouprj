﻿namespace jp.co.fsi.ky.kycm1051
{
    partial class KYCM1053
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblGinkoCdFr = new System.Windows.Forms.Label();
            this.lblCodeBet1 = new System.Windows.Forms.Label();
            this.txtGinkoCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtGinkoCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGinkoCdTo = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pnlDebug.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(542, 23);
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(5, 39);
            this.pnlDebug.Size = new System.Drawing.Size(559, 100);
            // 
            // lblGinkoCdFr
            // 
            this.lblGinkoCdFr.BackColor = System.Drawing.Color.Silver;
            this.lblGinkoCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGinkoCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGinkoCdFr.Location = new System.Drawing.Point(80, 27);
            this.lblGinkoCdFr.Name = "lblGinkoCdFr";
            this.lblGinkoCdFr.Size = new System.Drawing.Size(180, 19);
            this.lblGinkoCdFr.TabIndex = 915;
            this.lblGinkoCdFr.Text = "先  頭";
            this.lblGinkoCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCodeBet1
            // 
            this.lblCodeBet1.AutoSize = true;
            this.lblCodeBet1.Location = new System.Drawing.Point(266, 31);
            this.lblCodeBet1.Name = "lblCodeBet1";
            this.lblCodeBet1.Size = new System.Drawing.Size(20, 13);
            this.lblCodeBet1.TabIndex = 916;
            this.lblCodeBet1.Text = "～";
            // 
            // txtGinkoCdFr
            // 
            this.txtGinkoCdFr.AutoSizeFromLength = false;
            this.txtGinkoCdFr.DisplayLength = null;
            this.txtGinkoCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtGinkoCdFr.Location = new System.Drawing.Point(26, 27);
            this.txtGinkoCdFr.MaxLength = 4;
            this.txtGinkoCdFr.Name = "txtGinkoCdFr";
            this.txtGinkoCdFr.Size = new System.Drawing.Size(48, 20);
            this.txtGinkoCdFr.TabIndex = 912;
            this.txtGinkoCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGinkoCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtGinkoCdFr_Validating);
            // 
            // txtGinkoCdTo
            // 
            this.txtGinkoCdTo.AutoSizeFromLength = false;
            this.txtGinkoCdTo.DisplayLength = null;
            this.txtGinkoCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtGinkoCdTo.Location = new System.Drawing.Point(289, 27);
            this.txtGinkoCdTo.MaxLength = 4;
            this.txtGinkoCdTo.Name = "txtGinkoCdTo";
            this.txtGinkoCdTo.Size = new System.Drawing.Size(48, 20);
            this.txtGinkoCdTo.TabIndex = 913;
            this.txtGinkoCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGinkoCdTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LastControl_KeyDown);
            this.txtGinkoCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtGinkoCdTo_Validating);
            // 
            // lblGinkoCdTo
            // 
            this.lblGinkoCdTo.BackColor = System.Drawing.Color.Silver;
            this.lblGinkoCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGinkoCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGinkoCdTo.Location = new System.Drawing.Point(343, 27);
            this.lblGinkoCdTo.Name = "lblGinkoCdTo";
            this.lblGinkoCdTo.Size = new System.Drawing.Size(180, 19);
            this.lblGinkoCdTo.TabIndex = 914;
            this.lblGinkoCdTo.Text = "最　後";
            this.lblGinkoCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblGinkoCdFr);
            this.groupBox1.Controls.Add(this.lblCodeBet1);
            this.groupBox1.Controls.Add(this.txtGinkoCdFr);
            this.groupBox1.Controls.Add(this.txtGinkoCdTo);
            this.groupBox1.Controls.Add(this.lblGinkoCdTo);
            this.groupBox1.Font = new System.Drawing.Font("MS UI Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBox1.Location = new System.Drawing.Point(8, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(553, 68);
            this.groupBox1.TabIndex = 917;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "銀行コード範囲";
            // 
            // KYCM1053
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(567, 142);
            this.Controls.Add(this.groupBox1);
            this.Name = "KYCM1053";
            this.Text = "銀行の印刷";
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.pnlDebug.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblGinkoCdFr;
        private System.Windows.Forms.Label lblCodeBet1;
        private common.controls.FsiTextBox txtGinkoCdFr;
        private common.controls.FsiTextBox txtGinkoCdTo;
        private System.Windows.Forms.Label lblGinkoCdTo;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}