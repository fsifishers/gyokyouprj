﻿using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.ky.kycm1051
{
    /// <summary>
    /// 銀行の登録(KYCM1052)
    /// </summary>
    public partial class KYCM1052 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(新規)
        /// </summary>
        private const string MODE_NEW = "1";

        /// <summary>
        /// モード(編集)
        /// </summary>
        private const string MODE_EDIT = "2";
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KYCM1052()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="par1">引数1</param>
        public KYCM1052(string par1)
            : base(par1)
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // タイトルは非表示
            this.lblTitle.Visible = false;

            // 引数：Par1／モード(1:新規、2:変更)、InData：銀行コード
            if (MODE_NEW.Equals(this.Par1))
            {
                // 新規モードの初期表示
                InitDispOnNew();
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 編集モードの初期表示
                InitDispOnEdit();
            }
            else
            {
                // 不正な起動として閉じる
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }
            this.ShowFButton = true;

            // ボタンを調整
            this.btnF1.Visible = true;
            this.btnF2.Visible = false;
            this.btnF3.Visible = true;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF6.Visible = true;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;

            this.btnEsc.Location = this.btnF1.Location;
            this.btnF1.Location = this.btnF2.Location;
            this.btnF6.Location = this.btnF4.Location;
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F3キー押下時処理
        /// </summary>
        public override void PressF3()
        {
            // 新規モードの場合は処理させない
            if (this.Par1.Equals(MODE_NEW)) return;

            // 確認メッセージを表示
            string msg = "削除しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // 削除用パラメータ
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@GINKO_CD", SqlDbType.Decimal, 4, Util.ToString(this.InData));

            try
            {
                // トランザクション開始
                this.Dba.BeginTransaction();

                if (MODE_EDIT.Equals(this.Par1))
                {
                    // データ削除
                    // 給与銀行マスタ
                    this.Dba.Delete("TB_KY_GINKO", "GINKO_CD = @GINKO_CD", dpc);
                }

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        
        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        public override void PressF6()
        {
            // 確認メッセージを表示
            string msg = (MODE_NEW.Equals(this.Par1) ? "登録" : "更新") + "しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // 入力値をバインドパラメータとしてセットする
            ArrayList alParamsKyGinko = SetKyGinkoParams();

            try
            {
                // トランザクション開始
                this.Dba.BeginTransaction();

                if (MODE_NEW.Equals(this.Par1))
                {
                    // データ登録
                    // 給与銀行マスタ
                    this.Dba.Insert("TB_KY_GINKO", (DbParamCollection)alParamsKyGinko[0]);
                }
                else if (MODE_EDIT.Equals(this.Par1))
                {
                    // データ更新
                    // 給与銀行マスタ
                    this.Dba.Update("TB_KY_GINKO",
                        (DbParamCollection)alParamsKyGinko[1],
                        "GINKO_CD = @GINKO_CD",
                        (DbParamCollection)alParamsKyGinko[0]);
                }

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 銀行コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGinkoCd_Validating(object sender, CancelEventArgs e)
        {
            // 入力値補正
            this.txtGinkoCd.Text = Util.ToString(Util.ToDecimal(this.txtGinkoCd.Text));

            if (!IsValidGinkoCd())
            {
                e.Cancel = true;
                this.txtGinkoCd.SelectAll();
            }
        }

        /// <summary>
        /// 銀行名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGinkoNm_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidGinkoNm())
            {
                e.Cancel = true;
                this.txtGinkoNm.SelectAll();
            }
        }

        /// <summary>
        /// カナ名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGinkoKanaNm_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidGinkoKanaNm())
            {
                e.Cancel = true;
                this.txtGinkoKanaNm.SelectAll();
            }
        }

        /// <summary>
        /// 画面の最後のコントールでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LastControl_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.btnF6.Enabled)
            {
                this.PressF6();
            }
        }
        #endregion

        #region privateメソッド

        /// <summary>
        /// 新規モードの初期表示
        /// </summary>
        private void InitDispOnNew()
        {
            // 初期値
            this.txtGinkoCd.Text="0";

            // 追加・変更状態表示
            this.lblMODE.Text = "【追加】";

            // 銀行コードに初期フォーカス
            this.ActiveControl = this.txtGinkoCd;
            this.txtGinkoCd.Focus();
        }

        /// <summary>
        /// 編集モードの初期表示
        /// </summary>
        private void InitDispOnEdit()
        {
            // 現在DBに登録されている値、入力制御を実装
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@GINKO_CD", SqlDbType.Decimal, 4, Util.ToString(this.InData));
            string cols = "GINKO_CD, GINKO_NM, GINKO_KANA_NM";
            string from = "TB_KY_GINKO";
            string where = "GINKO_CD = @GINKO_CD";
            DataTable dt = this.Dba.GetDataTableByConditionWithParams(cols, from, where, dpc);
            if (dt.Rows.Count == 0)
            {
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }

            // 取得した内容を表示
            DataRow drDispData = dt.Rows[0];
            this.txtGinkoCd.Text = Util.ToString(drDispData["GINKO_CD"]);
            this.txtGinkoNm.Text = Util.ToString(drDispData["GINKO_NM"]);
            this.txtGinkoKanaNm.Text = Util.ToString(drDispData["GINKO_KANA_NM"]);

            // 追加・変更状態表示
            this.lblMODE.Text = "【変更】";

            // 銀行コードは入力不可
            this.lblGinkoCd.Enabled = false;
            this.txtGinkoCd.Enabled = false;
        }

        /// <summary>
        /// 銀行コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidGinkoCd()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtGinkoCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 既に存在するコードを入力した場合はエラーとする
            if (this.txtGinkoCd.Text != "0")
            {
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@GINKO_CD", SqlDbType.Decimal, 4, this.txtGinkoCd.Text);
                string cols = "GINKO_CD";
                string from = "TB_KY_GINKO";
                string where = "GINKO_CD = @GINKO_CD";
                DataTable dt = this.Dba.GetDataTableByConditionWithParams(cols, from, where, dpc);
                if (dt.Rows.Count > 0)
                {
                    Msg.Error("既に存在する銀行コードと重複しています。");
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 銀行名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidGinkoNm()
        {
            // 指定バイト数を超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtGinkoNm.Text, this.txtGinkoNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// カナ名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidGinkoKanaNm()
        {
            // 指定バイト数を超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtGinkoKanaNm.Text, this.txtGinkoKanaNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 必須項目のチェック
            if (this.txtGinkoCd.Text == "0")
            {
                Msg.Notice("ゼロは使用できません。");
                this.txtGinkoCd.Focus();
                return false;
            }

            if (MODE_NEW.Equals(this.Par1))
            {
                // 銀行コードのチェック
                if (!IsValidGinkoCd())
                {
                    this.txtGinkoCd.Focus();
                    return false;
                }
            }

            // 銀行名のチェック
            if (!IsValidGinkoNm())
            {
                this.txtGinkoNm.Focus();
                return false;
            }

            // カナ名のチェック
            if (!IsValidGinkoKanaNm())
            {
                this.txtGinkoKanaNm.Focus();
                return false;
            }

            return true;
        }

        /// <summary>
        /// TB_KY_GINKOに更新するためのパラメータ設定をします。
        /// </summary>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// 更新処理：DbParamCollection*2(Where句,Set句)
        /// </returns>
        private ArrayList SetKyGinkoParams()
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            if (MODE_NEW.Equals(this.Par1))
            {
                // 銀行コード
                updParam.SetParam("@GINKO_CD", SqlDbType.Decimal, 4, this.txtGinkoCd.Text);
                // 登録日
                updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWDATE");
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // WHERE句のパラメータに設定
                DbParamCollection whereParam = new DbParamCollection();
                whereParam.SetParam("@GINKO_CD", SqlDbType.Decimal, 4, this.txtGinkoCd.Text);
                alParams.Add(whereParam);
            }

            // 銀行名
            updParam.SetParam("@GINKO_NM", SqlDbType.VarChar, 30, this.txtGinkoNm.Text);
            // カナ名
            updParam.SetParam("@GINKO_KANA_NM", SqlDbType.VarChar, 30, this.txtGinkoKanaNm.Text);
            // 更新日
            updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWDATE");

            alParams.Add(updParam);

            return alParams;
        }

        #endregion

    }
}
