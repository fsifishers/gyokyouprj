﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.ky.kycm1051
{
    /// <summary>
    /// 銀行の印刷(KYCM1053)
    /// </summary>
    public partial class KYCM1053 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// データ取得用
        /// </summary>
        private const int MAX_LINE_NO = 50;
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KYCM1053()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // タイトルは非表示
            this.lblTitle.Visible = false;

            // ボタンを設定
            this.ShowFButton = true;
            this.btnEsc.Visible = true;
            this.btnF1.Visible = true;
            this.btnF2.Visible = false;
            this.btnF3.Visible = false;
            this.btnF4.Visible = true;
            this.btnF5.Visible = true;
            this.btnF6.Visible = false;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF1.Location = this.btnF2.Location;
            this.btnF5.Location = this.btnF4.Location;
            this.btnF4.Location = this.btnF3.Location;

            // 初期フォーカスを設定
            this.txtGinkoCdFr.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            switch (this.ActiveCtlNm)
            {
                // 銀行コード(自)・(至)の場合のみ有効にする
                case "txtGinkoCdFr":
                case "txtGinkoCdTo":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理 検索
        /// </summary>
        public override void PressF1()
        {
            switch (this.ActiveCtlNm)
            {
                // 銀行コード(自)・(至)の場合、銀行の検索画面を立ち上げる
                case "txtGinkoCdFr":
                case "txtGinkoCdTo":
                    // 銀行選択画面
                    KYCM1051 frm1051 = new KYCM1051();
                    frm1051.Par1 = "1";
                    frm1051.ShowDialog(this);

                    if (frm1051.DialogResult == DialogResult.OK)
                    {
                        string[] result = (string[])frm1051.OutData;

                        if (this.ActiveCtlNm == "txtGinkoCdFr")
                        {
                            string[] outData = (string[])frm1051.OutData;
                            this.txtGinkoCdFr.Text = outData[0];
                            this.lblGinkoCdFr.Text = outData[1];
                        }
                        if (this.ActiveCtlNm == "txtGinkoCdTo")
                        {
                            string[] outData = (string[])frm1051.OutData;
                            this.txtGinkoCdTo.Text = outData[0];
                            this.lblGinkoCdTo.Text = outData[1];

                        }
                    }
                    frm1051.Dispose();
                    break;

                default:
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理 
        /// </summary>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfYesNo("実行しますか？") == DialogResult.Yes)
            {
                // プレビュー処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理 
        /// </summary>
        public override void PressF5()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfYesNo("実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }
        #endregion

        #region イベント
        /// <summary>
        /// 銀行コード(自)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>        
        private void txtGinkoCdFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidGinkoCdFr())
            {
                e.Cancel = true;
                this.txtGinkoCdFr.SelectAll();
            }
        }

        /// <summary>
        /// 銀行コード(至)の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGinkoCdTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidGinkoCdTo())
            {
                e.Cancel = true;
                this.txtGinkoCdTo.SelectAll();
            }
        }

        /// <summary>
        /// 画面の最後のコントールでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LastControl_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.btnF4.Enabled)
            {
                this.PressF4();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 銀行コード(自)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidGinkoCdFr()
        {
            // 未入力の場合、「先　頭」を表示
            if (ValChk.IsEmpty(this.txtGinkoCdFr.Text))
            {
                this.lblGinkoCdFr.Text = "先　頭";
            }
            // 数字のみの入力を許可
            else if (!ValChk.IsNumber(this.txtGinkoCdFr.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 存在するコードの場合はラベルに名称を表示する
            else
            {
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@GINKO_CD", SqlDbType.Decimal, 4, this.txtGinkoCdFr.Text);
                StringBuilder where = new StringBuilder("GINKO_CD = @GINKO_CD");
                DataTable dtGinkoDate =
                    this.Dba.GetDataTableByConditionWithParams("GINKO_NM",
                        "TB_KY_GINKO", Util.ToString(where), dpc);
                if (dtGinkoDate.Rows.Count > 0)
                {
                    this.lblGinkoCdFr.Text = dtGinkoDate.Rows[0]["GINKO_NM"].ToString();
                }
                else
                {
                    this.lblGinkoCdFr.Text = "";
                }
            }

            return true;
        }

        /// <summary>
        /// 銀行コード(至)の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidGinkoCdTo()
        {
            // 未入力の場合「最　後」を表示
            if (ValChk.IsEmpty(this.txtGinkoCdTo.Text))
            {
                this.lblGinkoCdTo.Text = "最　後";
            }
            // 数字のみの入力を許可
            else if (!ValChk.IsNumber(this.txtGinkoCdTo.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 存在するコードの場合はラベルに名称を表示する
            else
            {
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@GINKO_CD", SqlDbType.Decimal, 4, this.txtGinkoCdTo.Text);
                StringBuilder where = new StringBuilder("GINKO_CD = @GINKO_CD");
                DataTable dtGinkoDate =
                    this.Dba.GetDataTableByConditionWithParams("GINKO_NM",
                        "TB_KY_GINKO", Util.ToString(where), dpc);
                if (dtGinkoDate.Rows.Count > 0)
                {
                    this.lblGinkoCdTo.Text = dtGinkoDate.Rows[0]["GINKO_NM"].ToString();
                }
                else
                {
                    this.lblGinkoCdTo.Text = "";
                }
            }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 銀行コード(自)のチェック
            if (!IsValidGinkoCdFr())
            {
                this.txtGinkoCdFr.Focus();
                return false;
            }

            // 銀行コード(至)のチェック
            if (!IsValidGinkoCdTo())
            {
                this.txtGinkoCdTo.Focus();
                return false;
            }

            return true;
        }

        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview)
        {
            // 現状仮の状態
            try
            {
                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                bool dataFlag = MakeWkData();

                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    StringBuilder cols = new StringBuilder();
                    cols.Append("  ITEM001");
                    cols.Append(" ,ITEM002");
                    cols.Append(" ,ITEM003");
                    cols.Append(" ,ITEM004");

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                    // データの取得
                    DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_KY_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    // 帳票オブジェクトをインスタンス化
                    KYCM1051R rpt = new KYCM1051R(dtOutput);

                    if (isPreview)
                    {
                        // プレビュー画面表示
                        PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                        pFrm.WindowState = FormWindowState.Maximized;
                        pFrm.Show();
                    }
                    else
                    {
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
            }
            finally
            {
                this.Dba.Rollback();
            }
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {
            #region 設定値をセットする
            // 銀行コードを取得
            string GinkoCdFr;
            string GinkoCdTo;
            if (Util.ToString(this.txtGinkoCdFr.Text) != "")
            {
                GinkoCdFr = this.txtGinkoCdFr.Text;
            }
            else
            {
                GinkoCdFr = "0";
            }
            if (Util.ToString(this.txtGinkoCdTo.Text) != "")
            {
                GinkoCdTo = this.txtGinkoCdTo.Text;
            }
            else
            {
                GinkoCdTo = "9999";
            }
            #endregion

            #region データを取得する
            // 現状仮のSQL。
            StringBuilder Sql = new StringBuilder();
            Sql.Append("SELECT");
            Sql.Append(" GINKO_CD,");
            Sql.Append(" GINKO_NM,");
            Sql.Append(" GINKO_KANA_NM ");
            Sql.Append("FROM");
            Sql.Append(" TB_KY_GINKO ");
            Sql.Append("WHERE");
            Sql.Append(" GINKO_CD BETWEEN @GINKO_CD_FR AND @GINKO_CD_TO ");
            Sql.Append("ORDER BY");
            Sql.Append(" GINKO_CD ASC");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@GINKO_CD_FR", SqlDbType.Decimal, 4, GinkoCdFr); // 銀行コードFr
            dpc.SetParam("@GINKO_CD_TO", SqlDbType.Decimal, 4, GinkoCdTo); // 銀行コードTo

            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            #endregion

            int i = 0; // ソート番号用変数
            if (dtMainLoop.Rows.Count == 0)
            {
                Msg.Info("該当データがありません。");
                return false;
            }
            else
            {
                #region 印刷ワークテーブルに登録
                foreach (DataRow dr in dtMainLoop.Rows)
                {
                    Sql = new StringBuilder();
                    dpc = new DbParamCollection();
                    Sql.Append("INSERT INTO PR_KY_TBL(");
                    Sql.Append(" GUID");
                    Sql.Append(" ,SORT");
                    Sql.Append(" ,ITEM001");
                    Sql.Append(" ,ITEM002");
                    Sql.Append(" ,ITEM003");
                    Sql.Append(") ");
                    Sql.Append("VALUES(");
                    Sql.Append("  @GUID");
                    Sql.Append(" ,@SORT");
                    Sql.Append(" ,@ITEM001");
                    Sql.Append(" ,@ITEM002");
                    Sql.Append(" ,@ITEM003");
                    Sql.Append(") ");
                    // データを設定
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
                    dpc.SetParam("@ITEM001", SqlDbType.VarChar, 200, dr["GINKO_CD"].ToString()); // ｺｰﾄﾞ
                    dpc.SetParam("@ITEM002", SqlDbType.VarChar, 200, dr["GINKO_NM"].ToString()); // 銀行名
                    dpc.SetParam("@ITEM003", SqlDbType.VarChar, 200, dr["GINKO_KANA_NM"].ToString()); // 銀行カナ名
                    // データを登録
                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                    i++;
                }
                #endregion
                if (MAX_LINE_NO == 0)
                {
                #region 印刷ワークテーブルに空行登録
                // 現在のページの行を計算する
                int Amari = i % MAX_LINE_NO;
                for (int j = Amari; j < MAX_LINE_NO; j++)
                {
                    Sql = new StringBuilder();
                    dpc = new DbParamCollection();
                    Sql.Append("INSERT INTO PR_KY_TBL(");
                    Sql.Append(" GUID");
                    Sql.Append(" ,SORT");
                    Sql.Append(") ");
                    Sql.Append("VALUES(");
                    Sql.Append("  @GUID");
                    Sql.Append(" ,@SORT");
                    Sql.Append(") ");
                    // データを設定
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
                    // データを登録
                    this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                    i++;
                }
                    #endregion
                }

                // 印刷ワークテーブルのデータ件数を取得
                dpc = new DbParamCollection();
                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                DataTable tmpdtPR_KY_TBL = this.Dba.GetDataTableByConditionWithParams(
                    "SORT",
                    "PR_KY_TBL",
                    "GUID = @GUID",
                    dpc);

                bool dataFlag;
                if (tmpdtPR_KY_TBL.Rows.Count > 0)
                {
                    dataFlag = true;
                }
                else
                {
                    dataFlag = false;
                }

                return dataFlag;
            }
        }
        #endregion
    }
}
