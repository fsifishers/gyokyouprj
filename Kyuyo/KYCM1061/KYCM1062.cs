﻿using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.ky.kycm1061
{
    /// <summary>
    /// 支店の登録(KYCM1062)
    /// </summary>
    public partial class KYCM1062 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(新規)
        /// </summary>
        private const string MODE_NEW = "1";

        /// <summary>
        /// モード(編集)
        /// </summary>
        private const string MODE_EDIT = "2";
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KYCM1062()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="par1">引数1</param>
        public KYCM1062(string par1)
            : base(par1)
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 引数：Par1／モード(1:新規、2:変更)、InData：銀行コード、支店コード
            string[] code = (string[])this.InData;
            
            // 銀行情報表示
            this.lblGinkoCd.Text = code[0];
            this.lblGinkoNm.Text = this.Dba.GetName(this.UInfo, "TB_KY_GINKO", " ", code[0]);

            // 初期表示
            if (MODE_NEW.Equals(this.Par1))
            {
                // 新規モード
                InitDispOnNew();
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 編集モード
                InitDispOnEdit();
            }
            else
            {
                // 不正な起動として閉じる
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }
            this.ShowFButton = true;

            // タイトルは非表示
            this.lblTitle.Visible = false;
            // EscapeとF1のみ表示
            this.ShowFButton = true;
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF1.Location = this.btnF2.Location;
            this.btnF6.Location = this.btnF4.Location;
            this.btnF2.Visible = false;
            this.btnF3.Visible = true;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF6.Visible = true;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F3キー押下時処理
        /// </summary>
        public override void PressF3()
        {
            // 新規モードの場合は処理させない
            if (this.Par1.Equals(MODE_NEW)) return;

            // 確認メッセージを表示
            string msg = "削除しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // 削除用パラメータ
            string[] code = (string[])this.InData;
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@GINKO_CD", SqlDbType.Decimal, 4, code[0]);
            dpc.SetParam("@SHITEN_CD", SqlDbType.Decimal, 4, code[1]);

            try
            {
                // トランザクション開始
                this.Dba.BeginTransaction();

                if (MODE_EDIT.Equals(this.Par1))
                {
                    // データ削除
                    // 給与支店マスタ
                    this.Dba.Delete("TB_KY_GINKO_SHITEN", 
                        "GINKO_CD = @GINKO_CD AND SHITEN_CD = @SHITEN_CD", dpc);
                }

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        
        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        public override void PressF6()
        {
            // 確認メッセージを表示
            string msg = (MODE_NEW.Equals(this.Par1) ? "登録" : "更新") + "しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // 入力値をバインドパラメータとしてセットする
            ArrayList alParamsKyShiten = SetKyShitenParams();

            try
            {
                // トランザクション開始
                this.Dba.BeginTransaction();

                if (MODE_NEW.Equals(this.Par1))
                {
                    // データ登録
                    // 給与支店マスタ
                    this.Dba.Insert("TB_KY_GINKO_SHITEN", (DbParamCollection)alParamsKyShiten[0]);
                }
                else if (MODE_EDIT.Equals(this.Par1))
                {
                    // データ更新
                    // 給与支店マスタ
                    this.Dba.Update("TB_KY_GINKO_SHITEN",
                        (DbParamCollection)alParamsKyShiten[1],
                        "GINKO_CD = @GINKO_CD AND SHITEN_CD = @SHITEN_CD",
                        (DbParamCollection)alParamsKyShiten[0]);
                }

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 支店コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShitenCd_Validating(object sender, CancelEventArgs e)
        {
            // 入力値補正
            this.txtShitenCd.Text = Util.ToString(Util.ToDecimal(this.txtShitenCd.Text));

            if (!IsValidShitenCd())
            {
                e.Cancel = true;
                this.txtShitenCd.SelectAll();
            }
            
        }

        /// <summary>
        /// 支店名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShitenNm_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShitenNm())
            {
                e.Cancel = true;
                this.txtShitenNm.SelectAll();
            }
        }

        /// <summary>
        /// カナ名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShitenKanaNm_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShitenKanaNm())
            {
                e.Cancel = true;
                this.txtShitenKanaNm.SelectAll();
            }
        }

        /// <summary>
        /// 画面の最後のコントールでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LastControl_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.btnF6.Enabled)
            {
                this.PressF6();
            }
        }
        #endregion

        #region privateメソッド

        /// <summary>
        /// 新規モードの初期表示
        /// </summary>
        private void InitDispOnNew()
        {
            // 初期値
            this.txtShitenCd.Text="0";

            // 追加・変更状態表示
            this.lblMODE.Text = "【追加】";

            // 支店コードに初期フォーカス
            this.ActiveControl = this.txtShitenCd;
            this.txtShitenCd.Focus();
        }

        /// <summary>
        /// 編集モードの初期表示
        /// </summary>
        private void InitDispOnEdit()
        {
            string[] code = (string[])this.InData;

            // 現在DBに登録されている値、入力制御を実装
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@GINKO_CD", SqlDbType.Decimal, 4, code[0]);
            dpc.SetParam("@SHITEN_CD", SqlDbType.Decimal, 4, code[1]);
            string cols = "SHITEN_CD, SHITEN_NM, SHITEN_KANA_NM";
            string from = "TB_KY_GINKO_SHITEN";
            string where = "GINKO_CD = @GINKO_CD AND SHITEN_CD = @SHITEN_CD";
            DataTable dt = this.Dba.GetDataTableByConditionWithParams(cols, from, where, dpc);
            if (dt.Rows.Count == 0)
            {
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }

            // 取得した内容を表示
            DataRow drDispData = dt.Rows[0];
            this.txtShitenCd.Text = Util.ToString(drDispData["SHITEN_CD"]);
            this.txtShitenNm.Text = Util.ToString(drDispData["SHITEN_NM"]);
            this.txtShitenKanaNm.Text = Util.ToString(drDispData["SHITEN_KANA_NM"]);

            // 追加・変更状態表示
            this.lblMODE.Text = "【変更】";

            // 支店コードは入力不可
            this.lblShitenCd.Enabled = false;
            this.txtShitenCd.Enabled = false;
        }

        /// <summary>
        /// 支店コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShitenCd()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtShitenCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 既に存在するコードを入力した場合はエラーとする
            if (this.txtShitenCd.Text != "0")
            {
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@GINKO_CD", SqlDbType.Decimal, 4, this.lblGinkoCd.Text);
                dpc.SetParam("@SHITEN_CD", SqlDbType.Decimal, 4, this.txtShitenCd.Text);
                string cols = "GINKO_CD";
                string from = "TB_KY_GINKO_SHITEN";
                string where = "GINKO_CD = @GINKO_CD AND SHITEN_CD = @SHITEN_CD";
                DataTable dt = this.Dba.GetDataTableByConditionWithParams(cols, from, where, dpc);
                if (dt.Rows.Count > 0)
                {
                    Msg.Error("既に存在する支店コードと重複しています。");
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 支店名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShitenNm()
        {
            // 指定バイト数を超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtShitenNm.Text, this.txtShitenNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// カナ名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShitenKanaNm()
        {
            // 指定バイト数を超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtShitenKanaNm.Text, this.txtShitenKanaNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 必須項目のチェック
            if (this.txtShitenCd.Text == "0")
            {
                Msg.Notice("ゼロは使用できません。");
                this.txtShitenCd.Focus();
                return false;
            }

            if (MODE_NEW.Equals(this.Par1))
            {
                // 支店コードのチェック
                if (!IsValidShitenCd())
                {
                    this.txtShitenCd.Focus();
                    return false;
                }
            }

            // 支店名のチェック
            if (!IsValidShitenNm())
            {
                this.txtShitenNm.Focus();
                return false;
            }

            // カナ名のチェック
            if (!IsValidShitenNm())
            {
                this.txtShitenNm.Focus();
                return false;
            }

            return true;
        }

        /// <summary>
        /// TB_KY_GINKO_SHITENに更新するためのパラメータ設定をします。
        /// </summary>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// 更新処理：DbParamCollection*2(Where句,Set句)
        /// </returns>
        private ArrayList SetKyShitenParams()
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            if (MODE_NEW.Equals(this.Par1))
            {
                // 銀行コード
                updParam.SetParam("@GINKO_CD", SqlDbType.Decimal, 4, this.lblGinkoCd.Text);
                // 支店コード
                updParam.SetParam("@SHITEN_CD", SqlDbType.Decimal, 4, this.txtShitenCd.Text);
                // 登録日
                updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWDATE");
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // WHERE句のパラメータに設定
                DbParamCollection whereParam = new DbParamCollection();
                whereParam.SetParam("@GINKO_CD", SqlDbType.Decimal, 4, this.lblGinkoCd.Text);
                whereParam.SetParam("@SHITEN_CD", SqlDbType.Decimal, 4, this.txtShitenCd.Text);
                alParams.Add(whereParam);
            }

            // 支店名
            updParam.SetParam("@SHITEN_NM", SqlDbType.VarChar, 30, this.txtShitenNm.Text);
            // カナ名
            updParam.SetParam("@SHITEN_KANA_NM", SqlDbType.VarChar, 30, this.txtShitenKanaNm.Text);
            // 更新日
            updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWDATE");

            alParams.Add(updParam);

            return alParams;
        }

        #endregion

    }
}
