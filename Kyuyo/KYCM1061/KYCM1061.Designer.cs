﻿namespace jp.co.fsi.ky.kycm1061
{
    partial class KYCM1061
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblKanaName = new System.Windows.Forms.Label();
            this.txtKanaName = new System.Windows.Forms.TextBox();
            this.dgvList = new System.Windows.Forms.DataGridView();
            this.txtGinkoCd = new System.Windows.Forms.TextBox();
            this.lblGinkoCd = new System.Windows.Forms.Label();
            this.lblGinkoNm = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Text = "支店の登録";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            // 
            // lblKanaName
            // 
            this.lblKanaName.BackColor = System.Drawing.Color.Silver;
            this.lblKanaName.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKanaName.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKanaName.Location = new System.Drawing.Point(281, 50);
            this.lblKanaName.Name = "lblKanaName";
            this.lblKanaName.Size = new System.Drawing.Size(85, 20);
            this.lblKanaName.TabIndex = 1;
            this.lblKanaName.Text = "カ　ナ　名";
            this.lblKanaName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKanaName
            // 
            this.txtKanaName.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKanaName.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtKanaName.Location = new System.Drawing.Point(365, 50);
            this.txtKanaName.MaxLength = 30;
            this.txtKanaName.Name = "txtKanaName";
            this.txtKanaName.Size = new System.Drawing.Size(180, 20);
            this.txtKanaName.TabIndex = 1;
            this.txtKanaName.Validating += new System.ComponentModel.CancelEventHandler(this.txtKanaName_Validating);
            // 
            // dgvList
            // 
            this.dgvList.AllowUserToAddRows = false;
            this.dgvList.AllowUserToDeleteRows = false;
            this.dgvList.AllowUserToResizeColumns = false;
            this.dgvList.AllowUserToResizeRows = false;
            this.dgvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvList.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.dgvList.Location = new System.Drawing.Point(17, 74);
            this.dgvList.MultiSelect = false;
            this.dgvList.Name = "dgvList";
            this.dgvList.ReadOnly = true;
            this.dgvList.RowHeadersVisible = false;
            this.dgvList.RowTemplate.Height = 21;
            this.dgvList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvList.Size = new System.Drawing.Size(530, 296);
            this.dgvList.TabIndex = 2;
            this.dgvList.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvList_CellDoubleClick);
            this.dgvList.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvList_KeyDown);
            // 
            // txtGinkoCd
            // 
            this.txtGinkoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGinkoCd.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtGinkoCd.Location = new System.Drawing.Point(101, 50);
            this.txtGinkoCd.MaxLength = 4;
            this.txtGinkoCd.Name = "txtGinkoCd";
            this.txtGinkoCd.Size = new System.Drawing.Size(48, 20);
            this.txtGinkoCd.TabIndex = 0;
            this.txtGinkoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGinkoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtGinkoCd_Validating);
            // 
            // lblGinkoCd
            // 
            this.lblGinkoCd.BackColor = System.Drawing.Color.Silver;
            this.lblGinkoCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGinkoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGinkoCd.Location = new System.Drawing.Point(17, 50);
            this.lblGinkoCd.Name = "lblGinkoCd";
            this.lblGinkoCd.Size = new System.Drawing.Size(85, 20);
            this.lblGinkoCd.TabIndex = 902;
            this.lblGinkoCd.Text = "銀行コード";
            this.lblGinkoCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblGinkoNm
            // 
            this.lblGinkoNm.BackColor = System.Drawing.Color.Silver;
            this.lblGinkoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGinkoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGinkoNm.Location = new System.Drawing.Point(146, 50);
            this.lblGinkoNm.Name = "lblGinkoNm";
            this.lblGinkoNm.Size = new System.Drawing.Size(126, 20);
            this.lblGinkoNm.TabIndex = 904;
            this.lblGinkoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // KYCM1061
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.lblGinkoNm);
            this.Controls.Add(this.txtGinkoCd);
            this.Controls.Add(this.lblGinkoCd);
            this.Controls.Add(this.dgvList);
            this.Controls.Add(this.txtKanaName);
            this.Controls.Add(this.lblKanaName);
            this.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.Name = "KYCM1061";
            this.Text = "支店の登録";
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.lblKanaName, 0);
            this.Controls.SetChildIndex(this.txtKanaName, 0);
            this.Controls.SetChildIndex(this.dgvList, 0);
            this.Controls.SetChildIndex(this.lblGinkoCd, 0);
            this.Controls.SetChildIndex(this.txtGinkoCd, 0);
            this.Controls.SetChildIndex(this.lblGinkoNm, 0);
            this.pnlDebug.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblKanaName;
        private System.Windows.Forms.TextBox txtKanaName;
        private System.Windows.Forms.DataGridView dgvList;
        private System.Windows.Forms.TextBox txtGinkoCd;
        private System.Windows.Forms.Label lblGinkoCd;
        private System.Windows.Forms.Label lblGinkoNm;

    }
}