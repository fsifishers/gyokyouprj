﻿namespace jp.co.fsi.ky.kycm1061
{
    partial class KYCM1062
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtShitenCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShitenCd = new System.Windows.Forms.Label();
            this.lblMODE = new System.Windows.Forms.Label();
            this.txtShitenKanaNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShitenKanaNm = new System.Windows.Forms.Label();
            this.txtShitenNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShitenNm = new System.Windows.Forms.Label();
            this.lblGinko = new System.Windows.Forms.Label();
            this.lblGinkoCd = new System.Windows.Forms.Label();
            this.lblGinkoNm = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(380, 23);
            this.lblTitle.Text = "支店の登録";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(5, 75);
            this.pnlDebug.Size = new System.Drawing.Size(413, 100);
            // 
            // txtShitenCd
            // 
            this.txtShitenCd.AutoSizeFromLength = true;
            this.txtShitenCd.DisplayLength = null;
            this.txtShitenCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShitenCd.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtShitenCd.Location = new System.Drawing.Point(127, 15);
            this.txtShitenCd.MaxLength = 4;
            this.txtShitenCd.Name = "txtShitenCd";
            this.txtShitenCd.Size = new System.Drawing.Size(64, 23);
            this.txtShitenCd.TabIndex = 0;
            this.txtShitenCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShitenCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtShitenCd_Validating);
            // 
            // lblShitenCd
            // 
            this.lblShitenCd.BackColor = System.Drawing.Color.Silver;
            this.lblShitenCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShitenCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShitenCd.Location = new System.Drawing.Point(12, 15);
            this.lblShitenCd.Name = "lblShitenCd";
            this.lblShitenCd.Size = new System.Drawing.Size(115, 23);
            this.lblShitenCd.TabIndex = 1;
            this.lblShitenCd.Text = "支店コード";
            this.lblShitenCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMODE
            // 
            this.lblMODE.BackColor = System.Drawing.Color.White;
            this.lblMODE.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMODE.Location = new System.Drawing.Point(318, 15);
            this.lblMODE.Name = "lblMODE";
            this.lblMODE.Size = new System.Drawing.Size(78, 23);
            this.lblMODE.TabIndex = 902;
            this.lblMODE.Text = "【編集】";
            this.lblMODE.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtShitenKanaNm
            // 
            this.txtShitenKanaNm.AutoSizeFromLength = false;
            this.txtShitenKanaNm.DisplayLength = null;
            this.txtShitenKanaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShitenKanaNm.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtShitenKanaNm.Location = new System.Drawing.Point(127, 95);
            this.txtShitenKanaNm.MaxLength = 30;
            this.txtShitenKanaNm.Name = "txtShitenKanaNm";
            this.txtShitenKanaNm.Size = new System.Drawing.Size(269, 23);
            this.txtShitenKanaNm.TabIndex = 911;
            this.txtShitenKanaNm.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LastControl_KeyDown);
            this.txtShitenKanaNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtShitenKanaNm_Validating);
            // 
            // lblShitenKanaNm
            // 
            this.lblShitenKanaNm.BackColor = System.Drawing.Color.Silver;
            this.lblShitenKanaNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShitenKanaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShitenKanaNm.Location = new System.Drawing.Point(12, 95);
            this.lblShitenKanaNm.Name = "lblShitenKanaNm";
            this.lblShitenKanaNm.Size = new System.Drawing.Size(115, 23);
            this.lblShitenKanaNm.TabIndex = 912;
            this.lblShitenKanaNm.Text = "カナ名";
            this.lblShitenKanaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShitenNm
            // 
            this.txtShitenNm.AutoSizeFromLength = false;
            this.txtShitenNm.DisplayLength = null;
            this.txtShitenNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShitenNm.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtShitenNm.Location = new System.Drawing.Point(127, 72);
            this.txtShitenNm.MaxLength = 30;
            this.txtShitenNm.Name = "txtShitenNm";
            this.txtShitenNm.Size = new System.Drawing.Size(269, 23);
            this.txtShitenNm.TabIndex = 909;
            this.txtShitenNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtShitenNm_Validating);
            // 
            // lblShitenNm
            // 
            this.lblShitenNm.BackColor = System.Drawing.Color.Silver;
            this.lblShitenNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShitenNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShitenNm.Location = new System.Drawing.Point(12, 72);
            this.lblShitenNm.Name = "lblShitenNm";
            this.lblShitenNm.Size = new System.Drawing.Size(115, 23);
            this.lblShitenNm.TabIndex = 910;
            this.lblShitenNm.Text = "支店名";
            this.lblShitenNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblGinko
            // 
            this.lblGinko.BackColor = System.Drawing.Color.LightGray;
            this.lblGinko.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGinko.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGinko.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.lblGinko.Location = new System.Drawing.Point(12, 49);
            this.lblGinko.Name = "lblGinko";
            this.lblGinko.Size = new System.Drawing.Size(84, 23);
            this.lblGinko.TabIndex = 915;
            this.lblGinko.Text = "銀行";
            this.lblGinko.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblGinkoCd
            // 
            this.lblGinkoCd.BackColor = System.Drawing.Color.LightGray;
            this.lblGinkoCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGinkoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGinkoCd.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.lblGinkoCd.Location = new System.Drawing.Point(96, 49);
            this.lblGinkoCd.Name = "lblGinkoCd";
            this.lblGinkoCd.Size = new System.Drawing.Size(31, 23);
            this.lblGinkoCd.TabIndex = 914;
            this.lblGinkoCd.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblGinkoNm
            // 
            this.lblGinkoNm.BackColor = System.Drawing.Color.LightGray;
            this.lblGinkoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGinkoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGinkoNm.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.lblGinkoNm.Location = new System.Drawing.Point(127, 49);
            this.lblGinkoNm.Name = "lblGinkoNm";
            this.lblGinkoNm.Size = new System.Drawing.Size(269, 23);
            this.lblGinkoNm.TabIndex = 913;
            this.lblGinkoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // KYCM1062
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(405, 178);
            this.Controls.Add(this.lblGinko);
            this.Controls.Add(this.lblGinkoCd);
            this.Controls.Add(this.lblGinkoNm);
            this.Controls.Add(this.txtShitenKanaNm);
            this.Controls.Add(this.lblShitenKanaNm);
            this.Controls.Add(this.txtShitenNm);
            this.Controls.Add(this.lblShitenNm);
            this.Controls.Add(this.lblMODE);
            this.Controls.Add(this.txtShitenCd);
            this.Controls.Add(this.lblShitenCd);
            this.Name = "KYCM1062";
            this.Text = "支店の登録";
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblShitenCd, 0);
            this.Controls.SetChildIndex(this.txtShitenCd, 0);
            this.Controls.SetChildIndex(this.lblMODE, 0);
            this.Controls.SetChildIndex(this.lblShitenNm, 0);
            this.Controls.SetChildIndex(this.txtShitenNm, 0);
            this.Controls.SetChildIndex(this.lblShitenKanaNm, 0);
            this.Controls.SetChildIndex(this.txtShitenKanaNm, 0);
            this.Controls.SetChildIndex(this.lblGinkoNm, 0);
            this.Controls.SetChildIndex(this.lblGinkoCd, 0);
            this.Controls.SetChildIndex(this.lblGinko, 0);
            this.pnlDebug.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private jp.co.fsi.common.controls.FsiTextBox txtShitenCd;
        private System.Windows.Forms.Label lblShitenCd;
        private System.Windows.Forms.Label lblMODE;
        private common.controls.FsiTextBox txtShitenKanaNm;
        private System.Windows.Forms.Label lblShitenKanaNm;
        private common.controls.FsiTextBox txtShitenNm;
        private System.Windows.Forms.Label lblShitenNm;
        private System.Windows.Forms.Label lblGinko;
        private System.Windows.Forms.Label lblGinkoCd;
        private System.Windows.Forms.Label lblGinkoNm;
    }
}