﻿using System;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Collections.Generic;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;
using jp.co.fsi.common.constants;
using System.Diagnostics;
using System.Security.Policy;
using System.Collections;
using System.Drawing;

namespace jp.co.fsi.kyu.kyub3061
{
    /// <summary>
    /// 源泉徴収税額表取込(KYUB3061)
    /// </summary>
    public partial class KYUB3061 : BasePgForm
    {
        #region 定数
        // 列の番号
        const int COL_CHECK = 0;                //チェック
        const int COL_GYO_NO = 1;                  // 行ナンバー
        const int COL_FILE_NM = 2;              // ファイル名
        const int COL_FILE_PATH = 3;            // ファイルパス
        private DateTime today = DateTime.Today; // 本日日付
        private int kubun = 1; // 現在の区分を保持　// 1:給与　2：賞与
        
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KYUB3061()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            SearchData(1);
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 日付、船主コード１・２に
            // フォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtDateYear":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            int status = 0;
            int i = 0;//カウント変数
            int gyo_no = MaxGyoNo(kubun) + 1;
            //OpenFileDialogクラスのインスタンスを作成
            OpenFileDialog ofd = new OpenFileDialog();
            //複数のファイルを選択できるようにする
            ofd.Multiselect = true;

            //はじめのファイル名を指定する
            //はじめに「ファイル名」で表示される文字列を指定する
            ofd.FileName = "";
            //[ファイルの種類]に表示される選択肢を指定する
            //指定しないとすべてのファイルが表示される
            ofd.Filter = "|*.*";
            //「すべてのファイル」が選択されているようにする
            ofd.FilterIndex = 2;
            //タイトルを設定する
            ofd.Title = "更新するcsvファイルを選択してください。";
            //ダイアログボックスを閉じる前に現在のディレクトリを復元するようにする
            ofd.RestoreDirectory = true;

            //ダイアログを表示する
            if (ofd.ShowDialog() == DialogResult.OK)
            {                
                //選択されたファイル名をすべて表示する
                foreach (string fn in ofd.FileNames)
                {
                    //パラメータセット
                    ArrayList alParams = new ArrayList();
                    DbParamCollection insParam = new DbParamCollection();
                    StringBuilder sql = new StringBuilder();
                    StringBuilder columns = new StringBuilder();
                    StringBuilder values = new StringBuilder();

                    sql.Append("INSERT INTO TB_KY_ZEIGAKU_HYO_PATH (");
                    sql.Append("KAISHA_CD,");
                    sql.Append("GYO_NO,");
                    sql.Append("KUBUN,");
                    sql.Append("FILE_NM,");
                    sql.Append("FILE_PATH,");
                    sql.Append("REGIST_DATE,");
                    sql.Append("UPDATE_DATE)");
                    sql.Append(" VALUES (");
                    sql.Append("@KAISHA_CD,");
                    sql.Append("@GYO_NO,");
                    sql.Append("@KUBUN,");
                    sql.Append("@FILE_NM,");
                    sql.Append("@FILE_PATH,");
                    sql.Append("@REGIST_DATE,");
                    sql.Append("@UPDATE_DATE)");

                    alParams.Add(sql.ToString());


                    insParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                    insParam.SetParam("@GYO_NO", SqlDbType.Decimal, 4, gyo_no);
                    insParam.SetParam("@KUBUN", SqlDbType.Decimal, 4, kubun);
                    // フォルダ名＋ファイル名のフルパスからファイル名を取得
                    String FileName = System.IO.Path.GetFileName(ofd.FileNames[i]);
                    insParam.SetParam("@FILE_NM", SqlDbType.VarChar, 200, FileName);   // ファイル名
                    insParam.SetParam("@FILE_PATH", SqlDbType.VarChar, 200, ofd.FileNames[i]);   // ファイルパス
                    insParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, today);   // 支給月
                    insParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, today);   // 支給月


                    alParams.Add(insParam);

                    try
                    {
                        // トランザクション開始
                        this.Dba.BeginTransaction();

                        // TB_KY_ZEIGAKU_HYO_PATHにInsert する
                        status = this.Dba.ModifyBySql(alParams[0].ToString(), (DbParamCollection)alParams[1]);

                        // トランザクションをコミット
                        this.Dba.Commit();

                        //カウントアップ
                        gyo_no++;
                        i++;
                    }
                    catch (Exception e)
                    {
                        // ロールバック
                        this.Dba.Rollback();
                        Msg.Error(e.Message);
                    }
                    finally
                    {
                        // 処理なし
                        ;
                    }
                }
            }
            SearchData(kubun);
        }

        /// <summary>
        /// F3キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF6();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF3()
        {
            if (Msg.ConfNmYesNo("削除", "実行しますか？") == DialogResult.Yes)
            {
                try
                {
                    this.Dba.BeginTransaction();

                    if (kubun == 1)//給与
                    {
                        foreach (DataGridViewRow dr in this.dgvInputList.Rows)
                        {
                            // チェックがついてる行を削除
                            if (dr.Cells[COL_CHECK].Value.Equals("1"))
                            {
                                // 仕切明細テーブルに存在するデータを削除
                                DbParamCollection dpc = new DbParamCollection();
                                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                                dpc.SetParam("@GYO_NO", SqlDbType.Decimal, 4, dr.Cells[COL_GYO_NO].Value);
                                dpc.SetParam("@KUBUN", SqlDbType.Decimal, 4, kubun);

                                // 処理実行
                                this.Dba.Delete("TB_KY_ZEIGAKU_HYO_PATH", "KAISHA_CD = @KAISHA_CD AND GYO_NO = @GYO_NO AND KUBUN = @KUBUN", dpc);
                            }
                        }
                    }
                    else
                    {
                        foreach (DataGridViewRow dr in this.dataGridView1.Rows)
                        {
                            // チェックがついてる行を削除
                            if (dr.Cells[COL_CHECK].Value.Equals("1"))
                            {
                                // 仕切明細テーブルに存在するデータを削除
                                DbParamCollection dpc = new DbParamCollection();
                                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                                dpc.SetParam("@GYO_NO", SqlDbType.Decimal, 4, dr.Cells[COL_GYO_NO].Value);
                                dpc.SetParam("@KUBUN", SqlDbType.Decimal, 4, kubun);
                                // 処理実行
                                this.Dba.Delete("TB_KY_ZEIGAKU_HYO_PATH", "KAISHA_CD = @KAISHA_CD AND GYO_NO = @GYO_NO AND KUBUN = @KUBUN", dpc);
                            }
                        }
                    }

                    // トランザクションをコミット
                    this.Dba.Commit();

                    // DialogResultに「OK」をセットし結果を返却
                    this.DialogResult = DialogResult.OK;
                }
                catch (Exception e)
                {
                    // ロールバック
                    this.Dba.Rollback();

                    // 失敗通知ダイアログ
                    Msg.Error("削除に失敗しました。");
                }
                finally
                {
                    SearchData(kubun);
                }

            }
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF6();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF6()
        {
            if (Msg.ConfNmYesNo("更新", "実行しますか？") == DialogResult.Yes)
            {
                if (kubun == 1)//給与
                {
                    foreach (DataGridViewRow dr in this.dgvInputList.Rows)
                    {
                        // チェックがついてる行のインサート
                        if (dr.Cells[COL_CHECK].Value.Equals("1"))
                        {
                            ReadData(Util.ToInt(dr.Cells[COL_GYO_NO].Value));
                        }
                    }
                }
                else// 賞与
                {
                    foreach (DataGridViewRow dr in this.dataGridView1.Rows)
                    {
                        // チェックがついてる行のインサート
                        if (dr.Cells[COL_CHECK].Value.Equals("1"))
                        {
                            ReadData(Util.ToInt(dr.Cells[COL_GYO_NO].Value));
                        }
                    }
                }
            }
        }

        #endregion

        #region イベント
        /// <summary>
        /// タブ変更時
        /// </summary>
        private void tabPages_SelectedIndexChanged(object sender, EventArgs e)
        {
            // 区分変更
            if (kubun == 1)
            {
                kubun = 2;
                SearchData(kubun);
            }
            else
            {
                kubun = 1;
                SearchData(kubun);
            }
        }

        /// <summary>
        /// 給与チェック時処理
        /// </summary>
        private void dgvInputList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            // チェック列以外は処理しない
            if (e.RowIndex == -1 || e.ColumnIndex != COL_CHECK)
            {
                return;
            }
            foreach(DataGridViewRow dr in this.dgvInputList.Rows)
            {
                //チェックを全てはずす
                dr.Cells[COL_CHECK].Value = 0; 
            }
            // チェックをする
            this.dgvInputList[COL_CHECK, this.dgvInputList.CurrentCell.RowIndex].Value = "1";
        }

        /// <summary>
        /// 賞与チェック時処理
        /// </summary>
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            // チェック列以外は処理しない
            if (e.RowIndex == -1 || e.ColumnIndex != COL_CHECK)
            {
                return;
            }
            foreach (DataGridViewRow dr in this.dataGridView1.Rows)
            {
                //チェックを全てはずす
                dr.Cells[COL_CHECK].Value = 0;
            }
            // チェックをする
            this.dataGridView1[COL_CHECK, this.dataGridView1.CurrentCell.RowIndex].Value = "1";
        }
        #endregion

        #region privateメソッド

        /// <summary>
        /// データベースにインポート
        /// </summary>
        private void ReadData(int gyo_no)
        {
            #region 変数宣言
            string Url;
            Uri dirPath;
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();
            #endregion

            #region データ取得準備

            sql.Append(" SELECT ");
            sql.Append(" FILE_PATH ");
            sql.Append(" FROM ");
            sql.Append(" TB_KY_ZEIGAKU_HYO_PATH ");
            sql.Append(" WHERE ");
            sql.Append(" KAISHA_CD = @KAISHA_CD AND");
            sql.Append(" KUBUN  = @KUBUN AND ");
            sql.Append(" GYO_NO = @GYO_NO ");

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@KUBUN", SqlDbType.Decimal, 4, kubun);
            dpc.SetParam("@GYO_NO", SqlDbType.Decimal, 4, gyo_no);
            DataTable dt = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);

            if(dt.Rows.Count == 0)
            {
                Msg.Info("ファイルパスを取得できませんでした");
                return;
            }
            else
            {
               Url = Util.ToString(dt.Rows[0]["FILE_PATH"]);
            }
            dirPath = new Uri(Url); // 基準となるパス
            
            // 保存先の存在チェック
            if (!File.Exists(dirPath.LocalPath))
            {
                Msg.Info("ファイルが見つかりません。");
                return;
            }
            #endregion

            if (kubun == 1)// 給与
            {
                try
                {
                    // トランザクションの開始
                    this.Dba.BeginTransaction();

                    if (dt.Rows.Count > 0)
                    {
                        if (Msg.ConfYesNo("更新してもよろしいでしょうか？") == DialogResult.No)
                        {
                            return;
                        }
                        #region セリデータ削除処理
                        dpc = new DbParamCollection();
                        sql = new StringBuilder();
                        sql.Append("DELETE ");
                        sql.Append("  FROM TW_KY_F_GETSUGAKU_STKZIGK_HYO");

                        this.Dba.ModifyBySql(Util.ToString(sql), dpc);
                        #endregion
                    }

                    //テキストファイルからデータを取得
                    dpc = new DbParamCollection();
                    sql = new StringBuilder();
                    sql.Append(" BULK INSERT TW_KY_F_GETSUGAKU_STKZIGK_HYO ");
                    sql.Append(" FROM '" + dirPath.LocalPath + "'");
                    sql.Append(" WITH ");
                    sql.Append(" ( FIELDTERMINATOR = ',',");
                    sql.Append("    ROWTERMINATOR = '\n' ");
                    sql.Append("  );  ");
                    this.Dba.ModifyBySql(Util.ToString(sql), dpc);

                    // トランザクションをコミット
                    this.Dba.Commit();
                    Msg.Info("取り込み処理が正常に終了しました。");
                }
                finally
                {
                    // ロールバック
                    this.Dba.Rollback();
                }
            }
            else// 賞与
            {
                try
                {
                    // トランザクションの開始
                    this.Dba.BeginTransaction();

                    if (dt.Rows.Count > 0)
                    {
                        if (Msg.ConfYesNo("更新してもよろしいでしょうか？") == DialogResult.No)
                        {
                            return;
                        }
                        #region セリデータ削除処理
                        dpc = new DbParamCollection();
                        sql = new StringBuilder();
                        sql.Append("DELETE ");
                        sql.Append("  FROM TW_KY_F_SHY_SHTK_ZIGK_SNSHTS_R");

                        this.Dba.ModifyBySql(Util.ToString(sql), dpc);
                        #endregion
                    }

                    //テキストファイルからデータを取得
                    dpc = new DbParamCollection();
                    sql = new StringBuilder();
                    sql.Append(" BULK INSERT TW_KY_F_SHY_SHTK_ZIGK_SNSHTS_R ");
                    sql.Append(" FROM '" + dirPath.LocalPath + "'");
                    sql.Append(" WITH ");
                    sql.Append(" ( FIELDTERMINATOR = ',',");
                    sql.Append("    ROWTERMINATOR = '\n' ");
                    sql.Append("  );  ");
                    this.Dba.ModifyBySql(Util.ToString(sql), dpc);

                    // トランザクションをコミット
                    this.Dba.Commit();
                    Msg.Info("取り込み処理が正常に終了しました。");
                }
                finally
                {
                    // ロールバック
                    this.Dba.Rollback();
                }
            }
        }
        /// <summary>
        /// 行番号の最大値を取得
        /// </summary>
        private int MaxGyoNo(int kubun)
        {

            int max_no = 0; 
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();
            sql.Append(" SELECT ");
            sql.Append(" MAX(GYO_NO) AS MAX_NO");
            sql.Append(" FROM ");
            sql.Append(" TB_KY_ZEIGAKU_HYO_PATH ");
            sql.Append(" WHERE ");
            sql.Append(" KAISHA_CD = @KAISHA_CD AND");
            sql.Append(" KUBUN = @KUBUN ");
            
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@KUBUN", SqlDbType.Decimal, 4, kubun);
            DataTable dt = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql) ,dpc);

            if(dt.Rows.Count > 0)
            {
                max_no = Util.ToInt(dt.Rows[0]["MAX_NO"]);
            }
            return max_no;
        }
        /// <summary>
        /// データを表示する
        /// </summary>
        private void SearchData(int kubun)
        {
            // データを取得して表示
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder where = new StringBuilder("");
            // 会社コード指定
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
            dpc.SetParam("@KUBUN", SqlDbType.Decimal, 1, kubun);

            where.Append("KAISHA_CD = @KAISHA_CD AND ");
            where.Append("KUBUN = @KUBUN");

            string cols = "GYO_NO,";
            cols += "FILE_NM,";
            cols += "FILE_PATH";
            string from = "TB_KY_ZEIGAKU_HYO_PATH";

            DataTable dt =
                this.Dba.GetDataTableByConditionWithParams(cols, from,
                    Util.ToString(where), "GYO_NO", dpc);

            if (kubun == 1)//給与
            {
                // データグリッドビュー初期化
                this.dgvInputList.Rows.Clear();

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    // 列の追加
                    this.dgvInputList.Rows.Add();
                    // 列の高さ設定
                    this.dgvInputList.Rows[i].Height = 27;
                    // 列の値設定
                    this.dgvInputList[COL_GYO_NO, i].Value = dt.Rows[i]["GYO_NO"];
                    this.dgvInputList[COL_FILE_NM, i].Value = dt.Rows[i]["FILE_NM"];
                    this.dgvInputList[COL_FILE_PATH, i].Value = dt.Rows[i]["FILE_PATH"];
                    //最初の行だけチェックする
                    if (i == 0)
                    {
                        this.dgvInputList[COL_CHECK, i].Value = "1";
                    }
                    else
                    {
                        this.dgvInputList[COL_CHECK, i].Value = "0";
                    }
                }
                
                // ユーザーによるソートを禁止させる
                foreach (DataGridViewColumn c in this.dgvInputList.Columns)
                    c.SortMode = DataGridViewColumnSortMode.NotSortable;

                // フォントを設定する
                this.dgvInputList.ColumnHeadersVisible = false;
                this.dgvInputList.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F);
            }
            else// 賞与
            {
                // データグリッドビュー初期化
                this.dataGridView1.Rows.Clear();

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    // 列の追加
                    this.dataGridView1.Rows.Add();
                    // 列の高さ設定
                    this.dataGridView1.Rows[i].Height = 27;
                    // 列の値設定
                    this.dataGridView1[COL_GYO_NO, i].Value = dt.Rows[i]["GYO_NO"];
                    this.dataGridView1[COL_FILE_NM, i].Value = dt.Rows[i]["FILE_NM"];
                    this.dataGridView1[COL_FILE_PATH, i].Value = dt.Rows[i]["FILE_PATH"];
                    //最初の行だけチェックする
                    if (i == 0)
                    {
                        this.dataGridView1[COL_CHECK, i].Value = "1";
                    }
                    else
                    {
                        this.dataGridView1[COL_CHECK, i].Value = "0";
                    }
                }

                // ユーザーによるソートを禁止させる
                foreach (DataGridViewColumn c in this.dataGridView1.Columns)
                    c.SortMode = DataGridViewColumnSortMode.NotSortable;

                // フォントを設定する
                this.dataGridView1.ColumnHeadersVisible = false;
                this.dataGridView1.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F);
            }

        }
        #endregion
    }
}