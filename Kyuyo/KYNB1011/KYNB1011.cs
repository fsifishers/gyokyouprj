﻿using System;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using System.Reflection;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;
using System.Text;
using System.Collections;

namespace jp.co.fsi.ky.kynb1011
{

    /// <summary>
    /// 年末調整処理(KYNB1011)
    /// </summary>
    public partial class KYNB1011 : BasePgForm
    {
        #region 変数
        DataTable _dtKyuSti;                  //  給与合計項目設定データ
        DataTable _dtShoSti;                  //  賞与合計項目設定データ
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KYNB1011()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)

        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 年度初期値
            if (Util.ToInt(Util.ToString(this.txtGengoYearNendo.Text)) == 0)
            {
                string[] nendo = Util.ConvJpDate(new DateTime(this.UInfo.KaikeiNendo, 4, 1), this.Dba);
                this.lblGengoNendo.Text = nendo[0];
                this.txtGengoYearNendo.Text = nendo[2];
            }

            // 還付年月の初期値取得
            string[] jpDate;
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            string sql = "SELECT MAX(NENGETSU) AS MAX_NENGETSU";
            sql += " FROM TB_KY_KYUYO_MEISAI_KINTAI WHERE KAISHA_CD = @KAISHA_CD";
            DataTable dtShikyuInfo =
                this.Dba.GetDataTableFromSqlWithParams(sql, dpc);
            if (dtShikyuInfo.Rows.Count == 0 || ValChk.IsEmpty(dtShikyuInfo.Rows[0]["MAX_NENGETSU"]))
            {
                // 該当支給データ存在しない場合はシステム日付をセット
                jpDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            }
            else
            {
                jpDate = Util.ConvJpDate(Util.ToDate(dtShikyuInfo.Rows[0]["MAX_NENGETSU"]), this.Dba);
            }
            lblGengoNengetsu.Text     = jpDate[0];
            txtGengoYearNengetsu.Text = jpDate[2];
            txtMonthNengetsu.Text     = jpDate[3];

            // 調整方法初期値
            this.rdoChosei3.Checked = true;

            // 初期フォーカス
            this.txtGengoYearNendo.Focus();
            this.txtGengoYearNendo.SelectAll();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 元号年とコード項目に
            // フォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtGengoYearNendo":
                case "txtGengoYearNengetsu":
                case "txtShainCdFr":
                case "txtShainCdTo":
                case "txtBumonCdFr":
                case "txtBumonCdTo":
                case "txtBukaCdFr":
                case "txtBukaCdTo":
                    this.btnF1.Enabled = true;
                    break;
                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        public override void PressF1()
        {
            Assembly asm;
            Type t;

            switch (this.ActiveCtlNm)
            {
                case "txtGengoYearNendo":
                case "txtGengoYearNengetsu":
                    #region 元号検索
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            if (this.ActiveCtlNm == "txtGengoYearNendo")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.InData = this.lblGengoNendo.Text;
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.lblGengoNendo.Text = result[1];

                                    // 和暦年未入力時の初期値
                                    if (Util.ToInt(Util.ToString(this.txtGengoYearNendo.Text)) == 0)
                                    {
                                        string[] nendo = Util.ConvJpDate(new DateTime(this.UInfo.KaikeiNendo, 4, 1), this.Dba);
                                        this.lblGengoNendo.Text = nendo[0];
                                        this.txtGengoYearNendo.Text = nendo[2];
                                    }

                                    // 和暦年補正
                                    DateTime adDate =
                                        Util.ConvAdDate(this.lblGengoNendo.Text, this.txtGengoYearNendo.Text, "4", "1", this.Dba);
                                    string[] jpDate = Util.ConvJpDate(adDate, this.Dba);
                                    this.lblGengoNendo.Text = jpDate[0];
                                    this.txtGengoYearNendo.Text = jpDate[2];
                                }
                            }
                            else if (this.ActiveCtlNm == "txtGengoYearNengetsu")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.InData = this.lblGengoNengetsu.Text;
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.lblGengoNengetsu.Text = result[1];

                                    // 存在しない日付の場合、補正して存在する日付に戻す
                                    string[] arrJpDate =
                                        Util.FixJpDate(this.lblGengoNengetsu.Text,
                                            this.txtGengoYearNengetsu.Text,
                                            this.txtMonthNengetsu.Text,
                                            "1",
                                            this.Dba);
                                    this.lblGengoNengetsu.Text = arrJpDate[0];
                                    this.txtGengoYearNengetsu.Text = arrJpDate[2];
                                    this.txtMonthNengetsu.Text = arrJpDate[3];
                                }
                            }
                        }

                    }
                    #endregion
                    break;
                case "txtShainCdFr":
                case "txtShainCdTo":
                    #region 社員検索
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("KYCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.ky.kycm1021.KYCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            if (this.ActiveCtlNm == "txtShainCdFr")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.Par1 = "1";                     // ダイアログ起動パラメータ
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.txtShainCdFr.Text = result[0];
                                    this.lblShainCdFr.Text = result[1];
                                }
                            }
                            else if (this.ActiveCtlNm == "txtShainCdTo")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.Par1 = "1";                     // ダイアログ起動パラメータ
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.txtShainCdTo.Text = result[0];
                                    this.lblShainCdTo.Text = result[1];
                                }
                            }
                        }
                    }
                    #endregion
                    break;
                case "txtBumonCdFr":
                case "txtBumonCdTo":
                    #region 部門検索
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("KYCM1031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.ky.kycm1031.KYCM1031");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            if (this.ActiveCtlNm == "txtBumonCdFr")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.Par1 = "1";
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.txtBumonCdFr.Text = result[0];
                                    this.lblBumonCdFr.Text = result[1];
                                    this.txtBukaCdFr.Text = "";
                                    this.lblBukaCdFr.Text = "先　頭";
                                }
                            }
                            else if (this.ActiveCtlNm == "txtBumonCdTo")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.Par1 = "1";
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.txtBumonCdTo.Text = result[0];
                                    this.lblBumonCdTo.Text = result[1];
                                    this.txtBukaCdTo.Text = "";
                                    this.lblBukaCdTo.Text = "最　後";
                                }
                            }
                        }
                    }
                    #endregion
                    break;                
                case "txtBukaCdFr":
                case "txtBukaCdTo":
                    #region 部課検索
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("KYCM1041.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.ky.kycm1041.KYCM1041");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            if (this.ActiveCtlNm == "txtBukaCdFr" 
                                && !ValChk.IsEmpty(this.txtBumonCdFr.Text))
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.Par1 = "1";                         // 検索ダイアログ起動パラメータ
                                frm.InData = this.txtBumonCdFr.Text;    // (string)部門コード
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.txtBukaCdFr.Text = result[0];
                                    this.lblBukaCdFr.Text = result[1];
                                }
                            }
                            else if (this.ActiveCtlNm == "txtBukaCdTo"
                                && !ValChk.IsEmpty(this.txtBumonCdTo.Text))
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.Par1 = "1";                         // 検索ダイアログ起動パラメータ
                                frm.InData = this.txtBumonCdTo.Text;    // (string)部門コード
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.txtBukaCdTo.Text = result[0];
                                    this.lblBukaCdTo.Text = result[1];
                                }
                            }
                        }
                    }
                    #endregion
                    break;
            }
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        public override void PressF6()
        {
            if (Msg.ConfYesNo("実行しますか？") == DialogResult.Yes)
            {
                NenmatsuChoseiExecute();
            }
        }
        #endregion

        #region イベント

        /// <summary>
        /// 調整方法変更時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rdoChosei_CheckedChanged(object sender, EventArgs e)
        {
            // 給与調整の時のみ還付年月を表示
            this.grpNengetsu.Visible = this.rdoChosei1.Checked;
        }

        /// <summary>
        /// 社員コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShainCdFr_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtShainCdFr.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtShainCdFr.SelectAll();
                e.Cancel = true;
                return;
            }

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (this.txtShainCdFr.Text.Length > 0)
            {
                this.lblShainCdFr.Text = this.Dba.GetName(this.UInfo, "TB_KY_SHAIN_JOHO", " ", this.txtShainCdFr.Text);
            }
            else
            {
                this.lblShainCdFr.Text = "先　頭";
            }
        }

        /// <summary>
        /// 社員コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShainCdTo_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtShainCdTo.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtShainCdTo.SelectAll();
                e.Cancel = true;
                return;
            }

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (this.txtShainCdTo.Text.Length > 0)
            {
                this.lblShainCdTo.Text = this.Dba.GetName(this.UInfo, "TB_KY_SHAIN_JOHO", " ", this.txtShainCdTo.Text);
            }
            else
            {
                this.lblShainCdTo.Text = "最　後";
            }
        }

        /// <summary>
        /// 部門コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBumonCdFr_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtBumonCdFr.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtBumonCdFr.SelectAll();
                e.Cancel = true;
                return;
            }

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (this.txtBumonCdFr.Text.Length > 0)
            {
                this.lblBumonCdFr.Text = this.Dba.GetName(this.UInfo, "TB_KY_BUMON", " ", this.txtBumonCdFr.Text);
            }
            else
            {
                this.lblBumonCdFr.Text = "先　頭";
            }
        }

        /// <summary>
        /// 部門コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBumonCdTo_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtBumonCdTo.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtBumonCdTo.SelectAll();
                e.Cancel = true;
                return;
            }

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (this.txtBumonCdTo.Text.Length > 0)
            {
                this.lblBumonCdTo.Text = this.Dba.GetName(this.UInfo, "TB_KY_BUMON", " ", this.txtBumonCdTo.Text);
            }
            else
            {
                this.lblBumonCdTo.Text = "最　後";
            }
        }

        /// <summary>
        /// 部課コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBukaCdFr_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtBukaCdFr.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtBukaCdFr.SelectAll();
                e.Cancel = true;
                return;
            }

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (this.txtBukaCdFr.Text.Length > 0)
            {
                this.lblBukaCdFr.Text = this.Dba.GetBukaNm(this.UInfo, this.txtBumonCdFr.Text, this.txtBukaCdFr.Text);
            }
            else
            {
                this.lblBukaCdFr.Text = "先　頭";
            }
        }

        /// <summary>
        /// 部課コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBukaCdTo_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtBukaCdTo.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtBukaCdTo.SelectAll();
                e.Cancel = true;
                return;
            }

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (this.txtBukaCdTo.Text.Length > 0)
            {
                this.lblBukaCdTo.Text = this.Dba.GetBukaNm(this.UInfo,this.txtBumonCdTo.Text, this.txtBukaCdTo.Text);
            }
            else
            {
                this.lblBukaCdTo.Text = "最　後";
            }
        }

        /// <summary>
        /// 支給月(年)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGengoYearNengetsu_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtGengoYearNengetsu.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtGengoYearNengetsu.SelectAll();
                e.Cancel = true;
                return;
            }

            // 空の場合、0年として処理
            if (ValChk.IsEmpty(this.txtGengoYearNengetsu.Text))
            {
                this.txtGengoYearNengetsu.Text = "0";
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            this.SetJpDateNengetsu(Util.FixJpDate(this.lblGengoNengetsu.Text, this.txtGengoYearNengetsu.Text,
                this.txtMonthNengetsu.Text, "1", this.Dba));
        }

        /// <summary>
        /// 支給月(月)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonthNengetsu_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtMonthNengetsu.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtMonthNengetsu.SelectAll();
                e.Cancel = true;
                return;
            }

            if (ValChk.IsEmpty(this.txtMonthNengetsu.Text) || Util.ToInt(this.txtMonthNengetsu.Text) == 0)
            {
                // 空の場合、1月として処理
                this.txtMonthNengetsu.Text = "1";
            }
            else
            {
                // 12を超える月が入力された場合、12月として処理
                if (Util.ToInt(this.txtMonthNengetsu.Text) > 12)
                {
                    this.txtMonthNengetsu.Text = "12";
                }
            }

            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            this.SetJpDateNengetsu(Util.FixJpDate(this.lblGengoNengetsu.Text, this.txtGengoYearNengetsu.Text,
                this.txtMonthNengetsu.Text, "1", this.Dba));
        }

        /// <summary>
        /// 対象年月(年)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGengoYearNendo_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtGengoYearNendo.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtGengoYearNendo.SelectAll();
                e.Cancel = true;
                return;
            }

            string[] nendo;

            // 年度の取得
            if (Util.ToInt(Util.ToString(this.txtGengoYearNendo.Text)) == 0)
            {
                // 無効年入力の時
                nendo = Util.ConvJpDate(new DateTime(this.UInfo.KaikeiNendo, 1, 1), this.Dba);
            }
            else
            {
                DateTime adNendo =
                    Util.ConvAdDate(this.lblGengoNendo.Text, this.txtGengoYearNendo.Text, "4", "1", this.Dba);
                nendo = Util.ConvJpDate(adNendo, this.Dba);
            }
            this.lblGengoNendo.Text = nendo[0];
            this.txtGengoYearNendo.Text = nendo[2];
        }

        /// <summary>
        /// 画面の最後のコントールでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LastControl_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.btnF6.Enabled)
            {
                this.PressF6();
            }
        }
        #endregion

        #region privateメソッド

        /// <summary>
        /// 配列に格納された和暦支給月を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpDateNengetsu(string[] arrJpDate)
        {
            this.lblGengoNengetsu.Text = arrJpDate[0];
            this.txtGengoYearNengetsu.Text = arrJpDate[2];
            this.txtMonthNengetsu.Text = arrJpDate[3];
        }

        /// <summary>
        /// 年末調整処理
        /// </summary>
        private void NenmatsuChoseiExecute()
        {
            DateTime nendo;                                             // 年度
            DateTime nengetsu;                                          // 還付年月
            DateTime shoyoNengetsu;                                     // 賞与調整年月
            string shainCdFr;                                           // 社員コード(自)
            string shainCdTo;                                           // 社員コード(至)
            string bumonCdFr;                                           // 部門コード(自)
            string bumonCdTo;                                           // 部門コード(至)
            string bukaCdFr;                                            // 部課コード(自)
            string bukaCdTo;                                            // 部課コード(至)

            DbParamCollection dpc;
            StringBuilder sql;
            ArrayList alParams;
            string tableNm;
            string where;
            string order;

            // 共通情報セット

            #region 画面上の指定情報を保持
            nendo =
                Util.ConvAdDate(this.lblGengoNendo.Text, this.txtGengoYearNendo.Text, "1", "1", this.Dba);
            nengetsu =
                Util.ConvAdDate(this.lblGengoNengetsu.Text, this.txtGengoYearNengetsu.Text
                , this.txtMonthNengetsu.Text, "1", this.Dba);
            shoyoNengetsu =
                Util.ConvAdDate(this.lblGengoNendo.Text, this.txtGengoYearNendo.Text, "12", "1", this.Dba);
            // 社員コード設定
            if (Util.ToDecimal(txtShainCdFr.Text) > 0)
            {
                shainCdFr = txtShainCdFr.Text;
            }
            else
            {
                shainCdFr = "0";
            }
            if (Util.ToDecimal(txtShainCdTo.Text) > 0)
            {
                shainCdTo = txtShainCdTo.Text;
            }
            else
            {
                shainCdTo = "999999";
            }
            // 部門コード設定
            if (Util.ToDecimal(txtBumonCdFr.Text) > 0)
            {
                bumonCdFr = txtBumonCdFr.Text;
            }
            else
            {
                bumonCdFr = "0";
            }
            if (Util.ToDecimal(txtBumonCdTo.Text) > 0)
            {
                bumonCdTo = txtBumonCdTo.Text;
            }
            else
            {
                bumonCdTo = "9999";
            }
            // 部課コード設定
            if (Util.ToDecimal(txtBukaCdFr.Text) > 0)
            {
                bukaCdFr = txtBukaCdFr.Text;
            }
            else
            {
                bukaCdFr = "0";
            }
            if (Util.ToDecimal(txtBukaCdTo.Text) > 0)
            {
                bukaCdTo = txtBukaCdTo.Text;
            }
            else
            {
                bukaCdTo = "9999";
            }
            #endregion

            #region dtShainJoho = 処理対象社員データ
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 2, this.UInfo.KaishaCd);
            dpc.SetParam("@SHAIN_CD_FR", SqlDbType.VarChar, 6, shainCdFr);
            dpc.SetParam("@SHAIN_CD_TO", SqlDbType.VarChar, 6, shainCdTo);
            dpc.SetParam("@BUMON_CD_FR", SqlDbType.VarChar, 4, bumonCdFr);
            dpc.SetParam("@BUMON_CD_TO", SqlDbType.VarChar, 4, bumonCdTo);
            dpc.SetParam("@BUKA_CD_FR", SqlDbType.VarChar, 4, bukaCdFr);
            dpc.SetParam("@BUKA_CD_TO", SqlDbType.VarChar, 4, bukaCdTo);
            sql = new StringBuilder();
            sql.Append("SELECT * FROM TB_KY_SHAIN_JOHO");
            sql.Append(" WHERE KAISHA_CD = @KAISHA_CD");
            sql.Append(" AND TAISHOKU_NENGAPPI IS NULL");
            sql.Append(" AND SHAIN_CD BETWEEN @SHAIN_CD_FR AND @SHAIN_CD_TO");
            sql.Append(" AND BUMON_CD BETWEEN @BUMON_CD_FR AND @BUMON_CD_TO");
            sql.Append(" AND BUKA_CD BETWEEN @BUKA_CD_FR AND @BUKA_CD_TO");
            sql.Append(" ORDER BY KAISHA_CD ASC, SHAIN_CD ASC");
            DataTable dtShainJoho = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
            #endregion

            #region 給与/賞与合計項目設定データの読込
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            StringBuilder cols = new StringBuilder();
            cols.Append("KOMOKU_BANGO");
            cols.Append(" ,KOMOKU_NM");
            cols.Append(" ,KOMOKU_KUBUN1");
            cols.Append(" ,KOMOKU_KUBUN2");
            cols.Append(" ,KOMOKU_KUBUN3");
            cols.Append(" ,KOMOKU_KUBUN4");
            cols.Append(" ,KOMOKU_KUBUN5");
            cols.Append(" ,NYURYOKU_KUBUN");
            cols.Append(" ,KEISAN_KUBUN");
            cols.Append(" ,KEISAN_JUNI");
            cols.Append(" ,KEISANSHIKI");
            where = "KAISHA_CD = @KAISHA_CD";
            order = "KOMOKU_BANGO";
            tableNm = "TB_KY_KYUYO_GOKEI_KOMOKU_STI";           // 給与合計項目設定データ
            _dtKyuSti = this.Dba.GetDataTableByConditionWithParams(Util.ToString(cols), tableNm, where, order, dpc);
            tableNm = "TB_KY_SHOYO_GOKEI_KOMOKU_STI";           // 賞与合計項目設定データ
            _dtShoSti = this.Dba.GetDataTableByConditionWithParams(Util.ToString(cols), tableNm, where, order, dpc);
            #endregion

            #region 年調計算用変数定義
            // TB年末調整への更新あり
            decimal kyuyotoKazeiKingaku;                    // ■給与等課税金額 
            decimal kyuyotoShotokuzeigaku;                  // ■給与等所得税額 
            decimal shoyotoKazeiKingaku;                    // ■賞与等課税金額 
            decimal shoyotoShotokuzeigaku;                  // ■賞与等所得税額 
            decimal kyuyoShotokuKojogoNoKngk;               // ■給与所得控除後の金額 
            decimal shakaiHokenryoKjgkKyuyoto;              // ■社会保険料控除額給与等
            decimal haigushaKojogaku;                       // ■配偶者控除額 
            decimal fuyoKojogaku;                           // ■扶養控除額 
            decimal kisoKojogaku;                           // ■基礎控除額 
            decimal shogaishaKojogaku;                      // ■障害者控除額 
            decimal ronenshaKafuKojogaku;                   // ■老年者寡婦控除額 
            decimal kinroGakuseiKojogaku;                   // ■勤労学生控除額 
            decimal kazeiKyuyoShotokuKingaku;               // ■課税給与所得金額 
            decimal sanshutsuNenzeigaku;                    // ■算出年税額 
            decimal nenchoNenzeigaku;                       // ■TB年末調整.年調年税額(年調所得税額)
            decimal nenzeigaku;                             // ■年税額
            // TB年末調整への更新なし
            decimal kyuyoNoSogaku;                          // ●給与の総額
            decimal kojoGokei;                              // ●所得控除額の合計
            decimal choshugaku;                             // ●徴収額
            decimal kabusokugaku;                           // ●過不足額
            #endregion
            
            bool isNenmatsuChosei;                                      // 年末調整実行フラグ              

            // トランザクション開始
            this.Dba.BeginTransaction();

            // 各社員別に年調処理実行
            foreach (DataRow drShainJoho in dtShainJoho.Rows)
            {
                DispMessage("処理中です・・・社員番号： " + Util.ToString(drShainJoho["SHAIN_CD"]));

                #region 変数初期化
                kyuyotoKazeiKingaku = 0;                    // ■給与等課税金額 
                kyuyotoShotokuzeigaku = 0;                  // ■給与等所得税額 
                shoyotoKazeiKingaku = 0;                    // ■賞与等課税金額 
                shoyotoShotokuzeigaku = 0;                  // ■賞与等所得税額 
                kyuyoShotokuKojogoNoKngk = 0;               // ■給与所得控除後の金額 
                shakaiHokenryoKjgkKyuyoto = 0;              // ■社会保険料控除額給与等
                haigushaKojogaku = 0;                       // ■配偶者控除額 
                fuyoKojogaku = 0;                           // ■扶養控除額 
                kisoKojogaku = 0;                           // ■基礎控除額 
                shogaishaKojogaku = 0;                      // ■障害者控除額 
                ronenshaKafuKojogaku = 0;                   // ■老年者寡婦控除額 
                kinroGakuseiKojogaku = 0;                   // ■勤労学生控除額 
                kazeiKyuyoShotokuKingaku = 0;               // ■課税給与所得金額 
                sanshutsuNenzeigaku = 0;                    // ■算出年税額 
                nenchoNenzeigaku = 0;                       // ■TB年末調整.年調年税額(年調所得税額)
                nenzeigaku = 0;                             // ■年税額
                kyuyoNoSogaku = 0;                          // ●給与の総額
                kojoGokei = 0;                              // ●所得控除額の合計
                choshugaku = 0;                             // ●徴収額
                kabusokugaku = 0;                           // ●過不足額
                #endregion

                #region dtNenmatsuChosei = 年調入力データ 
                dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 2, this.UInfo.KaishaCd);
                dpc.SetParam("@NENDO", SqlDbType.DateTime, nendo);
                dpc.SetParam("@SHAIN_CD", SqlDbType.VarChar, 6, Util.ToString(drShainJoho["SHAIN_CD"]));
                sql = new StringBuilder();
                sql.Append("SELECT * FROM TB_KY_NENMATSU_CHOSEI");
                sql.Append(" WHERE KAISHA_CD = @KAISHA_CD");
                sql.Append(" AND NENDO = @NENDO");
                sql.Append(" AND SHAIN_CD = @SHAIN_CD");
                DataTable dtNenmatsuChosei = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
                #endregion

                // 前回調整情報をクリア
                if (dtNenmatsuChosei.Rows.Count > 0)
                {
                    if (Util.ToInt(dtNenmatsuChosei.Rows[0]["CHOSEI_F"]) == 1
                        && dtNenmatsuChosei.Rows[0]["KYUYO_KANPU_DATE"] != null)
                    {
                        ShikyuMeisaiChosei(1, drShainJoho, Util.ToDate(dtNenmatsuChosei.Rows[0]["KYUYO_KANPU_DATE"]), 0);
                    }
                    else if (Util.ToInt(dtNenmatsuChosei.Rows[0]["CHOSEI_F"]) == 2)
                    {
                        ShikyuMeisaiChosei(2, drShainJoho, shoyoNengetsu, 0);
                    }
                }

                // 年調対象判定
                isNenmatsuChosei = (Util.ToInt(drShainJoho["NENMATSU_CHOSEI"]) == 1);

                // 年調計算
                if (isNenmatsuChosei)
                {
                    #region 【P51】3-1年末調整の対象となる給与と徴収税額の集計
                    // 給与集計
                    decimal[] kyuyoto = GetMeisaiKingaku(1, nendo, Util.ToString(drShainJoho["SHAIN_CD"]));
                    kyuyotoKazeiKingaku = kyuyoto[0];                                       // ■給与等課税金額 
                    kyuyotoShotokuzeigaku = kyuyoto[2];                                     // ■給与等所得税額
                    // 賞与集計
                    decimal[] shoyoto = GetMeisaiKingaku(2, nendo, Util.ToString(drShainJoho["SHAIN_CD"]));
                    shoyotoKazeiKingaku = shoyoto[0];                                       // ■賞与等課税金額
                    shoyotoShotokuzeigaku = shoyoto[2];                                     // ■賞与等所得税額

                    shakaiHokenryoKjgkKyuyoto = kyuyoto[1] + shoyoto[1];                    // ■社会保険料控除額給与等

                    kyuyoNoSogaku = kyuyotoKazeiKingaku + shoyotoKazeiKingaku;              // ●給与の総額
                    choshugaku = kyuyoto[2] + shoyoto[2];                                   // ●徴収額

                    // 給与の総額・徴収額　～前職分、調整分を加算
                    if (dtNenmatsuChosei.Rows.Count > 0)
                    {
                        kyuyoNoSogaku += Util.ToDecimal(dtNenmatsuChosei.Rows[0]["ZENSHOKUBUN_KAZEI_KYUYOGAKU"]); 
                        kyuyoNoSogaku += Util.ToDecimal(dtNenmatsuChosei.Rows[0]["CHOSEI_KYUYO_KAZEI_KYUYOGAKU"]); 
                        kyuyoNoSogaku += Util.ToDecimal(dtNenmatsuChosei.Rows[0]["CHOSEI_SHOYO_KAZEI_KYUYOGAKU"]);
                        choshugaku += Util.ToDecimal(dtNenmatsuChosei.Rows[0]["ZENSHOKUBUN_SHOTOKUZEIGAKU"]);
                        choshugaku += Util.ToDecimal(dtNenmatsuChosei.Rows[0]["CHOSEI_KYUYO_SHOTOKUZEIGAKU"]);
                        choshugaku += Util.ToDecimal(dtNenmatsuChosei.Rows[0]["CHOSEI_SHOYO_SHOTOKUZEIGAKU"]);
                    }
                    #endregion

                    #region 【P53】3-2給与所得控除後の給与等の金額の計算
                    dpc = new DbParamCollection();
                    dpc.SetParam("@KYUYO_KINGAKU", SqlDbType.Decimal, 9, kyuyoNoSogaku);
                    sql = new StringBuilder();
                    sql.Append("SELECT KINGAKU AS KINGAKU");
                    sql.Append(",KEISANSHIKI AS KEISANSHIKI");
                    sql.Append(" FROM TB_KY_F_NNGK_KYSTK_KJGK_KISNH");
                    sql.Append(" WHERE KYUYO_KINGAKU_IJO <= @KYUYO_KINGAKU");
                    sql.Append(" AND KYUYO_KINGAKU_IKA >= @KYUYO_KINGAKU");
                    DataTable dtKystkKjgk = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
                    if (dtKystkKjgk.Rows.Count > 0)
                    {
                        if (!ValChk.IsEmpty(dtKystkKjgk.Rows[0]["KINGAKU"]))
                        {
                            // ■給与所得控除後の金額
                            kyuyoShotokuKojogoNoKngk = Util.ToDecimal(dtKystkKjgk.Rows[0]["KINGAKU"]);
                        }
                        else if (!ValChk.IsEmpty(dtKystkKjgk.Rows[0]["KEISANSHIKI"]))
                        {
                            // 計算式の適用
                            string shiki = Util.ToString(dtKystkKjgk.Rows[0]["KEISANSHIKI"]);
                            shiki = shiki.Replace("給与金額", Util.ToString(kyuyoNoSogaku));
                            DataTable dt = this.Dba.GetDataTableFromSql(
                                "SELECT CAST(" + shiki + " AS DECIMAL(9, 0)) AS KINGAKU");
                            if (dt.Rows.Count > 0)
                            {
                                // ■給与所得控除後の金額
                                kyuyoShotokuKojogoNoKngk = Util.ToDecimal(dt.Rows[0]["KINGAKU"]);
                            }
                        }
                    }
                    #endregion

                    #region 【P55】3-5(1)課税給与所得金額の計算
                    if (dtNenmatsuChosei.Rows.Count > 0)
                    {
                        // ■社会保険料控除額給与等 ～前職分・申告分・調整分を加算
                        //shakaiHokenryoKjgkKyuyoto += Util.ToDecimal(dtNenmatsuChosei.Rows[0]["ZENSHOKUBUN_SHAKAI_HOKENRYO"]);
                        //shakaiHokenryoKjgkKyuyoto += Util.ToDecimal(dtNenmatsuChosei.Rows[0]["SHAKAI_HOKENRYO_KJGK_SNKKBN"]);
                        //shakaiHokenryoKjgkKyuyoto += Util.ToDecimal(dtNenmatsuChosei.Rows[0]["CHOSEI_KYUYO_SHAKAI_HOKENRYO"]);
                        //shakaiHokenryoKjgkKyuyoto += Util.ToDecimal(dtNenmatsuChosei.Rows[0]["CHOSEI_KYUYO_SHAKAI_HOKENRYO"]);

                        // ■配偶者控除額
                        haigushaKojogaku = GetKojogaku(1, Util.ToInt(drShainJoho["HAIGUSHA_KUBUN"]));

                        // ■扶養控除額
                        fuyoKojogaku = GetKojogaku(2, 1) * Util.ToDecimal(drShainJoho["FUYO_IPPAN"]);       // 扶養一般
                        fuyoKojogaku += GetKojogaku(2, 3) * Util.ToDecimal(drShainJoho["FUYO_TOKUTEI"]);    // 扶養特定
                        fuyoKojogaku += GetKojogaku(2, 4)
                            * Util.ToDecimal(drShainJoho["FUYO_ROJIN_DOKYO_ROSHINTO_IGAI"]);    // 扶養老人同居老親等以外
                        fuyoKojogaku += GetKojogaku(2, 5)
                            * Util.ToDecimal(drShainJoho["FUYO_ROJIN_DOKYO_ROSHINTO"]);         // 扶養老人同居老親等

                        // ■障害者控除額
                        shogaishaKojogaku = GetKojogaku(2, 6) 
                            * Util.ToDecimal(drShainJoho["FUYO_DOKYO_TOKUSHO_IPPAN"]);                      // 扶養一般
                        shogaishaKojogaku += GetKojogaku(3, 1) 
                            * Util.ToDecimal(drShainJoho["SHOGAISHA_IPPAN"]);                               // 扶養特定
                        shogaishaKojogaku += GetKojogaku(3, 2) 
                            * Util.ToDecimal(drShainJoho["SHOGAISHA_TOKUBETSU"]);                           // 扶養特定
                        shogaishaKojogaku += GetKojogaku(3, Util.ToInt(drShainJoho["HONNIN_KUBUN2"]));      // 障害者本人

                        // ■老年者寡婦控除額
                        ronenshaKafuKojogaku = GetKojogaku(4, Util.ToInt(drShainJoho["HONNIN_KUBUN1"]));    // 寡夫・寡婦

                        // ■勤労学生控除額
                        kinroGakuseiKojogaku = GetKojogaku(5, Util.ToInt(drShainJoho["HONNIN_KUBUN3"]));    // 勤労学生

                        // ■基礎控除
                        kisoKojogaku = GetKojogaku(6, 1);

                        // ●所得控除額の合計
                        kojoGokei = shakaiHokenryoKjgkKyuyoto;
                        kojoGokei += Util.ToDecimal(dtNenmatsuChosei.Rows[0]["ZENSHOKUBUN_SHAKAI_HOKENRYO"]);
                        kojoGokei += Util.ToDecimal(dtNenmatsuChosei.Rows[0]["CHOSEI_KYUYO_SHAKAI_HOKENRYO"]);
                        kojoGokei += Util.ToDecimal(dtNenmatsuChosei.Rows[0]["CHOSEI_SHOYO_SHAKAI_HOKENRYO"]);
                        kojoGokei += Util.ToDecimal(dtNenmatsuChosei.Rows[0]["SHAKAI_HOKENRYO_KJGK_SNKKBN"]);
                        kojoGokei += Util.ToDecimal(dtNenmatsuChosei.Rows[0]["SKB_KIGYO_KYOSAI_KAKEKIN_KJGK"]);
                        kojoGokei += Util.ToDecimal(dtNenmatsuChosei.Rows[0]["SEIMEIHOKENRYO_KOJOGAKU"]);
                        kojoGokei += Util.ToDecimal(dtNenmatsuChosei.Rows[0]["SONGAIHOKENRYO_KOJOGAKU"]);
                        kojoGokei += Util.ToDecimal(dtNenmatsuChosei.Rows[0]["HAIGUSHA_TOKUBETSU_KOJOGAKU"]);
                        kojoGokei += haigushaKojogaku;
                        kojoGokei += fuyoKojogaku;
                        kojoGokei += shogaishaKojogaku;
                        kojoGokei += ronenshaKafuKojogaku;
                        kojoGokei += kinroGakuseiKojogaku;
                        kojoGokei += kisoKojogaku;

                        // ■課税給与所得金額
                        if (kyuyoShotokuKojogoNoKngk > kojoGokei)
                        {
                            kazeiKyuyoShotokuKingaku = Math.Floor((kyuyoShotokuKojogoNoKngk - kojoGokei) / 1000m) * 1000;
                        }
                        else
                        {
                            kazeiKyuyoShotokuKingaku = Math.Floor((kojoGokei - kyuyoShotokuKojogoNoKngk) / 1000m) * -1000;
                        }
                    }
                    #endregion

                    #region 【P55】3-5(2)課税給与所得金額に対する算出所得税額の計算
                    dpc = new DbParamCollection();
                    dpc.SetParam("@SHOTOKU_KINGAKU", SqlDbType.Decimal, 9, kazeiKyuyoShotokuKingaku);
                    sql = new StringBuilder();
                    sql.Append("SELECT KEISANSHIKI AS KEISANSHIKI");
                    sql.Append(" FROM TB_KY_F_NENGAKU_STKZIGK_KISNH");
                    sql.Append(" WHERE SHOTOKU_KINGAKU_IJO <= @SHOTOKU_KINGAKU");
                    sql.Append(" AND SHOTOKU_KINGAKU_IKA >= @SHOTOKU_KINGAKU");
                    DataTable dtStkzigk = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
                    if (dtStkzigk.Rows.Count > 0)
                    {
                        if (dtStkzigk.Rows[0]["KEISANSHIKI"] != null)
                        {
                            // 計算式の適用
                            string shiki = Util.ToString(dtStkzigk.Rows[0]["KEISANSHIKI"]);
                            shiki = shiki.Replace("所得金額", Util.ToString(kazeiKyuyoShotokuKingaku));
                            DataTable dt = this.Dba.GetDataTableFromSql(
                                "SELECT CAST(" + shiki + " AS DECIMAL(9, 0)) AS KINGAKU");
                            if (dt.Rows.Count > 0)
                            {
                                // ■算出年税額
                                sanshutsuNenzeigaku = Util.ToDecimal(dt.Rows[0]["KINGAKU"]);
                            }
                        }
                    }
                    #endregion

                    #region 【P55】3-6(2)年調所得税額の計算
                    // ■TB年末調整.年調年税額(年調所得税額)
                    nenchoNenzeigaku = sanshutsuNenzeigaku;
                    if (dtNenmatsuChosei.Rows.Count > 0)
                    {
                        nenchoNenzeigaku -= Util.ToDecimal(dtNenmatsuChosei.Rows[0]["JUTAKU_SHUTOKUTO_TKBT_KOJOGAKU"]);
                    }
                    if (nenchoNenzeigaku < 0)
                    {
                        nenchoNenzeigaku = 0;
                    }
                    #endregion

                    #region 【P55】3-6(3)年税額の計算
                    nenzeigaku = Math.Floor((nenchoNenzeigaku * 1.021m) / 100m) * 100m;
                    #endregion

                    #region 【P57】過不足額
                    kabusokugaku = nenzeigaku - choshugaku;                                 // ●過不足額
                    #endregion
                }
                else
                {

                    #region 【P51】3-1年末調整の対象となる給与と徴収税額の集計
                    // 給与集計
                    decimal[] kyuyoto = GetMeisaiKingaku(1, nendo, Util.ToString(drShainJoho["SHAIN_CD"]));
                    kyuyotoKazeiKingaku = kyuyoto[0];                                       // ■給与等課税金額 
                    kyuyotoShotokuzeigaku = kyuyoto[2];                                     // ■給与等所得税額
                    // 賞与集計
                    decimal[] shoyoto = GetMeisaiKingaku(2, nendo, Util.ToString(drShainJoho["SHAIN_CD"]));
                    shoyotoKazeiKingaku = shoyoto[0];                                       // ■賞与等課税金額
                    shoyotoShotokuzeigaku = shoyoto[2];                                     // ■賞与等所得税額

                    shakaiHokenryoKjgkKyuyoto = kyuyoto[1] + shoyoto[1];                    // ■社会保険料控除額給与等
                    #endregion
                }
                // 年調データ更新
                tableNm = "TB_KY_NENMATSU_CHOSEI";
                where = "KAISHA_CD = @KAISHA_CD AND NENDO = @NENDO AND SHAIN_CD = @SHAIN_CD";
                alParams = SetKyNenmatsuChoseiParams(
                    drShainJoho                             // 社員情報DataRow
                    , nendo                                 // 年度
                    , nengetsu                              // 還付年月
                    , kyuyotoKazeiKingaku                   // ■給与等課税金額 
                    , kyuyotoShotokuzeigaku                 // ■給与等所得税額 
                    , shoyotoKazeiKingaku                   // ■賞与等課税金額 
                    , shoyotoShotokuzeigaku                 // ■賞与等所得税額 
                    , kyuyoShotokuKojogoNoKngk              // ■給与所得控除後の金額 
                    , shakaiHokenryoKjgkKyuyoto             // ■社会保険料控除額給与等
                    , haigushaKojogaku                      // ■配偶者控除額 
                    , fuyoKojogaku                          // ■扶養控除額 
                    , kisoKojogaku                          // ■基礎控除額 
                    , shogaishaKojogaku                     // ■障害者控除額 
                    , ronenshaKafuKojogaku                  // ■老年者寡婦控除額 
                    , kinroGakuseiKojogaku                  // ■勤労学生控除額 
                    , kazeiKyuyoShotokuKingaku              // ■課税給与所得金額 
                    , sanshutsuNenzeigaku                   // ■算出年税額 
                    , nenchoNenzeigaku                      // ■TB年末調整.年調年税額(年調所得税額)
                    , nenzeigaku                            // ■年税額
                    );
                this.Dba.Update(tableNm, (DbParamCollection)alParams[1], where, (DbParamCollection)alParams[0]);

                // 給与調整
                if (this.rdoChosei1.Checked)
                {
                    ShikyuMeisaiChosei(1, drShainJoho, nengetsu, kabusokugaku);
                }

                // 賞与調整
                if (this.rdoChosei2.Checked)
                {
                    ShikyuMeisaiChosei(2, drShainJoho, shoyoNengetsu, kabusokugaku);
                }
            }
            // トランザクションをコミット
            this.Dba.Commit();

            DispMessage("");
            Msg.Info("処理を終了しました。＜件数：" + Util.ToString(dtShainJoho.Rows.Count) + "件＞");
        }

        /// <summary>
        /// 給与/賞与支給明細へ過不足額調整
        /// </summary>
        /// <param name="type">1給与 2賞与</param>
        /// <param name="shainCd">社員情報DataRow</param>
        /// <param name="nengetsu">年月</param>
        /// <param name="kabusokugaku">過不足額</param>
        private void ShikyuMeisaiChosei(int type, DataRow drShainJoho, DateTime nengetsu, decimal kabusokugaku)
        {
            DbParamCollection setDpc;
            DbParamCollection dpc;
            StringBuilder sql;
            string tableNm;
            string where;

            DataTable dtSti;
            string kyuSho = (type == 1) ? "KYUYO" : "SHOYO";

            #region 控除レコードへ過不足額をセット

            // 過不足セット項目番号を取得
            string updField = "";
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 2, this.UInfo.KaishaCd);
            sql = new StringBuilder();
            sql.Append("SELECT KOMOKU_BANGO");
            sql.Append(" FROM TB_KY_" + kyuSho + "_KOJO_KOMOKU_SETTEI");
            sql.Append(" WHERE KAISHA_CD = @KAISHA_CD");
            sql.Append(" AND KOMOKU_KUBUN3 = 36");
            dtSti = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
            if (dtSti.Rows.Count == 1)
            {
                updField = "KOMOKU" + Util.ToString(dtSti.Rows[0]["KOMOKU_BANGO"]);
            }
            else
            {
                throw new Exception("明細項目設定エラー");
            }

            // 支給明細控除レコードへ過不足額更新
            tableNm = "TB_KY_" + kyuSho + "_MEISAI_KOJO";
            where = "KAISHA_CD = @KAISHA_CD AND SHAIN_CD = @SHAIN_CD AND NENGETSU = @NENGETSU";
            // SET句パラメータ
            setDpc = new DbParamCollection();
            setDpc.SetParam("@" + updField, SqlDbType.Decimal, 9, kabusokugaku);
            // WHERE句パラメータ
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 2, this.UInfo.KaishaCd);
            dpc.SetParam("@SHAIN_CD", SqlDbType.Decimal, 6, Util.ToDecimal(drShainJoho["SHAIN_CD"]));
            dpc.SetParam("@NENGETSU", SqlDbType.DateTime, nengetsu);
            this.Dba.Update(tableNm, setDpc, where, dpc);
            
            #endregion

            #region 合計レコードの再計算を実行

            // 利用する項目設定合計レコード
            dtSti = (type == 1) ? _dtKyuSti : _dtShoSti;

            // 支給明細データの参照
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 2, this.UInfo.KaishaCd);
            dpc.SetParam("@SHAIN_CD", SqlDbType.Decimal, 6, Util.ToDecimal(drShainJoho["SHAIN_CD"]));
            dpc.SetParam("@NENGETSU", SqlDbType.DateTime, nengetsu);
            DataTable dt = this.Dba.GetDataTableFromSqlWithParams(GetKyMeisaiSql(type), dpc);
            if (dt.Rows.Count == 1)
            {
                DataRow[] foundRows;
                string filter;
                string order;
                string bango;
                decimal shikyugaku = 0;

                // 《★》合計レコードのSET句パラメータを準備する
                setDpc = new DbParamCollection();

                #region 計算式の実行
                filter = "KOMOKU_KUBUN3 = 0";                                       // 41-43現預金支給項目を除く
                order = "KEISAN_JUNI";
                foundRows = dtSti.Select(filter, order);
                foreach (DataRow dr in foundRows)
                {
                    bango = Util.ToString(dr["KOMOKU_BANGO"]);

                    if (!ValChk.IsEmpty(dr["KEISANSHIKI"]))
                    {
                        // 支給明細合計データテーブルに計算式列を追加して計算結果を得る
                        dt.Columns.Add("CALC" + bango, typeof(decimal), Util.ToString(dr["KEISANSHIKI"]));
                        object value = dt.Compute("SUM(CALC" + bango + ")", null);
                        if (value != null)
                        {
                            // 《★》計算式の実行結果を更新パラメータにセットする
                            setDpc.SetParam("@KOMOKU" + bango, SqlDbType.Decimal, Util.ToDecimal(value)); 

                            // 項目名「差引支給額」の計算値を保持する
                            if (Util.ToString(dr["KOMOKU_NM"]) == "差引支給額")
                            {
                                shikyugaku = Util.ToDecimal(value);
                            }
                        }
                    }
                }
                #endregion

                #region 振込計算の実行
                filter = "KOMOKU_KUBUN3 = 42 OR KOMOKU_KUBUN3 = 43";             // 42-43現預金支給項目
                foundRows = dtSti.Select(filter);
                foreach (DataRow dr in foundRows)
                {
                    bango = Util.ToString(dr["KOMOKU_BANGO"]);

                    // 給与振込１・社員支給方法１が振込の時
                    if (Util.ToString(dr["KOMOKU_KUBUN3"]).Equals("42")
                            && Util.ToString(drShainJoho["KYUYO_SHIKYU_HOHO1"]).Equals("2"))
                    {
                        // 固定金額が設定の場合
                        if (Util.ToDecimal(drShainJoho["KYUYO_SHIKYU_KINGAKU1"]) > 0)
                        {
                            // NONE 再更新はしない
                        }
                        else if (Util.ToDecimal(drShainJoho["KYUYO_SHIKYU_RITSU1"]) > 0)
                        {
                            // 《★》差引支給額から率指定で算出した金額を更新パラメータにセットする
                            decimal ritsu = Util.ToDecimal(drShainJoho["KYUYO_SHIKYU_RITSU1"]);
                            setDpc.SetParam("@KOMOKU" + bango, SqlDbType.Decimal, Util.Round(shikyugaku * ritsu / 100m, 0));
                        }
                    }
                    // 給与振込２・社員支給方法１が振込の時
                    if (Util.ToString(dr["KOMOKU_KUBUN3"]).Equals("43")
                            && Util.ToString(drShainJoho["KYUYO_SHIKYU_HOHO2"]).Equals("2"))
                    {
                        // 固定金額が設定の場合
                        if (Util.ToDecimal(drShainJoho["KYUYO_SHIKYU_KINGAKU2"]) > 0)
                        {
                            // NONE 再更新はしない
                        }
                        else if (Util.ToDecimal(drShainJoho["KYUYO_SHIKYU_RITSU2"]) > 0)
                        {
                            // 《★》差引支給額から率指定で算出した金額を更新パラメータにセットする
                            decimal ritsu = Util.ToDecimal(drShainJoho["KYUYO_SHIKYU_RITSU2"]);
                            setDpc.SetParam("@KOMOKU" + bango, SqlDbType.Decimal, Util.Round(shikyugaku * ritsu / 100m, 0));
                        }
                    }
                }
                #endregion

                // 《★》合計レコードの更新
                tableNm = "TB_KY_" + kyuSho + "_MEISAI_GOKEI";
                where = "KAISHA_CD = @KAISHA_CD AND SHAIN_CD = @SHAIN_CD AND NENGETSU = @NENGETSU";
                // WHERE句パラメータ
                dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 2, this.UInfo.KaishaCd);
                dpc.SetParam("@SHAIN_CD", SqlDbType.Decimal, 6, Util.ToDecimal(drShainJoho["SHAIN_CD"]));
                dpc.SetParam("@NENGETSU", SqlDbType.DateTime, nengetsu);
                this.Dba.Update(tableNm, setDpc, where, dpc);
            }
            
            #endregion
        }

        /// <summary>
        /// 給与/賞与明細データの照会SQLを取得
        /// </summary>
        /// <param name="type">1給与 2賞与</param>
        /// <returns></returns>
        private string GetKyMeisaiSql(int type)
        {
            string kyuSho = (type == 1) ? "KYUYO" : "SHOYO";

            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT 0 AS DUMMY");
            for (int i = 1; i <= 20; i++)
            {
                sql.Append(", K1.KOMOKU" + Util.ToString(i) + " AS 'K1.項目" + Util.ToString(i) + "'");   // 勤怠項目
            }
            for (int i = 1; i <= 20; i++)
            {
                sql.Append(", K2.KOMOKU" + Util.ToString(i) + " AS 'K2.項目" + Util.ToString(i) + "'");   // 支給項目
            }
            for (int i = 1; i <= 20; i++)
            {
                sql.Append(", K3.KOMOKU" + Util.ToString(i) + " AS 'K3.項目" + Util.ToString(i) + "'");   // 控除項目
            }
            for (int i = 1; i <= 10; i++)
            {
                sql.Append(", K4.KOMOKU" + Util.ToString(i) + " AS 'K4.項目" + Util.ToString(i) + "'");   // 合計項目
            }
            sql.Append(" FROM TB_KY_" + kyuSho + "_MEISAI_KINTAI AS K1");
            sql.Append("  INNER JOIN TB_KY_" + kyuSho + "_MEISAI_SHIKYU AS K2");
            sql.Append("  ON  K1.KAISHA_CD = K2.KAISHA_CD");
            sql.Append("  AND K1.SHAIN_CD = K2.SHAIN_CD");
            sql.Append("  AND K1.NENGETSU = K2.NENGETSU");
            sql.Append("  INNER JOIN TB_KY_" + kyuSho + "_MEISAI_KOJO AS K3");
            sql.Append("  ON  K1.KAISHA_CD = K3.KAISHA_CD");
            sql.Append("  AND K1.SHAIN_CD = K3.SHAIN_CD");
            sql.Append("  AND K1.NENGETSU = K3.NENGETSU");
            sql.Append("  INNER JOIN TB_KY_" + kyuSho + "_MEISAI_GOKEI AS K4");
            sql.Append("  ON  K1.KAISHA_CD = K4.KAISHA_CD");
            sql.Append("  AND K1.SHAIN_CD = K4.SHAIN_CD");
            sql.Append("  AND K1.NENGETSU = K4.NENGETSU");
            sql.Append(" WHERE K1.KAISHA_CD = @KAISHA_CD");
            sql.Append(" AND K1.SHAIN_CD = @SHAIN_CD");
            sql.Append(" AND K1.NENGETSU = @NENGETSU");
            return Util.ToString(sql);
        }

        /// <summary>
        /// 対象となる給与/賞与支給明細金額の集計
        /// </summary>
        /// <param name="type">1給与 2賞与</param>
        /// <param name="nendo">年度</param>
        /// <param name="shainCd">社員コード</param>
        /// <returns>給与課税額、給与社会保険料等、給与徴収税額</returns>
        private decimal[] GetMeisaiKingaku(int type, DateTime nendo, string shainCd)
        {
            decimal[] ret = new decimal[3];

            // 給与・賞与の別
            string kyuSho = (type == 1) ? "KYUYO" : "SHOYO";

            DbParamCollection dpc;
            StringBuilder sql;
            // 支給項目：課税項目情報
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 2, this.UInfo.KaishaCd);
            sql = new StringBuilder();
            sql.Append("SELECT KOMOKU_BANGO");
            sql.Append(" FROM TB_KY_" + kyuSho + "_SHIKYU_KOMOKU_STI");
            sql.Append(" WHERE KAISHA_CD = @KAISHA_CD");
            sql.Append(" AND KOMOKU_KUBUN1 = 1");
            DataTable dtKazeiSti = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
            // 控除項目：社会保険料情報
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 2, this.UInfo.KaishaCd);
            sql = new StringBuilder();
            sql.Append("SELECT KOMOKU_BANGO");
            sql.Append(" FROM TB_KY_" + kyuSho + "_KOJO_KOMOKU_SETTEI");
            sql.Append(" WHERE KAISHA_CD = @KAISHA_CD");
            sql.Append(" AND KOMOKU_KUBUN3 BETWEEN 31 AND 33");
            DataTable dtShakaiHokenryoSti = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
            // 控除項目：所得税情報
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 2, this.UInfo.KaishaCd);
            sql = new StringBuilder();
            sql.Append("SELECT KOMOKU_BANGO");
            sql.Append(" FROM TB_KY_" + kyuSho + "_KOJO_KOMOKU_SETTEI");
            sql.Append(" WHERE KAISHA_CD = @KAISHA_CD");
            sql.Append(" AND KOMOKU_KUBUN3 = 34");
            DataTable dtShotokuzeiSti = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);

            // 給与/賞与支給明細より集計
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 2, this.UInfo.KaishaCd);
            dpc.SetParam("@SHAIN_CD", SqlDbType.VarChar, 6, shainCd);
            dpc.SetParam("@NENGETSU_FR", SqlDbType.DateTime, nendo);
            dpc.SetParam("@NENGETSU_TO", SqlDbType.DateTime, nendo.AddMonths(11));
            sql = new StringBuilder();
            sql.Append("SELECT");
            sql.Append(" K2.SHAIN_CD AS SHAIN_CD");
            // 課税金額                             ～SUM( 0 + K2.KOMOKU? + K2.KOMOKU? ･･･) AS KAZEI_KINGAKU
            sql.Append(",SUM( 0");
            foreach (DataRow dr in dtKazeiSti.Rows)
            {
                sql.Append(" + ISNULL(K2.KOMOKU" + Util.ToString(dr["KOMOKU_BANGO"]) + ", 0)");
            }
            sql.Append(" ) AS KAZEI_KINGAKU");
            // 社会保険料                           ～SUM( 0 + K3.KOMOKU? + K3.KOMOKU? ･･･) AS SHAKAI_HOKENRYO
            sql.Append(",SUM( 0");
            foreach (DataRow dr in dtShakaiHokenryoSti.Rows)
            {
                sql.Append(" + ISNULL(K3.KOMOKU" + Util.ToString(dr["KOMOKU_BANGO"]) + ", 0)");
            }
            sql.Append(" ) AS SHAKAI_HOKENRYO");
            // 所得税                               ～SUM( 0 + K3.KOMOKU? + K3.KOMOKU? ･･･) AS SHOTOKUZEI_KINGAKU
            sql.Append(",SUM( 0");
            foreach (DataRow dr in dtShotokuzeiSti.Rows)
            {
                sql.Append(" + ISNULL(K3.KOMOKU" + Util.ToString(dr["KOMOKU_BANGO"]) + ", 0)");
            }
            sql.Append(" ) AS SHOTOKUZEI_KINGAKU");
            sql.Append(" FROM TB_KY_" + kyuSho + "_MEISAI_SHIKYU AS K2");
            sql.Append(" LEFT OUTER JOIN TB_KY_" + kyuSho + "_MEISAI_KOJO AS K3");
            sql.Append(" ON (K2.KAISHA_CD = K3.KAISHA_CD)");
            sql.Append(" AND (K2.SHAIN_CD = K3.SHAIN_CD)");
            sql.Append(" AND (K2.NENGETSU = K3.NENGETSU)");
            sql.Append(" WHERE K2.KAISHA_CD = @KAISHA_CD");
            sql.Append(" AND K2.SHAIN_CD = @SHAIN_CD");
            sql.Append(" AND K2.NENGETSU BETWEEN @NENGETSU_FR AND @NENGETSU_TO");
            sql.Append(" GROUP BY K2.SHAIN_CD");
            DataTable dt = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
            if (dt.Rows.Count > 0)
            {
                ret[0] = Util.ToDecimal(dt.Rows[0]["KAZEI_KINGAKU"]);
                ret[1] = Util.ToDecimal(dt.Rows[0]["SHAKAI_HOKENRYO"]);
                ret[2] = Util.ToDecimal(dt.Rows[0]["SHOTOKUZEI_KINGAKU"]);
            }
            return ret;
        }

        /// <summary>
        /// P184 配偶者/扶養/基礎/障害者等控除額の取得
        /// </summary>
        /// <param name="kubun1"></param>
        /// <param name="kubun2"></param>
        /// <returns></returns>
        private decimal GetKojogaku(int kubun1, int kubun2)
        {
            decimal ret = 0;

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KUBUN1", SqlDbType.Decimal, 2, kubun1);
            dpc.SetParam("@KUBUN2", SqlDbType.Decimal, 2, kubun2);
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT KOJOGAKU");
            sql.Append(" FROM TB_KY_F_NENGAKU_STK_KJGK_H");
            sql.Append(" WHERE KUBUN1 = @KUBUN1");
            sql.Append(" AND KUBUN2 = @KUBUN2");
            DataTable dt = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
            if (dt.Rows.Count > 0)
            {
                ret = Util.ToDecimal(dt.Rows[0]["KOJOGAKU"]);
            }

            return ret;
        }

        /// <summary>
        /// TB_KY_NENMATSU_CHOSEIに更新するためのパラメータ設定をします。
        /// </summary>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 更新処理：DbParamCollection*2(Where句,Set句)
        /// </returns>
        private ArrayList SetKyNenmatsuChoseiParams(
                DataRow drShainJoho                         // TB社員情報DataRow
                , DateTime nendo                            // 年度
                , DateTime nengetsu                         // 還付年月
                , decimal kyuyotoKazeiKingaku               // ■給与等課税金額 
                , decimal kyuyotoShotokuzeigaku             // ■給与等所得税額 
                , decimal shoyotoKazeiKingaku               // ■賞与等課税金額 
                , decimal shoyotoShotokuzeigaku             // ■賞与等所得税額 
                , decimal kyuyoShotokuKojogoNoKngk          // ■給与所得控除後の金額 
                , decimal shakaiHokenryoKjgkKyuyoto         // ■社会保険料控除額給与等
                , decimal haigushaKojogaku                  // ■配偶者控除額 
                , decimal fuyoKojogaku                      // ■扶養控除額 
                , decimal kisoKojogaku                      // ■基礎控除額 
                , decimal shogaishaKojogaku                 // ■障害者控除額 
                , decimal ronenshaKafuKojogaku              // ■老年者寡婦控除額 
                , decimal kinroGakuseiKojogaku              // ■勤労学生控除額 
                , decimal kazeiKyuyoShotokuKingaku          // ■課税給与所得金額 
                , decimal sanshutsuNenzeigaku               // ■算出年税額 
                , decimal nenchoNenzeigaku                  // ■TB年末調整.年調年税額(年調所得税額)
                , decimal nenzeigaku                        // ■年税額
            )
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            // WHERE句パラメータ
            DbParamCollection whereParam = new DbParamCollection();
            whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
            whereParam.SetParam("@NENDO", SqlDbType.DateTime, nendo);
            whereParam.SetParam("@SHAIN_CD", SqlDbType.Decimal, 6, Util.ToDecimal(drShainJoho["SHAIN_CD"]));

            alParams.Add(whereParam);

            // TB社員情報からの複写項目
            updParam.SetParam("@ZEIGAKU_HYO", SqlDbType.Decimal, 1, Util.ToDecimal(drShainJoho["ZEIGAKU_HYO"]));
            updParam.SetParam("@NENMATSU_CHOSEI", SqlDbType.Decimal, 1, Util.ToDecimal(drShainJoho["NENMATSU_CHOSEI"]));
            updParam.SetParam("@HONNIN_KUBUN1", SqlDbType.Decimal, 1, Util.ToDecimal(drShainJoho["HONNIN_KUBUN1"]));
            updParam.SetParam("@HONNIN_KUBUN2", SqlDbType.Decimal, 1, Util.ToDecimal(drShainJoho["HONNIN_KUBUN2"]));
            updParam.SetParam("@HONNIN_KUBUN3", SqlDbType.Decimal, 1, Util.ToDecimal(drShainJoho["HONNIN_KUBUN3"]));
            updParam.SetParam("@HAIGUSHA_KUBUN", SqlDbType.Decimal, 1, Util.ToDecimal(drShainJoho["HAIGUSHA_KUBUN"]));
            updParam.SetParam("@FUYO_IPPAN", SqlDbType.Decimal, 2, Util.ToDecimal(drShainJoho["FUYO_IPPAN"]));
            updParam.SetParam("@FUYO_TOKUTEI", SqlDbType.Decimal, 2, Util.ToDecimal(drShainJoho["FUYO_TOKUTEI"]));
            updParam.SetParam("@FUYO_ROZIN_DOKYO_ROSHINTO_IGAI", SqlDbType.Decimal, 2, Util.ToDecimal(drShainJoho["FUYO_ROJIN_DOKYO_ROSHINTO_IGAI"]));
            updParam.SetParam("@FUYO_ROZIN_DOKYO_ROSHINTO", SqlDbType.Decimal, 2, Util.ToDecimal(drShainJoho["FUYO_ROJIN_DOKYO_ROSHINTO"]));
            updParam.SetParam("@FUYO_DOKYO_TOKUSHO_IPPAN", SqlDbType.Decimal, 2, Util.ToDecimal(drShainJoho["FUYO_DOKYO_TOKUSHO_IPPAN"]));
            updParam.SetParam("@SHOGAISHA_IPPAN", SqlDbType.Decimal, 2, Util.ToDecimal(drShainJoho["SHOGAISHA_IPPAN"]));
            updParam.SetParam("@SHOGAISHA_TOKUBETSU", SqlDbType.Decimal, 2, Util.ToDecimal(drShainJoho["SHOGAISHA_TOKUBETSU"]));
            updParam.SetParam("@MISEINENSHA", SqlDbType.Decimal, 2, Util.ToDecimal(drShainJoho["MISEINENSHA"]));
            updParam.SetParam("@SHIBO_TAISHOKU", SqlDbType.Decimal, 2, Util.ToDecimal(drShainJoho["SHIBO_TAISHOKU"]));
            updParam.SetParam("@SAIGAISHA", SqlDbType.Decimal, 2, Util.ToDecimal(drShainJoho["SAIGAISHA"]));
            updParam.SetParam("@GAIKOKUJIN", SqlDbType.Decimal, 2, Util.ToDecimal(drShainJoho["GAIKOKUJIN"]));
            updParam.SetParam("@TEKIYO1", SqlDbType.VarChar, 50, Util.ToString(drShainJoho["TEKIYO1"]));
            updParam.SetParam("@TEKIYO2", SqlDbType.VarChar, 50, Util.ToString(drShainJoho["TEKIYO2"]));
            updParam.SetParam("@SAI16_MIMAN", SqlDbType.Decimal, 2, Util.ToDecimal(drShainJoho["SAI16_MIMAN"]));

            // 算出項目
            updParam.SetParam("@KYUYOTO_KAZEI_KINGAKU", SqlDbType.Decimal, 9, 0, kyuyotoKazeiKingaku);
            updParam.SetParam("@KYUYOTO_SHOTOKUZEIGAKU", SqlDbType.Decimal, 9, 0, kyuyotoShotokuzeigaku);
            updParam.SetParam("@SHOYOTO_KAZEI_KINGAKU", SqlDbType.Decimal, 9, 0, shoyotoKazeiKingaku);
            updParam.SetParam("@SHOYOTO_SHOTOKUZEIGAKU", SqlDbType.Decimal, 9, 0, shoyotoShotokuzeigaku);
            updParam.SetParam("@KYUYO_SHOTOKU_KOJOGO_NO_KNGK", SqlDbType.Decimal, 9, 0, kyuyoShotokuKojogoNoKngk);
            updParam.SetParam("@SHAKAI_HOKENRYO_KJGK_KYUYOTO", SqlDbType.Decimal, 9, 0, shakaiHokenryoKjgkKyuyoto);
            updParam.SetParam("@HAIGUSHA_KOJOGAKU", SqlDbType.Decimal, 9, 0, haigushaKojogaku);
            updParam.SetParam("@FUYO_KOJOGAKU", SqlDbType.Decimal, 9, 0, fuyoKojogaku);
            updParam.SetParam("@KISO_KOJOGAKU ", SqlDbType.Decimal, 9, 0, kisoKojogaku);
            updParam.SetParam("@SHOGAISHA_KOJOGAKU", SqlDbType.Decimal, 9, 0, shogaishaKojogaku);
            updParam.SetParam("@RONENSHA_KAFU_KOJOGAKU", SqlDbType.Decimal, 9, 0, ronenshaKafuKojogaku);
            updParam.SetParam("@KINRO_GAKUSEI_KOJOGAKU", SqlDbType.Decimal, 9, 0, kinroGakuseiKojogaku);
            updParam.SetParam("@KAZEI_KYUYO_SHOTOKU_KINGAKU", SqlDbType.Decimal, 9, 0, kazeiKyuyoShotokuKingaku);
            updParam.SetParam("@SANSHUTSU_NENZEIGAKU", SqlDbType.Decimal, 9, 0, sanshutsuNenzeigaku);
            updParam.SetParam("@NENCHO_NENZEIGAKU", SqlDbType.Decimal, 9, 0, nenchoNenzeigaku);
            updParam.SetParam("@NENZEIGAKU", SqlDbType.Decimal, 9, 0, nenzeigaku);

            // 管理情報項目
            if (this.rdoChosei1.Checked)
            {
                updParam.SetParam("@KYUYO_KANPU_DATE", SqlDbType.DateTime, nengetsu);
            }
            else
            {
                updParam.SetParam("@KYUYO_KANPU_DATE", SqlDbType.DateTime, DBNull.Value);
            }
            updParam.SetParam("@KEISAN_F", SqlDbType.Decimal, 1, 1);
            updParam.SetParam("@CHOSEI_F", SqlDbType.Decimal, 1, this.rdoChosei1.Checked ? 1 : this.rdoChosei2.Checked ? 2 : 3);
            updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWDATE");

            alParams.Add(updParam);

            return alParams;
        }

        /// <summary>
        /// 処理中メッセージを表示
        /// </summary>
        /// <param name="message">表示メッセージ ※IsEmptyの時表示終了</param>
        private void DispMessage(string message)
        {
            if (ValChk.IsEmpty(message))
            {
                // メッセージ終了
                this.pnlProcessingMessage.Visible = false;
            }
            else
            {
                this.pnlProcessingMessage.Visible = true;
                this.lblProcessingMessage.Text = message;
                this.Refresh();
            }
        }

        #endregion

    }
}