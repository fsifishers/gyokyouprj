﻿namespace jp.co.fsi.ky.kynb1011
{
    partial class KYNB1011
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpNengetsu = new System.Windows.Forms.GroupBox();
            this.lblMonthNengetsu = new System.Windows.Forms.Label();
            this.lblYearNengetsu = new System.Windows.Forms.Label();
            this.txtMonthNengetsu = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGengoNengetsu = new System.Windows.Forms.Label();
            this.txtGengoYearNengetsu = new jp.co.fsi.common.controls.FsiTextBox();
            this.grpTaisho = new System.Windows.Forms.GroupBox();
            this.lblYearTaisho = new System.Windows.Forms.Label();
            this.lblGengoNendo = new System.Windows.Forms.Label();
            this.txtGengoYearNendo = new jp.co.fsi.common.controls.FsiTextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.grpBukaCd = new System.Windows.Forms.GroupBox();
            this.lblBukaCdTo = new System.Windows.Forms.Label();
            this.txtBukaCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.lblBukaCdFr = new System.Windows.Forms.Label();
            this.txtBukaCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.grpBumonCd = new System.Windows.Forms.GroupBox();
            this.lblBumonCdTo = new System.Windows.Forms.Label();
            this.txtBumonCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblBumonCdFr = new System.Windows.Forms.Label();
            this.txtBumonCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.grpShainCd = new System.Windows.Forms.GroupBox();
            this.lblShainCdTo = new System.Windows.Forms.Label();
            this.txtShainCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblCodeBet = new System.Windows.Forms.Label();
            this.lblShainCdFr = new System.Windows.Forms.Label();
            this.txtShainCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.grpChosei = new System.Windows.Forms.GroupBox();
            this.rdoChosei3 = new System.Windows.Forms.RadioButton();
            this.rdoChosei2 = new System.Windows.Forms.RadioButton();
            this.rdoChosei1 = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.pnlProcessingMessage = new jp.co.fsi.common.FsiPanel();
            this.lblProcessingMessage = new System.Windows.Forms.Label();
            this.lblMizuageShisho = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.grpNengetsu.SuspendLayout();
            this.grpTaisho.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.grpBukaCd.SuspendLayout();
            this.grpBumonCd.SuspendLayout();
            this.grpShainCd.SuspendLayout();
            this.grpChosei.SuspendLayout();
            this.pnlProcessingMessage.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Text = "年末調整処理";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            // 
            // grpNengetsu
            // 
            this.grpNengetsu.Controls.Add(this.lblMonthNengetsu);
            this.grpNengetsu.Controls.Add(this.lblYearNengetsu);
            this.grpNengetsu.Controls.Add(this.txtMonthNengetsu);
            this.grpNengetsu.Controls.Add(this.lblGengoNengetsu);
            this.grpNengetsu.Controls.Add(this.txtGengoYearNengetsu);
            this.grpNengetsu.Controls.Add(this.label1);
            this.grpNengetsu.Font = new System.Drawing.Font("ＭＳ ゴシック", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.grpNengetsu.Location = new System.Drawing.Point(12, 123);
            this.grpNengetsu.Name = "grpNengetsu";
            this.grpNengetsu.Size = new System.Drawing.Size(222, 60);
            this.grpNengetsu.TabIndex = 1;
            this.grpNengetsu.TabStop = false;
            this.grpNengetsu.Text = "還付年月";
            // 
            // lblMonthNengetsu
            // 
            this.lblMonthNengetsu.BackColor = System.Drawing.Color.Silver;
            this.lblMonthNengetsu.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMonthNengetsu.Location = new System.Drawing.Point(172, 22);
            this.lblMonthNengetsu.Name = "lblMonthNengetsu";
            this.lblMonthNengetsu.Size = new System.Drawing.Size(24, 16);
            this.lblMonthNengetsu.TabIndex = 4;
            this.lblMonthNengetsu.Text = "月";
            // 
            // lblYearNengetsu
            // 
            this.lblYearNengetsu.BackColor = System.Drawing.Color.Silver;
            this.lblYearNengetsu.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYearNengetsu.Location = new System.Drawing.Point(107, 22);
            this.lblYearNengetsu.Name = "lblYearNengetsu";
            this.lblYearNengetsu.Size = new System.Drawing.Size(24, 16);
            this.lblYearNengetsu.TabIndex = 2;
            this.lblYearNengetsu.Text = "年";
            // 
            // txtMonthNengetsu
            // 
            this.txtMonthNengetsu.AutoSizeFromLength = false;
            this.txtMonthNengetsu.DisplayLength = null;
            this.txtMonthNengetsu.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMonthNengetsu.Location = new System.Drawing.Point(132, 19);
            this.txtMonthNengetsu.MaxLength = 2;
            this.txtMonthNengetsu.Name = "txtMonthNengetsu";
            this.txtMonthNengetsu.Size = new System.Drawing.Size(34, 23);
            this.txtMonthNengetsu.TabIndex = 3;
            this.txtMonthNengetsu.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonthNengetsu.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthNengetsu_Validating);
            // 
            // lblGengoNengetsu
            // 
            this.lblGengoNengetsu.BackColor = System.Drawing.Color.Silver;
            this.lblGengoNengetsu.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengoNengetsu.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengoNengetsu.Location = new System.Drawing.Point(24, 20);
            this.lblGengoNengetsu.Name = "lblGengoNengetsu";
            this.lblGengoNengetsu.Size = new System.Drawing.Size(42, 21);
            this.lblGengoNengetsu.TabIndex = 0;
            this.lblGengoNengetsu.Text = "平成";
            this.lblGengoNengetsu.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtGengoYearNengetsu
            // 
            this.txtGengoYearNengetsu.AutoSizeFromLength = false;
            this.txtGengoYearNengetsu.DisplayLength = null;
            this.txtGengoYearNengetsu.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGengoYearNengetsu.Location = new System.Drawing.Point(67, 19);
            this.txtGengoYearNengetsu.MaxLength = 2;
            this.txtGengoYearNengetsu.Name = "txtGengoYearNengetsu";
            this.txtGengoYearNengetsu.Size = new System.Drawing.Size(34, 23);
            this.txtGengoYearNengetsu.TabIndex = 1;
            this.txtGengoYearNengetsu.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGengoYearNengetsu.Validating += new System.ComponentModel.CancelEventHandler(this.txtGengoYearNengetsu_Validating);
            // 
            // grpTaisho
            // 
            this.grpTaisho.Controls.Add(this.lblYearTaisho);
            this.grpTaisho.Controls.Add(this.lblGengoNendo);
            this.grpTaisho.Controls.Add(this.txtGengoYearNendo);
            this.grpTaisho.Controls.Add(this.lblMizuageShisho);
            this.grpTaisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.grpTaisho.Location = new System.Drawing.Point(12, 52);
            this.grpTaisho.Name = "grpTaisho";
            this.grpTaisho.Size = new System.Drawing.Size(222, 60);
            this.grpTaisho.TabIndex = 0;
            this.grpTaisho.TabStop = false;
            this.grpTaisho.Text = "年度";
            // 
            // lblYearTaisho
            // 
            this.lblYearTaisho.BackColor = System.Drawing.Color.Silver;
            this.lblYearTaisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYearTaisho.Location = new System.Drawing.Point(103, 24);
            this.lblYearTaisho.Name = "lblYearTaisho";
            this.lblYearTaisho.Size = new System.Drawing.Size(40, 19);
            this.lblYearTaisho.TabIndex = 15;
            this.lblYearTaisho.Text = "年度";
            // 
            // lblGengoNendo
            // 
            this.lblGengoNendo.BackColor = System.Drawing.Color.Silver;
            this.lblGengoNendo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengoNendo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengoNendo.Location = new System.Drawing.Point(23, 21);
            this.lblGengoNendo.Name = "lblGengoNendo";
            this.lblGengoNendo.Size = new System.Drawing.Size(42, 23);
            this.lblGengoNendo.TabIndex = 13;
            this.lblGengoNendo.Text = "平成";
            this.lblGengoNendo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtGengoYearNendo
            // 
            this.txtGengoYearNendo.AutoSizeFromLength = false;
            this.txtGengoYearNendo.DisplayLength = null;
            this.txtGengoYearNendo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGengoYearNendo.Location = new System.Drawing.Point(65, 22);
            this.txtGengoYearNendo.MaxLength = 2;
            this.txtGengoYearNendo.Name = "txtGengoYearNendo";
            this.txtGengoYearNendo.Size = new System.Drawing.Size(34, 23);
            this.txtGengoYearNendo.TabIndex = 14;
            this.txtGengoYearNendo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGengoYearNendo.Validating += new System.ComponentModel.CancelEventHandler(this.txtGengoYearNendo_Validating);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.grpBukaCd);
            this.groupBox1.Controls.Add(this.grpBumonCd);
            this.groupBox1.Controls.Add(this.grpShainCd);
            this.groupBox1.Font = new System.Drawing.Font("ＭＳ ゴシック", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBox1.Location = new System.Drawing.Point(12, 192);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(590, 206);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "範囲指定";
            // 
            // grpBukaCd
            // 
            this.grpBukaCd.Controls.Add(this.lblBukaCdTo);
            this.grpBukaCd.Controls.Add(this.txtBukaCdTo);
            this.grpBukaCd.Controls.Add(this.label5);
            this.grpBukaCd.Controls.Add(this.lblBukaCdFr);
            this.grpBukaCd.Controls.Add(this.txtBukaCdFr);
            this.grpBukaCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.grpBukaCd.Location = new System.Drawing.Point(5, 138);
            this.grpBukaCd.Name = "grpBukaCd";
            this.grpBukaCd.Size = new System.Drawing.Size(575, 53);
            this.grpBukaCd.TabIndex = 5;
            this.grpBukaCd.TabStop = false;
            this.grpBukaCd.Text = "部課コード";
            // 
            // lblBukaCdTo
            // 
            this.lblBukaCdTo.BackColor = System.Drawing.Color.Silver;
            this.lblBukaCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBukaCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBukaCdTo.Location = new System.Drawing.Point(365, 20);
            this.lblBukaCdTo.Name = "lblBukaCdTo";
            this.lblBukaCdTo.Size = new System.Drawing.Size(194, 20);
            this.lblBukaCdTo.TabIndex = 4;
            this.lblBukaCdTo.Text = "最　後";
            this.lblBukaCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtBukaCdTo
            // 
            this.txtBukaCdTo.AutoSizeFromLength = false;
            this.txtBukaCdTo.DisplayLength = null;
            this.txtBukaCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtBukaCdTo.Location = new System.Drawing.Point(303, 19);
            this.txtBukaCdTo.MaxLength = 4;
            this.txtBukaCdTo.Name = "txtBukaCdTo";
            this.txtBukaCdTo.Size = new System.Drawing.Size(55, 23);
            this.txtBukaCdTo.TabIndex = 3;
            this.txtBukaCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBukaCdTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LastControl_KeyDown);
            this.txtBukaCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtBukaCdTo_Validating);
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label5.Location = new System.Drawing.Point(276, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(18, 20);
            this.label5.TabIndex = 2;
            this.label5.Text = "～";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblBukaCdFr
            // 
            this.lblBukaCdFr.BackColor = System.Drawing.Color.Silver;
            this.lblBukaCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBukaCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBukaCdFr.Location = new System.Drawing.Point(76, 20);
            this.lblBukaCdFr.Name = "lblBukaCdFr";
            this.lblBukaCdFr.Size = new System.Drawing.Size(194, 20);
            this.lblBukaCdFr.TabIndex = 1;
            this.lblBukaCdFr.Text = "先　頭";
            this.lblBukaCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtBukaCdFr
            // 
            this.txtBukaCdFr.AutoSizeFromLength = false;
            this.txtBukaCdFr.DisplayLength = null;
            this.txtBukaCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtBukaCdFr.Location = new System.Drawing.Point(15, 19);
            this.txtBukaCdFr.MaxLength = 4;
            this.txtBukaCdFr.Name = "txtBukaCdFr";
            this.txtBukaCdFr.Size = new System.Drawing.Size(55, 23);
            this.txtBukaCdFr.TabIndex = 0;
            this.txtBukaCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBukaCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtBukaCdFr_Validating);
            // 
            // grpBumonCd
            // 
            this.grpBumonCd.Controls.Add(this.lblBumonCdTo);
            this.grpBumonCd.Controls.Add(this.txtBumonCdTo);
            this.grpBumonCd.Controls.Add(this.label2);
            this.grpBumonCd.Controls.Add(this.lblBumonCdFr);
            this.grpBumonCd.Controls.Add(this.txtBumonCdFr);
            this.grpBumonCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.grpBumonCd.Location = new System.Drawing.Point(6, 79);
            this.grpBumonCd.Name = "grpBumonCd";
            this.grpBumonCd.Size = new System.Drawing.Size(575, 53);
            this.grpBumonCd.TabIndex = 4;
            this.grpBumonCd.TabStop = false;
            this.grpBumonCd.Text = "部門コード";
            // 
            // lblBumonCdTo
            // 
            this.lblBumonCdTo.BackColor = System.Drawing.Color.Silver;
            this.lblBumonCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBumonCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBumonCdTo.Location = new System.Drawing.Point(365, 20);
            this.lblBumonCdTo.Name = "lblBumonCdTo";
            this.lblBumonCdTo.Size = new System.Drawing.Size(194, 20);
            this.lblBumonCdTo.TabIndex = 4;
            this.lblBumonCdTo.Text = "最　後";
            this.lblBumonCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtBumonCdTo
            // 
            this.txtBumonCdTo.AutoSizeFromLength = false;
            this.txtBumonCdTo.DisplayLength = null;
            this.txtBumonCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtBumonCdTo.Location = new System.Drawing.Point(303, 19);
            this.txtBumonCdTo.MaxLength = 4;
            this.txtBumonCdTo.Name = "txtBumonCdTo";
            this.txtBumonCdTo.Size = new System.Drawing.Size(55, 23);
            this.txtBumonCdTo.TabIndex = 3;
            this.txtBumonCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBumonCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtBumonCdTo_Validating);
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(276, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(18, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "～";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblBumonCdFr
            // 
            this.lblBumonCdFr.BackColor = System.Drawing.Color.Silver;
            this.lblBumonCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBumonCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBumonCdFr.Location = new System.Drawing.Point(76, 20);
            this.lblBumonCdFr.Name = "lblBumonCdFr";
            this.lblBumonCdFr.Size = new System.Drawing.Size(194, 20);
            this.lblBumonCdFr.TabIndex = 1;
            this.lblBumonCdFr.Text = "先　頭";
            this.lblBumonCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtBumonCdFr
            // 
            this.txtBumonCdFr.AutoSizeFromLength = false;
            this.txtBumonCdFr.DisplayLength = null;
            this.txtBumonCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtBumonCdFr.Location = new System.Drawing.Point(15, 19);
            this.txtBumonCdFr.MaxLength = 4;
            this.txtBumonCdFr.Name = "txtBumonCdFr";
            this.txtBumonCdFr.Size = new System.Drawing.Size(55, 23);
            this.txtBumonCdFr.TabIndex = 0;
            this.txtBumonCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBumonCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtBumonCdFr_Validating);
            // 
            // grpShainCd
            // 
            this.grpShainCd.Controls.Add(this.lblShainCdTo);
            this.grpShainCd.Controls.Add(this.txtShainCdTo);
            this.grpShainCd.Controls.Add(this.lblCodeBet);
            this.grpShainCd.Controls.Add(this.lblShainCdFr);
            this.grpShainCd.Controls.Add(this.txtShainCdFr);
            this.grpShainCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.grpShainCd.Location = new System.Drawing.Point(6, 20);
            this.grpShainCd.Name = "grpShainCd";
            this.grpShainCd.Size = new System.Drawing.Size(575, 53);
            this.grpShainCd.TabIndex = 3;
            this.grpShainCd.TabStop = false;
            this.grpShainCd.Text = "社員コード";
            // 
            // lblShainCdTo
            // 
            this.lblShainCdTo.BackColor = System.Drawing.Color.Silver;
            this.lblShainCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShainCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShainCdTo.Location = new System.Drawing.Point(365, 20);
            this.lblShainCdTo.Name = "lblShainCdTo";
            this.lblShainCdTo.Size = new System.Drawing.Size(194, 20);
            this.lblShainCdTo.TabIndex = 4;
            this.lblShainCdTo.Text = "最　後";
            this.lblShainCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShainCdTo
            // 
            this.txtShainCdTo.AutoSizeFromLength = false;
            this.txtShainCdTo.DisplayLength = null;
            this.txtShainCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShainCdTo.Location = new System.Drawing.Point(303, 19);
            this.txtShainCdTo.MaxLength = 4;
            this.txtShainCdTo.Name = "txtShainCdTo";
            this.txtShainCdTo.Size = new System.Drawing.Size(55, 23);
            this.txtShainCdTo.TabIndex = 3;
            this.txtShainCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShainCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtShainCdTo_Validating);
            // 
            // lblCodeBet
            // 
            this.lblCodeBet.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCodeBet.Location = new System.Drawing.Point(276, 20);
            this.lblCodeBet.Name = "lblCodeBet";
            this.lblCodeBet.Size = new System.Drawing.Size(18, 20);
            this.lblCodeBet.TabIndex = 2;
            this.lblCodeBet.Text = "～";
            this.lblCodeBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShainCdFr
            // 
            this.lblShainCdFr.BackColor = System.Drawing.Color.Silver;
            this.lblShainCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShainCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShainCdFr.Location = new System.Drawing.Point(76, 20);
            this.lblShainCdFr.Name = "lblShainCdFr";
            this.lblShainCdFr.Size = new System.Drawing.Size(194, 20);
            this.lblShainCdFr.TabIndex = 1;
            this.lblShainCdFr.Text = "先　頭";
            this.lblShainCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShainCdFr
            // 
            this.txtShainCdFr.AutoSizeFromLength = false;
            this.txtShainCdFr.DisplayLength = null;
            this.txtShainCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShainCdFr.Location = new System.Drawing.Point(15, 19);
            this.txtShainCdFr.MaxLength = 4;
            this.txtShainCdFr.Name = "txtShainCdFr";
            this.txtShainCdFr.Size = new System.Drawing.Size(55, 23);
            this.txtShainCdFr.TabIndex = 0;
            this.txtShainCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShainCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtShainCdFr_Validating);
            // 
            // grpChosei
            // 
            this.grpChosei.Controls.Add(this.rdoChosei3);
            this.grpChosei.Controls.Add(this.rdoChosei2);
            this.grpChosei.Controls.Add(this.rdoChosei1);
            this.grpChosei.Font = new System.Drawing.Font("ＭＳ ゴシック", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.grpChosei.Location = new System.Drawing.Point(344, 57);
            this.grpChosei.Name = "grpChosei";
            this.grpChosei.Size = new System.Drawing.Size(258, 53);
            this.grpChosei.TabIndex = 2;
            this.grpChosei.TabStop = false;
            this.grpChosei.Text = "調整方法";
            // 
            // rdoChosei3
            // 
            this.rdoChosei3.AutoSize = true;
            this.rdoChosei3.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoChosei3.Location = new System.Drawing.Point(188, 20);
            this.rdoChosei3.Name = "rdoChosei3";
            this.rdoChosei3.Size = new System.Drawing.Size(58, 20);
            this.rdoChosei3.TabIndex = 2;
            this.rdoChosei3.Text = "単独";
            this.rdoChosei3.UseVisualStyleBackColor = true;
            // 
            // rdoChosei2
            // 
            this.rdoChosei2.AutoSize = true;
            this.rdoChosei2.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoChosei2.Location = new System.Drawing.Point(94, 20);
            this.rdoChosei2.Name = "rdoChosei2";
            this.rdoChosei2.Size = new System.Drawing.Size(58, 20);
            this.rdoChosei2.TabIndex = 1;
            this.rdoChosei2.Text = "賞与";
            this.rdoChosei2.UseVisualStyleBackColor = true;
            // 
            // rdoChosei1
            // 
            this.rdoChosei1.AutoSize = true;
            this.rdoChosei1.Checked = true;
            this.rdoChosei1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoChosei1.Location = new System.Drawing.Point(9, 20);
            this.rdoChosei1.Name = "rdoChosei1";
            this.rdoChosei1.Size = new System.Drawing.Size(58, 20);
            this.rdoChosei1.TabIndex = 0;
            this.rdoChosei1.TabStop = true;
            this.rdoChosei1.Text = "給与";
            this.rdoChosei1.UseVisualStyleBackColor = true;
            this.rdoChosei1.CheckedChanged += new System.EventHandler(this.rdoChosei_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(342, 117);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(245, 13);
            this.label3.TabIndex = 903;
            this.label3.Text = "(給与調整)還付年月給与支給分で処理";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(412, 138);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(203, 13);
            this.label4.TabIndex = 904;
            this.label4.Text = "※還付年月の明細データが必要";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label6.Location = new System.Drawing.Point(342, 159);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(217, 13);
            this.label6.TabIndex = 905;
            this.label6.Text = "(賞与調整)12月賞与支給分で処理";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label7.Location = new System.Drawing.Point(342, 180);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(203, 13);
            this.label7.TabIndex = 906;
            this.label7.Text = "(単独調整)年末調整データのみ";
            // 
            // pnlProcessingMessage
            // 
            this.pnlProcessingMessage.BackColor = System.Drawing.Color.Silver;
            this.pnlProcessingMessage.Controls.Add(this.lblProcessingMessage);
            this.pnlProcessingMessage.Location = new System.Drawing.Point(264, 180);
            this.pnlProcessingMessage.Name = "pnlProcessingMessage";
            this.pnlProcessingMessage.Size = new System.Drawing.Size(258, 100);
            this.pnlProcessingMessage.TabIndex = 907;
            this.pnlProcessingMessage.Visible = false;
            // 
            // lblProcessingMessage
            // 
            this.lblProcessingMessage.AutoSize = true;
            this.lblProcessingMessage.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblProcessingMessage.Location = new System.Drawing.Point(17, 44);
            this.lblProcessingMessage.Name = "lblProcessingMessage";
            this.lblProcessingMessage.Size = new System.Drawing.Size(49, 13);
            this.lblProcessingMessage.TabIndex = 906;
            this.lblProcessingMessage.Text = "処理中";
            // 
            // lblMizuageShisho
            // 
            this.lblMizuageShisho.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblMizuageShisho.Location = new System.Drawing.Point(20, 19);
            this.lblMizuageShisho.Name = "lblMizuageShisho";
            this.lblMizuageShisho.Size = new System.Drawing.Size(128, 29);
            this.lblMizuageShisho.TabIndex = 16;
            this.lblMizuageShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Silver;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.label1.Location = new System.Drawing.Point(21, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(180, 28);
            this.label1.TabIndex = 8;
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // KYNB1011
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.pnlProcessingMessage);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.grpChosei);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.grpTaisho);
            this.Controls.Add(this.grpNengetsu);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "KYNB1011";
            this.Text = "年末調整処理";
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.grpNengetsu, 0);
            this.Controls.SetChildIndex(this.grpTaisho, 0);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.Controls.SetChildIndex(this.grpChosei, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.Controls.SetChildIndex(this.label6, 0);
            this.Controls.SetChildIndex(this.label7, 0);
            this.Controls.SetChildIndex(this.pnlProcessingMessage, 0);
            this.pnlDebug.ResumeLayout(false);
            this.grpNengetsu.ResumeLayout(false);
            this.grpNengetsu.PerformLayout();
            this.grpTaisho.ResumeLayout(false);
            this.grpTaisho.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.grpBukaCd.ResumeLayout(false);
            this.grpBukaCd.PerformLayout();
            this.grpBumonCd.ResumeLayout(false);
            this.grpBumonCd.PerformLayout();
            this.grpShainCd.ResumeLayout(false);
            this.grpShainCd.PerformLayout();
            this.grpChosei.ResumeLayout(false);
            this.grpChosei.PerformLayout();
            this.pnlProcessingMessage.ResumeLayout(false);
            this.pnlProcessingMessage.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox grpNengetsu;
        private System.Windows.Forms.Label lblMonthNengetsu;
        private System.Windows.Forms.Label lblYearNengetsu;
        private jp.co.fsi.common.controls.FsiTextBox txtMonthNengetsu;
        private System.Windows.Forms.Label lblGengoNengetsu;
        private jp.co.fsi.common.controls.FsiTextBox txtGengoYearNengetsu;
        private System.Windows.Forms.GroupBox grpTaisho;
        private System.Windows.Forms.Label lblYearTaisho;
        private System.Windows.Forms.Label lblGengoNendo;
        private common.controls.FsiTextBox txtGengoYearNendo;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox grpChosei;
        private System.Windows.Forms.RadioButton rdoChosei2;
        private System.Windows.Forms.RadioButton rdoChosei1;
        private System.Windows.Forms.GroupBox grpBukaCd;
        private System.Windows.Forms.Label lblBukaCdTo;
        private common.controls.FsiTextBox txtBukaCdTo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblBukaCdFr;
        private common.controls.FsiTextBox txtBukaCdFr;
        private System.Windows.Forms.GroupBox grpBumonCd;
        private System.Windows.Forms.Label lblBumonCdTo;
        private common.controls.FsiTextBox txtBumonCdTo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblBumonCdFr;
        private common.controls.FsiTextBox txtBumonCdFr;
        private System.Windows.Forms.GroupBox grpShainCd;
        private System.Windows.Forms.Label lblShainCdTo;
        private common.controls.FsiTextBox txtShainCdTo;
        private System.Windows.Forms.Label lblCodeBet;
        private System.Windows.Forms.Label lblShainCdFr;
        private common.controls.FsiTextBox txtShainCdFr;
        private System.Windows.Forms.RadioButton rdoChosei3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private jp.co.fsi.common.FsiPanel pnlProcessingMessage;
        private System.Windows.Forms.Label lblProcessingMessage;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblMizuageShisho;
    }
}