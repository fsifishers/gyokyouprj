﻿namespace jp.co.fsi.ky.kynr1031
{
    /// <summary>
    /// KYNR1031R_3 の帳票
    /// </summary>
    partial class KYNR1031R_3
    {
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(KYNR1031R_3));
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.textBox395 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox396 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox397 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox398 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox399 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox400 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox401 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox402 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox403 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox404 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox405 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox406 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox407 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox408 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox409 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox410 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox411 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox412 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox413 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox414 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox415 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox416 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox417 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox418 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox419 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox420 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox421 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox422 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox423 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox424 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox425 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox426 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox427 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox428 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox429 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox430 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox431 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox432 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox433 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox434 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox435 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox436 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox437 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox438 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox439 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox440 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox441 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox442 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox443 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox444 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox445 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox446 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox447 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox448 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox449 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox450 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox451 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox452 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox453 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox454 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox455 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox456 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox457 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox458 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox459 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox460 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox461 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox462 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox463 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox464 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox465 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox466 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox467 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox468 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox469 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox470 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox471 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox472 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox473 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox474 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox475 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox476 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox477 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox478 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox479 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox480 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox481 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox482 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox483 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            ((System.ComponentModel.ISupportInitialize)(this.textBox395)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox396)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox397)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox398)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox399)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox400)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox401)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox402)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox403)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox404)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox405)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox406)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox407)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox408)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox409)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox410)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox411)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox412)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox413)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox414)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox415)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox416)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox417)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox418)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox419)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox420)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox421)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox422)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox423)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox424)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox425)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox426)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox427)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox428)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox429)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox430)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox431)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox432)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox433)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox434)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox435)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox436)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox437)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox438)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox439)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox440)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox441)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox442)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox443)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox444)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox445)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox446)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox447)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox448)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox449)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox450)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox451)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox452)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox453)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox454)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox455)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox456)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox457)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox458)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox459)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox460)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox461)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox462)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox463)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox464)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox465)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox466)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox467)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox468)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox469)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox470)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox471)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox472)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox473)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox474)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox475)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox476)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox477)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox478)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox479)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox480)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox481)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox482)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox483)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.textBox395,
            this.textBox396,
            this.textBox397,
            this.textBox398,
            this.textBox399,
            this.textBox400,
            this.textBox401,
            this.textBox402,
            this.textBox403,
            this.textBox404,
            this.textBox405,
            this.textBox406,
            this.textBox407,
            this.textBox408,
            this.textBox409,
            this.textBox410,
            this.textBox411,
            this.textBox412,
            this.textBox413,
            this.textBox414,
            this.textBox415,
            this.textBox416,
            this.textBox417,
            this.textBox418,
            this.textBox419,
            this.textBox420,
            this.textBox421,
            this.textBox422,
            this.textBox423,
            this.textBox424,
            this.textBox425,
            this.textBox426,
            this.textBox427,
            this.textBox428,
            this.textBox429,
            this.textBox430,
            this.textBox431,
            this.textBox432,
            this.textBox433,
            this.textBox434,
            this.textBox435,
            this.textBox436,
            this.textBox437,
            this.textBox438,
            this.textBox439,
            this.textBox440,
            this.textBox441,
            this.textBox442,
            this.textBox443,
            this.textBox444,
            this.label21,
            this.textBox445,
            this.textBox446,
            this.textBox447,
            this.textBox448,
            this.textBox449,
            this.textBox450,
            this.textBox451,
            this.textBox452,
            this.textBox453,
            this.textBox454,
            this.textBox455,
            this.textBox456,
            this.textBox457,
            this.textBox458,
            this.textBox459,
            this.textBox460,
            this.textBox461,
            this.textBox462,
            this.textBox463,
            this.textBox464,
            this.label22,
            this.textBox465,
            this.textBox466,
            this.textBox467,
            this.textBox468,
            this.textBox469,
            this.textBox470,
            this.textBox471,
            this.textBox472,
            this.textBox473,
            this.textBox474,
            this.textBox475,
            this.textBox476,
            this.textBox477,
            this.textBox478,
            this.textBox479,
            this.textBox480,
            this.textBox481,
            this.textBox482,
            this.textBox483,
            this.label1,
            this.label2});
            this.detail.Height = 1.256232F;
            this.detail.Name = "detail";
            // 
            // textBox395
            // 
            this.textBox395.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox395.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox395.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox395.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox395.CanGrow = false;
            this.textBox395.DataField = "ITEM121";
            this.textBox395.Height = 0.1458333F;
            this.textBox395.Left = 0.9854333F;
            this.textBox395.Name = "textBox395";
            this.textBox395.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox395.Tag = "";
            this.textBox395.Text = "ITEM121";
            this.textBox395.Top = 0.291732F;
            this.textBox395.Width = 0.7875F;
            // 
            // textBox396
            // 
            this.textBox396.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox396.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox396.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox396.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox396.CanGrow = false;
            this.textBox396.DataField = "ITEM122";
            this.textBox396.Height = 0.1458333F;
            this.textBox396.Left = 1.772931F;
            this.textBox396.Name = "textBox396";
            this.textBox396.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox396.Tag = "";
            this.textBox396.Text = "ITEM122";
            this.textBox396.Top = 0.291732F;
            this.textBox396.Width = 0.7875F;
            // 
            // textBox397
            // 
            this.textBox397.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox397.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox397.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox397.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox397.CanGrow = false;
            this.textBox397.DataField = "ITEM123";
            this.textBox397.Height = 0.1458333F;
            this.textBox397.Left = 2.560432F;
            this.textBox397.Name = "textBox397";
            this.textBox397.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox397.Tag = "";
            this.textBox397.Text = "ITEM123";
            this.textBox397.Top = 0.291732F;
            this.textBox397.Width = 0.7875F;
            // 
            // textBox398
            // 
            this.textBox398.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox398.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox398.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox398.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox398.CanGrow = false;
            this.textBox398.DataField = "ITEM124";
            this.textBox398.Height = 0.1458333F;
            this.textBox398.Left = 3.347932F;
            this.textBox398.Name = "textBox398";
            this.textBox398.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox398.Tag = "";
            this.textBox398.Text = "ITEM124";
            this.textBox398.Top = 0.291732F;
            this.textBox398.Width = 0.7875F;
            // 
            // textBox399
            // 
            this.textBox399.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox399.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox399.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox399.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox399.CanGrow = false;
            this.textBox399.DataField = "ITEM125";
            this.textBox399.Height = 0.1458333F;
            this.textBox399.Left = 4.135429F;
            this.textBox399.Name = "textBox399";
            this.textBox399.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox399.Tag = "";
            this.textBox399.Text = "ITEM125";
            this.textBox399.Top = 0.291732F;
            this.textBox399.Width = 0.7875F;
            // 
            // textBox400
            // 
            this.textBox400.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox400.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox400.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox400.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox400.CanGrow = false;
            this.textBox400.DataField = "ITEM126";
            this.textBox400.Height = 0.1458333F;
            this.textBox400.Left = 4.92293F;
            this.textBox400.Name = "textBox400";
            this.textBox400.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox400.Tag = "";
            this.textBox400.Text = "ITEM126";
            this.textBox400.Top = 0.291732F;
            this.textBox400.Width = 0.7875F;
            // 
            // textBox401
            // 
            this.textBox401.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox401.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox401.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox401.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox401.CanGrow = false;
            this.textBox401.DataField = "ITEM127";
            this.textBox401.Height = 0.1458333F;
            this.textBox401.Left = 5.710432F;
            this.textBox401.Name = "textBox401";
            this.textBox401.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox401.Tag = "";
            this.textBox401.Text = "ITEM127";
            this.textBox401.Top = 0.291732F;
            this.textBox401.Width = 0.7875F;
            // 
            // textBox402
            // 
            this.textBox402.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox402.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox402.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox402.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox402.CanGrow = false;
            this.textBox402.DataField = "ITEM128";
            this.textBox402.Height = 0.1458333F;
            this.textBox402.Left = 6.497931F;
            this.textBox402.Name = "textBox402";
            this.textBox402.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox402.Tag = "";
            this.textBox402.Text = "ITEM128";
            this.textBox402.Top = 0.291732F;
            this.textBox402.Width = 0.7875F;
            // 
            // textBox403
            // 
            this.textBox403.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox403.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox403.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox403.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox403.CanGrow = false;
            this.textBox403.DataField = "ITEM129";
            this.textBox403.Height = 0.1458333F;
            this.textBox403.Left = 7.28543F;
            this.textBox403.Name = "textBox403";
            this.textBox403.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox403.Tag = "";
            this.textBox403.Text = "ITEM129";
            this.textBox403.Top = 0.291732F;
            this.textBox403.Width = 0.7875F;
            // 
            // textBox404
            // 
            this.textBox404.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox404.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox404.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox404.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox404.CanGrow = false;
            this.textBox404.DataField = "ITEM111";
            this.textBox404.Height = 0.1458333F;
            this.textBox404.Left = 0.9854333F;
            this.textBox404.Name = "textBox404";
            this.textBox404.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox404.Tag = "";
            this.textBox404.Text = "ITEM111";
            this.textBox404.Top = 0.1458986F;
            this.textBox404.Width = 0.7875F;
            // 
            // textBox405
            // 
            this.textBox405.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox405.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox405.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox405.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox405.CanGrow = false;
            this.textBox405.DataField = "ITEM112";
            this.textBox405.Height = 0.1458333F;
            this.textBox405.Left = 1.772931F;
            this.textBox405.Name = "textBox405";
            this.textBox405.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox405.Tag = "";
            this.textBox405.Text = "ITEM112";
            this.textBox405.Top = 0.1458986F;
            this.textBox405.Width = 0.7875F;
            // 
            // textBox406
            // 
            this.textBox406.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox406.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox406.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox406.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox406.CanGrow = false;
            this.textBox406.DataField = "ITEM113";
            this.textBox406.Height = 0.1458333F;
            this.textBox406.Left = 2.560432F;
            this.textBox406.Name = "textBox406";
            this.textBox406.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox406.Tag = "";
            this.textBox406.Text = "ITEM113";
            this.textBox406.Top = 0.1458986F;
            this.textBox406.Width = 0.7875F;
            // 
            // textBox407
            // 
            this.textBox407.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox407.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox407.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox407.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox407.CanGrow = false;
            this.textBox407.DataField = "ITEM114";
            this.textBox407.Height = 0.1458333F;
            this.textBox407.Left = 3.347932F;
            this.textBox407.Name = "textBox407";
            this.textBox407.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox407.Tag = "";
            this.textBox407.Text = "ITEM114";
            this.textBox407.Top = 0.1458986F;
            this.textBox407.Width = 0.7875F;
            // 
            // textBox408
            // 
            this.textBox408.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox408.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox408.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox408.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox408.CanGrow = false;
            this.textBox408.DataField = "ITEM115";
            this.textBox408.Height = 0.1458333F;
            this.textBox408.Left = 4.135429F;
            this.textBox408.Name = "textBox408";
            this.textBox408.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox408.Tag = "";
            this.textBox408.Text = "ITEM115";
            this.textBox408.Top = 0.1458986F;
            this.textBox408.Width = 0.7875F;
            // 
            // textBox409
            // 
            this.textBox409.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox409.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox409.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox409.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox409.CanGrow = false;
            this.textBox409.DataField = "ITEM116";
            this.textBox409.Height = 0.1458333F;
            this.textBox409.Left = 4.92293F;
            this.textBox409.Name = "textBox409";
            this.textBox409.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox409.Tag = "";
            this.textBox409.Text = "ITEM116";
            this.textBox409.Top = 0.1458986F;
            this.textBox409.Width = 0.7875F;
            // 
            // textBox410
            // 
            this.textBox410.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox410.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox410.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox410.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox410.CanGrow = false;
            this.textBox410.DataField = "ITEM117";
            this.textBox410.Height = 0.1458333F;
            this.textBox410.Left = 5.710432F;
            this.textBox410.Name = "textBox410";
            this.textBox410.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox410.Tag = "";
            this.textBox410.Text = "ITEM117";
            this.textBox410.Top = 0.1458986F;
            this.textBox410.Width = 0.7875F;
            // 
            // textBox411
            // 
            this.textBox411.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox411.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox411.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox411.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox411.CanGrow = false;
            this.textBox411.DataField = "ITEM118";
            this.textBox411.Height = 0.1458333F;
            this.textBox411.Left = 6.497931F;
            this.textBox411.Name = "textBox411";
            this.textBox411.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox411.Tag = "";
            this.textBox411.Text = "ITEM118";
            this.textBox411.Top = 0.1458986F;
            this.textBox411.Width = 0.7875F;
            // 
            // textBox412
            // 
            this.textBox412.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox412.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox412.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox412.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox412.CanGrow = false;
            this.textBox412.DataField = "ITEM119";
            this.textBox412.Height = 0.1458333F;
            this.textBox412.Left = 7.28543F;
            this.textBox412.Name = "textBox412";
            this.textBox412.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox412.Tag = "";
            this.textBox412.Text = "ITEM119";
            this.textBox412.Top = 0.1458986F;
            this.textBox412.Width = 0.7875F;
            // 
            // textBox413
            // 
            this.textBox413.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox413.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox413.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox413.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox413.Height = 0.1458333F;
            this.textBox413.Left = 0.9846458F;
            this.textBox413.Name = "textBox413";
            this.textBox413.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "right; ddo-char-set: 1";
            this.textBox413.Tag = "";
            this.textBox413.Text = null;
            this.textBox413.Top = 0F;
            this.textBox413.Width = 0.7875F;
            // 
            // textBox414
            // 
            this.textBox414.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox414.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox414.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox414.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox414.Height = 0.1458333F;
            this.textBox414.Left = 1.772143F;
            this.textBox414.Name = "textBox414";
            this.textBox414.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "right; ddo-char-set: 1";
            this.textBox414.Tag = "";
            this.textBox414.Text = null;
            this.textBox414.Top = 0F;
            this.textBox414.Width = 0.7875F;
            // 
            // textBox415
            // 
            this.textBox415.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox415.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox415.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox415.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox415.Height = 0.1458333F;
            this.textBox415.Left = 2.559644F;
            this.textBox415.Name = "textBox415";
            this.textBox415.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "right; ddo-char-set: 1";
            this.textBox415.Tag = "";
            this.textBox415.Text = null;
            this.textBox415.Top = 0F;
            this.textBox415.Width = 0.7875F;
            // 
            // textBox416
            // 
            this.textBox416.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox416.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox416.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox416.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox416.Height = 0.1458333F;
            this.textBox416.Left = 3.347145F;
            this.textBox416.Name = "textBox416";
            this.textBox416.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "right; ddo-char-set: 1";
            this.textBox416.Tag = "";
            this.textBox416.Text = null;
            this.textBox416.Top = 0F;
            this.textBox416.Width = 0.7875F;
            // 
            // textBox417
            // 
            this.textBox417.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox417.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox417.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox417.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox417.Height = 0.1458333F;
            this.textBox417.Left = 4.134641F;
            this.textBox417.Name = "textBox417";
            this.textBox417.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "right; ddo-char-set: 1";
            this.textBox417.Tag = "";
            this.textBox417.Text = null;
            this.textBox417.Top = 0F;
            this.textBox417.Width = 0.7875F;
            // 
            // textBox418
            // 
            this.textBox418.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox418.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox418.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox418.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox418.Height = 0.1458333F;
            this.textBox418.Left = 4.922143F;
            this.textBox418.Name = "textBox418";
            this.textBox418.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "right; ddo-char-set: 1";
            this.textBox418.Tag = "";
            this.textBox418.Text = null;
            this.textBox418.Top = 0F;
            this.textBox418.Width = 0.7875F;
            // 
            // textBox419
            // 
            this.textBox419.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox419.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox419.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox419.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox419.Height = 0.1458333F;
            this.textBox419.Left = 5.709643F;
            this.textBox419.Name = "textBox419";
            this.textBox419.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "right; ddo-char-set: 1";
            this.textBox419.Tag = "";
            this.textBox419.Text = null;
            this.textBox419.Top = 0F;
            this.textBox419.Width = 0.7875F;
            // 
            // textBox420
            // 
            this.textBox420.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox420.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox420.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox420.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox420.Height = 0.1458333F;
            this.textBox420.Left = 6.497143F;
            this.textBox420.Name = "textBox420";
            this.textBox420.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "right; ddo-char-set: 1";
            this.textBox420.Tag = "";
            this.textBox420.Text = null;
            this.textBox420.Top = 0F;
            this.textBox420.Width = 0.7875F;
            // 
            // textBox421
            // 
            this.textBox421.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox421.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox421.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox421.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox421.Height = 0.1458333F;
            this.textBox421.Left = 7.284646F;
            this.textBox421.Name = "textBox421";
            this.textBox421.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: bold; text-align: " +
    "right; ddo-char-set: 1";
            this.textBox421.Tag = "";
            this.textBox421.Text = null;
            this.textBox421.Top = 0F;
            this.textBox421.Width = 0.7875F;
            // 
            // textBox422
            // 
            this.textBox422.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox422.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox422.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox422.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox422.CanGrow = false;
            this.textBox422.Height = 0.1458333F;
            this.textBox422.Left = 4.440892E-16F;
            this.textBox422.Name = "textBox422";
            this.textBox422.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.textBox422.Tag = "";
            this.textBox422.Text = "前　職　分";
            this.textBox422.Top = 0F;
            this.textBox422.Width = 0.9846458F;
            // 
            // textBox423
            // 
            this.textBox423.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox423.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox423.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox423.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox423.CanGrow = false;
            this.textBox423.Height = 0.2917323F;
            this.textBox423.Left = 4.440892E-16F;
            this.textBox423.Name = "textBox423";
            this.textBox423.Style = "background-color: Silver; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font" +
    "-weight: bold; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox423.Tag = "";
            this.textBox423.Text = "合　計";
            this.textBox423.Top = 0.1456687F;
            this.textBox423.Width = 0.9846458F;
            // 
            // textBox424
            // 
            this.textBox424.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox424.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox424.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox424.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox424.CanGrow = false;
            this.textBox424.Height = 0.7409452F;
            this.textBox424.Left = 4.440892E-16F;
            this.textBox424.Name = "textBox424";
            this.textBox424.Style = "background-color: Silver; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-" +
    "weight: bold; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox424.Tag = "";
            this.textBox424.Text = "年末調整";
            this.textBox424.Top = 0.5149597F;
            this.textBox424.Width = 0.1972222F;
            // 
            // textBox425
            // 
            this.textBox425.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox425.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox425.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox425.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox425.CanGrow = false;
            this.textBox425.DataField = "ITEM132";
            this.textBox425.Height = 0.1458333F;
            this.textBox425.Left = 0.9846458F;
            this.textBox425.Name = "textBox425";
            this.textBox425.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox425.Tag = "";
            this.textBox425.Text = "ITEM132";
            this.textBox425.Top = 0.7397639F;
            this.textBox425.Width = 0.7875F;
            // 
            // textBox426
            // 
            this.textBox426.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox426.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox426.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox426.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox426.CanGrow = false;
            this.textBox426.DataField = "ITEM133";
            this.textBox426.Height = 0.1458333F;
            this.textBox426.Left = 1.772143F;
            this.textBox426.Name = "textBox426";
            this.textBox426.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox426.Tag = "";
            this.textBox426.Text = "ITEM133";
            this.textBox426.Top = 0.7397639F;
            this.textBox426.Width = 0.7875F;
            // 
            // textBox427
            // 
            this.textBox427.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox427.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox427.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox427.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox427.CanGrow = false;
            this.textBox427.DataField = "ITEM134";
            this.textBox427.Height = 0.1458333F;
            this.textBox427.Left = 2.559644F;
            this.textBox427.Name = "textBox427";
            this.textBox427.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox427.Tag = "";
            this.textBox427.Text = "ITEM134";
            this.textBox427.Top = 0.7397639F;
            this.textBox427.Width = 0.7875F;
            // 
            // textBox428
            // 
            this.textBox428.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox428.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox428.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox428.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox428.CanGrow = false;
            this.textBox428.DataField = "ITEM135";
            this.textBox428.Height = 0.1458333F;
            this.textBox428.Left = 3.347145F;
            this.textBox428.Name = "textBox428";
            this.textBox428.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox428.Tag = "";
            this.textBox428.Text = "ITEM135";
            this.textBox428.Top = 0.7397639F;
            this.textBox428.Width = 0.7875F;
            // 
            // textBox429
            // 
            this.textBox429.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox429.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox429.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox429.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox429.CanGrow = false;
            this.textBox429.DataField = "ITEM136";
            this.textBox429.Height = 0.1458333F;
            this.textBox429.Left = 4.134641F;
            this.textBox429.Name = "textBox429";
            this.textBox429.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox429.Tag = "";
            this.textBox429.Text = "ITEM136";
            this.textBox429.Top = 0.7397639F;
            this.textBox429.Width = 0.7875F;
            // 
            // textBox430
            // 
            this.textBox430.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox430.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox430.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox430.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox430.CanGrow = false;
            this.textBox430.DataField = "ITEM137";
            this.textBox430.Height = 0.1458333F;
            this.textBox430.Left = 4.922143F;
            this.textBox430.Name = "textBox430";
            this.textBox430.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox430.Tag = "";
            this.textBox430.Text = "ITEM137";
            this.textBox430.Top = 0.7397639F;
            this.textBox430.Width = 0.7875F;
            // 
            // textBox431
            // 
            this.textBox431.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox431.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox431.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox431.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox431.CanGrow = false;
            this.textBox431.DataField = "ITEM138";
            this.textBox431.Height = 0.1458333F;
            this.textBox431.Left = 5.709643F;
            this.textBox431.Name = "textBox431";
            this.textBox431.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox431.Tag = "";
            this.textBox431.Text = "ITEM138";
            this.textBox431.Top = 0.7397639F;
            this.textBox431.Width = 0.7875F;
            // 
            // textBox432
            // 
            this.textBox432.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox432.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox432.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox432.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox432.CanGrow = false;
            this.textBox432.DataField = "ITEM139";
            this.textBox432.Height = 0.1458333F;
            this.textBox432.Left = 6.497141F;
            this.textBox432.Name = "textBox432";
            this.textBox432.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox432.Tag = "";
            this.textBox432.Text = "ITEM139";
            this.textBox432.Top = 0.7397639F;
            this.textBox432.Width = 0.7875F;
            // 
            // textBox433
            // 
            this.textBox433.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox433.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox433.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox433.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox433.CanGrow = false;
            this.textBox433.DataField = "ITEM140";
            this.textBox433.Height = 0.1458333F;
            this.textBox433.Left = 7.284639F;
            this.textBox433.Name = "textBox433";
            this.textBox433.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox433.Tag = "";
            this.textBox433.Text = "ITEM140";
            this.textBox433.Top = 0.7397639F;
            this.textBox433.Width = 0.7875F;
            // 
            // textBox434
            // 
            this.textBox434.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox434.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox434.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox434.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox434.CanGrow = false;
            this.textBox434.Height = 0.2245737F;
            this.textBox434.Left = 0.9846458F;
            this.textBox434.Name = "textBox434";
            this.textBox434.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; vertical-align: middle; ddo-char-set: 1";
            this.textBox434.Tag = "";
            this.textBox434.Text = "賞　与　等";
            this.textBox434.Top = 0.5151904F;
            this.textBox434.Width = 0.7875F;
            // 
            // textBox435
            // 
            this.textBox435.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox435.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox435.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox435.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox435.CanGrow = false;
            this.textBox435.Height = 0.2245737F;
            this.textBox435.Left = 1.772143F;
            this.textBox435.Name = "textBox435";
            this.textBox435.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; vertical-align: middle; ddo-char-set: 1";
            this.textBox435.Tag = "";
            this.textBox435.Text = "給料賞与等合計";
            this.textBox435.Top = 0.5151904F;
            this.textBox435.Width = 0.7875F;
            // 
            // textBox436
            // 
            this.textBox436.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox436.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox436.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox436.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox436.CanGrow = false;
            this.textBox436.Height = 0.2245737F;
            this.textBox436.Left = 2.559644F;
            this.textBox436.Name = "textBox436";
            this.textBox436.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; vertical-align: middle; ddo-char-set: 1";
            this.textBox436.Tag = "";
            this.textBox436.Text = "給与所得控除後\r\nの給与等の額";
            this.textBox436.Top = 0.5151904F;
            this.textBox436.Width = 0.7875F;
            // 
            // textBox437
            // 
            this.textBox437.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox437.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox437.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox437.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox437.Height = 0.2245737F;
            this.textBox437.Left = 3.347145F;
            this.textBox437.Name = "textBox437";
            this.textBox437.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-weight: bold; text-align: " +
    "center; vertical-align: middle; ddo-char-set: 1";
            this.textBox437.Tag = "";
            this.textBox437.Text = "\r\n";
            this.textBox437.Top = 0.5151904F;
            this.textBox437.Width = 0.7875F;
            // 
            // textBox438
            // 
            this.textBox438.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox438.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox438.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox438.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox438.CanGrow = false;
            this.textBox438.Height = 0.2245737F;
            this.textBox438.Left = 4.134641F;
            this.textBox438.Name = "textBox438";
            this.textBox438.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-weight: normal; text-align" +
    ": center; vertical-align: middle; ddo-char-set: 1";
            this.textBox438.Tag = "";
            this.textBox438.Text = "社会保険料控除額\r\n（申告分）";
            this.textBox438.Top = 0.5151904F;
            this.textBox438.Width = 0.7875F;
            // 
            // textBox439
            // 
            this.textBox439.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox439.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox439.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox439.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox439.CanGrow = false;
            this.textBox439.Height = 0.2245737F;
            this.textBox439.Left = 4.922143F;
            this.textBox439.Name = "textBox439";
            this.textBox439.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-weight: normal; text-align" +
    ": center; vertical-align: middle; ddo-char-set: 1";
            this.textBox439.Tag = "";
            this.textBox439.Text = "小規模企業共済等\r\n掛金の控除額";
            this.textBox439.Top = 0.5151904F;
            this.textBox439.Width = 0.7875F;
            // 
            // textBox440
            // 
            this.textBox440.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox440.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox440.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox440.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox440.CanGrow = false;
            this.textBox440.Height = 0.2245737F;
            this.textBox440.Left = 5.709643F;
            this.textBox440.Name = "textBox440";
            this.textBox440.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6.5pt; font-weight: normal; text-ali" +
    "gn: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox440.Tag = "";
            this.textBox440.Text = "生命保険料控除額";
            this.textBox440.Top = 0.5151904F;
            this.textBox440.Width = 0.7875F;
            // 
            // textBox441
            // 
            this.textBox441.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox441.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox441.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox441.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox441.CanGrow = false;
            this.textBox441.Height = 0.2245737F;
            this.textBox441.Left = 6.497141F;
            this.textBox441.Name = "textBox441";
            this.textBox441.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6.5pt; font-weight: normal; text-ali" +
    "gn: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox441.Tag = "";
            this.textBox441.Text = "地震保険料控除額";
            this.textBox441.Top = 0.5151904F;
            this.textBox441.Width = 0.7875F;
            // 
            // textBox442
            // 
            this.textBox442.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox442.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox442.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox442.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox442.CanGrow = false;
            this.textBox442.Height = 0.2245737F;
            this.textBox442.Left = 7.284639F;
            this.textBox442.Name = "textBox442";
            this.textBox442.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6.5pt; font-weight: normal; text-ali" +
    "gn: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox442.Tag = "";
            this.textBox442.Text = "配偶者特別控除額";
            this.textBox442.Top = 0.5151904F;
            this.textBox442.Width = 0.7875F;
            // 
            // textBox443
            // 
            this.textBox443.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox443.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox443.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox443.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox443.CanGrow = false;
            this.textBox443.DataField = "ITEM131";
            this.textBox443.Height = 0.1458333F;
            this.textBox443.Left = 0.1980315F;
            this.textBox443.Name = "textBox443";
            this.textBox443.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox443.Tag = "";
            this.textBox443.Text = "ITEM131";
            this.textBox443.Top = 0.7399269F;
            this.textBox443.Width = 0.7875F;
            // 
            // textBox444
            // 
            this.textBox444.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox444.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox444.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox444.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox444.CanGrow = false;
            this.textBox444.Height = 0.2245737F;
            this.textBox444.Left = 0.1980315F;
            this.textBox444.Name = "textBox444";
            this.textBox444.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; vertical-align: middle; ddo-char-set: 1";
            this.textBox444.Tag = "";
            this.textBox444.Text = "給料・手当等";
            this.textBox444.Top = 0.5153543F;
            this.textBox444.Width = 0.7875F;
            // 
            // label21
            // 
            this.label21.Height = 0.2244095F;
            this.label21.HyperLink = null;
            this.label21.Left = 3.351969F;
            this.label21.Name = "label21";
            this.label21.Style = "font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align: center; vert" +
    "ical-align: middle; ddo-char-set: 1";
            this.label21.Text = "社会保険料控除額\r\n（給与からの控除分）";
            this.label21.Top = 0.5153543F;
            this.label21.Width = 0.775591F;
            // 
            // textBox445
            // 
            this.textBox445.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox445.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox445.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox445.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox445.CanGrow = false;
            this.textBox445.DataField = "ITEM142";
            this.textBox445.Height = 0.1458333F;
            this.textBox445.Left = 0.9838583F;
            this.textBox445.Name = "textBox445";
            this.textBox445.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox445.Tag = "";
            this.textBox445.Text = "ITEM142";
            this.textBox445.Top = 1.110236F;
            this.textBox445.Width = 0.7875F;
            // 
            // textBox446
            // 
            this.textBox446.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox446.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox446.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox446.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox446.CanGrow = false;
            this.textBox446.DataField = "ITEM143";
            this.textBox446.Height = 0.1458333F;
            this.textBox446.Left = 1.771355F;
            this.textBox446.Name = "textBox446";
            this.textBox446.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox446.Tag = "";
            this.textBox446.Text = "ITEM143";
            this.textBox446.Top = 1.110236F;
            this.textBox446.Width = 0.7875F;
            // 
            // textBox447
            // 
            this.textBox447.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox447.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox447.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox447.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox447.CanGrow = false;
            this.textBox447.DataField = "ITEM144";
            this.textBox447.Height = 0.1458333F;
            this.textBox447.Left = 2.558856F;
            this.textBox447.Name = "textBox447";
            this.textBox447.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox447.Tag = "";
            this.textBox447.Text = "ITEM144";
            this.textBox447.Top = 1.110236F;
            this.textBox447.Width = 0.7875F;
            // 
            // textBox448
            // 
            this.textBox448.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox448.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox448.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox448.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox448.CanGrow = false;
            this.textBox448.DataField = "ITEM145";
            this.textBox448.Height = 0.1458333F;
            this.textBox448.Left = 3.346357F;
            this.textBox448.Name = "textBox448";
            this.textBox448.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox448.Tag = "";
            this.textBox448.Text = "ITEM145";
            this.textBox448.Top = 1.110236F;
            this.textBox448.Width = 0.7875F;
            // 
            // textBox449
            // 
            this.textBox449.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox449.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox449.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox449.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox449.CanGrow = false;
            this.textBox449.DataField = "ITEM146";
            this.textBox449.Height = 0.1458333F;
            this.textBox449.Left = 4.133854F;
            this.textBox449.Name = "textBox449";
            this.textBox449.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox449.Tag = "";
            this.textBox449.Text = "ITEM146";
            this.textBox449.Top = 1.110236F;
            this.textBox449.Width = 0.7875F;
            // 
            // textBox450
            // 
            this.textBox450.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox450.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox450.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox450.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox450.CanGrow = false;
            this.textBox450.DataField = "ITEM147";
            this.textBox450.Height = 0.1458333F;
            this.textBox450.Left = 4.921355F;
            this.textBox450.Name = "textBox450";
            this.textBox450.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox450.Tag = "";
            this.textBox450.Text = "ITEM147";
            this.textBox450.Top = 1.110236F;
            this.textBox450.Width = 0.7875F;
            // 
            // textBox451
            // 
            this.textBox451.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox451.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox451.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox451.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox451.CanGrow = false;
            this.textBox451.DataField = "ITEM148";
            this.textBox451.Height = 0.1458333F;
            this.textBox451.Left = 5.708856F;
            this.textBox451.Name = "textBox451";
            this.textBox451.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox451.Tag = "";
            this.textBox451.Text = "ITEM148";
            this.textBox451.Top = 1.110236F;
            this.textBox451.Width = 0.7875F;
            // 
            // textBox452
            // 
            this.textBox452.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox452.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox452.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox452.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox452.CanGrow = false;
            this.textBox452.DataField = "ITEM149";
            this.textBox452.Height = 0.1458333F;
            this.textBox452.Left = 6.496356F;
            this.textBox452.Name = "textBox452";
            this.textBox452.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox452.Tag = "";
            this.textBox452.Text = "ITEM149";
            this.textBox452.Top = 1.110236F;
            this.textBox452.Width = 0.7875F;
            // 
            // textBox453
            // 
            this.textBox453.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox453.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox453.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox453.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox453.CanGrow = false;
            this.textBox453.DataField = "ITEM150";
            this.textBox453.Height = 0.1458333F;
            this.textBox453.Left = 7.283854F;
            this.textBox453.Name = "textBox453";
            this.textBox453.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox453.Tag = "";
            this.textBox453.Text = "ITEM150";
            this.textBox453.Top = 1.110236F;
            this.textBox453.Width = 0.7875F;
            // 
            // textBox454
            // 
            this.textBox454.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox454.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox454.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox454.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox454.CanGrow = false;
            this.textBox454.Height = 0.2245737F;
            this.textBox454.Left = 0.9838583F;
            this.textBox454.Name = "textBox454";
            this.textBox454.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; vertical-align: middle; ddo-char-set: 1";
            this.textBox454.Tag = "";
            this.textBox454.Text = "所得控除額の\r\n合　計　額";
            this.textBox454.Top = 0.8856617F;
            this.textBox454.Width = 0.7875F;
            // 
            // textBox455
            // 
            this.textBox455.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox455.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox455.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox455.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox455.CanGrow = false;
            this.textBox455.Height = 0.2245737F;
            this.textBox455.Left = 1.771355F;
            this.textBox455.Name = "textBox455";
            this.textBox455.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": center; vertical-align: middle; ddo-char-set: 1";
            this.textBox455.Tag = "";
            this.textBox455.Top = 0.8856617F;
            this.textBox455.Width = 0.7875F;
            // 
            // textBox456
            // 
            this.textBox456.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox456.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox456.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox456.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox456.CanGrow = false;
            this.textBox456.Height = 0.2245737F;
            this.textBox456.Left = 2.558856F;
            this.textBox456.Name = "textBox456";
            this.textBox456.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; vertical-align: middle; ddo-char-set: 1";
            this.textBox456.Tag = "";
            this.textBox456.Text = "年　税　額";
            this.textBox456.Top = 0.8856617F;
            this.textBox456.Width = 0.7875F;
            // 
            // textBox457
            // 
            this.textBox457.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox457.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox457.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox457.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox457.Height = 0.2245737F;
            this.textBox457.Left = 3.346357F;
            this.textBox457.Name = "textBox457";
            this.textBox457.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-weight: bold; text-align: " +
    "center; vertical-align: middle; ddo-char-set: 1";
            this.textBox457.Tag = "";
            this.textBox457.Text = "\r\n";
            this.textBox457.Top = 0.8856617F;
            this.textBox457.Width = 0.7875F;
            // 
            // textBox458
            // 
            this.textBox458.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox458.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox458.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox458.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox458.CanGrow = false;
            this.textBox458.Height = 0.2245737F;
            this.textBox458.Left = 4.133854F;
            this.textBox458.Name = "textBox458";
            this.textBox458.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-weight: normal; text-align" +
    ": center; vertical-align: middle; ddo-char-set: 1";
            this.textBox458.Tag = "";
            this.textBox458.Text = "差引年税額";
            this.textBox458.Top = 0.8856617F;
            this.textBox458.Width = 0.7875F;
            // 
            // textBox459
            // 
            this.textBox459.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox459.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox459.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox459.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox459.CanGrow = false;
            this.textBox459.Height = 0.2245737F;
            this.textBox459.Left = 4.921355F;
            this.textBox459.Name = "textBox459";
            this.textBox459.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-weight: bold; text-align: " +
    "center; vertical-align: middle; ddo-char-set: 1";
            this.textBox459.Tag = "";
            this.textBox459.Text = "\r\n";
            this.textBox459.Top = 0.8856617F;
            this.textBox459.Width = 0.7875F;
            // 
            // textBox460
            // 
            this.textBox460.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox460.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox460.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox460.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox460.CanGrow = false;
            this.textBox460.Height = 0.2245737F;
            this.textBox460.Left = 5.708856F;
            this.textBox460.Name = "textBox460";
            this.textBox460.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6.5pt; font-weight: normal; text-ali" +
    "gn: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox460.Tag = "";
            this.textBox460.Text = "徴収年税額";
            this.textBox460.Top = 0.8856617F;
            this.textBox460.Width = 0.7875F;
            // 
            // textBox461
            // 
            this.textBox461.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox461.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox461.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox461.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox461.CanGrow = false;
            this.textBox461.Height = 0.2245737F;
            this.textBox461.Left = 6.496356F;
            this.textBox461.Name = "textBox461";
            this.textBox461.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6.5pt; font-weight: normal; text-ali" +
    "gn: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox461.Tag = "";
            this.textBox461.Text = "徴収税額";
            this.textBox461.Top = 0.8856617F;
            this.textBox461.Width = 0.7875F;
            // 
            // textBox462
            // 
            this.textBox462.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox462.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox462.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox462.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox462.CanGrow = false;
            this.textBox462.Height = 0.2245737F;
            this.textBox462.Left = 7.283854F;
            this.textBox462.Name = "textBox462";
            this.textBox462.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6.5pt; font-weight: normal; text-ali" +
    "gn: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox462.Tag = "";
            this.textBox462.Text = "差引過不足税額";
            this.textBox462.Top = 0.8856617F;
            this.textBox462.Width = 0.7875F;
            // 
            // textBox463
            // 
            this.textBox463.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox463.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox463.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox463.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox463.CanGrow = false;
            this.textBox463.DataField = "ITEM141";
            this.textBox463.Height = 0.1458333F;
            this.textBox463.Left = 0.197244F;
            this.textBox463.Name = "textBox463";
            this.textBox463.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox463.Tag = "";
            this.textBox463.Text = "ITEM141";
            this.textBox463.Top = 1.110399F;
            this.textBox463.Width = 0.7875F;
            // 
            // textBox464
            // 
            this.textBox464.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox464.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox464.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox464.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox464.CanGrow = false;
            this.textBox464.Height = 0.2245737F;
            this.textBox464.Left = 0.197244F;
            this.textBox464.Name = "textBox464";
            this.textBox464.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": center; vertical-align: middle; ddo-char-set: 1";
            this.textBox464.Tag = "";
            this.textBox464.Top = 0.8858272F;
            this.textBox464.Width = 0.7875F;
            // 
            // label22
            // 
            this.label22.Height = 0.2362205F;
            this.label22.HyperLink = null;
            this.label22.Left = 3.351181F;
            this.label22.Name = "label22";
            this.label22.Style = "font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align: center; vert" +
    "ical-align: middle; ddo-char-set: 1";
            this.label22.Text = "住 宅 借 入 金 等\r\n特 別 控 除 額";
            this.label22.Top = 0.8858268F;
            this.label22.Width = 0.775591F;
            // 
            // textBox465
            // 
            this.textBox465.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox465.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox465.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox465.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox465.CanGrow = false;
            this.textBox465.DataField = "ITEM151";
            this.textBox465.Height = 0.1458333F;
            this.textBox465.Left = 8.255512F;
            this.textBox465.Name = "textBox465";
            this.textBox465.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox465.Tag = "";
            this.textBox465.Text = "ITEM151";
            this.textBox465.Top = 0.2102362F;
            this.textBox465.Width = 0.7875F;
            // 
            // textBox466
            // 
            this.textBox466.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox466.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox466.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox466.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox466.CanGrow = false;
            this.textBox466.DataField = "ITEM152";
            this.textBox466.Height = 0.1458333F;
            this.textBox466.Left = 9.043005F;
            this.textBox466.Name = "textBox466";
            this.textBox466.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox466.Tag = "";
            this.textBox466.Text = "ITEM152";
            this.textBox466.Top = 0.2102362F;
            this.textBox466.Width = 0.7875F;
            // 
            // textBox467
            // 
            this.textBox467.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox467.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox467.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox467.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox467.CanGrow = false;
            this.textBox467.DataField = "ITEM153";
            this.textBox467.Height = 0.1458333F;
            this.textBox467.Left = 9.830505F;
            this.textBox467.Name = "textBox467";
            this.textBox467.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox467.Tag = "";
            this.textBox467.Text = "ITEM153";
            this.textBox467.Top = 0.2102362F;
            this.textBox467.Width = 0.7875F;
            // 
            // textBox468
            // 
            this.textBox468.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox468.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox468.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox468.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox468.CanGrow = false;
            this.textBox468.Height = 0.147244F;
            this.textBox468.Left = 8.255512F;
            this.textBox468.Name = "textBox468";
            this.textBox468.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6.5pt; font-weight: normal; text-ali" +
    "gn: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox468.Tag = "";
            this.textBox468.Text = "配偶者の合計所得";
            this.textBox468.Top = 0.06299246F;
            this.textBox468.Width = 0.7875F;
            // 
            // textBox469
            // 
            this.textBox469.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox469.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox469.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox469.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox469.CanGrow = false;
            this.textBox469.Height = 0.147244F;
            this.textBox469.Left = 9.043005F;
            this.textBox469.Name = "textBox469";
            this.textBox469.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6.5pt; font-weight: normal; text-ali" +
    "gn: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox469.Tag = "";
            this.textBox469.Text = "新生命保険料";
            this.textBox469.Top = 0.06299246F;
            this.textBox469.Width = 0.7875F;
            // 
            // textBox470
            // 
            this.textBox470.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox470.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox470.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox470.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox470.CanGrow = false;
            this.textBox470.Height = 0.147244F;
            this.textBox470.Left = 9.830505F;
            this.textBox470.Name = "textBox470";
            this.textBox470.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6.5pt; font-weight: normal; text-ali" +
    "gn: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox470.Tag = "";
            this.textBox470.Text = "旧生命保険料";
            this.textBox470.Top = 0.06299246F;
            this.textBox470.Width = 0.7875F;
            // 
            // textBox471
            // 
            this.textBox471.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox471.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox471.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox471.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox471.CanGrow = false;
            this.textBox471.DataField = "ITEM154";
            this.textBox471.Height = 0.1458333F;
            this.textBox471.Left = 8.255512F;
            this.textBox471.Name = "textBox471";
            this.textBox471.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox471.Tag = "";
            this.textBox471.Text = "ITEM154";
            this.textBox471.Top = 0.5031487F;
            this.textBox471.Width = 0.7875F;
            // 
            // textBox472
            // 
            this.textBox472.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox472.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox472.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox472.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox472.CanGrow = false;
            this.textBox472.DataField = "ITEM155";
            this.textBox472.Height = 0.1458333F;
            this.textBox472.Left = 9.043005F;
            this.textBox472.Name = "textBox472";
            this.textBox472.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox472.Tag = "";
            this.textBox472.Text = "ITEM155";
            this.textBox472.Top = 0.5031487F;
            this.textBox472.Width = 0.7875F;
            // 
            // textBox473
            // 
            this.textBox473.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox473.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox473.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox473.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox473.CanGrow = false;
            this.textBox473.DataField = "ITEM156";
            this.textBox473.Height = 0.1458333F;
            this.textBox473.Left = 9.830505F;
            this.textBox473.Name = "textBox473";
            this.textBox473.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox473.Tag = "";
            this.textBox473.Text = "ITEM156";
            this.textBox473.Top = 0.5031487F;
            this.textBox473.Width = 0.7875F;
            // 
            // textBox474
            // 
            this.textBox474.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox474.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox474.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox474.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox474.CanGrow = false;
            this.textBox474.Height = 0.147244F;
            this.textBox474.Left = 8.255512F;
            this.textBox474.Name = "textBox474";
            this.textBox474.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6.5pt; font-weight: normal; text-ali" +
    "gn: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox474.Tag = "";
            this.textBox474.Text = "介護医療保険料";
            this.textBox474.Top = 0.3559062F;
            this.textBox474.Width = 0.7875F;
            // 
            // textBox475
            // 
            this.textBox475.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox475.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox475.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox475.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox475.CanGrow = false;
            this.textBox475.Height = 0.147244F;
            this.textBox475.Left = 9.043005F;
            this.textBox475.Name = "textBox475";
            this.textBox475.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6.5pt; font-weight: normal; text-ali" +
    "gn: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox475.Tag = "";
            this.textBox475.Text = "新個人年金保険料";
            this.textBox475.Top = 0.3559062F;
            this.textBox475.Width = 0.7875F;
            // 
            // textBox476
            // 
            this.textBox476.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox476.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox476.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox476.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox476.CanGrow = false;
            this.textBox476.Height = 0.147244F;
            this.textBox476.Left = 9.830505F;
            this.textBox476.Name = "textBox476";
            this.textBox476.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6.5pt; font-weight: normal; text-ali" +
    "gn: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox476.Tag = "";
            this.textBox476.Text = "旧個人年金保険料";
            this.textBox476.Top = 0.3559062F;
            this.textBox476.Width = 0.7875F;
            // 
            // textBox477
            // 
            this.textBox477.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox477.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox477.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox477.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox477.CanGrow = false;
            this.textBox477.Height = 0.3141733F;
            this.textBox477.Left = 8.255512F;
            this.textBox477.Name = "textBox477";
            this.textBox477.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox477.Tag = "";
            this.textBox477.Text = null;
            this.textBox477.Top = 0.9417321F;
            this.textBox477.Width = 2.362599F;
            // 
            // textBox478
            // 
            this.textBox478.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox478.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox478.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox478.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox478.CanGrow = false;
            this.textBox478.DataField = "ITEM157";
            this.textBox478.Height = 0.1458333F;
            this.textBox478.Left = 8.255512F;
            this.textBox478.Name = "textBox478";
            this.textBox478.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox478.Tag = "";
            this.textBox478.Text = "ITEM157";
            this.textBox478.Top = 0.7960634F;
            this.textBox478.Width = 0.7875F;
            // 
            // textBox479
            // 
            this.textBox479.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox479.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox479.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox479.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox479.CanGrow = false;
            this.textBox479.Height = 0.1458333F;
            this.textBox479.Left = 9.043005F;
            this.textBox479.Name = "textBox479";
            this.textBox479.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox479.Tag = "";
            this.textBox479.Text = null;
            this.textBox479.Top = 0.7960634F;
            this.textBox479.Width = 0.7875F;
            // 
            // textBox480
            // 
            this.textBox480.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox480.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox480.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox480.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox480.CanGrow = false;
            this.textBox480.Height = 0.1458333F;
            this.textBox480.Left = 9.830505F;
            this.textBox480.Name = "textBox480";
            this.textBox480.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox480.Tag = "";
            this.textBox480.Text = null;
            this.textBox480.Top = 0.7960634F;
            this.textBox480.Width = 0.7875F;
            // 
            // textBox481
            // 
            this.textBox481.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox481.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox481.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox481.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox481.CanGrow = false;
            this.textBox481.Height = 0.147244F;
            this.textBox481.Left = 8.255512F;
            this.textBox481.Name = "textBox481";
            this.textBox481.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6.5pt; font-weight: normal; text-ali" +
    "gn: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox481.Tag = "";
            this.textBox481.Text = "旧長期損害保険料";
            this.textBox481.Top = 0.648819F;
            this.textBox481.Width = 0.7875F;
            // 
            // textBox482
            // 
            this.textBox482.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox482.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox482.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox482.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox482.CanGrow = false;
            this.textBox482.Height = 0.147244F;
            this.textBox482.Left = 9.043005F;
            this.textBox482.Name = "textBox482";
            this.textBox482.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6.5pt; font-weight: normal; text-ali" +
    "gn: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox482.Tag = "";
            this.textBox482.Text = "\r\n";
            this.textBox482.Top = 0.648819F;
            this.textBox482.Width = 0.7875F;
            // 
            // textBox483
            // 
            this.textBox483.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox483.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox483.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox483.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox483.CanGrow = false;
            this.textBox483.Height = 0.147244F;
            this.textBox483.Left = 9.830505F;
            this.textBox483.Name = "textBox483";
            this.textBox483.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6.5pt; font-weight: normal; text-ali" +
    "gn: center; vertical-align: middle; ddo-char-set: 1";
            this.textBox483.Tag = "";
            this.textBox483.Text = "\r\n";
            this.textBox483.Top = 0.648819F;
            this.textBox483.Width = 0.7875F;
            // 
            // label1
            // 
            this.label1.Height = 0.2244095F;
            this.label1.HyperLink = null;
            this.label1.Left = 0.157874F;
            this.label1.Name = "label1";
            this.label1.Style = "font-family: ＭＳ 明朝; font-size: 5.5pt; font-weight: normal; text-align: center; ve" +
    "rtical-align: middle; ddo-char-set: 1";
            this.label1.Text = "配・扶・基及び障害等\r\nの控除額の合計";
            this.label1.Top = 0.8897638F;
            this.label1.Width = 0.8637796F;
            // 
            // label2
            // 
            this.label2.Height = 0.2244095F;
            this.label2.HyperLink = null;
            this.label2.Left = 1.727953F;
            this.label2.Name = "label2";
            this.label2.Style = "font-family: ＭＳ 明朝; font-size: 5.5pt; font-weight: normal; text-align: center; ve" +
    "rtical-align: middle; ddo-char-set: 1";
            this.label2.Text = "差引課税給与所得金額\r\n（1,000未満切捨て）";
            this.label2.Top = 0.8897638F;
            this.label2.Width = 0.8637799F;
            // 
            // KYUR3051R_3
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 10.62992F;
            this.Sections.Add(this.detail);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.textBox395)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox396)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox397)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox398)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox399)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox400)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox401)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox402)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox403)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox404)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox405)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox406)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox407)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox408)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox409)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox410)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox411)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox412)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox413)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox414)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox415)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox416)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox417)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox418)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox419)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox420)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox421)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox422)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox423)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox424)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox425)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox426)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox427)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox428)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox429)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox430)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox431)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox432)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox433)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox434)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox435)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox436)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox437)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox438)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox439)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox440)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox441)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox442)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox443)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox444)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox445)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox446)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox447)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox448)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox449)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox450)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox451)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox452)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox453)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox454)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox455)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox456)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox457)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox458)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox459)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox460)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox461)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox462)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox463)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox464)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox465)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox466)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox467)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox468)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox469)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox470)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox471)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox472)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox473)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox474)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox475)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox476)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox477)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox478)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox479)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox480)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox481)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox482)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox483)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox395;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox396;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox397;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox398;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox399;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox400;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox401;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox402;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox403;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox404;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox405;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox406;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox407;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox408;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox409;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox410;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox411;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox412;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox413;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox414;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox415;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox416;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox417;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox418;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox419;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox420;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox421;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox422;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox423;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox424;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox425;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox426;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox427;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox428;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox429;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox430;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox431;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox432;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox433;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox434;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox435;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox436;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox437;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox438;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox439;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox440;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox441;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox442;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox443;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox444;
        private GrapeCity.ActiveReports.SectionReportModel.Label label21;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox445;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox446;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox447;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox448;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox449;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox450;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox451;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox452;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox453;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox454;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox455;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox456;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox457;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox458;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox459;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox460;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox461;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox462;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox463;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox464;
        private GrapeCity.ActiveReports.SectionReportModel.Label label22;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox465;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox466;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox467;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox468;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox469;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox470;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox471;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox472;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox473;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox474;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox475;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox476;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox477;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox478;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox479;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox480;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox481;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox482;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox483;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.Label label2;
    }
}
