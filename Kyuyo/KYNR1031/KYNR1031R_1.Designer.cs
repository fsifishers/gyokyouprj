﻿namespace jp.co.fsi.ky.kynr1031
{
    /// <summary>
    /// KYNR1031R_1 の帳票
    /// </summary>
    partial class KYNR1031R_1
    {
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(KYNR1031R_1));
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.ITEM121 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM122 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM123 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM124 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM125 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM126 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM127 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM128 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM129 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM103 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM111 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM112 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM113 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM114 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM115 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM116 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM117 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM118 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM119 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.title = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM102 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.直線464 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM121)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM122)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM123)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM124)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM125)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM126)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM127)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM128)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM129)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM103)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM111)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM112)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM113)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM114)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM115)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM116)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM117)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM118)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM119)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.title)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM102)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.ITEM121,
            this.ITEM122,
            this.ITEM123,
            this.ITEM124,
            this.ITEM125,
            this.ITEM126,
            this.ITEM127,
            this.ITEM128,
            this.ITEM129,
            this.ITEM103,
            this.ITEM111,
            this.ITEM112,
            this.ITEM113,
            this.ITEM114,
            this.ITEM115,
            this.ITEM116,
            this.ITEM117,
            this.ITEM118,
            this.ITEM119,
            this.title,
            this.ITEM102,
            this.直線464});
            this.detail.Height = 0.2916667F;
            this.detail.Name = "detail";
            // 
            // ITEM121
            // 
            this.ITEM121.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM121.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM121.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM121.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM121.CanGrow = false;
            this.ITEM121.DataField = "ITEM121";
            this.ITEM121.Height = 0.1458333F;
            this.ITEM121.Left = 0.9854167F;
            this.ITEM121.Name = "ITEM121";
            this.ITEM121.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.ITEM121.Tag = "";
            this.ITEM121.Text = "ITEM121";
            this.ITEM121.Top = 0.1458333F;
            this.ITEM121.Width = 0.7875F;
            // 
            // ITEM122
            // 
            this.ITEM122.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM122.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM122.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM122.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM122.CanGrow = false;
            this.ITEM122.DataField = "ITEM122";
            this.ITEM122.Height = 0.1458333F;
            this.ITEM122.Left = 1.772916F;
            this.ITEM122.Name = "ITEM122";
            this.ITEM122.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.ITEM122.Tag = "";
            this.ITEM122.Text = "ITEM122";
            this.ITEM122.Top = 0.1458333F;
            this.ITEM122.Width = 0.7875F;
            // 
            // ITEM123
            // 
            this.ITEM123.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM123.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM123.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM123.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM123.CanGrow = false;
            this.ITEM123.DataField = "ITEM123";
            this.ITEM123.Height = 0.1458333F;
            this.ITEM123.Left = 2.560416F;
            this.ITEM123.Name = "ITEM123";
            this.ITEM123.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.ITEM123.Tag = "";
            this.ITEM123.Text = "ITEM123";
            this.ITEM123.Top = 0.1458333F;
            this.ITEM123.Width = 0.7875F;
            // 
            // ITEM124
            // 
            this.ITEM124.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM124.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM124.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM124.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM124.CanGrow = false;
            this.ITEM124.DataField = "ITEM124";
            this.ITEM124.Height = 0.1458333F;
            this.ITEM124.Left = 3.347917F;
            this.ITEM124.Name = "ITEM124";
            this.ITEM124.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.ITEM124.Tag = "";
            this.ITEM124.Text = "ITEM124";
            this.ITEM124.Top = 0.1458333F;
            this.ITEM124.Width = 0.7875F;
            // 
            // ITEM125
            // 
            this.ITEM125.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM125.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM125.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM125.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM125.CanGrow = false;
            this.ITEM125.DataField = "ITEM125";
            this.ITEM125.Height = 0.1458333F;
            this.ITEM125.Left = 4.135415F;
            this.ITEM125.Name = "ITEM125";
            this.ITEM125.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.ITEM125.Tag = "";
            this.ITEM125.Text = "ITEM125";
            this.ITEM125.Top = 0.1458333F;
            this.ITEM125.Width = 0.7875F;
            // 
            // ITEM126
            // 
            this.ITEM126.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM126.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM126.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM126.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM126.CanGrow = false;
            this.ITEM126.DataField = "ITEM126";
            this.ITEM126.Height = 0.1458333F;
            this.ITEM126.Left = 4.922915F;
            this.ITEM126.Name = "ITEM126";
            this.ITEM126.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.ITEM126.Tag = "";
            this.ITEM126.Text = "ITEM126";
            this.ITEM126.Top = 0.1458333F;
            this.ITEM126.Width = 0.7875F;
            // 
            // ITEM127
            // 
            this.ITEM127.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM127.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM127.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM127.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM127.CanGrow = false;
            this.ITEM127.DataField = "ITEM127";
            this.ITEM127.Height = 0.1458333F;
            this.ITEM127.Left = 5.710416F;
            this.ITEM127.Name = "ITEM127";
            this.ITEM127.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.ITEM127.Tag = "";
            this.ITEM127.Text = "ITEM127";
            this.ITEM127.Top = 0.1458333F;
            this.ITEM127.Width = 0.7875F;
            // 
            // ITEM128
            // 
            this.ITEM128.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM128.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM128.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM128.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM128.CanGrow = false;
            this.ITEM128.DataField = "ITEM128";
            this.ITEM128.Height = 0.1458333F;
            this.ITEM128.Left = 6.497915F;
            this.ITEM128.Name = "ITEM128";
            this.ITEM128.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.ITEM128.Tag = "";
            this.ITEM128.Text = "ITEM128";
            this.ITEM128.Top = 0.1458333F;
            this.ITEM128.Width = 0.7875F;
            // 
            // ITEM129
            // 
            this.ITEM129.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM129.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM129.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM129.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM129.CanGrow = false;
            this.ITEM129.DataField = "ITEM129";
            this.ITEM129.Height = 0.1458333F;
            this.ITEM129.Left = 7.285417F;
            this.ITEM129.Name = "ITEM129";
            this.ITEM129.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.ITEM129.Tag = "";
            this.ITEM129.Text = "ITEM129";
            this.ITEM129.Top = 0.1458333F;
            this.ITEM129.Width = 0.7875F;
            // 
            // ITEM103
            // 
            this.ITEM103.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM103.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM103.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM103.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM103.DataField = "ITEM103";
            this.ITEM103.Height = 0.2916667F;
            this.ITEM103.Left = 0.3944445F;
            this.ITEM103.Name = "ITEM103";
            this.ITEM103.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.ITEM103.Tag = "";
            this.ITEM103.Text = "ITEM103";
            this.ITEM103.Top = 0F;
            this.ITEM103.Width = 0.5909723F;
            // 
            // ITEM111
            // 
            this.ITEM111.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM111.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM111.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM111.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM111.CanGrow = false;
            this.ITEM111.DataField = "ITEM111";
            this.ITEM111.Height = 0.1458333F;
            this.ITEM111.Left = 0.9854167F;
            this.ITEM111.Name = "ITEM111";
            this.ITEM111.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.ITEM111.Tag = "";
            this.ITEM111.Text = "ITEM111";
            this.ITEM111.Top = 0F;
            this.ITEM111.Width = 0.7875F;
            // 
            // ITEM112
            // 
            this.ITEM112.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM112.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM112.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM112.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM112.CanGrow = false;
            this.ITEM112.DataField = "ITEM112";
            this.ITEM112.Height = 0.1458333F;
            this.ITEM112.Left = 1.772916F;
            this.ITEM112.Name = "ITEM112";
            this.ITEM112.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.ITEM112.Tag = "";
            this.ITEM112.Text = "ITEM112";
            this.ITEM112.Top = 0F;
            this.ITEM112.Width = 0.7875F;
            // 
            // ITEM113
            // 
            this.ITEM113.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM113.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM113.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM113.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM113.CanGrow = false;
            this.ITEM113.DataField = "ITEM113";
            this.ITEM113.Height = 0.1458333F;
            this.ITEM113.Left = 2.560416F;
            this.ITEM113.Name = "ITEM113";
            this.ITEM113.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.ITEM113.Tag = "";
            this.ITEM113.Text = "ITEM113";
            this.ITEM113.Top = 0F;
            this.ITEM113.Width = 0.7875F;
            // 
            // ITEM114
            // 
            this.ITEM114.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM114.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM114.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM114.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM114.CanGrow = false;
            this.ITEM114.DataField = "ITEM114";
            this.ITEM114.Height = 0.1458333F;
            this.ITEM114.Left = 3.347917F;
            this.ITEM114.Name = "ITEM114";
            this.ITEM114.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.ITEM114.Tag = "";
            this.ITEM114.Text = "ITEM114";
            this.ITEM114.Top = 0F;
            this.ITEM114.Width = 0.7875F;
            // 
            // ITEM115
            // 
            this.ITEM115.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM115.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM115.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM115.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM115.CanGrow = false;
            this.ITEM115.DataField = "ITEM115";
            this.ITEM115.Height = 0.1458333F;
            this.ITEM115.Left = 4.135415F;
            this.ITEM115.Name = "ITEM115";
            this.ITEM115.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.ITEM115.Tag = "";
            this.ITEM115.Text = "ITEM115";
            this.ITEM115.Top = 0F;
            this.ITEM115.Width = 0.7875F;
            // 
            // ITEM116
            // 
            this.ITEM116.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM116.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM116.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM116.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM116.CanGrow = false;
            this.ITEM116.DataField = "ITEM116";
            this.ITEM116.Height = 0.1458333F;
            this.ITEM116.Left = 4.922915F;
            this.ITEM116.Name = "ITEM116";
            this.ITEM116.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.ITEM116.Tag = "";
            this.ITEM116.Text = "ITEM116";
            this.ITEM116.Top = 0F;
            this.ITEM116.Width = 0.7875F;
            // 
            // ITEM117
            // 
            this.ITEM117.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM117.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM117.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM117.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM117.CanGrow = false;
            this.ITEM117.DataField = "ITEM117";
            this.ITEM117.Height = 0.1458333F;
            this.ITEM117.Left = 5.710416F;
            this.ITEM117.Name = "ITEM117";
            this.ITEM117.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.ITEM117.Tag = "";
            this.ITEM117.Text = "ITEM117";
            this.ITEM117.Top = 0F;
            this.ITEM117.Width = 0.7875F;
            // 
            // ITEM118
            // 
            this.ITEM118.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM118.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM118.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM118.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM118.CanGrow = false;
            this.ITEM118.DataField = "ITEM118";
            this.ITEM118.Height = 0.1458333F;
            this.ITEM118.Left = 6.497915F;
            this.ITEM118.Name = "ITEM118";
            this.ITEM118.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.ITEM118.Tag = "";
            this.ITEM118.Text = "ITEM118";
            this.ITEM118.Top = 0F;
            this.ITEM118.Width = 0.7875F;
            // 
            // ITEM119
            // 
            this.ITEM119.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM119.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM119.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM119.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM119.CanGrow = false;
            this.ITEM119.DataField = "ITEM119";
            this.ITEM119.Height = 0.1458333F;
            this.ITEM119.Left = 7.285417F;
            this.ITEM119.Name = "ITEM119";
            this.ITEM119.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.ITEM119.Tag = "";
            this.ITEM119.Text = "ITEM119";
            this.ITEM119.Top = 0F;
            this.ITEM119.Width = 0.7875F;
            // 
            // title
            // 
            this.title.DataField = "ITEM101";
            this.title.Height = 0.2913387F;
            this.title.Left = 0F;
            this.title.Name = "title";
            this.title.Style = "background-color: Silver; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-" +
    "weight: normal; text-align: center; ddo-char-set: 1";
            this.title.Tag = "";
            this.title.Text = "ITEM101";
            this.title.Top = 0F;
            this.title.Width = 0.1972222F;
            // 
            // ITEM102
            // 
            this.ITEM102.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM102.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM102.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM102.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM102.DataField = "ITEM102";
            this.ITEM102.Height = 0.2916667F;
            this.ITEM102.Left = 0.1972222F;
            this.ITEM102.Name = "ITEM102";
            this.ITEM102.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.ITEM102.Tag = "";
            this.ITEM102.Text = "ITEM102";
            this.ITEM102.Top = 0F;
            this.ITEM102.Width = 0.1972222F;
            // 
            // 直線464
            // 
            this.直線464.Height = 0.2916667F;
            this.直線464.Left = 0F;
            this.直線464.LineWeight = 0F;
            this.直線464.Name = "直線464";
            this.直線464.Tag = "";
            this.直線464.Top = 0F;
            this.直線464.Width = 0F;
            this.直線464.X1 = 0F;
            this.直線464.X2 = 0F;
            this.直線464.Y1 = 0F;
            this.直線464.Y2 = 0.2916667F;
            // 
            // KYUR3051R_1
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 8.083333F;
            this.Sections.Add(this.detail);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.ITEM121)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM122)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM123)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM124)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM125)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM126)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM127)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM128)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM129)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM103)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM111)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM112)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM113)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM114)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM115)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM116)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM117)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM118)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM119)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.title)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM102)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM121;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM122;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM123;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM124;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM125;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM126;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM127;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM128;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM129;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM102;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM103;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM111;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM112;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM113;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM114;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM115;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM116;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM117;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM118;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM119;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox title;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線464;

    }
}
