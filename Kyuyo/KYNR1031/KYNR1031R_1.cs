﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using jp.co.fsi.common.report;
using System.Data;

namespace jp.co.fsi.ky.kynr1031
{
    /// <summary>
    /// KYNR1031R_1 の帳票
    /// </summary>
    public partial class KYNR1031R_1 : BaseReport
    {

        public KYNR1031R_1(DataTable tgtData)
            : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }
    }
}
