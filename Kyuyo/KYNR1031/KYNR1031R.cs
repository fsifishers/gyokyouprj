﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using jp.co.fsi.common.report;
using System.Data;

namespace jp.co.fsi.ky.kynr1031
{
    /// <summary>
    /// KYNR1031R の帳票
    /// </summary>
    public partial class KYNR1031R : BaseReport
    {
        private KYNR1031R_1 rpt;        //賞与サブレポート
        private KYNR1031R_2 rpt1;       //該当区分サブレポート
        private KYNR1031R_3 rpt2;       //合計サブレポート
        DataTable _shoyo;
        DataTable _gaitoKubun;
        DataTable _gokei;
        public KYNR1031R(DataTable kyuyo, DataTable shoyo, DataTable kubun, DataTable gokei)
            : base(kyuyo)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
            _shoyo = shoyo;
            _gaitoKubun = kubun;
            _gokei = gokei;
        }

        private void pageHeader_Format(object sender, EventArgs e)
        {
            // 賞与データテーブルを条件（社員）抽出
            #region
            DataTable dtshoyo = _shoyo.Clone();
            DataRow r = null;
            foreach (DataRow dtRow in _shoyo.Select("ITEM001 = " + this.txt001.Value))
            {
                r = dtshoyo.NewRow();
                for (int n = 0; n < dtRow.ItemArray.Length; n++)
                {
                    r[n] = dtRow[n];
                }
                dtshoyo.Rows.Add(r);
            }
            rpt = new KYNR1031R_1(dtshoyo);
            #endregion

            // 該当区分データテーブルを条件（社員）抽出
            #region
            DataTable dtKubun = _gaitoKubun.Clone();
            DataRow r1 = null;
            foreach (DataRow dtRow in _gaitoKubun.Select("ITEM001 = " + this.txt001.Value))
            {
                r1 = dtKubun.NewRow();
                for (int n = 0; n < dtRow.ItemArray.Length; n++)
                {
                    r1[n] = dtRow[n];
                }
                dtKubun.Rows.Add(r1);
            }
            rpt1 = new KYNR1031R_2(dtKubun);
            #endregion

            // 合計データテーブルを条件（社員）抽出
            #region
            DataTable dtGokei = _gokei.Clone();
            DataRow r2 = null;
            foreach (DataRow dtRow in _gokei.Select("ITEM001 = " + this.txt001.Value))
            {
                r2 = dtGokei.NewRow();
                for (int n = 0; n < dtRow.ItemArray.Length; n++)
                {
                    r2[n] = dtRow[n];
                }
                dtGokei.Rows.Add(r2);
            }
            rpt2 = new KYNR1031R_3(dtGokei);
            #endregion
        }
        private void detail_Format(object sender, EventArgs e)
        {
            this.KYNR1031R_1.CanGrow = true;
            this.KYNR1031R_1.CanShrink = false;
            this.KYNR1031R_1.ReportName = "KYNR1031R_1";
            this.KYNR1031R_2.CanGrow = true;
            this.KYNR1031R_2.CanShrink = false;
            this.KYNR1031R_2.ReportName = "KYNR1031R_2";
            this.KYNR1031R_3.CanGrow = true;
            this.KYNR1031R_3.CanShrink = false;
            this.KYNR1031R_3.ReportName = "KYNR1031R_3";
            this.KYNR1031R_1.Report = rpt;
            this.KYNR1031R_2.Report = rpt1;
            this.KYNR1031R_3.Report = rpt2;
        }
    }
}
