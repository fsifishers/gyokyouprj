﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Reflection;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.access;
using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.ky.kynr1031
{
    /// <summary>
    /// 源泉徴収簿(KYNR1031)
    /// </summary>
    public partial class KYNR1031 : BasePgForm
    {
        #region 変数
        int _sort;                                          // 出力ワークSORTフィールド
        decimal[] _shainGokei;                              // 社員別支給実績合計用
        DataTable _dtKyuSti;                                // 給与項目設定データ
        DataTable _dtShoSti;                                // 賞与項目設定データ
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KYNR1031()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 年度初期値
            if (Util.ToInt(Util.ToString(this.txtGengoYearNendo.Text)) == 0)
            {
                string[] nendo = Util.ConvJpDate(new DateTime(this.UInfo.KaikeiNendo, 4, 1), this.Dba);
                this.lblGengoNendo.Text = nendo[0];
                this.txtGengoYearNendo.Text = nendo[2];
            }

            // オプション初期値
            this.rdoTaishoku1.Checked = true;
            this.rdoOrder2.Checked = true;
            this.rdoKanpu1.Checked = true;

            // 初期フォーカス
            this.txtGengoYearNendo.Focus();
            this.txtGengoYearNendo.SelectAll();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 元号年とコード項目に
            // フォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtGengoYearNendo":
                case "txtShainCdFr":
                case "txtShainCdTo":
                case "txtBumonCdFr":
                case "txtBumonCdTo":
                case "txtBukaCdFr":
                case "txtBukaCdTo":
                    this.btnF1.Enabled = true;
                    break;
                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        public override void PressF1()
        {
            Assembly asm;
            Type t;

            switch (this.ActiveCtlNm)
            {
                case "txtGengoYearNendo":
                    #region 元号検索
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            if (this.ActiveCtlNm == "txtGengoYearNendo")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.InData = this.lblGengoNendo.Text;
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.lblGengoNendo.Text = result[1];

                                    // 和暦年未入力時の初期値
                                    if (Util.ToInt(Util.ToString(this.txtGengoYearNendo.Text)) == 0)
                                    {
                                        string[] nendo = Util.ConvJpDate(new DateTime(this.UInfo.KaikeiNendo, 4, 1), this.Dba);
                                        this.lblGengoNendo.Text = nendo[0];
                                        this.txtGengoYearNendo.Text = nendo[2];
                                    }

                                    // 和暦年補正
                                    DateTime adDate =
                                        Util.ConvAdDate(this.lblGengoNendo.Text, this.txtGengoYearNendo.Text, "4", "1", this.Dba);
                                    string[] jpDate = Util.ConvJpDate(adDate, this.Dba);
                                    this.lblGengoNendo.Text = jpDate[0];
                                    this.txtGengoYearNendo.Text = jpDate[2];
                                }
                            }
                        }

                    }
                    #endregion
                    break;
                case "txtShainCdFr":
                case "txtShainCdTo":
                    #region 社員検索
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("KYCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.ky.kycm1021.KYCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            if (this.ActiveCtlNm == "txtShainCdFr")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.Par1 = "1";                     // ダイアログ起動パラメータ
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.txtShainCdFr.Text = result[0];
                                    this.lblShainCdFr.Text = result[1];
                                }
                            }
                            else if (this.ActiveCtlNm == "txtShainCdTo")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.Par1 = "1";                     // ダイアログ起動パラメータ
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.txtShainCdTo.Text = result[0];
                                    this.lblShainCdTo.Text = result[1];
                                }
                            }
                        }
                    }
                    #endregion
                    break;
                case "txtBumonCdFr":
                case "txtBumonCdTo":
                    #region 部門検索
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("KYCM1031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.ky.kycm1031.KYCM1031");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            if (this.ActiveCtlNm == "txtBumonCdFr")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.Par1 = "1";
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.txtBumonCdFr.Text = result[0];
                                    this.lblBumonCdFr.Text = result[1];
                                    this.txtBukaCdFr.Text = "";
                                    this.lblBukaCdFr.Text = "先　頭";
                                }
                            }
                            else if (this.ActiveCtlNm == "txtBumonCdTo")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.Par1 = "1";
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.txtBumonCdTo.Text = result[0];
                                    this.lblBumonCdTo.Text = result[1];
                                    this.txtBukaCdTo.Text = "";
                                    this.lblBukaCdTo.Text = "最　後";
                                }
                            }
                        }
                    }
                    #endregion
                    break;                
                case "txtBukaCdFr":
                case "txtBukaCdTo":
                    #region 部課検索
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("KYCM1041.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.ky.kycm1041.KYCM1041");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            if (this.ActiveCtlNm == "txtBukaCdFr" 
                                && !ValChk.IsEmpty(this.txtBumonCdFr.Text))
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.Par1 = "1";                         // 検索ダイアログ起動パラメータ
                                frm.InData = this.txtBumonCdFr.Text;    // (string)部門コード
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.txtBukaCdFr.Text = result[0];
                                    this.lblBukaCdFr.Text = result[1];
                                }
                            }
                            else if (this.ActiveCtlNm == "txtBukaCdTo"
                                && !ValChk.IsEmpty(this.txtBumonCdTo.Text))
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.Par1 = "1";                         // 検索ダイアログ起動パラメータ
                                frm.InData = this.txtBumonCdTo.Text;    // (string)部門コード
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.txtBukaCdTo.Text = result[0];
                                    this.lblBukaCdTo.Text = result[1];
                                }
                            }
                        }
                    }
                    #endregion
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
            {
                // プレビュー処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            if (Msg.ConfNmYesNo("印刷", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }
        #endregion

        #region イベント
        /// <summary>
        /// 社員コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShainCdFr_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtShainCdFr.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtShainCdFr.SelectAll();
                e.Cancel = true;
                return;
            }

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (this.txtShainCdFr.Text.Length > 0)
            {
                this.lblShainCdFr.Text = this.Dba.GetName(this.UInfo, "TB_KY_SHAIN_JOHO"," ", this.txtShainCdFr.Text);
            }
            else
            {
                this.lblShainCdFr.Text = "先　頭";
            }
        }

        /// <summary>
        /// 社員コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShainCdTo_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtShainCdTo.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtShainCdTo.SelectAll();
                e.Cancel = true;
                return;
            }

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (this.txtShainCdTo.Text.Length > 0)
            {
                this.lblShainCdTo.Text = this.Dba.GetName(this.UInfo, "TB_KY_SHAIN_JOHO", " ", this.txtShainCdTo.Text);
            }
            else
            {
                this.lblShainCdTo.Text = "最　後";
            }
        }

        /// <summary>
        /// 部門コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBumonCdFr_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtBumonCdFr.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtBumonCdFr.SelectAll();
                e.Cancel = true;
                return;
            }

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (this.txtBumonCdFr.Text.Length > 0)
            {
                this.lblBumonCdFr.Text = this.Dba.GetName(this.UInfo, "TB_KY_BUMON", " ", this.txtBumonCdFr.Text);
            }
            else
            {
                this.lblBumonCdFr.Text = "先　頭";
            }
        }

        /// <summary>
        /// 部門コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBumonCdTo_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtBumonCdTo.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtBumonCdTo.SelectAll();
                e.Cancel = true;
                return;
            }

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (this.txtBumonCdTo.Text.Length > 0)
            {
                this.lblBumonCdTo.Text = this.Dba.GetName(this.UInfo, "TB_KY_BUMON", " ", this.txtBumonCdTo.Text);
            }
            else
            {
                this.lblBumonCdTo.Text = "最　後";
            }
        }

        /// <summary>
        /// 部課コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBukaCdFr_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtBukaCdFr.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtBukaCdFr.SelectAll();
                e.Cancel = true;
                return;
            }

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (this.txtBukaCdFr.Text.Length > 0)
            {
                this.lblBukaCdFr.Text = this.Dba.GetBukaNm(this.UInfo, this.txtBumonCdFr.Text, this.txtBukaCdFr.Text);
            }
            else
            {
                this.lblBukaCdFr.Text = "先　頭";
            }
        }

        /// <summary>
        /// 部課コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBukaCdTo_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtBukaCdTo.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtBukaCdTo.SelectAll();
                e.Cancel = true;
                return;
            }

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (this.txtBukaCdTo.Text.Length > 0)
            {
                this.lblBukaCdTo.Text = this.Dba.GetBukaNm(this.UInfo,this.txtBumonCdTo.Text, this.txtBukaCdTo.Text);
            }
            else
            {
                this.lblBukaCdTo.Text = "最　後";
            }
        }

        /// <summary>
        /// 年調還付金の控除税反映(しない)のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rdoKanpu1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter )
            {
                if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
                {
                    // ﾌﾟﾚﾋﾞｭｰ処理
                    DoPrint(true);
                }
                else
                {
                    this.rdoKanpu1.Focus();
                }
            }
        }

        /// <summary>
        /// 年調還付金の控除税反映(する)のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rdoKanpu2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
                {
                    // ﾌﾟﾚﾋﾞｭｰ処理
                    DoPrint(true);
                }
                else
                {
                    this.rdoKanpu2.Focus();
                }
            }
        }

        /// <summary>
        /// 年度(年)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGengoYearNendo_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtGengoYearNendo.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtGengoYearNendo.SelectAll();
                e.Cancel = true;
                return;
            }

            string[] nendo;

            // 年度の取得
            if (Util.ToInt(Util.ToString(this.txtGengoYearNendo.Text)) == 0)
            {
                // 無効年入力の時
                nendo = Util.ConvJpDate(new DateTime(this.UInfo.KaikeiNendo, 1, 1), this.Dba);
            }
            else
            {
                DateTime adNendo =
                    Util.ConvAdDate(this.lblGengoNendo.Text, this.txtGengoYearNendo.Text, "4", "1", this.Dba);
                nendo = Util.ConvJpDate(adNendo, this.Dba);
            }
            this.lblGengoNendo.Text = nendo[0];
            this.txtGengoYearNendo.Text = nendo[2];
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview)
        {
            try
            {
                bool dataFlag;

                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                dataFlag = MakeWkData();

                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    StringBuilder cols = new StringBuilder();
                    cols.Append("  ITEM001");
                    cols.Append(" ,ITEM002");
                    cols.Append(" ,ITEM003");
                    cols.Append(" ,ITEM004");
                    cols.Append(" ,ITEM005");
                    cols.Append(" ,ITEM006");
                    cols.Append(" ,ITEM007");
                    cols.Append(" ,ITEM008");
                    cols.Append(" ,ITEM009");
                    cols.Append(" ,ITEM010");
                    cols.Append(" ,ITEM011");
                    cols.Append(" ,ITEM012");
                    cols.Append(" ,ITEM013");
                    cols.Append(" ,ITEM014");
                    cols.Append(" ,ITEM015");
                    cols.Append(" ,ITEM016");
                    cols.Append(" ,ITEM017");
                    cols.Append(" ,ITEM018");
                    cols.Append(" ,ITEM019");
                    cols.Append(" ,ITEM020");
                    cols.Append(" ,ITEM021");
                    cols.Append(" ,ITEM022");
                    cols.Append(" ,ITEM023");
                    cols.Append(" ,ITEM024");
                    cols.Append(" ,ITEM025");
                    cols.Append(" ,ITEM026");
                    cols.Append(" ,ITEM027");
                    cols.Append(" ,ITEM028");
                    cols.Append(" ,ITEM029");
                    cols.Append(" ,ITEM030");
                    cols.Append(" ,ITEM031");
                    cols.Append(" ,ITEM032");
                    cols.Append(" ,ITEM033");
                    cols.Append(" ,ITEM034");
                    cols.Append(" ,ITEM035");
                    cols.Append(" ,ITEM036");
                    cols.Append(" ,ITEM037");
                    cols.Append(" ,ITEM038");
                    cols.Append(" ,ITEM039");
                    cols.Append(" ,ITEM040");
                    cols.Append(" ,ITEM041");
                    cols.Append(" ,ITEM042");
                    cols.Append(" ,ITEM043");
                    cols.Append(" ,ITEM044");
                    cols.Append(" ,ITEM045");
                    cols.Append(" ,ITEM046");
                    cols.Append(" ,ITEM047");
                    cols.Append(" ,ITEM048");
                    cols.Append(" ,ITEM049");
                    cols.Append(" ,ITEM050");
                    cols.Append(" ,ITEM051");
                    cols.Append(" ,ITEM052");
                    cols.Append(" ,ITEM053");
                    cols.Append(" ,ITEM054");
                    cols.Append(" ,ITEM055");
                    cols.Append(" ,ITEM056");
                    cols.Append(" ,ITEM057");
                    cols.Append(" ,ITEM058");
                    cols.Append(" ,ITEM059");
                    cols.Append(" ,ITEM060");
                    cols.Append(" ,ITEM061");
                    cols.Append(" ,ITEM062");
                    cols.Append(" ,ITEM063");
                    cols.Append(" ,ITEM064");
                    cols.Append(" ,ITEM065");
                    cols.Append(" ,ITEM066");
                    cols.Append(" ,ITEM067");
                    cols.Append(" ,ITEM068");
                    cols.Append(" ,ITEM069");
                    cols.Append(" ,ITEM070");
                    cols.Append(" ,ITEM071");
                    cols.Append(" ,ITEM072");
                    cols.Append(" ,ITEM073");
                    cols.Append(" ,ITEM074");
                    cols.Append(" ,ITEM075");
                    cols.Append(" ,ITEM076");
                    cols.Append(" ,ITEM077");
                    cols.Append(" ,ITEM078");
                    cols.Append(" ,ITEM079");
                    cols.Append(" ,ITEM080");
                    cols.Append(" ,ITEM081");
                    cols.Append(" ,ITEM082");
                    cols.Append(" ,ITEM083");
                    cols.Append(" ,ITEM084");
                    cols.Append(" ,ITEM085");
                    cols.Append(" ,ITEM086");
                    cols.Append(" ,ITEM087");
                    cols.Append(" ,ITEM088");
                    cols.Append(" ,ITEM089");
                    cols.Append(" ,ITEM090");
                    cols.Append(" ,ITEM091");
                    cols.Append(" ,ITEM092");
                    cols.Append(" ,ITEM093");
                    cols.Append(" ,ITEM094");
                    cols.Append(" ,ITEM095");
                    cols.Append(" ,ITEM096");
                    cols.Append(" ,ITEM097");
                    cols.Append(" ,ITEM098");
                    cols.Append(" ,ITEM099");
                    cols.Append(" ,ITEM100");
                    cols.Append(" ,ITEM101");
                    cols.Append(" ,ITEM102");
                    cols.Append(" ,ITEM103");
                    cols.Append(" ,ITEM104");
                    cols.Append(" ,ITEM105");
                    cols.Append(" ,ITEM106");
                    cols.Append(" ,ITEM107");
                    cols.Append(" ,ITEM108");
                    cols.Append(" ,ITEM109");
                    cols.Append(" ,ITEM110");
                    cols.Append(" ,ITEM111");
                    cols.Append(" ,ITEM112");
                    cols.Append(" ,ITEM113");
                    cols.Append(" ,ITEM114");
                    cols.Append(" ,ITEM115");
                    cols.Append(" ,ITEM116");
                    cols.Append(" ,ITEM117");
                    cols.Append(" ,ITEM118");
                    cols.Append(" ,ITEM119");
                    cols.Append(" ,ITEM120");
                    cols.Append(" ,ITEM121");
                    cols.Append(" ,ITEM122");
                    cols.Append(" ,ITEM123");
                    cols.Append(" ,ITEM124");
                    cols.Append(" ,ITEM125");
                    cols.Append(" ,ITEM126");
                    cols.Append(" ,ITEM127");
                    cols.Append(" ,ITEM128");
                    cols.Append(" ,ITEM129");
                    cols.Append(" ,ITEM130");
                    cols.Append(" ,ITEM131");
                    cols.Append(" ,ITEM132");
                    cols.Append(" ,ITEM133");
                    cols.Append(" ,ITEM134");
                    cols.Append(" ,ITEM135");
                    cols.Append(" ,ITEM136");
                    cols.Append(" ,ITEM137");
                    cols.Append(" ,ITEM138");
                    cols.Append(" ,ITEM139");
                    cols.Append(" ,ITEM140");
                    cols.Append(" ,ITEM141");
                    cols.Append(" ,ITEM142");
                    cols.Append(" ,ITEM143");
                    cols.Append(" ,ITEM144");
                    cols.Append(" ,ITEM145");
                    cols.Append(" ,ITEM146");
                    cols.Append(" ,ITEM147");
                    cols.Append(" ,ITEM148");
                    cols.Append(" ,ITEM149");
                    cols.Append(" ,ITEM150");
                    cols.Append(" ,ITEM151");
                    cols.Append(" ,ITEM152");
                    cols.Append(" ,ITEM153");
                    cols.Append(" ,ITEM154");
                    cols.Append(" ,ITEM155");
                    cols.Append(" ,ITEM156");
                    cols.Append(" ,ITEM157");
                    cols.Append(" ,ITEM158");
                    cols.Append(" ,ITEM159");
                    cols.Append(" ,ITEM160");
                    cols.Append(" ,ITEM161");
                    cols.Append(" ,ITEM162");
                    cols.Append(" ,ITEM163");
                    cols.Append(" ,ITEM164");
                    cols.Append(" ,ITEM165");
                    cols.Append(" ,ITEM166");
                    cols.Append(" ,ITEM167");
                    cols.Append(" ,ITEM168");
                    cols.Append(" ,ITEM169");
                    cols.Append(" ,ITEM170");
                    cols.Append(" ,ITEM171");
                    cols.Append(" ,ITEM172");
                    cols.Append(" ,ITEM173");
                    cols.Append(" ,ITEM174");
                    cols.Append(" ,ITEM175");
                    cols.Append(" ,ITEM176");
                    cols.Append(" ,ITEM177");
                    cols.Append(" ,ITEM178");
                    cols.Append(" ,ITEM179");
                    cols.Append(" ,ITEM180");
                    cols.Append(" ,ITEM181");
                    cols.Append(" ,ITEM182");
                    cols.Append(" ,ITEM183");
                    cols.Append(" ,ITEM184");
                    cols.Append(" ,ITEM185");
                    cols.Append(" ,ITEM186");
                    cols.Append(" ,ITEM187");
                    cols.Append(" ,ITEM188");
                    cols.Append(" ,ITEM189");
                    cols.Append(" ,ITEM190");
                    cols.Append(" ,ITEM191");
                    cols.Append(" ,ITEM192");
                    cols.Append(" ,ITEM193");
                    cols.Append(" ,ITEM194");
                    cols.Append(" ,ITEM195");
                    cols.Append(" ,ITEM196");
                    cols.Append(" ,ITEM197");
                    cols.Append(" ,ITEM198");
                    cols.Append(" ,ITEM199");
                    cols.Append(" ,ITEM200");

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                    //this.Dba.Commit();

                    //給与データ取得
                    DataTable dtKyuyo = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_KY_TBL", "GUID = @GUID AND ITEM002 = 1", "SORT ASC", dpc);
                    //賞与データ取得
                    DataTable dtShoyo = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_KY_TBL", "GUID = @GUID AND ITEM002 = 2", "SORT ASC", dpc);
                    //区分データ取得
                    DataTable dtKubun = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_KY_TBL", "GUID = @GUID AND ITEM002 = 3", "SORT ASC", dpc);
                    //合計データ取得
                    DataTable dtGokei = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_KY_TBL", "GUID = @GUID AND ITEM002 = 4", "SORT ASC", dpc);
                    
                    // 帳票オブジェクトをインスタンス化
                    KYNR1031R rpt = new KYNR1031R(dtKyuyo, dtShoyo, dtKubun, dtGokei);


                    if (isPreview)
                    {
                        // プレビュー画面表示
                        PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                        pFrm.WindowState = FormWindowState.Maximized;
                        pFrm.Show();
                    }
                    else
                    {
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
            }
            finally
            {
                this.Dba.Rollback();
            }
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {
            // 年度を西暦・和暦で保持
            DateTime nendo = Util.ConvAdDate(this.lblGengoNendo.Text, this.txtGengoYearNendo.Text, "1", "1", this.Dba);
            string[] jpNendo = Util.ConvJpDate(nendo, this.Dba);

            #region 画面上での指定値
            // 社員コード設定
            string shainCdFr;
            string shainCdTo;
            if (Util.ToDecimal(txtShainCdFr.Text) > 0) 
            {
                shainCdFr = txtShainCdFr.Text;
            }
            else
            {
                shainCdFr = "0";
            }
            if (Util.ToDecimal(txtShainCdTo.Text) > 0) 
            {
                shainCdTo = txtShainCdTo.Text;
            }
            else
            {
                shainCdTo = "999999";
            }
            // 部門コード設定
            string bumonCdFr;
            string bumonCdTo;
            if (Util.ToDecimal(txtBumonCdFr.Text) > 0)
            {
                bumonCdFr = txtBumonCdFr.Text;
            }
            else
            {
                bumonCdFr = "0";
            }
            if (Util.ToDecimal(txtBumonCdTo.Text) > 0)
            {
                bumonCdTo = txtBumonCdTo.Text;
            }
            else
            {
                bumonCdTo = "9999";
            }
            // 部課コード設定
            string bukaCdFr;
            string bukaCdTo;
            if (Util.ToDecimal(txtBukaCdFr.Text) > 0)
            {
                bukaCdFr = txtBukaCdFr.Text;
            }
            else
            {
                bukaCdFr = "0";
            }
            if (Util.ToDecimal(txtBukaCdTo.Text) > 0)
            {
                bukaCdTo = txtBukaCdTo.Text;
            }
            else
            {
                bukaCdTo = "9999";
            }
            #endregion

            DbParamCollection dpc;
            StringBuilder sql;

            #region _給与/賞与項目設定データ
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 2, this.UInfo.KaishaCd);
            // VI給与項目設定
            _dtKyuSti = this.Dba.GetDataTableByConditionWithParams(
                "*",
                "VI_KY_KYUYO_KOMOKU_STI",
                //"KAISHA_CD = @KAISHA_CD AND INJI_KUBUN <> '0'",
                "KAISHA_CD = @KAISHA_CD",
                "KOMOKU_SHUBETSU, KOMOKU_BANGO",
                dpc);
            if (_dtKyuSti.Rows.Count == 0)
            {
                return false;
            }
            // VI賞与項目設定
            _dtShoSti = this.Dba.GetDataTableByConditionWithParams(
                "*",
                "VI_KY_SHOYO_KOMOKU_SETTEI",
                //"KAISHA_CD = @KAISHA_CD AND INJI_KUBUN <> '0'",
                "KAISHA_CD = @KAISHA_CD",
                "KOMOKU_SHUBETSU, KOMOKU_BANGO",
                dpc);
            if (_dtShoSti.Rows.Count == 0)
            {
                return false;
            }
            #endregion

            #region dtMainLoop = VI社員情報(VI_KY_SHAIN_JOHO)
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 2, this.UInfo.KaishaCd);
            dpc.SetParam("@SHAIN_CD_FR", SqlDbType.VarChar, 6, shainCdFr);
            dpc.SetParam("@SHAIN_CD_TO", SqlDbType.VarChar, 6, shainCdTo);
            dpc.SetParam("@BUMON_CD_FR", SqlDbType.VarChar, 4, bumonCdFr);
            dpc.SetParam("@BUMON_CD_TO", SqlDbType.VarChar, 4, bumonCdTo);
            dpc.SetParam("@BUKA_CD_FR", SqlDbType.VarChar, 4, bukaCdFr);
            dpc.SetParam("@BUKA_CD_TO", SqlDbType.VarChar, 4, bukaCdTo);
            sql = new StringBuilder();
            sql.Append("SELECT * FROM VI_KY_SHAIN_JOHO");
            sql.Append(" WHERE KAISHA_CD = @KAISHA_CD");
            sql.Append(" AND SHAIN_CD BETWEEN @SHAIN_CD_FR AND @SHAIN_CD_TO");
            sql.Append(" AND BUMON_CD BETWEEN @BUMON_CD_FR AND @BUMON_CD_TO");
            sql.Append(" AND BUKA_CD BETWEEN @BUKA_CD_FR AND @BUKA_CD_TO");
            if (this.rdoTaishoku1.Checked)
            {
                sql.Append(" AND TAISHOKU_NENGAPPI IS NULL");
            }
            // ORDER句
            if (this.rdoOrder1.Checked) 
            {
                sql.Append(" ORDER BY KAISHA_CD ASC,SHAIN_CD ASC");                            // 社員順
            }
            else
            {
                sql.Append(" ORDER BY KAISHA_CD ASC,BUMON_CD ASC,BUKA_CD ASC,SHAIN_CD ASC");   // 部門順
            }
            #endregion

            #region columnNm = 列見出し
            string[] columnNm = new string[30];
            DataRow[] foundRows;
            string filter;
            string order;
            // 支給項目名を取得
            filter = "KOMOKU_SHUBETSU = 2";
            order = "KOMOKU_BANGO";
            foundRows = _dtKyuSti.Select(filter, order);
            //※名護仕様
            //foreach (DataRow dr in foundRows)
            //{
            //    // 項目番号＝1～6 の項目名をセット
            //    if (Util.ToInt(dr["KOMOKU_BANGO"]) >= 1 && Util.ToInt(dr["KOMOKU_BANGO"]) <= 6)
            //    {
            //        columnNm[Util.ToInt(dr["KOMOKU_BANGO"]) - 1] = Util.ToString(dr["KOMOKU_NM"]);
            //    }
            //}

            columnNm[0] = "基本給";
            columnNm[1] = "勤務手当";
            columnNm[2] = "通勤手当（非）";
            columnNm[3] = "住宅手当";
            columnNm[4] = "危険物手当";
            columnNm[5] = "早出手当";
            columnNm[6] = "その他支給額";
            columnNm[7] = "支給額合計";
            columnNm[8] = "課税対象額";
            columnNm[9] = "";
            // 控除項目名をセット
            columnNm[10] = "健康保険料";
            columnNm[11] = "厚生年金保険料";
            columnNm[12] = "雇用保険料";
            columnNm[13] = "社会保険料計";
            columnNm[14] = "所得税";
            columnNm[15] = "住民税";
            columnNm[16] = "その他控除額";
            columnNm[17] = "控除額合計";
            columnNm[18] = "差引支給額";
            columnNm[19] = "";
            // 勤怠項目名を取得
            filter = "KOMOKU_SHUBETSU = 1";
            order = "KOMOKU_BANGO";
            foundRows = _dtKyuSti.Select(filter, order);
            foreach (DataRow dr in foundRows)
            {
                // 項目番号＝1～10 の項目名をセット
                if (Util.ToInt(dr["KOMOKU_BANGO"]) >= 1 && Util.ToInt(dr["KOMOKU_BANGO"]) <= 10)
                {
                    columnNm[20 + Util.ToInt(dr["KOMOKU_BANGO"]) - 1] = Util.ToString(dr["KOMOKU_NM"]);
                }
            }
            #endregion

            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
            if (dtMainLoop.Rows.Count == 0)
            {
                return false;
            }
            else
            {
                _sort = 0;                                                                  // SORTフィールドカウンタ初期化
                _shainGokei = new decimal[20];

                foreach (DataRow dr in dtMainLoop.Rows)
                {
                    for (int i = 0; i < _shainGokei.Length; i++)                            // 社員合計用変数初期化
                    {
                        _shainGokei[i] = 0;
                    }

                    // TB年末調整データ取得
                    dpc = new DbParamCollection();
                    dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 2, this.UInfo.KaishaCd);
                    dpc.SetParam("@NENDO", SqlDbType.DateTime, nendo);
                    dpc.SetParam("@SHAIN_CD", SqlDbType.Decimal, 6, 0, Util.ToDecimal(dr["SHAIN_CD"]));
                    sql = new StringBuilder();
                    sql.Append("SELECT * FROM TB_KY_NENMATSU_CHOSEI");
                    sql.Append(" WHERE KAISHA_CD = @KAISHA_CD");
                    sql.Append(" AND NENDO = @NENDO");
                    sql.Append(" AND SHAIN_CD = @SHAIN_CD");
                    DataTable dtNenmatsuChosei = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
                    DataRow drNenmatsuChosei;
                    if (dtNenmatsuChosei.Rows.Count == 1)
                    {
                        drNenmatsuChosei = dtNenmatsuChosei.Rows[0];
                    }
                    else
                    {
                        drNenmatsuChosei = null;
                    }

                    // 給与セクションデータ出力
                    MakeWkDataKyuyoSection(dr, drNenmatsuChosei, columnNm, nendo);

                    // 賞与セクションデータ出力
                    MakeWkDataShoyoSection(dr, drNenmatsuChosei, columnNm, nendo);

                    // 該当区分セクションデータ出力
                    MakeWkDataGaitoSection(dr, drNenmatsuChosei, columnNm, nendo);

                    // 合計セクションデータ出力
                    MakeWkDataGokeiSection(dr, drNenmatsuChosei, columnNm, nendo);
                }
            }

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_KY_TBL",
                "GUID = @GUID",
                dpc);
            bool dataFlag;
            if (tmpdtPR_HN_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                dataFlag = false;
            }

            return dataFlag;
        }

        /// <summary>
        /// 給与セクションのワークデータ作成
        /// </summary>
        /// <param name="drShainJoho">社員情報DataRow</param>
        /// <param name="drNenmatsuChosei">年末調整DataRow</param>
        /// <param name="columnNm">列見出し</param>
        /// <param name="nendo">年度</param>
        private void MakeWkDataKyuyoSection(DataRow drShainJoho, DataRow drNenmatsuChosei, string[] columnNm, DateTime nendo)
        {
            string[] item = new string[3];
            decimal[] value = new decimal[30];
            decimal[] gokei = new decimal[30];

            ArrayList alParam;
            DbParamCollection dpc;

            DataTable dt;
            string s = "給与・手当等";
            for (int i = 0; i < 12; i++)
            {
                // 区分文字列
                if ( s.Length > 0 && (i % 2) == 1)      // 明細２行目から１行おきにセット
                {
                    item[0] = s.Substring(0, 1);
                    s = s.Substring(1);
                }
                else
                {
                    item[0] = "";
                }
                // 支給月数
                item[1] = Util.ToString(i + 1);

                dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 2, this.UInfo.KaishaCd);
                dpc.SetParam("@SHAIN_CD", SqlDbType.Decimal, 6, 0, Util.ToDecimal(drShainJoho["SHAIN_CD"]));
                dpc.SetParam("@NENGETSU", SqlDbType.DateTime, nendo.AddMonths(i));
                dt = this.Dba.GetDataTableFromSqlWithParams(GetKyKyuShoMeisaiSql(1, this.rdoKanpu2.Checked), dpc);
                if (dt.Rows.Count == 1)
                {
                    // 支給月日
                    item[2] = string.Format("{3}月{4}日", Util.ConvJpDate(Util.ToDate(dt.Rows[0]["SHIKYUBI"]), this.Dba));                   

                    for (int j = 0; j < 9; j++)
                    {
                        // 支給項目1-9
                        value[j] = Util.ToDecimal(dt.Rows[0]["SHIKYU_KOMOKU" + Util.ToString(j + 1)]);
                        gokei[j] += value[j];
                        // 控除項目1-9
                        value[j + 10] = Util.ToDecimal(dt.Rows[0]["KOJO_KOMOKU" + Util.ToString(j + 1)]);
                        gokei[j + 10] += value[j + 10];
                    }
                    for (int j = 0; j < 10; j++)
                    {
                        // 勤怠項目1-10
                        value[j + 20] = Util.ToDecimal(dt.Rows[0]["KINTAI_KOMOKU" + Util.ToString(j + 1)]);
                        gokei[j + 20] += value[j + 20];
                    }
                    // そのほか:(給与)扶養親族等の数
                    value[19] = Util.ToDecimal(Util.ToString(dt.Rows[0]["KOJO_KOMOKU31"]));
                }
                else
                {
                    // 支給月日
                    item[2] = "";

                    for (int j = 0; j < 30; j++)
                    {
                        value[j] = 0;
                    }
                }
                // 各月レコード出力
                alParam = SetPrKyTblParams_KyuSho(drShainJoho, drNenmatsuChosei, 1, nendo, columnNm, item, value);
                this.Dba.Insert("PR_KY_TBL", (DbParamCollection)alParam[0]);

            }
            // 合計レコード出力
            item[0] = "";
            item[1] = "";
            item[2] = "合計";
            alParam = SetPrKyTblParams_KyuSho(drShainJoho, drNenmatsuChosei, 1, nendo, columnNm, item, gokei);
            this.Dba.Insert("PR_KY_TBL", (DbParamCollection)alParam[0]);

            // 社員合計
            for (int i = 0; i < 20; i++)
            {
                _shainGokei[i] = gokei[i];
            }
        }

        /// <summary>
        /// 賞与セクションのワークデータ作成
        /// </summary>
        /// <param name="drShainJoho">社員情報DataRow</param>
        /// <param name="drNenmatsuChosei">年末調整DataRow</param>
        /// <param name="columnNm">列見出し</param>
        /// <param name="nendo">年度</param>
        private void MakeWkDataShoyoSection(DataRow drShainJoho, DataRow drNenmatsuChosei, string[] columnNm, DateTime nendo)
        {
            string[] item = new string[3];
            decimal[] value = new decimal[30];
            decimal[] gokei = new decimal[30];

            ArrayList alParam;
            DbParamCollection dpc;

            DataTable dt;
            int shikyuCount = 0;
            for (int i = 0; i < 12; i++)
            {
                dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 2, this.UInfo.KaishaCd);
                dpc.SetParam("@SHAIN_CD", SqlDbType.Decimal, 6, 0, Util.ToDecimal(drShainJoho["SHAIN_CD"]));
                dpc.SetParam("@NENGETSU", SqlDbType.DateTime, nendo.AddMonths(i));
                dt = this.Dba.GetDataTableFromSqlWithParams(GetKyKyuShoMeisaiSql(2, this.rdoKanpu2.Checked), dpc);
                if (dt.Rows.Count == 1)
                {
                    // 支給回数
                    item[1] = Util.ToString(++shikyuCount);
                    // 支給月日
                    item[2] = string.Format("{3}月{4}日", Util.ConvJpDate(Util.ToDate(dt.Rows[0]["SHIKYUBI"]), this.Dba));

                    for (int j = 0; j < 9; j++)
                    {
                        // 支給項目1-9
                        value[j] = Util.ToDecimal(dt.Rows[0]["SHIKYU_KOMOKU" + Util.ToString(j + 1)]);
                        gokei[j] += value[j];
                        // 控除項目1-9
                        value[j + 10] = Util.ToDecimal(dt.Rows[0]["KOJO_KOMOKU" + Util.ToString(j + 1)]);
                        gokei[j + 10] += value[j + 10];
                    }
                    // そのほか:(賞与)税率
                    value[19] = Util.ToDecimal(Util.ToString(dt.Rows[0]["KOJO_KOMOKU32"]));
                }
                else
                {
                    // 支給回数
                    item[1] = "";
                    // 支給月日
                    item[2] = "";

                    for (int j = 0; j < 30; j++)
                    {
                        value[j] = 0;
                    }
                }
                // 各月レコード出力(最大４件まで)
                if (Util.ToInt(item[1]) > 0 && shikyuCount <= 4)
                {
                    // 区分
                    item[0] = (shikyuCount == 1) ? "賞" : (shikyuCount == 3) ? "与" : "";

                    alParam = SetPrKyTblParams_KyuSho(drShainJoho, drNenmatsuChosei, 2, nendo, columnNm, item, value);
                    this.Dba.Insert("PR_KY_TBL", (DbParamCollection)alParam[0]);
                }
            }
            // 空白行レコード出力
            item[2] = "";
            for (int j = 0; j < 30; j++)
            {
                value[j] = 0;
            }
            while (shikyuCount < 4)
            {
                // 支給回数
                item[1] = Util.ToString(++shikyuCount);

                // 区分
                item[0] = (shikyuCount == 1) ? "賞" : (shikyuCount == 3) ? "与" : "";

                alParam = SetPrKyTblParams_KyuSho(drShainJoho, drNenmatsuChosei, 2, nendo, columnNm, item, value);
                this.Dba.Insert("PR_KY_TBL", (DbParamCollection)alParam[0]);
            }
            // 合計レコード出力
            item[0] = "等";
            item[1] = "";
            item[2] = "合計";
            alParam = SetPrKyTblParams_KyuSho(drShainJoho, drNenmatsuChosei, 2, nendo, columnNm, item, gokei);
            this.Dba.Insert("PR_KY_TBL", (DbParamCollection)alParam[0]);

            // 社員合計
            for (int i = 0; i < 20; i++)
            {
                _shainGokei[i] += gokei[i];
            }
        }

        /// <summary>
        /// 該当区分セクションのワークデータ作成
        /// </summary>
        /// <param name="drShainJoho">社員情報DataRow</param>
        /// <param name="drNenmatsuChosei">年末調整DataRow</param>
        /// <param name="columnNm">列見出し</param>
        /// <param name="nendo">年度</param>
        private void MakeWkDataGaitoSection(DataRow drShainJoho, DataRow drNenmatsuChosei, string[] columnNm, DateTime nendo)
        {
            string[] item = new string[3];
            decimal[] value = new decimal[30];

            // 合計レコード出力
            ArrayList alParam = SetPrKyTblParams_KyuSho(drShainJoho, drNenmatsuChosei, 3, nendo, columnNm, item, value);
            this.Dba.Insert("PR_KY_TBL", (DbParamCollection)alParam[0]);
        }

        /// <summary>
        /// 合計セクションのワークデータ作成
        /// </summary>
        /// <param name="drShainJoho">社員情報DataRow</param>
        /// <param name="drNenmatsuChosei">年末調整DataRow</param>
        /// <param name="columnNm">列見出し</param>
        /// <param name="nendo">年度</param>
        private void MakeWkDataGokeiSection(DataRow drShainJoho, DataRow drNenmatsuChosei, string[] columnNm, DateTime nendo)
        {
            string[] item = new string[3];
            decimal[] value = new decimal[30];

            // 合計レコード出力
            ArrayList alParam = SetPrKyTblParams_KyuSho(drShainJoho, drNenmatsuChosei, 4, nendo, columnNm, item, value);
            this.Dba.Insert("PR_KY_TBL", (DbParamCollection)alParam[0]);
        }

        /// <summary>
        /// 給与/賞与明細データ照会SQLの取得
        /// </summary>
        /// <param name="type">1給与 2賞与</param>
        /// <param name="optionKanpu">還付金反映フラグ</param>
        /// <returns></returns>
        private string GetKyKyuShoMeisaiSql(int type, bool optionKanpu)
        {
            string kyuSho = (type == 1) ? "KYUYO" : "SHOYO";

            DataTable dtSti = (type == 1) ? _dtKyuSti : _dtShoSti;

            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT K2.NENGETSU AS NENGETSU");
            sql.Append(" , K2.SHIKYUBI AS SHIKYUBI");

            DataRow[] foundRows;
            string filter;
            string order;

            #region 支給項目の照会式
            if (type == 1)
            {
                // 支給項目1-6
                sql.Append(", ISNULL(K2.KOMOKU1,0) AS SHIKYU_KOMOKU1");
                sql.Append(", ISNULL(K2.KOMOKU2,0) AS SHIKYU_KOMOKU2");
                sql.Append(", ISNULL(K2.KOMOKU3,0) AS SHIKYU_KOMOKU3");
                sql.Append(", ISNULL(K2.KOMOKU4,0) AS SHIKYU_KOMOKU4");
                sql.Append(", ISNULL(K2.KOMOKU5,0) AS SHIKYU_KOMOKU5");
                sql.Append(", ISNULL(K2.KOMOKU8,0) AS SHIKYU_KOMOKU6");

                // 支給項目7(その他支給額)
                sql.Append(", 0");
                for (int i = 9; i <= 20; i++)
                {
                    sql.Append(" + ISNULL(K2.KOMOKU" + Util.ToString(i) + " , 0)");
                }
                // 名護漁協田港さん指定により、項目を追加 kisemori 2016/05/19
                sql.Append(" + ISNULL(K2.KOMOKU7" + " , 0)");
                sql.Append(" AS SHIKYU_KOMOKU7");
            }
            else
            {
                // 支給項目1-6
                sql.Append(", ISNULL(K2.KOMOKU1,0) AS SHIKYU_KOMOKU1");
                sql.Append(", 0 AS SHIKYU_KOMOKU2");
                sql.Append(", 0 AS SHIKYU_KOMOKU3");
                sql.Append(", 0 AS SHIKYU_KOMOKU4");
                sql.Append(", 0 AS SHIKYU_KOMOKU5");
                sql.Append(", 0 AS SHIKYU_KOMOKU6");
                // 支給項目7(その他支給額)
                sql.Append(", 0");
                for (int i = 2; i <= 20; i++)
                {
                    sql.Append(" + ISNULL(K2.KOMOKU" + Util.ToString(i) + " , 0)");
                }
                sql.Append(" AS SHIKYU_KOMOKU7");
            }
            // 支給項目8(支給額合計)※名護仕様KOMOKU7の値が不明なため省く
            sql.Append(", 0");
            for (int i = 1; i <= 6; i++)
            {
                sql.Append(" + ISNULL(K2.KOMOKU" + Util.ToString(i) + " , 0)");
            }
            for (int i = 8; i <= 20; i++)
            {
                sql.Append(" + ISNULL(K2.KOMOKU" + Util.ToString(i) + " , 0)");
            }
            sql.Append(" AS SHIKYU_KOMOKU8");
            // 支給項目9(所得税課税額合計)
            filter = "KOMOKU_SHUBETSU = 2 AND KOMOKU_KUBUN1 = 1";
            order = "KOMOKU_BANGO";
            foundRows = dtSti.Select(filter, order);
            sql.Append(", 0");
            foreach (DataRow dr in foundRows)
            {
                sql.Append(" + ISNULL(K2.KOMOKU" + Util.ToString(dr["KOMOKU_BANGO"]) + " , 0)");
            }
            sql.Append(" AS SHIKYU_KOMOKU9");
            #endregion

            #region 控除項目の照会式
            // 控除項目1(健康保険料)
            filter = "KOMOKU_SHUBETSU = 3 AND KOMOKU_KUBUN3 = 31";
            order = "KOMOKU_BANGO";
            foundRows = dtSti.Select(filter, order);
            sql.Append(", 0");
            foreach (DataRow dr in foundRows)
            {
                sql.Append(" + ISNULL(K3.KOMOKU" + Util.ToString(dr["KOMOKU_BANGO"]) + " , 0)");
            }
            sql.Append(" AS KOJO_KOMOKU1");
            // 控除項目2(厚生年金保険料)
            filter = "KOMOKU_SHUBETSU = 3 AND KOMOKU_KUBUN3 = 32";
            order = "KOMOKU_BANGO";
            foundRows = dtSti.Select(filter, order);
            sql.Append(", 0");
            foreach (DataRow dr in foundRows)
            {
                sql.Append(" + ISNULL(K3.KOMOKU" + Util.ToString(dr["KOMOKU_BANGO"]) + " , 0)");
            }
            sql.Append(" AS KOJO_KOMOKU2");
            // 控除項目3(雇用保険料)
            filter = "KOMOKU_SHUBETSU = 3 AND KOMOKU_KUBUN3 = 33";
            order = "KOMOKU_BANGO";
            foundRows = dtSti.Select(filter, order);
            sql.Append(", 0");
            foreach (DataRow dr in foundRows)
            {
                sql.Append(" + ISNULL(K3.KOMOKU" + Util.ToString(dr["KOMOKU_BANGO"]) + " , 0)");
            }
            sql.Append(" AS KOJO_KOMOKU3");
            // 控除項目4(社会保険料等)
            filter = "KOMOKU_SHUBETSU = 3 AND KOMOKU_KUBUN3 >= 31 AND KOMOKU_KUBUN3 <= 33";
            order = "KOMOKU_BANGO";
            foundRows = dtSti.Select(filter, order);
            sql.Append(", 0");
            foreach (DataRow dr in foundRows)
            {
                sql.Append(" + ISNULL(K3.KOMOKU" + Util.ToString(dr["KOMOKU_BANGO"]) + " , 0)");
            }
            sql.Append(" AS KOJO_KOMOKU4");
            // 控除項目5(所得税)
            if (optionKanpu)
            {
                filter = "KOMOKU_SHUBETSU = 3 AND (KOMOKU_KUBUN3 = 34 OR KOMOKU_KUBUN3 = 36)";               // 調整所得税を含む
            }
            else
            {
                filter = "KOMOKU_SHUBETSU = 3 AND KOMOKU_KUBUN3 = 34";
            }
            order = "KOMOKU_BANGO";
            foundRows = dtSti.Select(filter, order);
            sql.Append(", 0");
            foreach (DataRow dr in foundRows)
            {
                sql.Append(" + ISNULL(K3.KOMOKU" + Util.ToString(dr["KOMOKU_BANGO"]) + " , 0)");
            }
            sql.Append(" AS KOJO_KOMOKU5");
            // 控除項目6(住民税)
            filter = "KOMOKU_SHUBETSU = 3 AND KOMOKU_KUBUN3 = 35";
            order = "KOMOKU_BANGO";
            foundRows = dtSti.Select(filter, order);
            sql.Append(", 0");
            foreach (DataRow dr in foundRows)
            {
                sql.Append(" + ISNULL(K3.KOMOKU" + Util.ToString(dr["KOMOKU_BANGO"]) + " , 0)");
            }
            sql.Append(" AS KOJO_KOMOKU6");
            // 控除項目7(控除項目その他)
            filter = "KOMOKU_SHUBETSU = 3 AND KOMOKU_KUBUN3 = 0";
            order = "KOMOKU_BANGO";
            foundRows = dtSti.Select(filter, order);
            sql.Append(", 0");
            foreach (DataRow dr in foundRows)
            {
                sql.Append(" + ISNULL(K3.KOMOKU" + Util.ToString(dr["KOMOKU_BANGO"]) + " , 0)");
            }
            sql.Append(" AS KOJO_KOMOKU7");
            // 控除項目8(控除額合計)
            if (optionKanpu)
            {
                filter = "KOMOKU_SHUBETSU = 3";
            }
            else
            {
                filter = "KOMOKU_SHUBETSU = 3 AND KOMOKU_KUBUN3 <> 36";                                     // 調整所得税を除く
            }
            order = "KOMOKU_BANGO";
            foundRows = dtSti.Select(filter, order);
            sql.Append(", 0");
            foreach (DataRow dr in foundRows)
            {
                sql.Append(" + ISNULL(K3.KOMOKU" + Util.ToString(dr["KOMOKU_BANGO"]) + " , 0)");
            }
            sql.Append(" AS KOJO_KOMOKU8");
            // 控除項目9(差引支給額)KOMOKU7の値が不明
            sql.Append(", 0");
            for (int i = 1; i <= 6; i++)
            {
                sql.Append(" + ISNULL(K2.KOMOKU" + Util.ToString(i) + " , 0)");
            } 
            for (int i = 8; i <= 20; i++)
            {
                sql.Append(" + ISNULL(K2.KOMOKU" + Util.ToString(i) + " , 0)");
            }
            if (optionKanpu)
            {
                filter = "KOMOKU_SHUBETSU = 3";
            }
            else
            {
                filter = "KOMOKU_SHUBETSU = 3 AND KOMOKU_KUBUN3 <> 36";                                     // 調整所得税を除く
            }
            order = "KOMOKU_BANGO";
            foundRows = dtSti.Select(filter, order);
            foreach (DataRow dr in foundRows)
            {
                sql.Append(" - ISNULL(K3.KOMOKU" + Util.ToString(dr["KOMOKU_BANGO"]) + " , 0)");
            }
            sql.Append(" AS KOJO_KOMOKU9");
            #endregion

            // 勤怠項目
            for (int i = 1; i <= 20; i++)
            {
                sql.Append(", ISNULL(K1.KOMOKU" + Util.ToString(i) + " , 0) AS KINTAI_KOMOKU" + Util.ToString(i));
            }

            // そのほか
            sql.Append(", K3.KOMOKU31 AS KOJO_KOMOKU31");       // (給与)扶養親族等の数
            sql.Append(", K3.KOMOKU32 AS KOJO_KOMOKU32");       // (賞与)利用した税率
            sql.Append(", K3.KOMOKU33 AS KOJO_KOMOKU33");       // (賞与)前月分の給与額
            sql.Append(", K3.KOMOKU34 AS KOJO_KOMOKU34");       // (賞与)賞与計算期間

            sql.Append(" FROM");
            sql.Append(" TB_KY_" + kyuSho + "_MEISAI_SHIKYU AS K2");
            sql.Append(" LEFT OUTER JOIN TB_KY_" + kyuSho + "_MEISAI_KOJO AS K3");
            sql.Append(" ON (K2.KAISHA_CD = K3.KAISHA_CD)");
            sql.Append(" AND (K2.SHAIN_CD = K3.SHAIN_CD)");
            sql.Append(" AND (K2.NENGETSU = K3.NENGETSU)");
            sql.Append(" LEFT OUTER JOIN TB_KY_" + kyuSho + "_MEISAI_KINTAI AS K1");
            sql.Append(" ON (K2.KAISHA_CD = K1.KAISHA_CD)");
            sql.Append(" AND (K2.SHAIN_CD = K1.SHAIN_CD)");
            sql.Append(" AND (K2.NENGETSU = K1.NENGETSU)");
            sql.Append(" WHERE K2.KAISHA_CD = @KAISHA_CD");
            sql.Append(" AND K2.SHAIN_CD = @SHAIN_CD");
            sql.Append(" AND K2.NENGETSU = @NENGETSU");

            return Util.ToString(sql);
        }

        /// <summary>
        /// PR_KY_TBLに更新するためのパラメータ設定をします。
        /// </summary>
        /// <param name="drShainJoho">社員情報DataRow</param>
        /// <param name="drNenmatsuChosei">年末調整DataRow</param>
        /// <param name="type">1給与レコード 2賞与レコード 3該当区分レコード 4合計レコード</param>
        /// <param name="nendo">年度</param>
        /// <param name="columnNm">列見出し</param>
        /// <param name="item">明細文字情報</param>
        /// <param name="value">明細数値情報</param>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// </returns>
        private ArrayList SetPrKyTblParams_KyuSho(DataRow drShainJoho, DataRow drNenmatsuChosei,
            int type, DateTime nendo, string[] columnNm, string[] item, decimal[] value)
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            ++_sort;

            // 制御情報
            updParam.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            updParam.SetParam("@SORT", SqlDbType.Int, _sort);
            updParam.SetParam("@ITEM001", SqlDbType.VarChar, 200, string.Format("{0:D6}", Util.ToInt(drShainJoho["SHAIN_CD"])));
            updParam.SetParam("@ITEM002", SqlDbType.VarChar, 200, Util.ToString(type));

            #region 共通情報
            updParam.SetParam("@ITEM010", SqlDbType.VarChar, 200, 
                string.Format("{0} {2}年分 所得税源泉徴収簿 兼 賃金台帳", Util.ConvJpDate(nendo, this.Dba)));
            updParam.SetParam("@ITEM011", SqlDbType.VarChar, 200, Util.ToString(drShainJoho["SHAIN_CD"]));      
            updParam.SetParam("@ITEM012", SqlDbType.VarChar, 200, Util.ToString(drShainJoho["SHAIN_NM"]));       
            updParam.SetParam("@ITEM013", SqlDbType.VarChar, 200, Util.ToString(drShainJoho["SHAIN_KANA_NM"])); 
            updParam.SetParam("@ITEM014", SqlDbType.VarChar, 200,
                Util.ToString(drShainJoho["BUMON_NM"]) + Util.ToString(drShainJoho["BUKA_NM"]));
            if (ValChk.IsEmpty(drShainJoho["SEINENGAPPI"]))
            {
                updParam.SetParam("@ITEM015", SqlDbType.VarChar, 200, "生年月日 　　  年   月   日");
            }
            else
            {
                updParam.SetParam("@ITEM015", SqlDbType.VarChar, 200, string.Format("生年月日 {0}{2,2}年 {3,2}月 {4,2}日"
                    , Util.ConvJpDate(Util.ToDate(drShainJoho["SEINENGAPPI"]), this.Dba)));
            }
            if(Util.ToInt(drShainJoho["SEIBETSU"]) == 1)
            {
                updParam.SetParam("@ITEM016", SqlDbType.VarChar, 200, "＊");
            }
            else
            {
                updParam.SetParam("@ITEM017", SqlDbType.VarChar, 200, "＊");
            }
            if (ValChk.IsEmpty(drShainJoho["NYUSHA_NENGAPPI"]))
            {
                updParam.SetParam("@ITEM018", SqlDbType.VarChar, 200, "入 社 日 　　  年   月   日");
            }
            else
            {
                updParam.SetParam("@ITEM018", SqlDbType.VarChar, 200, string.Format("入 社 日 {0}{2,2}年 {3,2}月 {4,2}日"
                    , Util.ConvJpDate(Util.ToDate(drShainJoho["NYUSHA_NENGAPPI"]), this.Dba)));
            }
            if (ValChk.IsEmpty(drShainJoho["TAISHOKU_NENGAPPI"]))
            {
                updParam.SetParam("@ITEM019", SqlDbType.VarChar, 200, "退 職 日 　　  年   月   日");
            }
            else
            {
                updParam.SetParam("@ITEM019", SqlDbType.VarChar, 200, string.Format("退 職 日{0} {2,2}年 {3,2}月 {4,2}日"
                    , Util.ConvJpDate(Util.ToDate(drShainJoho["TAISHOKU_NENGAPPI"]), this.Dba)));
            }
            updParam.SetParam("@ITEM020", SqlDbType.VarChar, 200, Util.ToString(drShainJoho["YUBIN_BANGO1"]));
            updParam.SetParam("@ITEM021", SqlDbType.VarChar, 200, Util.ToString(drShainJoho["YUBIN_BANGO2"]));
            updParam.SetParam("@ITEM022", SqlDbType.VarChar, 200, Util.ToString(drShainJoho["JUSHO1"]));
            updParam.SetParam("@ITEM023", SqlDbType.VarChar, 200, Util.ToString(drShainJoho["JUSHO2"]));

            // 支給項目列見出し
            for (int i = 0; i < 9; i++)
            {
                updParam.SetParam("@ITEM0" + Util.ToString(i + 31), SqlDbType.VarChar, 200, columnNm[i]);
            }
            // 控除項目列見出し
            for (int i = 0; i < 9; i++)
            {
                updParam.SetParam("@ITEM0" + Util.ToString(i + 41), SqlDbType.VarChar, 200, columnNm[i + 10]);
            }
            // 勤怠項目列見出し
            for (int i = 0; i < 10; i++)
            {
                updParam.SetParam("@ITEM0" + Util.ToString(i + 51), SqlDbType.VarChar, 200, columnNm[i + 20]);
            }
            #endregion

            // ワークレコードタイプ別パラメータ設定
            switch (type)
            {
                case 1:
                    #region 給与レコードの時
                    // 明細文字情報
                    updParam.SetParam("@ITEM101", SqlDbType.VarChar, 200, item[0]);
                    updParam.SetParam("@ITEM102", SqlDbType.VarChar, 200, item[1]);
                    updParam.SetParam("@ITEM103", SqlDbType.VarChar, 200, item[2]);

                    // 明細数値情報
                    for (int i = 0; i < 20; i++)
                    {
                        updParam.SetParam("@ITEM" + Util.ToString(i + 111), SqlDbType.VarChar, 200, this.FormatNum(value[i]));
                    }
                    for (int i = 0; i < 10; i++)
                    {
                        if (value[i + 20] != 0)
                        {
                            updParam.SetParam("@ITEM" + Util.ToString(i + 131), SqlDbType.VarChar, 200, Util.ToString(value[i + 20]));
                        }
                    }
                    #endregion
                    break;
                case 2:
                    #region 賞与レコードの時
                    // 明細文字情報
                    updParam.SetParam("@ITEM101", SqlDbType.VarChar, 200, item[0]);
                    updParam.SetParam("@ITEM102", SqlDbType.VarChar, 200, item[1]);
                    updParam.SetParam("@ITEM103", SqlDbType.VarChar, 200, item[2]);

                    // 明細数値情報
                    for (int i = 0; i < 20; i++)
                    {
                        updParam.SetParam("@ITEM" + Util.ToString(i + 111), SqlDbType.VarChar, 200, this.FormatNum(value[i]));
                    }
                    for (int i = 0; i < 10; i++)
                    {
                        if (value[i + 20] != 0)
                        {
                            updParam.SetParam("@ITEM" + Util.ToString(i + 131), SqlDbType.VarChar, 200, Util.ToString(value[i + 20]));
                        }
                    }
                    #endregion
                    break;
                case 3:
                    #region 該当区分レコードの時
                    if (drNenmatsuChosei != null)
                    {
                        updParam.SetParam("@ITEM102", SqlDbType.VarChar, 200, item[1]);
                        updParam.SetParam("@ITEM111", SqlDbType.VarChar, 200                                // 税額表甲
                            , (Util.ToInt(drNenmatsuChosei["ZEIGAKU_HYO"]) == 1) ? "＊" : "");
                        updParam.SetParam("@ITEM112", SqlDbType.VarChar, 200                                // 税額表乙
                            , (Util.ToInt(drNenmatsuChosei["ZEIGAKU_HYO"]) == 2) ? "＊" : "");
                        updParam.SetParam("@ITEM113", SqlDbType.VarChar, 200                                // 控除対象配偶者有
                            , (Util.ToInt(drNenmatsuChosei["HAIGUSHA_KUBUN"]) == 1) ? "＊" : "");
                        updParam.SetParam("@ITEM114", SqlDbType.VarChar, 200                                // 控除対象配偶者無
                            , (Util.ToInt(drNenmatsuChosei["HAIGUSHA_KUBUN"]) == 0) ? "＊" : "");
                        updParam.SetParam("@ITEM115", SqlDbType.VarChar, 200                                // 控除対象配偶者老人
                            , (Util.ToInt(drNenmatsuChosei["HAIGUSHA_KUBUN"]) == 2) ? "＊" : "");

                        updParam.SetParam("@ITEM116", SqlDbType.VarChar, 200                                // 扶養親族数・特定
                            , this.FormatNum(Util.ToInt(drNenmatsuChosei["FUYO_TOKUTEI"])));
                        updParam.SetParam("@ITEM117", SqlDbType.VarChar, 200                                // 扶養親族数・老人同居
                            , this.FormatNum(Util.ToInt(drNenmatsuChosei["FUYO_ROZIN_DOKYO_ROSHINTO"])));
                        updParam.SetParam("@ITEM118", SqlDbType.VarChar, 200                                // 扶養親族数・老人計
                            , this.FormatNum( Util.ToInt(drNenmatsuChosei["FUYO_ROZIN_DOKYO_ROSHINTO"])
                                + Util.ToInt(drNenmatsuChosei["FUYO_ROZIN_DOKYO_ROSHINTO_IGAI"])));
                        updParam.SetParam("@ITEM119", SqlDbType.VarChar, 200                                // 扶養親族数・その他
                            , this.FormatNum(Util.ToInt(drNenmatsuChosei["FUYO_IPPAN"])
                                - Util.ToInt(drNenmatsuChosei["FUYO_TOKUTEI"])
                                - Util.ToInt(drNenmatsuChosei["FUYO_ROZIN_DOKYO_ROSHINTO"])
                                - Util.ToInt(drNenmatsuChosei["FUYO_ROZIN_DOKYO_ROSHINTO_IGAI"])));
                        updParam.SetParam("@ITEM120", SqlDbType.VarChar, 200                                // 障害者数・特別同居
                            , this.FormatNum(Util.ToInt(drNenmatsuChosei["FUYO_DOKYO_TOKUSHO_IPPAN"])));
                        updParam.SetParam("@ITEM121", SqlDbType.VarChar, 200                                // 障害者数・特別計
                            , this.FormatNum(Util.ToInt(drNenmatsuChosei["FUYO_DOKYO_TOKUSHO_IPPAN"])
                                + Util.ToInt(drNenmatsuChosei["SHOGAISHA_TOKUBETSU"])));
                        updParam.SetParam("@ITEM122", SqlDbType.VarChar, 200                                // 障害者数・その他
                            , this.FormatNum(Util.ToInt(drNenmatsuChosei["SHOGAISHA_IPPAN"])));
                        updParam.SetParam("@ITEM123", SqlDbType.VarChar, 200                                // 16歳未満
                            , this.FormatNum(Util.ToInt(drNenmatsuChosei["SAI16_MIMAN"])));

                        updParam.SetParam("@ITEM124", SqlDbType.VarChar, 200                                // 本人・未成年者
                            , (Util.ToInt(drNenmatsuChosei["MISEINENSHA"]) == 1) ? "＊" : "");
                        updParam.SetParam("@ITEM125", SqlDbType.VarChar, 200                                // 本人・障害者特別
                            , (Util.ToInt(drNenmatsuChosei["HONNIN_KUBUN2"]) == 2) ? "＊" : "");
                        updParam.SetParam("@ITEM126", SqlDbType.VarChar, 200                                // 本人・障害者その他
                            , (Util.ToInt(drNenmatsuChosei["HONNIN_KUBUN2"]) == 1) ? "＊" : "");
                        updParam.SetParam("@ITEM128", SqlDbType.VarChar, 200                                // 本人・寡婦一般
                            , (Util.ToInt(drNenmatsuChosei["HONNIN_KUBUN1"]) == 2) ? "＊" : "");
                        updParam.SetParam("@ITEM129", SqlDbType.VarChar, 200                                // 本人・寡婦特別
                            , (Util.ToInt(drNenmatsuChosei["HONNIN_KUBUN1"]) == 3) ? "＊" : "");
                        updParam.SetParam("@ITEM130", SqlDbType.VarChar, 200                                // 本人・寡夫
                            , (Util.ToInt(drNenmatsuChosei["HONNIN_KUBUN1"]) == 4) ? "＊" : "");
                        updParam.SetParam("@ITEM131", SqlDbType.VarChar, 200                                // 本人・勤労学生
                            , (Util.ToInt(drNenmatsuChosei["HONNIN_KUBUN3"]) == 1) ? "＊" : "");
                        updParam.SetParam("@ITEM132", SqlDbType.VarChar, 200                                // 本人・死亡退職
                            , (Util.ToInt(drNenmatsuChosei["SHIBO_TAISHOKU"]) == 1) ? "＊" : "");
                        updParam.SetParam("@ITEM133", SqlDbType.VarChar, 200                                // 本人・災害者
                            , (Util.ToInt(drNenmatsuChosei["SAIGAISHA"]) == 1) ? "＊" : "");
                        updParam.SetParam("@ITEM134", SqlDbType.VarChar, 200                                // 本人・外国人
                            , (Util.ToInt(drNenmatsuChosei["GAIKOKUJIN"]) == 1) ? "＊" : "");
                    }
                    #endregion
                    break;
                case 4:
                    #region 合計レコードの時
                    if (drNenmatsuChosei != null)
                    {
                        updParam.SetParam("@ITEM102", SqlDbType.VarChar, 200,
                            this.FormatNum(Util.ToDecimal(drNenmatsuChosei["ZENSHOKUBUN_SHAKAI_HOKENRYO"])));       // 前職分社会保険料
                        updParam.SetParam("@ITEM103", SqlDbType.VarChar, 200,
                            this.FormatNum(Util.ToDecimal(drNenmatsuChosei["ZENSHOKUBUN_SHOTOKUZEIGAKU"])));        // 　　　所得税
                        updParam.SetParam("@ITEM104", SqlDbType.VarChar, 200,
                            this.FormatNum(Util.ToDecimal(drNenmatsuChosei["ZENSHOKUBUN_KAZEI_KYUYOGAKU"])));       // 　　　課税給与額

                        for (int i = 0; i < 20; i++)
                        {
                            updParam.SetParam("@ITEM" + Util.ToString(i + 111), SqlDbType.VarChar, 200,
                                this.FormatNum(Util.ToDecimal(_shainGokei[i])));                                    // 給与・賞与の支給控除明細合計
                        }

                        updParam.SetParam("@ITEM131", SqlDbType.VarChar, 200,
                            this.FormatNum(Util.ToDecimal(drNenmatsuChosei["KYUYOTO_KAZEI_KINGAKU"])                // 給料・手当て等
                                            + Util.ToDecimal(drNenmatsuChosei["ZENSHOKUBUN_KAZEI_KYUYOGAKU"])
                                            + Util.ToDecimal(drNenmatsuChosei["CHOSEI_KYUYO_KAZEI_KYUYOGAKU"])));             
                        updParam.SetParam("@ITEM132", SqlDbType.VarChar, 200,
                            this.FormatNum(Util.ToDecimal(drNenmatsuChosei["SHOYOTO_KAZEI_KINGAKU"])                // 賞与等
                                            + Util.ToDecimal(drNenmatsuChosei["CHOSEI_SHOYO_KAZEI_KYUYOGAKU"])));   
                        updParam.SetParam("@ITEM133", SqlDbType.VarChar, 200,
                            this.FormatNum(Util.ToDecimal(drNenmatsuChosei["KYUYOTO_KAZEI_KINGAKU"])                // 給料賞与等合計
                                            + Util.ToDecimal(drNenmatsuChosei["SHOYOTO_KAZEI_KINGAKU"])
                                            + Util.ToDecimal(drNenmatsuChosei["ZENSHOKUBUN_KAZEI_KYUYOGAKU"])
                                            + Util.ToDecimal(drNenmatsuChosei["CHOSEI_KYUYO_KAZEI_KYUYOGAKU"])       
                                            + Util.ToDecimal(drNenmatsuChosei["CHOSEI_SHOYO_KAZEI_KYUYOGAKU"])));   
                        updParam.SetParam("@ITEM134", SqlDbType.VarChar, 200,
                            this.FormatNum(Util.ToDecimal(drNenmatsuChosei["KYUYO_SHOTOKU_KOJOGO_NO_KNGK"])));      // 給与所得控除後の給与等の額
                        updParam.SetParam("@ITEM135", SqlDbType.VarChar, 200,
                            this.FormatNum(Util.ToDecimal(drNenmatsuChosei["SHAKAI_HOKENRYO_KJGK_KYUYOTO"])         // 社会保険料控除額(給与からの控除分)
                                            + Util.ToDecimal(drNenmatsuChosei["ZENSHOKUBUN_SHAKAI_HOKENRYO"])
                                            + Util.ToDecimal(drNenmatsuChosei["CHOSEI_KYUYO_SHAKAI_HOKENRYO"])
                                            + Util.ToDecimal(drNenmatsuChosei["CHOSEI_SHOYO_SHAKAI_HOKENRYO"])));
                        updParam.SetParam("@ITEM136", SqlDbType.VarChar, 200,
                            this.FormatNum(Util.ToDecimal(drNenmatsuChosei["SHAKAI_HOKENRYO_KJGK_SNKKBN"])));       // 社会保険料控除額(申告分)
                        updParam.SetParam("@ITEM137", SqlDbType.VarChar, 200,
                            this.FormatNum(Util.ToDecimal(drNenmatsuChosei["SKB_KIGYO_KYOSAI_KAKEKIN_KJGK"])));     // 小規模共済等掛金の控除額
                        updParam.SetParam("@ITEM138", SqlDbType.VarChar, 200,
                            this.FormatNum(Util.ToDecimal(drNenmatsuChosei["SEIMEIHOKENRYO_KOJOGAKU"])));           // 生命保険料控除額
                        updParam.SetParam("@ITEM139", SqlDbType.VarChar, 200,
                            this.FormatNum(Util.ToDecimal(drNenmatsuChosei["SONGAIHOKENRYO_KOJOGAKU"])));           // 地震保険料控除額
                        updParam.SetParam("@ITEM140", SqlDbType.VarChar, 200,
                            this.FormatNum(Util.ToDecimal(drNenmatsuChosei["HAIGUSHA_TOKUBETSU_KOJOGAKU"])));       // 配偶者特別控除額
                        updParam.SetParam("@ITEM141", SqlDbType.VarChar, 200,
                            this.FormatNum(Util.ToDecimal(drNenmatsuChosei["HAIGUSHA_KOJOGAKU"])                    // 配・扶・基及び障害等の控除額の合計
                                    + Util.ToDecimal(drNenmatsuChosei["FUYO_KOJOGAKU"])
                                    + Util.ToDecimal(drNenmatsuChosei["KISO_KOJOGAKU"])
                                    + Util.ToDecimal(drNenmatsuChosei["SHOGAISHA_KOJOGAKU"])
                                    + Util.ToDecimal(drNenmatsuChosei["RONENSHA_KAFU_KOJOGAKU"])
                                    + Util.ToDecimal(drNenmatsuChosei["KINRO_GAKUSEI_KOJOGAKU"])));
                        updParam.SetParam("@ITEM142", SqlDbType.VarChar, 200,
                            this.FormatNum(Util.ToDecimal(drNenmatsuChosei["SHAKAI_HOKENRYO_KJGK_KYUYOTO"])         // 所得控除額の合計額
                                    + Util.ToDecimal(drNenmatsuChosei["ZENSHOKUBUN_SHAKAI_HOKENRYO"])
                                    + Util.ToDecimal(drNenmatsuChosei["CHOSEI_KYUYO_SHAKAI_HOKENRYO"])
                                    + Util.ToDecimal(drNenmatsuChosei["CHOSEI_SHOYO_SHAKAI_HOKENRYO"])
                                    + Util.ToDecimal(drNenmatsuChosei["SHAKAI_HOKENRYO_KJGK_SNKKBN"])
                                    + Util.ToDecimal(drNenmatsuChosei["SKB_KIGYO_KYOSAI_KAKEKIN_KJGK"])
                                    + Util.ToDecimal(drNenmatsuChosei["SEIMEIHOKENRYO_KOJOGAKU"])
                                    + Util.ToDecimal(drNenmatsuChosei["SONGAIHOKENRYO_KOJOGAKU"])
                                    + Util.ToDecimal(drNenmatsuChosei["HAIGUSHA_TOKUBETSU_KOJOGAKU"])
                                    + Util.ToDecimal(drNenmatsuChosei["HAIGUSHA_KOJOGAKU"])
                                    + Util.ToDecimal(drNenmatsuChosei["FUYO_KOJOGAKU"])
                                    + Util.ToDecimal(drNenmatsuChosei["KISO_KOJOGAKU"])
                                    + Util.ToDecimal(drNenmatsuChosei["SHOGAISHA_KOJOGAKU"])
                                    + Util.ToDecimal(drNenmatsuChosei["RONENSHA_KAFU_KOJOGAKU"])
                                    + Util.ToDecimal(drNenmatsuChosei["KINRO_GAKUSEI_KOJOGAKU"])));
                        updParam.SetParam("@ITEM143", SqlDbType.VarChar, 200,
                            this.FormatNum(Util.ToDecimal(drNenmatsuChosei["KAZEI_KYUYO_SHOTOKU_KINGAKU"])));       // 差引課税給与所得金額(千円未満切捨て)
                        updParam.SetParam("@ITEM144", SqlDbType.VarChar, 200,
                            this.FormatNum(Util.ToDecimal(drNenmatsuChosei["SANSHUTSU_NENZEIGAKU"])));              // 年税額
                        //updParam.SetParam("@ITEM145", SqlDbType.VarChar, 200,
                        //    this.FormatNum(Util.ToDecimal(drNenmatsuChosei["JUTAKU_SHUTOKUTO_TKBT_KOJOGAKU"])));    // 住宅借入金等特別控除額
                        updParam.SetParam("@ITEM145", SqlDbType.VarChar, 200,                               // 住宅借入金等特別控除額(実控除額)
                            this.FormatNum(Util.ToDecimal(drNenmatsuChosei["SANSHUTSU_NENZEIGAKU"])
                                            - Util.ToDecimal(drNenmatsuChosei["NENCHO_NENZEIGAKU"])));    
                        updParam.SetParam("@ITEM146", SqlDbType.VarChar, 200,
                            this.FormatNum(Util.ToDecimal(drNenmatsuChosei["NENCHO_NENZEIGAKU"])));                 // 差引年税額
                        // value[46]
                        updParam.SetParam("@ITEM148", SqlDbType.VarChar, 200,
                            this.FormatNum(Util.ToDecimal(drNenmatsuChosei["NENZEIGAKU"])));                        // 徴収年税額
                        updParam.SetParam("@ITEM149", SqlDbType.VarChar, 200,
                            this.FormatNum(Util.ToDecimal(drNenmatsuChosei["ZENSHOKUBUN_SHOTOKUZEIGAKU"])           // 徴収税額
                                    + Util.ToDecimal(drNenmatsuChosei["CHOSEI_KYUYO_SHOTOKUZEIGAKU"])
                                    + Util.ToDecimal(drNenmatsuChosei["CHOSEI_SHOYO_SHOTOKUZEIGAKU"])
                                    + Util.ToDecimal(drNenmatsuChosei["KYUYOTO_SHOTOKUZEIGAKU"])
                                    + Util.ToDecimal(drNenmatsuChosei["SHOYOTO_SHOTOKUZEIGAKU"])));
                        updParam.SetParam("@ITEM150", SqlDbType.VarChar, 200,
                            this.FormatNum(Util.ToDecimal(drNenmatsuChosei["NENZEIGAKU"])                           // 差引過不足税額
                                    - Util.ToDecimal(drNenmatsuChosei["ZENSHOKUBUN_SHOTOKUZEIGAKU"])
                                    - Util.ToDecimal(drNenmatsuChosei["CHOSEI_KYUYO_SHOTOKUZEIGAKU"])
                                    - Util.ToDecimal(drNenmatsuChosei["CHOSEI_SHOYO_SHOTOKUZEIGAKU"])
                                    - Util.ToDecimal(drNenmatsuChosei["KYUYOTO_SHOTOKUZEIGAKU"])
                                    - Util.ToDecimal(drNenmatsuChosei["SHOYOTO_SHOTOKUZEIGAKU"])));
                        updParam.SetParam("@ITEM151", SqlDbType.VarChar, 200,
                            this.FormatNum(Util.ToDecimal(drNenmatsuChosei["HAIGUSHA_GOKEI_SHOTOKU"])));            // 配偶者の合計所得
                        updParam.SetParam("@ITEM152", SqlDbType.VarChar, 200,
                            this.FormatNum(Util.ToDecimal(drNenmatsuChosei["SHIN_SEIMEI_HOKENRYO"])));              // 新生命保険料
                        updParam.SetParam("@ITEM153", SqlDbType.VarChar, 200,
                            this.FormatNum(Util.ToDecimal(drNenmatsuChosei["KYU_SEIMEI_HOKENRYO"])));               // 旧生命保険料
                        updParam.SetParam("@ITEM154", SqlDbType.VarChar, 200,
                            this.FormatNum(Util.ToDecimal(drNenmatsuChosei["KAIGO_IRYO_HOKENRYO"])));               // 介護医療保険料
                        updParam.SetParam("@ITEM155", SqlDbType.VarChar, 200,
                            this.FormatNum(Util.ToDecimal(drNenmatsuChosei["SHIN_KOJIN_NENKIN_HOKENRYO"])));        // 新個人年金保険料
                        updParam.SetParam("@ITEM156", SqlDbType.VarChar, 200,
                            this.FormatNum(Util.ToDecimal(drNenmatsuChosei["KOJIN_NENKIN_HOKENRYO_SHRIGK"])));      // 旧個人年金保険料
                        updParam.SetParam("@ITEM157", SqlDbType.VarChar, 200,
                            this.FormatNum(Util.ToDecimal(drNenmatsuChosei["CHOKI_SONGAI_HOKENRYO_SHRIGK"])));      // 旧長期損害保険料
                        updParam.SetParam("@ITEM160", SqlDbType.VarChar, 200,
                            this.FormatNum(Util.ToDecimal(drNenmatsuChosei["CHOSEI_KYUYO_KAZEI_KYUYOGAKU"])));      // 調整給与課税対象額
                        updParam.SetParam("@ITEM161", SqlDbType.VarChar, 200,
                            this.FormatNum(Util.ToDecimal(drNenmatsuChosei["CHOSEI_KYUYO_SHAKAI_HOKENRYO"])));      //         社会保険料
                        updParam.SetParam("@ITEM162", SqlDbType.VarChar, 200,
                            this.FormatNum(Util.ToDecimal(drNenmatsuChosei["CHOSEI_KYUYO_SHOTOKUZEIGAKU"])));       //         所得税
                        updParam.SetParam("@ITEM163", SqlDbType.VarChar, 200,
                            this.FormatNum(Util.ToDecimal(drNenmatsuChosei["CHOSEI_SHOYO_KAZEI_KYUYOGAKU"])));      // 調整賞与課税対象額
                        updParam.SetParam("@ITEM164", SqlDbType.VarChar, 200,
                            this.FormatNum(Util.ToDecimal(drNenmatsuChosei["CHOSEI_SHOYO_SHAKAI_HOKENRYO"])));      //         社会保険料
                        updParam.SetParam("@ITEM165", SqlDbType.VarChar, 200,
                            this.FormatNum(Util.ToDecimal(drNenmatsuChosei["CHOSEI_SHOYO_SHOTOKUZEIGAKU"])));       //         所得税
                    }
                    #endregion
                    break;
            }

            alParams.Add(updParam);

            return alParams;
        }

        /// <summary>
        /// 数値フォーマット(０値を表示しない)
        /// </summary>
        private string FormatNum(object num)
        {
            string ret = "";
            if (num != null) ret = Util.FormatNum(num);
            if (ret == "0") ret = "";
            return ret;
        }
        #endregion
    }
}