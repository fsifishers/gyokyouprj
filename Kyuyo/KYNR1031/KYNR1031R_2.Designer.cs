﻿namespace jp.co.fsi.ky.kynr1031
{
    /// <summary>
    /// KYNR1031R_2 の帳票
    /// </summary>
    partial class KYNR1031R_2
    {
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(KYNR1031R_2));
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.textBox320 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox321 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox322 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox323 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox324 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox325 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox326 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox327 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox328 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox329 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox330 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox331 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox332 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox333 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox334 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox335 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox336 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox337 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox338 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox339 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox340 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox341 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox342 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox343 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox344 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox345 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox346 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox347 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox348 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox349 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox350 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox351 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox352 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox353 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox354 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox355 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox356 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox357 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox358 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox359 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox360 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox361 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox362 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox363 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox364 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox365 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox366 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox367 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox368 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox369 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox370 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox371 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox372 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox373 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox374 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox375 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox376 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox377 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox378 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox379 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox380 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox381 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox382 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox383 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox384 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox385 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox386 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox387 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox388 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox389 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox390 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox391 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox392 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox393 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox394 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            ((System.ComponentModel.ISupportInitialize)(this.textBox320)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox321)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox322)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox323)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox324)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox325)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox326)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox327)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox328)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox329)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox330)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox331)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox332)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox333)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox334)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox335)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox336)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox337)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox338)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox339)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox340)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox341)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox342)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox343)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox344)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox345)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox346)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox347)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox348)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox349)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox350)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox351)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox352)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox353)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox354)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox355)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox356)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox357)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox358)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox359)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox360)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox361)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox362)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox363)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox364)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox365)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox366)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox367)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox368)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox369)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox370)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox371)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox372)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox373)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox374)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox375)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox376)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox377)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox378)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox379)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox380)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox381)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox382)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox383)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox384)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox385)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox386)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox387)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox388)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox389)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox390)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox391)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox392)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox393)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox394)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.textBox320,
            this.textBox321,
            this.textBox322,
            this.textBox323,
            this.textBox324,
            this.textBox325,
            this.textBox326,
            this.textBox327,
            this.textBox328,
            this.textBox329,
            this.textBox330,
            this.textBox331,
            this.textBox332,
            this.textBox333,
            this.textBox334,
            this.textBox335,
            this.textBox336,
            this.textBox337,
            this.textBox338,
            this.textBox339,
            this.textBox340,
            this.textBox341,
            this.textBox342,
            this.textBox343,
            this.textBox344,
            this.textBox345,
            this.textBox346,
            this.textBox347,
            this.textBox348,
            this.textBox349,
            this.textBox350,
            this.textBox351,
            this.textBox352,
            this.textBox353,
            this.textBox354,
            this.textBox355,
            this.textBox356,
            this.textBox357,
            this.textBox358,
            this.textBox359,
            this.textBox360,
            this.textBox361,
            this.textBox362,
            this.textBox363,
            this.textBox364,
            this.textBox365,
            this.textBox366,
            this.textBox367,
            this.textBox368,
            this.textBox369,
            this.textBox370,
            this.textBox371,
            this.textBox372,
            this.textBox373,
            this.textBox374,
            this.textBox375,
            this.textBox376,
            this.textBox377,
            this.textBox378,
            this.textBox379,
            this.textBox380,
            this.textBox381,
            this.textBox382,
            this.textBox383,
            this.textBox384,
            this.textBox385,
            this.textBox386,
            this.textBox387,
            this.textBox388,
            this.textBox389,
            this.textBox390,
            this.textBox391,
            this.textBox392,
            this.textBox393,
            this.textBox394,
            this.label12,
            this.label13,
            this.label14,
            this.label15,
            this.label16,
            this.label17,
            this.label18,
            this.label19,
            this.label20});
            this.detail.Height = 1.458432F;
            this.detail.Name = "detail";
            // 
            // textBox320
            // 
            this.textBox320.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox320.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox320.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox320.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox320.CanGrow = false;
            this.textBox320.Height = 0.4375F;
            this.textBox320.Left = 0.003937008F;
            this.textBox320.Name = "textBox320";
            this.textBox320.Style = "background-color: Silver; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-" +
    "weight: bold; text-align: center; ddo-char-set: 1";
            this.textBox320.Tag = "";
            this.textBox320.Text = "有給休暇";
            this.textBox320.Top = 0F;
            this.textBox320.Width = 0.1555556F;
            // 
            // textBox321
            // 
            this.textBox321.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox321.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox321.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox321.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox321.CanGrow = false;
            this.textBox321.Height = 0.1458333F;
            this.textBox321.Left = 0.5488205F;
            this.textBox321.Name = "textBox321";
            this.textBox321.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; vertical-align: middle; ddo-char-set: 1";
            this.textBox321.Tag = "";
            this.textBox321.Text = "繰越";
            this.textBox321.Top = 0.1456693F;
            this.textBox321.Width = 0.3152778F;
            // 
            // textBox322
            // 
            this.textBox322.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox322.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox322.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox322.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox322.CanGrow = false;
            this.textBox322.Height = 0.1458333F;
            this.textBox322.Left = 0.8640909F;
            this.textBox322.Name = "textBox322";
            this.textBox322.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; vertical-align: middle; ddo-char-set: 1";
            this.textBox322.Tag = "";
            this.textBox322.Text = "本年";
            this.textBox322.Top = 0.1458333F;
            this.textBox322.Width = 0.3152778F;
            // 
            // textBox323
            // 
            this.textBox323.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox323.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox323.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox323.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox323.CanGrow = false;
            this.textBox323.Height = 0.1458333F;
            this.textBox323.Left = 1.179368F;
            this.textBox323.Name = "textBox323";
            this.textBox323.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; vertical-align: middle; ddo-char-set: 1";
            this.textBox323.Tag = "";
            this.textBox323.Text = "合計";
            this.textBox323.Top = 0.1458333F;
            this.textBox323.Width = 0.3152778F;
            // 
            // textBox324
            // 
            this.textBox324.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox324.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox324.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox324.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox324.CanGrow = false;
            this.textBox324.Height = 0.1458333F;
            this.textBox324.Left = 1.494646F;
            this.textBox324.Name = "textBox324";
            this.textBox324.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-weight: normal; text-align" +
    ": center; vertical-align: middle; ddo-char-set: 1";
            this.textBox324.Tag = "";
            this.textBox324.Text = "前年分";
            this.textBox324.Top = 0.1458333F;
            this.textBox324.Width = 0.3152778F;
            // 
            // textBox325
            // 
            this.textBox325.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox325.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox325.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox325.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox325.CanGrow = false;
            this.textBox325.Height = 0.1458333F;
            this.textBox325.Left = 1.809924F;
            this.textBox325.Name = "textBox325";
            this.textBox325.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-weight: normal; text-align" +
    ": center; vertical-align: middle; ddo-char-set: 1";
            this.textBox325.Tag = "";
            this.textBox325.Text = "本年分";
            this.textBox325.Top = 0.1456693F;
            this.textBox325.Width = 0.3152778F;
            // 
            // textBox326
            // 
            this.textBox326.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox326.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox326.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox326.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox326.CanGrow = false;
            this.textBox326.Height = 0.2916667F;
            this.textBox326.Left = 0.1555557F;
            this.textBox326.Name = "textBox326";
            this.textBox326.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-weight: normal; text-align" +
    ": center; vertical-align: middle; ddo-char-set: 1";
            this.textBox326.Tag = "";
            this.textBox326.Text = "有給休暇\r\n付与日";
            this.textBox326.Top = 0F;
            this.textBox326.Width = 0.3932635F;
            // 
            // textBox327
            // 
            this.textBox327.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox327.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox327.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox327.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox327.CanGrow = false;
            this.textBox327.Height = 0.1458333F;
            this.textBox327.Left = 0.5488205F;
            this.textBox327.Name = "textBox327";
            this.textBox327.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.textBox327.Tag = "";
            this.textBox327.Text = "付  与";
            this.textBox327.Top = 0F;
            this.textBox327.Width = 0.9448813F;
            // 
            // textBox328
            // 
            this.textBox328.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox328.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox328.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox328.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox328.CanGrow = false;
            this.textBox328.Height = 0.1458333F;
            this.textBox328.Left = 1.494646F;
            this.textBox328.Name = "textBox328";
            this.textBox328.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.textBox328.Tag = "";
            this.textBox328.Text = "消 化";
            this.textBox328.Top = 0F;
            this.textBox328.Width = 0.6305556F;
            // 
            // textBox329
            // 
            this.textBox329.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox329.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox329.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox329.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox329.CanGrow = false;
            this.textBox329.Height = 0.2916667F;
            this.textBox329.Left = 2.125202F;
            this.textBox329.Name = "textBox329";
            this.textBox329.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; vertical-align: middle; ddo-char-set: 1";
            this.textBox329.Tag = "";
            this.textBox329.Text = "残日数";
            this.textBox329.Top = 0F;
            this.textBox329.Width = 0.4338578F;
            // 
            // textBox330
            // 
            this.textBox330.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox330.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox330.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox330.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox330.CanGrow = false;
            this.textBox330.Height = 0.1458333F;
            this.textBox330.Left = 0.5492153F;
            this.textBox330.Name = "textBox330";
            this.textBox330.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: bold; text-align: " +
    "center; ddo-char-set: 1";
            this.textBox330.Tag = "";
            this.textBox330.Text = null;
            this.textBox330.Top = 0.2917323F;
            this.textBox330.Width = 0.3152778F;
            // 
            // textBox331
            // 
            this.textBox331.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox331.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox331.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox331.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox331.CanGrow = false;
            this.textBox331.Height = 0.1458333F;
            this.textBox331.Left = 0.8644934F;
            this.textBox331.Name = "textBox331";
            this.textBox331.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: bold; text-align: " +
    "center; ddo-char-set: 1";
            this.textBox331.Tag = "";
            this.textBox331.Text = null;
            this.textBox331.Top = 0.2917323F;
            this.textBox331.Width = 0.3152778F;
            // 
            // textBox332
            // 
            this.textBox332.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox332.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox332.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox332.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox332.CanGrow = false;
            this.textBox332.Height = 0.1458333F;
            this.textBox332.Left = 1.179771F;
            this.textBox332.Name = "textBox332";
            this.textBox332.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: bold; text-align: " +
    "center; ddo-char-set: 1";
            this.textBox332.Tag = "";
            this.textBox332.Text = null;
            this.textBox332.Top = 0.2917323F;
            this.textBox332.Width = 0.3152778F;
            // 
            // textBox333
            // 
            this.textBox333.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox333.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox333.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox333.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox333.CanGrow = false;
            this.textBox333.Height = 0.1458333F;
            this.textBox333.Left = 1.495049F;
            this.textBox333.Name = "textBox333";
            this.textBox333.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: bold; text-align: " +
    "center; ddo-char-set: 1";
            this.textBox333.Tag = "";
            this.textBox333.Text = null;
            this.textBox333.Top = 0.2917323F;
            this.textBox333.Width = 0.3152778F;
            // 
            // textBox334
            // 
            this.textBox334.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox334.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox334.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox334.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox334.CanGrow = false;
            this.textBox334.Height = 0.1458333F;
            this.textBox334.Left = 1.808271F;
            this.textBox334.Name = "textBox334";
            this.textBox334.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: bold; text-align: " +
    "center; ddo-char-set: 1";
            this.textBox334.Tag = "";
            this.textBox334.Text = null;
            this.textBox334.Top = 0.2917323F;
            this.textBox334.Width = 0.3152778F;
            // 
            // textBox335
            // 
            this.textBox335.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox335.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox335.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox335.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox335.CanGrow = false;
            this.textBox335.Height = 0.1458333F;
            this.textBox335.Left = 0.1555147F;
            this.textBox335.Name = "textBox335";
            this.textBox335.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: bold; text-align: " +
    "center; ddo-char-set: 1";
            this.textBox335.Tag = "";
            this.textBox335.Text = null;
            this.textBox335.Top = 0.2917323F;
            this.textBox335.Width = 0.3933074F;
            // 
            // textBox336
            // 
            this.textBox336.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox336.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox336.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox336.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox336.CanGrow = false;
            this.textBox336.Height = 0.1458333F;
            this.textBox336.Left = 2.128362F;
            this.textBox336.Name = "textBox336";
            this.textBox336.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: bold; text-align: " +
    "center; ddo-char-set: 1";
            this.textBox336.Tag = "";
            this.textBox336.Text = null;
            this.textBox336.Top = 0.2917323F;
            this.textBox336.Width = 0.4306974F;
            // 
            // textBox337
            // 
            this.textBox337.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox337.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox337.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox337.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox337.CanGrow = false;
            this.textBox337.Height = 1.020768F;
            this.textBox337.Left = 0.003937008F;
            this.textBox337.Name = "textBox337";
            this.textBox337.Style = "background-color: Silver; color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-" +
    "weight: bold; text-align: center; ddo-char-set: 1";
            this.textBox337.Tag = "";
            this.textBox337.Text = "\r\n該\r\n\r\n当\r\n\r\n区\r\n\r\n分";
            this.textBox337.Top = 0.4374016F;
            this.textBox337.Width = 0.1555556F;
            // 
            // textBox338
            // 
            this.textBox338.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox338.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox338.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox338.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox338.CanGrow = false;
            this.textBox338.Height = 0.1180556F;
            this.textBox338.Left = 0.5488205F;
            this.textBox338.Name = "textBox338";
            this.textBox338.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.textBox338.Tag = "";
            this.textBox338.Text = "控除対象配偶者";
            this.textBox338.Top = 0.4374016F;
            this.textBox338.Width = 0.6298611F;
            // 
            // textBox339
            // 
            this.textBox339.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox339.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox339.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox339.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox339.CanGrow = false;
            this.textBox339.Height = 0.1180556F;
            this.textBox339.Left = 1.179924F;
            this.textBox339.Name = "textBox339";
            this.textBox339.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.textBox339.Tag = "";
            this.textBox339.Text = "扶養親族数";
            this.textBox339.Top = 0.4374016F;
            this.textBox339.Width = 0.7492521F;
            // 
            // textBox340
            // 
            this.textBox340.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox340.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox340.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox340.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox340.CanGrow = false;
            this.textBox340.Height = 0.1499563F;
            this.textBox340.Left = 0.1555557F;
            this.textBox340.Name = "textBox340";
            this.textBox340.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.textBox340.Tag = "";
            this.textBox340.Text = "甲";
            this.textBox340.Top = 0.5555556F;
            this.textBox340.Width = 0.1972011F;
            // 
            // textBox341
            // 
            this.textBox341.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox341.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox341.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox341.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox341.CanGrow = false;
            this.textBox341.Height = 0.1458333F;
            this.textBox341.Left = 0.939765F;
            this.textBox341.Name = "textBox341";
            this.textBox341.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.textBox341.Tag = "";
            this.textBox341.Text = "老人";
            this.textBox341.Top = 0.5555118F;
            this.textBox341.Width = 0.236614F;
            // 
            // textBox342
            // 
            this.textBox342.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox342.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox342.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox342.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox342.Height = 0.1458333F;
            this.textBox342.Left = 1.179924F;
            this.textBox342.Name = "textBox342";
            this.textBox342.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-weight: bold; text-align: " +
    "center; ddo-char-set: 1";
            this.textBox342.Tag = "";
            this.textBox342.Text = null;
            this.textBox342.Top = 0.5554571F;
            this.textBox342.Width = 0.159668F;
            // 
            // textBox343
            // 
            this.textBox343.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox343.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox343.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox343.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox343.CanGrow = false;
            this.textBox343.Height = 0.1180556F;
            this.textBox343.Left = 0.1555557F;
            this.textBox343.Name = "textBox343";
            this.textBox343.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.textBox343.Tag = "";
            this.textBox343.Text = "税額表";
            this.textBox343.Top = 0.4375001F;
            this.textBox343.Width = 0.3932635F;
            // 
            // textBox344
            // 
            this.textBox344.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox344.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox344.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox344.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox344.CanGrow = false;
            this.textBox344.Height = 0.1180556F;
            this.textBox344.Left = 1.9285F;
            this.textBox344.Name = "textBox344";
            this.textBox344.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.textBox344.Tag = "";
            this.textBox344.Text = "障害者数";
            this.textBox344.Top = 0.4374016F;
            this.textBox344.Width = 0.6305556F;
            // 
            // textBox345
            // 
            this.textBox345.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox345.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox345.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox345.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox345.CanGrow = false;
            this.textBox345.Height = 0.1458333F;
            this.textBox345.Left = 0.3566952F;
            this.textBox345.Name = "textBox345";
            this.textBox345.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.textBox345.Tag = "";
            this.textBox345.Text = "乙";
            this.textBox345.Top = 0.5555118F;
            this.textBox345.Width = 0.1889766F;
            // 
            // textBox346
            // 
            this.textBox346.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox346.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox346.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox346.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox346.CanGrow = false;
            this.textBox346.Height = 0.1458333F;
            this.textBox346.Left = 0.5492153F;
            this.textBox346.Name = "textBox346";
            this.textBox346.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.textBox346.Tag = "";
            this.textBox346.Text = "有";
            this.textBox346.Top = 0.5555118F;
            this.textBox346.Width = 0.1933073F;
            // 
            // textBox347
            // 
            this.textBox347.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox347.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox347.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox347.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox347.CanGrow = false;
            this.textBox347.Height = 0.1458333F;
            this.textBox347.Left = 0.7440977F;
            this.textBox347.Name = "textBox347";
            this.textBox347.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.textBox347.Tag = "";
            this.textBox347.Text = "無";
            this.textBox347.Top = 0.5555118F;
            this.textBox347.Width = 0.1933073F;
            // 
            // textBox348
            // 
            this.textBox348.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox348.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox348.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox348.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox348.CanGrow = false;
            this.textBox348.Height = 0.07525159F;
            this.textBox348.Left = 1.339765F;
            this.textBox348.Name = "textBox348";
            this.textBox348.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 4pt; font-weight: bold; text-align: " +
    "center; ddo-char-set: 1";
            this.textBox348.Tag = "";
            this.textBox348.Text = null;
            this.textBox348.Top = 0.6259843F;
            this.textBox348.Width = 0.1972222F;
            // 
            // textBox349
            // 
            this.textBox349.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox349.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox349.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox349.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox349.Height = 0.07525159F;
            this.textBox349.Left = 1.533097F;
            this.textBox349.Name = "textBox349";
            this.textBox349.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 4pt; font-weight: bold; text-align: " +
    "center; ddo-char-set: 1";
            this.textBox349.Tag = "";
            this.textBox349.Text = null;
            this.textBox349.Top = 0.6259843F;
            this.textBox349.Width = 0.1972222F;
            // 
            // textBox350
            // 
            this.textBox350.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox350.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox350.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox350.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox350.CanGrow = false;
            this.textBox350.Height = 0.1458333F;
            this.textBox350.Left = 1.734035F;
            this.textBox350.Name = "textBox350";
            this.textBox350.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: bold; text-align: " +
    "center; ddo-char-set: 1";
            this.textBox350.Tag = "";
            this.textBox350.Text = null;
            this.textBox350.Top = 0.5554571F;
            this.textBox350.Width = 0.1972222F;
            // 
            // textBox351
            // 
            this.textBox351.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox351.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox351.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox351.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox351.CanGrow = false;
            this.textBox351.Height = 0.07519688F;
            this.textBox351.Left = 1.92716F;
            this.textBox351.Name = "textBox351";
            this.textBox351.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: bold; text-align: " +
    "center; ddo-char-set: 1";
            this.textBox351.Tag = "";
            this.textBox351.Text = null;
            this.textBox351.Top = 0.6259843F;
            this.textBox351.Width = 0.2011819F;
            // 
            // textBox352
            // 
            this.textBox352.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox352.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox352.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox352.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox352.CanGrow = false;
            this.textBox352.Height = 0.07519688F;
            this.textBox352.Left = 2.128347F;
            this.textBox352.Name = "textBox352";
            this.textBox352.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: bold; text-align: " +
    "center; ddo-char-set: 1";
            this.textBox352.Tag = "";
            this.textBox352.Text = null;
            this.textBox352.Top = 0.6259843F;
            this.textBox352.Width = 0.2248044F;
            // 
            // textBox353
            // 
            this.textBox353.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox353.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox353.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox353.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox353.CanGrow = false;
            this.textBox353.Height = 0.1458333F;
            this.textBox353.Left = 2.350792F;
            this.textBox353.Name = "textBox353";
            this.textBox353.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: bold; text-align: " +
    "center; vertical-align: middle; ddo-char-set: 1";
            this.textBox353.Tag = "";
            this.textBox353.Text = null;
            this.textBox353.Top = 0.5555118F;
            this.textBox353.Width = 0.2082676F;
            // 
            // textBox354
            // 
            this.textBox354.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox354.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox354.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox354.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox354.CanGrow = false;
            this.textBox354.DataField = "ITEM111";
            this.textBox354.Height = 0.1499563F;
            this.textBox354.Left = 0.1555147F;
            this.textBox354.Name = "textBox354";
            this.textBox354.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.textBox354.Tag = "";
            this.textBox354.Text = "ITEM111";
            this.textBox354.Top = 0.7055118F;
            this.textBox354.Width = 0.1972011F;
            // 
            // textBox355
            // 
            this.textBox355.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox355.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox355.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox355.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox355.CanGrow = false;
            this.textBox355.DataField = "ITEM115";
            this.textBox355.Height = 0.1458333F;
            this.textBox355.Left = 0.939723F;
            this.textBox355.Name = "textBox355";
            this.textBox355.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.textBox355.Tag = "";
            this.textBox355.Text = "ITEM115";
            this.textBox355.Top = 0.7055118F;
            this.textBox355.Width = 0.236614F;
            // 
            // textBox356
            // 
            this.textBox356.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox356.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox356.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox356.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox356.CanGrow = false;
            this.textBox356.DataField = "ITEM116";
            this.textBox356.Height = 0.1458333F;
            this.textBox356.Left = 1.178701F;
            this.textBox356.Name = "textBox356";
            this.textBox356.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.textBox356.Tag = "";
            this.textBox356.Text = "ITEM116";
            this.textBox356.Top = 0.7055118F;
            this.textBox356.Width = 0.1608486F;
            // 
            // textBox357
            // 
            this.textBox357.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox357.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox357.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox357.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox357.CanGrow = false;
            this.textBox357.DataField = "ITEM112";
            this.textBox357.Height = 0.1458333F;
            this.textBox357.Left = 0.3566532F;
            this.textBox357.Name = "textBox357";
            this.textBox357.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.textBox357.Tag = "";
            this.textBox357.Text = "ITEM112";
            this.textBox357.Top = 0.7055118F;
            this.textBox357.Width = 0.1889766F;
            // 
            // textBox358
            // 
            this.textBox358.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox358.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox358.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox358.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox358.CanGrow = false;
            this.textBox358.DataField = "ITEM113";
            this.textBox358.Height = 0.1458333F;
            this.textBox358.Left = 0.5492153F;
            this.textBox358.Name = "textBox358";
            this.textBox358.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.textBox358.Tag = "";
            this.textBox358.Text = "ITEM113";
            this.textBox358.Top = 0.7055118F;
            this.textBox358.Width = 0.1933073F;
            // 
            // textBox359
            // 
            this.textBox359.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox359.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox359.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox359.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox359.CanGrow = false;
            this.textBox359.DataField = "ITEM114";
            this.textBox359.Height = 0.1458333F;
            this.textBox359.Left = 0.7440977F;
            this.textBox359.Name = "textBox359";
            this.textBox359.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.textBox359.Tag = "";
            this.textBox359.Text = "ITEM114";
            this.textBox359.Top = 0.7055118F;
            this.textBox359.Width = 0.1933073F;
            // 
            // textBox360
            // 
            this.textBox360.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox360.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox360.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox360.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox360.CanGrow = false;
            this.textBox360.DataField = "ITEM117";
            this.textBox360.Height = 0.1458333F;
            this.textBox360.Left = 1.339549F;
            this.textBox360.Name = "textBox360";
            this.textBox360.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.textBox360.Tag = "";
            this.textBox360.Text = "ITEM117";
            this.textBox360.Top = 0.7055118F;
            this.textBox360.Width = 0.1972222F;
            // 
            // textBox361
            // 
            this.textBox361.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox361.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox361.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox361.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox361.CanGrow = false;
            this.textBox361.DataField = "ITEM118";
            this.textBox361.Height = 0.1458333F;
            this.textBox361.Left = 1.536771F;
            this.textBox361.Name = "textBox361";
            this.textBox361.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.textBox361.Tag = "";
            this.textBox361.Text = "ITEM118";
            this.textBox361.Top = 0.7055118F;
            this.textBox361.Width = 0.1972222F;
            // 
            // textBox362
            // 
            this.textBox362.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox362.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox362.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox362.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox362.CanGrow = false;
            this.textBox362.DataField = "ITEM119";
            this.textBox362.Height = 0.1458333F;
            this.textBox362.Left = 1.733994F;
            this.textBox362.Name = "textBox362";
            this.textBox362.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.textBox362.Tag = "";
            this.textBox362.Text = "ITEM119";
            this.textBox362.Top = 0.7055118F;
            this.textBox362.Width = 0.1972222F;
            // 
            // textBox363
            // 
            this.textBox363.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox363.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox363.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox363.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox363.CanGrow = false;
            this.textBox363.DataField = "ITEM120";
            this.textBox363.Height = 0.1458333F;
            this.textBox363.Left = 1.92716F;
            this.textBox363.Name = "textBox363";
            this.textBox363.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.textBox363.Tag = "";
            this.textBox363.Text = "ITEM120";
            this.textBox363.Top = 0.7055118F;
            this.textBox363.Width = 0.2011819F;
            // 
            // textBox364
            // 
            this.textBox364.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox364.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox364.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox364.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox364.CanGrow = false;
            this.textBox364.DataField = "ITEM121";
            this.textBox364.Height = 0.1458333F;
            this.textBox364.Left = 2.128347F;
            this.textBox364.Name = "textBox364";
            this.textBox364.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.textBox364.Tag = "";
            this.textBox364.Text = "ITEM121";
            this.textBox364.Top = 0.7055118F;
            this.textBox364.Width = 0.2248044F;
            // 
            // textBox365
            // 
            this.textBox365.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox365.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox365.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox365.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox365.CanGrow = false;
            this.textBox365.DataField = "ITEM122";
            this.textBox365.Height = 0.1458333F;
            this.textBox365.Left = 2.350792F;
            this.textBox365.Name = "textBox365";
            this.textBox365.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.textBox365.Tag = "";
            this.textBox365.Text = "ITEM122";
            this.textBox365.Top = 0.7055118F;
            this.textBox365.Width = 0.2082676F;
            // 
            // textBox366
            // 
            this.textBox366.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox366.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox366.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox366.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox366.CanGrow = false;
            this.textBox366.Height = 0.1180556F;
            this.textBox366.Left = 0.1555557F;
            this.textBox366.Name = "textBox366";
            this.textBox366.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-weight: bold; text-align: " +
    "center; ddo-char-set: 1";
            this.textBox366.Tag = "";
            this.textBox366.Text = null;
            this.textBox366.Top = 0.8472222F;
            this.textBox366.Width = 0.2011378F;
            // 
            // textBox367
            // 
            this.textBox367.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox367.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox367.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox367.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox367.CanGrow = false;
            this.textBox367.Height = 0.1180556F;
            this.textBox367.Left = 0.3527575F;
            this.textBox367.Name = "textBox367";
            this.textBox367.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.textBox367.Tag = "";
            this.textBox367.Text = "本  人  に  該  当  す  る  項  目\r\n";
            this.textBox367.Top = 0.8472222F;
            this.textBox367.Width = 2.207085F;
            // 
            // textBox368
            // 
            this.textBox368.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox368.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox368.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox368.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox368.CanGrow = false;
            this.textBox368.Height = 0.3390532F;
            this.textBox368.Left = 0.1555147F;
            this.textBox368.Name = "textBox368";
            this.textBox368.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5.5pt; font-weight: normal; text-ali" +
    "gn: center; ddo-char-set: 1";
            this.textBox368.Tag = "";
            this.textBox368.Text = "16歳未満\r\n";
            this.textBox368.Top = 0.9653541F;
            this.textBox368.Width = 0.1929144F;
            // 
            // textBox369
            // 
            this.textBox369.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox369.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox369.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox369.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox369.CanGrow = false;
            this.textBox369.Height = 0.3390532F;
            this.textBox369.Left = 0.939765F;
            this.textBox369.Name = "textBox369";
            this.textBox369.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-weight: bold; text-align: " +
    "center; ddo-char-set: 1";
            this.textBox369.Tag = "";
            this.textBox369.Text = null;
            this.textBox369.Top = 0.9653541F;
            this.textBox369.Width = 0.236614F;
            // 
            // textBox370
            // 
            this.textBox370.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox370.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox370.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox370.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox370.CanGrow = false;
            this.textBox370.Height = 0.2288171F;
            this.textBox370.Left = 1.179528F;
            this.textBox370.Name = "textBox370";
            this.textBox370.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.textBox370.Tag = "";
            this.textBox370.Text = "一般";
            this.textBox370.Top = 1.07559F;
            this.textBox370.Width = 0.1751967F;
            // 
            // textBox371
            // 
            this.textBox371.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox371.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox371.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox371.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox371.CanGrow = false;
            this.textBox371.Height = 0.3390532F;
            this.textBox371.Left = 0.3555145F;
            this.textBox371.Name = "textBox371";
            this.textBox371.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5.5pt; font-weight: normal; text-ali" +
    "gn: center; ddo-char-set: 1";
            this.textBox371.Tag = "";
            this.textBox371.Text = "未成年者";
            this.textBox371.Top = 0.9653541F;
            this.textBox371.Width = 0.1929134F;
            // 
            // textBox372
            // 
            this.textBox372.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox372.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox372.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox372.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox372.CanGrow = false;
            this.textBox372.Height = 0.2326772F;
            this.textBox372.Left = 0.5492153F;
            this.textBox372.Name = "textBox372";
            this.textBox372.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.textBox372.Tag = "";
            this.textBox372.Text = "特別";
            this.textBox372.Top = 1.07559F;
            this.textBox372.Width = 0.1933073F;
            // 
            // textBox373
            // 
            this.textBox373.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox373.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox373.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox373.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox373.CanGrow = false;
            this.textBox373.Height = 0.2326772F;
            this.textBox373.Left = 0.7440977F;
            this.textBox373.Name = "textBox373";
            this.textBox373.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.textBox373.Tag = "";
            this.textBox373.Text = "そ\r\nの\r\n他";
            this.textBox373.Top = 1.07559F;
            this.textBox373.Width = 0.1933073F;
            // 
            // textBox374
            // 
            this.textBox374.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox374.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox374.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox374.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox374.CanGrow = false;
            this.textBox374.Height = 0.2288171F;
            this.textBox374.Left = 1.35473F;
            this.textBox374.Name = "textBox374";
            this.textBox374.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.textBox374.Tag = "";
            this.textBox374.Text = "特別";
            this.textBox374.Top = 1.07559F;
            this.textBox374.Width = 0.1775808F;
            // 
            // textBox375
            // 
            this.textBox375.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox375.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox375.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox375.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox375.CanGrow = false;
            this.textBox375.Height = 0.3390532F;
            this.textBox375.Left = 1.536618F;
            this.textBox375.Name = "textBox375";
            this.textBox375.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.textBox375.Tag = "";
            this.textBox375.Text = "寡夫";
            this.textBox375.Top = 0.9653541F;
            this.textBox375.Width = 0.1972222F;
            // 
            // textBox376
            // 
            this.textBox376.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox376.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox376.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox376.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox376.CanGrow = false;
            this.textBox376.Height = 0.3390532F;
            this.textBox376.Left = 1.731889F;
            this.textBox376.Name = "textBox376";
            this.textBox376.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.textBox376.Tag = "";
            this.textBox376.Text = "勤労学生";
            this.textBox376.Top = 0.9653541F;
            this.textBox376.Width = 0.1972222F;
            // 
            // textBox377
            // 
            this.textBox377.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox377.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox377.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox377.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox377.CanGrow = false;
            this.textBox377.Height = 0.3390532F;
            this.textBox377.Left = 1.92716F;
            this.textBox377.Name = "textBox377";
            this.textBox377.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.textBox377.Tag = "";
            this.textBox377.Text = "死亡退職";
            this.textBox377.Top = 0.9653541F;
            this.textBox377.Width = 0.2011819F;
            // 
            // textBox378
            // 
            this.textBox378.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox378.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox378.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox378.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox378.CanGrow = false;
            this.textBox378.Height = 0.3390532F;
            this.textBox378.Left = 2.128347F;
            this.textBox378.Name = "textBox378";
            this.textBox378.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-weight: normal; text-align" +
    ": center; vertical-align: middle; ddo-char-set: 1";
            this.textBox378.Tag = "";
            this.textBox378.Text = "災\r\n害\r\n者";
            this.textBox378.Top = 0.9653541F;
            this.textBox378.Width = 0.2248044F;
            // 
            // textBox379
            // 
            this.textBox379.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox379.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox379.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox379.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox379.CanGrow = false;
            this.textBox379.Height = 0.3390532F;
            this.textBox379.Left = 2.350792F;
            this.textBox379.Name = "textBox379";
            this.textBox379.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-weight: normal; text-align" +
    ": center; vertical-align: middle; ddo-char-set: 1";
            this.textBox379.Tag = "";
            this.textBox379.Text = "外国人";
            this.textBox379.Top = 0.9653541F;
            this.textBox379.Width = 0.2082676F;
            // 
            // textBox380
            // 
            this.textBox380.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox380.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox380.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox380.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox380.CanGrow = false;
            this.textBox380.Height = 0.1102362F;
            this.textBox380.Left = 0.5488205F;
            this.textBox380.Name = "textBox380";
            this.textBox380.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.textBox380.Tag = "";
            this.textBox380.Text = "障害者";
            this.textBox380.Top = 0.9653541F;
            this.textBox380.Width = 0.3885819F;
            // 
            // textBox381
            // 
            this.textBox381.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox381.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox381.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox381.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox381.CanGrow = false;
            this.textBox381.Height = 0.1102362F;
            this.textBox381.Left = 1.178743F;
            this.textBox381.Name = "textBox381";
            this.textBox381.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.textBox381.Tag = "";
            this.textBox381.Text = "寡婦";
            this.textBox381.Top = 0.9653541F;
            this.textBox381.Width = 0.3535928F;
            // 
            // textBox382
            // 
            this.textBox382.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox382.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox382.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox382.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox382.CanGrow = false;
            this.textBox382.DataField = "ITEM123";
            this.textBox382.Height = 0.1458333F;
            this.textBox382.Left = 0.1555147F;
            this.textBox382.Name = "textBox382";
            this.textBox382.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.textBox382.Tag = "";
            this.textBox382.Text = "ITEM123";
            this.textBox382.Top = 1.312599F;
            this.textBox382.Width = 0.1929144F;
            // 
            // textBox383
            // 
            this.textBox383.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox383.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox383.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox383.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox383.CanGrow = false;
            this.textBox383.DataField = "ITEM127";
            this.textBox383.Height = 0.1458333F;
            this.textBox383.Left = 0.939765F;
            this.textBox383.Name = "textBox383";
            this.textBox383.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.textBox383.Tag = "";
            this.textBox383.Text = "ITEM127";
            this.textBox383.Top = 1.312599F;
            this.textBox383.Width = 0.236614F;
            // 
            // textBox384
            // 
            this.textBox384.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox384.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox384.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox384.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox384.CanGrow = false;
            this.textBox384.DataField = "ITEM128";
            this.textBox384.Height = 0.1458333F;
            this.textBox384.Left = 1.179528F;
            this.textBox384.Name = "textBox384";
            this.textBox384.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.textBox384.Tag = "";
            this.textBox384.Text = "ITEM128";
            this.textBox384.Top = 1.312599F;
            this.textBox384.Width = 0.1751967F;
            // 
            // textBox385
            // 
            this.textBox385.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox385.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox385.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox385.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox385.CanGrow = false;
            this.textBox385.DataField = "ITEM124";
            this.textBox385.Height = 0.1458333F;
            this.textBox385.Left = 0.3555145F;
            this.textBox385.Name = "textBox385";
            this.textBox385.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.textBox385.Tag = "";
            this.textBox385.Text = "ITEM124";
            this.textBox385.Top = 1.312599F;
            this.textBox385.Width = 0.1929134F;
            // 
            // textBox386
            // 
            this.textBox386.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox386.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox386.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox386.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox386.CanGrow = false;
            this.textBox386.DataField = "ITEM125";
            this.textBox386.Height = 0.1458333F;
            this.textBox386.Left = 0.5492153F;
            this.textBox386.Name = "textBox386";
            this.textBox386.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.textBox386.Tag = "";
            this.textBox386.Text = "ITEM125";
            this.textBox386.Top = 1.312599F;
            this.textBox386.Width = 0.1933073F;
            // 
            // textBox387
            // 
            this.textBox387.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox387.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox387.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox387.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox387.CanGrow = false;
            this.textBox387.DataField = "ITEM126";
            this.textBox387.Height = 0.1458333F;
            this.textBox387.Left = 0.7440977F;
            this.textBox387.Name = "textBox387";
            this.textBox387.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.textBox387.Tag = "";
            this.textBox387.Text = "ITEM126";
            this.textBox387.Top = 1.312599F;
            this.textBox387.Width = 0.1933073F;
            // 
            // textBox388
            // 
            this.textBox388.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox388.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox388.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox388.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox388.CanGrow = false;
            this.textBox388.DataField = "ITEM129";
            this.textBox388.Height = 0.1458333F;
            this.textBox388.Left = 1.35473F;
            this.textBox388.Name = "textBox388";
            this.textBox388.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.textBox388.Tag = "";
            this.textBox388.Text = "ITEM129";
            this.textBox388.Top = 1.312599F;
            this.textBox388.Width = 0.1775808F;
            // 
            // textBox389
            // 
            this.textBox389.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox389.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox389.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox389.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox389.CanGrow = false;
            this.textBox389.DataField = "ITEM130";
            this.textBox389.Height = 0.1458333F;
            this.textBox389.Left = 1.536618F;
            this.textBox389.Name = "textBox389";
            this.textBox389.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.textBox389.Tag = "";
            this.textBox389.Text = "ITEM130";
            this.textBox389.Top = 1.312599F;
            this.textBox389.Width = 0.1972222F;
            // 
            // textBox390
            // 
            this.textBox390.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox390.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox390.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox390.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox390.CanGrow = false;
            this.textBox390.DataField = "ITEM131";
            this.textBox390.Height = 0.1458333F;
            this.textBox390.Left = 1.731889F;
            this.textBox390.Name = "textBox390";
            this.textBox390.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.textBox390.Tag = "";
            this.textBox390.Text = "ITEM131";
            this.textBox390.Top = 1.312599F;
            this.textBox390.Width = 0.1972222F;
            // 
            // textBox391
            // 
            this.textBox391.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox391.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox391.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox391.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox391.CanGrow = false;
            this.textBox391.DataField = "ITEM132";
            this.textBox391.Height = 0.1458333F;
            this.textBox391.Left = 1.92716F;
            this.textBox391.Name = "textBox391";
            this.textBox391.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.textBox391.Tag = "";
            this.textBox391.Text = "ITEM132";
            this.textBox391.Top = 1.312599F;
            this.textBox391.Width = 0.2011819F;
            // 
            // textBox392
            // 
            this.textBox392.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox392.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox392.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox392.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox392.CanGrow = false;
            this.textBox392.DataField = "ITEM133";
            this.textBox392.Height = 0.1458333F;
            this.textBox392.Left = 2.128347F;
            this.textBox392.Name = "textBox392";
            this.textBox392.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.textBox392.Tag = "";
            this.textBox392.Text = "ITEM133";
            this.textBox392.Top = 1.312599F;
            this.textBox392.Width = 0.2248044F;
            // 
            // textBox393
            // 
            this.textBox393.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox393.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox393.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox393.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox393.CanGrow = false;
            this.textBox393.DataField = "ITEM134";
            this.textBox393.Height = 0.1458333F;
            this.textBox393.Left = 2.350792F;
            this.textBox393.Name = "textBox393";
            this.textBox393.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.textBox393.Tag = "";
            this.textBox393.Text = "ITEM134";
            this.textBox393.Top = 1.312599F;
            this.textBox393.Width = 0.2082676F;
            // 
            // textBox394
            // 
            this.textBox394.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox394.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox394.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox394.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox394.CanGrow = false;
            this.textBox394.Height = 0.0748032F;
            this.textBox394.Left = 1.344494F;
            this.textBox394.Name = "textBox394";
            this.textBox394.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 4pt; font-weight: bold; text-align: " +
    "center; ddo-char-set: 1";
            this.textBox394.Tag = "";
            this.textBox394.Text = null;
            this.textBox394.Top = 0.5555118F;
            this.textBox394.Width = 0.3858269F;
            // 
            // label12
            // 
            this.label12.Height = 0.08031493F;
            this.label12.HyperLink = null;
            this.label12.Left = 1.344494F;
            this.label12.Name = "label12";
            this.label12.Style = "font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align: center; ddo-" +
    "char-set: 1";
            this.label12.Text = "老　人";
            this.label12.Top = 0.5456693F;
            this.label12.Width = 0.3893709F;
            // 
            // label13
            // 
            this.label13.Height = 0.08031493F;
            this.label13.HyperLink = null;
            this.label13.Left = 1.328348F;
            this.label13.Name = "label13";
            this.label13.Style = "font-family: ＭＳ 明朝; font-size: 4pt; font-weight: normal; text-align: center; ddo-" +
    "char-set: 1";
            this.label13.Text = "同居";
            this.label13.Top = 0.6291339F;
            this.label13.Width = 0.2122049F;
            // 
            // label14
            // 
            this.label14.Height = 0.08031493F;
            this.label14.HyperLink = null;
            this.label14.Left = 1.521653F;
            this.label14.Name = "label14";
            this.label14.Style = "font-family: ＭＳ 明朝; font-size: 4pt; font-weight: normal; text-align: center; ddo-" +
    "char-set: 1";
            this.label14.Text = "計";
            this.label14.Top = 0.6291339F;
            this.label14.Width = 0.227953F;
            // 
            // label15
            // 
            this.label15.Height = 0.08031493F;
            this.label15.HyperLink = null;
            this.label15.Left = 1.951181F;
            this.label15.Name = "label15";
            this.label15.Style = "font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align: center; ddo-" +
    "char-set: 1";
            this.label15.Text = "特　別";
            this.label15.Top = 0.5456693F;
            this.label15.Width = 0.3893709F;
            // 
            // label16
            // 
            this.label16.Height = 0.08031493F;
            this.label16.HyperLink = null;
            this.label16.Left = 1.935028F;
            this.label16.Name = "label16";
            this.label16.Style = "font-family: ＭＳ 明朝; font-size: 4pt; font-weight: normal; text-align: center; ddo-" +
    "char-set: 1";
            this.label16.Text = "同居";
            this.label16.Top = 0.6291339F;
            this.label16.Width = 0.2122049F;
            // 
            // label17
            // 
            this.label17.Height = 0.08031493F;
            this.label17.HyperLink = null;
            this.label17.Left = 2.128341F;
            this.label17.Name = "label17";
            this.label17.Style = "font-family: ＭＳ 明朝; font-size: 4pt; font-weight: normal; text-align: center; ddo-" +
    "char-set: 1";
            this.label17.Text = "計";
            this.label17.Top = 0.6291339F;
            this.label17.Width = 0.227953F;
            // 
            // label18
            // 
            this.label18.Height = 0.1456694F;
            this.label18.HyperLink = null;
            this.label18.Left = 1.176382F;
            this.label18.Name = "label18";
            this.label18.Style = "font-family: ＭＳ 明朝; font-size: 5pt; font-weight: normal; text-align: left; vertic" +
    "al-align: middle; ddo-char-set: 1";
            this.label18.Text = "特定";
            this.label18.Top = 0.5555118F;
            this.label18.Width = 0.167717F;
            // 
            // label19
            // 
            this.label19.Height = 0.1456694F;
            this.label19.HyperLink = null;
            this.label19.Left = 1.73032F;
            this.label19.Name = "label19";
            this.label19.Style = "font-family: ＭＳ 明朝; font-size: 4pt; font-weight: normal; text-align: center; vert" +
    "ical-align: middle; ddo-char-set: 1";
            this.label19.Text = "その他";
            this.label19.Top = 0.5555118F;
            this.label19.Width = 0.1952754F;
            // 
            // label20
            // 
            this.label20.Height = 0.1456694F;
            this.label20.HyperLink = null;
            this.label20.Left = 2.340549F;
            this.label20.Name = "label20";
            this.label20.Style = "font-family: ＭＳ 明朝; font-size: 4pt; font-weight: normal; text-align: center; vert" +
    "ical-align: middle; ddo-char-set: 1";
            this.label20.Text = "その他";
            this.label20.Top = 0.5555118F;
            this.label20.Width = 0.2110243F;
            // 
            // KYUR3051R_2
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 2.5625F;
            this.Sections.Add(this.detail);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.textBox320)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox321)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox322)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox323)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox324)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox325)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox326)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox327)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox328)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox329)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox330)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox331)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox332)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox333)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox334)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox335)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox336)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox337)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox338)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox339)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox340)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox341)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox342)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox343)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox344)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox345)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox346)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox347)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox348)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox349)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox350)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox351)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox352)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox353)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox354)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox355)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox356)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox357)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox358)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox359)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox360)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox361)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox362)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox363)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox364)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox365)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox366)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox367)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox368)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox369)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox370)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox371)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox372)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox373)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox374)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox375)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox376)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox377)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox378)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox379)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox380)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox381)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox382)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox383)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox384)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox385)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox386)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox387)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox388)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox389)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox390)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox391)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox392)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox393)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox394)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox320;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox321;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox322;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox323;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox324;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox325;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox326;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox327;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox328;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox329;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox330;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox331;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox332;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox333;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox334;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox335;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox336;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox337;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox338;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox339;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox340;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox341;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox342;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox343;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox344;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox345;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox346;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox347;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox348;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox349;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox350;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox351;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox352;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox353;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox354;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox355;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox356;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox357;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox358;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox359;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox360;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox361;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox362;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox363;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox364;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox365;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox366;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox367;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox368;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox369;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox370;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox371;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox372;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox373;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox374;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox375;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox376;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox377;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox378;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox379;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox380;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox381;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox382;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox383;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox384;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox385;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox386;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox387;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox388;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox389;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox390;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox391;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox392;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox393;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox394;
        private GrapeCity.ActiveReports.SectionReportModel.Label label12;
        private GrapeCity.ActiveReports.SectionReportModel.Label label13;
        private GrapeCity.ActiveReports.SectionReportModel.Label label14;
        private GrapeCity.ActiveReports.SectionReportModel.Label label15;
        private GrapeCity.ActiveReports.SectionReportModel.Label label16;
        private GrapeCity.ActiveReports.SectionReportModel.Label label17;
        private GrapeCity.ActiveReports.SectionReportModel.Label label18;
        private GrapeCity.ActiveReports.SectionReportModel.Label label19;
        private GrapeCity.ActiveReports.SectionReportModel.Label label20;
    }
}
