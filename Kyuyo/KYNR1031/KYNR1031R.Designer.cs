﻿namespace jp.co.fsi.ky.kynr1031
{
    /// <summary>
    /// KYNR1031R の帳票
    /// </summary>
    partial class KYNR1031R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(KYNR1031R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.ラベル389 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル382 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル395 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM010 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM011 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル380 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ITEM013 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM016_M = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM16_F = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM015 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM018 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM019 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM020 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM021 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM012 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM014 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM022 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM023 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル403 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ITEM031 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM032 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM033 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM034 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM035 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM036 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM037 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM038 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM039 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM041 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM042 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM043 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM044 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM045 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM046 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM047 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM048 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM049 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM051 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM052 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM053 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM054 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM055 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM056 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM057 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM058 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM059 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM060 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル396 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル381 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル384 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル385 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル386 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ラベル394 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txt001 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.title = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM121 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM122 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM123 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM124 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM125 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM126 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM127 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM128 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM129 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.直線464 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.ITEM102 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM103 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM111 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM112 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM113 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM114 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM115 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM116 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM117 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM118 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ITEM119 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.社員コードヘッダー = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.社員コードフッダー = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.KYNR1031R_2 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.KYNR1031R_3 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.KYNR1031R_1 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル389)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル382)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル395)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM010)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM011)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル380)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM013)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM016_M)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM16_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM015)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM018)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM019)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM020)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM021)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM012)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM014)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM022)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM023)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル403)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM031)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM032)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM033)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM034)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM035)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM036)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM037)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM038)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM039)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM041)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM042)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM043)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM044)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM045)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM046)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM047)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM048)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM049)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM051)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM052)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM053)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM054)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM055)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM056)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM057)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM058)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM059)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM060)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル396)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル381)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル384)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル385)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル386)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル394)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt001)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.title)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM121)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM122)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM123)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM124)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM125)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM126)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM127)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM128)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM129)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM102)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM103)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM111)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM112)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM113)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM114)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM115)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM116)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM117)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM118)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM119)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.ラベル389,
            this.ラベル382,
            this.ラベル395,
            this.ITEM010,
            this.ITEM011,
            this.ラベル380,
            this.ITEM013,
            this.ITEM016_M,
            this.ITEM16_F,
            this.ITEM015,
            this.ITEM018,
            this.ITEM019,
            this.ITEM020,
            this.ITEM021,
            this.ITEM012,
            this.ITEM014,
            this.ITEM022,
            this.ITEM023,
            this.ラベル403,
            this.ITEM031,
            this.ITEM032,
            this.ITEM033,
            this.ITEM034,
            this.ITEM035,
            this.ITEM036,
            this.ITEM037,
            this.ITEM038,
            this.ITEM039,
            this.ITEM041,
            this.ITEM042,
            this.ITEM043,
            this.ITEM044,
            this.ITEM045,
            this.ITEM046,
            this.ITEM047,
            this.ITEM048,
            this.ITEM049,
            this.ITEM051,
            this.ITEM052,
            this.ITEM053,
            this.ITEM054,
            this.ITEM055,
            this.ITEM056,
            this.ITEM057,
            this.ITEM058,
            this.ITEM059,
            this.ITEM060,
            this.ラベル396,
            this.ラベル381,
            this.ラベル384,
            this.ラベル385,
            this.ラベル386,
            this.ラベル394,
            this.txt001});
            this.pageHeader.Height = 1.049306F;
            this.pageHeader.Name = "pageHeader";
            this.pageHeader.Format += new System.EventHandler(this.pageHeader_Format);
            // 
            // ラベル389
            // 
            this.ラベル389.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル389.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル389.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル389.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル389.Height = 0.1576389F;
            this.ラベル389.Left = 0.1972222F;
            this.ラベル389.Name = "ラベル389";
            this.ラベル389.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.ラベル389.Tag = "";
            this.ラベル389.Text = "所属";
            this.ラベル389.Top = 0.5763889F;
            this.ラベル389.Width = 2.3625F;
            // 
            // ラベル382
            // 
            this.ラベル382.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル382.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル382.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル382.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル382.Height = 0.3152778F;
            this.ラベル382.Left = 0.1972222F;
            this.ラベル382.Name = "ラベル382";
            this.ラベル382.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.ラベル382.Tag = "";
            this.ラベル382.Text = "氏名";
            this.ラベル382.Top = 0.2611111F;
            this.ラベル382.Width = 2.3625F;
            // 
            // ラベル395
            // 
            this.ラベル395.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル395.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル395.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル395.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル395.Height = 0.4729167F;
            this.ラベル395.Left = 4.922222F;
            this.ラベル395.Name = "ラベル395";
            this.ラベル395.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.ラベル395.Tag = "";
            this.ラベル395.Text = "〒　　　－";
            this.ラベル395.Top = 0.2611111F;
            this.ラベル395.Width = 3.15F;
            // 
            // ITEM010
            // 
            this.ITEM010.DataField = "ITEM010";
            this.ITEM010.Height = 0.2291667F;
            this.ITEM010.Left = 1.568056F;
            this.ITEM010.Name = "ITEM010";
            this.ITEM010.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 12pt; font-" +
    "weight: bold; text-align: center; ddo-char-set: 1";
            this.ITEM010.Tag = "";
            this.ITEM010.Text = "ITEM010";
            this.ITEM010.Top = 0F;
            this.ITEM010.Width = 7.483333F;
            // 
            // ITEM011
            // 
            this.ITEM011.DataField = "ITEM011";
            this.ITEM011.Height = 0.15625F;
            this.ITEM011.Left = 10.15625F;
            this.ITEM011.Name = "ITEM011";
            this.ITEM011.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.ITEM011.Tag = "";
            this.ITEM011.Text = "ITEM011";
            this.ITEM011.Top = 0.08333334F;
            this.ITEM011.Width = 0.3895833F;
            // 
            // ラベル380
            // 
            this.ラベル380.Height = 0.15625F;
            this.ラベル380.HyperLink = null;
            this.ラベル380.Left = 9.409028F;
            this.ラベル380.Name = "ラベル380";
            this.ラベル380.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.ラベル380.Tag = "";
            this.ラベル380.Text = "整理番号";
            this.ラベル380.Top = 0.07847222F;
            this.ラベル380.Width = 0.7875F;
            // 
            // ITEM013
            // 
            this.ITEM013.DataField = "ITEM013";
            this.ITEM013.Height = 0.1576389F;
            this.ITEM013.Left = 0.59375F;
            this.ITEM013.Name = "ITEM013";
            this.ITEM013.Style = "color: Black; font-family: ＭＳ 明朝; font-weight: normal; text-align: left; ddo-char" +
    "-set: 1";
            this.ITEM013.Tag = "";
            this.ITEM013.Text = "ITEM013";
            this.ITEM013.Top = 0.2631944F;
            this.ITEM013.Width = 1.966667F;
            // 
            // ITEM016_M
            // 
            this.ITEM016_M.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM016_M.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM016_M.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM016_M.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM016_M.CanGrow = false;
            this.ITEM016_M.DataField = "ITEM016";
            this.ITEM016_M.Height = 0.1576389F;
            this.ITEM016_M.Left = 2.559722F;
            this.ITEM016_M.Name = "ITEM016_M";
            this.ITEM016_M.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.ITEM016_M.Tag = "";
            this.ITEM016_M.Text = "ITEM016";
            this.ITEM016_M.Top = 0.5763889F;
            this.ITEM016_M.Width = 0.1972222F;
            // 
            // ITEM16_F
            // 
            this.ITEM16_F.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM16_F.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM16_F.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM16_F.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM16_F.CanGrow = false;
            this.ITEM16_F.DataField = "ITEM017";
            this.ITEM16_F.Height = 0.1576389F;
            this.ITEM16_F.Left = 2.756944F;
            this.ITEM16_F.Name = "ITEM16_F";
            this.ITEM16_F.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.ITEM16_F.Tag = "";
            this.ITEM16_F.Text = "ITEM017";
            this.ITEM16_F.Top = 0.5763889F;
            this.ITEM16_F.Width = 0.1972222F;
            // 
            // ITEM015
            // 
            this.ITEM015.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM015.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM015.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM015.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM015.DataField = "ITEM015";
            this.ITEM015.Height = 0.1576389F;
            this.ITEM015.Left = 2.953472F;
            this.ITEM015.Name = "ITEM015";
            this.ITEM015.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.ITEM015.Tag = "";
            this.ITEM015.Text = "ITEM015";
            this.ITEM015.Top = 0.2611111F;
            this.ITEM015.Width = 1.772222F;
            // 
            // ITEM018
            // 
            this.ITEM018.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM018.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM018.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM018.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM018.DataField = "ITEM018";
            this.ITEM018.Height = 0.1576389F;
            this.ITEM018.Left = 2.953472F;
            this.ITEM018.Name = "ITEM018";
            this.ITEM018.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.ITEM018.Tag = "";
            this.ITEM018.Text = "ITEM018";
            this.ITEM018.Top = 0.41875F;
            this.ITEM018.Width = 1.772222F;
            // 
            // ITEM019
            // 
            this.ITEM019.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM019.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM019.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM019.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM019.DataField = "ITEM019";
            this.ITEM019.Height = 0.1576389F;
            this.ITEM019.Left = 2.953472F;
            this.ITEM019.Name = "ITEM019";
            this.ITEM019.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.ITEM019.Tag = "";
            this.ITEM019.Text = "ITEM019";
            this.ITEM019.Top = 0.5763889F;
            this.ITEM019.Width = 1.772222F;
            // 
            // ITEM020
            // 
            this.ITEM020.DataField = "ITEM020";
            this.ITEM020.Height = 0.1576389F;
            this.ITEM020.Left = 5.136111F;
            this.ITEM020.Name = "ITEM020";
            this.ITEM020.Style = "color: Black; font-family: ＭＳ 明朝; font-weight: normal; text-align: left; ddo-char" +
    "-set: 1";
            this.ITEM020.Tag = "";
            this.ITEM020.Text = "ITEM020";
            this.ITEM020.Top = 0.2604167F;
            this.ITEM020.Width = 0.3104167F;
            // 
            // ITEM021
            // 
            this.ITEM021.DataField = "ITEM021";
            this.ITEM021.Height = 0.1576389F;
            this.ITEM021.Left = 5.6875F;
            this.ITEM021.Name = "ITEM021";
            this.ITEM021.Style = "color: Black; font-family: ＭＳ 明朝; font-weight: normal; text-align: left; ddo-char" +
    "-set: 1";
            this.ITEM021.Tag = "";
            this.ITEM021.Text = "ITEM021";
            this.ITEM021.Top = 0.2604167F;
            this.ITEM021.Width = 0.39375F;
            // 
            // ITEM012
            // 
            this.ITEM012.DataField = "ITEM012";
            this.ITEM012.Height = 0.1576389F;
            this.ITEM012.Left = 0.59375F;
            this.ITEM012.Name = "ITEM012";
            this.ITEM012.Style = "color: Black; font-family: ＭＳ 明朝; font-weight: normal; text-align: left; ddo-char" +
    "-set: 1";
            this.ITEM012.Tag = "";
            this.ITEM012.Text = "ITEM012";
            this.ITEM012.Top = 0.4208333F;
            this.ITEM012.Width = 1.966667F;
            // 
            // ITEM014
            // 
            this.ITEM014.DataField = "ITEM014";
            this.ITEM014.Height = 0.1576389F;
            this.ITEM014.Left = 0.59375F;
            this.ITEM014.Name = "ITEM014";
            this.ITEM014.Style = "color: Black; font-family: ＭＳ 明朝; font-weight: normal; text-align: left; ddo-char" +
    "-set: 1";
            this.ITEM014.Tag = "";
            this.ITEM014.Text = "ITEM014";
            this.ITEM014.Top = 0.5784721F;
            this.ITEM014.Width = 1.966667F;
            // 
            // ITEM022
            // 
            this.ITEM022.DataField = "ITEM022";
            this.ITEM022.Height = 0.1576389F;
            this.ITEM022.Left = 5.136111F;
            this.ITEM022.Name = "ITEM022";
            this.ITEM022.Style = "color: Black; font-family: ＭＳ 明朝; font-weight: normal; text-align: left; ddo-char" +
    "-set: 1";
            this.ITEM022.Tag = "";
            this.ITEM022.Text = "ITEM022";
            this.ITEM022.Top = 0.4180556F;
            this.ITEM022.Width = 2.872917F;
            // 
            // ITEM023
            // 
            this.ITEM023.DataField = "ITEM023";
            this.ITEM023.Height = 0.1576389F;
            this.ITEM023.Left = 5.135417F;
            this.ITEM023.Name = "ITEM023";
            this.ITEM023.Style = "color: Black; font-family: ＭＳ 明朝; font-weight: normal; text-align: left; ddo-char" +
    "-set: 1";
            this.ITEM023.Tag = "";
            this.ITEM023.Text = "ITEM023";
            this.ITEM023.Top = 0.5729167F;
            this.ITEM023.Width = 2.872917F;
            // 
            // ラベル403
            // 
            this.ラベル403.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル403.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル403.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル403.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル403.Height = 0.3152778F;
            this.ラベル403.HyperLink = null;
            this.ラベル403.Left = 0.1972222F;
            this.ラベル403.Name = "ラベル403";
            this.ラベル403.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.ラベル403.Tag = "";
            this.ラベル403.Text = "支給年月";
            this.ラベル403.Top = 0.7340278F;
            this.ラベル403.Width = 0.7875F;
            // 
            // ITEM031
            // 
            this.ITEM031.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM031.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM031.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM031.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM031.CanGrow = false;
            this.ITEM031.DataField = "ITEM031";
            this.ITEM031.Height = 0.1576389F;
            this.ITEM031.Left = 0.9847222F;
            this.ITEM031.Name = "ITEM031";
            this.ITEM031.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.ITEM031.Tag = "";
            this.ITEM031.Text = "ITEM031";
            this.ITEM031.Top = 0.7340278F;
            this.ITEM031.Width = 0.7875F;
            // 
            // ITEM032
            // 
            this.ITEM032.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM032.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM032.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM032.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM032.CanGrow = false;
            this.ITEM032.DataField = "ITEM032";
            this.ITEM032.Height = 0.1576389F;
            this.ITEM032.Left = 1.772222F;
            this.ITEM032.Name = "ITEM032";
            this.ITEM032.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.ITEM032.Tag = "";
            this.ITEM032.Text = "ITEM032";
            this.ITEM032.Top = 0.7340278F;
            this.ITEM032.Width = 0.7875F;
            // 
            // ITEM033
            // 
            this.ITEM033.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM033.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM033.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM033.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM033.CanGrow = false;
            this.ITEM033.DataField = "ITEM033";
            this.ITEM033.Height = 0.1576389F;
            this.ITEM033.Left = 2.559722F;
            this.ITEM033.Name = "ITEM033";
            this.ITEM033.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.ITEM033.Tag = "";
            this.ITEM033.Text = "ITEM033";
            this.ITEM033.Top = 0.7340278F;
            this.ITEM033.Width = 0.7875F;
            // 
            // ITEM034
            // 
            this.ITEM034.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM034.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM034.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM034.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM034.CanGrow = false;
            this.ITEM034.DataField = "ITEM034";
            this.ITEM034.Height = 0.1576389F;
            this.ITEM034.Left = 3.347222F;
            this.ITEM034.Name = "ITEM034";
            this.ITEM034.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.ITEM034.Tag = "";
            this.ITEM034.Text = "ITEM034";
            this.ITEM034.Top = 0.7340278F;
            this.ITEM034.Width = 0.7875F;
            // 
            // ITEM035
            // 
            this.ITEM035.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM035.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM035.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM035.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM035.CanGrow = false;
            this.ITEM035.DataField = "ITEM035";
            this.ITEM035.Height = 0.1576389F;
            this.ITEM035.Left = 4.134722F;
            this.ITEM035.Name = "ITEM035";
            this.ITEM035.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.ITEM035.Tag = "";
            this.ITEM035.Text = "ITEM035";
            this.ITEM035.Top = 0.7340278F;
            this.ITEM035.Width = 0.7875F;
            // 
            // ITEM036
            // 
            this.ITEM036.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM036.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM036.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM036.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM036.CanGrow = false;
            this.ITEM036.DataField = "ITEM036";
            this.ITEM036.Height = 0.1576389F;
            this.ITEM036.Left = 4.922222F;
            this.ITEM036.Name = "ITEM036";
            this.ITEM036.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.ITEM036.Tag = "";
            this.ITEM036.Text = "ITEM036";
            this.ITEM036.Top = 0.7340278F;
            this.ITEM036.Width = 0.7875F;
            // 
            // ITEM037
            // 
            this.ITEM037.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM037.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM037.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM037.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM037.CanGrow = false;
            this.ITEM037.DataField = "ITEM037";
            this.ITEM037.Height = 0.1576389F;
            this.ITEM037.Left = 5.709722F;
            this.ITEM037.Name = "ITEM037";
            this.ITEM037.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.ITEM037.Tag = "";
            this.ITEM037.Text = "ITEM037";
            this.ITEM037.Top = 0.7340278F;
            this.ITEM037.Width = 0.7875F;
            // 
            // ITEM038
            // 
            this.ITEM038.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM038.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM038.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM038.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM038.CanGrow = false;
            this.ITEM038.DataField = "ITEM038";
            this.ITEM038.Height = 0.1576389F;
            this.ITEM038.Left = 6.497222F;
            this.ITEM038.Name = "ITEM038";
            this.ITEM038.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.ITEM038.Tag = "";
            this.ITEM038.Text = "ITEM038";
            this.ITEM038.Top = 0.7340278F;
            this.ITEM038.Width = 0.7875F;
            // 
            // ITEM039
            // 
            this.ITEM039.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM039.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM039.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM039.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM039.CanGrow = false;
            this.ITEM039.DataField = "ITEM039";
            this.ITEM039.Height = 0.1576389F;
            this.ITEM039.Left = 7.284722F;
            this.ITEM039.Name = "ITEM039";
            this.ITEM039.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.ITEM039.Tag = "";
            this.ITEM039.Text = "ITEM039";
            this.ITEM039.Top = 0.7340278F;
            this.ITEM039.Width = 0.7875F;
            // 
            // ITEM041
            // 
            this.ITEM041.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM041.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM041.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM041.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM041.CanGrow = false;
            this.ITEM041.DataField = "ITEM041";
            this.ITEM041.Height = 0.1576389F;
            this.ITEM041.Left = 0.9847222F;
            this.ITEM041.Name = "ITEM041";
            this.ITEM041.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.ITEM041.Tag = "";
            this.ITEM041.Text = "ITEM041";
            this.ITEM041.Top = 0.8916667F;
            this.ITEM041.Width = 0.7875F;
            // 
            // ITEM042
            // 
            this.ITEM042.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM042.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM042.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM042.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM042.CanGrow = false;
            this.ITEM042.DataField = "ITEM042";
            this.ITEM042.Height = 0.1576389F;
            this.ITEM042.Left = 1.772222F;
            this.ITEM042.Name = "ITEM042";
            this.ITEM042.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.ITEM042.Tag = "";
            this.ITEM042.Text = "ITEM042";
            this.ITEM042.Top = 0.8916667F;
            this.ITEM042.Width = 0.7875F;
            // 
            // ITEM043
            // 
            this.ITEM043.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM043.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM043.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM043.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM043.CanGrow = false;
            this.ITEM043.DataField = "ITEM043";
            this.ITEM043.Height = 0.1576389F;
            this.ITEM043.Left = 2.559722F;
            this.ITEM043.Name = "ITEM043";
            this.ITEM043.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.ITEM043.Tag = "";
            this.ITEM043.Text = "ITEM043";
            this.ITEM043.Top = 0.8916667F;
            this.ITEM043.Width = 0.7875F;
            // 
            // ITEM044
            // 
            this.ITEM044.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM044.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM044.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM044.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM044.CanGrow = false;
            this.ITEM044.DataField = "ITEM044";
            this.ITEM044.Height = 0.1576389F;
            this.ITEM044.Left = 3.347222F;
            this.ITEM044.Name = "ITEM044";
            this.ITEM044.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.ITEM044.Tag = "";
            this.ITEM044.Text = "ITEM044";
            this.ITEM044.Top = 0.8916667F;
            this.ITEM044.Width = 0.7875F;
            // 
            // ITEM045
            // 
            this.ITEM045.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM045.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM045.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM045.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM045.CanGrow = false;
            this.ITEM045.DataField = "ITEM045";
            this.ITEM045.Height = 0.1576389F;
            this.ITEM045.Left = 4.134722F;
            this.ITEM045.Name = "ITEM045";
            this.ITEM045.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.ITEM045.Tag = "";
            this.ITEM045.Text = "ITEM045";
            this.ITEM045.Top = 0.8916667F;
            this.ITEM045.Width = 0.7875F;
            // 
            // ITEM046
            // 
            this.ITEM046.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM046.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM046.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM046.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM046.CanGrow = false;
            this.ITEM046.DataField = "ITEM046";
            this.ITEM046.Height = 0.1576389F;
            this.ITEM046.Left = 4.922222F;
            this.ITEM046.Name = "ITEM046";
            this.ITEM046.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.ITEM046.Tag = "";
            this.ITEM046.Text = "ITEM046";
            this.ITEM046.Top = 0.8916667F;
            this.ITEM046.Width = 0.7875F;
            // 
            // ITEM047
            // 
            this.ITEM047.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM047.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM047.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM047.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM047.CanGrow = false;
            this.ITEM047.DataField = "ITEM047";
            this.ITEM047.Height = 0.1576389F;
            this.ITEM047.Left = 5.709722F;
            this.ITEM047.Name = "ITEM047";
            this.ITEM047.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.ITEM047.Tag = "";
            this.ITEM047.Text = "ITEM047";
            this.ITEM047.Top = 0.8916667F;
            this.ITEM047.Width = 0.7875F;
            // 
            // ITEM048
            // 
            this.ITEM048.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM048.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM048.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM048.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM048.CanGrow = false;
            this.ITEM048.DataField = "ITEM048";
            this.ITEM048.Height = 0.1576389F;
            this.ITEM048.Left = 6.497222F;
            this.ITEM048.Name = "ITEM048";
            this.ITEM048.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.ITEM048.Tag = "";
            this.ITEM048.Text = "ITEM048";
            this.ITEM048.Top = 0.8916667F;
            this.ITEM048.Width = 0.7875F;
            // 
            // ITEM049
            // 
            this.ITEM049.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM049.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM049.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM049.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM049.CanGrow = false;
            this.ITEM049.DataField = "ITEM049";
            this.ITEM049.Height = 0.1576389F;
            this.ITEM049.Left = 7.284722F;
            this.ITEM049.Name = "ITEM049";
            this.ITEM049.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.ITEM049.Tag = "";
            this.ITEM049.Text = "ITEM049";
            this.ITEM049.Top = 0.8916667F;
            this.ITEM049.Width = 0.7875F;
            // 
            // ITEM051
            // 
            this.ITEM051.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM051.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM051.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM051.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM051.CanGrow = false;
            this.ITEM051.DataField = "ITEM051";
            this.ITEM051.Height = 0.1576389F;
            this.ITEM051.Left = 8.072917F;
            this.ITEM051.Name = "ITEM051";
            this.ITEM051.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5.5pt; font-weight: normal; text-ali" +
    "gn: center; vertical-align: middle; ddo-char-set: 1";
            this.ITEM051.Tag = "";
            this.ITEM051.Text = "ITEM051";
            this.ITEM051.Top = 0.7340278F;
            this.ITEM051.Width = 0.5118055F;
            // 
            // ITEM052
            // 
            this.ITEM052.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM052.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM052.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM052.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM052.CanGrow = false;
            this.ITEM052.DataField = "ITEM052";
            this.ITEM052.Height = 0.1576389F;
            this.ITEM052.Left = 8.584723F;
            this.ITEM052.Name = "ITEM052";
            this.ITEM052.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5.5pt; font-weight: normal; text-ali" +
    "gn: center; vertical-align: middle; ddo-char-set: 1";
            this.ITEM052.Tag = "";
            this.ITEM052.Text = "ITEM052";
            this.ITEM052.Top = 0.7340278F;
            this.ITEM052.Width = 0.5118055F;
            // 
            // ITEM053
            // 
            this.ITEM053.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM053.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM053.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM053.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM053.CanGrow = false;
            this.ITEM053.DataField = "ITEM053";
            this.ITEM053.Height = 0.1576389F;
            this.ITEM053.Left = 9.096528F;
            this.ITEM053.Name = "ITEM053";
            this.ITEM053.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5.5pt; font-weight: normal; text-ali" +
    "gn: center; vertical-align: middle; ddo-char-set: 1";
            this.ITEM053.Tag = "";
            this.ITEM053.Text = "ITEM053";
            this.ITEM053.Top = 0.7340278F;
            this.ITEM053.Width = 0.5118055F;
            // 
            // ITEM054
            // 
            this.ITEM054.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM054.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM054.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM054.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM054.CanGrow = false;
            this.ITEM054.DataField = "ITEM054";
            this.ITEM054.Height = 0.1576389F;
            this.ITEM054.Left = 9.608334F;
            this.ITEM054.Name = "ITEM054";
            this.ITEM054.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5.5pt; font-weight: normal; text-ali" +
    "gn: center; vertical-align: middle; ddo-char-set: 1";
            this.ITEM054.Tag = "";
            this.ITEM054.Text = "ITEM054";
            this.ITEM054.Top = 0.7340278F;
            this.ITEM054.Width = 0.5118055F;
            // 
            // ITEM055
            // 
            this.ITEM055.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM055.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM055.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM055.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM055.CanGrow = false;
            this.ITEM055.DataField = "ITEM055";
            this.ITEM055.Height = 0.1576389F;
            this.ITEM055.Left = 10.12014F;
            this.ITEM055.Name = "ITEM055";
            this.ITEM055.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5.5pt; font-weight: normal; text-ali" +
    "gn: center; vertical-align: middle; ddo-char-set: 1";
            this.ITEM055.Tag = "";
            this.ITEM055.Text = "ITEM055";
            this.ITEM055.Top = 0.7340278F;
            this.ITEM055.Width = 0.5118055F;
            // 
            // ITEM056
            // 
            this.ITEM056.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM056.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM056.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM056.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM056.CanGrow = false;
            this.ITEM056.DataField = "ITEM056";
            this.ITEM056.Height = 0.1576389F;
            this.ITEM056.Left = 8.072917F;
            this.ITEM056.Name = "ITEM056";
            this.ITEM056.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5.5pt; font-weight: normal; text-ali" +
    "gn: center; vertical-align: middle; ddo-char-set: 1";
            this.ITEM056.Tag = "";
            this.ITEM056.Text = "ITEM056";
            this.ITEM056.Top = 0.8916667F;
            this.ITEM056.Width = 0.5118055F;
            // 
            // ITEM057
            // 
            this.ITEM057.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM057.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM057.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM057.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM057.CanGrow = false;
            this.ITEM057.DataField = "ITEM057";
            this.ITEM057.Height = 0.1576389F;
            this.ITEM057.Left = 8.584723F;
            this.ITEM057.Name = "ITEM057";
            this.ITEM057.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5.5pt; font-weight: normal; text-ali" +
    "gn: center; vertical-align: middle; ddo-char-set: 1";
            this.ITEM057.Tag = "";
            this.ITEM057.Text = "ITEM057";
            this.ITEM057.Top = 0.8916667F;
            this.ITEM057.Width = 0.5118055F;
            // 
            // ITEM058
            // 
            this.ITEM058.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM058.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM058.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM058.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM058.CanGrow = false;
            this.ITEM058.DataField = "ITEM058";
            this.ITEM058.Height = 0.1576389F;
            this.ITEM058.Left = 9.096528F;
            this.ITEM058.Name = "ITEM058";
            this.ITEM058.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5.5pt; font-weight: normal; text-ali" +
    "gn: center; vertical-align: middle; ddo-char-set: 1";
            this.ITEM058.Tag = "";
            this.ITEM058.Text = "ITEM058";
            this.ITEM058.Top = 0.8916667F;
            this.ITEM058.Width = 0.5118055F;
            // 
            // ITEM059
            // 
            this.ITEM059.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM059.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM059.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM059.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM059.CanGrow = false;
            this.ITEM059.DataField = "ITEM059";
            this.ITEM059.Height = 0.1576389F;
            this.ITEM059.Left = 9.608334F;
            this.ITEM059.Name = "ITEM059";
            this.ITEM059.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5.5pt; font-weight: normal; text-ali" +
    "gn: center; vertical-align: middle; ddo-char-set: 1";
            this.ITEM059.Tag = "";
            this.ITEM059.Text = "ITEM059";
            this.ITEM059.Top = 0.8916667F;
            this.ITEM059.Width = 0.5118055F;
            // 
            // ITEM060
            // 
            this.ITEM060.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM060.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM060.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM060.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM060.CanGrow = false;
            this.ITEM060.DataField = "ITEM060";
            this.ITEM060.Height = 0.1576389F;
            this.ITEM060.Left = 10.12014F;
            this.ITEM060.Name = "ITEM060";
            this.ITEM060.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 5.5pt; font-weight: normal; text-ali" +
    "gn: center; vertical-align: middle; ddo-char-set: 1";
            this.ITEM060.Tag = "";
            this.ITEM060.Text = "ITEM060";
            this.ITEM060.Top = 0.8916667F;
            this.ITEM060.Width = 0.5118055F;
            // 
            // ラベル396
            // 
            this.ラベル396.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル396.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル396.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル396.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル396.Height = 0.4729167F;
            this.ラベル396.Left = 8.072917F;
            this.ラベル396.Name = "ラベル396";
            this.ラベル396.Style = "background-color: Silver; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-" +
    "weight: normal; text-align: center; vertical-align: middle; ddo-char-set: 1";
            this.ラベル396.Tag = "";
            this.ラベル396.Text = "勤　　怠";
            this.ラベル396.Top = 0.2611111F;
            this.ラベル396.Width = 2.559722F;
            // 
            // ラベル381
            // 
            this.ラベル381.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル381.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル381.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル381.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル381.Height = 0.7881944F;
            this.ラベル381.Left = 0F;
            this.ラベル381.Name = "ラベル381";
            this.ラベル381.Style = "background-color: Silver; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-" +
    "weight: bold; text-align: center; ddo-char-set: 1";
            this.ラベル381.Tag = "";
            this.ラベル381.Text = "\r\n区\r\n\r\n分";
            this.ラベル381.Top = 0.2610236F;
            this.ラベル381.Width = 0.1972222F;
            // 
            // ラベル384
            // 
            this.ラベル384.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル384.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル384.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル384.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル384.Height = 0.1576389F;
            this.ラベル384.Left = 2.559722F;
            this.ラベル384.Name = "ラベル384";
            this.ラベル384.Style = "background-color: Silver; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-" +
    "weight: bold; text-align: center; ddo-char-set: 1";
            this.ラベル384.Tag = "";
            this.ラベル384.Text = "性別";
            this.ラベル384.Top = 0.2611111F;
            this.ラベル384.Width = 0.39375F;
            // 
            // ラベル385
            // 
            this.ラベル385.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル385.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル385.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル385.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル385.Height = 0.1576389F;
            this.ラベル385.Left = 2.559722F;
            this.ラベル385.Name = "ラベル385";
            this.ラベル385.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.ラベル385.Tag = "";
            this.ラベル385.Text = "男";
            this.ラベル385.Top = 0.41875F;
            this.ラベル385.Width = 0.1972222F;
            // 
            // ラベル386
            // 
            this.ラベル386.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル386.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル386.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル386.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル386.Height = 0.1576389F;
            this.ラベル386.Left = 2.756944F;
            this.ラベル386.Name = "ラベル386";
            this.ラベル386.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.ラベル386.Tag = "";
            this.ラベル386.Text = "女";
            this.ラベル386.Top = 0.41875F;
            this.ラベル386.Width = 0.1972222F;
            // 
            // ラベル394
            // 
            this.ラベル394.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル394.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル394.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル394.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ラベル394.Height = 0.4729167F;
            this.ラベル394.Left = 4.725695F;
            this.ラベル394.Name = "ラベル394";
            this.ラベル394.Style = "background-color: Silver; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-" +
    "weight: bold; text-align: center; ddo-char-set: 1";
            this.ラベル394.Tag = "";
            this.ラベル394.Text = "住\r\n\r\n所";
            this.ラベル394.Top = 0.2611111F;
            this.ラベル394.Width = 0.1972222F;
            // 
            // txt001
            // 
            this.txt001.DataField = "ITEM001";
            this.txt001.Height = 0.15625F;
            this.txt001.Left = 0.004724503F;
            this.txt001.Name = "txt001";
            this.txt001.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-w" +
    "eight: normal; text-align: right; ddo-char-set: 1";
            this.txt001.Tag = "";
            this.txt001.Text = "ITEM001";
            this.txt001.Top = 0F;
            this.txt001.Visible = false;
            this.txt001.Width = 0.6397637F;
            // 
            // detail
            // 
            this.detail.CanGrow = false;
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.title,
            this.ITEM121,
            this.ITEM122,
            this.ITEM123,
            this.ITEM124,
            this.ITEM125,
            this.ITEM126,
            this.ITEM127,
            this.ITEM128,
            this.ITEM129,
            this.直線464,
            this.ITEM102,
            this.ITEM103,
            this.ITEM111,
            this.ITEM112,
            this.ITEM113,
            this.ITEM114,
            this.ITEM115,
            this.ITEM116,
            this.ITEM117,
            this.ITEM118,
            this.ITEM119,
            this.textBox1,
            this.textBox2,
            this.textBox3,
            this.textBox4,
            this.textBox5,
            this.textBox6,
            this.textBox7,
            this.textBox8,
            this.textBox9,
            this.textBox10});
            this.detail.Height = 0.2916667F;
            this.detail.Name = "detail";
            this.detail.Format += new System.EventHandler(this.detail_Format);
            // 
            // title
            // 
            this.title.DataField = "ITEM101";
            this.title.Height = 0.2913387F;
            this.title.Left = 0F;
            this.title.Name = "title";
            this.title.Style = "background-color: Silver; color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-" +
    "weight: normal; text-align: center; ddo-char-set: 1";
            this.title.Tag = "";
            this.title.Text = "ITEM101";
            this.title.Top = 0F;
            this.title.Width = 0.1972222F;
            // 
            // ITEM121
            // 
            this.ITEM121.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM121.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM121.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM121.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM121.CanGrow = false;
            this.ITEM121.DataField = "ITEM121";
            this.ITEM121.Height = 0.1458333F;
            this.ITEM121.Left = 0.9854163F;
            this.ITEM121.Name = "ITEM121";
            this.ITEM121.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.ITEM121.Tag = "";
            this.ITEM121.Text = "ITEM121";
            this.ITEM121.Top = 0.1458333F;
            this.ITEM121.Width = 0.7875F;
            // 
            // ITEM122
            // 
            this.ITEM122.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM122.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM122.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM122.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM122.CanGrow = false;
            this.ITEM122.DataField = "ITEM122";
            this.ITEM122.Height = 0.1458333F;
            this.ITEM122.Left = 1.772916F;
            this.ITEM122.Name = "ITEM122";
            this.ITEM122.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.ITEM122.Tag = "";
            this.ITEM122.Text = "ITEM122";
            this.ITEM122.Top = 0.1458333F;
            this.ITEM122.Width = 0.7875F;
            // 
            // ITEM123
            // 
            this.ITEM123.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM123.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM123.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM123.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM123.CanGrow = false;
            this.ITEM123.DataField = "ITEM123";
            this.ITEM123.Height = 0.1458333F;
            this.ITEM123.Left = 2.560416F;
            this.ITEM123.Name = "ITEM123";
            this.ITEM123.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.ITEM123.Tag = "";
            this.ITEM123.Text = "ITEM123";
            this.ITEM123.Top = 0.1458333F;
            this.ITEM123.Width = 0.7875F;
            // 
            // ITEM124
            // 
            this.ITEM124.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM124.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM124.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM124.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM124.CanGrow = false;
            this.ITEM124.DataField = "ITEM124";
            this.ITEM124.Height = 0.1458333F;
            this.ITEM124.Left = 3.347917F;
            this.ITEM124.Name = "ITEM124";
            this.ITEM124.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.ITEM124.Tag = "";
            this.ITEM124.Text = "ITEM124";
            this.ITEM124.Top = 0.1458333F;
            this.ITEM124.Width = 0.7875F;
            // 
            // ITEM125
            // 
            this.ITEM125.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM125.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM125.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM125.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM125.CanGrow = false;
            this.ITEM125.DataField = "ITEM125";
            this.ITEM125.Height = 0.1458333F;
            this.ITEM125.Left = 4.135415F;
            this.ITEM125.Name = "ITEM125";
            this.ITEM125.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.ITEM125.Tag = "";
            this.ITEM125.Text = "ITEM125";
            this.ITEM125.Top = 0.1458333F;
            this.ITEM125.Width = 0.7875F;
            // 
            // ITEM126
            // 
            this.ITEM126.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM126.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM126.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM126.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM126.CanGrow = false;
            this.ITEM126.DataField = "ITEM126";
            this.ITEM126.Height = 0.1458333F;
            this.ITEM126.Left = 4.922915F;
            this.ITEM126.Name = "ITEM126";
            this.ITEM126.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.ITEM126.Tag = "";
            this.ITEM126.Text = "ITEM126";
            this.ITEM126.Top = 0.1458333F;
            this.ITEM126.Width = 0.7875F;
            // 
            // ITEM127
            // 
            this.ITEM127.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM127.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM127.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM127.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM127.CanGrow = false;
            this.ITEM127.DataField = "ITEM127";
            this.ITEM127.Height = 0.1458333F;
            this.ITEM127.Left = 5.710416F;
            this.ITEM127.Name = "ITEM127";
            this.ITEM127.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.ITEM127.Tag = "";
            this.ITEM127.Text = "ITEM127";
            this.ITEM127.Top = 0.1458333F;
            this.ITEM127.Width = 0.7875F;
            // 
            // ITEM128
            // 
            this.ITEM128.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM128.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM128.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM128.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM128.CanGrow = false;
            this.ITEM128.DataField = "ITEM128";
            this.ITEM128.Height = 0.1458333F;
            this.ITEM128.Left = 6.497915F;
            this.ITEM128.Name = "ITEM128";
            this.ITEM128.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.ITEM128.Tag = "";
            this.ITEM128.Text = "ITEM128";
            this.ITEM128.Top = 0.1458333F;
            this.ITEM128.Width = 0.7875F;
            // 
            // ITEM129
            // 
            this.ITEM129.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM129.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM129.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM129.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM129.CanGrow = false;
            this.ITEM129.DataField = "ITEM129";
            this.ITEM129.Height = 0.1458333F;
            this.ITEM129.Left = 7.285415F;
            this.ITEM129.Name = "ITEM129";
            this.ITEM129.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.ITEM129.Tag = "";
            this.ITEM129.Text = "ITEM129";
            this.ITEM129.Top = 0.1458333F;
            this.ITEM129.Width = 0.7875F;
            // 
            // 直線464
            // 
            this.直線464.Height = 0.2916667F;
            this.直線464.Left = 0F;
            this.直線464.LineWeight = 0F;
            this.直線464.Name = "直線464";
            this.直線464.Tag = "";
            this.直線464.Top = 0F;
            this.直線464.Width = 0F;
            this.直線464.X1 = 0F;
            this.直線464.X2 = 0F;
            this.直線464.Y1 = 0F;
            this.直線464.Y2 = 0.2916667F;
            // 
            // ITEM102
            // 
            this.ITEM102.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM102.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM102.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM102.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM102.DataField = "ITEM102";
            this.ITEM102.Height = 0.2916667F;
            this.ITEM102.Left = 0.1972222F;
            this.ITEM102.Name = "ITEM102";
            this.ITEM102.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.ITEM102.Tag = "";
            this.ITEM102.Text = "ITEM102";
            this.ITEM102.Top = 0F;
            this.ITEM102.Width = 0.1972222F;
            // 
            // ITEM103
            // 
            this.ITEM103.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM103.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM103.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM103.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM103.DataField = "ITEM103";
            this.ITEM103.Height = 0.2916667F;
            this.ITEM103.Left = 0.3944445F;
            this.ITEM103.Name = "ITEM103";
            this.ITEM103.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.ITEM103.Tag = "";
            this.ITEM103.Text = "ITEM103";
            this.ITEM103.Top = 0F;
            this.ITEM103.Width = 0.5909723F;
            // 
            // ITEM111
            // 
            this.ITEM111.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM111.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM111.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM111.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM111.CanGrow = false;
            this.ITEM111.DataField = "ITEM111";
            this.ITEM111.Height = 0.1458333F;
            this.ITEM111.Left = 0.9854163F;
            this.ITEM111.Name = "ITEM111";
            this.ITEM111.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.ITEM111.Tag = "";
            this.ITEM111.Text = "ITEM111";
            this.ITEM111.Top = 0F;
            this.ITEM111.Width = 0.7875F;
            // 
            // ITEM112
            // 
            this.ITEM112.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM112.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM112.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM112.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM112.CanGrow = false;
            this.ITEM112.DataField = "ITEM112";
            this.ITEM112.Height = 0.1458333F;
            this.ITEM112.Left = 1.772916F;
            this.ITEM112.Name = "ITEM112";
            this.ITEM112.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.ITEM112.Tag = "";
            this.ITEM112.Text = "ITEM112";
            this.ITEM112.Top = 0F;
            this.ITEM112.Width = 0.7875F;
            // 
            // ITEM113
            // 
            this.ITEM113.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM113.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM113.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM113.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM113.CanGrow = false;
            this.ITEM113.DataField = "ITEM113";
            this.ITEM113.Height = 0.1458333F;
            this.ITEM113.Left = 2.560416F;
            this.ITEM113.Name = "ITEM113";
            this.ITEM113.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.ITEM113.Tag = "";
            this.ITEM113.Text = "ITEM113";
            this.ITEM113.Top = 0F;
            this.ITEM113.Width = 0.7875F;
            // 
            // ITEM114
            // 
            this.ITEM114.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM114.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM114.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM114.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM114.CanGrow = false;
            this.ITEM114.DataField = "ITEM114";
            this.ITEM114.Height = 0.1458333F;
            this.ITEM114.Left = 3.347917F;
            this.ITEM114.Name = "ITEM114";
            this.ITEM114.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.ITEM114.Tag = "";
            this.ITEM114.Text = "ITEM114";
            this.ITEM114.Top = 0F;
            this.ITEM114.Width = 0.7875F;
            // 
            // ITEM115
            // 
            this.ITEM115.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM115.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM115.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM115.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM115.CanGrow = false;
            this.ITEM115.DataField = "ITEM115";
            this.ITEM115.Height = 0.1458333F;
            this.ITEM115.Left = 4.135415F;
            this.ITEM115.Name = "ITEM115";
            this.ITEM115.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.ITEM115.Tag = "";
            this.ITEM115.Text = "ITEM115";
            this.ITEM115.Top = 0F;
            this.ITEM115.Width = 0.7875F;
            // 
            // ITEM116
            // 
            this.ITEM116.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM116.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM116.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM116.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM116.CanGrow = false;
            this.ITEM116.DataField = "ITEM116";
            this.ITEM116.Height = 0.1458333F;
            this.ITEM116.Left = 4.922915F;
            this.ITEM116.Name = "ITEM116";
            this.ITEM116.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.ITEM116.Tag = "";
            this.ITEM116.Text = "ITEM116";
            this.ITEM116.Top = 0F;
            this.ITEM116.Width = 0.7875F;
            // 
            // ITEM117
            // 
            this.ITEM117.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM117.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM117.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM117.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM117.CanGrow = false;
            this.ITEM117.DataField = "ITEM117";
            this.ITEM117.Height = 0.1458333F;
            this.ITEM117.Left = 5.710416F;
            this.ITEM117.Name = "ITEM117";
            this.ITEM117.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.ITEM117.Tag = "";
            this.ITEM117.Text = "ITEM117";
            this.ITEM117.Top = 0F;
            this.ITEM117.Width = 0.7875F;
            // 
            // ITEM118
            // 
            this.ITEM118.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM118.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM118.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM118.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM118.CanGrow = false;
            this.ITEM118.DataField = "ITEM118";
            this.ITEM118.Height = 0.1458333F;
            this.ITEM118.Left = 6.497915F;
            this.ITEM118.Name = "ITEM118";
            this.ITEM118.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.ITEM118.Tag = "";
            this.ITEM118.Text = "ITEM118";
            this.ITEM118.Top = 0F;
            this.ITEM118.Width = 0.7875F;
            // 
            // ITEM119
            // 
            this.ITEM119.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM119.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM119.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM119.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.ITEM119.CanGrow = false;
            this.ITEM119.DataField = "ITEM119";
            this.ITEM119.Height = 0.1458333F;
            this.ITEM119.Left = 7.285415F;
            this.ITEM119.Name = "ITEM119";
            this.ITEM119.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.ITEM119.Tag = "";
            this.ITEM119.Text = "ITEM119";
            this.ITEM119.Top = 0F;
            this.ITEM119.Width = 0.7875F;
            // 
            // textBox1
            // 
            this.textBox1.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox1.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox1.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox1.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox1.CanGrow = false;
            this.textBox1.DataField = "ITEM131";
            this.textBox1.Height = 0.1458333F;
            this.textBox1.Left = 8.072048F;
            this.textBox1.Name = "textBox1";
            this.textBox1.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox1.Tag = "";
            this.textBox1.Text = "ITEM131";
            this.textBox1.Top = 0F;
            this.textBox1.Width = 0.5125977F;
            // 
            // textBox2
            // 
            this.textBox2.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox2.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox2.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox2.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox2.CanGrow = false;
            this.textBox2.DataField = "ITEM132";
            this.textBox2.Height = 0.1458333F;
            this.textBox2.Left = 8.584646F;
            this.textBox2.Name = "textBox2";
            this.textBox2.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox2.Tag = "";
            this.textBox2.Text = "ITEM132";
            this.textBox2.Top = 0F;
            this.textBox2.Width = 0.5125977F;
            // 
            // textBox3
            // 
            this.textBox3.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox3.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox3.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox3.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox3.CanGrow = false;
            this.textBox3.DataField = "ITEM133";
            this.textBox3.Height = 0.1458333F;
            this.textBox3.Left = 9.09567F;
            this.textBox3.Name = "textBox3";
            this.textBox3.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox3.Tag = "";
            this.textBox3.Text = "ITEM133";
            this.textBox3.Top = 0F;
            this.textBox3.Width = 0.5125977F;
            // 
            // textBox4
            // 
            this.textBox4.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox4.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox4.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox4.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox4.CanGrow = false;
            this.textBox4.DataField = "ITEM134";
            this.textBox4.Height = 0.1458333F;
            this.textBox4.Left = 9.607481F;
            this.textBox4.Name = "textBox4";
            this.textBox4.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox4.Tag = "";
            this.textBox4.Text = "ITEM134";
            this.textBox4.Top = 0F;
            this.textBox4.Width = 0.5125977F;
            // 
            // textBox5
            // 
            this.textBox5.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox5.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox5.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox5.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox5.CanGrow = false;
            this.textBox5.DataField = "ITEM135";
            this.textBox5.Height = 0.1458333F;
            this.textBox5.Left = 10.11929F;
            this.textBox5.Name = "textBox5";
            this.textBox5.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox5.Tag = "";
            this.textBox5.Text = "ITEM135";
            this.textBox5.Top = 0F;
            this.textBox5.Width = 0.5125977F;
            // 
            // textBox6
            // 
            this.textBox6.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox6.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox6.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox6.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox6.CanGrow = false;
            this.textBox6.DataField = "ITEM136";
            this.textBox6.Height = 0.1458333F;
            this.textBox6.Left = 8.072051F;
            this.textBox6.Name = "textBox6";
            this.textBox6.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox6.Tag = "";
            this.textBox6.Text = "ITEM136";
            this.textBox6.Top = 0.1456693F;
            this.textBox6.Width = 0.5125977F;
            // 
            // textBox7
            // 
            this.textBox7.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox7.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox7.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox7.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox7.CanGrow = false;
            this.textBox7.DataField = "ITEM137";
            this.textBox7.Height = 0.1458333F;
            this.textBox7.Left = 8.584648F;
            this.textBox7.Name = "textBox7";
            this.textBox7.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox7.Tag = "";
            this.textBox7.Text = "ITEM137";
            this.textBox7.Top = 0.1456693F;
            this.textBox7.Width = 0.5125977F;
            // 
            // textBox8
            // 
            this.textBox8.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox8.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox8.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox8.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox8.CanGrow = false;
            this.textBox8.DataField = "ITEM138";
            this.textBox8.Height = 0.1458333F;
            this.textBox8.Left = 9.09567F;
            this.textBox8.Name = "textBox8";
            this.textBox8.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox8.Tag = "";
            this.textBox8.Text = "ITEM138";
            this.textBox8.Top = 0.1456693F;
            this.textBox8.Width = 0.5125977F;
            // 
            // textBox9
            // 
            this.textBox9.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox9.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox9.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox9.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox9.CanGrow = false;
            this.textBox9.DataField = "ITEM139";
            this.textBox9.Height = 0.1458333F;
            this.textBox9.Left = 9.607482F;
            this.textBox9.Name = "textBox9";
            this.textBox9.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox9.Tag = "";
            this.textBox9.Text = "ITEM139";
            this.textBox9.Top = 0.1456693F;
            this.textBox9.Width = 0.5125977F;
            // 
            // textBox10
            // 
            this.textBox10.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox10.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox10.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox10.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.textBox10.CanGrow = false;
            this.textBox10.DataField = "ITEM140";
            this.textBox10.Height = 0.1458333F;
            this.textBox10.Left = 10.1193F;
            this.textBox10.Name = "textBox10";
            this.textBox10.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox10.Tag = "";
            this.textBox10.Text = "ITEM140";
            this.textBox10.Top = 0.1456693F;
            this.textBox10.Width = 0.5125977F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            // 
            // 社員コードヘッダー
            // 
            this.社員コードヘッダー.DataField = "ITEM001";
            this.社員コードヘッダー.Height = 0F;
            this.社員コードヘッダー.Name = "社員コードヘッダー";
            // 
            // 社員コードフッダー
            // 
            this.社員コードフッダー.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.KYNR1031R_2,
            this.KYNR1031R_3,
            this.KYNR1031R_1});
            this.社員コードフッダー.Height = 2.735433F;
            this.社員コードフッダー.Name = "社員コードフッダー";
            // 
            // KYNR1031R_2
            // 
            this.KYNR1031R_2.CloseBorder = false;
            this.KYNR1031R_2.Height = 1.458333F;
            this.KYNR1031R_2.Left = 8.072835F;
            this.KYNR1031R_2.Name = "KYNR1031R_2";
            this.KYNR1031R_2.Report = null;
            this.KYNR1031R_2.ReportName = "KYNR1031R_2";
            this.KYNR1031R_2.Top = 0F;
            this.KYNR1031R_2.Width = 2.570867F;
            // 
            // KYNR1031R_3
            // 
            this.KYNR1031R_3.CloseBorder = false;
            this.KYNR1031R_3.Height = 1.256299F;
            this.KYNR1031R_3.Left = 0F;
            this.KYNR1031R_3.Name = "KYNR1031R_3";
            this.KYNR1031R_3.Report = null;
            this.KYNR1031R_3.ReportName = "KYNR1031R_3";
            this.KYNR1031R_3.Top = 1.458268F;
            this.KYNR1031R_3.Width = 10.69449F;
            // 
            // KYNR1031R_1
            // 
            this.KYNR1031R_1.CloseBorder = false;
            this.KYNR1031R_1.Height = 1.458268F;
            this.KYNR1031R_1.Left = 0F;
            this.KYNR1031R_1.Name = "KYNR1031R_1";
            this.KYNR1031R_1.Report = null;
            this.KYNR1031R_1.ReportName = "KYNR1031R_1";
            this.KYNR1031R_1.Top = 0F;
            this.KYNR1031R_1.Width = 8.062597F;
            // 
            // KYNR1031R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.2755905F;
            this.PageSettings.Margins.Left = 0.3937007F;
            this.PageSettings.Margins.Right = 0.1968504F;
            this.PageSettings.Margins.Top = 0.3937007F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.267716F;
            this.PrintWidth = 10.92708F;
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.社員コードヘッダー);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.社員コードフッダー);
            this.Sections.Add(this.pageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.ラベル389)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル382)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル395)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM010)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM011)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル380)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM013)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM016_M)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM16_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM015)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM018)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM019)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM020)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM021)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM012)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM014)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM022)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM023)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル403)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM031)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM032)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM033)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM034)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM035)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM036)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM037)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM038)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM039)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM041)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM042)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM043)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM044)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM045)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM046)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM047)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM048)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM049)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM051)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM052)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM053)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM054)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM055)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM056)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM057)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM058)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM059)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM060)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル396)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル381)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル384)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル385)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル386)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル394)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt001)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.title)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM121)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM122)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM123)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM124)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM125)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM126)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM127)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM128)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM129)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM102)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM103)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM111)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM112)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM113)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM114)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM115)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM116)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM117)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM118)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM119)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.TextBox ラベル389;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ラベル382;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ラベル395;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM010;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM011;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル380;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM013;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM016_M;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM16_F;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM015;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM018;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM019;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM020;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM021;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM012;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM014;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM022;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM023;
        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル403;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM031;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM032;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM033;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM034;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM035;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM036;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM037;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM038;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM039;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM041;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM042;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM043;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM044;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM045;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM046;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM047;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM048;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM049;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM051;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM052;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM053;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM054;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM055;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM056;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM057;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM058;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM059;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM060;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ラベル396;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ラベル381;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ラベル384;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ラベル385;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ラベル386;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ラベル394;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox title;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM121;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM122;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM123;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM124;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM125;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM126;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM127;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM128;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM129;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線464;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM102;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM103;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM111;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM112;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM113;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM114;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM115;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM116;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM117;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM118;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM119;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox10;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader 社員コードヘッダー;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter 社員コードフッダー;
        private GrapeCity.ActiveReports.SectionReportModel.SubReport KYNR1031R_2;
        private GrapeCity.ActiveReports.SectionReportModel.SubReport KYNR1031R_3;
        private GrapeCity.ActiveReports.SectionReportModel.SubReport KYNR1031R_1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txt001;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
    }
}
