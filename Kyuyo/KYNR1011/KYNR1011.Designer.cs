﻿namespace jp.co.fsi.ky.kynr1011
{
    partial class KYNR1011
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpNendo = new System.Windows.Forms.GroupBox();
            this.lblYearNendo = new System.Windows.Forms.Label();
            this.lblGengoNendo = new System.Windows.Forms.Label();
            this.txtGengoYearNendo = new jp.co.fsi.common.controls.FsiTextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.grpBukaCd = new System.Windows.Forms.GroupBox();
            this.lblBukaCdTo = new System.Windows.Forms.Label();
            this.txtBukaCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.lblBukaCdFr = new System.Windows.Forms.Label();
            this.txtBukaCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.grpBumonCd = new System.Windows.Forms.GroupBox();
            this.lblBumonCdTo = new System.Windows.Forms.Label();
            this.txtBumonCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblBumonCdFr = new System.Windows.Forms.Label();
            this.txtBumonCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.grpShainCd = new System.Windows.Forms.GroupBox();
            this.lblShainCdTo = new System.Windows.Forms.Label();
            this.txtShainCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblCodeBet = new System.Windows.Forms.Label();
            this.lblShainCdFr = new System.Windows.Forms.Label();
            this.txtShainCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.grpOrder = new System.Windows.Forms.GroupBox();
            this.rdoOrder2 = new System.Windows.Forms.RadioButton();
            this.rdoOrder1 = new System.Windows.Forms.RadioButton();
            this.grpTaisho = new System.Windows.Forms.GroupBox();
            this.rdoTaisho2 = new System.Windows.Forms.RadioButton();
            this.rdoTaisho1 = new System.Windows.Forms.RadioButton();
            this.lblMizuageShisho = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.grpNendo.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.grpBukaCd.SuspendLayout();
            this.grpBumonCd.SuspendLayout();
            this.grpShainCd.SuspendLayout();
            this.grpOrder.SuspendLayout();
            this.grpTaisho.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Text = "年末調整一覧表";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            // 
            // grpNendo
            // 
            this.grpNendo.Controls.Add(this.lblYearNendo);
            this.grpNendo.Controls.Add(this.lblGengoNendo);
            this.grpNendo.Controls.Add(this.txtGengoYearNendo);
            this.grpNendo.Controls.Add(this.lblMizuageShisho);
            this.grpNendo.Font = new System.Drawing.Font("ＭＳ ゴシック", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.grpNendo.Location = new System.Drawing.Point(12, 46);
            this.grpNendo.Name = "grpNendo";
            this.grpNendo.Size = new System.Drawing.Size(179, 74);
            this.grpNendo.TabIndex = 0;
            this.grpNendo.TabStop = false;
            this.grpNendo.Text = "年度";
            // 
            // lblYearNendo
            // 
            this.lblYearNendo.BackColor = System.Drawing.Color.Silver;
            this.lblYearNendo.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYearNendo.Location = new System.Drawing.Point(113, 37);
            this.lblYearNendo.Name = "lblYearNendo";
            this.lblYearNendo.Size = new System.Drawing.Size(40, 16);
            this.lblYearNendo.TabIndex = 2;
            this.lblYearNendo.Text = "年度";
            // 
            // lblGengoNendo
            // 
            this.lblGengoNendo.BackColor = System.Drawing.Color.Silver;
            this.lblGengoNendo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengoNendo.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengoNendo.Location = new System.Drawing.Point(28, 35);
            this.lblGengoNendo.Name = "lblGengoNendo";
            this.lblGengoNendo.Size = new System.Drawing.Size(42, 20);
            this.lblGengoNendo.TabIndex = 0;
            this.lblGengoNendo.Text = "平成";
            this.lblGengoNendo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtGengoYearNendo
            // 
            this.txtGengoYearNendo.AutoSizeFromLength = false;
            this.txtGengoYearNendo.DisplayLength = null;
            this.txtGengoYearNendo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGengoYearNendo.Location = new System.Drawing.Point(73, 33);
            this.txtGengoYearNendo.MaxLength = 2;
            this.txtGengoYearNendo.Name = "txtGengoYearNendo";
            this.txtGengoYearNendo.Size = new System.Drawing.Size(34, 23);
            this.txtGengoYearNendo.TabIndex = 1;
            this.txtGengoYearNendo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGengoYearNendo.Validating += new System.ComponentModel.CancelEventHandler(this.txtGengoYearNendo_Validating);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.grpBukaCd);
            this.groupBox1.Controls.Add(this.grpBumonCd);
            this.groupBox1.Controls.Add(this.grpShainCd);
            this.groupBox1.Font = new System.Drawing.Font("ＭＳ ゴシック", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBox1.Location = new System.Drawing.Point(12, 128);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(590, 206);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "出力範囲";
            // 
            // grpBukaCd
            // 
            this.grpBukaCd.Controls.Add(this.lblBukaCdTo);
            this.grpBukaCd.Controls.Add(this.txtBukaCdTo);
            this.grpBukaCd.Controls.Add(this.label5);
            this.grpBukaCd.Controls.Add(this.lblBukaCdFr);
            this.grpBukaCd.Controls.Add(this.txtBukaCdFr);
            this.grpBukaCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.grpBukaCd.Location = new System.Drawing.Point(5, 138);
            this.grpBukaCd.Name = "grpBukaCd";
            this.grpBukaCd.Size = new System.Drawing.Size(575, 53);
            this.grpBukaCd.TabIndex = 5;
            this.grpBukaCd.TabStop = false;
            this.grpBukaCd.Text = "部課コード";
            // 
            // lblBukaCdTo
            // 
            this.lblBukaCdTo.BackColor = System.Drawing.Color.Silver;
            this.lblBukaCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBukaCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBukaCdTo.Location = new System.Drawing.Point(365, 20);
            this.lblBukaCdTo.Name = "lblBukaCdTo";
            this.lblBukaCdTo.Size = new System.Drawing.Size(194, 20);
            this.lblBukaCdTo.TabIndex = 4;
            this.lblBukaCdTo.Text = "最　後";
            this.lblBukaCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtBukaCdTo
            // 
            this.txtBukaCdTo.AutoSizeFromLength = false;
            this.txtBukaCdTo.DisplayLength = null;
            this.txtBukaCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtBukaCdTo.Location = new System.Drawing.Point(303, 19);
            this.txtBukaCdTo.MaxLength = 4;
            this.txtBukaCdTo.Name = "txtBukaCdTo";
            this.txtBukaCdTo.Size = new System.Drawing.Size(55, 23);
            this.txtBukaCdTo.TabIndex = 3;
            this.txtBukaCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBukaCdTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtBukaCdTo_KeyDown);
            this.txtBukaCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtBukaCdTo_Validating);
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label5.Location = new System.Drawing.Point(276, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(18, 20);
            this.label5.TabIndex = 2;
            this.label5.Text = "～";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblBukaCdFr
            // 
            this.lblBukaCdFr.BackColor = System.Drawing.Color.Silver;
            this.lblBukaCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBukaCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBukaCdFr.Location = new System.Drawing.Point(76, 20);
            this.lblBukaCdFr.Name = "lblBukaCdFr";
            this.lblBukaCdFr.Size = new System.Drawing.Size(194, 20);
            this.lblBukaCdFr.TabIndex = 1;
            this.lblBukaCdFr.Text = "先　頭";
            this.lblBukaCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtBukaCdFr
            // 
            this.txtBukaCdFr.AutoSizeFromLength = false;
            this.txtBukaCdFr.DisplayLength = null;
            this.txtBukaCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtBukaCdFr.Location = new System.Drawing.Point(15, 19);
            this.txtBukaCdFr.MaxLength = 4;
            this.txtBukaCdFr.Name = "txtBukaCdFr";
            this.txtBukaCdFr.Size = new System.Drawing.Size(55, 23);
            this.txtBukaCdFr.TabIndex = 0;
            this.txtBukaCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBukaCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtBukaCdFr_Validating);
            // 
            // grpBumonCd
            // 
            this.grpBumonCd.Controls.Add(this.lblBumonCdTo);
            this.grpBumonCd.Controls.Add(this.txtBumonCdTo);
            this.grpBumonCd.Controls.Add(this.label2);
            this.grpBumonCd.Controls.Add(this.lblBumonCdFr);
            this.grpBumonCd.Controls.Add(this.txtBumonCdFr);
            this.grpBumonCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.grpBumonCd.Location = new System.Drawing.Point(6, 79);
            this.grpBumonCd.Name = "grpBumonCd";
            this.grpBumonCd.Size = new System.Drawing.Size(575, 53);
            this.grpBumonCd.TabIndex = 4;
            this.grpBumonCd.TabStop = false;
            this.grpBumonCd.Text = "部門コード";
            // 
            // lblBumonCdTo
            // 
            this.lblBumonCdTo.BackColor = System.Drawing.Color.Silver;
            this.lblBumonCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBumonCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBumonCdTo.Location = new System.Drawing.Point(365, 20);
            this.lblBumonCdTo.Name = "lblBumonCdTo";
            this.lblBumonCdTo.Size = new System.Drawing.Size(194, 20);
            this.lblBumonCdTo.TabIndex = 4;
            this.lblBumonCdTo.Text = "最　後";
            this.lblBumonCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtBumonCdTo
            // 
            this.txtBumonCdTo.AutoSizeFromLength = false;
            this.txtBumonCdTo.DisplayLength = null;
            this.txtBumonCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtBumonCdTo.Location = new System.Drawing.Point(303, 19);
            this.txtBumonCdTo.MaxLength = 4;
            this.txtBumonCdTo.Name = "txtBumonCdTo";
            this.txtBumonCdTo.Size = new System.Drawing.Size(55, 23);
            this.txtBumonCdTo.TabIndex = 3;
            this.txtBumonCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBumonCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtBumonCdTo_Validating);
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(276, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(18, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "～";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblBumonCdFr
            // 
            this.lblBumonCdFr.BackColor = System.Drawing.Color.Silver;
            this.lblBumonCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBumonCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBumonCdFr.Location = new System.Drawing.Point(76, 20);
            this.lblBumonCdFr.Name = "lblBumonCdFr";
            this.lblBumonCdFr.Size = new System.Drawing.Size(194, 20);
            this.lblBumonCdFr.TabIndex = 1;
            this.lblBumonCdFr.Text = "先　頭";
            this.lblBumonCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtBumonCdFr
            // 
            this.txtBumonCdFr.AutoSizeFromLength = false;
            this.txtBumonCdFr.DisplayLength = null;
            this.txtBumonCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtBumonCdFr.Location = new System.Drawing.Point(15, 19);
            this.txtBumonCdFr.MaxLength = 4;
            this.txtBumonCdFr.Name = "txtBumonCdFr";
            this.txtBumonCdFr.Size = new System.Drawing.Size(55, 23);
            this.txtBumonCdFr.TabIndex = 0;
            this.txtBumonCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBumonCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtBumonCdFr_Validating);
            // 
            // grpShainCd
            // 
            this.grpShainCd.Controls.Add(this.lblShainCdTo);
            this.grpShainCd.Controls.Add(this.txtShainCdTo);
            this.grpShainCd.Controls.Add(this.lblCodeBet);
            this.grpShainCd.Controls.Add(this.lblShainCdFr);
            this.grpShainCd.Controls.Add(this.txtShainCdFr);
            this.grpShainCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.grpShainCd.Location = new System.Drawing.Point(6, 20);
            this.grpShainCd.Name = "grpShainCd";
            this.grpShainCd.Size = new System.Drawing.Size(575, 53);
            this.grpShainCd.TabIndex = 3;
            this.grpShainCd.TabStop = false;
            this.grpShainCd.Text = "社員コード";
            // 
            // lblShainCdTo
            // 
            this.lblShainCdTo.BackColor = System.Drawing.Color.Silver;
            this.lblShainCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShainCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShainCdTo.Location = new System.Drawing.Point(365, 20);
            this.lblShainCdTo.Name = "lblShainCdTo";
            this.lblShainCdTo.Size = new System.Drawing.Size(194, 20);
            this.lblShainCdTo.TabIndex = 4;
            this.lblShainCdTo.Text = "最　後";
            this.lblShainCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShainCdTo
            // 
            this.txtShainCdTo.AutoSizeFromLength = false;
            this.txtShainCdTo.DisplayLength = null;
            this.txtShainCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShainCdTo.Location = new System.Drawing.Point(303, 19);
            this.txtShainCdTo.MaxLength = 4;
            this.txtShainCdTo.Name = "txtShainCdTo";
            this.txtShainCdTo.Size = new System.Drawing.Size(55, 23);
            this.txtShainCdTo.TabIndex = 3;
            this.txtShainCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShainCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtShainCdTo_Validating);
            // 
            // lblCodeBet
            // 
            this.lblCodeBet.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCodeBet.Location = new System.Drawing.Point(276, 20);
            this.lblCodeBet.Name = "lblCodeBet";
            this.lblCodeBet.Size = new System.Drawing.Size(18, 20);
            this.lblCodeBet.TabIndex = 2;
            this.lblCodeBet.Text = "～";
            this.lblCodeBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShainCdFr
            // 
            this.lblShainCdFr.BackColor = System.Drawing.Color.Silver;
            this.lblShainCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShainCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShainCdFr.Location = new System.Drawing.Point(76, 20);
            this.lblShainCdFr.Name = "lblShainCdFr";
            this.lblShainCdFr.Size = new System.Drawing.Size(194, 20);
            this.lblShainCdFr.TabIndex = 1;
            this.lblShainCdFr.Text = "先　頭";
            this.lblShainCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShainCdFr
            // 
            this.txtShainCdFr.AutoSizeFromLength = false;
            this.txtShainCdFr.DisplayLength = null;
            this.txtShainCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShainCdFr.Location = new System.Drawing.Point(15, 19);
            this.txtShainCdFr.MaxLength = 4;
            this.txtShainCdFr.Name = "txtShainCdFr";
            this.txtShainCdFr.Size = new System.Drawing.Size(55, 23);
            this.txtShainCdFr.TabIndex = 0;
            this.txtShainCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShainCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtShainCdFr_Validating);
            // 
            // grpOrder
            // 
            this.grpOrder.Controls.Add(this.rdoOrder2);
            this.grpOrder.Controls.Add(this.rdoOrder1);
            this.grpOrder.Font = new System.Drawing.Font("ＭＳ ゴシック", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.grpOrder.Location = new System.Drawing.Point(422, 46);
            this.grpOrder.Name = "grpOrder";
            this.grpOrder.Size = new System.Drawing.Size(180, 74);
            this.grpOrder.TabIndex = 2;
            this.grpOrder.TabStop = false;
            this.grpOrder.Text = "出力順";
            // 
            // rdoOrder2
            // 
            this.rdoOrder2.AutoSize = true;
            this.rdoOrder2.Checked = true;
            this.rdoOrder2.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoOrder2.Location = new System.Drawing.Point(94, 32);
            this.rdoOrder2.Name = "rdoOrder2";
            this.rdoOrder2.Size = new System.Drawing.Size(74, 20);
            this.rdoOrder2.TabIndex = 1;
            this.rdoOrder2.TabStop = true;
            this.rdoOrder2.Text = "部門順";
            this.rdoOrder2.UseVisualStyleBackColor = true;
            // 
            // rdoOrder1
            // 
            this.rdoOrder1.AutoSize = true;
            this.rdoOrder1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoOrder1.Location = new System.Drawing.Point(9, 32);
            this.rdoOrder1.Name = "rdoOrder1";
            this.rdoOrder1.Size = new System.Drawing.Size(74, 20);
            this.rdoOrder1.TabIndex = 0;
            this.rdoOrder1.TabStop = true;
            this.rdoOrder1.Text = "社員順";
            this.rdoOrder1.UseVisualStyleBackColor = true;
            // 
            // grpTaisho
            // 
            this.grpTaisho.Controls.Add(this.rdoTaisho2);
            this.grpTaisho.Controls.Add(this.rdoTaisho1);
            this.grpTaisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.grpTaisho.Location = new System.Drawing.Point(202, 46);
            this.grpTaisho.Name = "grpTaisho";
            this.grpTaisho.Size = new System.Drawing.Size(207, 74);
            this.grpTaisho.TabIndex = 1;
            this.grpTaisho.TabStop = false;
            this.grpTaisho.Text = "対象者の印字";
            // 
            // rdoTaisho2
            // 
            this.rdoTaisho2.AutoSize = true;
            this.rdoTaisho2.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoTaisho2.Location = new System.Drawing.Point(134, 32);
            this.rdoTaisho2.Name = "rdoTaisho2";
            this.rdoTaisho2.Size = new System.Drawing.Size(58, 20);
            this.rdoTaisho2.TabIndex = 3;
            this.rdoTaisho2.TabStop = true;
            this.rdoTaisho2.Text = "全て";
            this.rdoTaisho2.UseVisualStyleBackColor = true;
            // 
            // rdoTaisho1
            // 
            this.rdoTaisho1.AutoSize = true;
            this.rdoTaisho1.Checked = true;
            this.rdoTaisho1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoTaisho1.Location = new System.Drawing.Point(14, 32);
            this.rdoTaisho1.Name = "rdoTaisho1";
            this.rdoTaisho1.Size = new System.Drawing.Size(106, 20);
            this.rdoTaisho1.TabIndex = 2;
            this.rdoTaisho1.TabStop = true;
            this.rdoTaisho1.Text = "対象者のみ";
            this.rdoTaisho1.UseVisualStyleBackColor = true;
            // 
            // lblMizuageShisho
            // 
            this.lblMizuageShisho.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblMizuageShisho.Location = new System.Drawing.Point(22, 31);
            this.lblMizuageShisho.Name = "lblMizuageShisho";
            this.lblMizuageShisho.Size = new System.Drawing.Size(137, 28);
            this.lblMizuageShisho.TabIndex = 8;
            this.lblMizuageShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // KYNR1011
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.grpTaisho);
            this.Controls.Add(this.grpOrder);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.grpNendo);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "KYNR1011";
            this.Text = "年末調整一覧表";
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.grpNendo, 0);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.Controls.SetChildIndex(this.grpOrder, 0);
            this.Controls.SetChildIndex(this.grpTaisho, 0);
            this.pnlDebug.ResumeLayout(false);
            this.grpNendo.ResumeLayout(false);
            this.grpNendo.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.grpBukaCd.ResumeLayout(false);
            this.grpBukaCd.PerformLayout();
            this.grpBumonCd.ResumeLayout(false);
            this.grpBumonCd.PerformLayout();
            this.grpShainCd.ResumeLayout(false);
            this.grpShainCd.PerformLayout();
            this.grpOrder.ResumeLayout(false);
            this.grpOrder.PerformLayout();
            this.grpTaisho.ResumeLayout(false);
            this.grpTaisho.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpNendo;
        private System.Windows.Forms.Label lblYearNendo;
        private System.Windows.Forms.Label lblGengoNendo;
        private jp.co.fsi.common.controls.FsiTextBox txtGengoYearNendo;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox grpOrder;
        private System.Windows.Forms.RadioButton rdoOrder2;
        private System.Windows.Forms.RadioButton rdoOrder1;
        private System.Windows.Forms.GroupBox grpTaisho;
        private System.Windows.Forms.RadioButton rdoTaisho2;
        private System.Windows.Forms.RadioButton rdoTaisho1;
        private System.Windows.Forms.GroupBox grpBukaCd;
        private System.Windows.Forms.Label lblBukaCdTo;
        private common.controls.FsiTextBox txtBukaCdTo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblBukaCdFr;
        private common.controls.FsiTextBox txtBukaCdFr;
        private System.Windows.Forms.GroupBox grpBumonCd;
        private System.Windows.Forms.Label lblBumonCdTo;
        private common.controls.FsiTextBox txtBumonCdTo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblBumonCdFr;
        private common.controls.FsiTextBox txtBumonCdFr;
        private System.Windows.Forms.GroupBox grpShainCd;
        private System.Windows.Forms.Label lblShainCdTo;
        private common.controls.FsiTextBox txtShainCdTo;
        private System.Windows.Forms.Label lblCodeBet;
        private System.Windows.Forms.Label lblShainCdFr;
        private common.controls.FsiTextBox txtShainCdFr;
        private System.Windows.Forms.Label lblMizuageShisho;
    }
}