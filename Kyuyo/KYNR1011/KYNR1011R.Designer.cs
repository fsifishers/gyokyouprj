﻿namespace jp.co.fsi.ky.kynr1011
{
    /// <summary>
    /// KYNR1011R の帳票
    /// </summary>
    partial class KYNR1011R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(KYNR1011R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.shape1 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.ラベル0 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.ITEM01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line19 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line20 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line21 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line22 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line23 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line24 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line26 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line27 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.label28 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label30 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line28 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.label31 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label32 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line29 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.label34 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label37 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label38 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label39 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label40 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label41 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label42 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label43 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label44 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line30 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line32 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line33 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line34 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line31 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line35 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line36 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line37 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line38 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.label45 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label47 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label48 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label49 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label50 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line39 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.label51 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line40 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.label53 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label56 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line41 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line42 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line43 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line44 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line45 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.label57 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line46 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line47 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.label58 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line48 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line49 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.label46 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line50 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.label35 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label36 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line25 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtToday = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label52 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox55 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.line51 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line52 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line53 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line54 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line55 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line56 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line57 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line58 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line59 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line60 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line61 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line62 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line63 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line64 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line65 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line66 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line67 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line68 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line69 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line70 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line71 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line72 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line73 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line74 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line75 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line76 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line77 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line78 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line79 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line80 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line81 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line82 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line83 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line84 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line85 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line86 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line87 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line88 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line89 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line90 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line91 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line92 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox22 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox23 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox24 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox25 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox26 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox27 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox28 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox29 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox30 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox32 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox33 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox34 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox35 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox36 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox37 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox38 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox39 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox40 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox41 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtkubun1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtkubun2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtkubun3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtkubun4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtkubun5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtkubun6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtkubun7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtkubun8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox51 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lbl1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lbl2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lbl3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox31 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox50 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox52 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox53 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox54 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lbl4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label54 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label55 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label59 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label60 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            ((System.ComponentModel.ISupportInitialize)(this.ラベル0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label56)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label57)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label58)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtkubun1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtkubun2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtkubun3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtkubun4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtkubun5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtkubun6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtkubun7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtkubun8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label59)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label60)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.shape1,
            this.ラベル0,
            this.ITEM01,
            this.textBox2,
            this.label1,
            this.line1,
            this.line2,
            this.label2,
            this.label3,
            this.label4,
            this.label5,
            this.label6,
            this.label7,
            this.label8,
            this.label9,
            this.label10,
            this.label11,
            this.label12,
            this.label13,
            this.label14,
            this.label15,
            this.label16,
            this.label17,
            this.label18,
            this.label19,
            this.label20,
            this.label21,
            this.label22,
            this.label23,
            this.label24,
            this.label25,
            this.label26,
            this.label27,
            this.line3,
            this.line4,
            this.line5,
            this.line6,
            this.line7,
            this.line8,
            this.line9,
            this.line10,
            this.line11,
            this.line12,
            this.line13,
            this.line14,
            this.line15,
            this.line17,
            this.line18,
            this.line19,
            this.line20,
            this.line21,
            this.line22,
            this.line23,
            this.line24,
            this.line26,
            this.line27,
            this.label28,
            this.label29,
            this.label30,
            this.line28,
            this.label31,
            this.label32,
            this.label33,
            this.line29,
            this.label34,
            this.label37,
            this.label38,
            this.label39,
            this.label40,
            this.label41,
            this.label42,
            this.label43,
            this.label44,
            this.line30,
            this.line32,
            this.line33,
            this.line34,
            this.line31,
            this.line35,
            this.line36,
            this.line37,
            this.line38,
            this.label45,
            this.label47,
            this.label48,
            this.label49,
            this.label50,
            this.line39,
            this.label51,
            this.line40,
            this.label53,
            this.label56,
            this.line41,
            this.line42,
            this.line43,
            this.line44,
            this.line45,
            this.label57,
            this.line46,
            this.line47,
            this.label58,
            this.line48,
            this.line49,
            this.label46,
            this.line50,
            this.label35,
            this.label36,
            this.line25,
            this.txtToday,
            this.label52,
            this.textBox55});
            this.pageHeader.Height = 1.313102F;
            this.pageHeader.Name = "pageHeader";
            this.pageHeader.Format += new System.EventHandler(this.pageHeader_Format);
            // 
            // shape1
            // 
            this.shape1.BackColor = System.Drawing.Color.Aqua;
            this.shape1.Height = 0.7086615F;
            this.shape1.Left = 0F;
            this.shape1.Name = "shape1";
            this.shape1.RoundingRadius = 9.999999F;
            this.shape1.Top = 0.5905512F;
            this.shape1.Width = 11.34016F;
            // 
            // ラベル0
            // 
            this.ラベル0.Height = 0.2291667F;
            this.ラベル0.HyperLink = null;
            this.ラベル0.Left = 5.173623F;
            this.ラベル0.Name = "ラベル0";
            this.ラベル0.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 16.75pt; font-weight: bold; text-ali" +
    "gn: left; ddo-char-set: 1";
            this.ラベル0.Tag = "";
            this.ラベル0.Text = "年末調整一覧表";
            this.ラベル0.Top = 0.2448819F;
            this.ラベル0.Width = 1.677165F;
            // 
            // ITEM01
            // 
            this.ITEM01.DataField = "ITEM001";
            this.ITEM01.Height = 0.157874F;
            this.ITEM01.Left = 0.1980315F;
            this.ITEM01.MultiLine = false;
            this.ITEM01.Name = "ITEM01";
            this.ITEM01.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: normal; text-a" +
    "lign: left; ddo-char-set: 128";
            this.ITEM01.Tag = "";
            this.ITEM01.Text = "ITEM001";
            this.ITEM01.Top = 0.1555118F;
            this.ITEM01.Width = 2.975197F;
            // 
            // textBox2
            // 
            this.textBox2.DataField = "ITEM002";
            this.textBox2.Height = 0.157874F;
            this.textBox2.Left = 3.173229F;
            this.textBox2.MultiLine = false;
            this.textBox2.Name = "textBox2";
            this.textBox2.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: normal; text-a" +
    "lign: right; ddo-char-set: 128";
            this.textBox2.Tag = "";
            this.textBox2.Text = "ITEM002";
            this.textBox2.Top = 0.1555119F;
            this.textBox2.Width = 0.9968507F;
            // 
            // label1
            // 
            this.label1.Height = 0.2291667F;
            this.label1.HyperLink = null;
            this.label1.Left = 4.170079F;
            this.label1.Name = "label1";
            this.label1.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: normal; text-a" +
    "lign: left; ddo-char-set: 1";
            this.label1.Tag = "";
            this.label1.Text = "分\r\n";
            this.label1.Top = 0.1555118F;
            this.label1.Width = 0.1875992F;
            // 
            // line1
            // 
            this.line1.Height = 0F;
            this.line1.Left = 0F;
            this.line1.LineWeight = 2F;
            this.line1.Name = "line1";
            this.line1.Top = 0.5905512F;
            this.line1.Width = 11.33858F;
            this.line1.X1 = 0F;
            this.line1.X2 = 11.33858F;
            this.line1.Y1 = 0.5905512F;
            this.line1.Y2 = 0.5905512F;
            // 
            // line2
            // 
            this.line2.Height = 0F;
            this.line2.Left = 0F;
            this.line2.LineWeight = 2F;
            this.line2.Name = "line2";
            this.line2.Top = 1.299213F;
            this.line2.Width = 11.33858F;
            this.line2.X1 = 0F;
            this.line2.X2 = 11.33858F;
            this.line2.Y1 = 1.299213F;
            this.line2.Y2 = 1.299213F;
            // 
            // label2
            // 
            this.label2.Height = 0.2291667F;
            this.label2.HyperLink = null;
            this.label2.Left = 0F;
            this.label2.Name = "label2";
            this.label2.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: normal; text-a" +
    "lign: center; ddo-char-set: 1";
            this.label2.Tag = "";
            this.label2.Text = "コード";
            this.label2.Top = 0.802756F;
            this.label2.Width = 0.4984252F;
            // 
            // label3
            // 
            this.label3.Height = 0.2291667F;
            this.label3.HyperLink = null;
            this.label3.Left = 0.5417323F;
            this.label3.Name = "label3";
            this.label3.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: normal; text-a" +
    "lign: center; ddo-char-set: 1";
            this.label3.Tag = "";
            this.label3.Text = "氏　　　　名";
            this.label3.Top = 0.802756F;
            this.label3.Width = 1.104331F;
            // 
            // label4
            // 
            this.label4.Height = 0.2291667F;
            this.label4.HyperLink = null;
            this.label4.Left = 1.646063F;
            this.label4.Name = "label4";
            this.label4.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-weight: normal; text-align" +
    ": center; vertical-align: middle; ddo-char-set: 1";
            this.label4.Tag = "";
            this.label4.Text = "給与等金額";
            this.label4.Top = 0.6141733F;
            this.label4.Width = 0.9224411F;
            // 
            // label5
            // 
            this.label5.Height = 0.2291667F;
            this.label5.HyperLink = null;
            this.label5.Left = 1.646063F;
            this.label5.Name = "label5";
            this.label5.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-weight: normal; text-align" +
    ": center; vertical-align: middle; ddo-char-set: 1";
            this.label5.Tag = "";
            this.label5.Text = "社会保険（給与）";
            this.label5.Top = 0.8433071F;
            this.label5.Width = 0.9224411F;
            // 
            // label6
            // 
            this.label6.Height = 0.2291667F;
            this.label6.HyperLink = null;
            this.label6.Left = 1.646063F;
            this.label6.Name = "label6";
            this.label6.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-weight: normal; text-align" +
    ": center; vertical-align: middle; ddo-char-set: 1";
            this.label6.Tag = "";
            this.label6.Text = "課税所得金額";
            this.label6.Top = 1.072441F;
            this.label6.Width = 0.9224411F;
            // 
            // label7
            // 
            this.label7.Height = 0.2291667F;
            this.label7.HyperLink = null;
            this.label7.Left = 2.568504F;
            this.label7.Name = "label7";
            this.label7.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-weight: normal; text-align" +
    ": center; vertical-align: middle; ddo-char-set: 1";
            this.label7.Tag = "";
            this.label7.Text = "賞与等金額";
            this.label7.Top = 0.6141733F;
            this.label7.Width = 0.9173229F;
            // 
            // label8
            // 
            this.label8.Height = 0.2291667F;
            this.label8.HyperLink = null;
            this.label8.Left = 2.568504F;
            this.label8.Name = "label8";
            this.label8.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-weight: normal; text-align" +
    ": center; vertical-align: middle; ddo-char-set: 1";
            this.label8.Tag = "";
            this.label8.Text = "社会保険（申告）";
            this.label8.Top = 0.8433071F;
            this.label8.Width = 0.9173229F;
            // 
            // label9
            // 
            this.label9.Height = 0.2291667F;
            this.label9.HyperLink = null;
            this.label9.Left = 2.568504F;
            this.label9.Name = "label9";
            this.label9.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-weight: normal; text-align" +
    ": center; vertical-align: middle; ddo-char-set: 1";
            this.label9.Tag = "";
            this.label9.Text = "年　税　額";
            this.label9.Top = 1.072441F;
            this.label9.Width = 0.9173229F;
            // 
            // label10
            // 
            this.label10.Height = 0.2291667F;
            this.label10.HyperLink = null;
            this.label10.Left = 3.498425F;
            this.label10.Name = "label10";
            this.label10.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-weight: normal; text-align" +
    ": center; vertical-align: middle; ddo-char-set: 1";
            this.label10.Tag = "";
            this.label10.Text = "給与・賞与合計";
            this.label10.Top = 0.6141733F;
            this.label10.Width = 0.8082674F;
            // 
            // label11
            // 
            this.label11.Height = 0.2291667F;
            this.label11.HyperLink = null;
            this.label11.Left = 3.498425F;
            this.label11.Name = "label11";
            this.label11.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-weight: normal; text-align" +
    ": center; vertical-align: middle; ddo-char-set: 1";
            this.label11.Tag = "";
            this.label11.Text = "小規模企業共済";
            this.label11.Top = 0.8433071F;
            this.label11.Width = 0.8082674F;
            // 
            // label12
            // 
            this.label12.Height = 0.2291667F;
            this.label12.HyperLink = null;
            this.label12.Left = 3.498425F;
            this.label12.Name = "label12";
            this.label12.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-weight: normal; text-align" +
    ": center; vertical-align: middle; ddo-char-set: 1";
            this.label12.Tag = "";
            this.label12.Text = "住宅借入等特別";
            this.label12.Top = 1.072441F;
            this.label12.Width = 0.8082674F;
            // 
            // label13
            // 
            this.label13.Height = 0.2291667F;
            this.label13.HyperLink = null;
            this.label13.Left = 4.33504F;
            this.label13.Name = "label13";
            this.label13.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-weight: normal; text-align" +
    ": center; vertical-align: middle; ddo-char-set: 1";
            this.label13.Tag = "";
            this.label13.Text = "給与所得控除";
            this.label13.Top = 0.6141733F;
            this.label13.Width = 0.7559054F;
            // 
            // label14
            // 
            this.label14.Height = 0.2291667F;
            this.label14.HyperLink = null;
            this.label14.Left = 4.33504F;
            this.label14.Name = "label14";
            this.label14.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-weight: normal; text-align" +
    ": center; vertical-align: middle; ddo-char-set: 1";
            this.label14.Tag = "";
            this.label14.Text = "生命保険料";
            this.label14.Top = 0.8433071F;
            this.label14.Width = 0.7559054F;
            // 
            // label15
            // 
            this.label15.Height = 0.2291667F;
            this.label15.HyperLink = null;
            this.label15.Left = 4.33504F;
            this.label15.Name = "label15";
            this.label15.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-weight: normal; text-align" +
    ": center; vertical-align: middle; ddo-char-set: 1";
            this.label15.Tag = "";
            this.label15.Text = "差引年税額";
            this.label15.Top = 1.072441F;
            this.label15.Width = 0.7559052F;
            // 
            // label16
            // 
            this.label16.Height = 0.2291667F;
            this.label16.HyperLink = null;
            this.label16.Left = 5.090945F;
            this.label16.Name = "label16";
            this.label16.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-weight: normal; text-align" +
    ": center; vertical-align: middle; ddo-char-set: 1";
            this.label16.Tag = "";
            this.label16.Text = "";
            this.label16.Top = 0.6141733F;
            this.label16.Width = 0.6547244F;
            // 
            // label17
            // 
            this.label17.Height = 0.2291667F;
            this.label17.HyperLink = null;
            this.label17.Left = 5.090945F;
            this.label17.Name = "label17";
            this.label17.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-weight: normal; text-align" +
    ": center; vertical-align: middle; ddo-char-set: 1";
            this.label17.Tag = "";
            this.label17.Text = "地震保険料";
            this.label17.Top = 0.8433071F;
            this.label17.Width = 0.6547244F;
            // 
            // label18
            // 
            this.label18.Height = 0.2291667F;
            this.label18.HyperLink = null;
            this.label18.Left = 5.090945F;
            this.label18.Name = "label18";
            this.label18.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-weight: normal; text-align" +
    ": center; vertical-align: middle; ddo-char-set: 1";
            this.label18.Tag = "";
            this.label18.Text = "";
            this.label18.Top = 1.072441F;
            this.label18.Width = 0.6547244F;
            // 
            // label19
            // 
            this.label19.Height = 0.2291667F;
            this.label19.HyperLink = null;
            this.label19.Left = 5.74567F;
            this.label19.Name = "label19";
            this.label19.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-weight: normal; text-align" +
    ": center; vertical-align: middle; ddo-char-set: 1";
            this.label19.Tag = "";
            this.label19.Text = "調整課税給与額";
            this.label19.Top = 0.6141733F;
            this.label19.Width = 0.8204725F;
            // 
            // label20
            // 
            this.label20.Height = 0.2291667F;
            this.label20.HyperLink = null;
            this.label20.Left = 5.74567F;
            this.label20.Name = "label20";
            this.label20.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-weight: normal; text-align" +
    ": center; vertical-align: middle; ddo-char-set: 1";
            this.label20.Tag = "";
            this.label20.Text = "配偶者特別";
            this.label20.Top = 0.8433071F;
            this.label20.Width = 0.8204725F;
            // 
            // label21
            // 
            this.label21.Height = 0.2291667F;
            this.label21.HyperLink = null;
            this.label21.Left = 5.74567F;
            this.label21.Name = "label21";
            this.label21.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-weight: normal; text-align" +
    ": center; vertical-align: middle; ddo-char-set: 1";
            this.label21.Tag = "";
            this.label21.Text = "徴収年税額";
            this.label21.Top = 1.072441F;
            this.label21.Width = 0.8204725F;
            // 
            // label22
            // 
            this.label22.Height = 0.2291667F;
            this.label22.HyperLink = null;
            this.label22.Left = 6.566142F;
            this.label22.Name = "label22";
            this.label22.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-weight: normal; text-align" +
    ": center; vertical-align: middle; ddo-char-set: 1";
            this.label22.Tag = "";
            this.label22.Text = "調整社会保険料";
            this.label22.Top = 0.6141733F;
            this.label22.Width = 0.8385831F;
            // 
            // label23
            // 
            this.label23.Height = 0.2291667F;
            this.label23.HyperLink = null;
            this.label23.Left = 6.566142F;
            this.label23.Name = "label23";
            this.label23.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-weight: normal; text-align" +
    ": center; vertical-align: middle; ddo-char-set: 1";
            this.label23.Tag = "";
            this.label23.Text = "配偶・扶養等";
            this.label23.Top = 0.8433071F;
            this.label23.Width = 0.8385831F;
            // 
            // label24
            // 
            this.label24.Height = 0.2291667F;
            this.label24.HyperLink = null;
            this.label24.Left = 6.566142F;
            this.label24.Name = "label24";
            this.label24.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-weight: normal; text-align" +
    ": center; vertical-align: middle; ddo-char-set: 1";
            this.label24.Tag = "";
            this.label24.Text = "徴収税額";
            this.label24.Top = 1.072441F;
            this.label24.Width = 0.8385831F;
            // 
            // label25
            // 
            this.label25.Height = 0.2291667F;
            this.label25.HyperLink = null;
            this.label25.Left = 7.404725F;
            this.label25.Name = "label25";
            this.label25.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-weight: normal; text-align" +
    ": center; vertical-align: middle; ddo-char-set: 1";
            this.label25.Tag = "";
            this.label25.Text = "調整所得税額";
            this.label25.Top = 0.6141733F;
            this.label25.Width = 0.7448817F;
            // 
            // label26
            // 
            this.label26.Height = 0.2291667F;
            this.label26.HyperLink = null;
            this.label26.Left = 7.404725F;
            this.label26.Name = "label26";
            this.label26.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-weight: normal; text-align" +
    ": center; vertical-align: middle; ddo-char-set: 1";
            this.label26.Tag = "";
            this.label26.Text = "所得控除合計";
            this.label26.Top = 0.8433071F;
            this.label26.Width = 0.7448817F;
            // 
            // label27
            // 
            this.label27.Height = 0.2291667F;
            this.label27.HyperLink = null;
            this.label27.Left = 7.404725F;
            this.label27.Name = "label27";
            this.label27.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-weight: normal; text-align" +
    ": center; vertical-align: middle; ddo-char-set: 1";
            this.label27.Tag = "";
            this.label27.Text = "差引過不足額";
            this.label27.Top = 1.072441F;
            this.label27.Width = 0.7448817F;
            // 
            // line3
            // 
            this.line3.Height = 0.7047238F;
            this.line3.Left = 0.4984252F;
            this.line3.LineWeight = 1F;
            this.line3.Name = "line3";
            this.line3.Top = 0.5944882F;
            this.line3.Width = 0F;
            this.line3.X1 = 0.4984252F;
            this.line3.X2 = 0.4984252F;
            this.line3.Y1 = 0.5944882F;
            this.line3.Y2 = 1.299212F;
            // 
            // line4
            // 
            this.line4.Height = 0.7110237F;
            this.line4.Left = 1.646063F;
            this.line4.LineWeight = 1F;
            this.line4.Name = "line4";
            this.line4.Top = 0.5905512F;
            this.line4.Width = 0F;
            this.line4.X1 = 1.646063F;
            this.line4.X2 = 1.646063F;
            this.line4.Y1 = 0.5905512F;
            this.line4.Y2 = 1.301575F;
            // 
            // line5
            // 
            this.line5.Height = 0.7110237F;
            this.line5.Left = 2.568504F;
            this.line5.LineWeight = 1F;
            this.line5.Name = "line5";
            this.line5.Top = 0.5905512F;
            this.line5.Width = 0F;
            this.line5.X1 = 2.568504F;
            this.line5.X2 = 2.568504F;
            this.line5.Y1 = 0.5905512F;
            this.line5.Y2 = 1.301575F;
            // 
            // line6
            // 
            this.line6.Height = 0.7110237F;
            this.line6.Left = 3.485827F;
            this.line6.LineWeight = 1F;
            this.line6.Name = "line6";
            this.line6.Top = 0.5905512F;
            this.line6.Width = 0F;
            this.line6.X1 = 3.485827F;
            this.line6.X2 = 3.485827F;
            this.line6.Y1 = 0.5905512F;
            this.line6.Y2 = 1.301575F;
            // 
            // line7
            // 
            this.line7.Height = 0.7110237F;
            this.line7.Left = 4.318504F;
            this.line7.LineWeight = 1F;
            this.line7.Name = "line7";
            this.line7.Top = 0.5905512F;
            this.line7.Width = 0F;
            this.line7.X1 = 4.318504F;
            this.line7.X2 = 4.318504F;
            this.line7.Y1 = 0.5905512F;
            this.line7.Y2 = 1.301575F;
            // 
            // line8
            // 
            this.line8.Height = 0.7110237F;
            this.line8.Left = 5.090945F;
            this.line8.LineWeight = 1F;
            this.line8.Name = "line8";
            this.line8.Top = 0.5905512F;
            this.line8.Width = 0F;
            this.line8.X1 = 5.090945F;
            this.line8.X2 = 5.090945F;
            this.line8.Y1 = 0.5905512F;
            this.line8.Y2 = 1.301575F;
            // 
            // line9
            // 
            this.line9.Height = 0.7110237F;
            this.line9.Left = 5.74567F;
            this.line9.LineWeight = 1F;
            this.line9.Name = "line9";
            this.line9.Top = 0.5905512F;
            this.line9.Width = 0F;
            this.line9.X1 = 5.74567F;
            this.line9.X2 = 5.74567F;
            this.line9.Y1 = 0.5905512F;
            this.line9.Y2 = 1.301575F;
            // 
            // line10
            // 
            this.line10.Height = 0.7110237F;
            this.line10.Left = 6.566142F;
            this.line10.LineWeight = 1F;
            this.line10.Name = "line10";
            this.line10.Top = 0.5905512F;
            this.line10.Width = 0F;
            this.line10.X1 = 6.566142F;
            this.line10.X2 = 6.566142F;
            this.line10.Y1 = 0.5905512F;
            this.line10.Y2 = 1.301575F;
            // 
            // line11
            // 
            this.line11.Height = 0.7110237F;
            this.line11.Left = 7.404725F;
            this.line11.LineWeight = 1F;
            this.line11.Name = "line11";
            this.line11.Top = 0.5905512F;
            this.line11.Width = 0F;
            this.line11.X1 = 7.404725F;
            this.line11.X2 = 7.404725F;
            this.line11.Y1 = 0.5905512F;
            this.line11.Y2 = 1.301575F;
            // 
            // line12
            // 
            this.line12.Height = 0.7110237F;
            this.line12.Left = 8.149607F;
            this.line12.LineWeight = 1F;
            this.line12.Name = "line12";
            this.line12.Top = 0.5905512F;
            this.line12.Width = 0F;
            this.line12.X1 = 8.149607F;
            this.line12.X2 = 8.149607F;
            this.line12.Y1 = 0.5905512F;
            this.line12.Y2 = 1.301575F;
            // 
            // line13
            // 
            this.line13.Height = 0.7110237F;
            this.line13.Left = 8.272048F;
            this.line13.LineWeight = 1F;
            this.line13.Name = "line13";
            this.line13.Top = 0.5905512F;
            this.line13.Width = 0F;
            this.line13.X1 = 8.272048F;
            this.line13.X2 = 8.272048F;
            this.line13.Y1 = 0.5905512F;
            this.line13.Y2 = 1.301575F;
            // 
            // line14
            // 
            this.line14.Height = 0.7110237F;
            this.line14.Left = 9.92126F;
            this.line14.LineWeight = 1F;
            this.line14.Name = "line14";
            this.line14.Top = 0.5905512F;
            this.line14.Width = 0F;
            this.line14.X1 = 9.92126F;
            this.line14.X2 = 9.92126F;
            this.line14.Y1 = 0.5905512F;
            this.line14.Y2 = 1.301575F;
            // 
            // line15
            // 
            this.line15.Height = 0.5535433F;
            this.line15.Left = 8.385827F;
            this.line15.LineWeight = 1F;
            this.line15.Name = "line15";
            this.line15.Top = 0.7480316F;
            this.line15.Width = 0F;
            this.line15.X1 = 8.385827F;
            this.line15.X2 = 8.385827F;
            this.line15.Y1 = 0.7480316F;
            this.line15.Y2 = 1.301575F;
            // 
            // line17
            // 
            this.line17.Height = 0.4157481F;
            this.line17.Left = 8.610631F;
            this.line17.LineWeight = 1F;
            this.line17.Name = "line17";
            this.line17.Top = 0.8858268F;
            this.line17.Width = 0F;
            this.line17.X1 = 8.610631F;
            this.line17.X2 = 8.610631F;
            this.line17.Y1 = 0.8858268F;
            this.line17.Y2 = 1.301575F;
            // 
            // line18
            // 
            this.line18.Height = 0.5535433F;
            this.line18.Left = 8.740158F;
            this.line18.LineWeight = 1F;
            this.line18.Name = "line18";
            this.line18.Top = 0.7480316F;
            this.line18.Width = 0F;
            this.line18.X1 = 8.740158F;
            this.line18.X2 = 8.740158F;
            this.line18.Y1 = 0.7480316F;
            this.line18.Y2 = 1.301575F;
            // 
            // line19
            // 
            this.line19.Height = 0.5535433F;
            this.line19.Left = 8.976378F;
            this.line19.LineWeight = 1F;
            this.line19.Name = "line19";
            this.line19.Top = 0.7480316F;
            this.line19.Width = 0F;
            this.line19.X1 = 8.976378F;
            this.line19.X2 = 8.976378F;
            this.line19.Y1 = 0.7480316F;
            this.line19.Y2 = 1.301575F;
            // 
            // line20
            // 
            this.line20.Height = 0.4157481F;
            this.line20.Left = 9.094489F;
            this.line20.LineWeight = 1F;
            this.line20.Name = "line20";
            this.line20.Top = 0.8858268F;
            this.line20.Width = 0F;
            this.line20.X1 = 9.094489F;
            this.line20.X2 = 9.094489F;
            this.line20.Y1 = 0.8858268F;
            this.line20.Y2 = 1.301575F;
            // 
            // line21
            // 
            this.line21.Height = 0.5535433F;
            this.line21.Left = 9.212599F;
            this.line21.LineWeight = 1F;
            this.line21.Name = "line21";
            this.line21.Top = 0.7480316F;
            this.line21.Width = 0F;
            this.line21.X1 = 9.212599F;
            this.line21.X2 = 9.212599F;
            this.line21.Y1 = 0.7480316F;
            this.line21.Y2 = 1.301575F;
            // 
            // line22
            // 
            this.line22.Height = 0.5535434F;
            this.line22.Left = 9.448819F;
            this.line22.LineWeight = 1F;
            this.line22.Name = "line22";
            this.line22.Top = 0.7480315F;
            this.line22.Width = 0F;
            this.line22.X1 = 9.448819F;
            this.line22.X2 = 9.448819F;
            this.line22.Y1 = 0.7480315F;
            this.line22.Y2 = 1.301575F;
            // 
            // line23
            // 
            this.line23.Height = 0.5535434F;
            this.line23.Left = 9.566929F;
            this.line23.LineWeight = 1F;
            this.line23.Name = "line23";
            this.line23.Top = 0.7480315F;
            this.line23.Width = 0F;
            this.line23.X1 = 9.566929F;
            this.line23.X2 = 9.566929F;
            this.line23.Y1 = 0.7480315F;
            this.line23.Y2 = 1.301575F;
            // 
            // line24
            // 
            this.line24.Height = 0.5535434F;
            this.line24.Left = 9.68504F;
            this.line24.LineWeight = 1F;
            this.line24.Name = "line24";
            this.line24.Top = 0.7480315F;
            this.line24.Width = 0F;
            this.line24.X1 = 9.68504F;
            this.line24.X2 = 9.68504F;
            this.line24.Y1 = 0.7480315F;
            this.line24.Y2 = 1.301575F;
            // 
            // line26
            // 
            this.line26.Height = 0.7110237F;
            this.line26.Left = 9.811024F;
            this.line26.LineWeight = 1F;
            this.line26.Name = "line26";
            this.line26.Top = 0.5905512F;
            this.line26.Width = 0F;
            this.line26.X1 = 9.811024F;
            this.line26.X2 = 9.811024F;
            this.line26.Y1 = 0.5905512F;
            this.line26.Y2 = 1.301575F;
            // 
            // line27
            // 
            this.line27.Height = 0F;
            this.line27.Left = 8.267716F;
            this.line27.LineWeight = 1F;
            this.line27.Name = "line27";
            this.line27.Top = 0.7480316F;
            this.line27.Width = 1.543307F;
            this.line27.X1 = 8.267716F;
            this.line27.X2 = 9.811024F;
            this.line27.Y1 = 0.7480316F;
            this.line27.Y2 = 0.7480316F;
            // 
            // label28
            // 
            this.label28.Height = 0.1259842F;
            this.label28.HyperLink = null;
            this.label28.Left = 8.650788F;
            this.label28.Name = "label28";
            this.label28.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.label28.Tag = "";
            this.label28.Text = "本　　　人";
            this.label28.Top = 0.6062993F;
            this.label28.Width = 0.7866145F;
            // 
            // label29
            // 
            this.label29.Height = 0.3909449F;
            this.label29.HyperLink = null;
            this.label29.Left = 8.157481F;
            this.label29.Name = "label29";
            this.label29.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1; ddo-font-vertical: true";
            this.label29.Tag = "";
            this.label29.Text = "税額票";
            this.label29.Top = 0.7598426F;
            this.label29.Width = 0.1066934F;
            // 
            // label30
            // 
            this.label30.Height = 0.523622F;
            this.label30.HyperLink = null;
            this.label30.Left = 8.385827F;
            this.label30.Name = "label30";
            this.label30.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1; ddo-font-vertical: true";
            this.label30.Tag = "";
            this.label30.Text = "未成年者";
            this.label30.Top = 0.7598426F;
            this.label30.Width = 0.1066933F;
            // 
            // line28
            // 
            this.line28.Height = 0F;
            this.line28.Left = 8.503938F;
            this.line28.LineWeight = 1F;
            this.line28.Name = "line28";
            this.line28.Top = 0.8858268F;
            this.line28.Width = 0.2377787F;
            this.line28.X1 = 8.503938F;
            this.line28.X2 = 8.741716F;
            this.line28.Y1 = 0.8858268F;
            this.line28.Y2 = 0.8858268F;
            // 
            // label31
            // 
            this.label31.Height = 0.1181102F;
            this.label31.HyperLink = null;
            this.label31.Left = 8.49252F;
            this.label31.Name = "label31";
            this.label31.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.label31.Tag = "";
            this.label31.Text = "本人";
            this.label31.Top = 0.7559056F;
            this.label31.Width = 0.2421257F;
            // 
            // label32
            // 
            this.label32.Height = 0.3976378F;
            this.label32.HyperLink = null;
            this.label32.Left = 8.496458F;
            this.label32.Name = "label32";
            this.label32.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1; ddo-font-vertical: true";
            this.label32.Tag = "";
            this.label32.Text = "特　別";
            this.label32.Top = 0.8937009F;
            this.label32.Width = 0.1027556F;
            // 
            // label33
            // 
            this.label33.Height = 0.3976378F;
            this.label33.HyperLink = null;
            this.label33.Left = 8.62008F;
            this.label33.Name = "label33";
            this.label33.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1; ddo-font-vertical: true";
            this.label33.Tag = "";
            this.label33.Text = "その他";
            this.label33.Top = 0.8937009F;
            this.label33.Width = 0.1027556F;
            // 
            // line29
            // 
            this.line29.Height = 0F;
            this.line29.Left = 8.976379F;
            this.line29.LineWeight = 1F;
            this.line29.Name = "line29";
            this.line29.Top = 0.8858269F;
            this.line29.Width = 0.2362204F;
            this.line29.X1 = 8.976379F;
            this.line29.X2 = 9.2126F;
            this.line29.Y1 = 0.8858269F;
            this.line29.Y2 = 0.8858269F;
            // 
            // label34
            // 
            this.label34.Height = 0.1141732F;
            this.label34.HyperLink = null;
            this.label34.Left = 8.98819F;
            this.label34.Name = "label34";
            this.label34.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.label34.Tag = "";
            this.label34.Text = "寡婦";
            this.label34.Top = 0.7559056F;
            this.label34.Width = 0.2224409F;
            // 
            // label37
            // 
            this.label37.Height = 0.3976378F;
            this.label37.HyperLink = null;
            this.label37.Left = 8.98819F;
            this.label37.Name = "label37";
            this.label37.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1; ddo-font-vertical: true";
            this.label37.Tag = "";
            this.label37.Text = "一　般";
            this.label37.Top = 0.8937008F;
            this.label37.Width = 0.09488202F;
            // 
            // label38
            // 
            this.label38.Height = 0.3976378F;
            this.label38.HyperLink = null;
            this.label38.Left = 9.102363F;
            this.label38.Name = "label38";
            this.label38.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1; ddo-font-vertical: true";
            this.label38.Tag = "";
            this.label38.Text = "特　別";
            this.label38.Top = 0.8937008F;
            this.label38.Width = 0.11063F;
            // 
            // label39
            // 
            this.label39.Height = 0.523622F;
            this.label39.HyperLink = null;
            this.label39.Left = 9.220473F;
            this.label39.Name = "label39";
            this.label39.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1; ddo-font-vertical: true";
            this.label39.Tag = "";
            this.label39.Text = "寡　夫";
            this.label39.Top = 0.7598426F;
            this.label39.Width = 0.1027556F;
            // 
            // label40
            // 
            this.label40.Height = 0.523622F;
            this.label40.HyperLink = null;
            this.label40.Left = 9.334646F;
            this.label40.Name = "label40";
            this.label40.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1; ddo-font-vertical: true";
            this.label40.Tag = "";
            this.label40.Text = "勤労学生";
            this.label40.Top = 0.7598426F;
            this.label40.Width = 0.1027556F;
            // 
            // label41
            // 
            this.label41.Height = 0.523622F;
            this.label41.HyperLink = null;
            this.label41.Left = 9.456694F;
            this.label41.Name = "label41";
            this.label41.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1; ddo-font-vertical: true";
            this.label41.Tag = "";
            this.label41.Text = "死亡退職";
            this.label41.Top = 0.7598426F;
            this.label41.Width = 0.09881879F;
            // 
            // label42
            // 
            this.label42.Height = 0.523622F;
            this.label42.HyperLink = null;
            this.label42.Left = 9.570867F;
            this.label42.Name = "label42";
            this.label42.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1; ddo-font-vertical: true";
            this.label42.Tag = "";
            this.label42.Text = "災害者";
            this.label42.Top = 0.7637796F;
            this.label42.Width = 0.1066933F;
            // 
            // label43
            // 
            this.label43.Height = 0.5236221F;
            this.label43.HyperLink = null;
            this.label43.Left = 9.696457F;
            this.label43.Name = "label43";
            this.label43.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1; ddo-font-vertical: true";
            this.label43.Tag = "";
            this.label43.Text = "外国人";
            this.label43.Top = 0.7637796F;
            this.label43.Width = 0.11063F;
            // 
            // label44
            // 
            this.label44.Height = 0.3909449F;
            this.label44.HyperLink = null;
            this.label44.Left = 9.811025F;
            this.label44.Name = "label44";
            this.label44.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1; ddo-font-vertical: true";
            this.label44.Tag = "";
            this.label44.Text = "配偶者";
            this.label44.Top = 0.7598426F;
            this.label44.Width = 0.1027557F;
            // 
            // line30
            // 
            this.line30.Height = 0.5535433F;
            this.line30.Left = 10.27559F;
            this.line30.LineWeight = 1F;
            this.line30.Name = "line30";
            this.line30.Top = 0.7480316F;
            this.line30.Width = 0F;
            this.line30.X1 = 10.27559F;
            this.line30.X2 = 10.27559F;
            this.line30.Y1 = 0.7480316F;
            this.line30.Y2 = 1.301575F;
            // 
            // line32
            // 
            this.line32.Height = 0.4157481F;
            this.line32.Left = 10.3937F;
            this.line32.LineWeight = 1F;
            this.line32.Name = "line32";
            this.line32.Top = 0.8858268F;
            this.line32.Width = 0F;
            this.line32.X1 = 10.3937F;
            this.line32.X2 = 10.3937F;
            this.line32.Y1 = 0.8858268F;
            this.line32.Y2 = 1.301575F;
            // 
            // line33
            // 
            this.line33.Height = 0.5535433F;
            this.line33.Left = 10.51181F;
            this.line33.LineWeight = 1F;
            this.line33.Name = "line33";
            this.line33.Top = 0.7480316F;
            this.line33.Width = 0F;
            this.line33.X1 = 10.51181F;
            this.line33.X2 = 10.51181F;
            this.line33.Y1 = 0.7480316F;
            this.line33.Y2 = 1.301575F;
            // 
            // line34
            // 
            this.line34.Height = 0.4157481F;
            this.line34.Left = 10.62992F;
            this.line34.LineWeight = 1F;
            this.line34.Name = "line34";
            this.line34.Top = 0.8858268F;
            this.line34.Width = 0F;
            this.line34.X1 = 10.62992F;
            this.line34.X2 = 10.62992F;
            this.line34.Y1 = 0.8858268F;
            this.line34.Y2 = 1.301575F;
            // 
            // line31
            // 
            this.line31.Height = 0.4157481F;
            this.line31.Left = 10.74803F;
            this.line31.LineWeight = 1F;
            this.line31.Name = "line31";
            this.line31.Top = 0.8858268F;
            this.line31.Width = 0F;
            this.line31.X1 = 10.74803F;
            this.line31.X2 = 10.74803F;
            this.line31.Y1 = 0.8858268F;
            this.line31.Y2 = 1.301575F;
            // 
            // line35
            // 
            this.line35.Height = 0.4157481F;
            this.line35.Left = 10.86614F;
            this.line35.LineWeight = 1F;
            this.line35.Name = "line35";
            this.line35.Top = 0.8858268F;
            this.line35.Width = 0F;
            this.line35.X1 = 10.86614F;
            this.line35.X2 = 10.86614F;
            this.line35.Y1 = 0.8858268F;
            this.line35.Y2 = 1.301575F;
            // 
            // line36
            // 
            this.line36.Height = 0.7110237F;
            this.line36.Left = 11.10236F;
            this.line36.LineWeight = 1F;
            this.line36.Name = "line36";
            this.line36.Top = 0.5905512F;
            this.line36.Width = 0F;
            this.line36.X1 = 11.10236F;
            this.line36.X2 = 11.10236F;
            this.line36.Y1 = 0.5905512F;
            this.line36.Y2 = 1.301575F;
            // 
            // line37
            // 
            this.line37.Height = 0.4157481F;
            this.line37.Left = 11.22047F;
            this.line37.LineWeight = 1F;
            this.line37.Name = "line37";
            this.line37.Top = 0.8858268F;
            this.line37.Width = 0F;
            this.line37.X1 = 11.22047F;
            this.line37.X2 = 11.22047F;
            this.line37.Y1 = 0.8858268F;
            this.line37.Y2 = 1.301575F;
            // 
            // line38
            // 
            this.line38.Height = 0F;
            this.line38.Left = 9.92126F;
            this.line38.LineWeight = 1F;
            this.line38.Name = "line38";
            this.line38.Top = 0.7480316F;
            this.line38.Width = 1.1811F;
            this.line38.X1 = 9.92126F;
            this.line38.X2 = 11.10236F;
            this.line38.Y1 = 0.7480316F;
            this.line38.Y2 = 0.7480316F;
            // 
            // label45
            // 
            this.label45.Height = 0.1259842F;
            this.label45.HyperLink = null;
            this.label45.Left = 10.03189F;
            this.label45.Name = "label45";
            this.label45.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.label45.Tag = "";
            this.label45.Text = "扶　養　者";
            this.label45.Top = 0.6062993F;
            this.label45.Width = 0.9523621F;
            // 
            // label47
            // 
            this.label47.Height = 0.523622F;
            this.label47.HyperLink = null;
            this.label47.Left = 10.15788F;
            this.label47.Name = "label47";
            this.label47.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1; ddo-font-vertical: true";
            this.label47.Tag = "";
            this.label47.Text = "特　定";
            this.label47.Top = 0.7637796F;
            this.label47.Width = 0.1066923F;
            // 
            // label48
            // 
            this.label48.Height = 0.1181103F;
            this.label48.HyperLink = null;
            this.label48.Left = 10.27953F;
            this.label48.Name = "label48";
            this.label48.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.label48.Tag = "";
            this.label48.Text = "老人";
            this.label48.Top = 0.7559056F;
            this.label48.Width = 0.222441F;
            // 
            // label49
            // 
            this.label49.Height = 0.3976378F;
            this.label49.HyperLink = null;
            this.label49.Left = 10.27953F;
            this.label49.Name = "label49";
            this.label49.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1; ddo-font-vertical: true";
            this.label49.Tag = "";
            this.label49.Text = "老親等";
            this.label49.Top = 0.8937008F;
            this.label49.Width = 0.1027556F;
            // 
            // label50
            // 
            this.label50.Height = 0.3976378F;
            this.label50.HyperLink = null;
            this.label50.Left = 10.40315F;
            this.label50.Name = "label50";
            this.label50.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1; ddo-font-vertical: true";
            this.label50.Tag = "";
            this.label50.Text = "以　外";
            this.label50.Top = 0.8937008F;
            this.label50.Width = 0.09881879F;
            // 
            // line39
            // 
            this.line39.Height = 0F;
            this.line39.Left = 10.27559F;
            this.line39.LineWeight = 1F;
            this.line39.Name = "line39";
            this.line39.Top = 0.8858268F;
            this.line39.Width = 1.06299F;
            this.line39.X1 = 10.27559F;
            this.line39.X2 = 11.33858F;
            this.line39.Y1 = 0.8858268F;
            this.line39.Y2 = 0.8858268F;
            // 
            // label51
            // 
            this.label51.Height = 0.3464566F;
            this.label51.HyperLink = null;
            this.label51.Left = 10.51969F;
            this.label51.Name = "label51";
            this.label51.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1; ddo-font-vertical: true";
            this.label51.Tag = "";
            this.label51.Text = "一　般";
            this.label51.Top = 0.8937008F;
            this.label51.Width = 0.09488201F;
            // 
            // line40
            // 
            this.line40.Height = 0F;
            this.line40.Left = 10.71142F;
            this.line40.LineWeight = 1F;
            this.line40.Name = "line40";
            this.line40.Top = 0.8858268F;
            this.line40.Width = 0.2395802F;
            this.line40.X1 = 10.71142F;
            this.line40.X2 = 10.951F;
            this.line40.Y1 = 0.8858268F;
            this.line40.Y2 = 0.8858268F;
            // 
            // label53
            // 
            this.label53.Height = 0.1181103F;
            this.label53.HyperLink = null;
            this.label53.Left = 10.51969F;
            this.label53.Name = "label53";
            this.label53.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.label53.Tag = "";
            this.label53.Text = "同居特別障害";
            this.label53.Top = 0.7559056F;
            this.label53.Width = 0.5433073F;
            // 
            // label56
            // 
            this.label56.Height = 0.4716535F;
            this.label56.HyperLink = null;
            this.label56.Left = 10.04331F;
            this.label56.Name = "label56";
            this.label56.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1; ddo-font-vertical: none";
            this.label56.Tag = "";
            this.label56.Text = "\r\n歳未満";
            this.label56.Top = 0.7566929F;
            this.label56.Width = 0.1145668F;
            // 
            // line41
            // 
            this.line41.Height = 0F;
            this.line41.Left = 1.649606F;
            this.line41.LineWeight = 1F;
            this.line41.Name = "line41";
            this.line41.Top = 0.8433071F;
            this.line41.Width = 6.500001F;
            this.line41.X1 = 1.649606F;
            this.line41.X2 = 8.149607F;
            this.line41.Y1 = 0.8433071F;
            this.line41.Y2 = 0.8433071F;
            // 
            // line42
            // 
            this.line42.Height = 0F;
            this.line42.Left = 1.649606F;
            this.line42.LineWeight = 1F;
            this.line42.Name = "line42";
            this.line42.Top = 1.072441F;
            this.line42.Width = 6.500001F;
            this.line42.X1 = 1.649606F;
            this.line42.X2 = 8.149607F;
            this.line42.Y1 = 1.072441F;
            this.line42.Y2 = 1.072441F;
            // 
            // line43
            // 
            this.line43.Height = 0.5535433F;
            this.line43.Left = 8.503938F;
            this.line43.LineWeight = 1F;
            this.line43.Name = "line43";
            this.line43.Top = 0.7480316F;
            this.line43.Width = 0F;
            this.line43.X1 = 8.503938F;
            this.line43.X2 = 8.503938F;
            this.line43.Y1 = 0.7480316F;
            this.line43.Y2 = 1.301575F;
            // 
            // line44
            // 
            this.line44.Height = 0.5535433F;
            this.line44.Left = 8.858268F;
            this.line44.LineWeight = 1F;
            this.line44.Name = "line44";
            this.line44.Top = 0.7480316F;
            this.line44.Width = 0F;
            this.line44.X1 = 8.858268F;
            this.line44.X2 = 8.858268F;
            this.line44.Y1 = 0.7480316F;
            this.line44.Y2 = 1.301575F;
            // 
            // line45
            // 
            this.line45.Height = 0.5535433F;
            this.line45.Left = 9.330709F;
            this.line45.LineWeight = 1F;
            this.line45.Name = "line45";
            this.line45.Top = 0.7480316F;
            this.line45.Width = 0F;
            this.line45.X1 = 9.330709F;
            this.line45.X2 = 9.330709F;
            this.line45.Y1 = 0.7480316F;
            this.line45.Y2 = 1.301575F;
            // 
            // label57
            // 
            this.label57.Height = 0.523622F;
            this.label57.HyperLink = null;
            this.label57.Left = 9.929134F;
            this.label57.Name = "label57";
            this.label57.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1; ddo-font-vertical: true";
            this.label57.Tag = "";
            this.label57.Text = "一　般";
            this.label57.Top = 0.7637796F;
            this.label57.Width = 0.1066933F;
            // 
            // line46
            // 
            this.line46.Height = 0.5535433F;
            this.line46.Left = 10.03937F;
            this.line46.LineWeight = 1F;
            this.line46.Name = "line46";
            this.line46.Top = 0.7480316F;
            this.line46.Width = 0F;
            this.line46.X1 = 10.03937F;
            this.line46.X2 = 10.03937F;
            this.line46.Y1 = 0.7480316F;
            this.line46.Y2 = 1.301575F;
            // 
            // line47
            // 
            this.line47.Height = 0.5535433F;
            this.line47.Left = 10.15748F;
            this.line47.LineWeight = 1F;
            this.line47.Name = "line47";
            this.line47.Top = 0.7480316F;
            this.line47.Width = 0F;
            this.line47.X1 = 10.15748F;
            this.line47.X2 = 10.15748F;
            this.line47.Y1 = 0.7480316F;
            this.line47.Y2 = 1.301575F;
            // 
            // label58
            // 
            this.label58.Height = 0.09448819F;
            this.label58.HyperLink = null;
            this.label58.Left = 10.03189F;
            this.label58.Name = "label58";
            this.label58.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 6.5pt; font-weight: normal; text-ali" +
    "gn: center; ddo-char-set: 1; ddo-font-vertical: none";
            this.label58.Tag = "";
            this.label58.Text = "16";
            this.label58.Top = 0.7637796F;
            this.label58.Width = 0.1196852F;
            // 
            // line48
            // 
            this.line48.Height = 0.4157481F;
            this.line48.Left = 10.98425F;
            this.line48.LineWeight = 1F;
            this.line48.Name = "line48";
            this.line48.Top = 0.8858268F;
            this.line48.Width = 0F;
            this.line48.X1 = 10.98425F;
            this.line48.X2 = 10.98425F;
            this.line48.Y1 = 0.8858268F;
            this.line48.Y2 = 1.301575F;
            // 
            // line49
            // 
            this.line49.Height = 0.7110237F;
            this.line49.Left = 11.33858F;
            this.line49.LineWeight = 2F;
            this.line49.Name = "line49";
            this.line49.Top = 0.5905512F;
            this.line49.Width = 0F;
            this.line49.X1 = 11.33858F;
            this.line49.X2 = 11.33858F;
            this.line49.Y1 = 0.5905512F;
            this.line49.Y2 = 1.301575F;
            // 
            // label46
            // 
            this.label46.Height = 0.1259842F;
            this.label46.HyperLink = null;
            this.label46.Left = 11.08661F;
            this.label46.Name = "label46";
            this.label46.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.label46.Tag = "";
            this.label46.Text = "障害";
            this.label46.Top = 0.6767717F;
            this.label46.Width = 0.2535439F;
            // 
            // line50
            // 
            this.line50.Height = 0.7110237F;
            this.line50.Left = 0F;
            this.line50.LineWeight = 2F;
            this.line50.Name = "line50";
            this.line50.Top = 0.5905512F;
            this.line50.Width = 0F;
            this.line50.X1 = 0F;
            this.line50.X2 = 0F;
            this.line50.Y1 = 0.5905512F;
            this.line50.Y2 = 1.301575F;
            // 
            // label35
            // 
            this.label35.Height = 0.3464566F;
            this.label35.HyperLink = null;
            this.label35.Left = 11.10984F;
            this.label35.Name = "label35";
            this.label35.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1; ddo-font-vertical: true";
            this.label35.Tag = "";
            this.label35.Text = "一　般";
            this.label35.Top = 0.8937008F;
            this.label35.Width = 0.1027555F;
            // 
            // label36
            // 
            this.label36.Height = 0.3464566F;
            this.label36.HyperLink = null;
            this.label36.Left = 11.22795F;
            this.label36.Name = "label36";
            this.label36.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1; ddo-font-vertical: true";
            this.label36.Tag = "";
            this.label36.Text = "特　別";
            this.label36.Top = 0.8937008F;
            this.label36.Width = 0.1027555F;
            // 
            // line25
            // 
            this.line25.Height = 0.0001804829F;
            this.line25.Left = 5.11811F;
            this.line25.LineWeight = 1F;
            this.line25.Name = "line25";
            this.line25.Top = 0.4895833F;
            this.line25.Width = 1.771654F;
            this.line25.X1 = 5.11811F;
            this.line25.X2 = 6.889764F;
            this.line25.Y1 = 0.4895833F;
            this.line25.Y2 = 0.4897638F;
            // 
            // txtToday
            // 
            this.txtToday.DataField = "ITEM003";
            this.txtToday.Height = 0.157874F;
            this.txtToday.Left = 8.283859F;
            this.txtToday.MultiLine = false;
            this.txtToday.Name = "txtToday";
            this.txtToday.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: normal; text-a" +
    "lign: right; ddo-char-set: 128";
            this.txtToday.Tag = "";
            this.txtToday.Text = "today";
            this.txtToday.Top = 0.1555118F;
            this.txtToday.Width = 2.122047F;
            // 
            // label52
            // 
            this.label52.Height = 0.2291667F;
            this.label52.HyperLink = null;
            this.label52.Left = 10.91457F;
            this.label52.Name = "label52";
            this.label52.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: normal; text-a" +
    "lign: left; ddo-char-set: 1";
            this.label52.Tag = "";
            this.label52.Text = "頁";
            this.label52.Top = 0.1555118F;
            this.label52.Width = 0.1875992F;
            // 
            // textBox55
            // 
            this.textBox55.Height = 0.157874F;
            this.textBox55.Left = 10.40591F;
            this.textBox55.MultiLine = false;
            this.textBox55.Name = "textBox55";
            this.textBox55.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; font-weight: normal; text-a" +
    "lign: right; ddo-char-set: 128";
            this.textBox55.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.textBox55.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.textBox55.Tag = "";
            this.textBox55.Text = null;
            this.textBox55.Top = 0.1555118F;
            this.textBox55.Width = 0.5086613F;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.line51,
            this.line52,
            this.line53,
            this.line54,
            this.line55,
            this.line56,
            this.line57,
            this.line58,
            this.line59,
            this.line60,
            this.line61,
            this.line62,
            this.line63,
            this.line64,
            this.line65,
            this.line66,
            this.line67,
            this.line68,
            this.line69,
            this.line70,
            this.line71,
            this.line72,
            this.line73,
            this.line74,
            this.line75,
            this.line76,
            this.line77,
            this.line78,
            this.line79,
            this.line80,
            this.line81,
            this.line82,
            this.line83,
            this.line84,
            this.line85,
            this.line86,
            this.line87,
            this.line88,
            this.line89,
            this.line90,
            this.line91,
            this.line92,
            this.line16,
            this.textBox1,
            this.textBox3,
            this.textBox4,
            this.textBox5,
            this.textBox6,
            this.textBox7,
            this.textBox8,
            this.textBox9,
            this.textBox10,
            this.textBox11,
            this.textBox12,
            this.textBox13,
            this.textBox14,
            this.textBox15,
            this.textBox16,
            this.textBox17,
            this.textBox18,
            this.textBox19,
            this.textBox20,
            this.textBox21,
            this.textBox22,
            this.textBox23,
            this.textBox24,
            this.textBox25,
            this.textBox26,
            this.textBox27,
            this.textBox28,
            this.textBox29,
            this.textBox30,
            this.textBox32,
            this.textBox33,
            this.textBox34,
            this.textBox35,
            this.textBox36,
            this.textBox37,
            this.textBox38,
            this.textBox39,
            this.textBox40,
            this.textBox41,
            this.txtkubun1,
            this.txtkubun2,
            this.txtkubun3,
            this.txtkubun4,
            this.txtkubun5,
            this.txtkubun6,
            this.txtkubun7,
            this.txtkubun8,
            this.textBox51,
            this.lbl1,
            this.lbl2,
            this.lbl3,
            this.textBox31,
            this.textBox50,
            this.textBox52,
            this.textBox53,
            this.textBox54,
            this.lbl4,
            this.label54,
            this.label55,
            this.label59,
            this.label60});
            this.detail.Height = 0.794346F;
            this.detail.Name = "detail";
            this.detail.Format += new System.EventHandler(this.detail_Format);
            // 
            // line51
            // 
            this.line51.Height = 0F;
            this.line51.Left = 0F;
            this.line51.LineWeight = 1F;
            this.line51.Name = "line51";
            this.line51.Top = 0.7874016F;
            this.line51.Width = 11.33858F;
            this.line51.X1 = 0F;
            this.line51.X2 = 11.33858F;
            this.line51.Y1 = 0.7874016F;
            this.line51.Y2 = 0.7874016F;
            // 
            // line52
            // 
            this.line52.Height = 0.7874016F;
            this.line52.Left = 0F;
            this.line52.LineWeight = 2F;
            this.line52.Name = "line52";
            this.line52.Top = 0F;
            this.line52.Width = 0F;
            this.line52.X1 = 0F;
            this.line52.X2 = 0F;
            this.line52.Y1 = 0F;
            this.line52.Y2 = 0.7874016F;
            // 
            // line53
            // 
            this.line53.Height = 0.7874016F;
            this.line53.Left = 0.4984252F;
            this.line53.LineWeight = 1F;
            this.line53.Name = "line53";
            this.line53.Top = 0F;
            this.line53.Width = 0F;
            this.line53.X1 = 0.4984252F;
            this.line53.X2 = 0.4984252F;
            this.line53.Y1 = 0F;
            this.line53.Y2 = 0.7874016F;
            // 
            // line54
            // 
            this.line54.Height = 0.7874014F;
            this.line54.Left = 1.646063F;
            this.line54.LineWeight = 1F;
            this.line54.Name = "line54";
            this.line54.Top = 0F;
            this.line54.Width = 0F;
            this.line54.X1 = 1.646063F;
            this.line54.X2 = 1.646063F;
            this.line54.Y1 = 0F;
            this.line54.Y2 = 0.7874014F;
            // 
            // line55
            // 
            this.line55.Height = 0.7874014F;
            this.line55.Left = 2.568504F;
            this.line55.LineWeight = 1F;
            this.line55.Name = "line55";
            this.line55.Top = 0F;
            this.line55.Width = 0F;
            this.line55.X1 = 2.568504F;
            this.line55.X2 = 2.568504F;
            this.line55.Y1 = 0F;
            this.line55.Y2 = 0.7874014F;
            // 
            // line56
            // 
            this.line56.Height = 0.7874014F;
            this.line56.Left = 3.485827F;
            this.line56.LineWeight = 1F;
            this.line56.Name = "line56";
            this.line56.Top = 0F;
            this.line56.Width = 0F;
            this.line56.X1 = 3.485827F;
            this.line56.X2 = 3.485827F;
            this.line56.Y1 = 0F;
            this.line56.Y2 = 0.7874014F;
            // 
            // line57
            // 
            this.line57.Height = 0.7874014F;
            this.line57.Left = 4.318504F;
            this.line57.LineWeight = 1F;
            this.line57.Name = "line57";
            this.line57.Top = 0F;
            this.line57.Width = 0F;
            this.line57.X1 = 4.318504F;
            this.line57.X2 = 4.318504F;
            this.line57.Y1 = 0F;
            this.line57.Y2 = 0.7874014F;
            // 
            // line58
            // 
            this.line58.Height = 0.7874014F;
            this.line58.Left = 5.090945F;
            this.line58.LineWeight = 1F;
            this.line58.Name = "line58";
            this.line58.Top = 0F;
            this.line58.Width = 0F;
            this.line58.X1 = 5.090945F;
            this.line58.X2 = 5.090945F;
            this.line58.Y1 = 0F;
            this.line58.Y2 = 0.7874014F;
            // 
            // line59
            // 
            this.line59.Height = 0.7874014F;
            this.line59.Left = 5.74567F;
            this.line59.LineWeight = 1F;
            this.line59.Name = "line59";
            this.line59.Top = 0F;
            this.line59.Width = 0F;
            this.line59.X1 = 5.74567F;
            this.line59.X2 = 5.74567F;
            this.line59.Y1 = 0F;
            this.line59.Y2 = 0.7874014F;
            // 
            // line60
            // 
            this.line60.Height = 0.7874014F;
            this.line60.Left = 6.566142F;
            this.line60.LineWeight = 1F;
            this.line60.Name = "line60";
            this.line60.Top = 0F;
            this.line60.Width = 0F;
            this.line60.X1 = 6.566142F;
            this.line60.X2 = 6.566142F;
            this.line60.Y1 = 0F;
            this.line60.Y2 = 0.7874014F;
            // 
            // line61
            // 
            this.line61.Height = 0.7874014F;
            this.line61.Left = 7.404725F;
            this.line61.LineWeight = 1F;
            this.line61.Name = "line61";
            this.line61.Top = 0F;
            this.line61.Width = 0F;
            this.line61.X1 = 7.404725F;
            this.line61.X2 = 7.404725F;
            this.line61.Y1 = 0F;
            this.line61.Y2 = 0.7874014F;
            // 
            // line62
            // 
            this.line62.Height = 0.7874014F;
            this.line62.Left = 8.149607F;
            this.line62.LineWeight = 1F;
            this.line62.Name = "line62";
            this.line62.Top = 0F;
            this.line62.Width = 0F;
            this.line62.X1 = 8.149607F;
            this.line62.X2 = 8.149607F;
            this.line62.Y1 = 0F;
            this.line62.Y2 = 0.7874014F;
            // 
            // line63
            // 
            this.line63.Height = 0.7874014F;
            this.line63.Left = 8.272048F;
            this.line63.LineWeight = 1F;
            this.line63.Name = "line63";
            this.line63.Top = 0F;
            this.line63.Width = 0F;
            this.line63.X1 = 8.272048F;
            this.line63.X2 = 8.272048F;
            this.line63.Y1 = 0F;
            this.line63.Y2 = 0.7874014F;
            // 
            // line64
            // 
            this.line64.Height = 0.1968504F;
            this.line64.Left = 8.385827F;
            this.line64.LineWeight = 1F;
            this.line64.Name = "line64";
            this.line64.Top = 0F;
            this.line64.Width = 0F;
            this.line64.X1 = 8.385827F;
            this.line64.X2 = 8.385827F;
            this.line64.Y1 = 0F;
            this.line64.Y2 = 0.1968504F;
            // 
            // line65
            // 
            this.line65.Height = 0.1968504F;
            this.line65.Left = 8.503938F;
            this.line65.LineWeight = 1F;
            this.line65.Name = "line65";
            this.line65.Top = 0F;
            this.line65.Width = 0F;
            this.line65.X1 = 8.503938F;
            this.line65.X2 = 8.503938F;
            this.line65.Y1 = 0F;
            this.line65.Y2 = 0.1968504F;
            // 
            // line66
            // 
            this.line66.Height = 0.1968504F;
            this.line66.Left = 8.622047F;
            this.line66.LineWeight = 1F;
            this.line66.Name = "line66";
            this.line66.Top = 0F;
            this.line66.Width = 0F;
            this.line66.X1 = 8.622047F;
            this.line66.X2 = 8.622047F;
            this.line66.Y1 = 0F;
            this.line66.Y2 = 0.1968504F;
            // 
            // line67
            // 
            this.line67.Height = 0.1968504F;
            this.line67.Left = 8.740158F;
            this.line67.LineWeight = 1F;
            this.line67.Name = "line67";
            this.line67.Top = 0F;
            this.line67.Width = 0F;
            this.line67.X1 = 8.740158F;
            this.line67.X2 = 8.740158F;
            this.line67.Y1 = 0F;
            this.line67.Y2 = 0.1968504F;
            // 
            // line68
            // 
            this.line68.Height = 0.1968504F;
            this.line68.Left = 8.858269F;
            this.line68.LineWeight = 1F;
            this.line68.Name = "line68";
            this.line68.Top = 0F;
            this.line68.Width = 0F;
            this.line68.X1 = 8.858269F;
            this.line68.X2 = 8.858269F;
            this.line68.Y1 = 0F;
            this.line68.Y2 = 0.1968504F;
            // 
            // line69
            // 
            this.line69.Height = 0.1968504F;
            this.line69.Left = 8.976378F;
            this.line69.LineWeight = 1F;
            this.line69.Name = "line69";
            this.line69.Top = 0F;
            this.line69.Width = 0F;
            this.line69.X1 = 8.976378F;
            this.line69.X2 = 8.976378F;
            this.line69.Y1 = 0F;
            this.line69.Y2 = 0.1968504F;
            // 
            // line70
            // 
            this.line70.Height = 0.1968504F;
            this.line70.Left = 9.094488F;
            this.line70.LineWeight = 1F;
            this.line70.Name = "line70";
            this.line70.Top = 0F;
            this.line70.Width = 0F;
            this.line70.X1 = 9.094488F;
            this.line70.X2 = 9.094488F;
            this.line70.Y1 = 0F;
            this.line70.Y2 = 0.1968504F;
            // 
            // line71
            // 
            this.line71.Height = 0.1968504F;
            this.line71.Left = 9.212599F;
            this.line71.LineWeight = 1F;
            this.line71.Name = "line71";
            this.line71.Top = 0F;
            this.line71.Width = 0F;
            this.line71.X1 = 9.212599F;
            this.line71.X2 = 9.212599F;
            this.line71.Y1 = 0F;
            this.line71.Y2 = 0.1968504F;
            // 
            // line72
            // 
            this.line72.Height = 0.1968504F;
            this.line72.Left = 9.330709F;
            this.line72.LineWeight = 1F;
            this.line72.Name = "line72";
            this.line72.Top = 0F;
            this.line72.Width = 0F;
            this.line72.X1 = 9.330709F;
            this.line72.X2 = 9.330709F;
            this.line72.Y1 = 0F;
            this.line72.Y2 = 0.1968504F;
            // 
            // line73
            // 
            this.line73.Height = 0.1968504F;
            this.line73.Left = 9.448819F;
            this.line73.LineWeight = 1F;
            this.line73.Name = "line73";
            this.line73.Top = 0F;
            this.line73.Width = 0F;
            this.line73.X1 = 9.448819F;
            this.line73.X2 = 9.448819F;
            this.line73.Y1 = 0F;
            this.line73.Y2 = 0.1968504F;
            // 
            // line74
            // 
            this.line74.Height = 0.1968504F;
            this.line74.Left = 9.56693F;
            this.line74.LineWeight = 1F;
            this.line74.Name = "line74";
            this.line74.Top = 0F;
            this.line74.Width = 0F;
            this.line74.X1 = 9.56693F;
            this.line74.X2 = 9.56693F;
            this.line74.Y1 = 0F;
            this.line74.Y2 = 0.1968504F;
            // 
            // line75
            // 
            this.line75.Height = 0.1968504F;
            this.line75.Left = 9.68504F;
            this.line75.LineWeight = 1F;
            this.line75.Name = "line75";
            this.line75.Top = 0F;
            this.line75.Width = 0F;
            this.line75.X1 = 9.68504F;
            this.line75.X2 = 9.68504F;
            this.line75.Y1 = 0F;
            this.line75.Y2 = 0.1968504F;
            // 
            // line76
            // 
            this.line76.Height = 0.7874014F;
            this.line76.Left = 9.80315F;
            this.line76.LineWeight = 1F;
            this.line76.Name = "line76";
            this.line76.Top = 0F;
            this.line76.Width = 0F;
            this.line76.X1 = 9.80315F;
            this.line76.X2 = 9.80315F;
            this.line76.Y1 = 0F;
            this.line76.Y2 = 0.7874014F;
            // 
            // line77
            // 
            this.line77.Height = 0.7874014F;
            this.line77.Left = 9.921261F;
            this.line77.LineWeight = 1F;
            this.line77.Name = "line77";
            this.line77.Top = 0F;
            this.line77.Width = 0F;
            this.line77.X1 = 9.921261F;
            this.line77.X2 = 9.921261F;
            this.line77.Y1 = 0F;
            this.line77.Y2 = 0.7874014F;
            // 
            // line78
            // 
            this.line78.Height = 0.1968504F;
            this.line78.Left = 10.03937F;
            this.line78.LineWeight = 1F;
            this.line78.Name = "line78";
            this.line78.Top = 0F;
            this.line78.Width = 0F;
            this.line78.X1 = 10.03937F;
            this.line78.X2 = 10.03937F;
            this.line78.Y1 = 0F;
            this.line78.Y2 = 0.1968504F;
            // 
            // line79
            // 
            this.line79.Height = 0.1968504F;
            this.line79.Left = 10.15748F;
            this.line79.LineWeight = 1F;
            this.line79.Name = "line79";
            this.line79.Top = 0F;
            this.line79.Width = 0F;
            this.line79.X1 = 10.15748F;
            this.line79.X2 = 10.15748F;
            this.line79.Y1 = 0F;
            this.line79.Y2 = 0.1968504F;
            // 
            // line80
            // 
            this.line80.Height = 0.1968504F;
            this.line80.Left = 10.27559F;
            this.line80.LineWeight = 1F;
            this.line80.Name = "line80";
            this.line80.Top = 0F;
            this.line80.Width = 0F;
            this.line80.X1 = 10.27559F;
            this.line80.X2 = 10.27559F;
            this.line80.Y1 = 0F;
            this.line80.Y2 = 0.1968504F;
            // 
            // line81
            // 
            this.line81.Height = 0.1968504F;
            this.line81.Left = 10.3937F;
            this.line81.LineWeight = 1F;
            this.line81.Name = "line81";
            this.line81.Top = 0F;
            this.line81.Width = 0F;
            this.line81.X1 = 10.3937F;
            this.line81.X2 = 10.3937F;
            this.line81.Y1 = 0F;
            this.line81.Y2 = 0.1968504F;
            // 
            // line82
            // 
            this.line82.Height = 0.1968504F;
            this.line82.Left = 10.51181F;
            this.line82.LineWeight = 1F;
            this.line82.Name = "line82";
            this.line82.Top = 0F;
            this.line82.Width = 0F;
            this.line82.X1 = 10.51181F;
            this.line82.X2 = 10.51181F;
            this.line82.Y1 = 0F;
            this.line82.Y2 = 0.1968504F;
            // 
            // line83
            // 
            this.line83.Height = 0.1968504F;
            this.line83.Left = 10.62992F;
            this.line83.LineWeight = 1F;
            this.line83.Name = "line83";
            this.line83.Top = 0F;
            this.line83.Width = 0F;
            this.line83.X1 = 10.62992F;
            this.line83.X2 = 10.62992F;
            this.line83.Y1 = 0F;
            this.line83.Y2 = 0.1968504F;
            // 
            // line84
            // 
            this.line84.Height = 0.1968504F;
            this.line84.Left = 10.74803F;
            this.line84.LineWeight = 1F;
            this.line84.Name = "line84";
            this.line84.Top = 0F;
            this.line84.Width = 0F;
            this.line84.X1 = 10.74803F;
            this.line84.X2 = 10.74803F;
            this.line84.Y1 = 0F;
            this.line84.Y2 = 0.1968504F;
            // 
            // line85
            // 
            this.line85.Height = 0.1968504F;
            this.line85.Left = 10.86614F;
            this.line85.LineWeight = 1F;
            this.line85.Name = "line85";
            this.line85.Top = 0F;
            this.line85.Width = 0F;
            this.line85.X1 = 10.86614F;
            this.line85.X2 = 10.86614F;
            this.line85.Y1 = 0F;
            this.line85.Y2 = 0.1968504F;
            // 
            // line86
            // 
            this.line86.Height = 0.1968504F;
            this.line86.Left = 10.98425F;
            this.line86.LineWeight = 1F;
            this.line86.Name = "line86";
            this.line86.Top = 0F;
            this.line86.Width = 0F;
            this.line86.X1 = 10.98425F;
            this.line86.X2 = 10.98425F;
            this.line86.Y1 = 0F;
            this.line86.Y2 = 0.1968504F;
            // 
            // line87
            // 
            this.line87.Height = 0.1968504F;
            this.line87.Left = 11.10236F;
            this.line87.LineWeight = 1F;
            this.line87.Name = "line87";
            this.line87.Top = 0F;
            this.line87.Width = 0F;
            this.line87.X1 = 11.10236F;
            this.line87.X2 = 11.10236F;
            this.line87.Y1 = 0F;
            this.line87.Y2 = 0.1968504F;
            // 
            // line88
            // 
            this.line88.Height = 0.1968504F;
            this.line88.Left = 11.22047F;
            this.line88.LineWeight = 1F;
            this.line88.Name = "line88";
            this.line88.Top = 0F;
            this.line88.Width = 0F;
            this.line88.X1 = 11.22047F;
            this.line88.X2 = 11.22047F;
            this.line88.Y1 = 0F;
            this.line88.Y2 = 0.1968504F;
            // 
            // line89
            // 
            this.line89.Height = 0F;
            this.line89.Left = 8.267716F;
            this.line89.LineWeight = 1F;
            this.line89.Name = "line89";
            this.line89.Top = 0.1968504F;
            this.line89.Width = 1.535433F;
            this.line89.X1 = 8.267716F;
            this.line89.X2 = 9.803149F;
            this.line89.Y1 = 0.1968504F;
            this.line89.Y2 = 0.1968504F;
            // 
            // line90
            // 
            this.line90.Height = 0F;
            this.line90.Left = 9.92126F;
            this.line90.LineWeight = 1F;
            this.line90.Name = "line90";
            this.line90.Top = 0.1968504F;
            this.line90.Width = 1.41732F;
            this.line90.X1 = 9.92126F;
            this.line90.X2 = 11.33858F;
            this.line90.Y1 = 0.1968504F;
            this.line90.Y2 = 0.1968504F;
            // 
            // line91
            // 
            this.line91.Height = 0F;
            this.line91.Left = 1.649606F;
            this.line91.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.line91.LineWeight = 1F;
            this.line91.Name = "line91";
            this.line91.Top = 0.2637796F;
            this.line91.Width = 6.500001F;
            this.line91.X1 = 1.649606F;
            this.line91.X2 = 8.149607F;
            this.line91.Y1 = 0.2637796F;
            this.line91.Y2 = 0.2637796F;
            // 
            // line92
            // 
            this.line92.Height = 0F;
            this.line92.Left = 1.649606F;
            this.line92.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.line92.LineWeight = 1F;
            this.line92.Name = "line92";
            this.line92.Top = 0.5275591F;
            this.line92.Width = 6.499999F;
            this.line92.X1 = 1.649606F;
            this.line92.X2 = 8.149605F;
            this.line92.Y1 = 0.5275591F;
            this.line92.Y2 = 0.5275591F;
            // 
            // line16
            // 
            this.line16.Height = 0.7874016F;
            this.line16.Left = 11.33858F;
            this.line16.LineWeight = 2F;
            this.line16.Name = "line16";
            this.line16.Top = 0F;
            this.line16.Width = 0F;
            this.line16.X1 = 11.33858F;
            this.line16.X2 = 11.33858F;
            this.line16.Y1 = 0F;
            this.line16.Y2 = 0.7874016F;
            // 
            // textBox1
            // 
            this.textBox1.DataField = "ITEM011";
            this.textBox1.Height = 0.157874F;
            this.textBox1.Left = 0.02716536F;
            this.textBox1.MultiLine = false;
            this.textBox1.Name = "textBox1";
            this.textBox1.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox1.Tag = "";
            this.textBox1.Text = "ITEM011";
            this.textBox1.Top = 0.03897638F;
            this.textBox1.Width = 0.4712599F;
            // 
            // textBox3
            // 
            this.textBox3.DataField = "ITEM012";
            this.textBox3.Height = 0.157874F;
            this.textBox3.Left = 0.4984252F;
            this.textBox3.MultiLine = false;
            this.textBox3.Name = "textBox3";
            this.textBox3.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.textBox3.Tag = "";
            this.textBox3.Text = "ITEM012";
            this.textBox3.Top = 0.03897638F;
            this.textBox3.Width = 1.118898F;
            // 
            // textBox4
            // 
            this.textBox4.DataField = "ITEM013";
            this.textBox4.Height = 0.157874F;
            this.textBox4.Left = 1.108268F;
            this.textBox4.MultiLine = false;
            this.textBox4.Name = "textBox4";
            this.textBox4.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.textBox4.Tag = "";
            this.textBox4.Text = "ITEM013";
            this.textBox4.Top = 0.2055118F;
            this.textBox4.Width = 0.5090549F;
            // 
            // textBox5
            // 
            this.textBox5.DataField = "ITEM014";
            this.textBox5.Height = 0.157874F;
            this.textBox5.Left = 0.4984252F;
            this.textBox5.MultiLine = false;
            this.textBox5.Name = "textBox5";
            this.textBox5.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.textBox5.Tag = "";
            this.textBox5.Text = "ITEM014";
            this.textBox5.Top = 0.6295276F;
            this.textBox5.Width = 1.147638F;
            // 
            // textBox6
            // 
            this.textBox6.DataField = "ITEM021";
            this.textBox6.Height = 0.157874F;
            this.textBox6.Left = 1.649606F;
            this.textBox6.MultiLine = false;
            this.textBox6.Name = "textBox6";
            this.textBox6.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox6.Tag = "";
            this.textBox6.Text = "ITEM021";
            this.textBox6.Top = 0.03897638F;
            this.textBox6.Width = 0.9188976F;
            // 
            // textBox7
            // 
            this.textBox7.DataField = "ITEM029";
            this.textBox7.Height = 0.157874F;
            this.textBox7.Left = 1.646063F;
            this.textBox7.MultiLine = false;
            this.textBox7.Name = "textBox7";
            this.textBox7.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox7.Tag = "";
            this.textBox7.Text = "ITEM029";
            this.textBox7.Top = 0.3181103F;
            this.textBox7.Width = 0.9188979F;
            // 
            // textBox8
            // 
            this.textBox8.DataField = "ITEM037";
            this.textBox8.Height = 0.157874F;
            this.textBox8.Left = 1.649606F;
            this.textBox8.MultiLine = false;
            this.textBox8.Name = "textBox8";
            this.textBox8.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox8.Tag = "";
            this.textBox8.Text = "ITEM037";
            this.textBox8.Top = 0.5862205F;
            this.textBox8.Width = 0.9188979F;
            // 
            // textBox9
            // 
            this.textBox9.DataField = "ITEM022";
            this.textBox9.Height = 0.157874F;
            this.textBox9.Left = 2.579528F;
            this.textBox9.MultiLine = false;
            this.textBox9.Name = "textBox9";
            this.textBox9.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox9.Tag = "";
            this.textBox9.Text = "ITEM022";
            this.textBox9.Top = 0.03897638F;
            this.textBox9.Width = 0.8877953F;
            // 
            // textBox10
            // 
            this.textBox10.DataField = "ITEM023";
            this.textBox10.Height = 0.157874F;
            this.textBox10.Left = 3.498425F;
            this.textBox10.MultiLine = false;
            this.textBox10.Name = "textBox10";
            this.textBox10.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox10.Tag = "";
            this.textBox10.Text = "ITEM023";
            this.textBox10.Top = 0.03897639F;
            this.textBox10.Width = 0.8082674F;
            // 
            // textBox11
            // 
            this.textBox11.DataField = "ITEM024";
            this.textBox11.Height = 0.157874F;
            this.textBox11.Left = 4.33504F;
            this.textBox11.MultiLine = false;
            this.textBox11.Name = "textBox11";
            this.textBox11.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox11.Tag = "";
            this.textBox11.Text = "ITEM024";
            this.textBox11.Top = 0.03897638F;
            this.textBox11.Width = 0.7559059F;
            // 
            // textBox12
            // 
            this.textBox12.DataField = "ITEM025";
            this.textBox12.Height = 0.157874F;
            this.textBox12.Left = 5.090945F;
            this.textBox12.MultiLine = false;
            this.textBox12.Name = "textBox12";
            this.textBox12.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox12.Tag = "";
            this.textBox12.Text = "ITEM025";
            this.textBox12.Top = 0.03897638F;
            this.textBox12.Width = 0.6507873F;
            // 
            // textBox13
            // 
            this.textBox13.DataField = "ITEM026";
            this.textBox13.Height = 0.157874F;
            this.textBox13.Left = 5.769292F;
            this.textBox13.MultiLine = false;
            this.textBox13.Name = "textBox13";
            this.textBox13.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox13.Tag = "";
            this.textBox13.Text = "ITEM026";
            this.textBox13.Top = 0.03897638F;
            this.textBox13.Width = 0.7759842F;
            // 
            // textBox14
            // 
            this.textBox14.DataField = "ITEM027";
            this.textBox14.Height = 0.157874F;
            this.textBox14.Left = 6.581497F;
            this.textBox14.MultiLine = false;
            this.textBox14.Name = "textBox14";
            this.textBox14.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox14.Tag = "";
            this.textBox14.Text = "ITEM027";
            this.textBox14.Top = 0.03897638F;
            this.textBox14.Width = 0.8114169F;
            // 
            // textBox15
            // 
            this.textBox15.DataField = "ITEM028";
            this.textBox15.Height = 0.157874F;
            this.textBox15.Left = 7.420866F;
            this.textBox15.MultiLine = false;
            this.textBox15.Name = "textBox15";
            this.textBox15.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox15.Tag = "";
            this.textBox15.Text = "ITEM028";
            this.textBox15.Top = 0.03897638F;
            this.textBox15.Width = 0.7287397F;
            // 
            // textBox16
            // 
            this.textBox16.DataField = "ITEM030";
            this.textBox16.Height = 0.157874F;
            this.textBox16.Left = 2.579528F;
            this.textBox16.MultiLine = false;
            this.textBox16.Name = "textBox16";
            this.textBox16.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox16.Tag = "";
            this.textBox16.Text = "ITEM030";
            this.textBox16.Top = 0.3181103F;
            this.textBox16.Width = 0.8877952F;
            // 
            // textBox17
            // 
            this.textBox17.DataField = "ITEM031";
            this.textBox17.Height = 0.157874F;
            this.textBox17.Left = 3.498426F;
            this.textBox17.MultiLine = false;
            this.textBox17.Name = "textBox17";
            this.textBox17.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox17.Tag = "";
            this.textBox17.Text = "ITEM031";
            this.textBox17.Top = 0.3181103F;
            this.textBox17.Width = 0.8082674F;
            // 
            // textBox18
            // 
            this.textBox18.DataField = "ITEM032";
            this.textBox18.Height = 0.157874F;
            this.textBox18.Left = 4.33504F;
            this.textBox18.MultiLine = false;
            this.textBox18.Name = "textBox18";
            this.textBox18.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox18.Tag = "";
            this.textBox18.Text = "ITEM032";
            this.textBox18.Top = 0.3181103F;
            this.textBox18.Width = 0.7559056F;
            // 
            // textBox19
            // 
            this.textBox19.DataField = "ITEM033";
            this.textBox19.Height = 0.157874F;
            this.textBox19.Left = 5.090945F;
            this.textBox19.MultiLine = false;
            this.textBox19.Name = "textBox19";
            this.textBox19.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox19.Tag = "";
            this.textBox19.Text = "ITEM033";
            this.textBox19.Top = 0.3181103F;
            this.textBox19.Width = 0.6507873F;
            // 
            // textBox20
            // 
            this.textBox20.DataField = "ITEM034";
            this.textBox20.Height = 0.157874F;
            this.textBox20.Left = 5.769292F;
            this.textBox20.MultiLine = false;
            this.textBox20.Name = "textBox20";
            this.textBox20.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox20.Tag = "";
            this.textBox20.Text = "ITEM034";
            this.textBox20.Top = 0.3181103F;
            this.textBox20.Width = 0.775984F;
            // 
            // textBox21
            // 
            this.textBox21.DataField = "ITEM035";
            this.textBox21.Height = 0.157874F;
            this.textBox21.Left = 6.581497F;
            this.textBox21.MultiLine = false;
            this.textBox21.Name = "textBox21";
            this.textBox21.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox21.Tag = "";
            this.textBox21.Text = "ITEM035";
            this.textBox21.Top = 0.3181103F;
            this.textBox21.Width = 0.8114166F;
            // 
            // textBox22
            // 
            this.textBox22.DataField = "ITEM036";
            this.textBox22.Height = 0.157874F;
            this.textBox22.Left = 7.420868F;
            this.textBox22.MultiLine = false;
            this.textBox22.Name = "textBox22";
            this.textBox22.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox22.Tag = "";
            this.textBox22.Text = "ITEM036";
            this.textBox22.Top = 0.3181103F;
            this.textBox22.Width = 0.7287396F;
            // 
            // textBox23
            // 
            this.textBox23.DataField = "ITEM038";
            this.textBox23.Height = 0.157874F;
            this.textBox23.Left = 2.579528F;
            this.textBox23.MultiLine = false;
            this.textBox23.Name = "textBox23";
            this.textBox23.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox23.Tag = "";
            this.textBox23.Text = "ITEM038";
            this.textBox23.Top = 0.5862205F;
            this.textBox23.Width = 0.8877952F;
            // 
            // textBox24
            // 
            this.textBox24.DataField = "ITEM039";
            this.textBox24.Height = 0.157874F;
            this.textBox24.Left = 3.498426F;
            this.textBox24.MultiLine = false;
            this.textBox24.Name = "textBox24";
            this.textBox24.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox24.Tag = "";
            this.textBox24.Text = "ITEM039";
            this.textBox24.Top = 0.5862205F;
            this.textBox24.Width = 0.8082674F;
            // 
            // textBox25
            // 
            this.textBox25.DataField = "ITEM040";
            this.textBox25.Height = 0.157874F;
            this.textBox25.Left = 4.33504F;
            this.textBox25.MultiLine = false;
            this.textBox25.Name = "textBox25";
            this.textBox25.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox25.Tag = "";
            this.textBox25.Text = "ITEM040";
            this.textBox25.Top = 0.5862205F;
            this.textBox25.Width = 0.7559056F;
            // 
            // textBox26
            // 
            this.textBox26.DataField = "ITEM041";
            this.textBox26.Height = 0.157874F;
            this.textBox26.Left = 5.090945F;
            this.textBox26.MultiLine = false;
            this.textBox26.Name = "textBox26";
            this.textBox26.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox26.Tag = "";
            this.textBox26.Text = "ITEM041";
            this.textBox26.Top = 0.5862205F;
            this.textBox26.Width = 0.6507873F;
            // 
            // textBox27
            // 
            this.textBox27.DataField = "ITEM042";
            this.textBox27.Height = 0.157874F;
            this.textBox27.Left = 5.769292F;
            this.textBox27.MultiLine = false;
            this.textBox27.Name = "textBox27";
            this.textBox27.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox27.Tag = "";
            this.textBox27.Text = "ITEM042";
            this.textBox27.Top = 0.5862205F;
            this.textBox27.Width = 0.775984F;
            // 
            // textBox28
            // 
            this.textBox28.DataField = "ITEM043";
            this.textBox28.Height = 0.157874F;
            this.textBox28.Left = 6.581497F;
            this.textBox28.MultiLine = false;
            this.textBox28.Name = "textBox28";
            this.textBox28.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox28.Tag = "";
            this.textBox28.Text = "ITEM043";
            this.textBox28.Top = 0.5862205F;
            this.textBox28.Width = 0.8114166F;
            // 
            // textBox29
            // 
            this.textBox29.DataField = "ITEM044";
            this.textBox29.Height = 0.157874F;
            this.textBox29.Left = 7.420868F;
            this.textBox29.MultiLine = false;
            this.textBox29.Name = "textBox29";
            this.textBox29.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox29.Tag = "";
            this.textBox29.Text = "ITEM044";
            this.textBox29.Top = 0.5862205F;
            this.textBox29.Width = 0.7287396F;
            // 
            // textBox30
            // 
            this.textBox30.DataField = "ITEM051";
            this.textBox30.Height = 0.157874F;
            this.textBox30.Left = 8.137795F;
            this.textBox30.MultiLine = false;
            this.textBox30.Name = "textBox30";
            this.textBox30.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.textBox30.Tag = "";
            this.textBox30.Text = "ITEM051";
            this.textBox30.Top = 0.2972441F;
            this.textBox30.Width = 0.1263779F;
            // 
            // textBox32
            // 
            this.textBox32.DataField = "ITEM052";
            this.textBox32.Height = 0.157874F;
            this.textBox32.Left = 8.393702F;
            this.textBox32.MultiLine = false;
            this.textBox32.Name = "textBox32";
            this.textBox32.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.textBox32.Tag = "";
            this.textBox32.Text = "*";
            this.textBox32.Top = 0.007874017F;
            this.textBox32.Width = 0.09488188F;
            // 
            // textBox33
            // 
            this.textBox33.DataField = "ITEM053";
            this.textBox33.Height = 0.157874F;
            this.textBox33.Left = 8.515749F;
            this.textBox33.MultiLine = false;
            this.textBox33.Name = "textBox33";
            this.textBox33.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.textBox33.Tag = "";
            this.textBox33.Text = "*";
            this.textBox33.Top = 0.007874017F;
            this.textBox33.Width = 0.09488188F;
            // 
            // textBox34
            // 
            this.textBox34.DataField = "ITEM054";
            this.textBox34.Height = 0.157874F;
            this.textBox34.Left = 8.639765F;
            this.textBox34.MultiLine = false;
            this.textBox34.Name = "textBox34";
            this.textBox34.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.textBox34.Tag = "";
            this.textBox34.Text = "*";
            this.textBox34.Top = 0.007874017F;
            this.textBox34.Width = 0.09488188F;
            // 
            // textBox35
            // 
            this.textBox35.DataField = "ITEM055";
            this.textBox35.Height = 0.157874F;
            this.textBox35.Left = 8.98819F;
            this.textBox35.MultiLine = false;
            this.textBox35.Name = "textBox35";
            this.textBox35.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.textBox35.Tag = "";
            this.textBox35.Text = "*";
            this.textBox35.Top = 0.007874017F;
            this.textBox35.Width = 0.09488188F;
            // 
            // textBox36
            // 
            this.textBox36.DataField = "ITEM056";
            this.textBox36.Height = 0.157874F;
            this.textBox36.Left = 9.1F;
            this.textBox36.MultiLine = false;
            this.textBox36.Name = "textBox36";
            this.textBox36.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.textBox36.Tag = "";
            this.textBox36.Text = "*";
            this.textBox36.Top = 0.007874017F;
            this.textBox36.Width = 0.09488188F;
            // 
            // textBox37
            // 
            this.textBox37.DataField = "ITEM057";
            this.textBox37.Height = 0.157874F;
            this.textBox37.Left = 9.22756F;
            this.textBox37.MultiLine = false;
            this.textBox37.Name = "textBox37";
            this.textBox37.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.textBox37.Tag = "";
            this.textBox37.Text = "*";
            this.textBox37.Top = 0.007874017F;
            this.textBox37.Width = 0.09488188F;
            // 
            // textBox38
            // 
            this.textBox38.DataField = "ITEM058";
            this.textBox38.Height = 0.157874F;
            this.textBox38.Left = 9.342521F;
            this.textBox38.MultiLine = false;
            this.textBox38.Name = "textBox38";
            this.textBox38.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.textBox38.Tag = "";
            this.textBox38.Text = "*";
            this.textBox38.Top = 0.007874017F;
            this.textBox38.Width = 0.09488188F;
            // 
            // textBox39
            // 
            this.textBox39.DataField = "ITEM059";
            this.textBox39.Height = 0.157874F;
            this.textBox39.Left = 9.46063F;
            this.textBox39.MultiLine = false;
            this.textBox39.Name = "textBox39";
            this.textBox39.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.textBox39.Tag = "";
            this.textBox39.Text = "*";
            this.textBox39.Top = 0.007874017F;
            this.textBox39.Width = 0.09488188F;
            // 
            // textBox40
            // 
            this.textBox40.DataField = "ITEM060";
            this.textBox40.Height = 0.157874F;
            this.textBox40.Left = 9.570867F;
            this.textBox40.MultiLine = false;
            this.textBox40.Name = "textBox40";
            this.textBox40.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.textBox40.Tag = "";
            this.textBox40.Text = "*";
            this.textBox40.Top = 0.007874017F;
            this.textBox40.Width = 0.09488188F;
            // 
            // textBox41
            // 
            this.textBox41.DataField = "ITEM061";
            this.textBox41.Height = 0.157874F;
            this.textBox41.Left = 9.700395F;
            this.textBox41.MultiLine = false;
            this.textBox41.Name = "textBox41";
            this.textBox41.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.textBox41.Tag = "";
            this.textBox41.Text = "*";
            this.textBox41.Top = 0.007874017F;
            this.textBox41.Width = 0.09488188F;
            // 
            // txtkubun1
            // 
            this.txtkubun1.DataField = "ITEM063";
            this.txtkubun1.Height = 0.157874F;
            this.txtkubun1.Left = 9.917717F;
            this.txtkubun1.MultiLine = false;
            this.txtkubun1.Name = "txtkubun1";
            this.txtkubun1.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.txtkubun1.Tag = "";
            this.txtkubun1.Text = "1";
            this.txtkubun1.Top = 0.04370079F;
            this.txtkubun1.Width = 0.1263779F;
            // 
            // txtkubun2
            // 
            this.txtkubun2.DataField = "ITEM064";
            this.txtkubun2.Height = 0.157874F;
            this.txtkubun2.Left = 10.03937F;
            this.txtkubun2.MultiLine = false;
            this.txtkubun2.Name = "txtkubun2";
            this.txtkubun2.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.txtkubun2.Tag = "";
            this.txtkubun2.Text = "1";
            this.txtkubun2.Top = 0.04370079F;
            this.txtkubun2.Width = 0.1263779F;
            // 
            // txtkubun3
            // 
            this.txtkubun3.DataField = "ITEM065";
            this.txtkubun3.Height = 0.157874F;
            this.txtkubun3.Left = 10.15709F;
            this.txtkubun3.MultiLine = false;
            this.txtkubun3.Name = "txtkubun3";
            this.txtkubun3.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.txtkubun3.Tag = "";
            this.txtkubun3.Text = "1";
            this.txtkubun3.Top = 0.04370079F;
            this.txtkubun3.Width = 0.1263779F;
            // 
            // txtkubun4
            // 
            this.txtkubun4.DataField = "ITEM066";
            this.txtkubun4.Height = 0.157874F;
            this.txtkubun4.Left = 10.27953F;
            this.txtkubun4.MultiLine = false;
            this.txtkubun4.Name = "txtkubun4";
            this.txtkubun4.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.txtkubun4.Tag = "";
            this.txtkubun4.Text = "1";
            this.txtkubun4.Top = 0.04370079F;
            this.txtkubun4.Width = 0.1263779F;
            // 
            // txtkubun5
            // 
            this.txtkubun5.DataField = "ITEM067";
            this.txtkubun5.Height = 0.157874F;
            this.txtkubun5.Left = 10.39528F;
            this.txtkubun5.MultiLine = false;
            this.txtkubun5.Name = "txtkubun5";
            this.txtkubun5.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.txtkubun5.Tag = "";
            this.txtkubun5.Text = "1";
            this.txtkubun5.Top = 0.04370079F;
            this.txtkubun5.Width = 0.1263779F;
            // 
            // txtkubun6
            // 
            this.txtkubun6.DataField = "ITEM068";
            this.txtkubun6.Height = 0.157874F;
            this.txtkubun6.Left = 10.51181F;
            this.txtkubun6.MultiLine = false;
            this.txtkubun6.Name = "txtkubun6";
            this.txtkubun6.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.txtkubun6.Tag = "";
            this.txtkubun6.Text = "1";
            this.txtkubun6.Top = 0.04370079F;
            this.txtkubun6.Width = 0.1263779F;
            // 
            // txtkubun7
            // 
            this.txtkubun7.DataField = "ITEM069";
            this.txtkubun7.Height = 0.157874F;
            this.txtkubun7.Left = 11.10197F;
            this.txtkubun7.MultiLine = false;
            this.txtkubun7.Name = "txtkubun7";
            this.txtkubun7.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.txtkubun7.Tag = "";
            this.txtkubun7.Text = "1";
            this.txtkubun7.Top = 0.04370079F;
            this.txtkubun7.Width = 0.1263779F;
            // 
            // txtkubun8
            // 
            this.txtkubun8.DataField = "ITEM070";
            this.txtkubun8.Height = 0.157874F;
            this.txtkubun8.Left = 11.22402F;
            this.txtkubun8.MultiLine = false;
            this.txtkubun8.Name = "txtkubun8";
            this.txtkubun8.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.txtkubun8.Tag = "";
            this.txtkubun8.Text = "1";
            this.txtkubun8.Top = 0.04370079F;
            this.txtkubun8.Width = 0.1263779F;
            // 
            // textBox51
            // 
            this.textBox51.DataField = "ITEM062";
            this.textBox51.Height = 0.157874F;
            this.textBox51.Left = 9.779529F;
            this.textBox51.MultiLine = false;
            this.textBox51.Name = "textBox51";
            this.textBox51.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.textBox51.Tag = "";
            this.textBox51.Text = "ITEM062";
            this.textBox51.Top = 0.3181103F;
            this.textBox51.Width = 0.1342524F;
            // 
            // lbl1
            // 
            this.lbl1.Height = 0.1665355F;
            this.lbl1.HyperLink = null;
            this.lbl1.Left = 8.283859F;
            this.lbl1.Name = "lbl1";
            this.lbl1.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.lbl1.Tag = "";
            this.lbl1.Text = "配偶者合計所得";
            this.lbl1.Top = 0.2291339F;
            this.lbl1.Width = 0.9188976F;
            // 
            // lbl2
            // 
            this.lbl2.Height = 0.1665355F;
            this.lbl2.HyperLink = null;
            this.lbl2.Left = 8.283859F;
            this.lbl2.Name = "lbl2";
            this.lbl2.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.lbl2.Tag = "";
            this.lbl2.Text = "個人年金保険料";
            this.lbl2.Top = 0.4192914F;
            this.lbl2.Width = 0.9188976F;
            // 
            // lbl3
            // 
            this.lbl3.Height = 0.1665355F;
            this.lbl3.HyperLink = null;
            this.lbl3.Left = 8.283859F;
            this.lbl3.Name = "lbl3";
            this.lbl3.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.lbl3.Tag = "";
            this.lbl3.Text = "旧長期損害保険料";
            this.lbl3.Top = 0.6098426F;
            this.lbl3.Width = 0.9188976F;
            // 
            // textBox31
            // 
            this.textBox31.DataField = "ITEM075";
            this.textBox31.Height = 0.1665355F;
            this.textBox31.Left = 9.22756F;
            this.textBox31.MultiLine = false;
            this.textBox31.Name = "textBox31";
            this.textBox31.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox31.Tag = "";
            this.textBox31.Text = "ITEM075";
            this.textBox31.Top = 0.2291339F;
            this.textBox31.Width = 0.5677165F;
            // 
            // textBox50
            // 
            this.textBox50.DataField = "ITEM076";
            this.textBox50.Height = 0.1665355F;
            this.textBox50.Left = 9.22756F;
            this.textBox50.MultiLine = false;
            this.textBox50.Name = "textBox50";
            this.textBox50.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox50.Tag = "";
            this.textBox50.Text = "ITEM076";
            this.textBox50.Top = 0.4192914F;
            this.textBox50.Width = 0.5677165F;
            // 
            // textBox52
            // 
            this.textBox52.DataField = "ITEM077";
            this.textBox52.Height = 0.1665355F;
            this.textBox52.Left = 9.22756F;
            this.textBox52.MultiLine = false;
            this.textBox52.Name = "textBox52";
            this.textBox52.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align" +
    ": right; ddo-char-set: 1";
            this.textBox52.Tag = "";
            this.textBox52.Text = "ITEM077";
            this.textBox52.Top = 0.6098426F;
            this.textBox52.Width = 0.5677165F;
            // 
            // textBox53
            // 
            this.textBox53.DataField = "ITEM078";
            this.textBox53.Height = 0.1665355F;
            this.textBox53.Left = 9.925198F;
            this.textBox53.MultiLine = false;
            this.textBox53.Name = "textBox53";
            this.textBox53.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.textBox53.Tag = "";
            this.textBox53.Text = "ITEM078";
            this.textBox53.Top = 0.4196851F;
            this.textBox53.Width = 1.394094F;
            // 
            // textBox54
            // 
            this.textBox54.DataField = "ITEM079";
            this.textBox54.Height = 0.1665355F;
            this.textBox54.Left = 9.925198F;
            this.textBox54.MultiLine = false;
            this.textBox54.Name = "textBox54";
            this.textBox54.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.textBox54.Tag = "";
            this.textBox54.Text = "ITEM079";
            this.textBox54.Top = 0.6102363F;
            this.textBox54.Width = 1.394094F;
            // 
            // lbl4
            // 
            this.lbl4.Height = 0.1665355F;
            this.lbl4.HyperLink = null;
            this.lbl4.Left = 9.947245F;
            this.lbl4.Name = "lbl4";
            this.lbl4.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 8pt; font-weight: normal; text-align" +
    ": left; ddo-char-set: 1";
            this.lbl4.Tag = "";
            this.lbl4.Text = "適要";
            this.lbl4.Top = 0.2055118F;
            this.lbl4.Width = 0.2578748F;
            // 
            // label54
            // 
            this.label54.Height = 0.157874F;
            this.label54.HyperLink = null;
            this.label54.Left = 10.62165F;
            this.label54.Name = "label54";
            this.label54.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.label54.Tag = "";
            this.label54.Text = "0";
            this.label54.Top = 0.04370079F;
            this.label54.Width = 0.1263779F;
            // 
            // label55
            // 
            this.label55.Height = 0.157874F;
            this.label55.HyperLink = null;
            this.label55.Left = 10.73976F;
            this.label55.Name = "label55";
            this.label55.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.label55.Tag = "";
            this.label55.Text = "0";
            this.label55.Top = 0.04370079F;
            this.label55.Width = 0.1263779F;
            // 
            // label59
            // 
            this.label59.Height = 0.157874F;
            this.label59.HyperLink = null;
            this.label59.Left = 10.86614F;
            this.label59.Name = "label59";
            this.label59.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.label59.Tag = "";
            this.label59.Text = "0";
            this.label59.Top = 0.04370079F;
            this.label59.Width = 0.1263779F;
            // 
            // label60
            // 
            this.label60.Height = 0.157874F;
            this.label60.HyperLink = null;
            this.label60.Left = 10.97598F;
            this.label60.Name = "label60";
            this.label60.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 7pt; font-weight: normal; text-align" +
    ": center; ddo-char-set: 1";
            this.label60.Tag = "";
            this.label60.Text = "0";
            this.label60.Top = 0.04370079F;
            this.label60.Width = 0.1263779F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0.125F;
            this.pageFooter.Name = "pageFooter";
            // 
            // KYNR1011R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0F;
            this.PageSettings.Margins.Left = 0.09842519F;
            this.PageSettings.Margins.Right = 0.09842519F;
            this.PageSettings.Margins.Top = 0.1968504F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.267716F;
            this.PrintWidth = 11.38025F;
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.pageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.ラベル0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ITEM01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label56)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label57)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label58)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtkubun1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtkubun2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtkubun3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtkubun4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtkubun5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtkubun6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtkubun7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtkubun8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label59)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label60)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Label ラベル0;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox ITEM01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox2;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.Label label2;
        private GrapeCity.ActiveReports.SectionReportModel.Label label3;
        private GrapeCity.ActiveReports.SectionReportModel.Label label4;
        private GrapeCity.ActiveReports.SectionReportModel.Label label5;
        private GrapeCity.ActiveReports.SectionReportModel.Label label6;
        private GrapeCity.ActiveReports.SectionReportModel.Label label7;
        private GrapeCity.ActiveReports.SectionReportModel.Label label8;
        private GrapeCity.ActiveReports.SectionReportModel.Label label9;
        private GrapeCity.ActiveReports.SectionReportModel.Label label10;
        private GrapeCity.ActiveReports.SectionReportModel.Label label11;
        private GrapeCity.ActiveReports.SectionReportModel.Label label12;
        private GrapeCity.ActiveReports.SectionReportModel.Label label13;
        private GrapeCity.ActiveReports.SectionReportModel.Label label14;
        private GrapeCity.ActiveReports.SectionReportModel.Label label15;
        private GrapeCity.ActiveReports.SectionReportModel.Label label16;
        private GrapeCity.ActiveReports.SectionReportModel.Label label17;
        private GrapeCity.ActiveReports.SectionReportModel.Label label18;
        private GrapeCity.ActiveReports.SectionReportModel.Label label19;
        private GrapeCity.ActiveReports.SectionReportModel.Label label20;
        private GrapeCity.ActiveReports.SectionReportModel.Label label21;
        private GrapeCity.ActiveReports.SectionReportModel.Label label22;
        private GrapeCity.ActiveReports.SectionReportModel.Label label23;
        private GrapeCity.ActiveReports.SectionReportModel.Label label24;
        private GrapeCity.ActiveReports.SectionReportModel.Label label25;
        private GrapeCity.ActiveReports.SectionReportModel.Label label26;
        private GrapeCity.ActiveReports.SectionReportModel.Label label27;
        private GrapeCity.ActiveReports.SectionReportModel.Line line3;
        private GrapeCity.ActiveReports.SectionReportModel.Line line4;
        private GrapeCity.ActiveReports.SectionReportModel.Line line5;
        private GrapeCity.ActiveReports.SectionReportModel.Line line6;
        private GrapeCity.ActiveReports.SectionReportModel.Line line7;
        private GrapeCity.ActiveReports.SectionReportModel.Line line8;
        private GrapeCity.ActiveReports.SectionReportModel.Line line9;
        private GrapeCity.ActiveReports.SectionReportModel.Line line10;
        private GrapeCity.ActiveReports.SectionReportModel.Line line11;
        private GrapeCity.ActiveReports.SectionReportModel.Line line12;
        private GrapeCity.ActiveReports.SectionReportModel.Line line13;
        private GrapeCity.ActiveReports.SectionReportModel.Line line14;
        private GrapeCity.ActiveReports.SectionReportModel.Line line15;
        private GrapeCity.ActiveReports.SectionReportModel.Line line17;
        private GrapeCity.ActiveReports.SectionReportModel.Line line18;
        private GrapeCity.ActiveReports.SectionReportModel.Line line19;
        private GrapeCity.ActiveReports.SectionReportModel.Line line20;
        private GrapeCity.ActiveReports.SectionReportModel.Line line21;
        private GrapeCity.ActiveReports.SectionReportModel.Line line22;
        private GrapeCity.ActiveReports.SectionReportModel.Line line23;
        private GrapeCity.ActiveReports.SectionReportModel.Line line24;
        private GrapeCity.ActiveReports.SectionReportModel.Line line26;
        private GrapeCity.ActiveReports.SectionReportModel.Line line27;
        private GrapeCity.ActiveReports.SectionReportModel.Label label28;
        private GrapeCity.ActiveReports.SectionReportModel.Label label29;
        private GrapeCity.ActiveReports.SectionReportModel.Label label30;
        private GrapeCity.ActiveReports.SectionReportModel.Line line28;
        private GrapeCity.ActiveReports.SectionReportModel.Label label31;
        private GrapeCity.ActiveReports.SectionReportModel.Label label32;
        private GrapeCity.ActiveReports.SectionReportModel.Label label33;
        private GrapeCity.ActiveReports.SectionReportModel.Line line29;
        private GrapeCity.ActiveReports.SectionReportModel.Label label34;
        private GrapeCity.ActiveReports.SectionReportModel.Label label37;
        private GrapeCity.ActiveReports.SectionReportModel.Label label38;
        private GrapeCity.ActiveReports.SectionReportModel.Label label39;
        private GrapeCity.ActiveReports.SectionReportModel.Label label40;
        private GrapeCity.ActiveReports.SectionReportModel.Label label41;
        private GrapeCity.ActiveReports.SectionReportModel.Label label42;
        private GrapeCity.ActiveReports.SectionReportModel.Label label43;
        private GrapeCity.ActiveReports.SectionReportModel.Label label44;
        private GrapeCity.ActiveReports.SectionReportModel.Line line30;
        private GrapeCity.ActiveReports.SectionReportModel.Line line32;
        private GrapeCity.ActiveReports.SectionReportModel.Line line33;
        private GrapeCity.ActiveReports.SectionReportModel.Line line34;
        private GrapeCity.ActiveReports.SectionReportModel.Line line31;
        private GrapeCity.ActiveReports.SectionReportModel.Line line35;
        private GrapeCity.ActiveReports.SectionReportModel.Line line36;
        private GrapeCity.ActiveReports.SectionReportModel.Line line37;
        private GrapeCity.ActiveReports.SectionReportModel.Line line38;
        private GrapeCity.ActiveReports.SectionReportModel.Label label45;
        private GrapeCity.ActiveReports.SectionReportModel.Label label47;
        private GrapeCity.ActiveReports.SectionReportModel.Label label48;
        private GrapeCity.ActiveReports.SectionReportModel.Label label49;
        private GrapeCity.ActiveReports.SectionReportModel.Label label50;
        private GrapeCity.ActiveReports.SectionReportModel.Line line39;
        private GrapeCity.ActiveReports.SectionReportModel.Label label51;
        private GrapeCity.ActiveReports.SectionReportModel.Line line40;
        private GrapeCity.ActiveReports.SectionReportModel.Label label53;
        private GrapeCity.ActiveReports.SectionReportModel.Label label56;
        private GrapeCity.ActiveReports.SectionReportModel.Line line41;
        private GrapeCity.ActiveReports.SectionReportModel.Line line42;
        private GrapeCity.ActiveReports.SectionReportModel.Line line43;
        private GrapeCity.ActiveReports.SectionReportModel.Line line44;
        private GrapeCity.ActiveReports.SectionReportModel.Line line45;
        private GrapeCity.ActiveReports.SectionReportModel.Label label57;
        private GrapeCity.ActiveReports.SectionReportModel.Line line46;
        private GrapeCity.ActiveReports.SectionReportModel.Line line47;
        private GrapeCity.ActiveReports.SectionReportModel.Label label58;
        private GrapeCity.ActiveReports.SectionReportModel.Line line48;
        private GrapeCity.ActiveReports.SectionReportModel.Line line49;
        private GrapeCity.ActiveReports.SectionReportModel.Label label46;
        private GrapeCity.ActiveReports.SectionReportModel.Line line50;
        private GrapeCity.ActiveReports.SectionReportModel.Label label35;
        private GrapeCity.ActiveReports.SectionReportModel.Label label36;
        private GrapeCity.ActiveReports.SectionReportModel.Line line51;
        private GrapeCity.ActiveReports.SectionReportModel.Line line52;
        private GrapeCity.ActiveReports.SectionReportModel.Line line53;
        private GrapeCity.ActiveReports.SectionReportModel.Line line54;
        private GrapeCity.ActiveReports.SectionReportModel.Line line55;
        private GrapeCity.ActiveReports.SectionReportModel.Line line56;
        private GrapeCity.ActiveReports.SectionReportModel.Line line57;
        private GrapeCity.ActiveReports.SectionReportModel.Line line58;
        private GrapeCity.ActiveReports.SectionReportModel.Line line59;
        private GrapeCity.ActiveReports.SectionReportModel.Line line60;
        private GrapeCity.ActiveReports.SectionReportModel.Line line61;
        private GrapeCity.ActiveReports.SectionReportModel.Line line62;
        private GrapeCity.ActiveReports.SectionReportModel.Line line63;
        private GrapeCity.ActiveReports.SectionReportModel.Line line64;
        private GrapeCity.ActiveReports.SectionReportModel.Line line65;
        private GrapeCity.ActiveReports.SectionReportModel.Line line66;
        private GrapeCity.ActiveReports.SectionReportModel.Line line67;
        private GrapeCity.ActiveReports.SectionReportModel.Line line68;
        private GrapeCity.ActiveReports.SectionReportModel.Line line69;
        private GrapeCity.ActiveReports.SectionReportModel.Line line70;
        private GrapeCity.ActiveReports.SectionReportModel.Line line71;
        private GrapeCity.ActiveReports.SectionReportModel.Line line72;
        private GrapeCity.ActiveReports.SectionReportModel.Line line73;
        private GrapeCity.ActiveReports.SectionReportModel.Line line74;
        private GrapeCity.ActiveReports.SectionReportModel.Line line75;
        private GrapeCity.ActiveReports.SectionReportModel.Line line76;
        private GrapeCity.ActiveReports.SectionReportModel.Line line77;
        private GrapeCity.ActiveReports.SectionReportModel.Line line78;
        private GrapeCity.ActiveReports.SectionReportModel.Line line79;
        private GrapeCity.ActiveReports.SectionReportModel.Line line80;
        private GrapeCity.ActiveReports.SectionReportModel.Line line81;
        private GrapeCity.ActiveReports.SectionReportModel.Line line82;
        private GrapeCity.ActiveReports.SectionReportModel.Line line83;
        private GrapeCity.ActiveReports.SectionReportModel.Line line84;
        private GrapeCity.ActiveReports.SectionReportModel.Line line85;
        private GrapeCity.ActiveReports.SectionReportModel.Line line86;
        private GrapeCity.ActiveReports.SectionReportModel.Line line87;
        private GrapeCity.ActiveReports.SectionReportModel.Line line88;
        private GrapeCity.ActiveReports.SectionReportModel.Line line89;
        private GrapeCity.ActiveReports.SectionReportModel.Line line90;
        private GrapeCity.ActiveReports.SectionReportModel.Line line91;
        private GrapeCity.ActiveReports.SectionReportModel.Line line92;
        private GrapeCity.ActiveReports.SectionReportModel.Line line16;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox14;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox15;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox16;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox17;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox18;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox19;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox20;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox21;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox22;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox23;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox24;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox25;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox26;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox27;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox28;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox29;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox30;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox32;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox33;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox34;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox35;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox36;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox37;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox38;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox39;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox40;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox41;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtkubun1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtkubun2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtkubun3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtkubun4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtkubun5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtkubun6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtkubun7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtkubun8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox51;
        private GrapeCity.ActiveReports.SectionReportModel.Label lbl1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lbl2;
        private GrapeCity.ActiveReports.SectionReportModel.Label lbl3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox31;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox50;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox52;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox53;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox54;
        private GrapeCity.ActiveReports.SectionReportModel.Label lbl4;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape1;
        private GrapeCity.ActiveReports.SectionReportModel.Line line25;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtToday;
        private GrapeCity.ActiveReports.SectionReportModel.Label label52;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox55;
        private GrapeCity.ActiveReports.SectionReportModel.Label label54;
        private GrapeCity.ActiveReports.SectionReportModel.Label label55;
        private GrapeCity.ActiveReports.SectionReportModel.Label label59;
        private GrapeCity.ActiveReports.SectionReportModel.Label label60;

    }
}
