﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Reflection;


using GrapeCity.ActiveReports;

using jp.co.fsi.common.access;
using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.ky.kynr1011
{

    /// <summary>
    /// 年末調整一覧表(KYNR1011)
    /// </summary>
    public partial class KYNR1011 : BasePgForm
    {
        #region 変数
        int _sort;                                          // 出力ワークSORTフィールド
        decimal[] _shainGokei;                              // 社員別支給実績合計用
        DataTable _dtKyuSti;                                // 給与項目設定データ
        DataTable _dtShoSti;                                // 賞与項目設定データ
        #endregion

        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理用変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KYNR1011()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)

        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 年度初期値
            if (Util.ToInt(Util.ToString(this.txtGengoYearNendo.Text)) == 0)
            {
                string[] nendo = Util.ConvJpDate(new DateTime(this.UInfo.KaikeiNendo, 4, 1), this.Dba);
                this.lblGengoNendo.Text = nendo[0];
                this.txtGengoYearNendo.Text = nendo[2];
            }

            // オプション初期値
            this.rdoTaisho1.Checked = true;
            this.rdoOrder2.Checked = true;

            // 初期フォーカス
            this.txtGengoYearNendo.Focus();
            this.txtGengoYearNendo.SelectAll();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 元号年とコード項目に
            // フォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtGengoYearNendo":
                case "txtShainCdFr":
                case "txtShainCdTo":
                case "txtBumonCdFr":
                case "txtBumonCdTo":
                case "txtBukaCdFr":
                case "txtBukaCdTo":
                    this.btnF1.Enabled = true;
                    break;
                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        public override void PressF1()
        {
            Assembly asm;
            Type t;

            switch (this.ActiveCtlNm)
            {
                case "txtGengoYearNendo":
                    #region 元号検索
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("CMCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            if (this.ActiveCtlNm == "txtGengoYearNendo")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.InData = this.lblGengoNendo.Text;
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.lblGengoNendo.Text = result[1];

                                    // 和暦年未入力時の初期値
                                    if (Util.ToInt(Util.ToString(this.txtGengoYearNendo.Text)) == 0)
                                    {
                                        string[] nendo = Util.ConvJpDate(new DateTime(this.UInfo.KaikeiNendo, 4, 1), this.Dba);
                                        this.lblGengoNendo.Text = nendo[0];
                                        this.txtGengoYearNendo.Text = nendo[2];
                                    }

                                    // 和暦年補正
                                    DateTime adDate =
                                        Util.ConvAdDate(this.lblGengoNendo.Text, this.txtGengoYearNendo.Text, "4", "1", this.Dba);
                                    string[] jpDate = Util.ConvJpDate(adDate, this.Dba);
                                    this.lblGengoNendo.Text = jpDate[0];
                                    this.txtGengoYearNendo.Text = jpDate[2];
                                }
                            }
                        }

                    }
                    #endregion
                    break;
                case "txtShainCdFr":
                case "txtShainCdTo":
                    #region 社員検索
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("KYCM1021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.ky.kycm1021.KYCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            if (this.ActiveCtlNm == "txtShainCdFr")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.Par1 = "1";                     // ダイアログ起動パラメータ
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.txtShainCdFr.Text = result[0];
                                    this.lblShainCdFr.Text = result[1];
                                }
                            }
                            else if (this.ActiveCtlNm == "txtShainCdTo")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.Par1 = "1";                     // ダイアログ起動パラメータ
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.txtShainCdTo.Text = result[0];
                                    this.lblShainCdTo.Text = result[1];
                                }
                            }
                        }
                    }
                    #endregion
                    break;
                case "txtBumonCdFr":
                case "txtBumonCdTo":
                    #region 部門検索
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("KYCM1031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.ky.kycm1031.KYCM1031");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            if (this.ActiveCtlNm == "txtBumonCdFr")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.Par1 = "1";
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.txtBumonCdFr.Text = result[0];
                                    this.lblBumonCdFr.Text = result[1];
                                    this.txtBukaCdFr.Text = "";
                                    this.lblBukaCdFr.Text = "先　頭";
                                }
                            }
                            else if (this.ActiveCtlNm == "txtBumonCdTo")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.Par1 = "1";
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.txtBumonCdTo.Text = result[0];
                                    this.lblBumonCdTo.Text = result[1];
                                    this.txtBukaCdTo.Text = "";
                                    this.lblBukaCdTo.Text = "最　後";
                                }
                            }
                        }
                    }
                    #endregion
                    break;                
                case "txtBukaCdFr":
                case "txtBukaCdTo":
                    #region 部課検索
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("KYCM1041.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.ky.kycm1041.KYCM1041");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            if (this.ActiveCtlNm == "txtBukaCdFr" 
                                && !ValChk.IsEmpty(this.txtBumonCdFr.Text))
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.Par1 = "1";                         // 検索ダイアログ起動パラメータ
                                frm.InData = this.txtBumonCdFr.Text;    // (string)部門コード
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.txtBukaCdFr.Text = result[0];
                                    this.lblBukaCdFr.Text = result[1];
                                }
                            }
                            else if (this.ActiveCtlNm == "txtBukaCdTo"
                                && !ValChk.IsEmpty(this.txtBumonCdTo.Text))
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.Par1 = "1";                         // 検索ダイアログ起動パラメータ
                                frm.InData = this.txtBumonCdTo.Text;    // (string)部門コード
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.txtBukaCdTo.Text = result[0];
                                    this.lblBukaCdTo.Text = result[1];
                                }
                            }
                        }
                    }
                    #endregion
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
            {
                // プレビュー処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            if (Msg.ConfNmYesNo("印刷", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }

        #endregion

        #region イベント

        /// <summary>
        /// 社員コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShainCdFr_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtShainCdFr.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtShainCdFr.SelectAll();
                e.Cancel = true;
                return;
            }

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (this.txtShainCdFr.Text.Length > 0)
            {
                this.lblShainCdFr.Text = this.Dba.GetName(this.UInfo, "TB_KY_SHAIN_JOHO", " ", this.txtShainCdFr.Text);
            }
            else
            {
                this.lblShainCdFr.Text = "先　頭";
            }
        }

        /// <summary>
        /// 社員コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShainCdTo_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtShainCdTo.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtShainCdTo.SelectAll();
                e.Cancel = true;
                return;
            }

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (this.txtShainCdTo.Text.Length > 0)
            {
                this.lblShainCdTo.Text = this.Dba.GetName(this.UInfo, "TB_KY_SHAIN_JOHO", " ", this.txtShainCdTo.Text);
            }
            else
            {
                this.lblShainCdTo.Text = "最　後";
            }
        }

        /// <summary>
        /// 部門コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBumonCdFr_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtBumonCdFr.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtBumonCdFr.SelectAll();
                e.Cancel = true;
                return;
            }

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (this.txtBumonCdFr.Text.Length > 0)
            {
                this.lblBumonCdFr.Text = this.Dba.GetName(this.UInfo, "TB_KY_BUMON", " ", this.txtBumonCdFr.Text);
            }
            else
            {
                this.lblBumonCdFr.Text = "先　頭";
            }
        }

        /// <summary>
        /// 部門コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBumonCdTo_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtBumonCdTo.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtBumonCdTo.SelectAll();
                e.Cancel = true;
                return;
            }

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (this.txtBumonCdTo.Text.Length > 0)
            {
                this.lblBumonCdTo.Text = this.Dba.GetName(this.UInfo, "TB_KY_BUMON", " ", this.txtBumonCdTo.Text);
            }
            else
            {
                this.lblBumonCdTo.Text = "最　後";
            }
        }

        /// <summary>
        /// 部課コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBukaCdFr_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtBukaCdFr.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtBukaCdFr.SelectAll();
                e.Cancel = true;
                return;
            }

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (this.txtBukaCdFr.Text.Length > 0)
            {
                this.lblBukaCdFr.Text = this.Dba.GetBukaNm(this.UInfo, this.txtBumonCdFr.Text, this.txtBukaCdFr.Text);
            }
            else
            {
                this.lblBukaCdFr.Text = "先　頭";
            }
        }

        /// <summary>
        /// 部課コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBukaCdTo_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtBukaCdTo.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtBukaCdTo.SelectAll();
                e.Cancel = true;
                // Enter処理を無効化
                this._dtFlg = false;
                return;
            }

            // コードを元に名称を取得する
            // 取得された場合、名称をラベルに反映する
            if (this.txtBukaCdTo.Text.Length > 0)
            {
                this.lblBukaCdTo.Text = this.Dba.GetBukaNm(this.UInfo,this.txtBumonCdTo.Text, this.txtBukaCdTo.Text);
            }
            else
            {
                this.lblBukaCdTo.Text = "最　後";
            }

            // Enter処理を有効化
            this._dtFlg = true;
        }

        /// <summary>
        /// 部課コード(至)のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBukaCdTo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Flg)
            {
                // Enter処理を無効化
                this._dtFlg = false;
                
                if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
                {
                    // ﾌﾟﾚﾋﾞｭｰ処理
                    DoPrint(true);
                }
                else
                {
                    this.txtBukaCdTo.Focus();
                }
            }
        }

        /// <summary>
        /// 年度(年)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGengoYearNendo_Validating(object sender, CancelEventArgs e)
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtGengoYearNendo.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                this.txtGengoYearNendo.SelectAll();
                e.Cancel = true;
                return;
            }

            string[] nendo;

            // 年度の取得
            if (Util.ToInt(Util.ToString(this.txtGengoYearNendo.Text)) == 0)
            {
                // 無効年入力の時
                nendo = Util.ConvJpDate(new DateTime(this.UInfo.KaikeiNendo, 1, 1), this.Dba);
            }
            else
            {
                DateTime adNendo =
                    Util.ConvAdDate(this.lblGengoNendo.Text, this.txtGengoYearNendo.Text, "4", "1", this.Dba);
                nendo = Util.ConvJpDate(adNendo, this.Dba);
            }
            this.lblGengoNendo.Text = nendo[0];
            this.txtGengoYearNendo.Text = nendo[2];
        }

        #endregion

        #region privateメソッド
        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview)
        {
            try
            {
                bool dataFlag;

                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                dataFlag = MakeWkData();

                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    StringBuilder cols = new StringBuilder();
                    cols.Append("  ITEM001");
                    cols.Append(" ,ITEM002");
                    cols.Append(" ,ITEM003");
                    cols.Append(" ,ITEM004");
                    cols.Append(" ,ITEM005");
                    cols.Append(" ,ITEM006");
                    cols.Append(" ,ITEM007");
                    cols.Append(" ,ITEM008");
                    cols.Append(" ,ITEM009");
                    cols.Append(" ,ITEM010");
                    cols.Append(" ,ITEM011");
                    cols.Append(" ,ITEM012");
                    cols.Append(" ,ITEM013");
                    cols.Append(" ,ITEM014");
                    cols.Append(" ,ITEM015");
                    cols.Append(" ,ITEM016");
                    cols.Append(" ,ITEM017");
                    cols.Append(" ,ITEM018");
                    cols.Append(" ,ITEM019");
                    cols.Append(" ,ITEM020");
                    cols.Append(" ,ITEM021");
                    cols.Append(" ,ITEM022");
                    cols.Append(" ,ITEM023");
                    cols.Append(" ,ITEM024");
                    cols.Append(" ,ITEM025");
                    cols.Append(" ,ITEM026");
                    cols.Append(" ,ITEM027");
                    cols.Append(" ,ITEM028");
                    cols.Append(" ,ITEM029");
                    cols.Append(" ,ITEM030");
                    cols.Append(" ,ITEM031");
                    cols.Append(" ,ITEM032");
                    cols.Append(" ,ITEM033");
                    cols.Append(" ,ITEM034");
                    cols.Append(" ,ITEM035");
                    cols.Append(" ,ITEM036");
                    cols.Append(" ,ITEM037");
                    cols.Append(" ,ITEM038");
                    cols.Append(" ,ITEM039");
                    cols.Append(" ,ITEM040");
                    cols.Append(" ,ITEM041");
                    cols.Append(" ,ITEM042");
                    cols.Append(" ,ITEM043");
                    cols.Append(" ,ITEM044");
                    cols.Append(" ,ITEM045");
                    cols.Append(" ,ITEM046");
                    cols.Append(" ,ITEM047");
                    cols.Append(" ,ITEM048");
                    cols.Append(" ,ITEM049");
                    cols.Append(" ,ITEM050");
                    cols.Append(" ,ITEM051");
                    cols.Append(" ,ITEM052");
                    cols.Append(" ,ITEM053");
                    cols.Append(" ,ITEM054");
                    cols.Append(" ,ITEM055");
                    cols.Append(" ,ITEM056");
                    cols.Append(" ,ITEM057");
                    cols.Append(" ,ITEM058");
                    cols.Append(" ,ITEM059");
                    cols.Append(" ,ITEM060");
                    cols.Append(" ,ITEM061");
                    cols.Append(" ,ITEM062");
                    cols.Append(" ,ITEM063");
                    cols.Append(" ,ITEM064");
                    cols.Append(" ,ITEM065");
                    cols.Append(" ,ITEM066");
                    cols.Append(" ,ITEM067");
                    cols.Append(" ,ITEM068");
                    cols.Append(" ,ITEM069");
                    cols.Append(" ,ITEM070");
                    cols.Append(" ,ITEM071");
                    cols.Append(" ,ITEM072");
                    cols.Append(" ,ITEM073");
                    cols.Append(" ,ITEM074");
                    cols.Append(" ,ITEM075");
                    cols.Append(" ,ITEM076");
                    cols.Append(" ,ITEM077");
                    cols.Append(" ,ITEM078");
                    cols.Append(" ,ITEM079");
                    cols.Append(" ,ITEM080");
                    cols.Append(" ,ITEM081");
                    cols.Append(" ,ITEM082");
                    cols.Append(" ,ITEM083");
                    cols.Append(" ,ITEM084");
                    cols.Append(" ,ITEM085");
                    cols.Append(" ,ITEM086");
                    cols.Append(" ,ITEM087");
                    cols.Append(" ,ITEM088");
                    cols.Append(" ,ITEM090");
                    cols.Append(" ,ITEM091");
                    cols.Append(" ,ITEM092");
                    cols.Append(" ,ITEM093");
                    cols.Append(" ,ITEM094");
                    cols.Append(" ,ITEM095");
                    cols.Append(" ,ITEM096");
                    cols.Append(" ,ITEM097");
                    cols.Append(" ,ITEM098");
                    cols.Append(" ,ITEM099");
                    cols.Append(" ,ITEM100");

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                    // データの取得
                    DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_KY_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    // 帳票オブジェクトをインスタンス化
                    KYNR1011R rpt = new KYNR1011R(dtOutput);

                    if (isPreview)
                    {
                        // プレビュー画面表示
                        PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                        pFrm.WindowState = FormWindowState.Maximized;
                        pFrm.Show();
                    }
                    else
                    {
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
                else
                {
                    Msg.Info("該当データが存在しません。");
                    return;
                }
            }
            finally
            {
                this.Dba.Rollback();
            }
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {
            // 年度を西暦・和暦で保持
            DateTime nendo = Util.ConvAdDate(this.lblGengoNendo.Text, this.txtGengoYearNendo.Text, "1", "1", this.Dba);
            // 年度（12月）を西暦・和暦で保持
            DateTime nendoTo = Util.ConvAdDate(this.lblGengoNendo.Text, this.txtGengoYearNendo.Text, "12", "1", this.Dba);
            string[] jpNendo = Util.ConvJpDate(nendo, this.Dba);
            // 出力日付設定
            string[] nowDate = Util.ConvJpDate(DateTime.Now, this.Dba);
            string outputDate = nowDate[5];

            #region 画面上での指定値
            // 社員コード設定
            string shainCdFr;
            string shainCdTo;
            if (Util.ToDecimal(txtShainCdFr.Text) > 0) 
            {
                shainCdFr = txtShainCdFr.Text;
            }
            else
            {
                shainCdFr = "0";
            }
            if (Util.ToDecimal(txtShainCdTo.Text) > 0) 
            {
                shainCdTo = txtShainCdTo.Text;
            }
            else
            {
                shainCdTo = "999999";
            }
            // 部門コード設定
            string bumonCdFr;
            string bumonCdTo;
            if (Util.ToDecimal(txtBumonCdFr.Text) > 0)
            {
                bumonCdFr = txtBumonCdFr.Text;
            }
            else
            {
                bumonCdFr = "0";
            }
            if (Util.ToDecimal(txtBumonCdTo.Text) > 0)
            {
                bumonCdTo = txtBumonCdTo.Text;
            }
            else
            {
                bumonCdTo = "9999";
            }
            // 部課コード設定
            string bukaCdFr;
            string bukaCdTo;
            if (Util.ToDecimal(txtBukaCdFr.Text) > 0)
            {
                bukaCdFr = txtBukaCdFr.Text;
            }
            else
            {
                bukaCdFr = "0";
            }
            if (Util.ToDecimal(txtBukaCdTo.Text) > 0)
            {
                bukaCdTo = txtBukaCdTo.Text;
            }
            else
            {
                bukaCdTo = "9999";
            }
            #endregion

            DbParamCollection dpc;
            StringBuilder sql;
            StringBuilder sql1;
            StringBuilder sql2;
            DataTable dt;
            ArrayList alParams;

            #region _給与/賞与項目設定データ
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 2, this.UInfo.KaishaCd);
            // VI給与項目設定
            _dtKyuSti = this.Dba.GetDataTableByConditionWithParams(
                "*",
                "VI_KY_KYUYO_KOMOKU_STI",
                "KAISHA_CD = @KAISHA_CD AND INJI_KUBUN <> '0'",
                "KOMOKU_SHUBETSU, KOMOKU_BANGO",
                dpc);
            if (_dtKyuSti.Rows.Count == 0)
            {
                return false;
            }
            // VI賞与項目設定
            _dtShoSti = this.Dba.GetDataTableByConditionWithParams(
                "*",
                "VI_KY_SHOYO_KOMOKU_SETTEI",
                "KAISHA_CD = @KAISHA_CD AND INJI_KUBUN <> '0'",
                "KOMOKU_SHUBETSU, KOMOKU_BANGO",
                dpc);
            if (_dtShoSti.Rows.Count == 0)
            {
                return false;
            }
            #endregion

            #region dtMainLoop = VI社員情報(VI_KY_SHAIN_JOHO)
            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 2, this.UInfo.KaishaCd);
            dpc.SetParam("@SHAIN_CD_FR", SqlDbType.VarChar, 6, shainCdFr);
            dpc.SetParam("@SHAIN_CD_TO", SqlDbType.VarChar, 6, shainCdTo);
            dpc.SetParam("@BUMON_CD_FR", SqlDbType.VarChar, 4, bumonCdFr);
            dpc.SetParam("@BUMON_CD_TO", SqlDbType.VarChar, 4, bumonCdTo);
            dpc.SetParam("@BUKA_CD_FR", SqlDbType.VarChar, 4, bukaCdFr);
            dpc.SetParam("@BUKA_CD_TO", SqlDbType.VarChar, 4, bukaCdTo);
            sql = new StringBuilder();
            sql.Append("SELECT * FROM VI_KY_SHAIN_JOHO");
            sql.Append(" WHERE KAISHA_CD = @KAISHA_CD");
            sql.Append(" AND SHAIN_CD BETWEEN @SHAIN_CD_FR AND @SHAIN_CD_TO");
            sql.Append(" AND BUMON_CD BETWEEN @BUMON_CD_FR AND @BUMON_CD_TO");
            sql.Append(" AND BUKA_CD BETWEEN @BUKA_CD_FR AND @BUKA_CD_TO");
            if (this.rdoTaisho1.Checked)
            {
                sql.Append(" AND NENMATSU_CHOSEI = 1");
            }
            // ORDER句
            if (this.rdoOrder1.Checked) 
            {
                sql.Append(" ORDER BY KAISHA_CD ASC,SHAIN_CD ASC");                            // 社員順
            }
            else
            {
                sql.Append(" ORDER BY KAISHA_CD ASC,BUMON_CD ASC,BUKA_CD ASC,SHAIN_CD ASC");   // 部門順
            }
            #endregion

            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
            if (dtMainLoop.Rows.Count == 0)
            {
                return false;
            }
            else
            {
                _sort = 0;                                                                  // SORTフィールドカウンタ初期化

                string kyuyoMeisaiSql = GetKyKyuShoMeisaiSql(1);                            // 給与明細集計用SQL文字列
                string shoyoMeisaiSql = GetKyKyuShoMeisaiSql(2);                            // 賞与明細集計用SQL文字列

                string[] shain = new string[4];                                             // 社員情報
                decimal[] kingaku = new decimal[24];                                        // 金額情報
                string[] kubun = new string[29];                                            // 区分情報

                decimal[] gokei0 = new decimal[24];                                         // 年調不要者金額合計
                decimal[] gokei1 = new decimal[24];                                         // 年調対象者金額合計
                decimal[] gokei = new decimal[24];                                          // 金額合計
                int ninzu0 = 0;                                                             // 年調不要者人数
                int ninzu1 = 0;                                                             // 年調対象者人数

                foreach (DataRow drS in dtMainLoop.Rows)
                {
                    // 社員情報をセット
                    shain[0] = Util.ToString(drS["SHAIN_CD"]);
                    shain[1] = Util.ToString(drS["SHAIN_NM"]);
                    shain[2] = "";
                    shain[3] = (Util.ToInt(drS["NENMATSU_CHOSEI"]) == 0) ? "年調不要" : "　　　　";
                    // 配列値初期化
                    for (int i = 0; i < 24; i++) kingaku[i] = 0;
                    for (int i = 0; i < 29; i++) kubun[i] = "";

                    // TB年末調整データ取得
                    dpc = new DbParamCollection();
                    dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 2, this.UInfo.KaishaCd);
                    dpc.SetParam("@NENDO", SqlDbType.DateTime, nendo);
                    dpc.SetParam("@NENDO_TO", SqlDbType.DateTime, nendoTo);
                    dpc.SetParam("@SHAIN_CD", SqlDbType.Decimal, 6, 0, Util.ToDecimal(drS["SHAIN_CD"]));
                    sql = new StringBuilder();
                    sql.Append("SELECT * FROM TB_KY_NENMATSU_CHOSEI");
                    sql.Append(" WHERE KAISHA_CD = @KAISHA_CD");
                    sql.Append(" AND NENDO = @NENDO");
                    sql.Append(" AND SHAIN_CD = @SHAIN_CD");
                    DataTable dtNenmatsuChosei = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);

                    if (dtNenmatsuChosei.Rows.Count == 1)
                    {
                        DataRow drN = dtNenmatsuChosei.Rows[0];

                        #region 年調済みの場合　TB年末調整データを出力

                        // 年調レコードステータス
                        shain[3] = shain[3] + ((Util.ToInt(drN["KEISAN_F"]) == 1) ? "<年調済>" : "")
                                            + ((Util.ToInt(drN["CHOSEI_F"]) == 1) ? "<給与>" : 
                                               (Util.ToInt(drN["CHOSEI_F"]) == 2) ? "<賞与>" :
                                               (Util.ToInt(drN["CHOSEI_F"]) == 3) ? "<単独>" : "");
                        //年調済みかどうか　済じゃない場合給与データと賞与データをセット
                        if (Util.ToInt(drN["KEISAN_F"]) == 1)
                        {
                            // TB年末調整データから金額情報をセット
                            kingaku[0] = Util.ToDecimal(drN["KYUYOTO_KAZEI_KINGAKU"])                           // 給与等金額
                                        + Util.ToDecimal(drN["ZENSHOKUBUN_KAZEI_KYUYOGAKU"])
                                        + Util.ToDecimal(drN["CHOSEI_KYUYO_KAZEI_KYUYOGAKU"]);
                            kingaku[1] = Util.ToDecimal(drN["SHOYOTO_KAZEI_KINGAKU"])                           // 賞与等金額
                                        + Util.ToDecimal(drN["CHOSEI_SHOYO_KAZEI_KYUYOGAKU"]);
                            kingaku[2] = kingaku[0] + kingaku[1];                                               // 給与・賞与合計
                            kingaku[3] = Util.ToDecimal(drN["KYUYO_SHOTOKU_KOJOGO_NO_KNGK"]);                   // 給与所得控除後
                            // kingaku[4]                                                                       // 空欄
                            kingaku[5] = Util.ToDecimal(drN["CHOSEI_KYUYO_KAZEI_KYUYOGAKU"])                    // 調整課税給与額
                                        + Util.ToDecimal(drN["CHOSEI_SHOYO_KAZEI_KYUYOGAKU"])
                                        + Util.ToDecimal(drN["ZENSHOKUBUN_KAZEI_KYUYOGAKU"]);
                            kingaku[6] = Util.ToDecimal(drN["CHOSEI_KYUYO_SHAKAI_HOKENRYO"])                    // 調整社会保険料
                                        + Util.ToDecimal(drN["CHOSEI_SHOYO_SHAKAI_HOKENRYO"])
                                        + Util.ToDecimal(drN["ZENSHOKUBUN_SHAKAI_HOKENRYO"]);
                            kingaku[7] = Util.ToDecimal(drN["CHOSEI_KYUYO_SHOTOKUZEIGAKU"])                     // 調整所得税
                                        + Util.ToDecimal(drN["CHOSEI_SHOYO_SHOTOKUZEIGAKU"])
                                        + Util.ToDecimal(drN["ZENSHOKUBUN_SHOTOKUZEIGAKU"]);
                            kingaku[8] = Util.ToDecimal(drN["SHAKAI_HOKENRYO_KJGK_KYUYOTO"])                    // 社会保険(給与)
                                        + kingaku[6];
                            kingaku[9] = Util.ToDecimal(drN["SHAKAI_HOKENRYO_KJGK_SNKKBN"]);                    // 社会保険(申告)
                            kingaku[10] = Util.ToDecimal(drN["SKB_KIGYO_KYOSAI_KAKEKIN_KJGK"]);                 // 小規模企業共済
                            kingaku[11] = Util.ToDecimal(drN["SEIMEIHOKENRYO_KOJOGAKU"]);                       // 生命保険料
                            kingaku[12] = Util.ToDecimal(drN["SONGAIHOKENRYO_KOJOGAKU"]);                       // 地震保険料
                            kingaku[13] = Util.ToDecimal(drN["HAIGUSHA_TOKUBETSU_KOJOGAKU"]);                   // 配偶者特別
                            kingaku[14] = Util.ToDecimal(drN["HAIGUSHA_KOJOGAKU"])                              // 配偶・扶養等
                                        + Util.ToDecimal(drN["FUYO_KOJOGAKU"])
                                        + Util.ToDecimal(drN["KISO_KOJOGAKU"])
                                        + Util.ToDecimal(drN["SHOGAISHA_KOJOGAKU"])
                                        + Util.ToDecimal(drN["RONENSHA_KAFU_KOJOGAKU"])
                                        + Util.ToDecimal(drN["KINRO_GAKUSEI_KOJOGAKU"]);
                            kingaku[15] = kingaku[8] + kingaku[9] + kingaku[10]                                 // 所得控除合計
                                            + kingaku[11] + kingaku[12] + kingaku[13]
                                            + kingaku[14];
                            kingaku[16] = Util.ToDecimal(drN["KAZEI_KYUYO_SHOTOKU_KINGAKU"]);                   // 課税所得金額
                            kingaku[17] = Util.ToDecimal(drN["SANSHUTSU_NENZEIGAKU"]);                          // 年税額
                            kingaku[18] = Util.ToDecimal(drN["JUTAKU_SHUTOKUTO_TKBT_KOJOGAKU"]);                // 住宅借入金等特別
                            kingaku[19] = Util.ToDecimal(drN["NENCHO_NENZEIGAKU"]);                             // 差引年税額
                            // kingaku[20]                                                                      // 空欄
                            kingaku[21] = Util.ToDecimal(drN["NENZEIGAKU"]);                                    // 徴収年税額
                            kingaku[22] = Util.ToDecimal(drN["KYUYOTO_SHOTOKUZEIGAKU"])                         // 徴収税額
                                        + Util.ToDecimal(drN["SHOYOTO_SHOTOKUZEIGAKU"])
                                        + Util.ToDecimal(drN["ZENSHOKUBUN_SHOTOKUZEIGAKU"])
                                        + Util.ToDecimal(drN["CHOSEI_KYUYO_SHOTOKUZEIGAKU"])
                                        + Util.ToDecimal(drN["CHOSEI_SHOYO_SHOTOKUZEIGAKU"]);
                            kingaku[23] = kingaku[21] - kingaku[22];                                            // 差引過不足額
                        }
                        else
                        {
                            decimal kyuyoKazeiKingaku = 0;
                            decimal kyuyoShaHo = 0;
                            decimal kyuyoShoToku = 0;
                            decimal shoyoKazeiKingaku = 0;
                            decimal shoyoShaHo = 0;
                            decimal shoyoShoToku = 0;


                            #region 給与データ取得
                            sql1 = new StringBuilder();
                            sql1.Append("SELECT ");
                            sql1.Append(" K2.SHAIN_CD AS SHAIN_CD ,");
                            sql1.Append(" SUM(ISNULL(K2.KOMOKU1,0)");
                            sql1.Append("   + ISNULL(K2.KOMOKU2,0)");
                            sql1.Append("   + ISNULL(K2.KOMOKU4,0)");
                            sql1.Append("   + ISNULL(K2.KOMOKU5,0)");
                            sql1.Append("   + ISNULL(K2.KOMOKU8,0)");
                            sql1.Append("   + ISNULL(K2.KOMOKU9,0)");
                            sql1.Append("   + ISNULL(K2.KOMOKU10,0)");
                            sql1.Append("   + ISNULL(K2.KOMOKU20,0))");
                            sql1.Append(" AS KAZEI_KINGAKU,");
                            sql1.Append(" SUM(ISNULL(K3.KOMOKU1,0)");
                            sql1.Append("   + ISNULL(K3.KOMOKU2,0)");
                            sql1.Append("   + ISNULL(K3.KOMOKU3,0))");
                            sql1.Append(" AS SHAKAI_HOKENRYO,");
                            sql1.Append(" SUM(ISNULL(K3.KOMOKU4,0)");
                            sql1.Append("   + ISNULL(K3.KOMOKU20,0))");
                            sql1.Append(" AS SHOTOKU_ZEIGAKU");
                            sql1.Append(" FROM TB_KY_KYUYO_MEISAI_SHIKYU AS K2 ");
                            sql1.Append(" LEFT OUTER JOIN  TB_KY_KYUYO_MEISAI_KOJO AS K3 ");
                            sql1.Append(" ON (K2.KAISHA_CD = K3.KAISHA_CD) AND");
                            sql1.Append(" (K2.SHAIN_CD = K3.SHAIN_CD) AND");
                            sql1.Append(" (K2.NENGETSU = K3.NENGETSU)");
                            sql1.Append(" WHERE K2.KAISHA_CD = @KAISHA_CD");
                            sql1.Append(" AND K2.SHAIN_CD = @SHAIN_CD");
                            sql1.Append(" AND K2.NENGETSU BETWEEN @NENDO AND @NENDO_TO ");
                            sql1.Append(" GROUP BY ");
                            sql1.Append(" K2.SHAIN_CD");
                            DataTable dtkyuyodata = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql1), dpc);
                            #endregion

                            //データセット
                            if (dtkyuyodata.Rows.Count > 0)
                            {
                                kyuyoKazeiKingaku = Util.ToDecimal(dtkyuyodata.Rows[0]["KAZEI_KINGAKU"]);
                                kyuyoShaHo = Util.ToDecimal(dtkyuyodata.Rows[0]["SHAKAI_HOKENRYO"]);
                                kyuyoShoToku = Util.ToDecimal(dtkyuyodata.Rows[0]["SHOTOKU_ZEIGAKU"]);
                            }

                            #region 賞与データ取得
                            sql2 = new StringBuilder();
                            sql2.Append("SELECT ");
                            sql2.Append(" K2.SHAIN_CD AS SHAIN_CD ,");
                            sql2.Append(" SUM(ISNULL(K2.KOMOKU1,0))");
                            sql2.Append(" AS KAZEI_KINGAKU,");
                            sql2.Append(" SUM(ISNULL(K3.KOMOKU1,0)");
                            sql2.Append("   + ISNULL(K3.KOMOKU2,0)");
                            sql2.Append("   + ISNULL(K3.KOMOKU3,0))");
                            sql2.Append(" AS SHAKAI_HOKENRYO,");
                            sql2.Append(" SUM(ISNULL(K3.KOMOKU4,0)");
                            sql2.Append("   + ISNULL(K3.KOMOKU20,0))");
                            sql2.Append(" AS SHOTOKU_ZEIGAKU");
                            sql2.Append(" FROM TB_KY_SHOYO_MEISAI_SHIKYU AS K2 ");
                            sql2.Append(" LEFT OUTER JOIN  TB_KY_SHOYO_MEISAI_KOJO AS K3 ");
                            sql2.Append(" ON (K2.KAISHA_CD = K3.KAISHA_CD) AND");
                            sql2.Append(" (K2.SHAIN_CD = K3.SHAIN_CD) AND");
                            sql2.Append(" (K2.NENGETSU = K3.NENGETSU)");
                            sql2.Append(" WHERE K2.KAISHA_CD = @KAISHA_CD");
                            sql2.Append(" AND K2.SHAIN_CD = @SHAIN_CD");
                            sql2.Append(" AND K2.NENGETSU BETWEEN @NENDO AND @NENDO_TO");
                            sql2.Append(" GROUP BY ");
                            sql2.Append(" K2.SHAIN_CD");
                            DataTable dtshoyodata = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql2), dpc);
                            #endregion

                            //データセット
                            if (dtshoyodata.Rows.Count > 0)
                            {
                                shoyoKazeiKingaku = Util.ToDecimal(dtshoyodata.Rows[0]["KAZEI_KINGAKU"]);
                                shoyoShaHo = Util.ToDecimal(dtshoyodata.Rows[0]["SHAKAI_HOKENRYO"]);
                                shoyoShoToku = Util.ToDecimal(dtshoyodata.Rows[0]["SHOTOKU_ZEIGAKU"]);
                            }
                            // TB年末調整データから金額情報をセット
                            kingaku[0] = kyuyoKazeiKingaku;                                                     // 給与等金額
                            kingaku[1] = shoyoKazeiKingaku;                                                     // 賞与等金額
                            kingaku[2] = kingaku[0] + kingaku[1];                                               // 給与・賞与合計
                            kingaku[3] = 0;                                                                     // 給与所得控除後
                            // kingaku[4]                                                                       // 空欄
                            kingaku[5] = 0;                                                                     // 調整課税給与額
                            kingaku[6] = 0;                                                                     // 調整社会保険料
                            kingaku[7] = 0;                                                                     // 調整所得税
                            kingaku[8] = 0;                                                                     // 社会保険(給与)
                            kingaku[9] = 0;                                                                     // 社会保険(申告)
                            kingaku[10] = 0;                                                                    // 小規模企業共済
                            kingaku[11] = 0;                                                                    // 生命保険料
                            kingaku[12] = 0;                                                                    // 地震保険料
                            kingaku[13] = 0;                                                                    // 配偶者特別
                            kingaku[14] = 0;                                                                    // 配偶・扶養等
                            kingaku[15] = 0;                                                                    // 所得控除合計
                            kingaku[16] = 0;                                                                    // 課税所得金額
                            kingaku[17] = 0;                                                                    // 年税額
                            kingaku[18] = 0;                                                                    // 住宅借入金等特別
                            kingaku[19] = 0;                                                                    // 差引年税額
                            // kingaku[20]                                                                      // 空欄
                            kingaku[21] = 0;                                                                    // 徴収年税額
                            kingaku[22] = kyuyoShaHo + kyuyoShoToku + shoyoShaHo + shoyoShoToku;                // 徴収税額
                            kingaku[23] = 0;                                                                    // 差引過不足税額
                        }

                        // TB年末調整から区分情報をセット
                        kubun[0] = (Util.ToInt(drN["ZEIGAKU_HYO"]) == 1) ? "甲" :                            // 税額表
                                    (Util.ToInt(drN["ZEIGAKU_HYO"]) == 2) ? "乙" : "";
                        kubun[1] = (Util.ToInt(drN["MISEINENSHA"]) == 1) ? "*" : "";                        // 本人未成年者
                        kubun[2] = (Util.ToInt(drN["HONNIN_KUBUN2"]) == 2) ? "*" : "";                      // 本人障害者特別
                        kubun[3] = (Util.ToInt(drN["HONNIN_KUBUN2"]) == 1) ? "*" : "";                      // 本人障害者その他
                        kubun[4] = (Util.ToInt(drN["HONNIN_KUBUN1"]) == 2) ? "*" : "";                      // 本人寡婦一般
                        kubun[5] = (Util.ToInt(drN["HONNIN_KUBUN1"]) == 3) ? "*" : "";                      // 本人寡婦特別
                        kubun[6] = (Util.ToInt(drN["HONNIN_KUBUN1"]) == 4) ? "*" : "";                      // 本人寡夫
                        kubun[7] = (Util.ToInt(drN["HONNIN_KUBUN3"]) == 1) ? "*" : "";                      // 本人勤労学生
                        kubun[8] = (Util.ToInt(drN["SHIBO_TAISHOKU"]) == 1) ? "*" : "";                     // 本人死亡退職
                        kubun[9] = (Util.ToInt(drN["SAIGAISHA"]) == 1) ? "*" : "";                          // 本人災害者
                        kubun[10] = (Util.ToInt(drN["GAIKOKUJIN"]) == 1) ? "*" : "";                        // 本人外国人
                        kubun[11] = (Util.ToInt(drN["HAIGUSHA_KUBUN"]) == 0) ? "無" :                        // 配偶者
                                    (Util.ToInt(drN["HAIGUSHA_KUBUN"]) == 1) ? "有" :
                                    (Util.ToInt(drN["HAIGUSHA_KUBUN"]) == 2) ? "老人" : "";

                        kubun[12] = this.FormatNum(Util.ToInt(drN["FUYO_IPPAN"]) - Util.ToInt(drN["FUYO_TOKUTEI"]) - 
                                                   Util.ToInt(drN["FUYO_ROZIN_DOKYO_ROSHINTO"]) - Util.ToInt(drN["FUYO_ROZIN_DOKYO_ROSHINTO_IGAI"]));       // 扶養・一般(一般 - 特定　- (老人・同居) - (老人・以外))
                        kubun[13] = this.FormatNum(Util.ToInt(drN["SAI16_MIMAN"]));                          // 扶養・16歳未満
                        kubun[14] = this.FormatNum(Util.ToInt(drN["FUYO_TOKUTEI"]));                         // 扶養・特定
                        kubun[15] = this.FormatNum(Util.ToInt(drN["FUYO_ROZIN_DOKYO_ROSHINTO"]));            // 扶養・老人・同居
                        kubun[16] = this.FormatNum(Util.ToInt(drN["FUYO_ROZIN_DOKYO_ROSHINTO_IGAI"]));       // 扶養・老人・以外
                        kubun[17] = this.FormatNum(Util.ToInt(drN["FUYO_DOKYO_TOKUSHO_IPPAN"]));             // 同居特別障害・一般
                        kubun[18] = this.FormatNum(Util.ToInt(drN["SHOGAISHA_IPPAN"]));                      // 障害・一般
                        kubun[19] = this.FormatNum(Util.ToInt(drN["SHOGAISHA_TOKUBETSU"]));                  // 障害・特別
                        // kubun[18-23] BLANK
                        kubun[24] = this.FormatNum(Util.ToDecimal(drN["HAIGUSHA_GOKEI_SHOTOKU"]));           // 配偶者合計所得
                        kubun[25] = this.FormatNum(Util.ToDecimal(drN["KOJIN_NENKIN_HOKENRYO_SHRIGK"]));     // 個人年金保険料
                        kubun[26] = this.FormatNum(Util.ToDecimal(drN["CHOKI_SONGAI_HOKENRYO_SHRIGK"]));     // 旧長期災害保険料
                        kubun[27] = Util.ToString(drN["TEKIYO1"]);                                           // 摘要１
                        kubun[28] = Util.ToString(drN["TEKIYO2"]);                                           // 摘要２

                        // ワークレコード追加
                        alParams = SetPrKyTblParams(jpNendo, shain, kingaku, kubun, outputDate);
                        this.Dba.Insert("PR_KY_TBL", (DbParamCollection)alParams[0]);

                        // 小計変数へ加算
                        if (Util.ToInt(drS["NENMATSU_CHOSEI"]) == 1)
                        {
                            ninzu1++;
                            for (int i = 0; i < 24; i++) gokei1[i] += kingaku[i];
                        }
                        else
                        {
                            ninzu0++;
                            for (int i = 0; i < 23; i++) gokei0[i] += kingaku[i];
                        }

                        #endregion
                    }
                    else
                    {
                        #region 年調未済の場合　TB給与/賞与明細データを出力(支給実績がある場合)

                        // 給与賞与明細データ抽出パラメータ
                        dpc = new DbParamCollection();
                        dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 2, this.UInfo.KaishaCd);
                        dpc.SetParam("@SHAIN_CD", SqlDbType.Decimal, 6, 0, Util.ToDecimal(drS["SHAIN_CD"]));
                        dpc.SetParam("@NENGETSU_FR", SqlDbType.DateTime, nendo);
                        dpc.SetParam("@NENGETSU_TO", SqlDbType.DateTime, nendo.AddMonths(11));

                        // 給与明細集計
                        dt = this.Dba.GetDataTableFromSqlWithParams(kyuyoMeisaiSql, dpc);
                        if (dt.Rows.Count > 0)
                        {
                            kingaku[0] = Util.ToDecimal(dt.Rows[0]["KAZEIGAKU"]);                               // 給与等金額
                            kingaku[8] = Util.ToDecimal(dt.Rows[0]["SHAKAIHOKENRYO"]);                          // 社会保険(給与)
                            kingaku[22] = Util.ToDecimal(dt.Rows[0]["SHOTOKUZEI"]);                             // 徴収税額
                        }
                        // 賞与明細集計
                        dt = this.Dba.GetDataTableFromSqlWithParams(shoyoMeisaiSql, dpc);
                        if (dt.Rows.Count > 0)
                        {
                            kingaku[1] = Util.ToDecimal(dt.Rows[0]["KAZEIGAKU"]);                               // 給与等金額
                            kingaku[8] += Util.ToDecimal(dt.Rows[0]["SHAKAIHOKENRYO"]);                         // 社会保険(給与)
                            kingaku[22] += Util.ToDecimal(dt.Rows[0]["SHOTOKUZEI"]);                            // 徴収税額
                        }


                        // 合計値
                        kingaku[2] = kingaku[0] + kingaku[1];                                                   // 給与・賞与合計
                        kingaku[15] = kingaku[8];                                                               // 所得控除合計

                        // 支給実績がある場合、ワークへ出力
                        if (kingaku[2] != 0 || kingaku[8] != 0 || kingaku[22] != 0)
                        {
                            // TB社員情報から区分情報を取得
                            kubun[0] = (Util.ToInt(drS["ZEIGAKU_HYO"]) == 1) ? "甲" :                             // 税額表
                                        (Util.ToInt(drS["ZEIGAKU_HYO"]) == 2) ? "乙" : "";
                            kubun[1] = (Util.ToInt(drS["MISEINENSHA"]) == 1) ? "*" : "";                         // 本人未成年者
                            kubun[2] = (Util.ToInt(drS["HONNIN_KUBUN2"]) == 2) ? "*" : "";                       // 本人障害者特別
                            kubun[3] = (Util.ToInt(drS["HONNIN_KUBUN2"]) == 1) ? "*" : "";                       // 本人障害者その他
                            kubun[4] = (Util.ToInt(drS["HONNIN_KUBUN1"]) == 2) ? "*" : "";                       // 本人寡婦一般
                            kubun[5] = (Util.ToInt(drS["HONNIN_KUBUN1"]) == 3) ? "*" : "";                       // 本人寡婦特別
                            kubun[6] = (Util.ToInt(drS["HONNIN_KUBUN1"]) == 4) ? "*" : "";                       // 本人寡夫
                            kubun[7] = (Util.ToInt(drS["HONNIN_KUBUN3"]) == 1) ? "*" : "";                       // 本人勤労学生
                            kubun[8] = (Util.ToInt(drS["SHIBO_TAISHOKU"]) == 1) ? "*" : "";                      // 本人死亡退職
                            kubun[9] = (Util.ToInt(drS["SAIGAISHA"]) == 1) ? "*" : "";                           // 本人災害者
                            kubun[10] = (Util.ToInt(drS["GAIKOKUJIN"]) == 1) ? "*" : "";                         // 本人外国人
                            kubun[11] = (Util.ToInt(drS["HAIGUSHA_KUBUN"]) == 0) ? "無" :                         // 配偶者
                                        (Util.ToInt(drS["HAIGUSHA_KUBUN"]) == 1) ? "有" : 
                                        (Util.ToInt(drS["HAIGUSHA_KUBUN"]) == 2) ? "老人" : "";

                            kubun[12] = this.FormatNum(Util.ToInt(drS["FUYO_IPPAN"]) - Util.ToInt(drS["FUYO_TOKUTEI"]) - 
                                                       Util.ToInt(drS["FUYO_ROJIN_DOKYO_ROSHINTO"]) - Util.ToInt(drS["FUYO_ROJIN_DOKYO_ROSHINTO_IGAI"]));       // 扶養・一般(一般 - 特定 - (老人・同居) - (老人・以外))
                            kubun[13] = this.FormatNum(Util.ToInt(drS["SAI16_MIMAN"]));                          // 16歳未満
                            kubun[14] = this.FormatNum(Util.ToInt(drS["FUYO_TOKUTEI"]));                         // 扶養・特定
                            kubun[15] = this.FormatNum(Util.ToInt(drS["FUYO_ROJIN_DOKYO_ROSHINTO"]));            // 扶養・老人・同居
                            kubun[16] = this.FormatNum(Util.ToInt(drS["FUYO_ROJIN_DOKYO_ROSHINTO_IGAI"]));       // 扶養・老人・以外
                            kubun[17] = this.FormatNum(Util.ToInt(drS["FUYO_DOKYO_TOKUSHO_IPPAN"]));             // 同居特別障害・一般
                            kubun[18] = this.FormatNum(Util.ToInt(drS["SHOGAISHA_IPPAN"]));                      // 障害・一般
                            kubun[19] = this.FormatNum(Util.ToInt(drS["SHOGAISHA_TOKUBETSU"]));                  // 障害・特別
                            
                            // kubun[20-23] BLANK
                            // kubun[24-26] 年末調整データ
                            kubun[27] = Util.ToString(drS["TEKIYO1"]);                                          // 摘要１
                            kubun[28] = Util.ToString(drS["TEKIYO2"]);                                          // 摘要２

                            // ワークレコード追加
                            alParams = SetPrKyTblParams(jpNendo, shain, kingaku, kubun, outputDate);
                            this.Dba.Insert("PR_KY_TBL", (DbParamCollection)alParams[0]);

                            // 小計変数へ加算
                            if (Util.ToInt(drS["NENMATSU_CHOSEI"]) == 1)
                            {
                                ninzu1++;
                                for (int i = 0; i < 24; i++) gokei1[i] += kingaku[i];      
                            }
                            else
                            {
                                ninzu0++;
                                for (int i = 0; i < 23; i++) gokei0[i] += kingaku[i];
                            }
                         }

                        #endregion
                    }
                }

                // 区分情報配列初期化
                for (int i = 0; i < 29; i++) kubun[i] = "";

                // 年調対象者小計
                if (ninzu1 > 0)
                {
                    // 社員情報変数へ見出しをセット
                    shain[0] = "";
                    shain[1] = "【年調対象者】";
                    shain[2] = Util.ToString(ninzu1) + "人";
                    shain[3] = "";
                    // 小計行レコード追加
                    alParams = SetPrKyTblParams(jpNendo, shain, gokei1, kubun, outputDate);
                    this.Dba.Insert("PR_KY_TBL", (DbParamCollection)alParams[0]);
                }
                // 年調不要者小計
                if (ninzu0 > 0)
                {
                    // 社員情報変数へ見出しをセット
                    shain[0] = "";
                    shain[1] = "【年調不要者】";
                    shain[2] = Util.ToString(ninzu0) + "人";
                    shain[3] = "";
                    // 小計行レコード追加
                    alParams = SetPrKyTblParams(jpNendo, shain, gokei0, kubun, outputDate);
                    this.Dba.Insert("PR_KY_TBL", (DbParamCollection)alParams[0]);
                }
                // 合計
                if (ninzu0 + ninzu1 > 0)
                {
                    for (int i = 0; i < 24; i++) gokei[i] = gokei0[i] + gokei1[i];

                    // 社員情報変数へ見出しをセット
                    shain[0] = "";
                    shain[1] = "【　合　計　】";
                    shain[2] = Util.ToString(ninzu0 + ninzu1) + "人";
                    shain[3] = "";
                    alParams = SetPrKyTblParams(jpNendo, shain, gokei, kubun, outputDate);
                    this.Dba.Insert("PR_KY_TBL", (DbParamCollection)alParams[0]);
                }
            }

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_HN_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_KY_TBL",
                "GUID = @GUID",
                dpc);
            bool dataFlag;
            if (tmpdtPR_HN_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                dataFlag = false;
            }

            return dataFlag;
        }

        /// <summary>
        /// 給与/賞与明細データ照会SQLの取得
        /// </summary>
        /// <param name="type">1給与 2賞与</param>
        /// <returns></returns>
        private string GetKyKyuShoMeisaiSql(int type)
        {
            string kyuSho = (type == 1) ? "KYUYO" : "SHOYO";

            DataTable dtSti = (type == 1) ? _dtKyuSti : _dtShoSti;

            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT K2.SHAIN_CD AS SHAIN_CD");

            DataRow[] foundRows;
            string filter;
            string order;

            // 所得税課税額
            filter = "KOMOKU_SHUBETSU = 2 AND KOMOKU_KUBUN1 = 1";
            order = "KOMOKU_BANGO";
            foundRows = dtSti.Select(filter, order);
            sql.Append(", SUM(0");
            foreach (DataRow dr in foundRows)
            {
                sql.Append(" + ISNULL(K2.KOMOKU" + Util.ToString(dr["KOMOKU_BANGO"]) + " , 0)");
            }
            sql.Append(") AS KAZEIGAKU");
            // 社会保険料等
            filter = "KOMOKU_SHUBETSU = 3 AND KOMOKU_KUBUN3 >= 31 AND KOMOKU_KUBUN3 <= 33";
            order = "KOMOKU_BANGO";
            foundRows = dtSti.Select(filter, order);
            sql.Append(", SUM(0");
            foreach (DataRow dr in foundRows)
            {
                sql.Append(" + ISNULL(K3.KOMOKU" + Util.ToString(dr["KOMOKU_BANGO"]) + " , 0)");
            }
            sql.Append(") AS SHAKAIHOKENRYO");
            // 所得税
            filter = "KOMOKU_SHUBETSU = 3 AND (KOMOKU_KUBUN3 = 34 OR KOMOKU_KUBUN3 = 36)";               // 調整所得税を含む
            order = "KOMOKU_BANGO";
            foundRows = dtSti.Select(filter, order);
            sql.Append(", SUM(0");
            foreach (DataRow dr in foundRows)
            {
                sql.Append(" + ISNULL(K3.KOMOKU" + Util.ToString(dr["KOMOKU_BANGO"]) + " , 0)");
            }
            sql.Append(") AS SHOTOKUZEI");

            sql.Append(" FROM");
            sql.Append(" TB_KY_" + kyuSho + "_MEISAI_SHIKYU AS K2");
            sql.Append(" LEFT OUTER JOIN TB_KY_" + kyuSho + "_MEISAI_KOJO AS K3");
            sql.Append(" ON (K2.KAISHA_CD = K3.KAISHA_CD)");
            sql.Append(" AND (K2.SHAIN_CD = K3.SHAIN_CD)");
            sql.Append(" AND (K2.NENGETSU = K3.NENGETSU)");
            sql.Append(" WHERE K2.KAISHA_CD = @KAISHA_CD");
            sql.Append(" AND K2.SHAIN_CD = @SHAIN_CD");
            sql.Append(" AND K2.NENGETSU BETWEEN @NENGETSU_FR AND @NENGETSU_TO");
            sql.Append(" GROUP BY K2.SHAIN_CD");

            return Util.ToString(sql);
        }

        /// <summary>
        /// PR_KY_TBLに更新するためのパラメータ設定をします。
        /// </summary>
        /// <param name="jpNendo">和暦年度</param>
        /// <param name="shain">社員情報</param>
        /// <param name="kingaku">金額情報</param>
        /// <param name="kubun">区分情報</param>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// </returns>
        private ArrayList SetPrKyTblParams(string[] jpNendo, string[] shain, decimal[] kingaku, string[] kubun, string outDate)
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            ++_sort;

            // 制御情報
            updParam.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            updParam.SetParam("@SORT", SqlDbType.Int, _sort);

            // 基本情報
            updParam.SetParam("@ITEM001", SqlDbType.VarChar, 200, this.UInfo.KaishaNm);
            updParam.SetParam("@ITEM002", SqlDbType.VarChar, 200, string.Format("{0}{2}年度", jpNendo));
            updParam.SetParam("@ITEM003", SqlDbType.VarChar, 200, outDate);
            // 社員情報
            updParam.SetParam("@ITEM011", SqlDbType.VarChar, 200, shain[0]);
            updParam.SetParam("@ITEM012", SqlDbType.VarChar, 200, shain[1]);
            updParam.SetParam("@ITEM013", SqlDbType.VarChar, 200, shain[2]);
            updParam.SetParam("@ITEM014", SqlDbType.VarChar, 200, shain[3]);

            // 金額情報
            for (int i = 0; i < 24; i++)
            {
                if (kingaku[i] == 0)
                {
                    updParam.SetParam("@ITEM0" + Util.ToString(i + 21), SqlDbType.VarChar, 200, "0");
                }
                else
                {
                    updParam.SetParam("@ITEM0" + Util.ToString(i + 21), SqlDbType.VarChar, 200, this.FormatNum(kingaku[i]));
                }
            }

            // 区分情報
            for (int i = 0; i < 29; i++)
            {
                updParam.SetParam("@ITEM0" + Util.ToString(i + 51), SqlDbType.VarChar, 200, kubun[i]);
            }

            alParams.Add(updParam);

            return alParams;
        }

        /// <summary>
        /// 数値フォーマット(０値を表示しない)
        /// </summary>
        private string FormatNum(object num)
        {
            string ret = "";
            if (num != null) ret = Util.FormatNum(num);
            if (ret == "0") ret = "";
            return ret;
        }

        #endregion
    }
}