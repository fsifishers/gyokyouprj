﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using jp.co.fsi.common.report;
using jp.co.fsi.common.util;

namespace jp.co.fsi.ky.kynr1011
{
    /// <summary>
    /// KYNR1011R の帳票
    /// </summary>
    public partial class KYNR1011R : BaseReport
    {
        public KYNR1011R(DataTable tgtData)    : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }

        private void detail_Format(object sender, EventArgs e)
        {
            if(Util.ToDecimal(this.textBox1.Value) > 0)
            {
                this.lbl1.Visible = true;
                this.lbl2.Visible = true;
                this.lbl3.Visible = true;
                this.lbl4.Visible = true;
                this.label54.Visible = true;
                this.label55.Visible = true;
                this.label59.Visible = true;
                this.label60.Visible = true;

                if (Util.ToString(this.txtkubun1.Value) == "")
                {
                    this.txtkubun1.Value = 0;
                }
                if (Util.ToString(this.txtkubun2.Value) == "")
                {
                    this.txtkubun2.Value = 0;
                }
                if (Util.ToString(this.txtkubun3.Value) == "")
                {
                    this.txtkubun3.Value = 0;
                }
                if (Util.ToString(this.txtkubun4.Value) == "")
                {
                    this.txtkubun4.Value = 0;
                }
                if (Util.ToString(this.txtkubun5.Value) == "")
                {
                    this.txtkubun5.Value = 0;
                }
                if (Util.ToString(this.txtkubun6.Value) == "")
                {
                    this.txtkubun6.Value = 0;
                }
                if (Util.ToString(this.txtkubun7.Value) == "")
                {
                    this.txtkubun7.Value = 0;
                }
                if (Util.ToString(this.txtkubun8.Value) == "")
                {
                    this.txtkubun8.Value = 0;
                }
                if (Util.ToString(this.textBox31.Value) == "")
                {
                    this.textBox31.Value = 0;
                }
                if (Util.ToString(this.textBox50.Value) == "")
                {
                    this.textBox50.Value = 0;
                }
                if (Util.ToString(this.textBox52.Value) == "")
                {
                    this.textBox52.Value = 0;
                }
            }
            else
            {
                this.lbl1.Visible = false;
                this.lbl2.Visible = false;
                this.lbl3.Visible = false;
                this.lbl4.Visible = false;
                this.label54.Visible = false;
                this.label55.Visible = false;
                this.label59.Visible = false;
                this.label60.Visible = false;
            }

        }

        private void pageHeader_Format(object sender, EventArgs e)
        {
            ////和暦でDataTimeを文字列に変換する
            //System.Globalization.CultureInfo ci =
            //    new System.Globalization.CultureInfo("ja-JP", false);
            //ci.DateTimeFormat.Calendar = new System.Globalization.JapaneseCalendar();

            //this.txtToday.Text = DateTime.Now.ToString("gy年 M月 d日", ci);
        }
    }
}
