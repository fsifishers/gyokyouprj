﻿using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.ky.kycm1081
{
    /// <summary>
    /// 市町村の登録(KYCM1082)
    /// </summary>
    public partial class KYCM1082 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(新規)
        /// </summary>
        private const string MODE_NEW = "1";

        /// <summary>
        /// モード(編集)
        /// </summary>
        private const string MODE_EDIT = "2";
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KYCM1082()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="par1">引数1</param>
        public KYCM1082(string par1)
            : base(par1)
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // タイトルは非表示
            this.lblTitle.Visible = false;

            // 引数：Par1／モード(1:新規、2:変更)、InData：市町村コード
            if (MODE_NEW.Equals(this.Par1))
            {
                // 新規モードの初期表示
                InitDispOnNew();
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 編集モードの初期表示
                InitDispOnEdit();
            }
            else
            {
                // 不正な起動として閉じる
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }
            this.ShowFButton = true;

            // EscapeとF1のみ表示
            this.ShowFButton = true;
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF1.Location = this.btnF2.Location;
            this.btnF6.Location = this.btnF4.Location;
            this.btnF2.Visible = false;
            this.btnF3.Visible = true;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF6.Visible = true;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F3キー押下時処理
        /// </summary>
        public override void PressF3()
        {
            // 新規モードの場合は処理させない
            if (this.Par1.Equals(MODE_NEW)) return;

            // 確認メッセージを表示
            string msg = "削除しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // 削除用パラメータ
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@SHICHOSON_CD", SqlDbType.Decimal, 6, Util.ToString(this.InData));

            try
            {
                // トランザクション開始
                this.Dba.BeginTransaction();

                if (MODE_EDIT.Equals(this.Par1))
                {
                    // データ削除
                    // 給与.市町村マスタ
                    this.Dba.Delete("TB_KY_SHICHOSON", "SHICHOSON_CD = @SHICHOSON_CD", dpc);
                }

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        
        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        public override void PressF6()
        {
            // 確認メッセージを表示
            string msg = (MODE_NEW.Equals(this.Par1) ? "登録" : "更新") + "しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // 入力値をバインドパラメータとしてセットする
            ArrayList alParamsKyShichoson = SetKyShichosonParams();

            try
            {
                // トランザクション開始
                this.Dba.BeginTransaction();

                if (MODE_NEW.Equals(this.Par1))
                {
                    // データ登録
                    // 給与.市町村マスタ
                    this.Dba.Insert("TB_KY_SHICHOSON", (DbParamCollection)alParamsKyShichoson[0]);
                }
                else if (MODE_EDIT.Equals(this.Par1))
                {
                    // データ更新
                    // 給与.市町村マスタ
                    this.Dba.Update("TB_KY_SHICHOSON",
                        (DbParamCollection)alParamsKyShichoson[1],
                        "SHICHOSON_CD = @SHICHOSON_CD",
                        (DbParamCollection)alParamsKyShichoson[0]);
                }

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 市町村コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShichosonCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShichosonCd())
            {
                e.Cancel = true;
                this.txtShichosonCd.SelectAll();
            }
        }

        /// <summary>
        /// 市町村名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShichosonNm_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShichosonNm())
            {
                e.Cancel = true;
                this.txtShichosonNm.SelectAll();
            }
        }

        /// <summary>
        /// カナ名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShichosonKanaNm_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShichosoKananNm())
            {
                e.Cancel = true;
                this.txtShichosonKanaNm.SelectAll();
            }
        }

        /// <summary>
        /// 加入者名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKanyushaNm_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidKanyushaNm())
            {
                e.Cancel = true;
                this.txtKanyushaNm.SelectAll();
            }
        }
        /// <summary>
        /// 口座番号の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKozaBango_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidKozaBango())
            {
                e.Cancel = true;
                this.txtKozaBango.SelectAll();
            }
        }
        /// <summary>
        /// 指定番号の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShiteiBango_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShiteiBango())
            {
                e.Cancel = true;
                this.txtShiteiBango.SelectAll();
            }
        }

        /// <summary>
        /// 画面の最後のコントールでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LastControl_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.btnF6.Enabled)
            {
                this.PressF6();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 新規モードの初期表示
        /// </summary>
        private void InitDispOnNew()
        {
            //// 初期値、入力制御を実装
            //// 市町村コードの初期値を取得
            //// 市町村の中でのMAX+1を初期表示する
            //DataTable dtMaxShichoson =
            //    this.Dba.GetDataTableFromSql("SELECT MAX(SHICHOSON_CD) AS MAX_CD FROM TB_KY_SHICHOSON");
            //if (dtMaxShichoson.Rows.Count > 0 && !ValChk.IsEmpty(Util.ToInt(dtMaxShichoson.Rows[0]["MAX_CD"])))
            //{
            //    this.txtShichosonCd.Text = Util.ToString(Util.ToInt(dtMaxShichoson.Rows[0]["MAX_CD"]) + 1);
            //}
            //else
            //{
            //    this.txtShichosonCd.Text="1";
            //}
            // 給与機能はマスタ追加時のコード初期値は０
            this.txtShichosonCd.Text = "0";

            // 追加・変更状態表示
            this.lblMODE.Text = "追加";

            // 市町村名に初期フォーカス
            this.ActiveControl = this.txtShichosonNm;
            this.txtShichosonNm.Focus();
        }

        /// <summary>
        /// 編集モードの初期表示
        /// </summary>
        private void InitDispOnEdit()
        {
            // 現在DBに登録されている値、入力制御を実装
            StringBuilder cols = new StringBuilder();
            cols.Append("SHICHOSON_CD");
            cols.Append(" ,SHICHOSON_NM");
            cols.Append(" ,SHICHOSON_KANA_NM");
            cols.Append(" ,KANYUSHA_NM");
            cols.Append(" ,KOZA_BANGO");
            cols.Append(" ,SHITEI_BANGO");

            StringBuilder from = new StringBuilder();
            from.Append("TB_KY_SHICHOSON");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@SHICHOSON_CD", SqlDbType.Decimal, 6, Util.ToString(this.InData));

            DataTable dtDispData =
                this.Dba.GetDataTableByConditionWithParams(
                    Util.ToString(cols), Util.ToString(from),
                    "SHICHOSON_CD = @SHICHOSON_CD",
                    dpc);

            if (dtDispData.Rows.Count == 0)
            {
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }

            // 取得した内容を表示
            DataRow drDispData = dtDispData.Rows[0];
            this.txtShichosonCd.Text = Util.ToString(drDispData["SHICHOSON_CD"]);
            this.txtShichosonNm.Text = Util.ToString(drDispData["SHICHOSON_NM"]);
            this.txtShichosonKanaNm.Text = Util.ToString(drDispData["SHICHOSON_KANA_NM"]);
            this.txtKanyushaNm.Text = Util.ToString(drDispData["KANYUSHA_NM"]);
            this.txtKozaBango.Text = Util.ToString(drDispData["KOZA_BANGO"]);
            this.txtShiteiBango.Text = Util.ToString(drDispData["SHITEI_BANGO"]);

            // 追加・変更状態表示
            this.lblMODE.Text = "変更";

            // 市町村コードは入力不可
            this.lblShiireCd.Enabled = false;
            this.txtShichosonCd.Enabled = false;
        }

        /// <summary>
        /// 市町村コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShichosonCd()
        {
            // 未入力とゼロはNG　数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtShichosonCd.Text)
                || ValChk.IsEmpty(this.txtShichosonCd.Text)
                || this.txtShichosonCd.Text == "0")
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 既に存在するコードを入力した場合はエラーとする
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@SHICHOSON_CD", SqlDbType.Decimal, 6, this.txtShichosonCd.Text);
            StringBuilder where = new StringBuilder("SHICHOSON_CD = @SHICHOSON_CD");
            DataTable dtShichoson =
                this.Dba.GetDataTableByConditionWithParams("SHICHOSON_CD",
                    "TB_KY_SHICHOSON", Util.ToString(where), dpc);
            if (dtShichoson.Rows.Count > 0)
            {
                Msg.Error("既に存在する市町村コードと重複しています。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 市町村名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShichosonNm()
        {
            // 30バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtShichosonNm.Text, this.txtShichosonNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// カナ名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShichosoKananNm()
        {
            // 30バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtShichosonKanaNm.Text, this.txtShichosonKanaNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 加入者名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidKanyushaNm()
        {
            // 30バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtKanyushaNm.Text, this.txtKanyushaNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 口座番号の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidKozaBango()
        {
            // 20バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtKozaBango.Text, this.txtKozaBango.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;

        }

        /// <summary>
        /// 指定番号の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShiteiBango()
        {
            // 20バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtShiteiBango.Text, this.txtShiteiBango.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;

        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            if (MODE_NEW.Equals(this.Par1))
            {
                // 市町村コードのチェック
                if (!IsValidShichosonCd())
                {
                    this.txtShichosonCd.Focus();
                    return false;
                }
            }

            // 市町村名のチェック
            if (!IsValidShichosonNm())
            {
                this.txtShichosonNm.Focus();
                return false;
            }

            // カナ名のチェック
            if (!IsValidShichosonNm())
            {
                this.txtShichosonNm.Focus();
                return false;
            }
            // 加入者名のチェック
            if (!IsValidKanyushaNm())
            {
                this.txtKanyushaNm.Focus();
                return false;
            }
            // 口座番号のチェック
            if (!IsValidKozaBango())
            {
                this.txtKozaBango.Focus();
                return false;
            }
            // 指定番号のチェック
            if (!IsValidShiteiBango())
            {
                this.txtShiteiBango.Focus();
                return false;
            }

            return true;
        }

        /// <summary>
        /// TB_KY_SHICHOSONに更新するためのパラメータ設定をします。
        /// </summary>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// 更新処理：DbParamCollection*2(Where句,Set句)
        /// </returns>
        private ArrayList SetKyShichosonParams()
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            if (MODE_NEW.Equals(this.Par1))
            {
                // 市町村コードを更新パラメータに設定
                updParam.SetParam("@SHICHOSON_CD", SqlDbType.Decimal, 6, this.txtShichosonCd.Text);
                // 登録日
                updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWDATE");
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 市町村コードをWhere句のパラメータに設定
                DbParamCollection whereParam = new DbParamCollection();
                whereParam.SetParam("@SHICHOSON_CD", SqlDbType.Decimal, 6, this.txtShichosonCd.Text);
                alParams.Add(whereParam);
            }

            // 市町村名
            updParam.SetParam("@SHICHOSON_NM", SqlDbType.VarChar, 30, this.txtShichosonNm.Text);
            // カナ名
            updParam.SetParam("@SHICHOSON_KANA_NM", SqlDbType.VarChar, 30, this.txtShichosonKanaNm.Text);
            // 加入者名
            updParam.SetParam("@KANYUSHA_NM", SqlDbType.VarChar, 30, this.txtKanyushaNm.Text);
            // 口座番号
            updParam.SetParam("@KOZA_BANGO", SqlDbType.VarChar, 20, this.txtKozaBango.Text);
            // 指定番号
            updParam.SetParam("@SHITEI_BANGO", SqlDbType.VarChar, 20, this.txtShiteiBango.Text);
            // 更新日
            updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWDATE");

            alParams.Add(updParam);

            return alParams;
        }
        #endregion

    }
}
