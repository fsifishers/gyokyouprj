﻿namespace jp.co.fsi.ky.kycm1081
{
    partial class KYCM1083
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblShichosonCdFr = new System.Windows.Forms.Label();
            this.lblCodeBet1 = new System.Windows.Forms.Label();
            this.txtShichosonCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtShichosonCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShichosonCdTo = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pnlDebug.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(536, 23);
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(5, 48);
            this.pnlDebug.Size = new System.Drawing.Size(553, 100);
            // 
            // lblShichosonCdFr
            // 
            this.lblShichosonCdFr.BackColor = System.Drawing.Color.Silver;
            this.lblShichosonCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShichosonCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShichosonCdFr.Location = new System.Drawing.Point(77, 35);
            this.lblShichosonCdFr.Name = "lblShichosonCdFr";
            this.lblShichosonCdFr.Size = new System.Drawing.Size(180, 19);
            this.lblShichosonCdFr.TabIndex = 920;
            this.lblShichosonCdFr.Text = "先  頭";
            this.lblShichosonCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCodeBet1
            // 
            this.lblCodeBet1.AutoSize = true;
            this.lblCodeBet1.Location = new System.Drawing.Point(263, 39);
            this.lblCodeBet1.Name = "lblCodeBet1";
            this.lblCodeBet1.Size = new System.Drawing.Size(20, 13);
            this.lblCodeBet1.TabIndex = 921;
            this.lblCodeBet1.Text = "～";
            // 
            // txtShichosonCdFr
            // 
            this.txtShichosonCdFr.AutoSizeFromLength = false;
            this.txtShichosonCdFr.DisplayLength = null;
            this.txtShichosonCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtShichosonCdFr.Location = new System.Drawing.Point(23, 35);
            this.txtShichosonCdFr.MaxLength = 4;
            this.txtShichosonCdFr.Name = "txtShichosonCdFr";
            this.txtShichosonCdFr.Size = new System.Drawing.Size(48, 20);
            this.txtShichosonCdFr.TabIndex = 917;
            this.txtShichosonCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShichosonCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtShichosonCdFr_Validating);
            // 
            // txtShichosonCdTo
            // 
            this.txtShichosonCdTo.AutoSizeFromLength = false;
            this.txtShichosonCdTo.DisplayLength = null;
            this.txtShichosonCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtShichosonCdTo.Location = new System.Drawing.Point(286, 35);
            this.txtShichosonCdTo.MaxLength = 4;
            this.txtShichosonCdTo.Name = "txtShichosonCdTo";
            this.txtShichosonCdTo.Size = new System.Drawing.Size(48, 20);
            this.txtShichosonCdTo.TabIndex = 918;
            this.txtShichosonCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShichosonCdTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LastControl_KeyDown);
            this.txtShichosonCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtShichosonCdTo_Validating);
            // 
            // lblShichosonCdTo
            // 
            this.lblShichosonCdTo.BackColor = System.Drawing.Color.Silver;
            this.lblShichosonCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShichosonCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShichosonCdTo.Location = new System.Drawing.Point(340, 35);
            this.lblShichosonCdTo.Name = "lblShichosonCdTo";
            this.lblShichosonCdTo.Size = new System.Drawing.Size(180, 19);
            this.lblShichosonCdTo.TabIndex = 919;
            this.lblShichosonCdTo.Text = "最　後";
            this.lblShichosonCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblShichosonCdFr);
            this.groupBox1.Controls.Add(this.lblCodeBet1);
            this.groupBox1.Controls.Add(this.txtShichosonCdFr);
            this.groupBox1.Controls.Add(this.txtShichosonCdTo);
            this.groupBox1.Controls.Add(this.lblShichosonCdTo);
            this.groupBox1.Font = new System.Drawing.Font("MS UI Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBox1.Location = new System.Drawing.Point(8, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(545, 78);
            this.groupBox1.TabIndex = 922;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "市町村コード範囲";
            // 
            // KYCM1083
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(561, 151);
            this.Controls.Add(this.groupBox1);
            this.Name = "KYCM1083";
            this.Text = "市町村の印刷";
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.pnlDebug.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblShichosonCdFr;
        private System.Windows.Forms.Label lblCodeBet1;
        private common.controls.FsiTextBox txtShichosonCdFr;
        private common.controls.FsiTextBox txtShichosonCdTo;
        private System.Windows.Forms.Label lblShichosonCdTo;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}