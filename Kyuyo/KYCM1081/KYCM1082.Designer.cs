﻿namespace jp.co.fsi.ky.kycm1081
{
    partial class KYCM1082
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtShichosonCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShiireCd = new System.Windows.Forms.Label();
            this.pnlMain = new jp.co.fsi.common.FsiPanel();
            this.txtShiteiBango = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblFaxBango = new System.Windows.Forms.Label();
            this.txtKozaBango = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblDenwaBango = new System.Windows.Forms.Label();
            this.txtKanyushaNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblJusho1 = new System.Windows.Forms.Label();
            this.txtShichosonKanaNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShiireKanaNm = new System.Windows.Forms.Label();
            this.txtShichosonNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShiireNm = new System.Windows.Forms.Label();
            this.lblMODE = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.pnlMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(405, 23);
            this.lblTitle.Text = "仕入先の登録";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(5, 118);
            this.pnlDebug.Size = new System.Drawing.Size(438, 100);
            // 
            // txtShichosonCd
            // 
            this.txtShichosonCd.AutoSizeFromLength = true;
            this.txtShichosonCd.DisplayLength = null;
            this.txtShichosonCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShichosonCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtShichosonCd.Location = new System.Drawing.Point(132, 13);
            this.txtShichosonCd.MaxLength = 4;
            this.txtShichosonCd.Name = "txtShichosonCd";
            this.txtShichosonCd.Size = new System.Drawing.Size(64, 20);
            this.txtShichosonCd.TabIndex = 2;
            this.txtShichosonCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShichosonCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtShichosonCd_Validating);
            // 
            // lblShiireCd
            // 
            this.lblShiireCd.BackColor = System.Drawing.Color.Silver;
            this.lblShiireCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShiireCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShiireCd.Location = new System.Drawing.Point(17, 13);
            this.lblShiireCd.Name = "lblShiireCd";
            this.lblShiireCd.Size = new System.Drawing.Size(115, 20);
            this.lblShiireCd.TabIndex = 1;
            this.lblShiireCd.Text = "市町村コード";
            this.lblShiireCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlMain
            // 
            this.pnlMain.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlMain.Controls.Add(this.txtShiteiBango);
            this.pnlMain.Controls.Add(this.lblFaxBango);
            this.pnlMain.Controls.Add(this.txtKozaBango);
            this.pnlMain.Controls.Add(this.lblDenwaBango);
            this.pnlMain.Controls.Add(this.txtKanyushaNm);
            this.pnlMain.Controls.Add(this.lblJusho1);
            this.pnlMain.Controls.Add(this.txtShichosonKanaNm);
            this.pnlMain.Controls.Add(this.lblShiireKanaNm);
            this.pnlMain.Controls.Add(this.txtShichosonNm);
            this.pnlMain.Controls.Add(this.lblShiireNm);
            this.pnlMain.Location = new System.Drawing.Point(12, 35);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(406, 125);
            this.pnlMain.TabIndex = 3;
            // 
            // txtShiteiBango
            // 
            this.txtShiteiBango.AutoSizeFromLength = true;
            this.txtShiteiBango.DisplayLength = null;
            this.txtShiteiBango.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShiteiBango.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtShiteiBango.Location = new System.Drawing.Point(118, 97);
            this.txtShiteiBango.MaxLength = 20;
            this.txtShiteiBango.Name = "txtShiteiBango";
            this.txtShiteiBango.Size = new System.Drawing.Size(146, 20);
            this.txtShiteiBango.TabIndex = 15;
            this.txtShiteiBango.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LastControl_KeyDown);
            this.txtShiteiBango.Validating += new System.ComponentModel.CancelEventHandler(this.txtShiteiBango_Validating);
            // 
            // lblFaxBango
            // 
            this.lblFaxBango.BackColor = System.Drawing.Color.Silver;
            this.lblFaxBango.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFaxBango.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFaxBango.Location = new System.Drawing.Point(3, 97);
            this.lblFaxBango.Name = "lblFaxBango";
            this.lblFaxBango.Size = new System.Drawing.Size(115, 20);
            this.lblFaxBango.TabIndex = 14;
            this.lblFaxBango.Text = "指定番号";
            this.lblFaxBango.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKozaBango
            // 
            this.txtKozaBango.AutoSizeFromLength = true;
            this.txtKozaBango.DisplayLength = null;
            this.txtKozaBango.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKozaBango.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtKozaBango.Location = new System.Drawing.Point(118, 74);
            this.txtKozaBango.MaxLength = 20;
            this.txtKozaBango.Name = "txtKozaBango";
            this.txtKozaBango.Size = new System.Drawing.Size(146, 20);
            this.txtKozaBango.TabIndex = 13;
            this.txtKozaBango.Validating += new System.ComponentModel.CancelEventHandler(this.txtKozaBango_Validating);
            // 
            // lblDenwaBango
            // 
            this.lblDenwaBango.BackColor = System.Drawing.Color.Silver;
            this.lblDenwaBango.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDenwaBango.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDenwaBango.Location = new System.Drawing.Point(3, 74);
            this.lblDenwaBango.Name = "lblDenwaBango";
            this.lblDenwaBango.Size = new System.Drawing.Size(115, 20);
            this.lblDenwaBango.TabIndex = 12;
            this.lblDenwaBango.Text = "口座番号";
            this.lblDenwaBango.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKanyushaNm
            // 
            this.txtKanyushaNm.AutoSizeFromLength = false;
            this.txtKanyushaNm.DisplayLength = null;
            this.txtKanyushaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKanyushaNm.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtKanyushaNm.Location = new System.Drawing.Point(118, 50);
            this.txtKanyushaNm.MaxLength = 30;
            this.txtKanyushaNm.Name = "txtKanyushaNm";
            this.txtKanyushaNm.Size = new System.Drawing.Size(269, 20);
            this.txtKanyushaNm.TabIndex = 9;
            this.txtKanyushaNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtKanyushaNm_Validating);
            // 
            // lblJusho1
            // 
            this.lblJusho1.BackColor = System.Drawing.Color.Silver;
            this.lblJusho1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblJusho1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJusho1.Location = new System.Drawing.Point(3, 50);
            this.lblJusho1.Name = "lblJusho1";
            this.lblJusho1.Size = new System.Drawing.Size(115, 20);
            this.lblJusho1.TabIndex = 8;
            this.lblJusho1.Text = "加入者名";
            this.lblJusho1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShichosonKanaNm
            // 
            this.txtShichosonKanaNm.AutoSizeFromLength = false;
            this.txtShichosonKanaNm.DisplayLength = null;
            this.txtShichosonKanaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShichosonKanaNm.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtShichosonKanaNm.Location = new System.Drawing.Point(118, 26);
            this.txtShichosonKanaNm.MaxLength = 30;
            this.txtShichosonKanaNm.Name = "txtShichosonKanaNm";
            this.txtShichosonKanaNm.Size = new System.Drawing.Size(269, 20);
            this.txtShichosonKanaNm.TabIndex = 3;
            this.txtShichosonKanaNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtShichosonKanaNm_Validating);
            // 
            // lblShiireKanaNm
            // 
            this.lblShiireKanaNm.BackColor = System.Drawing.Color.Silver;
            this.lblShiireKanaNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShiireKanaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShiireKanaNm.Location = new System.Drawing.Point(3, 26);
            this.lblShiireKanaNm.Name = "lblShiireKanaNm";
            this.lblShiireKanaNm.Size = new System.Drawing.Size(115, 20);
            this.lblShiireKanaNm.TabIndex = 2;
            this.lblShiireKanaNm.Text = "カナ名";
            this.lblShiireKanaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShichosonNm
            // 
            this.txtShichosonNm.AutoSizeFromLength = false;
            this.txtShichosonNm.DisplayLength = null;
            this.txtShichosonNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShichosonNm.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtShichosonNm.Location = new System.Drawing.Point(118, 3);
            this.txtShichosonNm.MaxLength = 30;
            this.txtShichosonNm.Name = "txtShichosonNm";
            this.txtShichosonNm.Size = new System.Drawing.Size(269, 20);
            this.txtShichosonNm.TabIndex = 1;
            this.txtShichosonNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtShichosonNm_Validating);
            // 
            // lblShiireNm
            // 
            this.lblShiireNm.BackColor = System.Drawing.Color.Silver;
            this.lblShiireNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShiireNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShiireNm.Location = new System.Drawing.Point(3, 3);
            this.lblShiireNm.Name = "lblShiireNm";
            this.lblShiireNm.Size = new System.Drawing.Size(115, 20);
            this.lblShiireNm.TabIndex = 0;
            this.lblShiireNm.Text = "市町村名";
            this.lblShiireNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMODE
            // 
            this.lblMODE.BackColor = System.Drawing.Color.White;
            this.lblMODE.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMODE.Location = new System.Drawing.Point(323, 12);
            this.lblMODE.Name = "lblMODE";
            this.lblMODE.Size = new System.Drawing.Size(78, 20);
            this.lblMODE.TabIndex = 902;
            this.lblMODE.Text = "【編集】";
            this.lblMODE.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // KYCM1082
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(430, 221);
            this.Controls.Add(this.lblMODE);
            this.Controls.Add(this.pnlMain);
            this.Controls.Add(this.txtShichosonCd);
            this.Controls.Add(this.lblShiireCd);
            this.Name = "KYCM1082";
            this.Text = "市町村の登録";
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblShiireCd, 0);
            this.Controls.SetChildIndex(this.txtShichosonCd, 0);
            this.Controls.SetChildIndex(this.pnlMain, 0);
            this.Controls.SetChildIndex(this.lblMODE, 0);
            this.pnlDebug.ResumeLayout(false);
            this.pnlMain.ResumeLayout(false);
            this.pnlMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private jp.co.fsi.common.controls.FsiTextBox txtShichosonCd;
        private System.Windows.Forms.Label lblShiireCd;
        private jp.co.fsi.common.FsiPanel pnlMain;
        private jp.co.fsi.common.controls.FsiTextBox txtShichosonNm;
        private System.Windows.Forms.Label lblShiireNm;
        private System.Windows.Forms.Label lblShiireKanaNm;
        private jp.co.fsi.common.controls.FsiTextBox txtShichosonKanaNm;
        private jp.co.fsi.common.controls.FsiTextBox txtKanyushaNm;
        private System.Windows.Forms.Label lblJusho1;
        private System.Windows.Forms.Label lblDenwaBango;
        private jp.co.fsi.common.controls.FsiTextBox txtKozaBango;
        private jp.co.fsi.common.controls.FsiTextBox txtShiteiBango;
        private System.Windows.Forms.Label lblFaxBango;
        private System.Windows.Forms.Label lblMODE;
    }
}