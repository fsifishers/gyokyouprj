﻿using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.ky.kycm1031
{
    /// <summary>
    /// 部門の登録(KYCM1032)
    /// </summary>
    public partial class KYCM1032 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(新規)
        /// </summary>
        private const string MODE_NEW = "1";

        /// <summary>
        /// モード(編集)
        /// </summary>
        private const string MODE_EDIT = "2";
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public KYCM1032()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="par1">引数1</param>
        public KYCM1032(string par1)
            : base(par1)
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 引数：Par1／モード(1:新規、2:変更)、InData：部門コード
            if (MODE_NEW.Equals(this.Par1))
            {
                // 新規モードの初期表示
                InitDispOnNew();
                this.btnF3.Enabled = false;
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 編集モードの初期表示
                InitDispOnEdit();
            }
            else
            {
                // 不正な起動として閉じる
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }
            this.ShowFButton = true;

            // タイトルは非表示
            this.lblTitle.Visible = false;
            // EscapeとF1のみ表示
            this.ShowFButton = true;
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF1.Location = this.btnF2.Location;
            this.btnF6.Location = this.btnF4.Location;
            this.btnF2.Visible = false;
            this.btnF3.Visible = true;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF6.Visible = true;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F3キー押下時処理
        /// </summary>
        public override void PressF3()
        {
            // 新規モードの場合は処理させない
            if (this.Par1.Equals(MODE_NEW)) return;

            // 確認メッセージを表示
            string msg = "削除しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // 削除用パラメータ
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
            dpc.SetParam("@BUMON_CD", SqlDbType.Decimal, 4, Util.ToString(this.InData));

            try
            {
                // トランザクション開始
                this.Dba.BeginTransaction();

                if (MODE_EDIT.Equals(this.Par1))
                {
                    // データ削除
                    // 給与部門マスタ
                    this.Dba.Delete("TB_KY_BUMON", "KAISHA_CD = @KAISHA_CD AND BUMON_CD = @BUMON_CD", dpc);
                }

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        
        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        public override void PressF6()
        {
            // 確認メッセージを表示
            string msg = (MODE_NEW.Equals(this.Par1) ? "登録" : "更新") + "しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // 入力値をバインドパラメータとしてセットする
            ArrayList alParamsKyBumon = SetKyBumonParams();

            try
            {
                // トランザクション開始
                this.Dba.BeginTransaction();

                if (MODE_NEW.Equals(this.Par1))
                {
                    // データ登録
                    // 給与部門マスタ
                    this.Dba.Insert("TB_KY_BUMON", (DbParamCollection)alParamsKyBumon[0]);
                }
                else if (MODE_EDIT.Equals(this.Par1))
                {
                    // データ更新
                    // 給与部門マスタ
                    this.Dba.Update("TB_KY_BUMON",
                        (DbParamCollection)alParamsKyBumon[1],
                        "KAISHA_CD = @KAISHA_CD AND BUMON_CD = @BUMON_CD",
                        (DbParamCollection)alParamsKyBumon[0]);
                }

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 部門コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBumonCd_Validating(object sender, CancelEventArgs e)
        {
            // 入力値補正
            this.txtBumonCd.Text = Util.ToString(Util.ToDecimal(this.txtBumonCd.Text));

            if (!IsValidBumonCd())
            {
                e.Cancel = true;
                this.txtBumonCd.SelectAll();
            }
        }

        /// <summary>
        /// 部門名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBumonNm_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidBumonNm())
            {
                e.Cancel = true;
                this.txtBumonNm.SelectAll();
            }
        }

        /// <summary>
        /// カナ名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBumonKanaNm_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidBumonKanaNm())
            {
                e.Cancel = true;
                this.txtBumonKanaNm.SelectAll();
            }
        }

        /// <summary>
        /// 画面の最後のコントールでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LastControl_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.btnF6.Enabled)
            {
                this.PressF6();
            }
        }
        #endregion

        #region privateメソッド

        /// <summary>
        /// 新規モードの初期表示
        /// </summary>
        private void InitDispOnNew()
        {
            // 初期値
            this.txtBumonCd.Text="0";

            // 追加・変更状態表示
            this.lblMODE.Text = "【追加】";

            // 部門コードに初期フォーカス
            this.ActiveControl = this.txtBumonCd;
            this.txtBumonCd.Focus();
        }

        /// <summary>
        /// 編集モードの初期表示
        /// </summary>
        private void InitDispOnEdit()
        {
            // 現在DBに登録されている値、入力制御を実装
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
            dpc.SetParam("@BUMON_CD", SqlDbType.Decimal, 4, Util.ToString(this.InData));
            string cols = "BUMON_CD, BUMON_NM, BUMON_KANA_NM";
            string from = "TB_KY_BUMON";
            string where = "KAISHA_CD = @KAISHA_CD AND BUMON_CD = @BUMON_CD";
            DataTable dt = this.Dba.GetDataTableByConditionWithParams(cols, from, where, dpc);
            if (dt.Rows.Count == 0)
            {
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }

            // 取得した内容を表示
            DataRow drDispData = dt.Rows[0];
            this.txtBumonCd.Text = Util.ToString(drDispData["BUMON_CD"]);
            this.txtBumonNm.Text = Util.ToString(drDispData["BUMON_NM"]);
            this.txtBumonKanaNm.Text = Util.ToString(drDispData["BUMON_KANA_NM"]);

            // 追加・変更状態表示
            this.lblMODE.Text = "【変更】";

            // 部門コードは入力不可
            this.lblBumonCd.Enabled = false;
            this.txtBumonCd.Enabled = false;
        }

        /// <summary>
        /// 部門コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidBumonCd()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtBumonCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 既に存在するコードを入力した場合はエラーとする
            if (this.txtBumonCd.Text != "0")
            {
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
                dpc.SetParam("@BUMON_CD", SqlDbType.Decimal, 4, this.txtBumonCd.Text);
                string cols = "BUMON_CD";
                string from = "TB_KY_BUMON";
                string where = "KAISHA_CD = @KAISHA_CD AND BUMON_CD = @BUMON_CD";
                DataTable dt = this.Dba.GetDataTableByConditionWithParams(cols, from, where, dpc);
                if (dt.Rows.Count > 0)
                {
                    Msg.Error("既に存在する部門コードと重複しています。");
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 部門名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidBumonNm()
        {
            // 指定バイト数を超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtBumonNm.Text, this.txtBumonNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// カナ名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidBumonKanaNm()
        {
            // 指定バイト数を超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtBumonKanaNm.Text, this.txtBumonKanaNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 必須項目のチェック
            if (this.txtBumonCd.Text == "0")
            {
                Msg.Notice("ゼロは使用できません。");
                this.txtBumonCd.Focus();
                return false;
            }

            if (MODE_NEW.Equals(this.Par1))
            {
                // 部門コードのチェック
                if (!IsValidBumonCd())
                {
                    this.txtBumonCd.Focus();
                    return false;
                }
            }

            // 部門名のチェック
            if (!IsValidBumonNm())
            {
                this.txtBumonNm.Focus();
                return false;
            }

            // カナ名のチェック
            if (!IsValidBumonNm())
            {
                this.txtBumonNm.Focus();
                return false;
            }

            return true;
        }

        /// <summary>
        /// TB_KY_BUMONに更新するためのパラメータ設定をします。
        /// </summary>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// 更新処理：DbParamCollection*2(Where句,Set句)
        /// </returns>
        private ArrayList SetKyBumonParams()
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            if (MODE_NEW.Equals(this.Par1))
            {
                // 会社コード
                updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
                // 部門コード
                updParam.SetParam("@BUMON_CD", SqlDbType.Decimal, 4, this.txtBumonCd.Text);
                // 登録日
                updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWDATE");
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // WHERE句のパラメータに設定
                DbParamCollection whereParam = new DbParamCollection();
                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
                whereParam.SetParam("@BUMON_CD", SqlDbType.Decimal, 4, this.txtBumonCd.Text);
                alParams.Add(whereParam);
            }

            // 部門名
            updParam.SetParam("@BUMON_NM", SqlDbType.VarChar, 30, this.txtBumonNm.Text);
            // カナ名
            updParam.SetParam("@BUMON_KANA_NM", SqlDbType.VarChar, 30, this.txtBumonKanaNm.Text);
            // 更新日
            updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWDATE");

            alParams.Add(updParam);

            return alParams;
        }
        /// <summary>
        /// TB_KY_BUMONからデータを削除
        /// </summary>
        /// <returns>
        /// </returns>
        private ArrayList SetDelParams()
        {
            // 会社コードと部門コードを削除パラメータに設定
            ArrayList alParams = new ArrayList();
            DbParamCollection dpcDenpyo = new DbParamCollection();
            dpcDenpyo.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
            dpcDenpyo.SetParam("@BUMON_CD", SqlDbType.VarChar, 6, this.txtBumonCd.Text);

            alParams.Add(dpcDenpyo);

            return alParams;
        }


        #endregion
    }
}
