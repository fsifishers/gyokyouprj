﻿namespace jp.co.fsi.ky.kycm1031
{
    partial class KYCM1033
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtBumonCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtBumonCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblBumonCdTo = new System.Windows.Forms.Label();
            this.lblBumonCdFr = new System.Windows.Forms.Label();
            this.lblCodeBet1 = new System.Windows.Forms.Label();
            this.gbxCondition = new System.Windows.Forms.GroupBox();
            this.pnlDebug.SuspendLayout();
            this.gbxCondition.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(531, 23);
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(12, 53);
            this.pnlDebug.Size = new System.Drawing.Size(548, 100);
            // 
            // txtBumonCdFr
            // 
            this.txtBumonCdFr.AutoSizeFromLength = false;
            this.txtBumonCdFr.DisplayLength = null;
            this.txtBumonCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtBumonCdFr.Location = new System.Drawing.Point(14, 32);
            this.txtBumonCdFr.MaxLength = 4;
            this.txtBumonCdFr.Name = "txtBumonCdFr";
            this.txtBumonCdFr.Size = new System.Drawing.Size(48, 20);
            this.txtBumonCdFr.TabIndex = 902;
            this.txtBumonCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBumonCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtBumonCdFr_Validating);
            // 
            // txtBumonCdTo
            // 
            this.txtBumonCdTo.AutoSizeFromLength = false;
            this.txtBumonCdTo.DisplayLength = null;
            this.txtBumonCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtBumonCdTo.Location = new System.Drawing.Point(277, 32);
            this.txtBumonCdTo.MaxLength = 4;
            this.txtBumonCdTo.Name = "txtBumonCdTo";
            this.txtBumonCdTo.Size = new System.Drawing.Size(48, 20);
            this.txtBumonCdTo.TabIndex = 903;
            this.txtBumonCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBumonCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtBumonCdTo_Validating);
            // 
            // lblBumonCdTo
            // 
            this.lblBumonCdTo.BackColor = System.Drawing.Color.Silver;
            this.lblBumonCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBumonCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBumonCdTo.Location = new System.Drawing.Point(331, 32);
            this.lblBumonCdTo.Name = "lblBumonCdTo";
            this.lblBumonCdTo.Size = new System.Drawing.Size(180, 19);
            this.lblBumonCdTo.TabIndex = 904;
            this.lblBumonCdTo.Text = "最　後";
            this.lblBumonCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblBumonCdFr
            // 
            this.lblBumonCdFr.BackColor = System.Drawing.Color.Silver;
            this.lblBumonCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBumonCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBumonCdFr.Location = new System.Drawing.Point(68, 32);
            this.lblBumonCdFr.Name = "lblBumonCdFr";
            this.lblBumonCdFr.Size = new System.Drawing.Size(180, 19);
            this.lblBumonCdFr.TabIndex = 905;
            this.lblBumonCdFr.Text = "先  頭";
            this.lblBumonCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCodeBet1
            // 
            this.lblCodeBet1.AutoSize = true;
            this.lblCodeBet1.Location = new System.Drawing.Point(254, 36);
            this.lblCodeBet1.Name = "lblCodeBet1";
            this.lblCodeBet1.Size = new System.Drawing.Size(20, 13);
            this.lblCodeBet1.TabIndex = 906;
            this.lblCodeBet1.Text = "～";
            // 
            // gbxCondition
            // 
            this.gbxCondition.Controls.Add(this.lblBumonCdFr);
            this.gbxCondition.Controls.Add(this.lblCodeBet1);
            this.gbxCondition.Controls.Add(this.txtBumonCdFr);
            this.gbxCondition.Controls.Add(this.txtBumonCdTo);
            this.gbxCondition.Controls.Add(this.lblBumonCdTo);
            this.gbxCondition.Font = new System.Drawing.Font("MS UI Gothic", 9.75F);
            this.gbxCondition.Location = new System.Drawing.Point(13, 11);
            this.gbxCondition.Name = "gbxCondition";
            this.gbxCondition.Size = new System.Drawing.Size(534, 83);
            this.gbxCondition.TabIndex = 907;
            this.gbxCondition.TabStop = false;
            this.gbxCondition.Text = "部門コード範囲";
            // 
            // KYCM1033
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(556, 153);
            this.Controls.Add(this.gbxCondition);
            this.Name = "KYCM1033";
            this.Text = "部門の印刷";
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.gbxCondition, 0);
            this.pnlDebug.ResumeLayout(false);
            this.gbxCondition.ResumeLayout(false);
            this.gbxCondition.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private common.controls.FsiTextBox txtBumonCdFr;
        private common.controls.FsiTextBox txtBumonCdTo;
        private System.Windows.Forms.Label lblBumonCdTo;
        private System.Windows.Forms.Label lblBumonCdFr;
        private System.Windows.Forms.Label lblCodeBet1;
        private System.Windows.Forms.GroupBox gbxCondition;
    }
}