﻿namespace jp.co.fsi.ky.kycm1031
{
    /// <summary>
    /// KYCM1031R の帳票
    /// </summary>
    partial class KYCM1031R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(KYCM1031R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.txtTitle04 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtToday = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtPageCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCompanyName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtTitle01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtValue02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue04 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtTitle04,
            this.txtToday,
            this.lblPage,
            this.txtPageCount,
            this.txtCompanyName,
            this.lblTitle,
            this.line1,
            this.txtTitle01,
            this.txtTitle02,
            this.txtTitle03,
            this.line18,
            this.line16,
            this.line2,
            this.line3,
            this.line4,
            this.line5,
            this.line11});
            this.pageHeader.Height = 0.8391569F;
            this.pageHeader.Name = "pageHeader";
            this.pageHeader.Format += new System.EventHandler(this.pageHeader_Format);
            // 
            // txtTitle04
            // 
            this.txtTitle04.Height = 0.2362205F;
            this.txtTitle04.Left = 4.744095F;
            this.txtTitle04.MultiLine = false;
            this.txtTitle04.Name = "txtTitle04";
            this.txtTitle04.Style = "background-color: Cyan; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: bol" +
    "d; text-align: center; vertical-align: middle";
            this.txtTitle04.Text = null;
            this.txtTitle04.Top = 0.5976378F;
            this.txtTitle04.Width = 2.736221F;
            // 
            // txtToday
            // 
            this.txtToday.Height = 0.2070866F;
            this.txtToday.Left = 5.844882F;
            this.txtToday.MultiLine = false;
            this.txtToday.Name = "txtToday";
            this.txtToday.OutputFormat = resources.GetString("txtToday.OutputFormat");
            this.txtToday.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle";
            this.txtToday.Text = "ggyy年M月d日";
            this.txtToday.Top = 0F;
            this.txtToday.Width = 1.181102F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.2070866F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 7.321261F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle";
            this.lblPage.Text = "頁";
            this.lblPage.Top = 0F;
            this.lblPage.Width = 0.1590552F;
            // 
            // txtPageCount
            // 
            this.txtPageCount.Height = 0.2070866F;
            this.txtPageCount.Left = 7.025986F;
            this.txtPageCount.MultiLine = false;
            this.txtPageCount.Name = "txtPageCount";
            this.txtPageCount.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle";
            this.txtPageCount.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtPageCount.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.txtPageCount.Text = "999";
            this.txtPageCount.Top = 0F;
            this.txtPageCount.Width = 0.2952756F;
            // 
            // txtCompanyName
            // 
            this.txtCompanyName.DataField = "ITEM001";
            this.txtCompanyName.Height = 0.1968504F;
            this.txtCompanyName.Left = 0.04094489F;
            this.txtCompanyName.MultiLine = false;
            this.txtCompanyName.Name = "txtCompanyName";
            this.txtCompanyName.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; vertical-align: middle";
            this.txtCompanyName.Text = null;
            this.txtCompanyName.Top = 0.0496063F;
            this.txtCompanyName.Width = 2.47874F;
            // 
            // lblTitle
            // 
            this.lblTitle.Height = 0.2464567F;
            this.lblTitle.HyperLink = null;
            this.lblTitle.Left = 2.843701F;
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Style = "font-family: ＭＳ 明朝; font-size: 15.75pt; font-weight: bold; text-align: center";
            this.lblTitle.Text = "部門リスト";
            this.lblTitle.Top = 0F;
            this.lblTitle.Width = 1.832284F;
            // 
            // line1
            // 
            this.line1.Height = 0F;
            this.line1.Left = 2.843701F;
            this.line1.LineWeight = 1F;
            this.line1.Name = "line1";
            this.line1.Top = 0.2464567F;
            this.line1.Width = 1.764172F;
            this.line1.X1 = 2.843701F;
            this.line1.X2 = 4.607873F;
            this.line1.Y1 = 0.2464567F;
            this.line1.Y2 = 0.2464567F;
            // 
            // txtTitle01
            // 
            this.txtTitle01.Height = 0.2362205F;
            this.txtTitle01.Left = 0.01968504F;
            this.txtTitle01.MultiLine = false;
            this.txtTitle01.Name = "txtTitle01";
            this.txtTitle01.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: norma" +
    "l; text-align: center; vertical-align: middle; ddo-char-set: 128";
            this.txtTitle01.Text = "ｺ ｰ ﾄﾞ";
            this.txtTitle01.Top = 0.5976378F;
            this.txtTitle01.Width = 0.7874016F;
            // 
            // txtTitle02
            // 
            this.txtTitle02.Height = 0.2362205F;
            this.txtTitle02.Left = 0.8070866F;
            this.txtTitle02.MultiLine = false;
            this.txtTitle02.Name = "txtTitle02";
            this.txtTitle02.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: norma" +
    "l; text-align: center; vertical-align: middle; ddo-char-set: 128";
            this.txtTitle02.Text = "部  門  名";
            this.txtTitle02.Top = 0.5976378F;
            this.txtTitle02.Width = 1.968504F;
            // 
            // txtTitle03
            // 
            this.txtTitle03.Height = 0.2362205F;
            this.txtTitle03.Left = 2.775591F;
            this.txtTitle03.MultiLine = false;
            this.txtTitle03.Name = "txtTitle03";
            this.txtTitle03.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: norma" +
    "l; text-align: center; vertical-align: middle; ddo-char-set: 128";
            this.txtTitle03.Text = "カ　ナ　名";
            this.txtTitle03.Top = 0.5976378F;
            this.txtTitle03.Width = 1.968504F;
            // 
            // line18
            // 
            this.line18.Height = 0.2362206F;
            this.line18.Left = 0.8070866F;
            this.line18.LineWeight = 2F;
            this.line18.Name = "line18";
            this.line18.Top = 0.5976378F;
            this.line18.Width = 0F;
            this.line18.X1 = 0.8070866F;
            this.line18.X2 = 0.8070866F;
            this.line18.Y1 = 0.5976378F;
            this.line18.Y2 = 0.8338584F;
            // 
            // line16
            // 
            this.line16.Height = 0.2362205F;
            this.line16.Left = 2.775591F;
            this.line16.LineWeight = 2F;
            this.line16.Name = "line16";
            this.line16.Top = 0.5976378F;
            this.line16.Width = 0F;
            this.line16.X1 = 2.775591F;
            this.line16.X2 = 2.775591F;
            this.line16.Y1 = 0.5976378F;
            this.line16.Y2 = 0.8338583F;
            // 
            // line2
            // 
            this.line2.Height = 1.192093E-07F;
            this.line2.Left = 0F;
            this.line2.LineWeight = 1F;
            this.line2.Name = "line2";
            this.line2.Top = 0.5976378F;
            this.line2.Width = 7.480316F;
            this.line2.X1 = 0F;
            this.line2.X2 = 7.480316F;
            this.line2.Y1 = 0.5976378F;
            this.line2.Y2 = 0.5976379F;
            // 
            // line3
            // 
            this.line3.Height = 0F;
            this.line3.Left = -3.72529E-09F;
            this.line3.LineWeight = 1F;
            this.line3.Name = "line3";
            this.line3.Top = 0.8338583F;
            this.line3.Width = 7.509449F;
            this.line3.X1 = -3.72529E-09F;
            this.line3.X2 = 7.509449F;
            this.line3.Y1 = 0.8338583F;
            this.line3.Y2 = 0.8338583F;
            // 
            // line4
            // 
            this.line4.Height = 0.2362205F;
            this.line4.Left = 7.480315F;
            this.line4.LineWeight = 2F;
            this.line4.Name = "line4";
            this.line4.Top = 0.5976378F;
            this.line4.Width = 0F;
            this.line4.X1 = 7.480315F;
            this.line4.X2 = 7.480315F;
            this.line4.Y1 = 0.5976378F;
            this.line4.Y2 = 0.8338583F;
            // 
            // line5
            // 
            this.line5.Height = 0.2480315F;
            this.line5.Left = 0.01181102F;
            this.line5.LineWeight = 2F;
            this.line5.Name = "line5";
            this.line5.Top = 0.5858268F;
            this.line5.Width = 0F;
            this.line5.X1 = 0.01181102F;
            this.line5.X2 = 0.01181102F;
            this.line5.Y1 = 0.5858268F;
            this.line5.Y2 = 0.8338583F;
            // 
            // line11
            // 
            this.line11.Height = 0.2362206F;
            this.line11.Left = 4.744095F;
            this.line11.LineWeight = 2F;
            this.line11.Name = "line11";
            this.line11.Top = 0.5976378F;
            this.line11.Width = 0F;
            this.line11.X1 = 4.744095F;
            this.line11.X2 = 4.744095F;
            this.line11.Y1 = 0.5976378F;
            this.line11.Y2 = 0.8338584F;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtValue02,
            this.txtValue03,
            this.txtValue04,
            this.txtValue01,
            this.line6,
            this.line7,
            this.line8,
            this.line9,
            this.line10,
            this.line12});
            this.detail.Height = 0.1980315F;
            this.detail.Name = "detail";
            this.detail.RepeatToFill = true;
            // 
            // txtValue02
            // 
            this.txtValue02.DataField = "ITEM003";
            this.txtValue02.Height = 0.1889764F;
            this.txtValue02.Left = 0.8070866F;
            this.txtValue02.MultiLine = false;
            this.txtValue02.Name = "txtValue02";
            this.txtValue02.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle";
            this.txtValue02.Text = "\r\n\r\n";
            this.txtValue02.Top = 0F;
            this.txtValue02.Width = 1.968504F;
            // 
            // txtValue03
            // 
            this.txtValue03.DataField = "ITEM004";
            this.txtValue03.Height = 0.1889764F;
            this.txtValue03.Left = 2.775591F;
            this.txtValue03.MultiLine = false;
            this.txtValue03.Name = "txtValue03";
            this.txtValue03.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle";
            this.txtValue03.Text = "\r\n\r\n";
            this.txtValue03.Top = 0F;
            this.txtValue03.Width = 1.968504F;
            // 
            // txtValue04
            // 
            this.txtValue04.DataField = "ITEM005";
            this.txtValue04.Height = 0.1889764F;
            this.txtValue04.Left = 4.744095F;
            this.txtValue04.MultiLine = false;
            this.txtValue04.Name = "txtValue04";
            this.txtValue04.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle";
            this.txtValue04.Text = "\r\n\r\n";
            this.txtValue04.Top = 0F;
            this.txtValue04.Width = 2.736221F;
            // 
            // txtValue01
            // 
            this.txtValue01.DataField = "ITEM002";
            this.txtValue01.Height = 0.1889764F;
            this.txtValue01.Left = 0F;
            this.txtValue01.MultiLine = false;
            this.txtValue01.Name = "txtValue01";
            this.txtValue01.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle";
            this.txtValue01.Text = "\r\n\r\n";
            this.txtValue01.Top = 0F;
            this.txtValue01.Width = 0.8070866F;
            // 
            // line6
            // 
            this.line6.Height = 0F;
            this.line6.Left = 3.72529E-09F;
            this.line6.LineWeight = 1F;
            this.line6.Name = "line6";
            this.line6.Top = 0.1889764F;
            this.line6.Width = 7.50945F;
            this.line6.X1 = 3.72529E-09F;
            this.line6.X2 = 7.50945F;
            this.line6.Y1 = 0.1889764F;
            this.line6.Y2 = 0.1889764F;
            // 
            // line7
            // 
            this.line7.Height = 0.1889764F;
            this.line7.Left = 0.8070866F;
            this.line7.LineWeight = 2F;
            this.line7.Name = "line7";
            this.line7.Top = 0F;
            this.line7.Width = 0F;
            this.line7.X1 = 0.8070866F;
            this.line7.X2 = 0.8070866F;
            this.line7.Y1 = 0F;
            this.line7.Y2 = 0.1889764F;
            // 
            // line8
            // 
            this.line8.Height = 0.1889764F;
            this.line8.Left = 2.775591F;
            this.line8.LineWeight = 2F;
            this.line8.Name = "line8";
            this.line8.Top = 0F;
            this.line8.Width = 0F;
            this.line8.X1 = 2.775591F;
            this.line8.X2 = 2.775591F;
            this.line8.Y1 = 0F;
            this.line8.Y2 = 0.1889764F;
            // 
            // line9
            // 
            this.line9.Height = 0.1889764F;
            this.line9.Left = 0.01181102F;
            this.line9.LineWeight = 2F;
            this.line9.Name = "line9";
            this.line9.Top = 0F;
            this.line9.Width = 0F;
            this.line9.X1 = 0.01181102F;
            this.line9.X2 = 0.01181102F;
            this.line9.Y1 = 0F;
            this.line9.Y2 = 0.1889764F;
            // 
            // line10
            // 
            this.line10.Height = 0.1889764F;
            this.line10.Left = 7.480316F;
            this.line10.LineWeight = 2F;
            this.line10.Name = "line10";
            this.line10.Top = 0F;
            this.line10.Width = 0F;
            this.line10.X1 = 7.480316F;
            this.line10.X2 = 7.480316F;
            this.line10.Y1 = 0F;
            this.line10.Y2 = 0.1889764F;
            // 
            // line12
            // 
            this.line12.Height = 0.1889764F;
            this.line12.Left = 4.744095F;
            this.line12.LineWeight = 2F;
            this.line12.Name = "line12";
            this.line12.Top = 0F;
            this.line12.Width = 0F;
            this.line12.X1 = 4.744095F;
            this.line12.X2 = 4.744095F;
            this.line12.Y1 = 0F;
            this.line12.Y2 = 0.1889764F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            // 
            // KYCM1031R
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.3937008F;
            this.PageSettings.Margins.Left = 0.3937008F;
            this.PageSettings.Margins.Right = 0.3937008F;
            this.PageSettings.Margins.Top = 0.3937008F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.480316F;
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.pageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtToday;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPageCount;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCompanyName;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle03;
        private GrapeCity.ActiveReports.SectionReportModel.Line line18;
        private GrapeCity.ActiveReports.SectionReportModel.Line line16;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.Line line3;
        private GrapeCity.ActiveReports.SectionReportModel.Line line4;
        private GrapeCity.ActiveReports.SectionReportModel.Line line5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue02;
        private GrapeCity.ActiveReports.SectionReportModel.Line line6;
        private GrapeCity.ActiveReports.SectionReportModel.Line line7;
        private GrapeCity.ActiveReports.SectionReportModel.Line line8;
        private GrapeCity.ActiveReports.SectionReportModel.Line line9;
        private GrapeCity.ActiveReports.SectionReportModel.Line line10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle04;
        private GrapeCity.ActiveReports.SectionReportModel.Line line11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue04;
        private GrapeCity.ActiveReports.SectionReportModel.Line line12;
    }
}
