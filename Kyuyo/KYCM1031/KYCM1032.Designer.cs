﻿namespace jp.co.fsi.ky.kycm1031
{
    partial class KYCM1032
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtBumonCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblBumonCd = new System.Windows.Forms.Label();
            this.lblMODE = new System.Windows.Forms.Label();
            this.txtBumonKanaNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblBumonKanaNm = new System.Windows.Forms.Label();
            this.txtBumonNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblBumonNm = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Location = new System.Drawing.Point(12, 22);
            this.lblTitle.Size = new System.Drawing.Size(358, 23);
            this.lblTitle.Text = "部門の登録";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(5, 97);
            this.pnlDebug.Size = new System.Drawing.Size(426, 100);
            // 
            // txtBumonCd
            // 
            this.txtBumonCd.AutoSizeFromLength = true;
            this.txtBumonCd.DisplayLength = null;
            this.txtBumonCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtBumonCd.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtBumonCd.Location = new System.Drawing.Point(130, 21);
            this.txtBumonCd.MaxLength = 4;
            this.txtBumonCd.Name = "txtBumonCd";
            this.txtBumonCd.Size = new System.Drawing.Size(64, 23);
            this.txtBumonCd.TabIndex = 0;
            this.txtBumonCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBumonCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtBumonCd_Validating);
            // 
            // lblBumonCd
            // 
            this.lblBumonCd.BackColor = System.Drawing.Color.Silver;
            this.lblBumonCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBumonCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBumonCd.Location = new System.Drawing.Point(16, 22);
            this.lblBumonCd.Name = "lblBumonCd";
            this.lblBumonCd.Size = new System.Drawing.Size(115, 23);
            this.lblBumonCd.TabIndex = 1;
            this.lblBumonCd.Text = "部門コード";
            this.lblBumonCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMODE
            // 
            this.lblMODE.BackColor = System.Drawing.Color.White;
            this.lblMODE.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMODE.Location = new System.Drawing.Point(321, 21);
            this.lblMODE.Name = "lblMODE";
            this.lblMODE.Size = new System.Drawing.Size(78, 23);
            this.lblMODE.TabIndex = 902;
            this.lblMODE.Text = "【編集】";
            this.lblMODE.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtBumonKanaNm
            // 
            this.txtBumonKanaNm.AutoSizeFromLength = false;
            this.txtBumonKanaNm.DisplayLength = null;
            this.txtBumonKanaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtBumonKanaNm.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtBumonKanaNm.Location = new System.Drawing.Point(130, 111);
            this.txtBumonKanaNm.MaxLength = 30;
            this.txtBumonKanaNm.Name = "txtBumonKanaNm";
            this.txtBumonKanaNm.Size = new System.Drawing.Size(269, 23);
            this.txtBumonKanaNm.TabIndex = 905;
            this.txtBumonKanaNm.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LastControl_KeyDown);
            this.txtBumonKanaNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtBumonKanaNm_Validating);
            // 
            // lblBumonKanaNm
            // 
            this.lblBumonKanaNm.BackColor = System.Drawing.Color.Silver;
            this.lblBumonKanaNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBumonKanaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBumonKanaNm.Location = new System.Drawing.Point(16, 112);
            this.lblBumonKanaNm.Name = "lblBumonKanaNm";
            this.lblBumonKanaNm.Size = new System.Drawing.Size(115, 23);
            this.lblBumonKanaNm.TabIndex = 906;
            this.lblBumonKanaNm.Text = "カナ名";
            this.lblBumonKanaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtBumonNm
            // 
            this.txtBumonNm.AutoSizeFromLength = false;
            this.txtBumonNm.DisplayLength = null;
            this.txtBumonNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtBumonNm.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtBumonNm.Location = new System.Drawing.Point(130, 66);
            this.txtBumonNm.MaxLength = 30;
            this.txtBumonNm.Name = "txtBumonNm";
            this.txtBumonNm.Size = new System.Drawing.Size(269, 23);
            this.txtBumonNm.TabIndex = 903;
            this.txtBumonNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtBumonNm_Validating);
            // 
            // lblBumonNm
            // 
            this.lblBumonNm.BackColor = System.Drawing.Color.Silver;
            this.lblBumonNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBumonNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBumonNm.Location = new System.Drawing.Point(16, 66);
            this.lblBumonNm.Name = "lblBumonNm";
            this.lblBumonNm.Size = new System.Drawing.Size(115, 23);
            this.lblBumonNm.TabIndex = 904;
            this.lblBumonNm.Text = "部門名";
            this.lblBumonNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // KYCM1032
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(418, 200);
            this.Controls.Add(this.txtBumonKanaNm);
            this.Controls.Add(this.lblBumonKanaNm);
            this.Controls.Add(this.txtBumonNm);
            this.Controls.Add(this.lblBumonNm);
            this.Controls.Add(this.lblMODE);
            this.Controls.Add(this.txtBumonCd);
            this.Controls.Add(this.lblBumonCd);
            this.Name = "KYCM1032";
            this.Text = "部門の登録";
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblBumonCd, 0);
            this.Controls.SetChildIndex(this.txtBumonCd, 0);
            this.Controls.SetChildIndex(this.lblMODE, 0);
            this.Controls.SetChildIndex(this.lblBumonNm, 0);
            this.Controls.SetChildIndex(this.txtBumonNm, 0);
            this.Controls.SetChildIndex(this.lblBumonKanaNm, 0);
            this.Controls.SetChildIndex(this.txtBumonKanaNm, 0);
            this.pnlDebug.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private jp.co.fsi.common.controls.FsiTextBox txtBumonCd;
        private System.Windows.Forms.Label lblBumonCd;
        private System.Windows.Forms.Label lblMODE;
        private common.controls.FsiTextBox txtBumonKanaNm;
        private System.Windows.Forms.Label lblBumonKanaNm;
        private common.controls.FsiTextBox txtBumonNm;
        private System.Windows.Forms.Label lblBumonNm;
    }
}