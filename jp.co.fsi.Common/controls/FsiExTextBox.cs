﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Design;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace jp.co.fsi.common
{

	public enum OrgnFormatStyle
	{
		ゼロ詰め = 0,
		カンマ = 1
	}

	public enum OrginFormatType
	{
		文字列 = 0,
		整数値 = 1,
		少数値 = 2

	}

	[Editor("System.Windows.Forms.Design.StringCollectionEditor, System.Design", typeof(UITypeEditor)),
	TypeConverter(typeof(CustomClassConverter))]
	public class FsiExTextBox : TextBox
	{
		private BaseValidate Validator;

		private bool isChanged = false;

		private string strCodevalue = string.Empty;

		private string strBisinessLogicClass = string.Empty;

		public string BisinessLogic
		{
			get { return strBisinessLogicClass; }
			set { strBisinessLogicClass = value; }
		}

		private int intScale = 0;
		public int OrgnScale
		{
			get { return intScale; }
			set { intScale = value; }
		}

		public OrgnFormatStyle oFormatS;
		public OrgnFormatStyle OrgnFormatS
		{
			get
			{
				return oFormatS;
			}
			set
			{
				oFormatS = value;
			}
		}

		public OrginFormatType oFormat;
		public OrginFormatType OrgnFormatType
		{
			get { return oFormat; }
			set { oFormat = value; }
		}

		/// <summary>
		/// バインディング用フィールド
		/// </summary>
		[Bindable(true)]
		[Description("バインド用フィールド")]
		public string CodeValue
		{
			get
			{
				return strCodevalue;
			}

			set
			{
				strCodevalue = value;
				switch (oFormat)
				{
					case OrginFormatType.整数値:
						switch (oFormatS)
						{
							case OrgnFormatStyle.ゼロ詰め:
								this.Text =
									this.CodeValue.PadLeft(intScale, '0');
								break;
							case OrgnFormatStyle.カンマ:

								this.Text =
									string.Format("{0:D" + intScale + "}",
									int.Parse(this.CodeValue));
								break;
						}
						break;
					case OrginFormatType.少数値:
						this.Text =
							string.Format("{0:D" + intScale + "}",
							Decimal.Parse(this.CodeValue));
						break;
					default:

						break;
				}
			}
		}

		public FsiExTextBox() : base()
		{
			if (!string.IsNullOrEmpty(strBisinessLogicClass))
			{

				Type masterType = Type.GetType("jp.co.fsi.common." + strBisinessLogicClass);

				Validator = ((BaseValidate)Activator.CreateInstance(masterType));

			}

		}

		protected override void OnEnter(EventArgs e)
		{
			isChanged = false;

			base.OnEnter(e);
		}

		protected override void OnTextChanged(EventArgs e)
		{
			isChanged = true;
			base.OnTextChanged(e);
		}

		protected override void OnValidating(CancelEventArgs e)
		{

			if (isChanged)
			{
				if (null != Validator)
				{

					this.Validator.Validating(this, e);

				}
			}
			base.OnValidating(e);
		}

	}
}
