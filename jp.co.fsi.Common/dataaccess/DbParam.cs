﻿using System.Data;

namespace jp.co.fsi.common.dataaccess
{
    /// <summary>
    /// DBのバインドパラメータ1件を保持するクラス
    /// </summary>
    class DbParam
    {
        #region プロパティ
        private SqlDbType _type;
        /// <summary>
        /// パラメータの型
        /// </summary>
        public SqlDbType Type
        {
            get
            {
                return this._type;
            }
            set
            {
                this._type = value;
            }
        }

        private int _fullLength;
        /// <summary>
        /// 全体のデータ長
        /// </summary>
        public int FullLength
        {
            get
            {
                return this._fullLength;
            }
            set
            {
                this._fullLength = value;
            }
        }

        private int _decLength;
        /// <summary>
        /// 小数部のデータ長
        /// </summary>
        public int DecLength
        {
            get
            {
                return this._decLength;
            }
            set
            {
                this._decLength = value;
            }
        }

        private object _val;
        /// <summary>
        /// パラメータにセットする値
        /// </summary>
        public object Val
        {
            get
            {
                return this._val;
            }
            set
            {
                this._val = value;
            }
        }
        #endregion
    }
}
