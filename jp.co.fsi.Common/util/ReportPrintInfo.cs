﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text.RegularExpressions;
using System.Text;

namespace jp.co.fsi.common.util
{
    /// <summary>
    /// 帳票の印刷設定を保持するクラスです。
    /// </summary>
    public class ReportPrintInfo
    {
        #region 定数
        /// <summary>
        /// 設定ファイルのファイル名
        /// </summary>
        private const string CONFIG_FILE_NM = "Report.csv";
        #endregion

        #region private変数
        /// <summary>
        /// CSVから取得したメニューの設定内容を保持するDataTable
        /// </summary>
        private DataTable _dtReportInfo;
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ReportPrintInfo()
        {
            LoadData();
        }
        #endregion

        #region publicメソッド
        /// <summary>
        /// 指定した帳票IDから設定を取得します。
        /// ※未設定の場合はエラーとします。
        /// </summary>
        /// <param name="repId">帳票ID</param>
        /// <returns>帳票の設定内容(1行分)</returns>
        public DataRow GetPrintSetting(string repId)
        {
            return GetPrintSetting(repId, true);
        }

        /// <summary>
        /// 指定した帳票IDから設定を取得します。
        /// </summary>
        /// <param name="repId">帳票ID</param>
        /// <param name="onNotExistErr">未設定の場合エラーとするか否か</param>
        /// <returns>帳票の設定内容(1行分)</returns>
        public DataRow GetPrintSetting(string repId, bool onNotExistErr)
        {
            DataRow[] aryDr = this._dtReportInfo.Select("帳票ID = '" + repId + "'");

            if (aryDr.Length == 0)
            {
                if (onNotExistErr)
                {
                    throw new ApplicationException("指定した帳票IDのプリンタ設定が存在しません。"
                        + Environment.NewLine + "印刷設定画面で設定をして下さい。");
                }
                else
                {
                    return null;
                }
            }

            return aryDr[0];
        }

        /// <summary>
        /// プリンタ設定を更新します。
        /// </summary>
        /// <param name="repId">帳票ID</param>
        /// <param name="docName">ドキュメント名</param>
        /// <param name="outPrinter">出力プリンタ(空白：通常使用するプリンタ)</param>
        /// <param name="manualPaper">手差し(空白：クライアントの標準設定、○：手差し)</param>
        /// <param name="colorPrint">カラー印刷(空白：クライアントの標準設定、○：カラー、×：白黒)</param>
        /// <param name="duplex">両面印刷(空白：クライアントの標準設定、○：両面印刷)</param>
        public void SetPrinterSetting(string repId, string docName, string outPrinter,
            string manualPaper, string colorPrint, string duplex)
        {
            DataRow[] aryDr = this._dtReportInfo.Select("帳票ID = '" + repId + "'");

            DataRow drTarget;
            if (aryDr.Length == 0)
            {
                drTarget = this._dtReportInfo.NewRow();
                drTarget["帳票ID"] = repId;
            }
            else
            {
                drTarget = aryDr[0];
            }

            drTarget["ドキュメント名"] = docName;
            drTarget["出力プリンタ"] = outPrinter;
            drTarget["手差し"] = manualPaper;
            drTarget["カラー"] = colorPrint;
            drTarget["両面印刷"] = duplex;

            if (aryDr.Length == 0)
            {
                this._dtReportInfo.Rows.Add(drTarget);
            }
        }

        /// <summary>
        /// 設定を保存する
        /// </summary>
        public void SaveSettings()
        {
            //CSVファイルに書き込むときに使うEncoding
            Encoding enc =
                Encoding.GetEncoding("Shift_JIS");

            //書き込むファイルを開く
            string path = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
            path = Path.Combine(path, CONFIG_FILE_NM);
            StreamWriter sr =
                new StreamWriter(path, false, enc);

            int colCount = this._dtReportInfo.Columns.Count;
            int lastColIndex = colCount - 1;

            //ヘッダを書き込む
            for (int i = 0; i < colCount; i++)
            {
                //ヘッダの取得
                string field = this._dtReportInfo.Columns[i].Caption;
                //"で囲む
                field = EncloseDoubleQuotesIfNeed(field);
                //フィールドを書き込む
                sr.Write(field);
                //カンマを書き込む
                if (lastColIndex > i)
                {
                    sr.Write(',');
                }
            }
            //改行する
            sr.Write(Environment.NewLine);

            //レコードを書き込む
            foreach (DataRow row in this._dtReportInfo.Rows)
            {
                for (int i = 0; i < colCount; i++)
                {
                    //フィールドの取得
                    string field = row[i].ToString();
                    //"で囲む
                    field = EncloseDoubleQuotesIfNeed(field);
                    //フィールドを書き込む
                    sr.Write(field);
                    //カンマを書き込む
                    if (lastColIndex > i)
                    {
                        sr.Write(',');
                    }
                }
                //改行する
                sr.Write(Environment.NewLine);
            }

            //閉じる
            sr.Close();
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// メニュー設定のCSVファイルから設定を読み込む
        /// </summary>
        /// <returns></returns>
        private void LoadData()
        {
            DataTable dtSave = new DataTable();
            dtSave.Columns.Add("帳票ID", typeof(string));
            dtSave.Columns.Add("ドキュメント名", typeof(string));
            dtSave.Columns.Add("出力プリンタ", typeof(string));
            dtSave.Columns.Add("手差し", typeof(string));
            dtSave.Columns.Add("カラー", typeof(string));
            dtSave.Columns.Add("両面印刷", typeof(string));
            dtSave.PrimaryKey = new DataColumn[] { dtSave.Columns["帳票ID"] };
            DataRow drSave = null;

            try
            {
                // 現在のアセンブリが存在するディレクトリを取得
                string path = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
                path = Path.Combine(path, CONFIG_FILE_NM);

                string[] strData; //分解後の文字用変数
                string strLine; //１行分のデータ
                bool fileExists = File.Exists(path);
                bool readTitle = false;
                if (fileExists)
                {
                    StreamReader sr = new StreamReader(path, Encoding.Default);
                    while (sr.Peek() >= 0)
                    {
                        strLine = sr.ReadLine();
                        // 1行目はすっ飛ばし
                        if (!readTitle)
                        {
                            readTitle = true;
                            continue;
                        }

                        strData = CsvToList(strLine)[0];
                        // 行を追加
                        drSave = dtSave.NewRow();
                        drSave["帳票ID"] = strData[0];
                        drSave["ドキュメント名"] = strData[1];
                        drSave["出力プリンタ"] = strData[2];
                        drSave["手差し"] = strData[3];
                        drSave["カラー"] = strData[4];
                        drSave["両面印刷"] = strData[5];
                        dtSave.Rows.Add(drSave);
                    }

                    sr.Close();
                }
            }
            catch (DataException dex)
            {
                throw dex;
            }
            catch (Exception)
            {
                //何もしない(デザイナへの影響を考慮)
                //TODO:出来れば二重キーを処理したい
            }

            this._dtReportInfo = dtSave;
        }

        /// <summary>
        /// CSVをListに変換する
        /// </summary>
        /// <param name="csvText">CSVデータの1行分</param>
        /// <returns>Listオブジェクト</returns>
        private List<string[]> CsvToList(string csvText)
        {
            List<string[]> csvRecords = new List<string[]>();

            //前後の改行を削除しておく
            csvText = csvText.Trim(new char[] { '\r', '\n' });

            //一行取り出すための正規表現
            Regex regLine = new Regex("^.*(?:\\n|$)", RegexOptions.Multiline);

            //1行のCSVから各フィールドを取得するための正規表現
            Regex regCsv =
                new Regex("\\s*(\"(?:[^\"]|\"\")*\"|[^,]*)\\s*,", RegexOptions.None);

            System.Text.RegularExpressions.Match mLine = regLine.Match(csvText);
            while (mLine.Success)
            {
                //一行取り出す
                string line = mLine.Value;
                //改行記号が"で囲まれているか調べる
                while ((CountString(line, "\"") % 2) == 1)
                {
                    mLine = mLine.NextMatch();
                    if (!mLine.Success)
                    {
                        throw new ApplicationException("不正なCSV");
                    }
                    line += mLine.Value;
                }
                //行の最後の改行記号を削除
                line = line.TrimEnd(new char[] { '\r', '\n' });
                //最後に「,」をつける
                line += ",";

                //1つの行からフィールドを取り出す
                List<string> csvFields = new List<string>();
                Match m = regCsv.Match(line);
                while (m.Success)
                {
                    string field = m.Groups[1].Value;
                    //前後の空白を削除
                    field = field.Trim();
                    //"で囲まれている時
                    if (field.StartsWith("\"") && field.EndsWith("\""))
                    {
                        //前後の"を取る
                        field = field.Substring(1, field.Length - 2);
                        //「""」を「"」にする
                        field = field.Replace("\"\"", "\"");
                    }
                    csvFields.Add(field);
                    m = m.NextMatch();
                }

                csvFields.TrimExcess();
                csvRecords.Add(csvFields.ToArray());

                mLine = mLine.NextMatch();
            }

            csvRecords.TrimExcess();
            return csvRecords;
        }

        /// <summary>
        /// 指定された文字列内にある文字列が幾つあるか数える
        /// </summary>
        /// <param name="strInput">strFindが幾つあるか数える文字列</param>
        /// <param name="strFind">数える文字列</param>
        /// <returns>strInput内にstrFindが幾つあったか</returns>
        private int CountString(string strInput, string strFind) {
            int foundCount = 0;
            int sPos = strInput.IndexOf(strFind);
            while(sPos > -1) {
                foundCount++;
                sPos = strInput.IndexOf(strFind, sPos + 1);
            }

            return foundCount;
        }

        /// <summary>
        /// 必要ならば、文字列をダブルクォートで囲む
        /// </summary>
        private string EncloseDoubleQuotesIfNeed(string field)
        {
            if (NeedEncloseDoubleQuotes(field))
            {
                return EncloseDoubleQuotes(field);
            }
            return field;
        }

        /// <summary>
        /// 文字列をダブルクォートで囲む
        /// </summary>
        private string EncloseDoubleQuotes(string field)
        {
            if (field.IndexOf('"') > -1)
            {
                //"を""とする
                field = field.Replace("\"", "\"\"");
            }
            return "\"" + field + "\"";
        }

        /// <summary>
        /// 文字列をダブルクォートで囲む必要があるか調べる
        /// </summary>
        private bool NeedEncloseDoubleQuotes(string field)
        {
            return field.IndexOf('"') > -1 ||
                field.IndexOf(',') > -1 ||
                field.IndexOf('\r') > -1 ||
                field.IndexOf('\n') > -1 ||
                field.StartsWith(" ") ||
                field.StartsWith("\t") ||
                field.EndsWith(" ") ||
                field.EndsWith("\t");
        }
        #endregion
    }
}
