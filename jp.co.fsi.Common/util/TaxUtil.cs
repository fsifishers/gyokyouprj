﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using jp.co.fsi.common.dataaccess;

namespace jp.co.fsi.common.util
{
    public static class TaxUtil
    {
        /// <summary>
        /// 明細転嫁
        /// </summary>        
        public const int SHIFT_CATEGORY_DETAIL = 1;
        /// <summary>
        /// 伝票転嫁
        /// </summary>        
        public const int SHIFT_CATEGORY_SLIP_TOTAL = 2;
        /// <summary>
        /// 請求転嫁
        /// </summary>
        public const int SHIFT_CATEGORY_CLOSE_THE_BOOKS = 3;

        /// <summary>
        /// 税抜き計算無し
        /// </summary>
        public const int INPUT_CATEGORY_FREE = 1;
        /// <summary>
        /// 税抜き計算あり
        /// </summary>
        public const int INPUT_CATEGORY_IMPOSITION = 2;
        /// <summary>
        /// 税込み計算あり
        /// </summary>
        public const int INPUT_CATEGORY_INCLUDED = 3;

        /// <summary>
        /// 消費税転嫁方法
        /// </summary>
        public enum INPUT_CATEGORY
        {
            /// <summary>
            /// 明細転嫁
            /// </summary>
            DETAIL = 1,
            /// <summary>
            /// 伝票転嫁
            /// </summary>
            SLIP_TOTAL = 2,
            /// <summary>
            /// 請求転嫁
            /// </summary>
            CLOSE_THE_BOOKS = 3
        }

        /// <summary>
        /// 消費税入力方法
        /// </summary>
        public enum SHIFT_CATEGORY
        {
            /// <summary>
            /// 税抜き計算無し
            /// </summary>
            FREE = 1,
            /// <summary>
            /// 税抜き計算あり
            /// </summary>
            IMPOSITION = 2,
            /// <summary>
            /// 税込み計算あり
            /// </summary>
            INCLUDED = 3
        }

        /// <summary>
        /// 端数処理
        /// </summary>
        public enum ROUND_CATEGORY
        {
            /// <summary>
            /// 切り捨て
            /// </summary>
            DOWN = 1,
            /// <summary>
            /// 四捨五入
            /// </summary>
            ADJUST = 2,
            /// <summary>
            /// 切り上げ
            /// </summary>
            UP = 3
        }

        /// <summary>
        /// 税率取得
        /// </summary>
        /// <param name="date">適用日付</param>
        /// <param name="ZeiKbn">税区分</param>
        /// <param name="dba">データアクセスクラス</param>
        /// <returns></returns>
        public static decimal GetTaxRate(DateTime date, decimal ZeiKbn, DbAccess dba)
        {
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT dbo.FNC_GetTaxRate( @ZEI_KUBUN, @DENPYO_DATE )");
            dpc.SetParam("@ZEI_KUBUN", SqlDbType.Decimal, 4, ZeiKbn);
            dpc.SetParam("@DENPYO_DATE", SqlDbType.DateTime, date);
            DataTable dt = dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);
            if (dt.Rows.Count == 0)
                return 0;
            else
                return ToDecimal(dt.Rows[0][0]);
        }

        public static string ToEmpty(object value)
        {
            if (value == null || value.ToString() == string.Empty)
                return string.Empty;

            return Convert.ToString(value);
        }

        public static Decimal ToDecimal(object value)
        {
            return ToDecimal(ToEmpty(value));
        }

        public static Decimal ToDecimal(string value)
        {
            Decimal data = 0m;
            if (string.IsNullOrWhiteSpace(value) || !Decimal.TryParse(value, out data))
            {
                return 0m;
            }
            else
            {
                return data;
            }
        }

        /// <summary>
        /// 端数処理
        /// </summary>
        /// <param name="num">対象値</param>
        /// <param name="pos">丸め位置</param>
        /// <param name="hasuKbn">処理区分</param>
        /// <returns></returns>
        public static decimal CalcFraction(decimal num, int pos, ROUND_CATEGORY hasuKbn)
        {
            return CalcFraction(num, pos, (int)hasuKbn);
        }
        /// <summary>
        /// 端数処理
        /// </summary>
        /// <param name="num">対象値</param>
        /// <param name="pos">丸め位置</param>
        /// <param name="hasuKbn">処理区分(1:切り捨て、2:四捨五入、3:切り上げ)</param>
        /// <returns></returns>
        public static decimal CalcFraction(decimal num, int pos, int hasuKbn)
        {
            bool minas = false;
            decimal abdNum = 0;
            decimal wrkNum = 1;
            decimal retNum = 0;

            if (num < 0)
                minas = true;

            abdNum = Math.Abs(num);

            // 編集位置
            if (pos < 0)
            {
                for (int i = 0; i < Math.Abs(pos); i++)
                    wrkNum *= 10;
                abdNum *= wrkNum;
            }
            else if (pos > 0)
            {
                for (int i = 0; i < Math.Abs(pos); i++)
                    wrkNum *= 10;
                abdNum /= wrkNum;
            }

            if (hasuKbn == 1)
            {
                // 切り捨て
                retNum = ToRoundDown(abdNum, 0);
            }
            else if (hasuKbn == 2)
            {
                // 四捨五入
                retNum = ToHalfAdjust(abdNum, 0);
            }
            else if (hasuKbn == 3)
            {
                // 切り上げ
                retNum = ToRoundUp(abdNum, 0);
            }
            else
            {
                retNum = ToRoundDown(abdNum, 0);
            }

            // 編集位置
            if (pos < 0)
            {
                retNum /= wrkNum;
            }
            else if (pos > 0)
            {
                retNum *= wrkNum;
            }

            if (minas)
                retNum *= -1;

            return retNum;
        }

        private static decimal ToRoundDown(decimal val, int digits)
        {
            decimal pos = new decimal(Math.Pow(10.0, (double)digits));
            if (val > 0)
                return decimal.Floor(val * pos) / pos;
            else
                return decimal.Ceiling(val * pos) / pos;
        }

        private static decimal ToHalfAdjust(decimal val, int digits)
        {
            return decimal.Round(val, digits, MidpointRounding.AwayFromZero);
        }

        private static decimal ToRoundUp(decimal val, int digits)
        {
            decimal pos = new decimal(Math.Pow(10.0, (double)digits));
            if (val > 0)
                return decimal.Ceiling(val * pos) / pos;
            else
                return decimal.Floor(val * pos) / pos;
        }
    }
}
