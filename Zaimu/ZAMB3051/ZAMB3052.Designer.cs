﻿namespace jp.co.fsi.zam.zamb3051
{
    partial class ZAMB3052
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvList = new System.Windows.Forms.DataGridView();
            this.grpOpt = new System.Windows.Forms.GroupBox();
            this.rdoOpt2 = new System.Windows.Forms.RadioButton();
            this.rdoOpt1 = new System.Windows.Forms.RadioButton();
            this.pnlDebug.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).BeginInit();
            this.grpOpt.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Text = "年次繰越[テーブル設定]";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            // 
            // dgvList
            // 
            this.dgvList.AllowUserToAddRows = false;
            this.dgvList.AllowUserToDeleteRows = false;
            this.dgvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvList.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvList.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.dgvList.Location = new System.Drawing.Point(12, 91);
            this.dgvList.MultiSelect = false;
            this.dgvList.Name = "dgvList";
            this.dgvList.ReadOnly = true;
            this.dgvList.RowHeadersVisible = false;
            this.dgvList.RowTemplate.Height = 21;
            this.dgvList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvList.Size = new System.Drawing.Size(381, 317);
            this.dgvList.TabIndex = 0;
            // 
            // grpOpt
            // 
            this.grpOpt.BackColor = System.Drawing.Color.LightGray;
            this.grpOpt.Controls.Add(this.rdoOpt2);
            this.grpOpt.Controls.Add(this.rdoOpt1);
            this.grpOpt.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.grpOpt.Location = new System.Drawing.Point(12, 45);
            this.grpOpt.Name = "grpOpt";
            this.grpOpt.Size = new System.Drawing.Size(271, 42);
            this.grpOpt.TabIndex = 0;
            this.grpOpt.TabStop = false;
            this.grpOpt.Text = "重複時の処理";
            // 
            // rdoOpt2
            // 
            this.rdoOpt2.AutoSize = true;
            this.rdoOpt2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoOpt2.Location = new System.Drawing.Point(143, 17);
            this.rdoOpt2.Name = "rdoOpt2";
            this.rdoOpt2.Size = new System.Drawing.Size(109, 17);
            this.rdoOpt2.TabIndex = 1;
            this.rdoOpt2.TabStop = true;
            this.rdoOpt2.Text = "上書きしない";
            this.rdoOpt2.UseVisualStyleBackColor = true;
            // 
            // rdoOpt1
            // 
            this.rdoOpt1.AutoSize = true;
            this.rdoOpt1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoOpt1.Location = new System.Drawing.Point(16, 17);
            this.rdoOpt1.Name = "rdoOpt1";
            this.rdoOpt1.Size = new System.Drawing.Size(95, 17);
            this.rdoOpt1.TabIndex = 0;
            this.rdoOpt1.TabStop = true;
            this.rdoOpt1.Text = "上書きする";
            this.rdoOpt1.UseVisualStyleBackColor = true;
            // 
            // ZAMB3052
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.grpOpt);
            this.Controls.Add(this.dgvList);
            this.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.Name = "ZAMB3052";
            this.ShowFButton = true;
            this.Text = "年次繰越";
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.dgvList, 0);
            this.Controls.SetChildIndex(this.grpOpt, 0);
            this.pnlDebug.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).EndInit();
            this.grpOpt.ResumeLayout(false);
            this.grpOpt.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvList;
        private System.Windows.Forms.GroupBox grpOpt;
        private System.Windows.Forms.RadioButton rdoOpt2;
        private System.Windows.Forms.RadioButton rdoOpt1;

    }
}