﻿namespace jp.co.fsi.zm.zmdr1031
{
    partial class ZMDR1032
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblGenkin = new System.Windows.Forms.Label();
            this.lblKanjoKamoku = new System.Windows.Forms.Label();
            this.txtKanjoKamoku = new System.Windows.Forms.TextBox();
            this.lblKanjoKamokuResults = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Text = "";
            // 
            // lblGenkin
            // 
            this.lblGenkin.BackColor = System.Drawing.Color.Silver;
            this.lblGenkin.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGenkin.Location = new System.Drawing.Point(12, 81);
            this.lblGenkin.Name = "lblGenkin";
            this.lblGenkin.Size = new System.Drawing.Size(82, 22);
            this.lblGenkin.TabIndex = 2;
            this.lblGenkin.Text = "設定コード";
            this.lblGenkin.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblKanjoKamoku
            // 
            this.lblKanjoKamoku.BackColor = System.Drawing.Color.Silver;
            this.lblKanjoKamoku.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKanjoKamoku.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKanjoKamoku.Location = new System.Drawing.Point(96, 55);
            this.lblKanjoKamoku.Name = "lblKanjoKamoku";
            this.lblKanjoKamoku.Size = new System.Drawing.Size(243, 23);
            this.lblKanjoKamoku.TabIndex = 1;
            this.lblKanjoKamoku.Text = "勘　定　科　目";
            this.lblKanjoKamoku.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtKanjoKamoku
            // 
            this.txtKanjoKamoku.Font = new System.Drawing.Font("ＭＳ ゴシック", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKanjoKamoku.Location = new System.Drawing.Point(96, 81);
            this.txtKanjoKamoku.MaxLength = 6;
            this.txtKanjoKamoku.Name = "txtKanjoKamoku";
            this.txtKanjoKamoku.Size = new System.Drawing.Size(53, 22);
            this.txtKanjoKamoku.TabIndex = 3;
            this.txtKanjoKamoku.Text = "123456";
            this.txtKanjoKamoku.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKanjoKamoku.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtKanjoKamoku_KeyDown);
            this.txtKanjoKamoku.Validating += new System.ComponentModel.CancelEventHandler(this.txtKanjoKamoku_Validating);
            // 
            // lblKanjoKamokuResults
            // 
            this.lblKanjoKamokuResults.BackColor = System.Drawing.Color.Silver;
            this.lblKanjoKamokuResults.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKanjoKamokuResults.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKanjoKamokuResults.Location = new System.Drawing.Point(151, 81);
            this.lblKanjoKamokuResults.Name = "lblKanjoKamokuResults";
            this.lblKanjoKamokuResults.Size = new System.Drawing.Size(188, 22);
            this.lblKanjoKamokuResults.TabIndex = 4;
            this.lblKanjoKamokuResults.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ZMDR1032
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(823, 638);
            this.Controls.Add(this.lblKanjoKamokuResults);
            this.Controls.Add(this.txtKanjoKamoku);
            this.Controls.Add(this.lblKanjoKamoku);
            this.Controls.Add(this.lblGenkin);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "ZMDR1032";
            this.ShowFButton = true;
            this.Text = "";
            this.Controls.SetChildIndex(this.lblGenkin, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.lblKanjoKamoku, 0);
            this.Controls.SetChildIndex(this.txtKanjoKamoku, 0);
            this.Controls.SetChildIndex(this.lblKanjoKamokuResults, 0);
            this.pnlDebug.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblGenkin;
        private System.Windows.Forms.Label lblKanjoKamoku;
        private System.Windows.Forms.TextBox txtKanjoKamoku;
        private System.Windows.Forms.Label lblKanjoKamokuResults;




    }
}