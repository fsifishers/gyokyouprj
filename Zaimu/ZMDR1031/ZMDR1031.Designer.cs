﻿namespace jp.co.fsi.zm.zmdr1031
{
    partial class ZMDR1031
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblFr = new System.Windows.Forms.Label();
            this.lblGengoFr = new System.Windows.Forms.Label();
            this.txtMonthFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtYearFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDayFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblYearFr = new System.Windows.Forms.Label();
            this.lblMonthFr = new System.Windows.Forms.Label();
            this.lblDayFr = new System.Windows.Forms.Label();
            this.gbxDate = new System.Windows.Forms.GroupBox();
            this.lblCodeBetDate = new System.Windows.Forms.Label();
            this.lblDayTo = new System.Windows.Forms.Label();
            this.lblMonthTo = new System.Windows.Forms.Label();
            this.lblYearTo = new System.Windows.Forms.Label();
            this.txtDayTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtYearTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtMonthTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGengoTo = new System.Windows.Forms.Label();
            this.lblTo = new System.Windows.Forms.Label();
            this.gbxShiwakeShurui = new System.Windows.Forms.GroupBox();
            this.rdoZenShiwake = new System.Windows.Forms.RadioButton();
            this.rdoTujoShiwake = new System.Windows.Forms.RadioButton();
            this.rdoKessanShiwake = new System.Windows.Forms.RadioButton();
            this.gbxJuni = new System.Windows.Forms.GroupBox();
            this.rdoDateJun = new System.Windows.Forms.RadioButton();
            this.rdoBangoJun = new System.Windows.Forms.RadioButton();
            this.gbxDenpyoBango = new System.Windows.Forms.GroupBox();
            this.lblDenpyoBangoBet = new System.Windows.Forms.Label();
            this.txtDenpyoBangoTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblDenpyoBangoTo = new System.Windows.Forms.Label();
            this.txtDenpyoBangoFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblDenpyoBangoFr = new System.Windows.Forms.Label();
            this.gbxTantosha = new System.Windows.Forms.GroupBox();
            this.lblTantoshaBet = new System.Windows.Forms.Label();
            this.txtTantoshaTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblTantoshaTo = new System.Windows.Forms.Label();
            this.txtTantoshaFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblTantoshaFr = new System.Windows.Forms.Label();
            this.gbxKanjoKamoku = new System.Windows.Forms.GroupBox();
            this.lblKanjoKamokuBet = new System.Windows.Forms.Label();
            this.txtKanjoKamokuTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKanjoKamokuTo = new System.Windows.Forms.Label();
            this.txtKanjoKamokuFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKanjoKamokuFr = new System.Windows.Forms.Label();
            this.gbxBumon = new System.Windows.Forms.GroupBox();
            this.lblBumonBet = new System.Windows.Forms.Label();
            this.txtBumonTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblBumonTo = new System.Windows.Forms.Label();
            this.txtBumonFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblBumonFr = new System.Windows.Forms.Label();
            this.gbxShuturyokuDenpyo = new System.Windows.Forms.GroupBox();
            this.rdoKamokuGokei = new System.Windows.Forms.RadioButton();
            this.rdoFurikaeDenpyo = new System.Windows.Forms.RadioButton();
            this.rdoNyukinDenpyo = new System.Windows.Forms.RadioButton();
            this.rdoShukkinDenpyo = new System.Windows.Forms.RadioButton();
            this.gbxMizuageShisho = new System.Windows.Forms.GroupBox();
            this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMizuageShishoNm = new System.Windows.Forms.Label();
            this.lblMizuageShisho = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.gbxDate.SuspendLayout();
            this.gbxShiwakeShurui.SuspendLayout();
            this.gbxJuni.SuspendLayout();
            this.gbxDenpyoBango.SuspendLayout();
            this.gbxTantosha.SuspendLayout();
            this.gbxKanjoKamoku.SuspendLayout();
            this.gbxBumon.SuspendLayout();
            this.gbxShuturyokuDenpyo.SuspendLayout();
            this.gbxMizuageShisho.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Text = "";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            // 
            // lblFr
            // 
            this.lblFr.BackColor = System.Drawing.Color.Silver;
            this.lblFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFr.Location = new System.Drawing.Point(17, 24);
            this.lblFr.Name = "lblFr";
            this.lblFr.Size = new System.Drawing.Size(199, 29);
            this.lblFr.TabIndex = 11;
            // 
            // lblGengoFr
            // 
            this.lblGengoFr.BackColor = System.Drawing.Color.Silver;
            this.lblGengoFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengoFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengoFr.Location = new System.Drawing.Point(19, 28);
            this.lblGengoFr.Name = "lblGengoFr";
            this.lblGengoFr.Size = new System.Drawing.Size(41, 21);
            this.lblGengoFr.TabIndex = 12;
            this.lblGengoFr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtMonthFr
            // 
            this.txtMonthFr.AutoSizeFromLength = false;
            this.txtMonthFr.DisplayLength = null;
            this.txtMonthFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMonthFr.Location = new System.Drawing.Point(112, 28);
            this.txtMonthFr.MaxLength = 2;
            this.txtMonthFr.Name = "txtMonthFr";
            this.txtMonthFr.Size = new System.Drawing.Size(30, 20);
            this.txtMonthFr.TabIndex = 15;
            this.txtMonthFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonthFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthFr_Validating);
            // 
            // txtYearFr
            // 
            this.txtYearFr.AutoSizeFromLength = false;
            this.txtYearFr.DisplayLength = null;
            this.txtYearFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtYearFr.Location = new System.Drawing.Point(62, 28);
            this.txtYearFr.MaxLength = 2;
            this.txtYearFr.Name = "txtYearFr";
            this.txtYearFr.Size = new System.Drawing.Size(30, 20);
            this.txtYearFr.TabIndex = 13;
            this.txtYearFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtYearFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtYearFr_Validating);
            // 
            // txtDayFr
            // 
            this.txtDayFr.AutoSizeFromLength = false;
            this.txtDayFr.DisplayLength = null;
            this.txtDayFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDayFr.Location = new System.Drawing.Point(162, 28);
            this.txtDayFr.MaxLength = 2;
            this.txtDayFr.Name = "txtDayFr";
            this.txtDayFr.Size = new System.Drawing.Size(30, 20);
            this.txtDayFr.TabIndex = 17;
            this.txtDayFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDayFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDayFr_Validating);
            // 
            // lblYearFr
            // 
            this.lblYearFr.BackColor = System.Drawing.Color.Silver;
            this.lblYearFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYearFr.Location = new System.Drawing.Point(94, 28);
            this.lblYearFr.Name = "lblYearFr";
            this.lblYearFr.Size = new System.Drawing.Size(17, 21);
            this.lblYearFr.TabIndex = 14;
            this.lblYearFr.Text = "年";
            this.lblYearFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMonthFr
            // 
            this.lblMonthFr.BackColor = System.Drawing.Color.Silver;
            this.lblMonthFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMonthFr.Location = new System.Drawing.Point(144, 28);
            this.lblMonthFr.Name = "lblMonthFr";
            this.lblMonthFr.Size = new System.Drawing.Size(15, 19);
            this.lblMonthFr.TabIndex = 16;
            this.lblMonthFr.Text = "月";
            this.lblMonthFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDayFr
            // 
            this.lblDayFr.BackColor = System.Drawing.Color.Silver;
            this.lblDayFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDayFr.Location = new System.Drawing.Point(194, 28);
            this.lblDayFr.Name = "lblDayFr";
            this.lblDayFr.Size = new System.Drawing.Size(20, 18);
            this.lblDayFr.TabIndex = 18;
            this.lblDayFr.Text = "日";
            this.lblDayFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gbxDate
            // 
            this.gbxDate.Controls.Add(this.lblCodeBetDate);
            this.gbxDate.Controls.Add(this.lblDayTo);
            this.gbxDate.Controls.Add(this.lblMonthTo);
            this.gbxDate.Controls.Add(this.lblYearTo);
            this.gbxDate.Controls.Add(this.txtDayTo);
            this.gbxDate.Controls.Add(this.txtYearTo);
            this.gbxDate.Controls.Add(this.txtMonthTo);
            this.gbxDate.Controls.Add(this.lblGengoTo);
            this.gbxDate.Controls.Add(this.lblTo);
            this.gbxDate.Controls.Add(this.lblDayFr);
            this.gbxDate.Controls.Add(this.lblMonthFr);
            this.gbxDate.Controls.Add(this.lblYearFr);
            this.gbxDate.Controls.Add(this.txtDayFr);
            this.gbxDate.Controls.Add(this.txtYearFr);
            this.gbxDate.Controls.Add(this.txtMonthFr);
            this.gbxDate.Controls.Add(this.lblGengoFr);
            this.gbxDate.Controls.Add(this.lblFr);
            this.gbxDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxDate.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gbxDate.Location = new System.Drawing.Point(12, 229);
            this.gbxDate.Name = "gbxDate";
            this.gbxDate.Size = new System.Drawing.Size(483, 67);
            this.gbxDate.TabIndex = 10;
            this.gbxDate.TabStop = false;
            this.gbxDate.Text = "伝票日付範囲";
            // 
            // lblCodeBetDate
            // 
            this.lblCodeBetDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCodeBetDate.Location = new System.Drawing.Point(220, 28);
            this.lblCodeBetDate.Name = "lblCodeBetDate";
            this.lblCodeBetDate.Size = new System.Drawing.Size(17, 22);
            this.lblCodeBetDate.TabIndex = 19;
            this.lblCodeBetDate.Text = "～";
            this.lblCodeBetDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDayTo
            // 
            this.lblDayTo.BackColor = System.Drawing.Color.Silver;
            this.lblDayTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDayTo.Location = new System.Drawing.Point(423, 28);
            this.lblDayTo.Name = "lblDayTo";
            this.lblDayTo.Size = new System.Drawing.Size(20, 18);
            this.lblDayTo.TabIndex = 27;
            this.lblDayTo.Text = "日";
            this.lblDayTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMonthTo
            // 
            this.lblMonthTo.BackColor = System.Drawing.Color.Silver;
            this.lblMonthTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMonthTo.Location = new System.Drawing.Point(373, 28);
            this.lblMonthTo.Name = "lblMonthTo";
            this.lblMonthTo.Size = new System.Drawing.Size(15, 19);
            this.lblMonthTo.TabIndex = 25;
            this.lblMonthTo.Text = "月";
            this.lblMonthTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblYearTo
            // 
            this.lblYearTo.BackColor = System.Drawing.Color.Silver;
            this.lblYearTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYearTo.Location = new System.Drawing.Point(323, 28);
            this.lblYearTo.Name = "lblYearTo";
            this.lblYearTo.Size = new System.Drawing.Size(17, 21);
            this.lblYearTo.TabIndex = 23;
            this.lblYearTo.Text = "年";
            this.lblYearTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDayTo
            // 
            this.txtDayTo.AutoSizeFromLength = false;
            this.txtDayTo.DisplayLength = null;
            this.txtDayTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDayTo.Location = new System.Drawing.Point(391, 28);
            this.txtDayTo.MaxLength = 2;
            this.txtDayTo.Name = "txtDayTo";
            this.txtDayTo.Size = new System.Drawing.Size(30, 20);
            this.txtDayTo.TabIndex = 26;
            this.txtDayTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDayTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDayTo_Validating);
            // 
            // txtYearTo
            // 
            this.txtYearTo.AutoSizeFromLength = false;
            this.txtYearTo.DisplayLength = null;
            this.txtYearTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtYearTo.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtYearTo.Location = new System.Drawing.Point(291, 28);
            this.txtYearTo.MaxLength = 2;
            this.txtYearTo.Name = "txtYearTo";
            this.txtYearTo.Size = new System.Drawing.Size(30, 20);
            this.txtYearTo.TabIndex = 22;
            this.txtYearTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtYearTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtYearTo_Validating);
            // 
            // txtMonthTo
            // 
            this.txtMonthTo.AutoSizeFromLength = false;
            this.txtMonthTo.DisplayLength = null;
            this.txtMonthTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMonthTo.Location = new System.Drawing.Point(341, 28);
            this.txtMonthTo.MaxLength = 2;
            this.txtMonthTo.Name = "txtMonthTo";
            this.txtMonthTo.Size = new System.Drawing.Size(30, 20);
            this.txtMonthTo.TabIndex = 24;
            this.txtMonthTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonthTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthTo_Validating);
            // 
            // lblGengoTo
            // 
            this.lblGengoTo.BackColor = System.Drawing.Color.Silver;
            this.lblGengoTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengoTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengoTo.Location = new System.Drawing.Point(248, 28);
            this.lblGengoTo.Name = "lblGengoTo";
            this.lblGengoTo.Size = new System.Drawing.Size(41, 21);
            this.lblGengoTo.TabIndex = 21;
            this.lblGengoTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTo
            // 
            this.lblTo.BackColor = System.Drawing.Color.Silver;
            this.lblTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTo.Location = new System.Drawing.Point(246, 24);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(199, 29);
            this.lblTo.TabIndex = 20;
            // 
            // gbxShiwakeShurui
            // 
            this.gbxShiwakeShurui.Controls.Add(this.rdoZenShiwake);
            this.gbxShiwakeShurui.Controls.Add(this.rdoTujoShiwake);
            this.gbxShiwakeShurui.Controls.Add(this.rdoKessanShiwake);
            this.gbxShiwakeShurui.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxShiwakeShurui.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gbxShiwakeShurui.Location = new System.Drawing.Point(12, 173);
            this.gbxShiwakeShurui.Name = "gbxShiwakeShurui";
            this.gbxShiwakeShurui.Size = new System.Drawing.Size(290, 50);
            this.gbxShiwakeShurui.TabIndex = 6;
            this.gbxShiwakeShurui.TabStop = false;
            this.gbxShiwakeShurui.Text = "仕訳種類";
            // 
            // rdoZenShiwake
            // 
            this.rdoZenShiwake.AutoSize = true;
            this.rdoZenShiwake.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoZenShiwake.Location = new System.Drawing.Point(191, 20);
            this.rdoZenShiwake.Name = "rdoZenShiwake";
            this.rdoZenShiwake.Size = new System.Drawing.Size(67, 17);
            this.rdoZenShiwake.TabIndex = 9;
            this.rdoZenShiwake.TabStop = true;
            this.rdoZenShiwake.Text = "全仕訳";
            this.rdoZenShiwake.UseVisualStyleBackColor = true;
            // 
            // rdoTujoShiwake
            // 
            this.rdoTujoShiwake.AutoSize = true;
            this.rdoTujoShiwake.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoTujoShiwake.Location = new System.Drawing.Point(14, 20);
            this.rdoTujoShiwake.Name = "rdoTujoShiwake";
            this.rdoTujoShiwake.Size = new System.Drawing.Size(81, 17);
            this.rdoTujoShiwake.TabIndex = 7;
            this.rdoTujoShiwake.TabStop = true;
            this.rdoTujoShiwake.Text = "通常仕訳";
            this.rdoTujoShiwake.UseVisualStyleBackColor = true;
            // 
            // rdoKessanShiwake
            // 
            this.rdoKessanShiwake.AutoSize = true;
            this.rdoKessanShiwake.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoKessanShiwake.Location = new System.Drawing.Point(101, 20);
            this.rdoKessanShiwake.Name = "rdoKessanShiwake";
            this.rdoKessanShiwake.Size = new System.Drawing.Size(81, 17);
            this.rdoKessanShiwake.TabIndex = 8;
            this.rdoKessanShiwake.TabStop = true;
            this.rdoKessanShiwake.Text = "決算仕訳";
            this.rdoKessanShiwake.UseVisualStyleBackColor = true;
            // 
            // gbxJuni
            // 
            this.gbxJuni.Controls.Add(this.rdoDateJun);
            this.gbxJuni.Controls.Add(this.rdoBangoJun);
            this.gbxJuni.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxJuni.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gbxJuni.Location = new System.Drawing.Point(286, 300);
            this.gbxJuni.Name = "gbxJuni";
            this.gbxJuni.Size = new System.Drawing.Size(209, 50);
            this.gbxJuni.TabIndex = 34;
            this.gbxJuni.TabStop = false;
            this.gbxJuni.Text = "出力順位";
            // 
            // rdoDateJun
            // 
            this.rdoDateJun.AutoSize = true;
            this.rdoDateJun.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoDateJun.Location = new System.Drawing.Point(10, 20);
            this.rdoDateJun.Name = "rdoDateJun";
            this.rdoDateJun.Size = new System.Drawing.Size(95, 17);
            this.rdoDateJun.TabIndex = 35;
            this.rdoDateJun.TabStop = true;
            this.rdoDateJun.Text = "伝票日付順";
            this.rdoDateJun.UseVisualStyleBackColor = true;
            // 
            // rdoBangoJun
            // 
            this.rdoBangoJun.AutoSize = true;
            this.rdoBangoJun.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoBangoJun.Location = new System.Drawing.Point(105, 20);
            this.rdoBangoJun.Name = "rdoBangoJun";
            this.rdoBangoJun.Size = new System.Drawing.Size(95, 17);
            this.rdoBangoJun.TabIndex = 36;
            this.rdoBangoJun.TabStop = true;
            this.rdoBangoJun.Text = "伝票番号順";
            this.rdoBangoJun.UseVisualStyleBackColor = true;
            // 
            // gbxDenpyoBango
            // 
            this.gbxDenpyoBango.Controls.Add(this.lblDenpyoBangoBet);
            this.gbxDenpyoBango.Controls.Add(this.txtDenpyoBangoTo);
            this.gbxDenpyoBango.Controls.Add(this.lblDenpyoBangoTo);
            this.gbxDenpyoBango.Controls.Add(this.txtDenpyoBangoFr);
            this.gbxDenpyoBango.Controls.Add(this.lblDenpyoBangoFr);
            this.gbxDenpyoBango.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxDenpyoBango.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gbxDenpyoBango.Location = new System.Drawing.Point(12, 300);
            this.gbxDenpyoBango.Name = "gbxDenpyoBango";
            this.gbxDenpyoBango.Size = new System.Drawing.Size(268, 50);
            this.gbxDenpyoBango.TabIndex = 28;
            this.gbxDenpyoBango.TabStop = false;
            this.gbxDenpyoBango.Text = "伝票番号範囲";
            // 
            // lblDenpyoBangoBet
            // 
            this.lblDenpyoBangoBet.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDenpyoBangoBet.Location = new System.Drawing.Point(123, 20);
            this.lblDenpyoBangoBet.Name = "lblDenpyoBangoBet";
            this.lblDenpyoBangoBet.Size = new System.Drawing.Size(17, 22);
            this.lblDenpyoBangoBet.TabIndex = 31;
            this.lblDenpyoBangoBet.Text = "～";
            this.lblDenpyoBangoBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDenpyoBangoTo
            // 
            this.txtDenpyoBangoTo.AutoSizeFromLength = false;
            this.txtDenpyoBangoTo.DisplayLength = null;
            this.txtDenpyoBangoTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDenpyoBangoTo.Location = new System.Drawing.Point(141, 20);
            this.txtDenpyoBangoTo.MaxLength = 6;
            this.txtDenpyoBangoTo.Name = "txtDenpyoBangoTo";
            this.txtDenpyoBangoTo.Size = new System.Drawing.Size(50, 20);
            this.txtDenpyoBangoTo.TabIndex = 32;
            this.txtDenpyoBangoTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDenpyoBangoTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDenpyoBangoTo_Validating);
            // 
            // lblDenpyoBangoTo
            // 
            this.lblDenpyoBangoTo.BackColor = System.Drawing.Color.Silver;
            this.lblDenpyoBangoTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDenpyoBangoTo.Location = new System.Drawing.Point(193, 20);
            this.lblDenpyoBangoTo.Name = "lblDenpyoBangoTo";
            this.lblDenpyoBangoTo.Size = new System.Drawing.Size(58, 20);
            this.lblDenpyoBangoTo.TabIndex = 33;
            this.lblDenpyoBangoTo.Text = "最　後";
            this.lblDenpyoBangoTo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtDenpyoBangoFr
            // 
            this.txtDenpyoBangoFr.AutoSizeFromLength = false;
            this.txtDenpyoBangoFr.DisplayLength = null;
            this.txtDenpyoBangoFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDenpyoBangoFr.Location = new System.Drawing.Point(13, 20);
            this.txtDenpyoBangoFr.MaxLength = 6;
            this.txtDenpyoBangoFr.Name = "txtDenpyoBangoFr";
            this.txtDenpyoBangoFr.Size = new System.Drawing.Size(50, 20);
            this.txtDenpyoBangoFr.TabIndex = 29;
            this.txtDenpyoBangoFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDenpyoBangoFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDenpyoBangoFr_Validating);
            // 
            // lblDenpyoBangoFr
            // 
            this.lblDenpyoBangoFr.BackColor = System.Drawing.Color.Silver;
            this.lblDenpyoBangoFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDenpyoBangoFr.Location = new System.Drawing.Point(65, 20);
            this.lblDenpyoBangoFr.Name = "lblDenpyoBangoFr";
            this.lblDenpyoBangoFr.Size = new System.Drawing.Size(58, 20);
            this.lblDenpyoBangoFr.TabIndex = 30;
            this.lblDenpyoBangoFr.Text = "先　頭";
            this.lblDenpyoBangoFr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // gbxTantosha
            // 
            this.gbxTantosha.Controls.Add(this.lblTantoshaBet);
            this.gbxTantosha.Controls.Add(this.txtTantoshaTo);
            this.gbxTantosha.Controls.Add(this.lblTantoshaTo);
            this.gbxTantosha.Controls.Add(this.txtTantoshaFr);
            this.gbxTantosha.Controls.Add(this.lblTantoshaFr);
            this.gbxTantosha.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxTantosha.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gbxTantosha.Location = new System.Drawing.Point(12, 361);
            this.gbxTantosha.Name = "gbxTantosha";
            this.gbxTantosha.Size = new System.Drawing.Size(483, 50);
            this.gbxTantosha.TabIndex = 37;
            this.gbxTantosha.TabStop = false;
            this.gbxTantosha.Text = "担当者範囲";
            // 
            // lblTantoshaBet
            // 
            this.lblTantoshaBet.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTantoshaBet.Location = new System.Drawing.Point(219, 20);
            this.lblTantoshaBet.Name = "lblTantoshaBet";
            this.lblTantoshaBet.Size = new System.Drawing.Size(17, 22);
            this.lblTantoshaBet.TabIndex = 40;
            this.lblTantoshaBet.Text = "～";
            this.lblTantoshaBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTantoshaTo
            // 
            this.txtTantoshaTo.AutoSizeFromLength = false;
            this.txtTantoshaTo.DisplayLength = null;
            this.txtTantoshaTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTantoshaTo.Location = new System.Drawing.Point(247, 20);
            this.txtTantoshaTo.MaxLength = 4;
            this.txtTantoshaTo.Name = "txtTantoshaTo";
            this.txtTantoshaTo.Size = new System.Drawing.Size(50, 20);
            this.txtTantoshaTo.TabIndex = 41;
            this.txtTantoshaTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTantoshaTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtTantoshaTo_Validating);
            // 
            // lblTantoshaTo
            // 
            this.lblTantoshaTo.BackColor = System.Drawing.Color.Silver;
            this.lblTantoshaTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTantoshaTo.Location = new System.Drawing.Point(299, 20);
            this.lblTantoshaTo.Name = "lblTantoshaTo";
            this.lblTantoshaTo.Size = new System.Drawing.Size(142, 20);
            this.lblTantoshaTo.TabIndex = 42;
            this.lblTantoshaTo.Text = "最　後";
            this.lblTantoshaTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTantoshaFr
            // 
            this.txtTantoshaFr.AutoSizeFromLength = false;
            this.txtTantoshaFr.DisplayLength = null;
            this.txtTantoshaFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTantoshaFr.Location = new System.Drawing.Point(17, 20);
            this.txtTantoshaFr.MaxLength = 4;
            this.txtTantoshaFr.Name = "txtTantoshaFr";
            this.txtTantoshaFr.Size = new System.Drawing.Size(50, 20);
            this.txtTantoshaFr.TabIndex = 38;
            this.txtTantoshaFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTantoshaFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtTantoshaFr_Validating);
            // 
            // lblTantoshaFr
            // 
            this.lblTantoshaFr.BackColor = System.Drawing.Color.Silver;
            this.lblTantoshaFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTantoshaFr.Location = new System.Drawing.Point(69, 20);
            this.lblTantoshaFr.Name = "lblTantoshaFr";
            this.lblTantoshaFr.Size = new System.Drawing.Size(143, 20);
            this.lblTantoshaFr.TabIndex = 39;
            this.lblTantoshaFr.Text = "先　頭";
            this.lblTantoshaFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gbxKanjoKamoku
            // 
            this.gbxKanjoKamoku.Controls.Add(this.lblKanjoKamokuBet);
            this.gbxKanjoKamoku.Controls.Add(this.txtKanjoKamokuTo);
            this.gbxKanjoKamoku.Controls.Add(this.lblKanjoKamokuTo);
            this.gbxKanjoKamoku.Controls.Add(this.txtKanjoKamokuFr);
            this.gbxKanjoKamoku.Controls.Add(this.lblKanjoKamokuFr);
            this.gbxKanjoKamoku.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxKanjoKamoku.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gbxKanjoKamoku.Location = new System.Drawing.Point(12, 422);
            this.gbxKanjoKamoku.Name = "gbxKanjoKamoku";
            this.gbxKanjoKamoku.Size = new System.Drawing.Size(483, 50);
            this.gbxKanjoKamoku.TabIndex = 43;
            this.gbxKanjoKamoku.TabStop = false;
            this.gbxKanjoKamoku.Text = "勘定科目範囲";
            // 
            // lblKanjoKamokuBet
            // 
            this.lblKanjoKamokuBet.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKanjoKamokuBet.Location = new System.Drawing.Point(222, 20);
            this.lblKanjoKamokuBet.Name = "lblKanjoKamokuBet";
            this.lblKanjoKamokuBet.Size = new System.Drawing.Size(17, 22);
            this.lblKanjoKamokuBet.TabIndex = 46;
            this.lblKanjoKamokuBet.Text = "～";
            this.lblKanjoKamokuBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKanjoKamokuTo
            // 
            this.txtKanjoKamokuTo.AutoSizeFromLength = false;
            this.txtKanjoKamokuTo.DisplayLength = null;
            this.txtKanjoKamokuTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKanjoKamokuTo.Location = new System.Drawing.Point(250, 20);
            this.txtKanjoKamokuTo.MaxLength = 6;
            this.txtKanjoKamokuTo.Name = "txtKanjoKamokuTo";
            this.txtKanjoKamokuTo.Size = new System.Drawing.Size(50, 20);
            this.txtKanjoKamokuTo.TabIndex = 47;
            this.txtKanjoKamokuTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKanjoKamokuTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtKanjoKamokuTo_Validating);
            // 
            // lblKanjoKamokuTo
            // 
            this.lblKanjoKamokuTo.BackColor = System.Drawing.Color.Silver;
            this.lblKanjoKamokuTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKanjoKamokuTo.Location = new System.Drawing.Point(302, 20);
            this.lblKanjoKamokuTo.Name = "lblKanjoKamokuTo";
            this.lblKanjoKamokuTo.Size = new System.Drawing.Size(142, 20);
            this.lblKanjoKamokuTo.TabIndex = 48;
            this.lblKanjoKamokuTo.Text = "最　後";
            this.lblKanjoKamokuTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKanjoKamokuFr
            // 
            this.txtKanjoKamokuFr.AutoSizeFromLength = false;
            this.txtKanjoKamokuFr.DisplayLength = null;
            this.txtKanjoKamokuFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKanjoKamokuFr.Location = new System.Drawing.Point(20, 20);
            this.txtKanjoKamokuFr.MaxLength = 6;
            this.txtKanjoKamokuFr.Name = "txtKanjoKamokuFr";
            this.txtKanjoKamokuFr.Size = new System.Drawing.Size(50, 20);
            this.txtKanjoKamokuFr.TabIndex = 44;
            this.txtKanjoKamokuFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKanjoKamokuFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtKanjoKamokuFr_Validating);
            // 
            // lblKanjoKamokuFr
            // 
            this.lblKanjoKamokuFr.BackColor = System.Drawing.Color.Silver;
            this.lblKanjoKamokuFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKanjoKamokuFr.Location = new System.Drawing.Point(72, 20);
            this.lblKanjoKamokuFr.Name = "lblKanjoKamokuFr";
            this.lblKanjoKamokuFr.Size = new System.Drawing.Size(143, 20);
            this.lblKanjoKamokuFr.TabIndex = 45;
            this.lblKanjoKamokuFr.Text = "先　頭";
            this.lblKanjoKamokuFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gbxBumon
            // 
            this.gbxBumon.Controls.Add(this.lblBumonBet);
            this.gbxBumon.Controls.Add(this.txtBumonTo);
            this.gbxBumon.Controls.Add(this.lblBumonTo);
            this.gbxBumon.Controls.Add(this.txtBumonFr);
            this.gbxBumon.Controls.Add(this.lblBumonFr);
            this.gbxBumon.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxBumon.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gbxBumon.Location = new System.Drawing.Point(12, 483);
            this.gbxBumon.Name = "gbxBumon";
            this.gbxBumon.Size = new System.Drawing.Size(483, 50);
            this.gbxBumon.TabIndex = 49;
            this.gbxBumon.TabStop = false;
            this.gbxBumon.Text = "部門範囲";
            // 
            // lblBumonBet
            // 
            this.lblBumonBet.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBumonBet.Location = new System.Drawing.Point(223, 20);
            this.lblBumonBet.Name = "lblBumonBet";
            this.lblBumonBet.Size = new System.Drawing.Size(17, 22);
            this.lblBumonBet.TabIndex = 52;
            this.lblBumonBet.Text = "～";
            this.lblBumonBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtBumonTo
            // 
            this.txtBumonTo.AutoSizeFromLength = false;
            this.txtBumonTo.DisplayLength = null;
            this.txtBumonTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtBumonTo.Location = new System.Drawing.Point(251, 20);
            this.txtBumonTo.MaxLength = 4;
            this.txtBumonTo.Name = "txtBumonTo";
            this.txtBumonTo.Size = new System.Drawing.Size(50, 20);
            this.txtBumonTo.TabIndex = 53;
            this.txtBumonTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBumonTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtBumonTo_KeyDown);
            this.txtBumonTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtBumonTo_Validating);
            // 
            // lblBumonTo
            // 
            this.lblBumonTo.BackColor = System.Drawing.Color.Silver;
            this.lblBumonTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBumonTo.Location = new System.Drawing.Point(303, 20);
            this.lblBumonTo.Name = "lblBumonTo";
            this.lblBumonTo.Size = new System.Drawing.Size(142, 20);
            this.lblBumonTo.TabIndex = 54;
            this.lblBumonTo.Text = "最　後";
            this.lblBumonTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtBumonFr
            // 
            this.txtBumonFr.AutoSizeFromLength = false;
            this.txtBumonFr.DisplayLength = null;
            this.txtBumonFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtBumonFr.Location = new System.Drawing.Point(21, 20);
            this.txtBumonFr.MaxLength = 4;
            this.txtBumonFr.Name = "txtBumonFr";
            this.txtBumonFr.Size = new System.Drawing.Size(50, 20);
            this.txtBumonFr.TabIndex = 50;
            this.txtBumonFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBumonFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtBumonFr_Validating);
            // 
            // lblBumonFr
            // 
            this.lblBumonFr.BackColor = System.Drawing.Color.Silver;
            this.lblBumonFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBumonFr.Location = new System.Drawing.Point(73, 20);
            this.lblBumonFr.Name = "lblBumonFr";
            this.lblBumonFr.Size = new System.Drawing.Size(143, 20);
            this.lblBumonFr.TabIndex = 51;
            this.lblBumonFr.Text = "先　頭";
            this.lblBumonFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gbxShuturyokuDenpyo
            // 
            this.gbxShuturyokuDenpyo.Controls.Add(this.rdoKamokuGokei);
            this.gbxShuturyokuDenpyo.Controls.Add(this.rdoFurikaeDenpyo);
            this.gbxShuturyokuDenpyo.Controls.Add(this.rdoNyukinDenpyo);
            this.gbxShuturyokuDenpyo.Controls.Add(this.rdoShukkinDenpyo);
            this.gbxShuturyokuDenpyo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxShuturyokuDenpyo.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gbxShuturyokuDenpyo.Location = new System.Drawing.Point(12, 115);
            this.gbxShuturyokuDenpyo.Name = "gbxShuturyokuDenpyo";
            this.gbxShuturyokuDenpyo.Size = new System.Drawing.Size(483, 50);
            this.gbxShuturyokuDenpyo.TabIndex = 1;
            this.gbxShuturyokuDenpyo.TabStop = false;
            this.gbxShuturyokuDenpyo.Text = "出力伝票";
            // 
            // rdoKamokuGokei
            // 
            this.rdoKamokuGokei.AutoSize = true;
            this.rdoKamokuGokei.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoKamokuGokei.Location = new System.Drawing.Point(286, 20);
            this.rdoKamokuGokei.Name = "rdoKamokuGokei";
            this.rdoKamokuGokei.Size = new System.Drawing.Size(123, 17);
            this.rdoKamokuGokei.TabIndex = 5;
            this.rdoKamokuGokei.TabStop = true;
            this.rdoKamokuGokei.Text = "科目合計で出力";
            this.rdoKamokuGokei.UseVisualStyleBackColor = true;
            // 
            // rdoFurikaeDenpyo
            // 
            this.rdoFurikaeDenpyo.AutoSize = true;
            this.rdoFurikaeDenpyo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoFurikaeDenpyo.Location = new System.Drawing.Point(199, 20);
            this.rdoFurikaeDenpyo.Name = "rdoFurikaeDenpyo";
            this.rdoFurikaeDenpyo.Size = new System.Drawing.Size(81, 17);
            this.rdoFurikaeDenpyo.TabIndex = 4;
            this.rdoFurikaeDenpyo.TabStop = true;
            this.rdoFurikaeDenpyo.Text = "振替伝票";
            this.rdoFurikaeDenpyo.UseVisualStyleBackColor = true;
            // 
            // rdoNyukinDenpyo
            // 
            this.rdoNyukinDenpyo.AutoSize = true;
            this.rdoNyukinDenpyo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoNyukinDenpyo.ForeColor = System.Drawing.Color.Red;
            this.rdoNyukinDenpyo.Location = new System.Drawing.Point(22, 20);
            this.rdoNyukinDenpyo.Name = "rdoNyukinDenpyo";
            this.rdoNyukinDenpyo.Size = new System.Drawing.Size(81, 17);
            this.rdoNyukinDenpyo.TabIndex = 2;
            this.rdoNyukinDenpyo.TabStop = true;
            this.rdoNyukinDenpyo.Text = "入金伝票";
            this.rdoNyukinDenpyo.UseVisualStyleBackColor = true;
            // 
            // rdoShukkinDenpyo
            // 
            this.rdoShukkinDenpyo.AutoSize = true;
            this.rdoShukkinDenpyo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoShukkinDenpyo.ForeColor = System.Drawing.Color.Blue;
            this.rdoShukkinDenpyo.Location = new System.Drawing.Point(109, 20);
            this.rdoShukkinDenpyo.Name = "rdoShukkinDenpyo";
            this.rdoShukkinDenpyo.Size = new System.Drawing.Size(81, 17);
            this.rdoShukkinDenpyo.TabIndex = 3;
            this.rdoShukkinDenpyo.TabStop = true;
            this.rdoShukkinDenpyo.Text = "出金伝票";
            this.rdoShukkinDenpyo.UseVisualStyleBackColor = true;
            // 
            // gbxMizuageShisho
            // 
            this.gbxMizuageShisho.Controls.Add(this.txtMizuageShishoCd);
            this.gbxMizuageShisho.Controls.Add(this.lblMizuageShishoNm);
            this.gbxMizuageShisho.Controls.Add(this.lblMizuageShisho);
            this.gbxMizuageShisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.gbxMizuageShisho.ForeColor = System.Drawing.Color.Black;
            this.gbxMizuageShisho.Location = new System.Drawing.Point(12, 49);
            this.gbxMizuageShisho.Name = "gbxMizuageShisho";
            this.gbxMizuageShisho.Size = new System.Drawing.Size(404, 59);
            this.gbxMizuageShisho.TabIndex = 903;
            this.gbxMizuageShisho.TabStop = false;
            this.gbxMizuageShisho.Text = "支所";
            // 
            // txtMizuageShishoCd
            // 
            this.txtMizuageShishoCd.AutoSizeFromLength = true;
            this.txtMizuageShishoCd.DisplayLength = null;
            this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtMizuageShishoCd.Location = new System.Drawing.Point(71, 22);
            this.txtMizuageShishoCd.MaxLength = 4;
            this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
            this.txtMizuageShishoCd.Size = new System.Drawing.Size(34, 20);
            this.txtMizuageShishoCd.TabIndex = 1;
            this.txtMizuageShishoCd.TabStop = false;
            this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
            // 
            // lblMizuageShishoNm
            // 
            this.lblMizuageShishoNm.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMizuageShishoNm.Location = new System.Drawing.Point(107, 22);
            this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
            this.lblMizuageShishoNm.Size = new System.Drawing.Size(270, 20);
            this.lblMizuageShishoNm.TabIndex = 2;
            this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMizuageShisho
            // 
            this.lblMizuageShisho.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblMizuageShisho.Location = new System.Drawing.Point(27, 20);
            this.lblMizuageShisho.Name = "lblMizuageShisho";
            this.lblMizuageShisho.Size = new System.Drawing.Size(354, 25);
            this.lblMizuageShisho.TabIndex = 0;
            this.lblMizuageShisho.Text = "支所";
            this.lblMizuageShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ZMDR1031
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.gbxMizuageShisho);
            this.Controls.Add(this.gbxShuturyokuDenpyo);
            this.Controls.Add(this.gbxBumon);
            this.Controls.Add(this.gbxKanjoKamoku);
            this.Controls.Add(this.gbxTantosha);
            this.Controls.Add(this.gbxJuni);
            this.Controls.Add(this.gbxDenpyoBango);
            this.Controls.Add(this.gbxDate);
            this.Controls.Add(this.gbxShiwakeShurui);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "ZMDR1031";
            this.Text = "";
            this.Controls.SetChildIndex(this.gbxShiwakeShurui, 0);
            this.Controls.SetChildIndex(this.gbxDate, 0);
            this.Controls.SetChildIndex(this.gbxDenpyoBango, 0);
            this.Controls.SetChildIndex(this.gbxJuni, 0);
            this.Controls.SetChildIndex(this.gbxTantosha, 0);
            this.Controls.SetChildIndex(this.gbxKanjoKamoku, 0);
            this.Controls.SetChildIndex(this.gbxBumon, 0);
            this.Controls.SetChildIndex(this.gbxShuturyokuDenpyo, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.gbxMizuageShisho, 0);
            this.pnlDebug.ResumeLayout(false);
            this.gbxDate.ResumeLayout(false);
            this.gbxDate.PerformLayout();
            this.gbxShiwakeShurui.ResumeLayout(false);
            this.gbxShiwakeShurui.PerformLayout();
            this.gbxJuni.ResumeLayout(false);
            this.gbxJuni.PerformLayout();
            this.gbxDenpyoBango.ResumeLayout(false);
            this.gbxDenpyoBango.PerformLayout();
            this.gbxTantosha.ResumeLayout(false);
            this.gbxTantosha.PerformLayout();
            this.gbxKanjoKamoku.ResumeLayout(false);
            this.gbxKanjoKamoku.PerformLayout();
            this.gbxBumon.ResumeLayout(false);
            this.gbxBumon.PerformLayout();
            this.gbxShuturyokuDenpyo.ResumeLayout(false);
            this.gbxShuturyokuDenpyo.PerformLayout();
            this.gbxMizuageShisho.ResumeLayout(false);
            this.gbxMizuageShisho.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblFr;
        private System.Windows.Forms.Label lblGengoFr;
        private jp.co.fsi.common.controls.FsiTextBox txtMonthFr;
        private jp.co.fsi.common.controls.FsiTextBox txtYearFr;
        private jp.co.fsi.common.controls.FsiTextBox txtDayFr;
        private System.Windows.Forms.Label lblYearFr;
        private System.Windows.Forms.Label lblMonthFr;
        private System.Windows.Forms.Label lblDayFr;
        private System.Windows.Forms.GroupBox gbxDate;
        private System.Windows.Forms.GroupBox gbxShiwakeShurui;
        private System.Windows.Forms.Label lblCodeBetDate;
        private System.Windows.Forms.Label lblDayTo;
        private System.Windows.Forms.Label lblMonthTo;
        private System.Windows.Forms.Label lblYearTo;
        private jp.co.fsi.common.controls.FsiTextBox txtDayTo;
        private jp.co.fsi.common.controls.FsiTextBox txtYearTo;
        private jp.co.fsi.common.controls.FsiTextBox txtMonthTo;
        private System.Windows.Forms.Label lblGengoTo;
        private System.Windows.Forms.Label lblTo;
        private System.Windows.Forms.RadioButton rdoZenShiwake;
        private System.Windows.Forms.RadioButton rdoTujoShiwake;
        private System.Windows.Forms.RadioButton rdoKessanShiwake;
        private System.Windows.Forms.GroupBox gbxJuni;
        private System.Windows.Forms.RadioButton rdoDateJun;
        private System.Windows.Forms.RadioButton rdoBangoJun;
        private System.Windows.Forms.GroupBox gbxDenpyoBango;
        private System.Windows.Forms.Label lblDenpyoBangoBet;
        private jp.co.fsi.common.controls.FsiTextBox txtDenpyoBangoTo;
        private System.Windows.Forms.Label lblDenpyoBangoTo;
        private jp.co.fsi.common.controls.FsiTextBox txtDenpyoBangoFr;
        private System.Windows.Forms.Label lblDenpyoBangoFr;
        private System.Windows.Forms.GroupBox gbxTantosha;
        private System.Windows.Forms.Label lblTantoshaBet;
        private jp.co.fsi.common.controls.FsiTextBox txtTantoshaTo;
        private System.Windows.Forms.Label lblTantoshaTo;
        private jp.co.fsi.common.controls.FsiTextBox txtTantoshaFr;
        private System.Windows.Forms.Label lblTantoshaFr;
        private System.Windows.Forms.GroupBox gbxKanjoKamoku;
        private System.Windows.Forms.Label lblKanjoKamokuBet;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuTo;
        private System.Windows.Forms.Label lblKanjoKamokuTo;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuFr;
        private System.Windows.Forms.Label lblKanjoKamokuFr;
        private System.Windows.Forms.GroupBox gbxBumon;
        private System.Windows.Forms.Label lblBumonBet;
        private jp.co.fsi.common.controls.FsiTextBox txtBumonTo;
        private System.Windows.Forms.Label lblBumonTo;
        private jp.co.fsi.common.controls.FsiTextBox txtBumonFr;
        private System.Windows.Forms.Label lblBumonFr;
        private System.Windows.Forms.GroupBox gbxShuturyokuDenpyo;
        private System.Windows.Forms.RadioButton rdoKamokuGokei;
        private System.Windows.Forms.RadioButton rdoFurikaeDenpyo;
        private System.Windows.Forms.RadioButton rdoNyukinDenpyo;
        private System.Windows.Forms.RadioButton rdoShukkinDenpyo;
        private System.Windows.Forms.GroupBox gbxMizuageShisho;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.Label lblMizuageShisho;
    }
}