﻿namespace jp.co.fsi.zm.zmcm1051
{
    partial class ZMCM1052
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtTekiyoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lbTekiyoCd = new System.Windows.Forms.Label();
            this.txtTekiyoNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.lbGyoshubnKana = new System.Windows.Forms.Label();
            this.txtTekiyoKana = new jp.co.fsi.common.controls.FsiTextBox();
            this.lbGyoshubnNM = new System.Windows.Forms.Label();
            this.pnlMain = new jp.co.fsi.common.FsiPanel();
            this.pnlDebug.SuspendLayout();
            this.pnlMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(341, 23);
            this.lblTitle.Text = "摘要の登録";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(5, 62);
            this.pnlDebug.Size = new System.Drawing.Size(374, 100);
            // 
            // txtTekiyoCd
            // 
            this.txtTekiyoCd.AutoSizeFromLength = true;
            this.txtTekiyoCd.DisplayLength = null;
            this.txtTekiyoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTekiyoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtTekiyoCd.Location = new System.Drawing.Point(132, 13);
            this.txtTekiyoCd.MaxLength = 4;
            this.txtTekiyoCd.Name = "txtTekiyoCd";
            this.txtTekiyoCd.Size = new System.Drawing.Size(55, 20);
            this.txtTekiyoCd.TabIndex = 2;
            this.txtTekiyoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTekiyoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtTekiyoCd_Validating);
            // 
            // lbTekiyoCd
            // 
            this.lbTekiyoCd.BackColor = System.Drawing.Color.Silver;
            this.lbTekiyoCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbTekiyoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbTekiyoCd.Location = new System.Drawing.Point(17, 11);
            this.lbTekiyoCd.Name = "lbTekiyoCd";
            this.lbTekiyoCd.Size = new System.Drawing.Size(173, 25);
            this.lbTekiyoCd.TabIndex = 1;
            this.lbTekiyoCd.Text = "摘要コード";
            this.lbTekiyoCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTekiyoNm
            // 
            this.txtTekiyoNm.AutoSizeFromLength = false;
            this.txtTekiyoNm.DisplayLength = null;
            this.txtTekiyoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTekiyoNm.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtTekiyoNm.Location = new System.Drawing.Point(118, 5);
            this.txtTekiyoNm.MaxLength = 20;
            this.txtTekiyoNm.Name = "txtTekiyoNm";
            this.txtTekiyoNm.Size = new System.Drawing.Size(220, 20);
            this.txtTekiyoNm.TabIndex = 1;
            this.txtTekiyoNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtTekiyoNm_Validating);
            // 
            // lbGyoshubnKana
            // 
            this.lbGyoshubnKana.BackColor = System.Drawing.Color.Silver;
            this.lbGyoshubnKana.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbGyoshubnKana.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbGyoshubnKana.Location = new System.Drawing.Point(3, 28);
            this.lbGyoshubnKana.Name = "lbGyoshubnKana";
            this.lbGyoshubnKana.Size = new System.Drawing.Size(339, 25);
            this.lbGyoshubnKana.TabIndex = 0;
            this.lbGyoshubnKana.Text = "摘要カナ名";
            this.lbGyoshubnKana.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTekiyoKana
            // 
            this.txtTekiyoKana.AutoSizeFromLength = false;
            this.txtTekiyoKana.DisplayLength = null;
            this.txtTekiyoKana.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.txtTekiyoKana.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtTekiyoKana.Location = new System.Drawing.Point(118, 30);
            this.txtTekiyoKana.MaxLength = 20;
            this.txtTekiyoKana.Name = "txtTekiyoKana";
            this.txtTekiyoKana.Size = new System.Drawing.Size(220, 20);
            this.txtTekiyoKana.TabIndex = 2;
            this.txtTekiyoKana.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtTekiyoKana_KeyDown);
            // 
            // lbGyoshubnNM
            // 
            this.lbGyoshubnNM.BackColor = System.Drawing.Color.Silver;
            this.lbGyoshubnNM.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbGyoshubnNM.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbGyoshubnNM.Location = new System.Drawing.Point(3, 3);
            this.lbGyoshubnNM.Name = "lbGyoshubnNM";
            this.lbGyoshubnNM.Size = new System.Drawing.Size(339, 25);
            this.lbGyoshubnNM.TabIndex = 3;
            this.lbGyoshubnNM.Text = "摘要名";
            this.lbGyoshubnNM.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlMain
            // 
            this.pnlMain.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlMain.Controls.Add(this.txtTekiyoKana);
            this.pnlMain.Controls.Add(this.txtTekiyoNm);
            this.pnlMain.Controls.Add(this.lbGyoshubnKana);
            this.pnlMain.Controls.Add(this.lbGyoshubnNM);
            this.pnlMain.Location = new System.Drawing.Point(12, 39);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(346, 58);
            this.pnlMain.TabIndex = 902;
            // 
            // ZMCM1052
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(366, 165);
            this.Controls.Add(this.pnlMain);
            this.Controls.Add(this.txtTekiyoCd);
            this.Controls.Add(this.lbTekiyoCd);
            this.Name = "ZMCM1052";
            this.ShowFButton = true;
            this.Text = "摘要の登録";
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lbTekiyoCd, 0);
            this.Controls.SetChildIndex(this.txtTekiyoCd, 0);
            this.Controls.SetChildIndex(this.pnlMain, 0);
            this.pnlDebug.ResumeLayout(false);
            this.pnlMain.ResumeLayout(false);
            this.pnlMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private jp.co.fsi.common.controls.FsiTextBox txtTekiyoCd;
        private System.Windows.Forms.Label lbTekiyoCd;
        private jp.co.fsi.common.controls.FsiTextBox txtTekiyoNm;
        private System.Windows.Forms.Label lbGyoshubnKana;
        private jp.co.fsi.common.controls.FsiTextBox txtTekiyoKana;
        private System.Windows.Forms.Label lbGyoshubnNM;
        private jp.co.fsi.common.FsiPanel pnlMain;
    };
}