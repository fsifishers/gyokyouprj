﻿namespace jp.co.fsi.zm.zmcm1051
{
    /// <summary>
    /// ZMCM10511R の概要の説明です。
    /// </summary>
    partial class ZMCM10511R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(ZMCM10511R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.txtToday = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtPageCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCompanyName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtTitle01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle04 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle05 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle06 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line19 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line21 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtValue01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtValue03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.crossSectionLine1 = new GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle05)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle06)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.textBox1,
            this.txtToday,
            this.lblPage,
            this.txtPageCount,
            this.txtCompanyName,
            this.lblTitle,
            this.line1,
            this.txtTitle01,
            this.txtTitle04,
            this.txtTitle02,
            this.txtTitle03,
            this.txtTitle05,
            this.txtTitle06,
            this.line18,
            this.line5,
            this.line16,
            this.line17,
            this.line19,
            this.line21,
            this.line7,
            this.line2,
            this.crossSectionLine1});
            this.pageHeader.Height = 0.7598425F;
            this.pageHeader.Name = "pageHeader";
            this.pageHeader.Format += new System.EventHandler(this.pageHeader_Format);
            // 
            // txtToday
            // 
            this.txtToday.Height = 0.1968504F;
            this.txtToday.Left = 5.796851F;
            this.txtToday.MultiLine = false;
            this.txtToday.Name = "txtToday";
            this.txtToday.OutputFormat = resources.GetString("txtToday.OutputFormat");
            this.txtToday.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle";
            this.txtToday.Text = "yyyy/MM/dd";
            this.txtToday.Top = 0F;
            this.txtToday.Width = 1.181102F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.1968504F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 7.325197F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle";
            this.lblPage.Text = "頁";
            this.lblPage.Top = 0F;
            this.lblPage.Width = 0.1590552F;
            // 
            // txtPageCount
            // 
            this.txtPageCount.Height = 0.1968504F;
            this.txtPageCount.Left = 7.009449F;
            this.txtPageCount.MultiLine = false;
            this.txtPageCount.Name = "txtPageCount";
            this.txtPageCount.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle";
            this.txtPageCount.SummaryFunc = GrapeCity.ActiveReports.SectionReportModel.SummaryFunc.Count;
            this.txtPageCount.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtPageCount.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.txtPageCount.Text = "999";
            this.txtPageCount.Top = 0F;
            this.txtPageCount.Width = 0.2952756F;
            // 
            // txtCompanyName
            // 
            this.txtCompanyName.DataField = "ITEM01";
            this.txtCompanyName.Height = 0.1968504F;
            this.txtCompanyName.Left = 0.03188977F;
            this.txtCompanyName.MultiLine = false;
            this.txtCompanyName.Name = "txtCompanyName";
            this.txtCompanyName.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; vertical-align: middle";
            this.txtCompanyName.Top = 0.2362205F;
            this.txtCompanyName.Width = 2.655906F;
            // 
            // lblTitle
            // 
            this.lblTitle.Height = 0.2362205F;
            this.lblTitle.HyperLink = null;
            this.lblTitle.Left = 2.745276F;
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Style = "font-family: ＭＳ 明朝; font-size: 15.75pt; font-weight: bold; text-align: center";
            this.lblTitle.Text = "摘要リスト";
            this.lblTitle.Top = 0F;
            this.lblTitle.Width = 1.832284F;
            // 
            // line1
            // 
            this.line1.Height = 0F;
            this.line1.Left = 2.745276F;
            this.line1.LineWeight = 1F;
            this.line1.Name = "line1";
            this.line1.Top = 0.2362205F;
            this.line1.Width = 1.832283F;
            this.line1.X1 = 2.745276F;
            this.line1.X2 = 4.577559F;
            this.line1.Y1 = 0.2362205F;
            this.line1.Y2 = 0.2362205F;
            // 
            // txtTitle01
            // 
            this.txtTitle01.Height = 0.2755905F;
            this.txtTitle01.Left = 0.01102363F;
            this.txtTitle01.MultiLine = false;
            this.txtTitle01.Name = "txtTitle01";
            this.txtTitle01.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; ver" +
    "tical-align: middle";
            this.txtTitle01.Text = "ｺｰﾄﾞ";
            this.txtTitle01.Top = 0.472441F;
            this.txtTitle01.Width = 0.5795276F;
            // 
            // txtTitle04
            // 
            this.txtTitle04.Height = 0.2755905F;
            this.txtTitle04.Left = 3.755118F;
            this.txtTitle04.MultiLine = false;
            this.txtTitle04.Name = "txtTitle04";
            this.txtTitle04.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; ver" +
    "tical-align: middle";
            this.txtTitle04.Text = "ｺｰﾄﾞ";
            this.txtTitle04.Top = 0.4665355F;
            this.txtTitle04.Width = 0.5795276F;
            // 
            // txtTitle02
            // 
            this.txtTitle02.Height = 0.2755905F;
            this.txtTitle02.Left = 0.5795276F;
            this.txtTitle02.MultiLine = false;
            this.txtTitle02.Name = "txtTitle02";
            this.txtTitle02.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; ver" +
    "tical-align: middle";
            this.txtTitle02.Text = "摘　要　名";
            this.txtTitle02.Top = 0.4665355F;
            this.txtTitle02.Width = 1.599606F;
            // 
            // txtTitle03
            // 
            this.txtTitle03.Height = 0.2755905F;
            this.txtTitle03.Left = 2.201181F;
            this.txtTitle03.MultiLine = false;
            this.txtTitle03.Name = "txtTitle03";
            this.txtTitle03.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; ver" +
    "tical-align: middle";
            this.txtTitle03.Text = "カ　ナ　名";
            this.txtTitle03.Top = 0.4665355F;
            this.txtTitle03.Width = 1.538976F;
            // 
            // txtTitle05
            // 
            this.txtTitle05.Height = 0.2755905F;
            this.txtTitle05.Left = 4.344489F;
            this.txtTitle05.MultiLine = false;
            this.txtTitle05.Name = "txtTitle05";
            this.txtTitle05.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; ver" +
    "tical-align: middle";
            this.txtTitle05.Text = "摘　要　名";
            this.txtTitle05.Top = 0.4665355F;
            this.txtTitle05.Width = 1.589764F;
            // 
            // txtTitle06
            // 
            this.txtTitle06.Height = 0.2755905F;
            this.txtTitle06.Left = 5.925591F;
            this.txtTitle06.MultiLine = false;
            this.txtTitle06.Name = "txtTitle06";
            this.txtTitle06.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold; text-align: center; ver" +
    "tical-align: middle";
            this.txtTitle06.Text = "カ　ナ　名";
            this.txtTitle06.Top = 0.4665355F;
            this.txtTitle06.Width = 1.538976F;
            // 
            // line18
            // 
            this.line18.Height = 0.2814962F;
            this.line18.Left = 0.5795276F;
            this.line18.LineWeight = 1F;
            this.line18.Name = "line18";
            this.line18.Top = 0.4665354F;
            this.line18.Width = 0F;
            this.line18.X1 = 0.5795276F;
            this.line18.X2 = 0.5795276F;
            this.line18.Y1 = 0.4665354F;
            this.line18.Y2 = 0.7480316F;
            // 
            // line5
            // 
            this.line5.Height = 0.2771654F;
            this.line5.Left = 3.755118F;
            this.line5.LineWeight = 1F;
            this.line5.Name = "line5";
            this.line5.Top = 0.4708662F;
            this.line5.Width = 0F;
            this.line5.X1 = 3.755118F;
            this.line5.X2 = 3.755118F;
            this.line5.Y1 = 0.4708662F;
            this.line5.Y2 = 0.7480316F;
            // 
            // line16
            // 
            this.line16.Height = 0.2814962F;
            this.line16.Left = 2.190158F;
            this.line16.LineWeight = 1F;
            this.line16.Name = "line16";
            this.line16.Top = 0.4665354F;
            this.line16.Width = 0F;
            this.line16.X1 = 2.190158F;
            this.line16.X2 = 2.190158F;
            this.line16.Y1 = 0.4665354F;
            this.line16.Y2 = 0.7480316F;
            // 
            // line17
            // 
            this.line17.Height = 0.2771655F;
            this.line17.Left = 4.322834F;
            this.line17.LineWeight = 1F;
            this.line17.Name = "line17";
            this.line17.Top = 0.4708661F;
            this.line17.Width = 0F;
            this.line17.X1 = 4.322834F;
            this.line17.X2 = 4.322834F;
            this.line17.Y1 = 0.4708661F;
            this.line17.Y2 = 0.7480316F;
            // 
            // line19
            // 
            this.line19.Height = 0.2830709F;
            this.line19.Left = 5.934252F;
            this.line19.LineWeight = 1F;
            this.line19.Name = "line19";
            this.line19.Top = 0.4649607F;
            this.line19.Width = 0F;
            this.line19.X1 = 5.934252F;
            this.line19.X2 = 5.934252F;
            this.line19.Y1 = 0.4649607F;
            this.line19.Y2 = 0.7480316F;
            // 
            // line21
            // 
            this.line21.Height = 0.2814962F;
            this.line21.Left = 0.01102362F;
            this.line21.LineWeight = 1F;
            this.line21.Name = "line21";
            this.line21.Top = 0.4665354F;
            this.line21.Width = 0F;
            this.line21.X1 = 0.01102362F;
            this.line21.X2 = 0.01102362F;
            this.line21.Y1 = 0.4665354F;
            this.line21.Y2 = 0.7480316F;
            // 
            // line7
            // 
            this.line7.Height = 0F;
            this.line7.Left = 0.01102362F;
            this.line7.LineWeight = 1F;
            this.line7.Name = "line7";
            this.line7.Top = 0.7480316F;
            this.line7.Width = 7.473228F;
            this.line7.X1 = 0.01102362F;
            this.line7.X2 = 7.484252F;
            this.line7.Y1 = 0.7480316F;
            this.line7.Y2 = 0.7480316F;
            // 
            // line2
            // 
            this.line2.Height = 1.192093E-07F;
            this.line2.Left = 0.01102362F;
            this.line2.LineWeight = 1F;
            this.line2.Name = "line2";
            this.line2.Top = 0.4665354F;
            this.line2.Width = 7.473228F;
            this.line2.X1 = 0.01102362F;
            this.line2.X2 = 7.484252F;
            this.line2.Y1 = 0.4665354F;
            this.line2.Y2 = 0.4665355F;
            // 
            // detail
            // 
            this.detail.ColumnCount = 2;
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtValue01,
            this.txtValue02,
            this.txtValue03,
            this.line9,
            this.line13,
            this.line12,
            this.line15,
            this.line14});
            this.detail.Height = 0.2358268F;
            this.detail.Name = "detail";
            // 
            // txtValue01
            // 
            this.txtValue01.DataField = "ITEM02";
            this.txtValue01.Height = 0.2204724F;
            this.txtValue01.Left = 9.313226E-10F;
            this.txtValue01.MultiLine = false;
            this.txtValue01.Name = "txtValue01";
            this.txtValue01.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle";
            this.txtValue01.Text = "\r\n\r\n";
            this.txtValue01.Top = 0.01299213F;
            this.txtValue01.Width = 0.5484253F;
            // 
            // txtValue02
            // 
            this.txtValue02.DataField = "ITEM03";
            this.txtValue02.Height = 0.2204724F;
            this.txtValue02.Left = 0.6110237F;
            this.txtValue02.MultiLine = false;
            this.txtValue02.Name = "txtValue02";
            this.txtValue02.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle";
            this.txtValue02.Text = "\r\n\r\n";
            this.txtValue02.Top = 0.01299213F;
            this.txtValue02.Width = 1.568111F;
            // 
            // txtValue03
            // 
            this.txtValue03.DataField = "ITEM04";
            this.txtValue03.Height = 0.2204724F;
            this.txtValue03.Left = 2.222047F;
            this.txtValue03.MultiLine = false;
            this.txtValue03.Name = "txtValue03";
            this.txtValue03.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle";
            this.txtValue03.Text = "\r\n\r\n";
            this.txtValue03.Top = 0.01299213F;
            this.txtValue03.Width = 1.51811F;
            // 
            // line9
            // 
            this.line9.Height = 0F;
            this.line9.Left = 0.01102362F;
            this.line9.LineWeight = 1F;
            this.line9.Name = "line9";
            this.line9.Top = 0.2350394F;
            this.line9.Width = 3.729134F;
            this.line9.X1 = 0.01102362F;
            this.line9.X2 = 3.740158F;
            this.line9.Y1 = 0.2350394F;
            this.line9.Y2 = 0.2350394F;
            // 
            // line13
            // 
            this.line13.Height = 0.2350394F;
            this.line13.Left = 2.190158F;
            this.line13.LineWeight = 1F;
            this.line13.Name = "line13";
            this.line13.Top = 0F;
            this.line13.Width = 0F;
            this.line13.X1 = 2.190158F;
            this.line13.X2 = 2.190158F;
            this.line13.Y1 = 0F;
            this.line13.Y2 = 0.2350394F;
            // 
            // line12
            // 
            this.line12.Height = 0.2350394F;
            this.line12.Left = 0.5795276F;
            this.line12.LineWeight = 1F;
            this.line12.Name = "line12";
            this.line12.Top = 0F;
            this.line12.Width = 0F;
            this.line12.X1 = 0.5795276F;
            this.line12.X2 = 0.5795276F;
            this.line12.Y1 = 0F;
            this.line12.Y2 = 0.2350394F;
            // 
            // line14
            // 
            this.line14.Height = 0.2350394F;
            this.line14.Left = 3.755118F;
            this.line14.LineWeight = 1F;
            this.line14.Name = "line14";
            this.line14.Top = 0F;
            this.line14.Width = 0F;
            this.line14.X1 = 3.755118F;
            this.line14.X2 = 3.755118F;
            this.line14.Y1 = 0F;
            this.line14.Y2 = 0.2350394F;
            // 
            // line15
            // 
            this.line15.Height = 0.234252F;
            this.line15.Left = 0.01102362F;
            this.line15.LineWeight = 1F;
            this.line15.Name = "line15";
            this.line15.Top = 0F;
            this.line15.Width = 0F;
            this.line15.X1 = 0.01102362F;
            this.line15.X2 = 0.01102362F;
            this.line15.Y1 = 0F;
            this.line15.Y2 = 0.234252F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            this.pageFooter.Visible = false;
            // 
            // textBox1
            // 
            this.textBox1.Height = 0.281496F;
            this.textBox1.Left = 0.01102362F;
            this.textBox1.MultiLine = false;
            this.textBox1.Name = "textBox1";
            this.textBox1.Style = "background-color: Cyan; font-family: ＭＳ ゴシック; font-size: 9.75pt; vertical-align: " +
    "middle";
            this.textBox1.Text = null;
            this.textBox1.Top = 0.4665355F;
            this.textBox1.Width = 7.473229F;
            // 
            // crossSectionLine1
            // 
            this.crossSectionLine1.Bottom = 0F;
            this.crossSectionLine1.Left = 7.484252F;
            this.crossSectionLine1.LineWeight = 1F;
            this.crossSectionLine1.Name = "crossSectionLine1";
            this.crossSectionLine1.Top = 0.4708662F;
            // 
            // ZAMC9031R
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.5905512F;
            this.PageSettings.Margins.Left = 0.3937007F;
            this.PageSettings.Margins.Right = 0.2755905F;
            this.PageSettings.Margins.Top = 0.3937007F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Portrait;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.484252F;
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.pageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle05)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle06)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValue03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtToday;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPageCount;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCompanyName;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.Line line7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue01;
        private GrapeCity.ActiveReports.SectionReportModel.Line line9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue03;
        private GrapeCity.ActiveReports.SectionReportModel.Line line13;
        private GrapeCity.ActiveReports.SectionReportModel.Line line12;
        private GrapeCity.ActiveReports.SectionReportModel.Line line14;
        private GrapeCity.ActiveReports.SectionReportModel.Line line15;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle04;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle05;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle06;
        private GrapeCity.ActiveReports.SectionReportModel.Line line18;
        private GrapeCity.ActiveReports.SectionReportModel.Line line5;
        private GrapeCity.ActiveReports.SectionReportModel.Line line16;
        private GrapeCity.ActiveReports.SectionReportModel.Line line17;
        private GrapeCity.ActiveReports.SectionReportModel.Line line19;
        private GrapeCity.ActiveReports.SectionReportModel.Line line21;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
        private GrapeCity.ActiveReports.SectionReportModel.CrossSectionLine crossSectionLine1;
    }
}
