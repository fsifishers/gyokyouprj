﻿namespace jp.co.fsi.zm.zmcm1051
{
    partial class ZMCM1053
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbxCondition = new System.Windows.Forms.GroupBox();
            this.lblTekiyoCdTo = new System.Windows.Forms.Label();
            this.txtTekiyoCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblCodeBet1 = new System.Windows.Forms.Label();
            this.lblTekiyoCdFr = new System.Windows.Forms.Label();
            this.txtTekiyoCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.pnlDebug.SuspendLayout();
            this.gbxCondition.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(508, 23);
            this.lblTitle.Text = "";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(5, 36);
            this.pnlDebug.Size = new System.Drawing.Size(541, 100);
            // 
            // gbxCondition
            // 
            this.gbxCondition.Controls.Add(this.lblTekiyoCdTo);
            this.gbxCondition.Controls.Add(this.txtTekiyoCdTo);
            this.gbxCondition.Controls.Add(this.lblCodeBet1);
            this.gbxCondition.Controls.Add(this.lblTekiyoCdFr);
            this.gbxCondition.Controls.Add(this.txtTekiyoCdFr);
            this.gbxCondition.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxCondition.Location = new System.Drawing.Point(12, 13);
            this.gbxCondition.Name = "gbxCondition";
            this.gbxCondition.Size = new System.Drawing.Size(511, 65);
            this.gbxCondition.TabIndex = 1;
            this.gbxCondition.TabStop = false;
            this.gbxCondition.Text = "摘要コード範囲";
            // 
            // lblTekiyoCdTo
            // 
            this.lblTekiyoCdTo.BackColor = System.Drawing.Color.Silver;
            this.lblTekiyoCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTekiyoCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTekiyoCdTo.Location = new System.Drawing.Point(314, 28);
            this.lblTekiyoCdTo.Name = "lblTekiyoCdTo";
            this.lblTekiyoCdTo.Size = new System.Drawing.Size(180, 20);
            this.lblTekiyoCdTo.TabIndex = 4;
            this.lblTekiyoCdTo.Text = "最　後";
            this.lblTekiyoCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTekiyoCdTo
            // 
            this.txtTekiyoCdTo.AutoSizeFromLength = true;
            this.txtTekiyoCdTo.DisplayLength = null;
            this.txtTekiyoCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTekiyoCdTo.Location = new System.Drawing.Point(263, 28);
            this.txtTekiyoCdTo.MaxLength = 4;
            this.txtTekiyoCdTo.Name = "txtTekiyoCdTo";
            this.txtTekiyoCdTo.Size = new System.Drawing.Size(48, 20);
            this.txtTekiyoCdTo.TabIndex = 3;
            this.txtTekiyoCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTekiyoCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtTekiyoCdTo_Validating);
            // 
            // lblCodeBet1
            // 
            this.lblCodeBet1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCodeBet1.Location = new System.Drawing.Point(239, 28);
            this.lblCodeBet1.Name = "lblCodeBet1";
            this.lblCodeBet1.Size = new System.Drawing.Size(18, 20);
            this.lblCodeBet1.TabIndex = 2;
            this.lblCodeBet1.Text = "～";
            this.lblCodeBet1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTekiyoCdFr
            // 
            this.lblTekiyoCdFr.BackColor = System.Drawing.Color.Silver;
            this.lblTekiyoCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTekiyoCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTekiyoCdFr.Location = new System.Drawing.Point(57, 28);
            this.lblTekiyoCdFr.Name = "lblTekiyoCdFr";
            this.lblTekiyoCdFr.Size = new System.Drawing.Size(180, 20);
            this.lblTekiyoCdFr.TabIndex = 1;
            this.lblTekiyoCdFr.Text = "先　頭";
            this.lblTekiyoCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTekiyoCdFr
            // 
            this.txtTekiyoCdFr.AutoSizeFromLength = true;
            this.txtTekiyoCdFr.DisplayLength = null;
            this.txtTekiyoCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTekiyoCdFr.Location = new System.Drawing.Point(6, 28);
            this.txtTekiyoCdFr.MaxLength = 4;
            this.txtTekiyoCdFr.Name = "txtTekiyoCdFr";
            this.txtTekiyoCdFr.Size = new System.Drawing.Size(48, 20);
            this.txtTekiyoCdFr.TabIndex = 0;
            this.txtTekiyoCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTekiyoCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtTekiyoCdFr_Validating);
            // 
            // ZMCM1053
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(533, 139);
            this.Controls.Add(this.gbxCondition);
            this.Name = "ZMCM1053";
            this.ShowFButton = true;
            this.Text = "摘要の印刷";
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.gbxCondition, 0);
            this.pnlDebug.ResumeLayout(false);
            this.gbxCondition.ResumeLayout(false);
            this.gbxCondition.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbxCondition;
        private System.Windows.Forms.Label lblTekiyoCdTo;
        private jp.co.fsi.common.controls.FsiTextBox txtTekiyoCdTo;
        private System.Windows.Forms.Label lblCodeBet1;
        private System.Windows.Forms.Label lblTekiyoCdFr;
        private jp.co.fsi.common.controls.FsiTextBox txtTekiyoCdFr;

    };
}