﻿namespace jp.co.fsi.zm.zmce1011
{
    partial class ZMCE1013
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gpbMeisai = new System.Windows.Forms.GroupBox();
            this.rdoUchiwake = new System.Windows.Forms.RadioButton();
            this.rdoGokei = new System.Windows.Forms.RadioButton();
            this.gpbInji = new System.Windows.Forms.GroupBox();
            this.rdoNashi = new System.Windows.Forms.RadioButton();
            this.rdoAri = new System.Windows.Forms.RadioButton();
            this.gpbKanjoKamokuCd = new System.Windows.Forms.GroupBox();
            this.lblBt = new System.Windows.Forms.Label();
            this.lblKanjoKamokuNmTo = new System.Windows.Forms.Label();
            this.txtKanjoKamokuCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKanjoKamokuNmFr = new System.Windows.Forms.Label();
            this.txtKanjoKamokuCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.pnlDebug.SuspendLayout();
            this.gpbMeisai.SuspendLayout();
            this.gpbInji.SuspendLayout();
            this.gpbKanjoKamokuCd.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(465, 23);
            this.lblTitle.Text = "補助科目残高";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(5, 107);
            this.pnlDebug.Size = new System.Drawing.Size(498, 100);
            // 
            // gpbMeisai
            // 
            this.gpbMeisai.Controls.Add(this.rdoUchiwake);
            this.gpbMeisai.Controls.Add(this.rdoGokei);
            this.gpbMeisai.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gpbMeisai.Location = new System.Drawing.Point(12, 13);
            this.gpbMeisai.Name = "gpbMeisai";
            this.gpbMeisai.Size = new System.Drawing.Size(199, 52);
            this.gpbMeisai.TabIndex = 0;
            this.gpbMeisai.TabStop = false;
            this.gpbMeisai.Text = "明細出力方法";
            // 
            // rdoUchiwake
            // 
            this.rdoUchiwake.AutoSize = true;
            this.rdoUchiwake.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoUchiwake.Location = new System.Drawing.Point(110, 22);
            this.rdoUchiwake.Name = "rdoUchiwake";
            this.rdoUchiwake.Size = new System.Drawing.Size(71, 16);
            this.rdoUchiwake.TabIndex = 1;
            this.rdoUchiwake.Text = "科目内訳";
            this.rdoUchiwake.UseVisualStyleBackColor = true;
            // 
            // rdoGokei
            // 
            this.rdoGokei.AutoSize = true;
            this.rdoGokei.Checked = true;
            this.rdoGokei.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoGokei.Location = new System.Drawing.Point(6, 22);
            this.rdoGokei.Name = "rdoGokei";
            this.rdoGokei.Size = new System.Drawing.Size(71, 16);
            this.rdoGokei.TabIndex = 0;
            this.rdoGokei.TabStop = true;
            this.rdoGokei.Text = "科目合計";
            this.rdoGokei.UseVisualStyleBackColor = true;
            // 
            // gpbInji
            // 
            this.gpbInji.Controls.Add(this.rdoNashi);
            this.gpbInji.Controls.Add(this.rdoAri);
            this.gpbInji.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gpbInji.Location = new System.Drawing.Point(239, 13);
            this.gpbInji.Name = "gpbInji";
            this.gpbInji.Size = new System.Drawing.Size(199, 52);
            this.gpbInji.TabIndex = 1;
            this.gpbInji.TabStop = false;
            this.gpbInji.Text = "発生額ゼロの印字";
            // 
            // rdoNashi
            // 
            this.rdoNashi.AutoSize = true;
            this.rdoNashi.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoNashi.Location = new System.Drawing.Point(110, 22);
            this.rdoNashi.Name = "rdoNashi";
            this.rdoNashi.Size = new System.Drawing.Size(47, 16);
            this.rdoNashi.TabIndex = 1;
            this.rdoNashi.Text = "無し";
            this.rdoNashi.UseVisualStyleBackColor = true;
            // 
            // rdoAri
            // 
            this.rdoAri.AutoSize = true;
            this.rdoAri.Checked = true;
            this.rdoAri.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoAri.Location = new System.Drawing.Point(6, 22);
            this.rdoAri.Name = "rdoAri";
            this.rdoAri.Size = new System.Drawing.Size(47, 16);
            this.rdoAri.TabIndex = 0;
            this.rdoAri.TabStop = true;
            this.rdoAri.Text = "有り";
            this.rdoAri.UseVisualStyleBackColor = true;
            // 
            // gpbKanjoKamokuCd
            // 
            this.gpbKanjoKamokuCd.Controls.Add(this.lblKanjoKamokuNmTo);
            this.gpbKanjoKamokuCd.Controls.Add(this.txtKanjoKamokuCdTo);
            this.gpbKanjoKamokuCd.Controls.Add(this.lblKanjoKamokuNmFr);
            this.gpbKanjoKamokuCd.Controls.Add(this.txtKanjoKamokuCdFr);
            this.gpbKanjoKamokuCd.Controls.Add(this.lblBt);
            this.gpbKanjoKamokuCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gpbKanjoKamokuCd.Location = new System.Drawing.Point(12, 91);
            this.gpbKanjoKamokuCd.Name = "gpbKanjoKamokuCd";
            this.gpbKanjoKamokuCd.Size = new System.Drawing.Size(468, 52);
            this.gpbKanjoKamokuCd.TabIndex = 2;
            this.gpbKanjoKamokuCd.TabStop = false;
            this.gpbKanjoKamokuCd.Text = "勘定科目CD範囲";
            // 
            // lblBt
            // 
            this.lblBt.BackColor = System.Drawing.Color.Transparent;
            this.lblBt.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBt.Location = new System.Drawing.Point(224, 20);
            this.lblBt.Name = "lblBt";
            this.lblBt.Size = new System.Drawing.Size(20, 20);
            this.lblBt.TabIndex = 2;
            this.lblBt.Text = "～";
            this.lblBt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblKanjoKamokuNmTo
            // 
            this.lblKanjoKamokuNmTo.BackColor = System.Drawing.Color.Silver;
            this.lblKanjoKamokuNmTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKanjoKamokuNmTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKanjoKamokuNmTo.Location = new System.Drawing.Point(279, 20);
            this.lblKanjoKamokuNmTo.Name = "lblKanjoKamokuNmTo";
            this.lblKanjoKamokuNmTo.Size = new System.Drawing.Size(179, 20);
            this.lblKanjoKamokuNmTo.TabIndex = 4;
            this.lblKanjoKamokuNmTo.Text = "最　後";
            this.lblKanjoKamokuNmTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKanjoKamokuCdTo
            // 
            this.txtKanjoKamokuCdTo.AutoSizeFromLength = true;
            this.txtKanjoKamokuCdTo.DisplayLength = null;
            this.txtKanjoKamokuCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKanjoKamokuCdTo.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtKanjoKamokuCdTo.Location = new System.Drawing.Point(243, 20);
            this.txtKanjoKamokuCdTo.MaxLength = 4;
            this.txtKanjoKamokuCdTo.Name = "txtKanjoKamokuCdTo";
            this.txtKanjoKamokuCdTo.Size = new System.Drawing.Size(34, 20);
            this.txtKanjoKamokuCdTo.TabIndex = 3;
            this.txtKanjoKamokuCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKanjoKamokuCdTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtKanjoKamokuCdTo_KeyDown);
            this.txtKanjoKamokuCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtKanjoKamokuCdTo_Validating);
            // 
            // lblKanjoKamokuNmFr
            // 
            this.lblKanjoKamokuNmFr.BackColor = System.Drawing.Color.Silver;
            this.lblKanjoKamokuNmFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKanjoKamokuNmFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKanjoKamokuNmFr.Location = new System.Drawing.Point(46, 20);
            this.lblKanjoKamokuNmFr.Name = "lblKanjoKamokuNmFr";
            this.lblKanjoKamokuNmFr.Size = new System.Drawing.Size(177, 20);
            this.lblKanjoKamokuNmFr.TabIndex = 1;
            this.lblKanjoKamokuNmFr.Text = "先　頭";
            this.lblKanjoKamokuNmFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKanjoKamokuCdFr
            // 
            this.txtKanjoKamokuCdFr.AutoSizeFromLength = true;
            this.txtKanjoKamokuCdFr.DisplayLength = null;
            this.txtKanjoKamokuCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKanjoKamokuCdFr.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtKanjoKamokuCdFr.Location = new System.Drawing.Point(10, 20);
            this.txtKanjoKamokuCdFr.MaxLength = 4;
            this.txtKanjoKamokuCdFr.Name = "txtKanjoKamokuCdFr";
            this.txtKanjoKamokuCdFr.Size = new System.Drawing.Size(34, 20);
            this.txtKanjoKamokuCdFr.TabIndex = 0;
            this.txtKanjoKamokuCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKanjoKamokuCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtKanjoKamokuCdFr_Validating);
            // 
            // ZMCE1013
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(490, 210);
            this.Controls.Add(this.gpbKanjoKamokuCd);
            this.Controls.Add(this.gpbInji);
            this.Controls.Add(this.gpbMeisai);
            this.Name = "ZMCE1013";
            this.Text = "期首残高の印刷";
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.gpbMeisai, 0);
            this.Controls.SetChildIndex(this.gpbInji, 0);
            this.Controls.SetChildIndex(this.gpbKanjoKamokuCd, 0);
            this.pnlDebug.ResumeLayout(false);
            this.gpbMeisai.ResumeLayout(false);
            this.gpbMeisai.PerformLayout();
            this.gpbInji.ResumeLayout(false);
            this.gpbInji.PerformLayout();
            this.gpbKanjoKamokuCd.ResumeLayout(false);
            this.gpbKanjoKamokuCd.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gpbMeisai;
        private System.Windows.Forms.RadioButton rdoUchiwake;
        private System.Windows.Forms.RadioButton rdoGokei;
        private System.Windows.Forms.GroupBox gpbInji;
        private System.Windows.Forms.RadioButton rdoNashi;
        private System.Windows.Forms.RadioButton rdoAri;
        private System.Windows.Forms.GroupBox gpbKanjoKamokuCd;
        private System.Windows.Forms.Label lblBt;
        private System.Windows.Forms.Label lblKanjoKamokuNmTo;
        private common.controls.FsiTextBox txtKanjoKamokuCdTo;
        private System.Windows.Forms.Label lblKanjoKamokuNmFr;
        private common.controls.FsiTextBox txtKanjoKamokuCdFr;



    }
}