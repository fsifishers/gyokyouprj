﻿namespace jp.co.fsi.zm.zmce1011
{
    partial class ZMCE1014
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtKanjoKmk = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKanjoKmk = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblHojoGokei = new System.Windows.Forms.Label();
            this.lblKanjoGokei = new System.Windows.Forms.Label();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.dgvKishuZan = new jp.co.fsi.zm.zmce1011.DataGridViewEx();
            this.dgvHjkBmnKei = new System.Windows.Forms.DataGridView();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.dgvBmnKei = new System.Windows.Forms.DataGridView();
            this.dgvKmkKei = new System.Windows.Forms.DataGridView();
            this.pnlDebug.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvKishuZan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHjkBmnKei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBmnKei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvKmkKei)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(888, 23);
            this.lblTitle.Text = "補助科目残高";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Controls.Add(this.label4);
            this.pnlDebug.Controls.Add(this.label3);
            this.pnlDebug.Location = new System.Drawing.Point(5, 472);
            this.pnlDebug.Size = new System.Drawing.Size(921, 100);
            this.pnlDebug.Controls.SetChildIndex(this.btnF6, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF7, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF5, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF8, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF4, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF9, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF3, 0);
            this.pnlDebug.Controls.SetChildIndex(this.label3, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF10, 0);
            this.pnlDebug.Controls.SetChildIndex(this.label4, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF2, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF11, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF1, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF12, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnEsc, 0);
            // 
            // txtKanjoKmk
            // 
            this.txtKanjoKmk.AutoSizeFromLength = false;
            this.txtKanjoKmk.BackColor = System.Drawing.SystemColors.Window;
            this.txtKanjoKmk.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtKanjoKmk.DisplayLength = null;
            this.txtKanjoKmk.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKanjoKmk.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKanjoKmk.Location = new System.Drawing.Point(90, 14);
            this.txtKanjoKmk.MaxLength = 6;
            this.txtKanjoKmk.Name = "txtKanjoKmk";
            this.txtKanjoKmk.Size = new System.Drawing.Size(40, 16);
            this.txtKanjoKmk.TabIndex = 2;
            this.txtKanjoKmk.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblKanjoKmk
            // 
            this.lblKanjoKmk.BackColor = System.Drawing.SystemColors.Window;
            this.lblKanjoKmk.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKanjoKmk.Location = new System.Drawing.Point(130, 10);
            this.lblKanjoKmk.Name = "lblKanjoKmk";
            this.lblKanjoKmk.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblKanjoKmk.Size = new System.Drawing.Size(187, 23);
            this.lblKanjoKmk.TabIndex = 3;
            this.lblKanjoKmk.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(12, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 23);
            this.label1.TabIndex = 1;
            this.label1.Text = "勘定科目：";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.PaleTurquoise;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(179, 1);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(251, 23);
            this.label3.TabIndex = 7;
            this.label3.Text = "合　計";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label3.Visible = false;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.PaleTurquoise;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.Location = new System.Drawing.Point(119, 1);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 23);
            this.label4.TabIndex = 6;
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label4.Visible = false;
            // 
            // lblHojoGokei
            // 
            this.lblHojoGokei.BackColor = System.Drawing.Color.PaleTurquoise;
            this.lblHojoGokei.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblHojoGokei.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblHojoGokei.Location = new System.Drawing.Point(613, 485);
            this.lblHojoGokei.Name = "lblHojoGokei";
            this.lblHojoGokei.Size = new System.Drawing.Size(181, 23);
            this.lblHojoGokei.TabIndex = 8;
            this.lblHojoGokei.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblHojoGokei.Visible = false;
            // 
            // lblKanjoGokei
            // 
            this.lblKanjoGokei.BackColor = System.Drawing.SystemColors.Window;
            this.lblKanjoGokei.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKanjoGokei.Location = new System.Drawing.Point(586, 12);
            this.lblKanjoGokei.Name = "lblKanjoGokei";
            this.lblKanjoGokei.Size = new System.Drawing.Size(208, 23);
            this.lblKanjoGokei.TabIndex = 4;
            this.lblKanjoGokei.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // splitContainer1
            // 
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(12, 39);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.dgvKishuZan);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dgvHjkBmnKei);
            this.splitContainer1.Size = new System.Drawing.Size(890, 391);
            this.splitContainer1.SplitterDistance = 687;
            this.splitContainer1.SplitterWidth = 1;
            this.splitContainer1.TabIndex = 902;
            // 
            // dgvKishuZan
            // 
            this.dgvKishuZan.AllowUserToAddRows = false;
            this.dgvKishuZan.AllowUserToDeleteRows = false;
            this.dgvKishuZan.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvKishuZan.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.dgvKishuZan.Location = new System.Drawing.Point(5, 2);
            this.dgvKishuZan.MultiSelect = false;
            this.dgvKishuZan.Name = "dgvKishuZan";
            this.dgvKishuZan.ReadOnly = true;
            this.dgvKishuZan.RowHeadersVisible = false;
            this.dgvKishuZan.RowTemplate.Height = 21;
            this.dgvKishuZan.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvKishuZan.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvKishuZan.Size = new System.Drawing.Size(680, 386);
            this.dgvKishuZan.TabIndex = 0;
            this.dgvKishuZan.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellEnter);
            this.dgvKishuZan.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dgv_CellValidating);
            this.dgvKishuZan.Scroll += new System.Windows.Forms.ScrollEventHandler(this.dgvKishuZan_Scroll);
            this.dgvKishuZan.Enter += new System.EventHandler(this.dgv_Enter);
            // 
            // dgvHjkBmnKei
            // 
            this.dgvHjkBmnKei.AllowUserToAddRows = false;
            this.dgvHjkBmnKei.AllowUserToDeleteRows = false;
            this.dgvHjkBmnKei.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvHjkBmnKei.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.dgvHjkBmnKei.Location = new System.Drawing.Point(1, 2);
            this.dgvHjkBmnKei.MultiSelect = false;
            this.dgvHjkBmnKei.Name = "dgvHjkBmnKei";
            this.dgvHjkBmnKei.ReadOnly = true;
            this.dgvHjkBmnKei.RowHeadersVisible = false;
            this.dgvHjkBmnKei.RowTemplate.Height = 21;
            this.dgvHjkBmnKei.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvHjkBmnKei.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvHjkBmnKei.Size = new System.Drawing.Size(184, 385);
            this.dgvHjkBmnKei.TabIndex = 904;
            this.dgvHjkBmnKei.TabStop = false;
            // 
            // splitContainer2
            // 
            this.splitContainer2.IsSplitterFixed = true;
            this.splitContainer2.Location = new System.Drawing.Point(12, 428);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.dgvBmnKei);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.dgvKmkKei);
            this.splitContainer2.Size = new System.Drawing.Size(890, 41);
            this.splitContainer2.SplitterDistance = 687;
            this.splitContainer2.SplitterWidth = 1;
            this.splitContainer2.TabIndex = 903;
            // 
            // dgvBmnKei
            // 
            this.dgvBmnKei.AllowUserToAddRows = false;
            this.dgvBmnKei.AllowUserToDeleteRows = false;
            this.dgvBmnKei.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBmnKei.ColumnHeadersVisible = false;
            this.dgvBmnKei.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.dgvBmnKei.Location = new System.Drawing.Point(5, 1);
            this.dgvBmnKei.MultiSelect = false;
            this.dgvBmnKei.Name = "dgvBmnKei";
            this.dgvBmnKei.ReadOnly = true;
            this.dgvBmnKei.RowHeadersVisible = false;
            this.dgvBmnKei.RowTemplate.Height = 21;
            this.dgvBmnKei.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvBmnKei.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvBmnKei.Size = new System.Drawing.Size(680, 22);
            this.dgvBmnKei.TabIndex = 907;
            this.dgvBmnKei.TabStop = false;
            this.dgvBmnKei.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dgvBmnKei_CellPainting);
            // 
            // dgvKmkKei
            // 
            this.dgvKmkKei.AllowUserToAddRows = false;
            this.dgvKmkKei.AllowUserToDeleteRows = false;
            this.dgvKmkKei.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvKmkKei.ColumnHeadersVisible = false;
            this.dgvKmkKei.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.dgvKmkKei.Location = new System.Drawing.Point(1, 1);
            this.dgvKmkKei.MultiSelect = false;
            this.dgvKmkKei.Name = "dgvKmkKei";
            this.dgvKmkKei.ReadOnly = true;
            this.dgvKmkKei.RowHeadersVisible = false;
            this.dgvKmkKei.RowTemplate.Height = 21;
            this.dgvKmkKei.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvKmkKei.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvKmkKei.Size = new System.Drawing.Size(184, 22);
            this.dgvKmkKei.TabIndex = 908;
            this.dgvKmkKei.TabStop = false;
            // 
            // ZMCE1014
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(913, 575);
            this.Controls.Add(this.splitContainer2);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.txtKanjoKmk);
            this.Controls.Add(this.lblKanjoGokei);
            this.Controls.Add(this.lblHojoGokei);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblKanjoKmk);
            this.Name = "ZMCE1014";
            this.Text = "補助科目残高";
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblKanjoKmk, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.lblHojoGokei, 0);
            this.Controls.SetChildIndex(this.lblKanjoGokei, 0);
            this.Controls.SetChildIndex(this.txtKanjoKmk, 0);
            this.Controls.SetChildIndex(this.splitContainer1, 0);
            this.Controls.SetChildIndex(this.splitContainer2, 0);
            this.pnlDebug.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvKishuZan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHjkBmnKei)).EndInit();
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvBmnKei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvKmkKei)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private common.controls.FsiTextBox txtKanjoKmk;
        private System.Windows.Forms.Label lblKanjoKmk;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblHojoGokei;
        private System.Windows.Forms.Label lblKanjoGokei;
        private System.Windows.Forms.SplitContainer splitContainer1;
        //private System.Windows.Forms.DataGridView dgvKishuZan;
        //private jp.co.fsi.common.controls.SjDataGridViewEx dgvKishuZan;
        private DataGridViewEx dgvKishuZan;
        private System.Windows.Forms.DataGridView dgvHjkBmnKei;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.DataGridView dgvBmnKei;
        private System.Windows.Forms.DataGridView dgvKmkKei;
    }
}