﻿namespace jp.co.fsi.zm.zmce1011
{
    partial class ZMCE1012
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtKanjoKmk = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKanjoKmk = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dgvKishuZan = new System.Windows.Forms.DataGridView();
            this.label4 = new System.Windows.Forms.Label();
            this.lblHojoGokei = new System.Windows.Forms.Label();
            this.lblKanjoGokei = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvKishuZan)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(595, 23);
            this.lblTitle.Text = "補助科目残高";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(5, 366);
            this.pnlDebug.Size = new System.Drawing.Size(628, 100);
            // 
            // txtKanjoKmk
            // 
            this.txtKanjoKmk.AutoSizeFromLength = false;
            this.txtKanjoKmk.BackColor = System.Drawing.SystemColors.Window;
            this.txtKanjoKmk.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtKanjoKmk.DisplayLength = null;
            this.txtKanjoKmk.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKanjoKmk.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKanjoKmk.Location = new System.Drawing.Point(90, 14);
            this.txtKanjoKmk.MaxLength = 6;
            this.txtKanjoKmk.Name = "txtKanjoKmk";
            this.txtKanjoKmk.Size = new System.Drawing.Size(40, 16);
            this.txtKanjoKmk.TabIndex = 2;
            this.txtKanjoKmk.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblKanjoKmk
            // 
            this.lblKanjoKmk.BackColor = System.Drawing.SystemColors.Window;
            this.lblKanjoKmk.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKanjoKmk.Location = new System.Drawing.Point(130, 10);
            this.lblKanjoKmk.Name = "lblKanjoKmk";
            this.lblKanjoKmk.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblKanjoKmk.Size = new System.Drawing.Size(187, 23);
            this.lblKanjoKmk.TabIndex = 3;
            this.lblKanjoKmk.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(12, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 23);
            this.label1.TabIndex = 1;
            this.label1.Text = "勘定科目：";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.PaleTurquoise;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(72, 383);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(251, 23);
            this.label3.TabIndex = 7;
            this.label3.Text = "合　計";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dgvKishuZan
            // 
            this.dgvKishuZan.AllowUserToAddRows = false;
            this.dgvKishuZan.AllowUserToDeleteRows = false;
            this.dgvKishuZan.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvKishuZan.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.dgvKishuZan.Location = new System.Drawing.Point(12, 44);
            this.dgvKishuZan.MultiSelect = false;
            this.dgvKishuZan.Name = "dgvKishuZan";
            this.dgvKishuZan.ReadOnly = true;
            this.dgvKishuZan.RowHeadersVisible = false;
            this.dgvKishuZan.RowTemplate.Height = 21;
            this.dgvKishuZan.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvKishuZan.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvKishuZan.Size = new System.Drawing.Size(591, 339);
            this.dgvKishuZan.TabIndex = 5;
            this.dgvKishuZan.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellEnter);
            this.dgvKishuZan.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dgv_CellValidating);
            this.dgvKishuZan.Enter += new System.EventHandler(this.dgv_Enter);
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.PaleTurquoise;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.Location = new System.Drawing.Point(12, 383);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 23);
            this.label4.TabIndex = 6;
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblHojoGokei
            // 
            this.lblHojoGokei.BackColor = System.Drawing.Color.PaleTurquoise;
            this.lblHojoGokei.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblHojoGokei.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblHojoGokei.Location = new System.Drawing.Point(322, 383);
            this.lblHojoGokei.Name = "lblHojoGokei";
            this.lblHojoGokei.Size = new System.Drawing.Size(281, 23);
            this.lblHojoGokei.TabIndex = 8;
            this.lblHojoGokei.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblKanjoGokei
            // 
            this.lblKanjoGokei.BackColor = System.Drawing.SystemColors.Window;
            this.lblKanjoGokei.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKanjoGokei.Location = new System.Drawing.Point(377, 12);
            this.lblKanjoGokei.Name = "lblKanjoGokei";
            this.lblKanjoGokei.Size = new System.Drawing.Size(208, 23);
            this.lblKanjoGokei.TabIndex = 4;
            this.lblKanjoGokei.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ZMCE1012
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(620, 469);
            this.Controls.Add(this.txtKanjoKmk);
            this.Controls.Add(this.lblKanjoGokei);
            this.Controls.Add(this.lblHojoGokei);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.dgvKishuZan);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblKanjoKmk);
            this.Name = "ZMCE1012";
            this.Text = "補助科目残高";
            this.Shown += new System.EventHandler(this.frm_Shown);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblKanjoKmk, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.Controls.SetChildIndex(this.dgvKishuZan, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.Controls.SetChildIndex(this.lblHojoGokei, 0);
            this.Controls.SetChildIndex(this.lblKanjoGokei, 0);
            this.Controls.SetChildIndex(this.txtKanjoKmk, 0);
            this.pnlDebug.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvKishuZan)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private common.controls.FsiTextBox txtKanjoKmk;
        private System.Windows.Forms.Label lblKanjoKmk;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dgvKishuZan;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblHojoGokei;
        private System.Windows.Forms.Label lblKanjoGokei;


    }
}