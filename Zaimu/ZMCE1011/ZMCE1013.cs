﻿using System;
using System.Data;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.zm.zmce1011
{
    /// <summary>
    /// 補助科目残高(ZAMM4013)
    /// </summary>
    public partial class ZMCE1013 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// 明細行数
        /// </summary>
        private const int P_MAX_ROWS_CNT = 40;


        public const int HOJO_UMU_ARI = 1;
        public const int BUMON_UMU_ARI = 1;
        #endregion

        decimal SHISHO_CD;
        private ZMCE1011M com = new ZMCE1011M();
        private ZMCE1011M.KISHUZAN_HEAD gHead;

        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理用変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ZMCE1013()
        {
            InitializeComponent();
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // EscapeとF6のみ表示
            this.ShowFButton = true;
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF5.Location = this.btnF4.Location;
            this.btnF4.Location = this.btnF3.Location;
            this.btnF1.Location = this.btnF2.Location;

            this.btnF2.Visible = false;
            this.btnF3.Visible = false;
            this.btnF6.Visible = false;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;
            this.lblTitle.Visible = false;

            SHISHO_CD = Util.ToDecimal(this.Par2);

            // 初期フォーカス
            this.rdoGokei.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // 日付、仕入先コード１・２に
            // フォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtKanjoKamokuCdFr":
                case "txtKanjoKamokuCdTo":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;

            switch (this.ActiveCtlNm)
            {
                case "txtKanjoKamokuCdFr":
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("ZMCM1031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.zm.zmcm1031.ZMCM1033");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "2";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtKanjoKamokuCdFr.Text = outData[0];
                                this.lblKanjoKamokuNmFr.Text = outData[1];
                            }
                        }
                    }
                    break;
                case "txtKanjoKamokuCdTo":
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("ZMCM1031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.zm.zmcm1031.ZMCM1033");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "2";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtKanjoKamokuCdTo.Text = outData[0];
                                this.lblKanjoKamokuNmTo.Text = outData[1];
                            }
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
            {
                // プレビュー処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfNmYesNo("印刷", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            // 設定画面の起動
            // MEMO:原則としてここで渡す帳票IDの設定はReport.csvに保持していることが前提ですが、
            // 保持していない場合は、設定画面での保存(F6)時に新規に設定が保持されます。
            PrintSettingForm psForm = new PrintSettingForm(new string[1] { "ZMCE1011R" });
            psForm.ShowDialog();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 勘定科目コード(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKanjoKamokuCdFr_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!IsValidKanjoKamokuCdFr())
            {
                e.Cancel = true;
                this.txtKanjoKamokuCdFr.SelectAll();
            }
        }

        /// <summary>
        /// 勘定科目コード(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKanjoKamokuCdTo_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!IsValidKanjoKamokuCdTo())
            {
                e.Cancel = true;
                this.txtKanjoKamokuCdTo.SelectAll();

                // Enter処理を無効化
                this._dtFlg = false;
            }
            else
            {
                // Enter処理を有効化
                this._dtFlg = true;
            }
        }

        /// <summary>
        /// 勘定科目コード(至)のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKanjoKamokuCdTo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Flg)
            {
                // Enter処理を無効化
                this._dtFlg = false;

                // 全項目を再度入力値チェック
                if (!ValidateAll())
                {
                    // エラーありの場合ここで処理終了
                    return;
                }

                if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
                {
                    // ﾌﾟﾚﾋﾞｭｰ処理
                    DoPrint(true);
                }
                else
                {
                    this.txtKanjoKamokuCdTo.Focus();
                }
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 勘定科目コード(自)の入力チェック
        /// </summary>
        private bool IsValidKanjoKamokuCdFr()
        {
            if (ValChk.IsEmpty(this.txtKanjoKamokuCdFr.Text))
            {
                this.lblKanjoKamokuNmFr.Text = "先　頭";
            }
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            else if (!ValChk.IsNumber(this.txtKanjoKamokuCdFr.Text))
            {
                Msg.Error("勘定科目コードは数値のみで入力してください。");
                return false;
            }
            // 指定文字数を超えていたらエラー
            else if (!ValChk.IsWithinLength(this.txtKanjoKamokuCdFr.Text, this.txtKanjoKamokuCdFr.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            else
            {
                // コードを元に名称を取得する
                this.lblKanjoKamokuNmFr.Text = this.Dba.GetName(this.UInfo, "TB_ZM_KANJO_KAMOKU", "", this.txtKanjoKamokuCdFr.Text);
            }

            return true;
        }

        /// <summary>
        /// 勘定科目コード(至)の入力チェック
        /// </summary>
        private bool IsValidKanjoKamokuCdTo()
        {
            if (ValChk.IsEmpty(this.txtKanjoKamokuCdTo.Text))
            {
                this.lblKanjoKamokuNmTo.Text = "最　後";
            }
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            else if (!ValChk.IsNumber(this.txtKanjoKamokuCdTo.Text))
            {
                Msg.Error("勘定科目コードは数値のみで入力してください。");
                return false;
            }
            // 指定文字数を超えていたらエラー
            else if (!ValChk.IsWithinLength(this.txtKanjoKamokuCdTo.Text, this.txtKanjoKamokuCdTo.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            else
            {
                // コードを元に名称を取得する
                this.lblKanjoKamokuNmTo.Text = this.Dba.GetName(this.UInfo, "TB_ZM_KANJO_KAMOKU", "",this.txtKanjoKamokuCdTo.Text);
            }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 勘定科目コード(自)の入力チェック
            if (!IsValidKanjoKamokuCdFr())
            {
                this.txtKanjoKamokuCdFr.Focus();
                this.txtKanjoKamokuCdFr.SelectAll();
                return false;
            }
            // 勘定科目コード(至)の入力チェック
            if (!IsValidKanjoKamokuCdTo())
            {
                this.txtKanjoKamokuCdTo.Focus();
                this.txtKanjoKamokuCdTo.SelectAll();
                return false;
            }

            return true;
        }

        /// <summary>
        /// 帳票を印刷する。
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview)
        {
            try
            {
                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                //bool dataFlag = MakeWkData();
                bool dataFlag = MakeWkData01();

                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    StringBuilder cols = new StringBuilder();
                    cols.Append("  ITEM01");
                    cols.Append(" ,ITEM02");
                    cols.Append(" ,ITEM03");
                    cols.Append(" ,ITEM04");
                    cols.Append(" ,ITEM05");
                    cols.Append(" ,ITEM06");
                    cols.Append(" ,ITEM07");
                    cols.Append(" ,ITEM08");
                    cols.Append(" ,ITEM09");
                    cols.Append(" ,ITEM10");

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                    // データの取得
                    DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_ZM_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    if (isPreview)
                    {
                        // 帳票オブジェクトをインスタンス化
                        ZMCE1011R rpt = new ZMCE1011R(dtOutput);
                        PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                        pFrm.WindowState = FormWindowState.Maximized;
                        // プレビュー画面表示
                        pFrm.Show();
                    }
                    else
                    {
                        // 帳票オブジェクトをインスタンス化
                        ZMCE1011R rpt = new ZMCE1011R(dtOutput);
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
            }
            finally
            {
                this.Dba.Rollback();
            }
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {
            #region 準備
            // 勘定科目コード設定
            String kanjoKamokuFr;
            String kanjoKamokuTo;
            if (Util.ToString(this.txtKanjoKamokuCdFr.Text) != "")
            {
                kanjoKamokuFr = this.txtKanjoKamokuCdFr.Text;
            }
            else
            {
                kanjoKamokuFr = "0";
            }
            if (Util.ToString(this.txtKanjoKamokuCdTo.Text) != "")
            {
                kanjoKamokuTo = this.txtKanjoKamokuCdTo.Text;
            }
            else
            {
                kanjoKamokuTo = "9999";
            }
            #endregion

            #region メインデータを取得
            StringBuilder Sql;
            // 明細出力方法で、科目合計が選択された場合
            if (this.rdoGokei.Checked)
            {
                Sql = MakeSql_gokei();
            }
            // 科目内訳が選択された場合
            else
            {
                Sql = MakeSql_uchiwake();
            }

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@KANJO_KAMOKU_CD_FR", SqlDbType.VarChar, 6, kanjoKamokuFr);
            dpc.SetParam("@KANJO_KAMOKU_CD_TO", SqlDbType.VarChar, 6, kanjoKamokuTo);

            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            #endregion

            if (dtMainLoop.Rows.Count > 0)
            {
                #region 印刷ワークテーブルに登録
                #region 準備
                // 会社情報テーブルより、期首を取得
                String getKishu = get_kishu();
                String f = "yyyy/MM/dd H:mm:ss"; // 取得日付の形式
                DateTime dtKishu = DateTime.ParseExact(getKishu, f, null); // DateTime型に変換
                //　和暦に変換
                CultureInfo culture = new CultureInfo("ja-JP", true);
                culture.DateTimeFormat.Calendar = new JapaneseCalendar();
                String kishu = dtKishu.ToString("ggyy年MM月dd日", culture);

                // メインループカウント用変数
                int cnt = 0;
                // 明細出力方法設定
                String meisai;
                if (this.rdoGokei.Checked)
                {
                    meisai = "【科目合計】";
                }
                else
                {
                    meisai = "【科目内訳】";
                }

                // 科目明細のサブデータ用変数
                DataTable dtSubLoop = new DataTable();
                #endregion

                #region インサートテーブル
                StringBuilder Sql_ins = new StringBuilder();
                Sql_ins.Append("INSERT INTO PR_ZM_TBL(");
                Sql_ins.Append("  GUID");
                Sql_ins.Append(" ,SORT");
                Sql_ins.Append(" ,ITEM01");
                Sql_ins.Append(" ,ITEM02");
                Sql_ins.Append(" ,ITEM03");
                Sql_ins.Append(" ,ITEM04");
                Sql_ins.Append(" ,ITEM05");
                Sql_ins.Append(" ,ITEM06");
                Sql_ins.Append(" ,ITEM07");
                Sql_ins.Append(" ,ITEM08");
                Sql_ins.Append(" ,ITEM09");
                Sql_ins.Append(" ,ITEM10");
                Sql_ins.Append(") ");
                Sql_ins.Append("VALUES(");
                Sql_ins.Append("  @GUID");
                Sql_ins.Append(" ,@SORT");
                Sql_ins.Append(" ,@ITEM01");
                Sql_ins.Append(" ,@ITEM02");
                Sql_ins.Append(" ,@ITEM03");
                Sql_ins.Append(" ,@ITEM04");
                Sql_ins.Append(" ,@ITEM05");
                Sql_ins.Append(" ,@ITEM06");
                Sql_ins.Append(" ,@ITEM07");
                Sql_ins.Append(" ,@ITEM08");
                Sql_ins.Append(" ,@ITEM09");
                Sql_ins.Append(" ,@ITEM10");
                Sql_ins.Append(") ");
                #endregion

                foreach (DataRow dr in dtMainLoop.Rows)
                {
                    // 発生額ゼロを印字しない場合
                    if (this.rdoNashi.Checked)
                    {
                        // 期首残高が無い場合、処理を行わない
                        if (Util.ToString(dr["KISHU_ZANDAKA"]) == "")
                        {
                            continue;
                        }
                    }
                    else
                    {
                        // 期首残高が無い場合、処理を行わない
                        if (Util.ToString(dr["KISHU_ZANDAKA"]) == "")
                        {
                            dr["KISHU_ZANDAKA"] = 0;
                        }
                    }

                    #region データの登録
                    dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                    dpc.SetParam("@SORT", SqlDbType.VarChar, 10, cnt);
                    // ページヘッダーデータを設定
                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, "決算期:第 " + this.UInfo.KessanKi + "期"); // 決算期
                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, "期首：" + kishu); // 期首
                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, meisai); // 明細出力方法
                    // データを設定
                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, dr["KANJO_KAMOKU_CD"]);
                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, dr["KANJO_KAMOKU_NM"]);
                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, dr["TAISHAKU_KUBUN_NM"]);
                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, dr["HOJO_KAMOKU_CD"]);
                    dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, dr["HOJO_KAMOKU_NM"]);
                    dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, dr["KISHU_ZANDAKA"]);

                    this.Dba.ModifyBySql(Util.ToString(Sql_ins), dpc);

                    cnt++;
                    #endregion

                    #region 科目内訳が選択 かつ 補助科目有無が1の場合
                    if (this.rdoUchiwake.Checked && Util.ToInt(dr["HOJO_KAMOKU_UMU"]) == 1)
                    {
                        // 補助使用区分が1の場合
                        if (Util.ToInt(dr["HOJO_SHIYO_KUBUN"]) == 1)
                        {
                            Sql = MakeSql_uchiwake_kubun1();

                            dpc = new DbParamCollection();
                            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
                            dpc.SetParam("@KANJO_KAMOKU_CD", SqlDbType.VarChar, 6, Util.ToString(dr["KANJO_KAMOKU_CD"]));

                            dtSubLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
                        }
                        // 補助使用区分が2の場合
                        else if (Util.ToInt(dr["HOJO_SHIYO_KUBUN"]) == 2)
                        {
                            Sql = MakeSql_uchiwake_kubun2();

                            dpc = new DbParamCollection();
                            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
                            dpc.SetParam("@KANJO_KAMOKU_CD", SqlDbType.VarChar, 6, Util.ToString(dr["KANJO_KAMOKU_CD"]));

                            dtSubLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
                        }

                        foreach (DataRow drSub in dtSubLoop.Rows)
                        {
                            // 発生額ゼロを印字しない場合
                            if (this.rdoNashi.Checked)
                            {
                                // 期首残高が無い場合、処理を行わない
                                if (Util.ToString(drSub["KISHU_ZANDAKA"]) == "" || Util.ToString(drSub["KISHU_ZANDAKA"]) == "0")
                                {
                                    continue;
                                }
                            }
                            else
                            {
                                // 期首残高が無い場合、処理を行わない
                                if (Util.ToString(drSub["KISHU_ZANDAKA"]) == "")
                                {
                                    drSub["KISHU_ZANDAKA"] = 0;
                                }
                            }

                            #region データの登録
                            dpc = new DbParamCollection();
                            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                            dpc.SetParam("@SORT", SqlDbType.VarChar, 10, cnt);
                            // ページヘッダーデータを設定
                            dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                            dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, "決算期:第 " + this.UInfo.KessanKi + "期"); // 決算期
                            dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, "期首：" + kishu); // 期首
                            dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, meisai); // 明細出力方法
                            // データを設定
                            // 各ページの最初の行の場合、勘定科目を表示する
                            if (cnt % P_MAX_ROWS_CNT == 0)
                            {
                                dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, dr["KANJO_KAMOKU_CD"]);
                                dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, dr["KANJO_KAMOKU_NM"]);
                                dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, dr["TAISHAKU_KUBUN_NM"]);
                            }
                            else
                            {
                                dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, "");
                                dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, "");
                                dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, "");
                            }
                            dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, drSub["HOJO_KAMOKU_CD"]);
                            dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, drSub["HOJO_KAMOKU_NM"]);
                            dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, drSub["KISHU_ZANDAKA"]);

                            this.Dba.ModifyBySql(Util.ToString(Sql_ins), dpc);

                            cnt++;
                            #endregion
                        }
                    }
                    #endregion
                }
                #endregion
            }

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_ZM_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_ZM_TBL",
                "GUID = @GUID",
                dpc);

            bool dataFlag;
            if (tmpdtPR_ZM_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                Msg.Info("該当データがありません。");
                dataFlag = false;
            }

            return dataFlag;
        }

        /// <summary>
        /// 科目合計のメインデータ取得SQLを作成します。
        /// </summary>
        private StringBuilder MakeSql_gokei()
        {
            StringBuilder Sql = new StringBuilder();

            //Sql.Append("SELECT");
            //Sql.Append(" A.KANJO_KAMOKU_CD AS KANJO_KAMOKU_CD,");
            //Sql.Append(" A.KANJO_KAMOKU_NM AS KANJO_KAMOKU_NM,");
            //Sql.Append(" B.TAISHAKU_KUBUN_NM AS TAISHAKU_KUBUN_NM,");
            //Sql.Append(" '' AS HOJO_KAMOKU_CD,");
            //Sql.Append(" '' AS HOJO_KAMOKU_NM,");
            //Sql.Append(" (SELECT");
            //Sql.Append("  SUM(CASE WHEN SA.TAISHAKU_KUBUN = SB.TAISHAKU_KUBUN THEN SA.ZEIKOMI_KINGAKU ELSE (SA.ZEIKOMI_KINGAKU * -1) END)");
            //Sql.Append(" FROM TB_ZM_SHIWAKE_MEISAI AS SA");
            //Sql.Append(" LEFT OUTER JOIN TB_ZM_KANJO_KAMOKU AS SB");
            //Sql.Append("  ON SA.KAISHA_CD = SB.KAISHA_CD AND");
            //Sql.Append("   SA.KAIKEI_NENDO = SB.KAIKEI_NENDO AND");
            //Sql.Append("   SA.KANJO_KAMOKU_CD = SB.KANJO_KAMOKU_CD");
            //Sql.Append(" WHERE");
            //Sql.Append("  SA.KAISHA_CD = A.KAISHA_CD AND");
            //Sql.Append("  SA.KAIKEI_NENDO = A.KAIKEI_NENDO AND");
            //Sql.Append("  SA.KANJO_KAMOKU_CD = A.KANJO_KAMOKU_CD AND");
            //Sql.Append("  SA.SHISHO_CD = @SHISHO_CD AND");
            //Sql.Append("  SA.DENPYO_BANGO = 0 AND");
            //Sql.Append("  SA.DENPYO_KUBUN = 0 ) AS KISHU_ZANDAKA ");
            //Sql.Append("FROM TB_ZM_KANJO_KAMOKU AS A ");
            //Sql.Append("LEFT OUTER JOIN TB_ZM_F_TAISHAKU_KUBUN AS B");
            //Sql.Append(" ON A.TAISHAKU_KUBUN = B.TAISHAKU_KUBUN ");
            //Sql.Append("WHERE");
            //Sql.Append(" A.KAISHA_CD = @KAISHA_CD AND");
            //Sql.Append(" A.KAIKEI_NENDO = @KAIKEI_NENDO AND");
            //Sql.Append(" A.KANJO_KAMOKU_CD BETWEEN @KANJO_KAMOKU_CD_FR AND @KANJO_KAMOKU_CD_TO AND");
            //Sql.Append(" (A.KAMOKU_BUNRUI_CD BETWEEN 10000 AND 19999 OR A.KAMOKU_BUNRUI_CD = 22400) ");
            //Sql.Append("ORDER BY");
            //Sql.Append(" A.KAISHA_CD,");
            //Sql.Append(" A.KAMOKU_BUNRUI_CD,");
            //Sql.Append(" A.KANJO_KAMOKU_CD");

            return Sql;
        }

        /// <summary>
        /// 科目明細のメインデータ取得SQLを作成します。
        /// </summary>
        private StringBuilder MakeSql_uchiwake()
        {
            StringBuilder Sql = new StringBuilder();

            //Sql.Append("SELECT");
            ////Sql.Append(" A.KAMOKU_BUNRUI_CD AS KAMOKU_BUNRUI_CD,");
            //Sql.Append(" A.KANJO_KAMOKU_CD AS KANJO_KAMOKU_CD,");
            //Sql.Append(" A.KANJO_KAMOKU_NM AS KANJO_KAMOKU_NM,");
            ////Sql.Append(" A.TAISHAKU_KUBUN AS TAISHAKU_KUBUN,");
            //Sql.Append(" B.TAISHAKU_KUBUN_NM AS TAISHAKU_KUBUN_NM,");
            //Sql.Append(" '' AS HOJO_KAMOKU_CD,");
            //Sql.Append(" '' AS HOJO_KAMOKU_NM,");
            //Sql.Append(" A.HOJO_KAMOKU_UMU AS HOJO_KAMOKU_UMU,");
            //Sql.Append(" A.HOJO_SHIYO_KUBUN AS HOJO_SHIYO_KUBUN,");
            ////Sql.Append(" A.BUMON_UMU AS BUMON_UMU,");
            ////Sql.Append(" A.KOJI_UMU AS KOJI_UMU,");
            //Sql.Append(" (SELECT");
            //Sql.Append("  SUM(CASE WHEN SA.TAISHAKU_KUBUN = SB.TAISHAKU_KUBUN THEN SA.ZEIKOMI_KINGAKU ELSE (SA.ZEIKOMI_KINGAKU * -1) END)");
            //Sql.Append(" FROM TB_ZM_SHIWAKE_MEISAI AS SA");
            //Sql.Append(" LEFT OUTER JOIN TB_ZM_KANJO_KAMOKU AS SB");
            //Sql.Append("  ON SA.KAISHA_CD = SB.KAISHA_CD AND");
            //Sql.Append("   SA.KAIKEI_NENDO = SB.KAIKEI_NENDO AND");
            //Sql.Append("   SA.KANJO_KAMOKU_CD = SB.KANJO_KAMOKU_CD");
            //Sql.Append(" WHERE");
            //Sql.Append("  SA.KAISHA_CD = A.KAISHA_CD AND");
            //Sql.Append("  SA.KAIKEI_NENDO = A.KAIKEI_NENDO AND");
            //Sql.Append("  SA.KANJO_KAMOKU_CD = A.KANJO_KAMOKU_CD AND");
            //Sql.Append("  SA.SHISHO_CD = @SHISHO_CD AND");
            //Sql.Append("  SA.DENPYO_BANGO = 0 AND");
            //Sql.Append("  SA.DENPYO_KUBUN = 0 ) AS KISHU_ZANDAKA ");
            //Sql.Append("FROM TB_ZM_KANJO_KAMOKU AS A ");
            //Sql.Append("LEFT OUTER JOIN TB_ZM_F_TAISHAKU_KUBUN AS B");
            //Sql.Append(" ON A.TAISHAKU_KUBUN = B.TAISHAKU_KUBUN ");
            //Sql.Append("WHERE");
            //Sql.Append(" A.KAISHA_CD = @KAISHA_CD AND");
            //Sql.Append(" A.KAIKEI_NENDO = @KAIKEI_NENDO AND");
            //Sql.Append(" A.KANJO_KAMOKU_CD BETWEEN @KANJO_KAMOKU_CD_FR AND @KANJO_KAMOKU_CD_TO AND");
            //Sql.Append(" (A.KAMOKU_BUNRUI_CD BETWEEN 10000 AND  19999 OR A.KAMOKU_BUNRUI_CD = 22400) ");
            //Sql.Append("ORDER BY");
            //Sql.Append(" A.KAISHA_CD,");
            //Sql.Append(" A.KAMOKU_BUNRUI_CD,");
            //Sql.Append(" A.KANJO_KAMOKU_CD");

            return Sql;
        }

        /// <summary>
        /// 科目明細のサブデータ取得SQLを作成します。
        /// </summary>
        private StringBuilder MakeSql_uchiwake_kubun1()
        {
            StringBuilder Sql = new StringBuilder();

            //Sql.Append("SELECT");
            //Sql.Append(" A.HOJO_KAMOKU_CD AS HOJO_KAMOKU_CD,");
            //Sql.Append(" A.HOJO_KAMOKU_NM AS HOJO_KAMOKU_NM,");
            //Sql.Append(" (SELECT");
            //Sql.Append("  SUM(CASE WHEN SA.TAISHAKU_KUBUN = SB.TAISHAKU_KUBUN THEN SA.ZEIKOMI_KINGAKU ELSE (SA.ZEIKOMI_KINGAKU * -1) END)");
            //Sql.Append(" FROM TB_ZM_SHIWAKE_MEISAI AS SA");
            //Sql.Append(" LEFT OUTER JOIN TB_ZM_KANJO_KAMOKU AS SB");
            //Sql.Append("  ON SA.KAISHA_CD = SB.KAISHA_CD AND");
            //Sql.Append("   SA.KAIKEI_NENDO = SB.KAIKEI_NENDO AND");
            //Sql.Append("   SA.KANJO_KAMOKU_CD = SB.KANJO_KAMOKU_CD");
            //Sql.Append(" WHERE");
            //Sql.Append("  SA.KAISHA_CD = A.KAISHA_CD AND");
            //Sql.Append("  SA.KAIKEI_NENDO = A.KAIKEI_NENDO AND");
            //Sql.Append("  SA.KANJO_KAMOKU_CD = A.KANJO_KAMOKU_CD AND");
            //Sql.Append("  SA.HOJO_KAMOKU_CD = A.HOJO_KAMOKU_CD AND");
            //Sql.Append("  SA.DENPYO_BANGO = 0 AND");
            //Sql.Append("  SA.DENPYO_KUBUN = 0 ) AS KISHU_ZANDAKA ");
            //Sql.Append("FROM TB_ZM_HOJO_KAMOKU AS A ");
            //Sql.Append("WHERE");
            //Sql.Append(" A.KAISHA_CD = @KAISHA_CD AND");
            //Sql.Append(" A.KAIKEI_NENDO = @KAIKEI_NENDO AND");
            //Sql.Append(" A.KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD ");
            //Sql.Append("ORDER BY");
            //Sql.Append(" A.KAISHA_CD,");
            //Sql.Append(" A.HOJO_KAMOKU_CD");

            return Sql;
        }

        /// <summary>
        /// 科目明細のサブデータ取得SQLを作成します。
        /// </summary>
        private StringBuilder MakeSql_uchiwake_kubun2()
        {
            StringBuilder Sql = new StringBuilder();

            //Sql.Append("SELECT");
            //Sql.Append(" A.TORIHIKISAKI_CD AS HOJO_KAMOKU_CD,");
            //Sql.Append(" A.TORIHIKISAKI_NM AS HOJO_KAMOKU_NM,");
            //Sql.Append(" (SELECT");
            //Sql.Append("  SUM(CASE WHEN SA.TAISHAKU_KUBUN = SB.TAISHAKU_KUBUN THEN SA.ZEIKOMI_KINGAKU ELSE (SA.ZEIKOMI_KINGAKU * -1) END)");
            //Sql.Append(" FROM TB_ZM_SHIWAKE_MEISAI AS SA");
            //Sql.Append(" LEFT OUTER JOIN TB_ZM_KANJO_KAMOKU AS SB");
            //Sql.Append("  ON SA.KAISHA_CD = SB.KAISHA_CD AND");
            //Sql.Append("   SA.KAIKEI_NENDO = SB.KAIKEI_NENDO AND");
            //Sql.Append("   SA.KANJO_KAMOKU_CD = SB.KANJO_KAMOKU_CD");
            //Sql.Append(" WHERE");
            //Sql.Append("  SA.KAISHA_CD = A.KAISHA_CD AND");
            //Sql.Append("  SA.KAIKEI_NENDO = @KAIKEI_NENDO AND");
            //Sql.Append("  SA.KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD AND");
            //Sql.Append("  SA.HOJO_KAMOKU_CD = A.TORIHIKISAKI_CD AND");
            //Sql.Append("  SA.DENPYO_BANGO = 0 AND");
            //Sql.Append("  SA.DENPYO_KUBUN = 0 ) AS KISHU_ZANDAKA ");
            //Sql.Append("FROM TB_CM_TORIHIKISAKI AS A ");
            //Sql.Append("WHERE");
            //Sql.Append(" A.KAISHA_CD = @KAISHA_CD ");
            //Sql.Append("ORDER BY");
            //Sql.Append(" A.KAISHA_CD,");
            //Sql.Append(" A.TORIHIKISAKI_CD");

            return Sql;
        }

        /// <summary>
        /// 会社情報テーブルより、期首を取得します。
        /// </summary>
        private String get_kishu()
        {
            StringBuilder Sql = new StringBuilder();
            DbParamCollection dpc = new DbParamCollection();

            Sql.Append("SELECT");
            Sql.Append(" KAIKEI_KIKAN_KAISHIBI ");
            Sql.Append("FROM");
            Sql.Append(" TB_ZM_KAISHA_JOHO ");
            Sql.Append("WHERE");
            Sql.Append(" KAISHA_CD = @KAISHA_CD AND");
            Sql.Append(" KAIKEI_NENDO = @KAIKEI_NENDO");

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);

            DataTable dt = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            String kishu = "";
            if (dt.Rows.Count > 0)
            {
                kishu = Util.ToString(dt.Rows[0]["KAIKEI_KIKAN_KAISHIBI"]);
            }

            return kishu;
        }

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData01()
        {
            #region 準備
            // 勘定科目コード設定
            String kanjoKamokuFr;
            String kanjoKamokuTo;
            if (Util.ToString(this.txtKanjoKamokuCdFr.Text) != "")
            {
                kanjoKamokuFr = this.txtKanjoKamokuCdFr.Text;
            }
            else
            {
                kanjoKamokuFr = "0";
            }
            if (Util.ToString(this.txtKanjoKamokuCdTo.Text) != "")
            {
                kanjoKamokuTo = this.txtKanjoKamokuCdTo.Text;
            }
            else
            {
                kanjoKamokuTo = "9999";
            }
            #endregion

            #region メインデータを取得
            StringBuilder Sql = new StringBuilder();
            //// 明細出力方法で、科目合計が選択された場合
            //if (this.rdoGokei.Checked)
            //{
            //    Sql = MakeSql_gokei();
            //}
            //// 科目内訳が選択された場合
            //else
            //{
            //    Sql = MakeSql_uchiwake();
            //}
            Sql.Append(com.GetKomokuStiSql(1));

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, SHISHO_CD);
            dpc.SetParam("@KANJO_KAMOKU_CD_FR", SqlDbType.VarChar, 6, kanjoKamokuFr);
            dpc.SetParam("@KANJO_KAMOKU_CD_TO", SqlDbType.VarChar, 6, kanjoKamokuTo);

            DataTable dtMainLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            #endregion

            if (dtMainLoop.Rows.Count > 0)
            {
                #region 印刷ワークテーブルに登録
                #region 準備
                // 会社情報テーブルより、期首を取得
                String getKishu = get_kishu();
                String f = "yyyy/MM/dd H:mm:ss"; // 取得日付の形式
                DateTime dtKishu = DateTime.ParseExact(getKishu, f, null); // DateTime型に変換
                //　和暦に変換
                CultureInfo culture = new CultureInfo("ja-JP", true);
                culture.DateTimeFormat.Calendar = new JapaneseCalendar();
                String kishu = dtKishu.ToString("ggyy年MM月dd日", culture);

                // メインループカウント用変数
                int cnt = 0;
                // 明細出力方法設定
                String meisai;
                if (this.rdoGokei.Checked)
                {
                    meisai = "【科目合計】";
                }
                else
                {
                    meisai = "【科目内訳】";
                }

                // 科目明細のサブデータ用変数
                DataTable dtSubLoop = new DataTable();
                #endregion

                #region インサートテーブル
                StringBuilder Sql_ins = new StringBuilder();
                Sql_ins.Append("INSERT INTO PR_ZM_TBL(");
                Sql_ins.Append("  GUID");
                Sql_ins.Append(" ,SORT");
                Sql_ins.Append(" ,ITEM01");
                Sql_ins.Append(" ,ITEM02");
                Sql_ins.Append(" ,ITEM03");
                Sql_ins.Append(" ,ITEM04");
                Sql_ins.Append(" ,ITEM05");
                Sql_ins.Append(" ,ITEM06");
                Sql_ins.Append(" ,ITEM07");
                Sql_ins.Append(" ,ITEM08");
                Sql_ins.Append(" ,ITEM09");
                Sql_ins.Append(" ,ITEM10");
                Sql_ins.Append(") ");
                Sql_ins.Append("VALUES(");
                Sql_ins.Append("  @GUID");
                Sql_ins.Append(" ,@SORT");
                Sql_ins.Append(" ,@ITEM01");
                Sql_ins.Append(" ,@ITEM02");
                Sql_ins.Append(" ,@ITEM03");
                Sql_ins.Append(" ,@ITEM04");
                Sql_ins.Append(" ,@ITEM05");
                Sql_ins.Append(" ,@ITEM06");
                Sql_ins.Append(" ,@ITEM07");
                Sql_ins.Append(" ,@ITEM08");
                Sql_ins.Append(" ,@ITEM09");
                Sql_ins.Append(" ,@ITEM10");
                Sql_ins.Append(") ");
                #endregion

                foreach (DataRow dr in dtMainLoop.Rows)
                {
                    // 発生額ゼロを印字しない場合
                    if (this.rdoNashi.Checked)
                    {
                        // 期首残高が無い場合、処理を行わない
                        if (Util.ToString(dr["KISHU_ZANDAKA"]) == "")
                        {
                            continue;
                        }
                    }
                    else
                    {
                        // 期首残高が無い場合、処理を行わない
                        if (Util.ToString(dr["KISHU_ZANDAKA"]) == "")
                        {
                            dr["KISHU_ZANDAKA"] = 0;
                        }
                    }

                    #region データの登録
                    dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                    dpc.SetParam("@SORT", SqlDbType.VarChar, 10, cnt);
                    // ページヘッダーデータを設定
                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, "決算期:第 " + this.UInfo.KessanKi + "期"); // 決算期
                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, "期首：" + kishu); // 期首
                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, meisai); // 明細出力方法
                    // データを設定
                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, dr["KANJO_KAMOKU_CD"]);
                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, dr["KANJO_KAMOKU_NM"]);
                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, dr["TAISHAKU_KUBUN_NM"]);
                    //dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, dr["HOJO_KAMOKU_CD"]);
                    //dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, dr["HOJO_KAMOKU_NM"]);
                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, "");
                    dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, "");
                    dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, dr["KISHU_ZANDAKA"]);

                    this.Dba.ModifyBySql(Util.ToString(Sql_ins), dpc);

                    cnt++;
                    #endregion
                    gHead.KANJO_KAMOKU_CD = Util.ToInt(dr["KANJO_KAMOKU_CD"]);
                    gHead.KANJO_KAMOKU_NM = Util.ToString(dr["KANJO_KAMOKU_NM"]);
                    gHead.TAISHAKU_KUBUN = Util.ToInt(dr["TAISHAKU_KUBUN"]);
                    gHead.KISHU_ZANDAKA = Util.ToDecimal(dr["KISHU_ZANDAKA"]);
                    gHead.HOJO_SHIYO_KUBUN = Util.ToInt(dr["HOJO_SHIYO_KUBUN"]);
                    gHead.HOJO_KAMOKU_UMU = Util.ToInt(dr["HOJO_KAMOKU_UMU"]);
                    gHead.BUMON_UMU = Util.ToInt(dr["BUMON_UMU"]);

                    #region 科目内訳が選択 かつ 補助科目有無が1の場合
                    if (this.rdoUchiwake.Checked)
                    {
                        Sql = new StringBuilder(); 
                        dpc = new DbParamCollection();
                        dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                        dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, SHISHO_CD);
                        dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
                        dpc.SetParam("@KANJO_KAMOKU_CD", SqlDbType.VarChar, 6, Util.ToString(gHead.KANJO_KAMOKU_CD));

                        // 補助科目有り、部門有り
                        if (gHead.BUMON_UMU == BUMON_UMU_ARI && gHead.HOJO_KAMOKU_UMU == HOJO_UMU_ARI)
                        {
                            Sql.Append(com.GetHjkBmnZan(gHead.KANJO_KAMOKU_CD, gHead.HOJO_SHIYO_KUBUN));

                            dtSubLoop = this.Dba.GetDataTableFromSqlWithParams(Sql.ToString(), dpc);
                        }
                        else if (gHead.BUMON_UMU == BUMON_UMU_ARI || gHead.HOJO_KAMOKU_UMU == HOJO_UMU_ARI)
                        {
                            if (gHead.HOJO_KAMOKU_UMU == HOJO_UMU_ARI)
                            {
                                // 補助科目あり
                                Sql.Append(com.GetHjkZan(gHead.KANJO_KAMOKU_CD, gHead.HOJO_SHIYO_KUBUN));

                                dtSubLoop = this.Dba.GetDataTableFromSqlWithParams(Sql.ToString(), dpc);
                            }
                            else
                            {
                                // 部門有り
                                Sql.Append(com.GetBmnZan(gHead.KANJO_KAMOKU_CD));

                                dtSubLoop = this.Dba.GetDataTableFromSqlWithParams(Sql.ToString(), dpc);
                            }
                        }

                    }

                    //if (this.rdoUchiwake.Checked && Util.ToInt(dr["HOJO_KAMOKU_UMU"]) == 1)
                    //{
                    //    // 補助使用区分が1の場合
                    //    if (Util.ToInt(dr["HOJO_SHIYO_KUBUN"]) == 1)
                    //    {
                    //        Sql = MakeSql_uchiwake_kubun1();

                    //        dpc = new DbParamCollection();
                    //        dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                    //        dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
                    //        dpc.SetParam("@KANJO_KAMOKU_CD", SqlDbType.VarChar, 4, Util.ToString(dr["KANJO_KAMOKU_CD"]));

                    //        dtSubLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
                    //    }
                    //    // 補助使用区分が2の場合
                    //    else if (Util.ToInt(dr["HOJO_SHIYO_KUBUN"]) == 2)
                    //    {
                    //        Sql = MakeSql_uchiwake_kubun2();

                    //        dpc = new DbParamCollection();
                    //        dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                    //        dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
                    //        dpc.SetParam("@KANJO_KAMOKU_CD", SqlDbType.VarChar, 4, Util.ToString(dr["KANJO_KAMOKU_CD"]));

                    //        dtSubLoop = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
                    //    }

                        foreach (DataRow drSub in dtSubLoop.Rows)
                        {
                            // 発生額ゼロを印字しない場合
                            if (this.rdoNashi.Checked)
                            {
                                // 期首残高が無い場合、処理を行わない
                                if (Util.ToString(drSub["KISHU_ZANDAKA"]) == "" || Util.ToString(drSub["KISHU_ZANDAKA"]) == "0")
                                {
                                    continue;
                                }
                            }
                            else
                            {
                                // 期首残高が無い場合、処理を行わない
                                if (Util.ToString(drSub["KISHU_ZANDAKA"]) == "")
                                {
                                    drSub["KISHU_ZANDAKA"] = 0;
                                }
                            }

                            #region データの登録
                            dpc = new DbParamCollection();
                            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                            dpc.SetParam("@SORT", SqlDbType.VarChar, 10, cnt);
                            // ページヘッダーデータを設定
                            dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                            dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, "決算期:第 " + this.UInfo.KessanKi + "期"); // 決算期
                            dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, "期首：" + kishu); // 期首
                            dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, meisai); // 明細出力方法
                            // データを設定
                            // 各ページの最初の行の場合、勘定科目を表示する
                            if (cnt % P_MAX_ROWS_CNT == 0)
                            {
                                dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, dr["KANJO_KAMOKU_CD"]);
                                dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, dr["KANJO_KAMOKU_NM"]);
                                dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, dr["TAISHAKU_KUBUN_NM"]);
                            }
                            else
                            {
                                dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, "");
                                dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, "");
                                dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, "");
                            }
                            dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, drSub["HOJO_KAMOKU_CD"]);
                            dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, drSub["HOJO_KAMOKU_NM"]);
                        dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, drSub["KISHU_ZANDAKA"]);

                        this.Dba.ModifyBySql(Util.ToString(Sql_ins), dpc);

                            cnt++;
                            #endregion
                        }
                    //}
                    #endregion
                }
                #endregion
            }

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_ZM_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_ZM_TBL",
                "GUID = @GUID",
                dpc);

            bool dataFlag;
            if (tmpdtPR_ZM_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                Msg.Info("該当データがありません。");
                dataFlag = false;
            }

            return dataFlag;
        }

        #endregion
    }
}
