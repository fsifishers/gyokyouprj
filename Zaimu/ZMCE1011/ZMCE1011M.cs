﻿using System.Text;

namespace jp.co.fsi.zm.zmce1011
{
    public class ZMCE1011M
    {
        /// <summary>
        /// 勘定科目設定および期首残高退避用構造体
        /// </summary>
        public struct KISHUZAN_HEAD
        {
            public int KANJO_KAMOKU_CD;         // 勘定科目コード
            public string KANJO_KAMOKU_NM;      // 勘定科目名称
            public int TAISHAKU_KUBUN;          // 貸借区分
            public string TAISHAKU_KUBUN_NM;    // 貸借区分名
            public decimal KISHU_ZANDAKA;       // 期首残高
            public int HOJO_KAMOKU_UMU;         // 補助科目有無
            public int HOJO_SHIYO_KUBUN;        // 補助科目使用区分（財務か取引先か）
            public int BUMON_UMU;               // 部門有無

            public void Clear()
            {
                KANJO_KAMOKU_CD = 0;
                KANJO_KAMOKU_NM = "";
                TAISHAKU_KUBUN = 0;
                TAISHAKU_KUBUN_NM = "";
                KISHU_ZANDAKA = 0;
                HOJO_KAMOKU_UMU = 0;
                HOJO_SHIYO_KUBUN = 0;
                BUMON_UMU = 0;
            }
        }

        /// <summary>
        /// 勘定科目毎期首残高および補助・部門有無設定の取得
        /// </summary>
        /// <param name="Kmk">勘定科目範囲の条件設定区分（デフォルトはゼロ、検索条件に含めない）</param>
        /// <returns>生成したＳＱＬを返す</returns>
        public string GetKomokuStiSql(int Kmk = 0)
        {
            StringBuilder sql = new StringBuilder();
            sql.AppendLine(" SELECT ");
            sql.AppendLine(" 	A.KAISHA_CD,");
            sql.AppendLine(" 	A.KAMOKU_BUNRUI_CD,");
            sql.AppendLine(" 	A.KANJO_KAMOKU_CD,");
            sql.AppendLine(" 	A.KANJO_KAMOKU_NM,");
            sql.AppendLine(" 	A.TAISHAKU_KUBUN,");
            sql.AppendLine(" 	B.TAISHAKU_KUBUN_NM,");
            sql.AppendLine(" 	A.HOJO_KAMOKU_UMU,");
            sql.AppendLine(" 	A.HOJO_SHIYO_KUBUN,");
            sql.AppendLine(" 	A.BUMON_UMU,");
            sql.AppendLine(EditSqlKishuZan("A.KANJO_KAMOKU_CD"));
            sql.AppendLine(" 	 AS KISHU_ZANDAKA");
            sql.AppendLine(" FROM TB_ZM_KANJO_KAMOKU A");
            sql.AppendLine(" LEFT OUTER JOIN TB_ZM_F_TAISHAKU_KUBUN AS B ");
            sql.AppendLine(" ON	A.TAISHAKU_KUBUN = B.TAISHAKU_KUBUN");
            sql.AppendLine(" WHERE");
            sql.AppendLine(" 	A.KAISHA_CD = @KAISHA_CD AND ");
            sql.AppendLine(" 	A.KAIKEI_NENDO = @KAIKEI_NENDO AND ");
            if (Kmk == 1)
            {
                sql.AppendLine(" 	A.KANJO_KAMOKU_CD BETWEEN @KANJO_KAMOKU_CD_FR AND @KANJO_KAMOKU_CD_TO AND ");
            }
            sql.AppendLine(" 	((A.KAMOKU_BUNRUI_CD >= 10000 AND A.KAMOKU_BUNRUI_CD <= 19999) OR A.KAMOKU_BUNRUI_CD = 22400)");
            sql.AppendLine(" ORDER BY");
            sql.AppendLine(" 	A.KAISHA_CD,");
            sql.AppendLine(" 	A.KAMOKU_BUNRUI_CD,");
            sql.AppendLine(" 	A.KANJO_KAMOKU_CD");
            return sql.ToString();
        }

        /// <summary>
        /// 部門毎補助科目毎の期首残高取得
        /// </summary>
        /// <param name="KanjoKamokuCd">勘定科目コード</param>
        /// <param name="HojoKubun">補助使用区分 1:補助科目マスタ（財務） 2:取引先</param>
        /// <returns>結果セットのデータテーブル(DataTable)を返す</returns>
        public string GetHjkBmnZan(int KanjoKamokuCd, int HojoKubun)
        {
            StringBuilder Sql = new StringBuilder();
            string ColCd;
            string ColNm;
            string tableNm;

            if (HojoKubun == Const.HOJO_SHIYO_KUBUN_ZAM)
            {
                ColCd = " A.HOJO_KAMOKU_CD";
                ColNm = " A.HOJO_KAMOKU_NM";
                tableNm = "TB_ZM_HOJO_KAMOKU";
            }
            else
            {
                ColCd = " A.TORIHIKISAKI_CD";
                ColNm = " A.TORIHIKISAKI_NM";
                tableNm = "TB_CM_TORIHIKISAKI";
            }
            Sql.AppendLine(" SELECT ");
            Sql.AppendLine(ColCd + " AS HOJO_KAMOKU_CD,");
            Sql.AppendLine(ColNm + " AS HOJO_KAMOKU_NM,");
            Sql.AppendLine(" B.BUMON_CD,");
            Sql.AppendLine(" B.BUMON_NM,");
            Sql.AppendLine(EditSqlKishuZan(KanjoKamokuCd.ToString(), ColCd, "B.BUMON_CD"));
            Sql.AppendLine(" AS KISHU_ZANDAKA ");
            Sql.AppendLine(" FROM " + tableNm + " AS A ");
            Sql.AppendLine(" LEFT OUTER JOIN TB_CM_BUMON AS B");
            Sql.AppendLine(" ON A.KAISHA_CD = B.KAISHA_CD");
            Sql.AppendLine(" WHERE ");
            Sql.AppendLine(" A.KAISHA_CD = @KAISHA_CD ");
            if (HojoKubun == Const.HOJO_SHIYO_KUBUN_ZAM)
            {
                Sql.AppendLine(" AND ");
                Sql.AppendLine(" A.SHISHO_CD = @SHISHO_CD AND ");
                Sql.AppendLine(" A.KAIKEI_NENDO = @KAIKEI_NENDO AND ");
                Sql.AppendLine(" A.KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD ");
            }
            Sql.AppendLine(" ORDER BY ");
            Sql.AppendLine(" A.KAISHA_CD,");
            Sql.AppendLine(" " + ColCd + ",");
            Sql.AppendLine(" B.BUMON_CD");
            return Sql.ToString();
        }

        /// <summary>
        /// 補助科目毎の期首残高取得
        /// </summary>
        /// <param name="KanjoKamokuCd">勘定科目コード</param>
        /// <param name="HojoKubun">補助使用区分 1:補助科目マスタ（財務） 2:取引先</param>
        /// <returns>結果セットのデータテーブル(DataTable)を返す</returns>
        public string GetHjkZan(int KanjoKamokuCd, int HojoKubun)
        {
            StringBuilder Sql = new StringBuilder();
            string ColCd;
            string ColNm;
            string tableNm;
            if (HojoKubun == Const.HOJO_SHIYO_KUBUN_ZAM)
            {
                ColCd = " A.HOJO_KAMOKU_CD";
                ColNm = " A.HOJO_KAMOKU_NM";
                tableNm = "TB_ZM_HOJO_KAMOKU";
            }
            else
            {
                ColCd = " A.TORIHIKISAKI_CD";
                ColNm = " A.TORIHIKISAKI_NM";
                tableNm = "TB_CM_TORIHIKISAKI";
            }
            Sql.AppendLine(" SELECT ");
            Sql.AppendLine(ColCd + " AS HOJO_KAMOKU_CD,");
            Sql.AppendLine(ColNm + " AS HOJO_KAMOKU_NM,");
            Sql.AppendLine(" 0       AS BUMON_CD,");
            Sql.AppendLine(" ''      AS BUMON_NM,");
            Sql.AppendLine(EditSqlKishuZan(KanjoKamokuCd.ToString(), ColCd));
            Sql.AppendLine(" AS KISHU_ZANDAKA ");
            Sql.AppendLine(" FROM " + tableNm + " AS A ");
            Sql.AppendLine(" WHERE ");
            Sql.AppendLine(" A.KAISHA_CD = @KAISHA_CD ");
            if (HojoKubun == Const.HOJO_SHIYO_KUBUN_ZAM)
            {
                Sql.AppendLine(" AND ");
                Sql.AppendLine(" A.SHISHO_CD = @SHISHO_CD AND ");
                Sql.AppendLine(" A.KAIKEI_NENDO = @KAIKEI_NENDO AND ");
                Sql.AppendLine(" A.KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD ");
            }
            Sql.AppendLine(" ORDER BY ");
            Sql.AppendLine(" A.KAISHA_CD,");
            Sql.AppendLine(" " + ColCd);
            return Sql.ToString();
        }

        /// <summary>
        /// 部門毎の期首残高取得
        /// </summary>
        /// <param name="KanjoKamokuCd">勘定科目コード</param>
        /// <returns>結果セットのデータテーブル(DataTable)を返す</returns>
        public string GetBmnZan(int KanjoKamokuCd)
        {
            StringBuilder Sql = new StringBuilder();
            Sql.AppendLine(" SELECT ");
            Sql.AppendLine("0 AS HOJO_KAMOKU_CD,");
            Sql.AppendLine("'' AS HOJO_KAMOKU_NM,");
            Sql.AppendLine("A.BUMON_CD,");
            Sql.AppendLine("A.BUMON_NM,");
            Sql.AppendLine(EditSqlKishuZan(KanjoKamokuCd.ToString(), "", "A.BUMON_CD"));
            Sql.AppendLine(" AS KISHU_ZANDAKA ");
            Sql.AppendLine(" FROM TB_CM_BUMON AS A ");
            Sql.AppendLine(" LEFT OUTER JOIN TB_CM_BUMON AS B");
            Sql.AppendLine(" ON A.KAISHA_CD = B.KAISHA_CD");
            Sql.AppendLine(" WHERE ");
            Sql.AppendLine(" A.KAISHA_CD = @KAISHA_CD ");
            Sql.AppendLine(" ORDER BY ");
            Sql.AppendLine(" A.KAISHA_CD,");
            Sql.AppendLine(" A.BUMON_CD");
            return Sql.ToString();
        }

        /// <summary>
        /// 期首残高取得SQLの生成
        /// 引数はコードや接続文字列を指定
        /// </summary>
        /// <param name="KanjoKamokuCd">勘定科目コード（文字列）</param>
        /// <param name="Hojo_Kamoku_Cd">補助科目コード（文字列）</param>
        /// <param name="BumonCd">部門コード（文字列）</param>
        /// <returns>生成した文字列を返す</returns>
        public string EditSqlKishuZan(string KanjoKamokuCd, string Hojo_Kamoku_Cd = "", string BumonCd = "")
        {
            StringBuilder retSql = new StringBuilder();
            retSql.AppendLine(" 	(");
            retSql.AppendLine(" 		SELECT ");
            retSql.AppendLine(" 		SUM(CASE WHEN SA.TAISHAKU_KUBUN = SB.TAISHAKU_KUBUN THEN SA.ZEIKOMI_KINGAKU ELSE (SA.ZEIKOMI_KINGAKU * -1) END)");
            retSql.AppendLine(" 		FROM TB_ZM_SHIWAKE_MEISAI AS SA");
            retSql.AppendLine(" 		INNER JOIN TB_ZM_KANJO_KAMOKU SB ");
            retSql.AppendLine(" 		ON	SA.KAISHA_CD = SB.KAISHA_CD");
            retSql.AppendLine(" 		AND SA.KANJO_KAMOKU_CD = SB.KANJO_KAMOKU_CD");
            retSql.AppendLine(" 		AND SA.KAIKEI_NENDO = SB.KAIKEI_NENDO");
            retSql.AppendLine(" 		AND SA.SHISHO_CD = @SHISHO_CD");
            retSql.AppendLine(" 		WHERE");
            retSql.AppendLine(" 		SA.KAISHA_CD = @KAISHA_CD AND ");
            retSql.AppendLine(" 		SA.KAIKEI_NENDO = @KAIKEI_NENDO AND ");
            if (KanjoKamokuCd != "")
            {
                retSql.AppendLine(" 		SA.KANJO_KAMOKU_CD = " + KanjoKamokuCd + " AND ");
            }
            if (Hojo_Kamoku_Cd != "")
            {
                retSql.AppendLine(" 		SA.HOJO_KAMOKU_CD = " + Hojo_Kamoku_Cd + " AND ");
            }
            if (BumonCd != "")
            {
                retSql.AppendLine(" 		SA.BUMON_CD = " + BumonCd + " AND ");
            }
            retSql.AppendLine(" 		SA.DENPYO_BANGO = 0 AND ");
            retSql.AppendLine(" 		SA.DENPYO_KUBUN = 0");
            retSql.AppendLine(" 	)");
            return retSql.ToString();
        }

    }
}
