﻿using System;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.zm.zmce1011
{
    /// <summary>
    /// 補助科目残高(ZAMM4012)
    /// </summary>
    public partial class ZMCE1014 : BasePgForm
    {
        #region 構造体
        // 残高合計用
        struct SumZan
        {
            public decimal BmnKei;
            public decimal HjoKei;
            public decimal Gokei;
            public void Clear()
            {
                BmnKei=0;
                HjoKei = 0;
                Gokei = 0;
            }
        }
        #endregion

        #region プロパティ
        /// <summary>
        /// 金額一覧
        /// </summary>
        private DataTable _dispList;

        /// <summary>
        /// 部門毎残高テーブル
        /// </summary>
        private DataTable _bumonList;
        #endregion

        #region private変数
        /// <summary>
        /// ZMCE1011(期首残高登録画面)のオブジェクト(設定内容の取得のため)
        /// </summary>
        ZMCE1011 _pForm;
        int _kanjo;
        string _hojoKbn;


        string SHISHO_CD;
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ZMCE1014(ZMCE1011 frm, string par1, int kanjoCd)
        {
            this._pForm = frm;
            this._kanjo = kanjoCd;
            this._hojoKbn = par1;

            InitializeComponent();
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // EscapeとF6のみ表示
            this.ShowFButton = true;
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF6.Location = this.btnF2.Location;
            this.btnF2.Visible = false;
            this.btnF3.Visible = false;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;
            this.lblTitle.Visible = false;

            SHISHO_CD = this.Par2;
            // 手当項目グリッド初期化
            InitGridKishuZan();

            // 初期フォーカス
            this.dgvKishuZan.Focus();
            if (this.dgvKishuZan.Rows.Count == 0)
            {
                Msg.Error("補助科目が登録されていません。");
                // DialogResultに「OK」をセットし結果を返却
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                // 先頭セルの選択
                //this.dgvKishuZan.CurrentCell = this.dgvKishuZan[2, 0];
                //this.dgvKishuZan.BeginEdit(true);
                //this.dgvKishuZan.SelectAll();
            }
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        public override void PressF6()
        {
            // 選択会計年度が今年度かチェック
            DateTime today = DateTime.Today;
            if (Util.GetKaikeiNendo(today, this.Dba) != this.UInfo.KaikeiNendo)
            {
                if (Msg.ConfNmYesNo("補助科目残高（保存）", "選択会計年度が今年度ではありませんが、宜しいですか？") == DialogResult.No)
                {
                    return;
                }
            }

            // 確認メッセージを表示
            string msg = ("保存しますか？");
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 全項目を再度入力値チェック
            if (!ValidateDataGridViewAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            DataRow[] arrAppend;
            // 入力内容を保存
            //foreach (DataRow row in _dispList.Rows)
            foreach (DataRow row in _bumonList.Rows)
            {
                DataTable BmnTbl = GetBumon();
                foreach (DataRow bDr in BmnTbl.Rows)
                {
                    // 該当列の名称を作成
                    string colNm = "BUMON_" + Util.ToInt(bDr["BUMON_CD"]).ToString("0000");
                    arrAppend = this._pForm.KinList.Select("KANJO_KAMOKU_CD = " + this._kanjo
                    + " AND HOJO_KAMOKU_CD  =" + Util.ToDecimal(row["HOJO_KAMOKU_CD"])
                    + " AND BUMON_CD  =" + Util.ToInt(bDr["BUMON_CD"]) );
                    foreach (DataRow rows in arrAppend)
                    {
                        rows["HOJO_KISHU_ZANDAKA"] = Util.ToDecimal(row[colNm]);
                    }
                }
            }
            arrAppend = this._pForm.KinList.Select("KANJO_KAMOKU_CD = " + this._kanjo);
            foreach (DataRow rows in arrAppend)
            {
                rows["KISHU_ZANDAKA"] = Util.ToDecimal(dgvKmkKei[0, 0].Value);
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion

        #region イベント
        /// <summary>
        /// グリッド共通EditingControlShowingイベントハンドラ
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            //表示されているコントロールがDataGridViewTextBoxEditingControlか調べる
            if (e.Control is DataGridViewTextBoxEditingControl)
            {
                DataGridView dgv = (DataGridView)sender;

                //編集のために表示されているコントロールを取得
                DataGridViewTextBoxEditingControl tb = (DataGridViewTextBoxEditingControl)e.Control;

                //該当する列か調べる
                string ColNm = dgv.CurrentCell.OwningColumn.Name;
                if (ColNm.Substring(0, 5) == "BUMON")
                {
                    tb.MaxLength = 15;
                }
            }
        }

        /// <summary>
        /// グリッド共通Enterイベントハンドラ
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_Enter(object sender, EventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            dgv.Focus();
            if (dgv.CurrentRow != null)
            {
                if (!dgv.IsCurrentCellDirty)
                    dgv.CurrentCell = dgv[2, dgv.CurrentRow.Index];
            }
        }

        /// <summary>
        /// グリッドCellEnter時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;

            int RowIndex = e.RowIndex;
            int ColIndex = e.ColumnIndex;

            this.dgvHjkBmnKei.CurrentCell = this.dgvHjkBmnKei[2, RowIndex];

            if (dgv.Columns[e.ColumnIndex].ReadOnly == false)
            {
                dgv.BeginEdit(true);
                //SendKeys.Send("{Tab}");
                //dgv.CurrentCell = dgv[2, RowIndex];
            }

            string ColNm = dgv.Columns[e.ColumnIndex].Name;
            if (ColNm.Substring(0, 5) == "BUMON")
            {
                //dgv.ImeMode = System.Windows.Forms.ImeMode.Alpha;
                dgv.ImeMode = System.Windows.Forms.ImeMode.Off;
                // 合計値を表示
                dgvSum();
            }
        }

        /// <summary>
        /// グリッドセル値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;

            if (dgv.IsCurrentCellDirty)
            {
                string ColNm = dgv.Columns[e.ColumnIndex].Name;
                if (ColNm.Substring(0, 5) == "BUMON")
                {
                    e.Cancel = !IsValidHojoKishuZan(e.FormattedValue);
                }
            }
        }

        /// <summary>
        /// グリッドのセル描画
        /// 部門合計グリッドの先頭２つのセルの間の罫線を消す
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvBmnKei_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.RowIndex == 0)
            {
                if (e.ColumnIndex == -1)
                {
                    e.AdvancedBorderStyle.Left = DataGridViewAdvancedCellBorderStyle.None;
                }
                else if (e.ColumnIndex == 0)
                {
                    e.AdvancedBorderStyle.Right = DataGridViewAdvancedCellBorderStyle.None;
                }
            }
        }

        /// <summary>
        /// 入力グリッドと合計グリッドのスクロール同期
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvKishuZan_Scroll(object sender, ScrollEventArgs e)
        {
            dgvBmnKei.HorizontalScrollingOffset = dgvKishuZan.HorizontalScrollingOffset;
        }

        #endregion

        #region privateメソッド
        /// <summary>
        /// 項目グリッド初期化
        /// </summary>
        private void InitGridKishuZan()
        {
            // グリッド一括設定
            SetGridProperty();

            #region 補助科目合計グリッド
            //　合計グリッドの設定
            this._dispList = new DataTable();
            _dispList.Columns.Add("HOJO_KAMOKU_CD", Type.GetType("System.Int32"));
            _dispList.Columns.Add("HOJO_KAMOKU_NM", Type.GetType("System.String"));
            _dispList.Columns.Add("HOJO_KISHU_ZANDAKA", Type.GetType("System.Int64"));
            _dispList.Columns.Add("KANJO_KAMOKU_CD", Type.GetType("System.Int32"));
            // 変数宣言
            decimal hojoKmkCd = 0;
            //データ絞込み
            DataRow[] aarDisp =
             this._pForm.KinList.Select("KANJO_KAMOKU_CD = " + this._kanjo);
            //行を追加
            foreach (DataRow row in aarDisp)
            {
                if (hojoKmkCd == (Util.ToDecimal(row["HOJO_KAMOKU_CD"])))
                {
                    continue;
                }
                DataRow bdrkei = this._dispList.NewRow();
                bdrkei["HOJO_KAMOKU_CD"] = Util.ToDecimal(row["HOJO_KAMOKU_CD"]);
                bdrkei["HOJO_KAMOKU_NM"] = Util.ToString(row["HOJO_KAMOKU_NM"]);
                bdrkei["HOJO_KISHU_ZANDAKA"] = Util.ToDecimal(row["KISHU_ZANDAKA"]);
                bdrkei["KANJO_KAMOKU_CD"] = Util.ToDecimal(row["KANJO_KAMOKU_CD"]);
                _dispList.Rows.Add(bdrkei);
                hojoKmkCd = Util.ToDecimal(row["HOJO_KAMOKU_CD"]);
            }

            // 補助科目が未登録時
            if (_dispList.Rows.Count == 0)
                return;

            // グリッドへデータソース設定
            this.dgvHjkBmnKei.DataSource = _dispList;
            // 勘定科目コード・勘定科目名を表示
            this.txtKanjoKmk.Text = Util.ToString(this._kanjo);
            this.lblKanjoKmk.Text = this.Dba.GetName(this.UInfo, "TB_ZM_KANJO_KAMOKU", SHISHO_CD, this.txtKanjoKmk.Text);
            this.txtKanjoKmk.Enabled = false;

            //// 合計値を表示
            //this.lblHojoGokei.Text = Util.ToString(this._dispList.Compute("Sum(HOJO_KISHU_ZANDAKA)", ""));
            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvHjkBmnKei.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            DataGridViewColumn col = this.dgvHjkBmnKei.Columns[2];
            col.HeaderText = "合　　計";
            col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            col.Width = 180;
            col.ReadOnly = true;
            col.Frozen = true;
            col.Resizable = DataGridViewTriState.False;
            col.DefaultCellStyle.Format = "#,0";

            this.dgvHjkBmnKei.Columns[0].Visible = false;
            this.dgvHjkBmnKei.Columns[1].Visible = false;
            this.dgvHjkBmnKei.Columns[3].Visible = false;
            #endregion

            #region 部門毎グリッド
            // 明細グリッドデータ定義
            this._bumonList = new DataTable();
            DataTable BumonKei = new DataTable();
            DataRow BumonKeiDr = BumonKei.NewRow();

            _bumonList.Columns.Add("HOJO_KAMOKU_CD", Type.GetType("System.Int32"));
            BumonKei.Columns.Add("HOJO_KAMOKU_CD", Type.GetType("System.String"));
            BumonKeiDr["HOJO_KAMOKU_CD"] = "";
            _bumonList.Columns.Add("HOJO_KAMOKU_NM", Type.GetType("System.String"));
            BumonKei.Columns.Add("HOJO_KAMOKU_NM", Type.GetType("System.String"));
            BumonKeiDr["HOJO_KAMOKU_NM"] = "　　合　　　　　計";
            DataTable BmnTbl = GetBumon();
            decimal BmnCount = BmnTbl.Rows.Count;
            decimal[] BmnKei = new decimal[0];
            if(BmnCount != 0)
            {
                foreach(DataRow bDr in BmnTbl.Rows)
                {
                    string ColName = "BUMON_" + Util.ToInt(bDr["BUMON_CD"]).ToString("0000");
                    _bumonList.Columns.Add(ColName, Type.GetType("System.Int64"));
                    BumonKei.Columns.Add(ColName, Type.GetType("System.Int64"));
                    BumonKeiDr[ColName] = 0;
                }
            }
            _bumonList.Columns.Add("HOJO_KISHU_ZANDAKA", Type.GetType("System.Int64"));
            BumonKei.Columns.Add("HOJO_KISHU_ZANDAKA", Type.GetType("System.Int64"));
            BumonKeiDr["HOJO_KISHU_ZANDAKA"] = 0;
            _bumonList.Columns.Add("KANJO_KAMOKU_CD", Type.GetType("System.Int32"));
            BumonKei.Columns.Add("KANJO_KAMOKU_CD", Type.GetType("System.Int32"));
            BumonKeiDr["KANJO_KAMOKU_CD"] = 0;
            BumonKei.Rows.Add(BumonKeiDr);

            // 変数宣言
            hojoKmkCd = 0;
            // 合計期首残高
            decimal kishjZandaka = 0;
            //行を追加
            DataRow Lsdr = this._bumonList.NewRow();
            foreach (DataRow row in aarDisp)
            {
                if (hojoKmkCd != Util.ToDecimal(row["HOJO_KAMOKU_CD"]))
                {
                    // 最初でなければ作成した行を追加する
                    if (hojoKmkCd != 0)
                    {
                        _bumonList.Rows.Add(Lsdr);
                        // 補助科目コードが変わったら行を追加する
                        Lsdr = this._bumonList.NewRow();
                        // 期首残高をクリア
                        kishjZandaka = 0;
                    }
                    Lsdr["HOJO_KAMOKU_CD"] = Util.ToDecimal(row["HOJO_KAMOKU_CD"]);
                    Lsdr["HOJO_KAMOKU_NM"] = Util.ToString(row["HOJO_KAMOKU_NM"]);
                    Lsdr["HOJO_KISHU_ZANDAKA"] = kishjZandaka;
                    Lsdr["KANJO_KAMOKU_CD"] = Util.ToDecimal(row["KANJO_KAMOKU_CD"]);
                }

                // 部門コード毎に残高を設定していく
                Lsdr["BUMON_" + Util.ToInt(row["BUMON_CD"]).ToString("0000")] = Util.ToDecimal(row["HOJO_KISHU_ZANDAKA"]);
                kishjZandaka += Util.ToDecimal(row["HOJO_KISHU_ZANDAKA"]);

                hojoKmkCd = Util.ToDecimal(row["HOJO_KAMOKU_CD"]);
            }
            _bumonList.Rows.Add(Lsdr);

            // グリッドへデータソース設定
            this.dgvKishuZan.DataSource = _bumonList;
            // 各列設定
            col = this.dgvKishuZan.Columns[0];
            col.HeaderText = "コード";
            col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            col.Width = 60;
            col.Resizable = DataGridViewTriState.False;
            col.ReadOnly = true;
            col = this.dgvKishuZan.Columns[1];
            col.HeaderText = "補助科目名";
            col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            col.Width = 200;
            col.ReadOnly = true;
            col.Resizable = DataGridViewTriState.False;
            col.Frozen = true;
            // 部門列の設定
            int ColIdx = 2;
            foreach (DataRow bDr in BmnTbl.Rows)
            {
                col = this.dgvKishuZan.Columns[ColIdx];
                col.HeaderText = Util.ToString(bDr["BUMON_NM"]);
                col.Width = 150;
                col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                col.Resizable = DataGridViewTriState.False;
                col.DefaultCellStyle.Format = "#,0";
                ColIdx++;
            }
            this.dgvKishuZan.Columns[ColIdx].Visible = false;
            this.dgvKishuZan.Columns[ColIdx+1].Visible = false;
            #endregion

            #region 部門合計グリッド
            this.dgvBmnKei.DataSource = BumonKei;
            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvKishuZan.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // 各列設定
            col = this.dgvBmnKei.Columns[0];
            col.Width = 60;
            col.Resizable = DataGridViewTriState.False;
            col.ReadOnly = true;
            col = this.dgvBmnKei.Columns[1];
            col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            col.Width = 200;
            col.ReadOnly = true;
            col.Resizable = DataGridViewTriState.False;
            col.Frozen = true;
            // 部門列の設定
            ColIdx = 2;
            foreach (DataRow bDr in BmnTbl.Rows)
            {
                col = this.dgvBmnKei.Columns[ColIdx];
                col.HeaderText = "";
                col.Width = 150;
                col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                col.Resizable = DataGridViewTriState.False;
                col.DefaultCellStyle.Format = "#,0";
                ColIdx++;
            }
            this.dgvKishuZan.Columns[ColIdx].Visible = false;
            this.dgvKishuZan.Columns[ColIdx + 1].Visible = false;

            #endregion

            #region 勘定科目合計グリッド
            // 科目合計の表示
            DataTable KmkKei = new DataTable();
            KmkKei.Columns.Add("KmkKei", Type.GetType("System.Int64"));
            DataRow drKei = KmkKei.NewRow();
            drKei["KmkKei"] = Util.ToString(this._dispList.Compute("Sum(HOJO_KISHU_ZANDAKA)", ""));
            KmkKei.Rows.Add(drKei);
            this.dgvKmkKei.DataSource = KmkKei;
            this.dgvKmkKei.Columns[0].Width = 180;
            this.dgvKmkKei.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvKmkKei.Columns[0].ReadOnly = true;
            this.dgvKmkKei.Columns[0].Visible = true;
            this.dgvKmkKei.Columns[0].Frozen = true;
            #endregion
            // グリッド合計
            dgvSum();

            // 合計グリッドの選択非表示
            //dgvKmkKei.DefaultCellStyle.SelectionForeColor = SystemColors.ControlText;
            //dgvKmkKei.DefaultCellStyle.SelectionBackColor = Color.Transparent;
            dgvHjkBmnKei.DefaultCellStyle.SelectionForeColor = SystemColors.ControlText;
            dgvHjkBmnKei.DefaultCellStyle.SelectionBackColor = Color.Transparent;
            dgvBmnKei.DefaultCellStyle.SelectionForeColor = SystemColors.ControlText;
            dgvBmnKei.DefaultCellStyle.SelectionBackColor = Color.Transparent;
        }

        /// <summary>
        /// グリッド項目名フィールドの入力チェック
        /// </summary>
        /// <param name="value"></param>
        /// <returns>true:OK,false:NG</returns>
        private bool IsValidHojoKishuZan(object value)
        {
            // 入力サイズが15バイト以上はNG
            if (!ValChk.IsWithinLength(Util.ToString(value), 15))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 数値以外はNG
            // 空はOKとする
            else if (!ValChk.IsEmpty(value) && !Regex.IsMatch(value.ToString(), "^-?[0-9]+$"))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// グリッド全編集行を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateDataGridViewAll()
        {
            foreach (DataGridViewRow row in dgvKishuZan.Rows)
            {
                // 期首残高
                if (!IsValidHojoKishuZan(row.Cells["HOJO_KISHU_ZANDAKA"].Value))
                {
                    dgvKishuZan.Focus();
                    row.Selected = true;
                    row.Cells["HOJO_KISHU_ZANDAKA"].Selected = true;
                    return false;
                }
            }
            return true;
        }
        #endregion

        /// <summary>
        /// 部門マスタデータ取得
        /// </summary>
        /// <returns></returns>
        private DataTable GetBumon()
        {
            DataTable BmnTable;

            StringBuilder sql = new StringBuilder();
            DbParamCollection dpc = new DbParamCollection();

            sql.AppendLine("SELECT * FROM TB_CM_BUMON");
            sql.AppendLine("WHERE ");
            sql.AppendLine("KAISHA_CD = @KAISHA_CD");
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);

            BmnTable = this.Dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return BmnTable;
        }

        /// <summary>
        /// 各グリッドの設定
        /// </summary>
        private void SetGridProperty()
        {
            // 　補助科目毎グリッド　
            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvHjkBmnKei.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;
            // フォントを設定する
            this.dgvHjkBmnKei.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F);
            this.dgvHjkBmnKei.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.dgvHjkBmnKei.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 12F);
            this.dgvHjkBmnKei.AlternatingRowsDefaultCellStyle.BackColor = Color.Aquamarine;
            // 行の高さは変更させない
            this.dgvHjkBmnKei.AllowUserToResizeRows = false;
            this.dgvHjkBmnKei.ReadOnly = true;


            // 補助科目別部門別グリッドの設定
            //// ユーザーによるソートを禁止させる
            //foreach (DataGridViewColumn c in this.dgvKishuZan.Columns)
            //    c.SortMode = DataGridViewColumnSortMode.NotSortable;
            // スクロールバー表示
            this.dgvKishuZan.ScrollBars = ScrollBars.Both;
            // フォントを設定する
            this.dgvKishuZan.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F);
            this.dgvKishuZan.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.dgvKishuZan.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 12F);
            this.dgvKishuZan.AlternatingRowsDefaultCellStyle.BackColor = Color.Aquamarine;
            // 編集設定
            this.dgvKishuZan.SelectionMode = DataGridViewSelectionMode.CellSelect;
            this.dgvKishuZan.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.dgvKishuZan.ReadOnly = false;
            this.dgvKishuZan.EditMode = DataGridViewEditMode.EditOnEnter;
            // 行の高さは変更させない
            this.dgvKishuZan.AllowUserToResizeRows = false;

            // 部門合計グリッドの設定
            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvBmnKei.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;
            this.dgvBmnKei.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 12F);
            // 行の高さは変更させない
            this.dgvBmnKei.AllowUserToResizeRows = false;
            this.dgvBmnKei.ReadOnly = true;

            // 科目総合計グリッド
            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvKmkKei.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;
            // 行の高さは変更させない
            this.dgvKmkKei.AllowUserToResizeRows = false;
            // フォントを設定する
            this.dgvKmkKei.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F);
            this.dgvKmkKei.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.dgvKmkKei.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 12F);
            this.dgvKmkKei.AlternatingRowsDefaultCellStyle.BackColor = Color.Aquamarine;
            this.dgvKmkKei.AllowUserToResizeRows = false;
            this.dgvKmkKei.ReadOnly = true;
        }

        /// <summary>
        /// 入力値の合計処理
        /// </summary>
        private void dgvSum()
        {
            decimal kmkKei = 0;
            decimal bmnKei = 0;
            decimal Sogokei = 0;

            this.dgvKishuZan.EndEdit();

            for (int Row = 0; Row < dgvKishuZan.RowCount; Row++)
            {
                for (int Col = 2; Col < dgvKishuZan.ColumnCount-1; Col++ )
                {
                    kmkKei += Util.ToDecimal( dgvKishuZan[Col, Row].Value);
                }
                dgvHjkBmnKei[2, Row].Value = kmkKei;
                Sogokei += kmkKei;
                kmkKei = 0;
            }

            for (int Col = 2; Col < dgvKishuZan.ColumnCount - 1; Col++)
            {
                for (int Row = 0; Row < dgvKishuZan.RowCount; Row++)
                {
                    bmnKei += Util.ToDecimal(dgvKishuZan[Col, Row].Value);
                }
                dgvBmnKei[Col, 0].Value = bmnKei;
                bmnKei = 0;
            }
            dgvKmkKei[0, 0].Value = Sogokei;
        }
    }
}
