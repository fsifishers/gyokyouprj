﻿namespace jp.co.fsi.zm.zmce1011
{
    /// <summary>
    /// ZMCE1011R の概要の説明です。
    /// </summary>
    partial class ZMCE1011R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(ZMCE1011R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.lblTitle01 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.直線2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblTitle02 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTitle03 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTitle04 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtKaisha = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.rptDate = new GrapeCity.ActiveReports.SectionReportModel.ReportInfo();
            this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.textBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.textBox9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.txtShisho = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKaisha)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rptDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShisho)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblTitle01,
            this.lblTitle,
            this.直線2,
            this.lblTitle02,
            this.lblTitle03,
            this.lblTitle04,
            this.txtKaisha,
            this.rptDate,
            this.txtPage,
            this.lblPage,
            this.textBox2,
            this.textBox3,
            this.textBox4,
            this.txtShisho});
            this.pageHeader.Height = 1.181103F;
            this.pageHeader.Name = "pageHeader";
            // 
            // lblTitle01
            // 
            this.lblTitle01.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.lblTitle01.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.lblTitle01.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.lblTitle01.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.lblTitle01.CharacterSpacing = 5F;
            this.lblTitle01.Height = 0.3937008F;
            this.lblTitle01.HyperLink = null;
            this.lblTitle01.Left = 0F;
            this.lblTitle01.Name = "lblTitle01";
            this.lblTitle01.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; " +
    "font-weight: bold; text-align: center; vertical-align: middle; ddo-char-set: 128" +
    "";
            this.lblTitle01.Tag = "";
            this.lblTitle01.Text = "勘定科目";
            this.lblTitle01.Top = 0.7874016F;
            this.lblTitle01.Width = 2.373622F;
            // 
            // lblTitle
            // 
            this.lblTitle.Height = 0.2258039F;
            this.lblTitle.HyperLink = null;
            this.lblTitle.Left = 2.662599F;
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Style = "color: Black; font-family: ＭＳ 明朝; font-size: 14.25pt; font-weight: bold; text-ali" +
    "gn: center; vertical-align: middle; ddo-char-set: 128";
            this.lblTitle.Tag = "";
            this.lblTitle.Text = "科目残高リスト";
            this.lblTitle.Top = 0F;
            this.lblTitle.Width = 2.448032F;
            // 
            // 直線2
            // 
            this.直線2.Height = 0F;
            this.直線2.Left = 2.662599F;
            this.直線2.LineWeight = 1F;
            this.直線2.Name = "直線2";
            this.直線2.Tag = "";
            this.直線2.Top = 0.2259843F;
            this.直線2.Width = 2.448032F;
            this.直線2.X1 = 2.662599F;
            this.直線2.X2 = 5.110631F;
            this.直線2.Y1 = 0.2259843F;
            this.直線2.Y2 = 0.2259843F;
            // 
            // lblTitle02
            // 
            this.lblTitle02.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.lblTitle02.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.lblTitle02.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.lblTitle02.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.lblTitle02.CharacterSpacing = 5F;
            this.lblTitle02.Height = 0.3937008F;
            this.lblTitle02.HyperLink = null;
            this.lblTitle02.Left = 2.373622F;
            this.lblTitle02.Name = "lblTitle02";
            this.lblTitle02.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; " +
    "font-weight: bold; text-align: center; vertical-align: middle; ddo-char-set: 128" +
    "";
            this.lblTitle02.Tag = "";
            this.lblTitle02.Text = "貸借";
            this.lblTitle02.Top = 0.7874017F;
            this.lblTitle02.Width = 0.7173228F;
            // 
            // lblTitle03
            // 
            this.lblTitle03.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.lblTitle03.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.lblTitle03.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.lblTitle03.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.lblTitle03.CharacterSpacing = 5F;
            this.lblTitle03.Height = 0.3937008F;
            this.lblTitle03.HyperLink = null;
            this.lblTitle03.Left = 3.090945F;
            this.lblTitle03.Name = "lblTitle03";
            this.lblTitle03.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; " +
    "font-weight: bold; text-align: center; vertical-align: middle; ddo-char-set: 128" +
    "";
            this.lblTitle03.Tag = "";
            this.lblTitle03.Text = "部門／補助科目";
            this.lblTitle03.Top = 0.7874017F;
            this.lblTitle03.Width = 3.175591F;
            // 
            // lblTitle04
            // 
            this.lblTitle04.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.lblTitle04.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.lblTitle04.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.lblTitle04.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.lblTitle04.CharacterSpacing = 5F;
            this.lblTitle04.Height = 0.3937008F;
            this.lblTitle04.HyperLink = null;
            this.lblTitle04.Left = 6.266536F;
            this.lblTitle04.Name = "lblTitle04";
            this.lblTitle04.Style = "background-color: #CCFFFF; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; " +
    "font-weight: bold; text-align: center; vertical-align: middle; ddo-char-set: 128" +
    "";
            this.lblTitle04.Tag = "";
            this.lblTitle04.Text = "期首残高";
            this.lblTitle04.Top = 0.7874017F;
            this.lblTitle04.Width = 1.217717F;
            // 
            // txtKaisha
            // 
            this.txtKaisha.DataField = "ITEM01";
            this.txtKaisha.Height = 0.2153872F;
            this.txtKaisha.Left = 0F;
            this.txtKaisha.MultiLine = false;
            this.txtKaisha.Name = "txtKaisha";
            this.txtKaisha.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: left; ddo-char-set: 128";
            this.txtKaisha.Tag = "";
            this.txtKaisha.Text = null;
            this.txtKaisha.Top = 0.2259843F;
            this.txtKaisha.Width = 2.271654F;
            // 
            // rptDate
            // 
            this.rptDate.FormatString = "{RunDateTime:yyyy/MM/dd}";
            this.rptDate.Height = 0.1875F;
            this.rptDate.Left = 5.686221F;
            this.rptDate.MultiLine = false;
            this.rptDate.Name = "rptDate";
            this.rptDate.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.rptDate.Top = 0.03858268F;
            this.rptDate.Width = 1.229314F;
            // 
            // txtPage
            // 
            this.txtPage.Height = 0.1874836F;
            this.txtPage.Left = 7.003149F;
            this.txtPage.Name = "txtPage";
            this.txtPage.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtPage.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtPage.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.txtPage.Text = "Page";
            this.txtPage.Top = 0.03876323F;
            this.txtPage.Width = 0.3145666F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.1874836F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 7.317717F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-family: ＭＳ 明朝; ddo-char-set: 1";
            this.lblPage.Text = "頁";
            this.lblPage.Top = 0.03876323F;
            this.lblPage.Width = 0.1666665F;
            // 
            // textBox2
            // 
            this.textBox2.DataField = "ITEM02";
            this.textBox2.Height = 0.2153872F;
            this.textBox2.Left = 2.373622F;
            this.textBox2.MultiLine = false;
            this.textBox2.Name = "textBox2";
            this.textBox2.Padding = new GrapeCity.ActiveReports.PaddingEx(1, 0, 0, 0);
            this.textBox2.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: left; ddo-char-set: 128";
            this.textBox2.Tag = "";
            this.textBox2.Text = null;
            this.textBox2.Top = 0.5401576F;
            this.textBox2.Width = 1.111418F;
            // 
            // textBox3
            // 
            this.textBox3.DataField = "ITEM03";
            this.textBox3.Height = 0.2153872F;
            this.textBox3.Left = 3.568504F;
            this.textBox3.MultiLine = false;
            this.textBox3.Name = "textBox3";
            this.textBox3.Padding = new GrapeCity.ActiveReports.PaddingEx(1, 0, 0, 0);
            this.textBox3.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: left; ddo-char-set: 128";
            this.textBox3.Tag = "";
            this.textBox3.Text = null;
            this.textBox3.Top = 0.5299214F;
            this.textBox3.Width = 1.722047F;
            // 
            // textBox4
            // 
            this.textBox4.DataField = "ITEM04";
            this.textBox4.Height = 0.2153872F;
            this.textBox4.Left = 6.363386F;
            this.textBox4.MultiLine = false;
            this.textBox4.Name = "textBox4";
            this.textBox4.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: right; ddo-char-set: 128";
            this.textBox4.Tag = "";
            this.textBox4.Text = null;
            this.textBox4.Top = 0.5196852F;
            this.textBox4.Width = 1.120866F;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.textBox9,
            this.textBox8,
            this.textBox7,
            this.textBox6,
            this.textBox5,
            this.textBox1,
            this.line1,
            this.line2,
            this.line3,
            this.line4,
            this.line5,
            this.line6,
            this.line7,
            this.line8});
            this.detail.Height = 0.2362205F;
            this.detail.Name = "detail";
            // 
            // textBox9
            // 
            this.textBox9.DataField = "ITEM10";
            this.textBox9.Height = 0.2362205F;
            this.textBox9.Left = 6.266536F;
            this.textBox9.MultiLine = false;
            this.textBox9.Name = "textBox9";
            this.textBox9.OutputFormat = resources.GetString("textBox9.OutputFormat");
            this.textBox9.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
            this.textBox9.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; ddo-char-set: 128";
            this.textBox9.Tag = "";
            this.textBox9.Text = null;
            this.textBox9.Top = 0F;
            this.textBox9.Width = 1.181102F;
            // 
            // textBox8
            // 
            this.textBox8.DataField = "ITEM09";
            this.textBox8.Height = 0.2362205F;
            this.textBox8.Left = 3.808268F;
            this.textBox8.MultiLine = false;
            this.textBox8.Name = "textBox8";
            this.textBox8.Padding = new GrapeCity.ActiveReports.PaddingEx(2, 0, 0, 0);
            this.textBox8.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: left; ddo-char-set: 128";
            this.textBox8.Tag = "";
            this.textBox8.Text = null;
            this.textBox8.Top = 0F;
            this.textBox8.Width = 2.458268F;
            // 
            // textBox7
            // 
            this.textBox7.DataField = "ITEM08";
            this.textBox7.Height = 0.2362205F;
            this.textBox7.Left = 3.090945F;
            this.textBox7.MultiLine = false;
            this.textBox7.Name = "textBox7";
            this.textBox7.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
            this.textBox7.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; ddo-char-set: 128";
            this.textBox7.Tag = "";
            this.textBox7.Text = null;
            this.textBox7.Top = 0F;
            this.textBox7.Width = 0.7173228F;
            // 
            // textBox6
            // 
            this.textBox6.DataField = "ITEM07";
            this.textBox6.Height = 0.2362205F;
            this.textBox6.Left = 2.373622F;
            this.textBox6.MultiLine = false;
            this.textBox6.Name = "textBox6";
            this.textBox6.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: center; ddo-char-set: 128";
            this.textBox6.Tag = "";
            this.textBox6.Text = null;
            this.textBox6.Top = 0F;
            this.textBox6.Width = 0.7173228F;
            // 
            // textBox5
            // 
            this.textBox5.DataField = "ITEM06";
            this.textBox5.Height = 0.2362205F;
            this.textBox5.Left = 0.4956694F;
            this.textBox5.MultiLine = false;
            this.textBox5.Name = "textBox5";
            this.textBox5.Padding = new GrapeCity.ActiveReports.PaddingEx(2, 0, 0, 0);
            this.textBox5.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: left; ddo-char-set: 128";
            this.textBox5.Tag = "";
            this.textBox5.Text = null;
            this.textBox5.Top = 0F;
            this.textBox5.Width = 1.877953F;
            // 
            // textBox1
            // 
            this.textBox1.DataField = "ITEM05";
            this.textBox1.Height = 0.2362205F;
            this.textBox1.Left = 0F;
            this.textBox1.MultiLine = false;
            this.textBox1.Name = "textBox1";
            this.textBox1.Padding = new GrapeCity.ActiveReports.PaddingEx(0, 0, 2, 0);
            this.textBox1.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 11.25pt; fo" +
    "nt-weight: normal; text-align: right; ddo-char-set: 128";
            this.textBox1.Tag = "";
            this.textBox1.Text = null;
            this.textBox1.Top = 0F;
            this.textBox1.Width = 0.4956693F;
            // 
            // line1
            // 
            this.line1.Height = 0F;
            this.line1.Left = 0F;
            this.line1.LineWeight = 1F;
            this.line1.Name = "line1";
            this.line1.Tag = "";
            this.line1.Top = 0.2362205F;
            this.line1.Width = 7.480315F;
            this.line1.X1 = 0F;
            this.line1.X2 = 7.480315F;
            this.line1.Y1 = 0.2362205F;
            this.line1.Y2 = 0.2362205F;
            // 
            // line2
            // 
            this.line2.Height = 0.2362205F;
            this.line2.Left = 2.373622F;
            this.line2.LineWeight = 1F;
            this.line2.Name = "line2";
            this.line2.Tag = "";
            this.line2.Top = 0F;
            this.line2.Width = 0F;
            this.line2.X1 = 2.373622F;
            this.line2.X2 = 2.373622F;
            this.line2.Y1 = 0F;
            this.line2.Y2 = 0.2362205F;
            // 
            // line3
            // 
            this.line3.Height = 0.2362205F;
            this.line3.Left = 3.090945F;
            this.line3.LineWeight = 1F;
            this.line3.Name = "line3";
            this.line3.Tag = "";
            this.line3.Top = 0F;
            this.line3.Width = 0F;
            this.line3.X1 = 3.090945F;
            this.line3.X2 = 3.090945F;
            this.line3.Y1 = 0F;
            this.line3.Y2 = 0.2362205F;
            // 
            // line4
            // 
            this.line4.Height = 0.2362205F;
            this.line4.Left = 6.266536F;
            this.line4.LineWeight = 1F;
            this.line4.Name = "line4";
            this.line4.Tag = "";
            this.line4.Top = 0F;
            this.line4.Width = 0F;
            this.line4.X1 = 6.266536F;
            this.line4.X2 = 6.266536F;
            this.line4.Y1 = 0F;
            this.line4.Y2 = 0.2362205F;
            // 
            // line5
            // 
            this.line5.Height = 0.2362205F;
            this.line5.Left = 7.480316F;
            this.line5.LineWeight = 1F;
            this.line5.Name = "line5";
            this.line5.Tag = "";
            this.line5.Top = 0F;
            this.line5.Width = 0F;
            this.line5.X1 = 7.480316F;
            this.line5.X2 = 7.480316F;
            this.line5.Y1 = 0F;
            this.line5.Y2 = 0.2362205F;
            // 
            // line6
            // 
            this.line6.Height = 0.2362205F;
            this.line6.Left = 0F;
            this.line6.LineWeight = 1F;
            this.line6.Name = "line6";
            this.line6.Tag = "";
            this.line6.Top = 0F;
            this.line6.Width = 0F;
            this.line6.X1 = 0F;
            this.line6.X2 = 0F;
            this.line6.Y1 = 0F;
            this.line6.Y2 = 0.2362205F;
            // 
            // line7
            // 
            this.line7.Height = 0.2362205F;
            this.line7.Left = 0.4956694F;
            this.line7.LineWeight = 1F;
            this.line7.Name = "line7";
            this.line7.Tag = "";
            this.line7.Top = 0F;
            this.line7.Width = 0F;
            this.line7.X1 = 0.4956694F;
            this.line7.X2 = 0.4956694F;
            this.line7.Y1 = 0F;
            this.line7.Y2 = 0.2362205F;
            // 
            // line8
            // 
            this.line8.Height = 0.2362205F;
            this.line8.Left = 3.808268F;
            this.line8.LineWeight = 1F;
            this.line8.Name = "line8";
            this.line8.Tag = "";
            this.line8.Top = 0F;
            this.line8.Width = 0F;
            this.line8.X1 = 3.808268F;
            this.line8.X2 = 3.808268F;
            this.line8.Y1 = 0F;
            this.line8.Y2 = 0.2362205F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            // 
            // textBox10
            // 
            this.txtShisho.DataField = "ITEM01";
            this.txtShisho.Height = 0.2153872F;
            this.txtShisho.Left = 0F;
            this.txtShisho.MultiLine = false;
            this.txtShisho.Name = "textBox10";
            this.txtShisho.Style = "background-color: White; color: Black; font-family: ＭＳ 明朝; font-size: 9.75pt; fon" +
    "t-weight: normal; text-align: left; ddo-char-set: 128";
            this.txtShisho.Tag = "";
            this.txtShisho.Text = null;
            this.txtShisho.Top = 0.5196851F;
            this.txtShisho.Width = 2.271654F;
            // 
            // ZMCE1011R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.3937008F;
            this.PageSettings.Margins.Left = 0.3937008F;
            this.PageSettings.Margins.Right = 0.3937008F;
            this.PageSettings.Margins.Top = 0.3937008F;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.267716F;
            this.PrintWidth = 7.484383F;
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.pageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKaisha)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rptDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShisho)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle01;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
        private GrapeCity.ActiveReports.SectionReportModel.Line 直線2;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle02;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKaisha;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle04;
        private GrapeCity.ActiveReports.SectionReportModel.ReportInfo rptDate;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.Line line3;
        private GrapeCity.ActiveReports.SectionReportModel.Line line4;
        private GrapeCity.ActiveReports.SectionReportModel.Line line5;
        private GrapeCity.ActiveReports.SectionReportModel.Line line6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox5;
        private GrapeCity.ActiveReports.SectionReportModel.Line line7;
        private GrapeCity.ActiveReports.SectionReportModel.Line line8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtShisho;
    }
}
