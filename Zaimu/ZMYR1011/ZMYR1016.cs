﻿using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;
using jp.co.fsi.common.controls;

namespace jp.co.fsi.zm.zmyr1011
{
    /// <summary>
    /// 利益処分注記(ZMYR1016)
    /// </summary>
    public partial class ZMYR1016 : BasePgForm
    {

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ZMYR1016()
        {
            InitializeComponent();
            BindGotFocusEvent();
        }
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // タイトルは非表示
            this.lblTitle.Visible = false;

            // サイズ変更
            this.Size = new Size(680, 540);
            // Escape F6 F8のみ表示
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF6.Location = this.btnF2.Location;
            this.btnF1.Visible = false;
            this.btnF2.Visible = false;
            this.btnF3.Visible = false;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;
            this.ShowFButton = true;

            // 画面の初期表示
            DispData();

            // フォーカス設定
            this.txtRiekiShobunanChuki1.Focus();
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        public override void PressF6()
        {
            // 確認メッセージを表示
            string msg = ("保存しますか？");
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            try
            {
                DbParamCollection dpc;

                // トランザクション開始
                this.Dba.BeginTransaction();

                dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Int, 4, this.UInfo.KaishaCd);
                dpc.SetParam("@SHISHO_CD", SqlDbType.Int, 4, 0);
                dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
                DataTable dtCheck = this.Dba.GetDataTableByConditionWithParams(
                    "COUNT(*) AS CNT",
                    "TB_ZM_KESSANSHO_CHUKI",
                    "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND KAIKEI_NENDO = @KAIKEI_NENDO",
                    dpc);
                if (Util.ToLong(dtCheck.Rows[0]["CNT"]) == 0)
                {
                    // 編集データ保存
                    string tableNm = "TB_ZM_KESSANSHO_CHUKI";
                    ArrayList alParams = InsZmKessanshoChukiParams();
                    this.Dba.Insert(tableNm, (DbParamCollection)alParams[0]);
                }
                else
                {
                    // 編集データ保存
                    string tableNm = "TB_ZM_KESSANSHO_CHUKI";
                    string where = "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = 0 AND KAIKEI_NENDO = @KAIKEI_NENDO";
                    ArrayList alParams = SetZmKessanshoChukiParams();
                    this.Dba.Update(tableNm, (DbParamCollection)alParams[1], where, (DbParamCollection)alParams[0]);
                }



                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// ESCキー押下時処理
        /// </summary>
        public override void PressEsc()
        {
            base.PressEsc();
        }
        #endregion

        #region イベント

        /// <summary>
        /// 注記テキストボックスValidating時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtChuki_Validating(object sender, CancelEventArgs e)
        {
            FsiTextBox txtBox = (FsiTextBox)sender;

            if (!IsValidChuki(txtBox))
            {
                e.Cancel = true;
                txtBox.SelectAll();
            }
        }

        #endregion

        #region privateメソッド

        /// <summary>
        /// データを表示
        /// </summary>
        private void DispData()
        {
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, 0);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            StringBuilder sql = new StringBuilder();
            #region sql文
            sql.Append("SELECT KAISHA_CD");
            sql.Append(",KAIKEI_NENDO");
            sql.Append(",RIEKI_SHOBUNAN_CHUKI1");
            sql.Append(",RIEKI_SHOBUNAN_CHUKI2");
            sql.Append(",RIEKI_SHOBUNAN_CHUKI3");
            sql.Append(",RIEKI_SHOBUNAN_CHUKI4");
            sql.Append(",RIEKI_SHOBUNAN_CHUKI5");
            sql.Append(",RIEKI_SHOBUNAN_CHUKI6");
            sql.Append(",RIEKI_SHOBUNAN_CHUKI7");
            sql.Append(",RIEKI_SHOBUNAN_CHUKI8");
            sql.Append(",RIEKI_SHOBUNAN_CHUKI9");
            sql.Append(",RIEKI_SHOBUNAN_CHUKI10");
            sql.Append(",RIEKI_SHOBUNAN_CHUKI11");
            sql.Append(",RIEKI_SHOBUNAN_CHUKI12");
            sql.Append(",RIEKI_SHOBUNAN_CHUKI13");
            sql.Append(",RIEKI_SHOBUNAN_CHUKI14");
            sql.Append(",RIEKI_SHOBUNAN_CHUKI15");
            sql.Append(",RIEKI_SHOBUNAN_CHUKI16");
            sql.Append(",RIEKI_SHOBUNAN_CHUKI17");
            sql.Append(",RIEKI_SHOBUNAN_CHUKI18");
            sql.Append(",RIEKI_SHOBUNAN_CHUKI19");
            sql.Append(",RIEKI_SHOBUNAN_CHUKI20");
            sql.Append(" FROM TB_ZM_KESSANSHO_CHUKI");
            sql.Append(" WHERE KAISHA_CD = @KAISHA_CD");
            sql.Append(" AND SHISHO_CD = @SHISHO_CD");
            sql.Append(" AND KAIKEI_NENDO = @KAIKEI_NENDO");
            #endregion
            DataTable dt = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(sql), dpc);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                this.txtRiekiShobunanChuki1.Text = Util.ToString(dr["RIEKI_SHOBUNAN_CHUKI1"]);
                this.txtRiekiShobunanChuki2.Text = Util.ToString(dr["RIEKI_SHOBUNAN_CHUKI2"]);
                this.txtRiekiShobunanChuki3.Text = Util.ToString(dr["RIEKI_SHOBUNAN_CHUKI3"]);
                this.txtRiekiShobunanChuki4.Text = Util.ToString(dr["RIEKI_SHOBUNAN_CHUKI4"]);
                this.txtRiekiShobunanChuki5.Text = Util.ToString(dr["RIEKI_SHOBUNAN_CHUKI5"]);
                this.txtRiekiShobunanChuki6.Text = Util.ToString(dr["RIEKI_SHOBUNAN_CHUKI6"]);
                this.txtRiekiShobunanChuki7.Text = Util.ToString(dr["RIEKI_SHOBUNAN_CHUKI7"]);
                this.txtRiekiShobunanChuki8.Text = Util.ToString(dr["RIEKI_SHOBUNAN_CHUKI8"]);
                this.txtRiekiShobunanChuki9.Text = Util.ToString(dr["RIEKI_SHOBUNAN_CHUKI9"]);
                this.txtRiekiShobunanChuki10.Text = Util.ToString(dr["RIEKI_SHOBUNAN_CHUKI10"]);
                this.txtRiekiShobunanChuki11.Text = Util.ToString(dr["RIEKI_SHOBUNAN_CHUKI11"]);
                this.txtRiekiShobunanChuki12.Text = Util.ToString(dr["RIEKI_SHOBUNAN_CHUKI12"]);
                this.txtRiekiShobunanChuki13.Text = Util.ToString(dr["RIEKI_SHOBUNAN_CHUKI13"]);
                this.txtRiekiShobunanChuki14.Text = Util.ToString(dr["RIEKI_SHOBUNAN_CHUKI14"]);
                this.txtRiekiShobunanChuki15.Text = Util.ToString(dr["RIEKI_SHOBUNAN_CHUKI15"]);
                this.txtRiekiShobunanChuki16.Text = Util.ToString(dr["RIEKI_SHOBUNAN_CHUKI16"]);
                this.txtRiekiShobunanChuki17.Text = Util.ToString(dr["RIEKI_SHOBUNAN_CHUKI17"]);
                this.txtRiekiShobunanChuki18.Text = Util.ToString(dr["RIEKI_SHOBUNAN_CHUKI18"]);
                this.txtRiekiShobunanChuki19.Text = Util.ToString(dr["RIEKI_SHOBUNAN_CHUKI19"]);
                this.txtRiekiShobunanChuki20.Text = Util.ToString(dr["RIEKI_SHOBUNAN_CHUKI20"]);
            }
            else
            {
                this.txtRiekiShobunanChuki1.Text = "上記の通りご報告申し上げます。";
                this.txtRiekiShobunanChuki11.Text = "監査の結果、いずれも適法かつ妥当である事を認めます。";
            }
        }

        /// <summary>
        /// 注記項目の入力チェック
        /// </summary>
        /// <param name="txt"></param>
        /// <returns></returns>
        private bool IsValidChuki(FsiTextBox txt)
        {
            // 入力サイズが指定バイト数以上はNG
            if (!ValChk.IsWithinLength(txt.Text, txt.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                txt.Focus();
                return false;
            }
            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            Control[] ctrl;
            FsiTextBox txtBox;

            for (int i = 1; i <= 20; i++)
            {
                // 利益処分案注記コントロール
                ctrl = this.Controls.Find("txtRiekiShobunanChuki" + Util.ToString(i), true);
                if (ctrl.Length > 0)
                {
                    txtBox = (FsiTextBox)ctrl[0];
                    return IsValidChuki(txtBox);
                }
            }

            return true;
        }

        /// <summary>
        /// TB_ZM_KESSANSHO_CHUKIに更新するためのパラメータ設定をします。
        /// </summary>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 更新処理：DbParamCollection*2(WHERE句・SET句)
        /// </returns>
        private ArrayList SetZmKessanshoChukiParams()
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection whereParam = new DbParamCollection();
            DbParamCollection setParam = new DbParamCollection();

            // WHERE句パラメータ
            whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            whereParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, 0);
            whereParam.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            alParams.Add(whereParam);
            // SET句パラメータ
            setParam.SetParam("@RIEKI_SHOBUNAN_CHUKI1", SqlDbType.VarChar, 128, this.txtRiekiShobunanChuki1.Text);
            setParam.SetParam("@RIEKI_SHOBUNAN_CHUKI2", SqlDbType.VarChar, 128, this.txtRiekiShobunanChuki2.Text);
            setParam.SetParam("@RIEKI_SHOBUNAN_CHUKI3", SqlDbType.VarChar, 128, this.txtRiekiShobunanChuki3.Text);
            setParam.SetParam("@RIEKI_SHOBUNAN_CHUKI4", SqlDbType.VarChar, 128, this.txtRiekiShobunanChuki4.Text);
            setParam.SetParam("@RIEKI_SHOBUNAN_CHUKI5", SqlDbType.VarChar, 128, this.txtRiekiShobunanChuki5.Text);
            setParam.SetParam("@RIEKI_SHOBUNAN_CHUKI6", SqlDbType.VarChar, 128, this.txtRiekiShobunanChuki6.Text);
            setParam.SetParam("@RIEKI_SHOBUNAN_CHUKI7", SqlDbType.VarChar, 128, this.txtRiekiShobunanChuki7.Text);
            setParam.SetParam("@RIEKI_SHOBUNAN_CHUKI8", SqlDbType.VarChar, 128, this.txtRiekiShobunanChuki8.Text);
            setParam.SetParam("@RIEKI_SHOBUNAN_CHUKI9", SqlDbType.VarChar, 128, this.txtRiekiShobunanChuki9.Text);
            setParam.SetParam("@RIEKI_SHOBUNAN_CHUKI10", SqlDbType.VarChar, 128, this.txtRiekiShobunanChuki10.Text);
            setParam.SetParam("@RIEKI_SHOBUNAN_CHUKI11", SqlDbType.VarChar, 128, this.txtRiekiShobunanChuki11.Text);
            setParam.SetParam("@RIEKI_SHOBUNAN_CHUKI12", SqlDbType.VarChar, 128, this.txtRiekiShobunanChuki12.Text);
            setParam.SetParam("@RIEKI_SHOBUNAN_CHUKI13", SqlDbType.VarChar, 128, this.txtRiekiShobunanChuki13.Text);
            setParam.SetParam("@RIEKI_SHOBUNAN_CHUKI14", SqlDbType.VarChar, 128, this.txtRiekiShobunanChuki14.Text);
            setParam.SetParam("@RIEKI_SHOBUNAN_CHUKI15", SqlDbType.VarChar, 128, this.txtRiekiShobunanChuki15.Text);
            setParam.SetParam("@RIEKI_SHOBUNAN_CHUKI16", SqlDbType.VarChar, 128, this.txtRiekiShobunanChuki16.Text);
            setParam.SetParam("@RIEKI_SHOBUNAN_CHUKI17", SqlDbType.VarChar, 128, this.txtRiekiShobunanChuki17.Text);
            setParam.SetParam("@RIEKI_SHOBUNAN_CHUKI18", SqlDbType.VarChar, 128, this.txtRiekiShobunanChuki18.Text);
            setParam.SetParam("@RIEKI_SHOBUNAN_CHUKI19", SqlDbType.VarChar, 128, this.txtRiekiShobunanChuki19.Text);
            setParam.SetParam("@RIEKI_SHOBUNAN_CHUKI20", SqlDbType.VarChar, 128, this.txtRiekiShobunanChuki20.Text);
            setParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWDATE");
            alParams.Add(setParam);

            return alParams;
        }

        private ArrayList InsZmKessanshoChukiParams()
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection dpcInsert = new DbParamCollection();

            dpcInsert.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpcInsert.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, 0);
            dpcInsert.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);

            dpcInsert.SetParam("@RIEKI_SHOBUNAN_CHUKI1", SqlDbType.VarChar, 128, this.txtRiekiShobunanChuki1.Text);
            dpcInsert.SetParam("@RIEKI_SHOBUNAN_CHUKI2", SqlDbType.VarChar, 128, this.txtRiekiShobunanChuki2.Text);
            dpcInsert.SetParam("@RIEKI_SHOBUNAN_CHUKI3", SqlDbType.VarChar, 128, this.txtRiekiShobunanChuki3.Text);
            dpcInsert.SetParam("@RIEKI_SHOBUNAN_CHUKI4", SqlDbType.VarChar, 128, this.txtRiekiShobunanChuki4.Text);
            dpcInsert.SetParam("@RIEKI_SHOBUNAN_CHUKI5", SqlDbType.VarChar, 128, this.txtRiekiShobunanChuki5.Text);
            dpcInsert.SetParam("@RIEKI_SHOBUNAN_CHUKI6", SqlDbType.VarChar, 128, this.txtRiekiShobunanChuki6.Text);
            dpcInsert.SetParam("@RIEKI_SHOBUNAN_CHUKI7", SqlDbType.VarChar, 128, this.txtRiekiShobunanChuki7.Text);
            dpcInsert.SetParam("@RIEKI_SHOBUNAN_CHUKI8", SqlDbType.VarChar, 128, this.txtRiekiShobunanChuki8.Text);
            dpcInsert.SetParam("@RIEKI_SHOBUNAN_CHUKI9", SqlDbType.VarChar, 128, this.txtRiekiShobunanChuki9.Text);
            dpcInsert.SetParam("@RIEKI_SHOBUNAN_CHUKI10", SqlDbType.VarChar, 128, this.txtRiekiShobunanChuki10.Text);
            dpcInsert.SetParam("@RIEKI_SHOBUNAN_CHUKI11", SqlDbType.VarChar, 128, this.txtRiekiShobunanChuki11.Text);
            dpcInsert.SetParam("@RIEKI_SHOBUNAN_CHUKI12", SqlDbType.VarChar, 128, this.txtRiekiShobunanChuki12.Text);
            dpcInsert.SetParam("@RIEKI_SHOBUNAN_CHUKI13", SqlDbType.VarChar, 128, this.txtRiekiShobunanChuki13.Text);
            dpcInsert.SetParam("@RIEKI_SHOBUNAN_CHUKI14", SqlDbType.VarChar, 128, this.txtRiekiShobunanChuki14.Text);
            dpcInsert.SetParam("@RIEKI_SHOBUNAN_CHUKI15", SqlDbType.VarChar, 128, this.txtRiekiShobunanChuki15.Text);
            dpcInsert.SetParam("@RIEKI_SHOBUNAN_CHUKI16", SqlDbType.VarChar, 128, this.txtRiekiShobunanChuki16.Text);
            dpcInsert.SetParam("@RIEKI_SHOBUNAN_CHUKI17", SqlDbType.VarChar, 128, this.txtRiekiShobunanChuki17.Text);
            dpcInsert.SetParam("@RIEKI_SHOBUNAN_CHUKI18", SqlDbType.VarChar, 128, this.txtRiekiShobunanChuki18.Text);
            dpcInsert.SetParam("@RIEKI_SHOBUNAN_CHUKI19", SqlDbType.VarChar, 128, this.txtRiekiShobunanChuki19.Text);
            dpcInsert.SetParam("@RIEKI_SHOBUNAN_CHUKI20", SqlDbType.VarChar, 128, this.txtRiekiShobunanChuki20.Text);
            dpcInsert.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWDATE");
            alParams.Add(dpcInsert);

            return alParams;
        }

        #endregion

    }
}
