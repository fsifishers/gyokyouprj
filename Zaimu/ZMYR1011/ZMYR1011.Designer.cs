﻿namespace jp.co.fsi.zm.zmyr1011
{
    partial class ZMYR1011
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbxDate = new System.Windows.Forms.GroupBox();
            this.lblKikanCodeBet = new System.Windows.Forms.Label();
            this.lblDayTo = new System.Windows.Forms.Label();
            this.lblMonthTo = new System.Windows.Forms.Label();
            this.lblYearTo = new System.Windows.Forms.Label();
            this.txtDayTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtYearTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtMonthTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGengoTo = new System.Windows.Forms.Label();
            this.lblDateTo = new System.Windows.Forms.Label();
            this.lblDayFr = new System.Windows.Forms.Label();
            this.lblMonthFr = new System.Windows.Forms.Label();
            this.lblYearFr = new System.Windows.Forms.Label();
            this.txtDayFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtYearFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtMonthFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGengoFr = new System.Windows.Forms.Label();
            this.lblDateFr = new System.Windows.Forms.Label();
            this.gbxShohizeiShori = new System.Windows.Forms.GroupBox();
            this.rdoZeinuki = new System.Windows.Forms.RadioButton();
            this.rdoZeikomi = new System.Windows.Forms.RadioButton();
            this.gbxBumon = new System.Windows.Forms.GroupBox();
            this.lblBumonTo = new System.Windows.Forms.Label();
            this.lblBumonCodeBet = new System.Windows.Forms.Label();
            this.txtBumonFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblBumonFr = new System.Windows.Forms.Label();
            this.txtBumonTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.gbxInji = new System.Windows.Forms.GroupBox();
            this.rdoYes = new System.Windows.Forms.RadioButton();
            this.rdoNo = new System.Windows.Forms.RadioButton();
            this.gbxShutsuryokuDate = new System.Windows.Forms.GroupBox();
            this.lblDay = new System.Windows.Forms.Label();
            this.lblMonth = new System.Windows.Forms.Label();
            this.lblYear = new System.Windows.Forms.Label();
            this.txtDay = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtYear = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtMonth = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGengo = new System.Windows.Forms.Label();
            this.lblDate = new System.Windows.Forms.Label();
            this.gbxShiwakeShurui = new System.Windows.Forms.GroupBox();
            this.rdoZenbu = new System.Windows.Forms.RadioButton();
            this.rdoKessan = new System.Windows.Forms.RadioButton();
            this.rdoTsujo = new System.Windows.Forms.RadioButton();
            this.cbxTaishaku = new System.Windows.Forms.CheckBox();
            this.cbxSoneki = new System.Windows.Forms.CheckBox();
            this.cbxHanbaihi = new System.Windows.Forms.CheckBox();
            this.cbxRieki = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.gbxMizuageShisho = new System.Windows.Forms.GroupBox();
            this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMizuageShishoNm = new System.Windows.Forms.Label();
            this.lblMizuageShisho = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.gbxDate.SuspendLayout();
            this.gbxShohizeiShori.SuspendLayout();
            this.gbxBumon.SuspendLayout();
            this.gbxInji.SuspendLayout();
            this.gbxShutsuryokuDate.SuspendLayout();
            this.gbxShiwakeShurui.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.gbxMizuageShisho.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Text = "";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            // 
            // gbxDate
            // 
            this.gbxDate.Controls.Add(this.lblKikanCodeBet);
            this.gbxDate.Controls.Add(this.lblDayTo);
            this.gbxDate.Controls.Add(this.lblMonthTo);
            this.gbxDate.Controls.Add(this.lblYearTo);
            this.gbxDate.Controls.Add(this.txtDayTo);
            this.gbxDate.Controls.Add(this.txtYearTo);
            this.gbxDate.Controls.Add(this.txtMonthTo);
            this.gbxDate.Controls.Add(this.lblGengoTo);
            this.gbxDate.Controls.Add(this.lblDateTo);
            this.gbxDate.Controls.Add(this.lblDayFr);
            this.gbxDate.Controls.Add(this.lblMonthFr);
            this.gbxDate.Controls.Add(this.lblYearFr);
            this.gbxDate.Controls.Add(this.txtDayFr);
            this.gbxDate.Controls.Add(this.txtYearFr);
            this.gbxDate.Controls.Add(this.txtMonthFr);
            this.gbxDate.Controls.Add(this.lblGengoFr);
            this.gbxDate.Controls.Add(this.lblDateFr);
            this.gbxDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxDate.ForeColor = System.Drawing.Color.Black;
            this.gbxDate.Location = new System.Drawing.Point(13, 195);
            this.gbxDate.Name = "gbxDate";
            this.gbxDate.Size = new System.Drawing.Size(559, 74);
            this.gbxDate.TabIndex = 2;
            this.gbxDate.TabStop = false;
            this.gbxDate.Text = "期間";
            // 
            // lblKikanCodeBet
            // 
            this.lblKikanCodeBet.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKikanCodeBet.ForeColor = System.Drawing.Color.Black;
            this.lblKikanCodeBet.Location = new System.Drawing.Point(270, 29);
            this.lblKikanCodeBet.Name = "lblKikanCodeBet";
            this.lblKikanCodeBet.Size = new System.Drawing.Size(18, 20);
            this.lblKikanCodeBet.TabIndex = 16;
            this.lblKikanCodeBet.Text = "～";
            this.lblKikanCodeBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDayTo
            // 
            this.lblDayTo.BackColor = System.Drawing.Color.Silver;
            this.lblDayTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDayTo.ForeColor = System.Drawing.Color.Black;
            this.lblDayTo.Location = new System.Drawing.Point(518, 29);
            this.lblDayTo.Name = "lblDayTo";
            this.lblDayTo.Size = new System.Drawing.Size(20, 18);
            this.lblDayTo.TabIndex = 15;
            this.lblDayTo.Text = "日";
            this.lblDayTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMonthTo
            // 
            this.lblMonthTo.BackColor = System.Drawing.Color.Silver;
            this.lblMonthTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMonthTo.ForeColor = System.Drawing.Color.Black;
            this.lblMonthTo.Location = new System.Drawing.Point(463, 29);
            this.lblMonthTo.Name = "lblMonthTo";
            this.lblMonthTo.Size = new System.Drawing.Size(15, 19);
            this.lblMonthTo.TabIndex = 13;
            this.lblMonthTo.Text = "月";
            this.lblMonthTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblYearTo
            // 
            this.lblYearTo.BackColor = System.Drawing.Color.Silver;
            this.lblYearTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYearTo.ForeColor = System.Drawing.Color.Black;
            this.lblYearTo.Location = new System.Drawing.Point(410, 29);
            this.lblYearTo.Name = "lblYearTo";
            this.lblYearTo.Size = new System.Drawing.Size(17, 21);
            this.lblYearTo.TabIndex = 11;
            this.lblYearTo.Text = "年";
            this.lblYearTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDayTo
            // 
            this.txtDayTo.AutoSizeFromLength = false;
            this.txtDayTo.DisplayLength = null;
            this.txtDayTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDayTo.ForeColor = System.Drawing.Color.Black;
            this.txtDayTo.Location = new System.Drawing.Point(485, 29);
            this.txtDayTo.MaxLength = 2;
            this.txtDayTo.Name = "txtDayTo";
            this.txtDayTo.Size = new System.Drawing.Size(30, 20);
            this.txtDayTo.TabIndex = 14;
            this.txtDayTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDayTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDayTo_Validating);
            // 
            // txtYearTo
            // 
            this.txtYearTo.AutoSizeFromLength = false;
            this.txtYearTo.DisplayLength = null;
            this.txtYearTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtYearTo.ForeColor = System.Drawing.Color.Black;
            this.txtYearTo.Location = new System.Drawing.Point(378, 29);
            this.txtYearTo.MaxLength = 2;
            this.txtYearTo.Name = "txtYearTo";
            this.txtYearTo.Size = new System.Drawing.Size(30, 20);
            this.txtYearTo.TabIndex = 10;
            this.txtYearTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtYearTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtYearTo_Validating);
            // 
            // txtMonthTo
            // 
            this.txtMonthTo.AutoSizeFromLength = false;
            this.txtMonthTo.DisplayLength = null;
            this.txtMonthTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMonthTo.ForeColor = System.Drawing.Color.Black;
            this.txtMonthTo.Location = new System.Drawing.Point(431, 29);
            this.txtMonthTo.MaxLength = 2;
            this.txtMonthTo.Name = "txtMonthTo";
            this.txtMonthTo.Size = new System.Drawing.Size(30, 20);
            this.txtMonthTo.TabIndex = 12;
            this.txtMonthTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonthTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthTo_Validating);
            // 
            // lblGengoTo
            // 
            this.lblGengoTo.BackColor = System.Drawing.Color.Silver;
            this.lblGengoTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengoTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengoTo.ForeColor = System.Drawing.Color.Black;
            this.lblGengoTo.Location = new System.Drawing.Point(333, 29);
            this.lblGengoTo.Name = "lblGengoTo";
            this.lblGengoTo.Size = new System.Drawing.Size(41, 21);
            this.lblGengoTo.TabIndex = 8;
            this.lblGengoTo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDateTo
            // 
            this.lblDateTo.BackColor = System.Drawing.Color.Silver;
            this.lblDateTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDateTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateTo.ForeColor = System.Drawing.Color.Black;
            this.lblDateTo.Location = new System.Drawing.Point(330, 26);
            this.lblDateTo.Name = "lblDateTo";
            this.lblDateTo.Size = new System.Drawing.Size(214, 27);
            this.lblDateTo.TabIndex = 9;
            this.lblDateTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDayFr
            // 
            this.lblDayFr.BackColor = System.Drawing.Color.Silver;
            this.lblDayFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDayFr.ForeColor = System.Drawing.Color.Black;
            this.lblDayFr.Location = new System.Drawing.Point(208, 29);
            this.lblDayFr.Name = "lblDayFr";
            this.lblDayFr.Size = new System.Drawing.Size(20, 18);
            this.lblDayFr.TabIndex = 7;
            this.lblDayFr.Text = "日";
            this.lblDayFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMonthFr
            // 
            this.lblMonthFr.BackColor = System.Drawing.Color.Silver;
            this.lblMonthFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMonthFr.ForeColor = System.Drawing.Color.Black;
            this.lblMonthFr.Location = new System.Drawing.Point(153, 29);
            this.lblMonthFr.Name = "lblMonthFr";
            this.lblMonthFr.Size = new System.Drawing.Size(15, 19);
            this.lblMonthFr.TabIndex = 5;
            this.lblMonthFr.Text = "月";
            this.lblMonthFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblYearFr
            // 
            this.lblYearFr.BackColor = System.Drawing.Color.Silver;
            this.lblYearFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYearFr.ForeColor = System.Drawing.Color.Black;
            this.lblYearFr.Location = new System.Drawing.Point(100, 29);
            this.lblYearFr.Name = "lblYearFr";
            this.lblYearFr.Size = new System.Drawing.Size(17, 21);
            this.lblYearFr.TabIndex = 3;
            this.lblYearFr.Text = "年";
            this.lblYearFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDayFr
            // 
            this.txtDayFr.AutoSizeFromLength = false;
            this.txtDayFr.DisplayLength = null;
            this.txtDayFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDayFr.ForeColor = System.Drawing.Color.Black;
            this.txtDayFr.Location = new System.Drawing.Point(175, 29);
            this.txtDayFr.MaxLength = 2;
            this.txtDayFr.Name = "txtDayFr";
            this.txtDayFr.Size = new System.Drawing.Size(30, 20);
            this.txtDayFr.TabIndex = 6;
            this.txtDayFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDayFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDayFr_Validating);
            // 
            // txtYearFr
            // 
            this.txtYearFr.AutoSizeFromLength = false;
            this.txtYearFr.DisplayLength = null;
            this.txtYearFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtYearFr.ForeColor = System.Drawing.Color.Black;
            this.txtYearFr.Location = new System.Drawing.Point(68, 29);
            this.txtYearFr.MaxLength = 2;
            this.txtYearFr.Name = "txtYearFr";
            this.txtYearFr.Size = new System.Drawing.Size(30, 20);
            this.txtYearFr.TabIndex = 2;
            this.txtYearFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtYearFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtYearFr_Validating);
            // 
            // txtMonthFr
            // 
            this.txtMonthFr.AutoSizeFromLength = false;
            this.txtMonthFr.DisplayLength = null;
            this.txtMonthFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMonthFr.ForeColor = System.Drawing.Color.Black;
            this.txtMonthFr.Location = new System.Drawing.Point(121, 29);
            this.txtMonthFr.MaxLength = 2;
            this.txtMonthFr.Name = "txtMonthFr";
            this.txtMonthFr.Size = new System.Drawing.Size(30, 20);
            this.txtMonthFr.TabIndex = 4;
            this.txtMonthFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonthFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthFr_Validating);
            // 
            // lblGengoFr
            // 
            this.lblGengoFr.BackColor = System.Drawing.Color.Silver;
            this.lblGengoFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengoFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengoFr.ForeColor = System.Drawing.Color.Black;
            this.lblGengoFr.Location = new System.Drawing.Point(23, 29);
            this.lblGengoFr.Name = "lblGengoFr";
            this.lblGengoFr.Size = new System.Drawing.Size(41, 21);
            this.lblGengoFr.TabIndex = 1;
            this.lblGengoFr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDateFr
            // 
            this.lblDateFr.BackColor = System.Drawing.Color.Silver;
            this.lblDateFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDateFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateFr.ForeColor = System.Drawing.Color.Black;
            this.lblDateFr.Location = new System.Drawing.Point(20, 26);
            this.lblDateFr.Name = "lblDateFr";
            this.lblDateFr.Size = new System.Drawing.Size(214, 27);
            this.lblDateFr.TabIndex = 1;
            this.lblDateFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gbxShohizeiShori
            // 
            this.gbxShohizeiShori.Controls.Add(this.rdoZeinuki);
            this.gbxShohizeiShori.Controls.Add(this.rdoZeikomi);
            this.gbxShohizeiShori.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxShohizeiShori.ForeColor = System.Drawing.Color.Black;
            this.gbxShohizeiShori.Location = new System.Drawing.Point(352, 116);
            this.gbxShohizeiShori.Name = "gbxShohizeiShori";
            this.gbxShohizeiShori.Size = new System.Drawing.Size(220, 73);
            this.gbxShohizeiShori.TabIndex = 1;
            this.gbxShohizeiShori.TabStop = false;
            this.gbxShohizeiShori.Text = "消費税処理";
            // 
            // rdoZeinuki
            // 
            this.rdoZeinuki.AutoSize = true;
            this.rdoZeinuki.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoZeinuki.ForeColor = System.Drawing.Color.Black;
            this.rdoZeinuki.Location = new System.Drawing.Point(125, 31);
            this.rdoZeinuki.Name = "rdoZeinuki";
            this.rdoZeinuki.Size = new System.Drawing.Size(53, 17);
            this.rdoZeinuki.TabIndex = 1;
            this.rdoZeinuki.TabStop = true;
            this.rdoZeinuki.Text = "税抜";
            this.rdoZeinuki.UseVisualStyleBackColor = true;
            // 
            // rdoZeikomi
            // 
            this.rdoZeikomi.AutoSize = true;
            this.rdoZeikomi.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoZeikomi.ForeColor = System.Drawing.Color.Black;
            this.rdoZeikomi.Location = new System.Drawing.Point(25, 31);
            this.rdoZeikomi.Name = "rdoZeikomi";
            this.rdoZeikomi.Size = new System.Drawing.Size(53, 17);
            this.rdoZeikomi.TabIndex = 0;
            this.rdoZeikomi.TabStop = true;
            this.rdoZeikomi.Text = "税込";
            this.rdoZeikomi.UseVisualStyleBackColor = true;
            // 
            // gbxBumon
            // 
            this.gbxBumon.Controls.Add(this.lblBumonTo);
            this.gbxBumon.Controls.Add(this.lblBumonCodeBet);
            this.gbxBumon.Controls.Add(this.txtBumonFr);
            this.gbxBumon.Controls.Add(this.lblBumonFr);
            this.gbxBumon.Controls.Add(this.txtBumonTo);
            this.gbxBumon.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxBumon.ForeColor = System.Drawing.Color.Black;
            this.gbxBumon.Location = new System.Drawing.Point(13, 275);
            this.gbxBumon.Name = "gbxBumon";
            this.gbxBumon.Size = new System.Drawing.Size(559, 79);
            this.gbxBumon.TabIndex = 3;
            this.gbxBumon.TabStop = false;
            this.gbxBumon.Text = "部門範囲";
            // 
            // lblBumonTo
            // 
            this.lblBumonTo.BackColor = System.Drawing.Color.Silver;
            this.lblBumonTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBumonTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBumonTo.ForeColor = System.Drawing.Color.Black;
            this.lblBumonTo.Location = new System.Drawing.Point(371, 28);
            this.lblBumonTo.Name = "lblBumonTo";
            this.lblBumonTo.Size = new System.Drawing.Size(170, 20);
            this.lblBumonTo.TabIndex = 4;
            this.lblBumonTo.Text = "最　後";
            this.lblBumonTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblBumonCodeBet
            // 
            this.lblBumonCodeBet.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBumonCodeBet.ForeColor = System.Drawing.Color.Black;
            this.lblBumonCodeBet.Location = new System.Drawing.Point(271, 28);
            this.lblBumonCodeBet.Name = "lblBumonCodeBet";
            this.lblBumonCodeBet.Size = new System.Drawing.Size(18, 20);
            this.lblBumonCodeBet.TabIndex = 2;
            this.lblBumonCodeBet.Text = "～";
            this.lblBumonCodeBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtBumonFr
            // 
            this.txtBumonFr.AutoSizeFromLength = false;
            this.txtBumonFr.DisplayLength = null;
            this.txtBumonFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtBumonFr.ForeColor = System.Drawing.Color.Black;
            this.txtBumonFr.Location = new System.Drawing.Point(24, 28);
            this.txtBumonFr.MaxLength = 4;
            this.txtBumonFr.Name = "txtBumonFr";
            this.txtBumonFr.Size = new System.Drawing.Size(40, 20);
            this.txtBumonFr.TabIndex = 0;
            this.txtBumonFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBumonFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtBumonFr_Validating);
            // 
            // lblBumonFr
            // 
            this.lblBumonFr.BackColor = System.Drawing.Color.Silver;
            this.lblBumonFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBumonFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBumonFr.ForeColor = System.Drawing.Color.Black;
            this.lblBumonFr.Location = new System.Drawing.Point(66, 28);
            this.lblBumonFr.Name = "lblBumonFr";
            this.lblBumonFr.Size = new System.Drawing.Size(170, 20);
            this.lblBumonFr.TabIndex = 1;
            this.lblBumonFr.Text = "先　頭";
            this.lblBumonFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtBumonTo
            // 
            this.txtBumonTo.AutoSizeFromLength = false;
            this.txtBumonTo.DisplayLength = null;
            this.txtBumonTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtBumonTo.ForeColor = System.Drawing.Color.Black;
            this.txtBumonTo.Location = new System.Drawing.Point(329, 28);
            this.txtBumonTo.MaxLength = 4;
            this.txtBumonTo.Name = "txtBumonTo";
            this.txtBumonTo.Size = new System.Drawing.Size(40, 20);
            this.txtBumonTo.TabIndex = 3;
            this.txtBumonTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBumonTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtBumonTo_Validating);
            // 
            // gbxInji
            // 
            this.gbxInji.Controls.Add(this.rdoYes);
            this.gbxInji.Controls.Add(this.rdoNo);
            this.gbxInji.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxInji.ForeColor = System.Drawing.Color.Black;
            this.gbxInji.Location = new System.Drawing.Point(13, 360);
            this.gbxInji.Name = "gbxInji";
            this.gbxInji.Size = new System.Drawing.Size(261, 71);
            this.gbxInji.TabIndex = 4;
            this.gbxInji.TabStop = false;
            this.gbxInji.Text = "金額がｾﾞﾛの科目を印字";
            // 
            // rdoYes
            // 
            this.rdoYes.AutoSize = true;
            this.rdoYes.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoYes.ForeColor = System.Drawing.Color.Black;
            this.rdoYes.Location = new System.Drawing.Point(141, 29);
            this.rdoYes.Name = "rdoYes";
            this.rdoYes.Size = new System.Drawing.Size(53, 17);
            this.rdoYes.TabIndex = 1;
            this.rdoYes.TabStop = true;
            this.rdoYes.Text = "する";
            this.rdoYes.UseVisualStyleBackColor = true;
            // 
            // rdoNo
            // 
            this.rdoNo.AutoSize = true;
            this.rdoNo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoNo.ForeColor = System.Drawing.Color.Black;
            this.rdoNo.Location = new System.Drawing.Point(41, 29);
            this.rdoNo.Name = "rdoNo";
            this.rdoNo.Size = new System.Drawing.Size(67, 17);
            this.rdoNo.TabIndex = 0;
            this.rdoNo.TabStop = true;
            this.rdoNo.Text = "しない";
            this.rdoNo.UseVisualStyleBackColor = true;
            // 
            // gbxShutsuryokuDate
            // 
            this.gbxShutsuryokuDate.Controls.Add(this.lblDay);
            this.gbxShutsuryokuDate.Controls.Add(this.lblMonth);
            this.gbxShutsuryokuDate.Controls.Add(this.lblYear);
            this.gbxShutsuryokuDate.Controls.Add(this.txtDay);
            this.gbxShutsuryokuDate.Controls.Add(this.txtYear);
            this.gbxShutsuryokuDate.Controls.Add(this.txtMonth);
            this.gbxShutsuryokuDate.Controls.Add(this.lblGengo);
            this.gbxShutsuryokuDate.Controls.Add(this.lblDate);
            this.gbxShutsuryokuDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxShutsuryokuDate.ForeColor = System.Drawing.Color.Black;
            this.gbxShutsuryokuDate.Location = new System.Drawing.Point(287, 360);
            this.gbxShutsuryokuDate.Name = "gbxShutsuryokuDate";
            this.gbxShutsuryokuDate.Size = new System.Drawing.Size(285, 71);
            this.gbxShutsuryokuDate.TabIndex = 5;
            this.gbxShutsuryokuDate.TabStop = false;
            this.gbxShutsuryokuDate.Text = "出力日付";
            // 
            // lblDay
            // 
            this.lblDay.BackColor = System.Drawing.Color.Silver;
            this.lblDay.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDay.ForeColor = System.Drawing.Color.Black;
            this.lblDay.Location = new System.Drawing.Point(223, 27);
            this.lblDay.Name = "lblDay";
            this.lblDay.Size = new System.Drawing.Size(20, 18);
            this.lblDay.TabIndex = 7;
            this.lblDay.Text = "日";
            this.lblDay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMonth
            // 
            this.lblMonth.BackColor = System.Drawing.Color.Silver;
            this.lblMonth.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMonth.ForeColor = System.Drawing.Color.Black;
            this.lblMonth.Location = new System.Drawing.Point(168, 27);
            this.lblMonth.Name = "lblMonth";
            this.lblMonth.Size = new System.Drawing.Size(15, 19);
            this.lblMonth.TabIndex = 5;
            this.lblMonth.Text = "月";
            this.lblMonth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblYear
            // 
            this.lblYear.BackColor = System.Drawing.Color.Silver;
            this.lblYear.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYear.ForeColor = System.Drawing.Color.Black;
            this.lblYear.Location = new System.Drawing.Point(115, 27);
            this.lblYear.Name = "lblYear";
            this.lblYear.Size = new System.Drawing.Size(17, 21);
            this.lblYear.TabIndex = 3;
            this.lblYear.Text = "年";
            this.lblYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDay
            // 
            this.txtDay.AutoSizeFromLength = false;
            this.txtDay.DisplayLength = null;
            this.txtDay.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDay.ForeColor = System.Drawing.Color.Black;
            this.txtDay.Location = new System.Drawing.Point(190, 27);
            this.txtDay.MaxLength = 2;
            this.txtDay.Name = "txtDay";
            this.txtDay.Size = new System.Drawing.Size(30, 20);
            this.txtDay.TabIndex = 6;
            this.txtDay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDay.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDay_KeyDown);
            this.txtDay.Validating += new System.ComponentModel.CancelEventHandler(this.txtDay_Validating);
            // 
            // txtYear
            // 
            this.txtYear.AutoSizeFromLength = false;
            this.txtYear.DisplayLength = null;
            this.txtYear.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtYear.ForeColor = System.Drawing.Color.Black;
            this.txtYear.Location = new System.Drawing.Point(83, 27);
            this.txtYear.MaxLength = 2;
            this.txtYear.Name = "txtYear";
            this.txtYear.Size = new System.Drawing.Size(30, 20);
            this.txtYear.TabIndex = 2;
            this.txtYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtYear.Validating += new System.ComponentModel.CancelEventHandler(this.txtYear_Validating);
            // 
            // txtMonth
            // 
            this.txtMonth.AutoSizeFromLength = false;
            this.txtMonth.DisplayLength = null;
            this.txtMonth.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMonth.ForeColor = System.Drawing.Color.Black;
            this.txtMonth.Location = new System.Drawing.Point(136, 27);
            this.txtMonth.MaxLength = 2;
            this.txtMonth.Name = "txtMonth";
            this.txtMonth.Size = new System.Drawing.Size(30, 20);
            this.txtMonth.TabIndex = 4;
            this.txtMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonth.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonth_Validating);
            // 
            // lblGengo
            // 
            this.lblGengo.BackColor = System.Drawing.Color.Silver;
            this.lblGengo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengo.ForeColor = System.Drawing.Color.Black;
            this.lblGengo.Location = new System.Drawing.Point(38, 27);
            this.lblGengo.Name = "lblGengo";
            this.lblGengo.Size = new System.Drawing.Size(41, 21);
            this.lblGengo.TabIndex = 1;
            this.lblGengo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDate
            // 
            this.lblDate.BackColor = System.Drawing.Color.Silver;
            this.lblDate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDate.ForeColor = System.Drawing.Color.Black;
            this.lblDate.Location = new System.Drawing.Point(35, 24);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(214, 27);
            this.lblDate.TabIndex = 1;
            this.lblDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gbxShiwakeShurui
            // 
            this.gbxShiwakeShurui.Controls.Add(this.rdoZenbu);
            this.gbxShiwakeShurui.Controls.Add(this.rdoKessan);
            this.gbxShiwakeShurui.Controls.Add(this.rdoTsujo);
            this.gbxShiwakeShurui.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxShiwakeShurui.ForeColor = System.Drawing.Color.Black;
            this.gbxShiwakeShurui.Location = new System.Drawing.Point(13, 116);
            this.gbxShiwakeShurui.Name = "gbxShiwakeShurui";
            this.gbxShiwakeShurui.Size = new System.Drawing.Size(316, 73);
            this.gbxShiwakeShurui.TabIndex = 0;
            this.gbxShiwakeShurui.TabStop = false;
            this.gbxShiwakeShurui.Text = "仕訳種類";
            // 
            // rdoZenbu
            // 
            this.rdoZenbu.AutoSize = true;
            this.rdoZenbu.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoZenbu.ForeColor = System.Drawing.Color.Black;
            this.rdoZenbu.Location = new System.Drawing.Point(222, 31);
            this.rdoZenbu.Name = "rdoZenbu";
            this.rdoZenbu.Size = new System.Drawing.Size(67, 17);
            this.rdoZenbu.TabIndex = 2;
            this.rdoZenbu.TabStop = true;
            this.rdoZenbu.Text = "全仕訳";
            this.rdoZenbu.UseVisualStyleBackColor = true;
            // 
            // rdoKessan
            // 
            this.rdoKessan.AutoSize = true;
            this.rdoKessan.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoKessan.ForeColor = System.Drawing.Color.Black;
            this.rdoKessan.Location = new System.Drawing.Point(122, 31);
            this.rdoKessan.Name = "rdoKessan";
            this.rdoKessan.Size = new System.Drawing.Size(81, 17);
            this.rdoKessan.TabIndex = 1;
            this.rdoKessan.TabStop = true;
            this.rdoKessan.Text = "決算仕訳";
            this.rdoKessan.UseVisualStyleBackColor = true;
            // 
            // rdoTsujo
            // 
            this.rdoTsujo.AutoSize = true;
            this.rdoTsujo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoTsujo.ForeColor = System.Drawing.Color.Black;
            this.rdoTsujo.Location = new System.Drawing.Point(22, 31);
            this.rdoTsujo.Name = "rdoTsujo";
            this.rdoTsujo.Size = new System.Drawing.Size(81, 17);
            this.rdoTsujo.TabIndex = 0;
            this.rdoTsujo.TabStop = true;
            this.rdoTsujo.Text = "通常仕訳";
            this.rdoTsujo.UseVisualStyleBackColor = true;
            // 
            // cbxTaishaku
            // 
            this.cbxTaishaku.AutoSize = true;
            this.cbxTaishaku.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cbxTaishaku.Location = new System.Drawing.Point(22, 27);
            this.cbxTaishaku.Name = "cbxTaishaku";
            this.cbxTaishaku.Size = new System.Drawing.Size(84, 16);
            this.cbxTaishaku.TabIndex = 906;
            this.cbxTaishaku.Text = "貸借対照表";
            this.cbxTaishaku.UseVisualStyleBackColor = true;
            // 
            // cbxSoneki
            // 
            this.cbxSoneki.AutoSize = true;
            this.cbxSoneki.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cbxSoneki.Location = new System.Drawing.Point(144, 27);
            this.cbxSoneki.Name = "cbxSoneki";
            this.cbxSoneki.Size = new System.Drawing.Size(84, 16);
            this.cbxSoneki.TabIndex = 907;
            this.cbxSoneki.Text = "損益計算書";
            this.cbxSoneki.UseVisualStyleBackColor = true;
            // 
            // cbxHanbaihi
            // 
            this.cbxHanbaihi.AutoSize = true;
            this.cbxHanbaihi.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cbxHanbaihi.Location = new System.Drawing.Point(23, 61);
            this.cbxHanbaihi.Name = "cbxHanbaihi";
            this.cbxHanbaihi.Size = new System.Drawing.Size(180, 16);
            this.cbxHanbaihi.TabIndex = 908;
            this.cbxHanbaihi.Text = "販売費及び一般管理費明細書";
            this.cbxHanbaihi.UseVisualStyleBackColor = true;
            // 
            // cbxRieki
            // 
            this.cbxRieki.AutoSize = true;
            this.cbxRieki.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cbxRieki.Location = new System.Drawing.Point(222, 61);
            this.cbxRieki.Name = "cbxRieki";
            this.cbxRieki.Size = new System.Drawing.Size(84, 16);
            this.cbxRieki.TabIndex = 909;
            this.cbxRieki.Text = "利益処分案";
            this.cbxRieki.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbxHanbaihi);
            this.groupBox1.Controls.Add(this.cbxRieki);
            this.groupBox1.Controls.Add(this.cbxTaishaku);
            this.groupBox1.Controls.Add(this.cbxSoneki);
            this.groupBox1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBox1.Location = new System.Drawing.Point(13, 437);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(334, 92);
            this.groupBox1.TabIndex = 910;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "印刷書類";
            // 
            // gbxMizuageShisho
            // 
            this.gbxMizuageShisho.Controls.Add(this.txtMizuageShishoCd);
            this.gbxMizuageShisho.Controls.Add(this.lblMizuageShishoNm);
            this.gbxMizuageShisho.Controls.Add(this.lblMizuageShisho);
            this.gbxMizuageShisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.gbxMizuageShisho.ForeColor = System.Drawing.Color.Black;
            this.gbxMizuageShisho.Location = new System.Drawing.Point(13, 51);
            this.gbxMizuageShisho.Name = "gbxMizuageShisho";
            this.gbxMizuageShisho.Size = new System.Drawing.Size(404, 59);
            this.gbxMizuageShisho.TabIndex = 911;
            this.gbxMizuageShisho.TabStop = false;
            this.gbxMizuageShisho.Text = "支所";
            // 
            // txtMizuageShishoCd
            // 
            this.txtMizuageShishoCd.AutoSizeFromLength = true;
            this.txtMizuageShishoCd.DisplayLength = null;
            this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtMizuageShishoCd.Location = new System.Drawing.Point(71, 22);
            this.txtMizuageShishoCd.MaxLength = 4;
            this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
            this.txtMizuageShishoCd.Size = new System.Drawing.Size(34, 20);
            this.txtMizuageShishoCd.TabIndex = 1;
            this.txtMizuageShishoCd.TabStop = false;
            this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
            // 
            // lblMizuageShishoNm
            // 
            this.lblMizuageShishoNm.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMizuageShishoNm.Location = new System.Drawing.Point(107, 22);
            this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
            this.lblMizuageShishoNm.Size = new System.Drawing.Size(270, 20);
            this.lblMizuageShishoNm.TabIndex = 2;
            this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMizuageShisho
            // 
            this.lblMizuageShisho.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblMizuageShisho.Location = new System.Drawing.Point(27, 20);
            this.lblMizuageShisho.Name = "lblMizuageShisho";
            this.lblMizuageShisho.Size = new System.Drawing.Size(354, 25);
            this.lblMizuageShisho.TabIndex = 0;
            this.lblMizuageShisho.Text = "支所";
            this.lblMizuageShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ZMYR1011
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.gbxMizuageShisho);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gbxShiwakeShurui);
            this.Controls.Add(this.gbxShutsuryokuDate);
            this.Controls.Add(this.gbxInji);
            this.Controls.Add(this.gbxBumon);
            this.Controls.Add(this.gbxShohizeiShori);
            this.Controls.Add(this.gbxDate);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "ZMYR1011";
            this.Text = "";
            this.Controls.SetChildIndex(this.gbxDate, 0);
            this.Controls.SetChildIndex(this.gbxShohizeiShori, 0);
            this.Controls.SetChildIndex(this.gbxBumon, 0);
            this.Controls.SetChildIndex(this.gbxInji, 0);
            this.Controls.SetChildIndex(this.gbxShutsuryokuDate, 0);
            this.Controls.SetChildIndex(this.gbxShiwakeShurui, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.Controls.SetChildIndex(this.gbxMizuageShisho, 0);
            this.pnlDebug.ResumeLayout(false);
            this.gbxDate.ResumeLayout(false);
            this.gbxDate.PerformLayout();
            this.gbxShohizeiShori.ResumeLayout(false);
            this.gbxShohizeiShori.PerformLayout();
            this.gbxBumon.ResumeLayout(false);
            this.gbxBumon.PerformLayout();
            this.gbxInji.ResumeLayout(false);
            this.gbxInji.PerformLayout();
            this.gbxShutsuryokuDate.ResumeLayout(false);
            this.gbxShutsuryokuDate.PerformLayout();
            this.gbxShiwakeShurui.ResumeLayout(false);
            this.gbxShiwakeShurui.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.gbxMizuageShisho.ResumeLayout(false);
            this.gbxMizuageShisho.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbxDate;
        private jp.co.fsi.common.controls.FsiTextBox txtYearFr;
        private System.Windows.Forms.Label lblGengoFr;
        private System.Windows.Forms.Label lblDateFr;
        private System.Windows.Forms.Label lblDayFr;
        private System.Windows.Forms.Label lblMonthFr;
        private System.Windows.Forms.Label lblYearFr;
        private jp.co.fsi.common.controls.FsiTextBox txtDayFr;
        private jp.co.fsi.common.controls.FsiTextBox txtMonthFr;
        private System.Windows.Forms.GroupBox gbxShohizeiShori;
        private System.Windows.Forms.RadioButton rdoZeinuki;
        private System.Windows.Forms.RadioButton rdoZeikomi;
        private System.Windows.Forms.Label lblKikanCodeBet;
        private System.Windows.Forms.Label lblDayTo;
        private System.Windows.Forms.Label lblMonthTo;
        private System.Windows.Forms.Label lblYearTo;
        private jp.co.fsi.common.controls.FsiTextBox txtDayTo;
        private jp.co.fsi.common.controls.FsiTextBox txtYearTo;
        private jp.co.fsi.common.controls.FsiTextBox txtMonthTo;
        private System.Windows.Forms.Label lblGengoTo;
        private System.Windows.Forms.Label lblDateTo;
        private System.Windows.Forms.GroupBox gbxBumon;
        private System.Windows.Forms.Label lblBumonTo;
        private System.Windows.Forms.Label lblBumonCodeBet;
        private jp.co.fsi.common.controls.FsiTextBox txtBumonFr;
        private System.Windows.Forms.Label lblBumonFr;
        private jp.co.fsi.common.controls.FsiTextBox txtBumonTo;
        private System.Windows.Forms.GroupBox gbxInji;
        private System.Windows.Forms.RadioButton rdoYes;
        private System.Windows.Forms.RadioButton rdoNo;
        private System.Windows.Forms.GroupBox gbxShutsuryokuDate;
        private System.Windows.Forms.Label lblDay;
        private System.Windows.Forms.Label lblMonth;
        private System.Windows.Forms.Label lblYear;
        private jp.co.fsi.common.controls.FsiTextBox txtDay;
        private jp.co.fsi.common.controls.FsiTextBox txtYear;
        private jp.co.fsi.common.controls.FsiTextBox txtMonth;
        private System.Windows.Forms.Label lblGengo;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.GroupBox gbxShiwakeShurui;
        private System.Windows.Forms.RadioButton rdoKessan;
        private System.Windows.Forms.RadioButton rdoTsujo;
        private System.Windows.Forms.RadioButton rdoZenbu;
        private System.Windows.Forms.CheckBox cbxTaishaku;
        private System.Windows.Forms.CheckBox cbxSoneki;
        private System.Windows.Forms.CheckBox cbxHanbaihi;
        private System.Windows.Forms.CheckBox cbxRieki;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox gbxMizuageShisho;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.Label lblMizuageShisho;
    }
}