﻿using System;
using System.Data;

using jp.co.fsi.common.report;
using jp.co.fsi.common.util;

using System.Globalization;

namespace jp.co.fsi.zm.zmyr1011
{
    /// <summary>
    /// ZMYR10112R の帳票
    /// </summary>
    public partial class ZMYR10112R : BaseReport
    {

        public ZMYR10112R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }

        /// <summary>
        /// ライン表示 非表示
        /// </summary>
        private void detail_Format(object sender, EventArgs e)
        {
            this.line17.Visible = false;
            this.line20.Visible = false;
            this.line21.Visible = false;
            if (this.txtItem15.Text == "1")
            {
                this.line17.Visible = true;
                this.line20.Visible = true;
            }
            if (this.txtItem14.Text == "1")
            {
                this.line21.Visible = true;
            }
        }
    }
}
