﻿namespace jp.co.fsi.zm.zmyr1011
{
    partial class ZMYR1015
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnl10 = new jp.co.fsi.common.FsiPanel();
            this.txtKingaku3 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKingaku2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtNiniTmttknTorikuzushigaku10 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtNiniTmttknTorikuzushigaku9 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtNiniTmttknTorikuzushigaku8 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtNiniTmttknTorikuzushigaku7 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtNiniTmttknTorikuzushigaku6 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtNiniTmttknTorikuzushigaku5 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtNiniTmttknTorikuzushigaku4 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtNiniTmttknTorikuzushigaku3 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtNiniTmttknTorikuzushigaku2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtNiniTmttknTorikuzushigaku1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKamokuBunruiNm30 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKamokuBunruiNm20 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKanjoKamokuNm90020_10 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKanjoKamokuNm90020_9 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKanjoKamokuNm90020_8 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKanjoKamokuNm90020_7 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKanjoKamokuNm90020_6 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKanjoKamokuNm90020_5 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKanjoKamokuNm90020_4 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKanjoKamokuNm90020_3 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKanjoKamokuNm90020_2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKanjoKamokuNm90020_1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.pnl00 = new jp.co.fsi.common.FsiPanel();
            this.txtKingaku1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKamokuBunruiNm10 = new jp.co.fsi.common.controls.FsiTextBox();
            this.panel1 = new jp.co.fsi.common.FsiPanel();
            this.txtKingaku5 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKingaku4 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtRiekikinShobungaku10 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtRiekikinShobungaku9 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtRiekikinShobungaku8 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtRiekikinShobungaku7 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtRiekikinShobungaku6 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtRiekikinShobungaku5 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtRiekikinShobungaku4 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtRiekikinShobungaku3 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtRiekikinShobungaku2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtRiekikinShobungaku1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKamokuBunruiNm50 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKamokuBunruiNm40 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKanjoKamokuNm90040_10 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKanjoKamokuNm90040_9 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKanjoKamokuNm90040_8 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKanjoKamokuNm90040_7 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKanjoKamokuNm90040_6 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKanjoKamokuNm90040_5 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKanjoKamokuNm90040_4 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKanjoKamokuNm90040_3 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKanjoKamokuNm90040_2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKanjoKamokuNm90040_1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.pnl10.SuspendLayout();
            this.pnl00.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.BackColor = System.Drawing.Color.White;
            this.lblTitle.Location = new System.Drawing.Point(4, 9);
            this.lblTitle.Size = new System.Drawing.Size(621, 23);
            this.lblTitle.Text = "";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(5, 521);
            this.pnlDebug.Size = new System.Drawing.Size(638, 100);
            // 
            // pnl10
            // 
            this.pnl10.BackColor = System.Drawing.Color.LightGray;
            this.pnl10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnl10.Controls.Add(this.txtKingaku3);
            this.pnl10.Controls.Add(this.txtKingaku2);
            this.pnl10.Controls.Add(this.txtNiniTmttknTorikuzushigaku10);
            this.pnl10.Controls.Add(this.txtNiniTmttknTorikuzushigaku9);
            this.pnl10.Controls.Add(this.txtNiniTmttknTorikuzushigaku8);
            this.pnl10.Controls.Add(this.txtNiniTmttknTorikuzushigaku7);
            this.pnl10.Controls.Add(this.txtNiniTmttknTorikuzushigaku6);
            this.pnl10.Controls.Add(this.txtNiniTmttknTorikuzushigaku5);
            this.pnl10.Controls.Add(this.txtNiniTmttknTorikuzushigaku4);
            this.pnl10.Controls.Add(this.txtNiniTmttknTorikuzushigaku3);
            this.pnl10.Controls.Add(this.txtNiniTmttknTorikuzushigaku2);
            this.pnl10.Controls.Add(this.txtNiniTmttknTorikuzushigaku1);
            this.pnl10.Controls.Add(this.txtKamokuBunruiNm30);
            this.pnl10.Controls.Add(this.txtKamokuBunruiNm20);
            this.pnl10.Controls.Add(this.txtKanjoKamokuNm90020_10);
            this.pnl10.Controls.Add(this.txtKanjoKamokuNm90020_9);
            this.pnl10.Controls.Add(this.txtKanjoKamokuNm90020_8);
            this.pnl10.Controls.Add(this.txtKanjoKamokuNm90020_7);
            this.pnl10.Controls.Add(this.txtKanjoKamokuNm90020_6);
            this.pnl10.Controls.Add(this.txtKanjoKamokuNm90020_5);
            this.pnl10.Controls.Add(this.txtKanjoKamokuNm90020_4);
            this.pnl10.Controls.Add(this.txtKanjoKamokuNm90020_3);
            this.pnl10.Controls.Add(this.txtKanjoKamokuNm90020_2);
            this.pnl10.Controls.Add(this.txtKanjoKamokuNm90020_1);
            this.pnl10.Location = new System.Drawing.Point(24, 63);
            this.pnl10.Name = "pnl10";
            this.pnl10.Size = new System.Drawing.Size(593, 249);
            this.pnl10.TabIndex = 1;
            // 
            // txtKingaku3
            // 
            this.txtKingaku3.AutoSizeFromLength = false;
            this.txtKingaku3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.txtKingaku3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtKingaku3.DisplayLength = null;
            this.txtKingaku3.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKingaku3.ForeColor = System.Drawing.Color.Black;
            this.txtKingaku3.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKingaku3.Location = new System.Drawing.Point(412, 223);
            this.txtKingaku3.MaxLength = 128;
            this.txtKingaku3.Name = "txtKingaku3";
            this.txtKingaku3.ReadOnly = true;
            this.txtKingaku3.Size = new System.Drawing.Size(173, 20);
            this.txtKingaku3.TabIndex = 23;
            this.txtKingaku3.TabStop = false;
            this.txtKingaku3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtKingaku2
            // 
            this.txtKingaku2.AutoSizeFromLength = false;
            this.txtKingaku2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.txtKingaku2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtKingaku2.DisplayLength = null;
            this.txtKingaku2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKingaku2.ForeColor = System.Drawing.Color.Black;
            this.txtKingaku2.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKingaku2.Location = new System.Drawing.Point(412, 203);
            this.txtKingaku2.MaxLength = 128;
            this.txtKingaku2.Name = "txtKingaku2";
            this.txtKingaku2.ReadOnly = true;
            this.txtKingaku2.Size = new System.Drawing.Size(173, 20);
            this.txtKingaku2.TabIndex = 22;
            this.txtKingaku2.TabStop = false;
            this.txtKingaku2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtNiniTmttknTorikuzushigaku10
            // 
            this.txtNiniTmttknTorikuzushigaku10.AutoSizeFromLength = false;
            this.txtNiniTmttknTorikuzushigaku10.DisplayLength = null;
            this.txtNiniTmttknTorikuzushigaku10.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtNiniTmttknTorikuzushigaku10.ForeColor = System.Drawing.Color.Black;
            this.txtNiniTmttknTorikuzushigaku10.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtNiniTmttknTorikuzushigaku10.Location = new System.Drawing.Point(239, 203);
            this.txtNiniTmttknTorikuzushigaku10.MaxLength = 15;
            this.txtNiniTmttknTorikuzushigaku10.Name = "txtNiniTmttknTorikuzushigaku10";
            this.txtNiniTmttknTorikuzushigaku10.Size = new System.Drawing.Size(173, 20);
            this.txtNiniTmttknTorikuzushigaku10.TabIndex = 20;
            this.txtNiniTmttknTorikuzushigaku10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtNiniTmttknTorikuzushigaku10.Enter += new System.EventHandler(this.txtKingaku_Enter);
            this.txtNiniTmttknTorikuzushigaku10.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
            // 
            // txtNiniTmttknTorikuzushigaku9
            // 
            this.txtNiniTmttknTorikuzushigaku9.AutoSizeFromLength = false;
            this.txtNiniTmttknTorikuzushigaku9.DisplayLength = null;
            this.txtNiniTmttknTorikuzushigaku9.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtNiniTmttknTorikuzushigaku9.ForeColor = System.Drawing.Color.Black;
            this.txtNiniTmttknTorikuzushigaku9.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtNiniTmttknTorikuzushigaku9.Location = new System.Drawing.Point(239, 183);
            this.txtNiniTmttknTorikuzushigaku9.MaxLength = 15;
            this.txtNiniTmttknTorikuzushigaku9.Name = "txtNiniTmttknTorikuzushigaku9";
            this.txtNiniTmttknTorikuzushigaku9.Size = new System.Drawing.Size(173, 20);
            this.txtNiniTmttknTorikuzushigaku9.TabIndex = 18;
            this.txtNiniTmttknTorikuzushigaku9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtNiniTmttknTorikuzushigaku9.Enter += new System.EventHandler(this.txtKingaku_Enter);
            this.txtNiniTmttknTorikuzushigaku9.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
            // 
            // txtNiniTmttknTorikuzushigaku8
            // 
            this.txtNiniTmttknTorikuzushigaku8.AutoSizeFromLength = false;
            this.txtNiniTmttknTorikuzushigaku8.DisplayLength = null;
            this.txtNiniTmttknTorikuzushigaku8.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtNiniTmttknTorikuzushigaku8.ForeColor = System.Drawing.Color.Black;
            this.txtNiniTmttknTorikuzushigaku8.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtNiniTmttknTorikuzushigaku8.Location = new System.Drawing.Point(239, 163);
            this.txtNiniTmttknTorikuzushigaku8.MaxLength = 15;
            this.txtNiniTmttknTorikuzushigaku8.Name = "txtNiniTmttknTorikuzushigaku8";
            this.txtNiniTmttknTorikuzushigaku8.Size = new System.Drawing.Size(173, 20);
            this.txtNiniTmttknTorikuzushigaku8.TabIndex = 16;
            this.txtNiniTmttknTorikuzushigaku8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtNiniTmttknTorikuzushigaku8.Enter += new System.EventHandler(this.txtKingaku_Enter);
            this.txtNiniTmttknTorikuzushigaku8.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
            // 
            // txtNiniTmttknTorikuzushigaku7
            // 
            this.txtNiniTmttknTorikuzushigaku7.AutoSizeFromLength = false;
            this.txtNiniTmttknTorikuzushigaku7.DisplayLength = null;
            this.txtNiniTmttknTorikuzushigaku7.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtNiniTmttknTorikuzushigaku7.ForeColor = System.Drawing.Color.Black;
            this.txtNiniTmttknTorikuzushigaku7.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtNiniTmttknTorikuzushigaku7.Location = new System.Drawing.Point(239, 143);
            this.txtNiniTmttknTorikuzushigaku7.MaxLength = 15;
            this.txtNiniTmttknTorikuzushigaku7.Name = "txtNiniTmttknTorikuzushigaku7";
            this.txtNiniTmttknTorikuzushigaku7.Size = new System.Drawing.Size(173, 20);
            this.txtNiniTmttknTorikuzushigaku7.TabIndex = 14;
            this.txtNiniTmttknTorikuzushigaku7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtNiniTmttknTorikuzushigaku7.Enter += new System.EventHandler(this.txtKingaku_Enter);
            this.txtNiniTmttknTorikuzushigaku7.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
            // 
            // txtNiniTmttknTorikuzushigaku6
            // 
            this.txtNiniTmttknTorikuzushigaku6.AutoSizeFromLength = false;
            this.txtNiniTmttknTorikuzushigaku6.DisplayLength = null;
            this.txtNiniTmttknTorikuzushigaku6.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtNiniTmttknTorikuzushigaku6.ForeColor = System.Drawing.Color.Black;
            this.txtNiniTmttknTorikuzushigaku6.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtNiniTmttknTorikuzushigaku6.Location = new System.Drawing.Point(239, 123);
            this.txtNiniTmttknTorikuzushigaku6.MaxLength = 15;
            this.txtNiniTmttknTorikuzushigaku6.Name = "txtNiniTmttknTorikuzushigaku6";
            this.txtNiniTmttknTorikuzushigaku6.Size = new System.Drawing.Size(173, 20);
            this.txtNiniTmttknTorikuzushigaku6.TabIndex = 12;
            this.txtNiniTmttknTorikuzushigaku6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtNiniTmttknTorikuzushigaku6.Enter += new System.EventHandler(this.txtKingaku_Enter);
            this.txtNiniTmttknTorikuzushigaku6.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
            // 
            // txtNiniTmttknTorikuzushigaku5
            // 
            this.txtNiniTmttknTorikuzushigaku5.AutoSizeFromLength = false;
            this.txtNiniTmttknTorikuzushigaku5.DisplayLength = null;
            this.txtNiniTmttknTorikuzushigaku5.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtNiniTmttknTorikuzushigaku5.ForeColor = System.Drawing.Color.Black;
            this.txtNiniTmttknTorikuzushigaku5.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtNiniTmttknTorikuzushigaku5.Location = new System.Drawing.Point(239, 103);
            this.txtNiniTmttknTorikuzushigaku5.MaxLength = 15;
            this.txtNiniTmttknTorikuzushigaku5.Name = "txtNiniTmttknTorikuzushigaku5";
            this.txtNiniTmttknTorikuzushigaku5.Size = new System.Drawing.Size(173, 20);
            this.txtNiniTmttknTorikuzushigaku5.TabIndex = 10;
            this.txtNiniTmttknTorikuzushigaku5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtNiniTmttknTorikuzushigaku5.Enter += new System.EventHandler(this.txtKingaku_Enter);
            this.txtNiniTmttknTorikuzushigaku5.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
            // 
            // txtNiniTmttknTorikuzushigaku4
            // 
            this.txtNiniTmttknTorikuzushigaku4.AutoSizeFromLength = false;
            this.txtNiniTmttknTorikuzushigaku4.DisplayLength = null;
            this.txtNiniTmttknTorikuzushigaku4.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtNiniTmttknTorikuzushigaku4.ForeColor = System.Drawing.Color.Black;
            this.txtNiniTmttknTorikuzushigaku4.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtNiniTmttknTorikuzushigaku4.Location = new System.Drawing.Point(239, 83);
            this.txtNiniTmttknTorikuzushigaku4.MaxLength = 15;
            this.txtNiniTmttknTorikuzushigaku4.Name = "txtNiniTmttknTorikuzushigaku4";
            this.txtNiniTmttknTorikuzushigaku4.Size = new System.Drawing.Size(173, 20);
            this.txtNiniTmttknTorikuzushigaku4.TabIndex = 8;
            this.txtNiniTmttknTorikuzushigaku4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtNiniTmttknTorikuzushigaku4.Enter += new System.EventHandler(this.txtKingaku_Enter);
            this.txtNiniTmttknTorikuzushigaku4.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
            // 
            // txtNiniTmttknTorikuzushigaku3
            // 
            this.txtNiniTmttknTorikuzushigaku3.AutoSizeFromLength = false;
            this.txtNiniTmttknTorikuzushigaku3.DisplayLength = null;
            this.txtNiniTmttknTorikuzushigaku3.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtNiniTmttknTorikuzushigaku3.ForeColor = System.Drawing.Color.Black;
            this.txtNiniTmttknTorikuzushigaku3.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtNiniTmttknTorikuzushigaku3.Location = new System.Drawing.Point(239, 63);
            this.txtNiniTmttknTorikuzushigaku3.MaxLength = 15;
            this.txtNiniTmttknTorikuzushigaku3.Name = "txtNiniTmttknTorikuzushigaku3";
            this.txtNiniTmttknTorikuzushigaku3.Size = new System.Drawing.Size(173, 20);
            this.txtNiniTmttknTorikuzushigaku3.TabIndex = 6;
            this.txtNiniTmttknTorikuzushigaku3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtNiniTmttknTorikuzushigaku3.Enter += new System.EventHandler(this.txtKingaku_Enter);
            this.txtNiniTmttknTorikuzushigaku3.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
            // 
            // txtNiniTmttknTorikuzushigaku2
            // 
            this.txtNiniTmttknTorikuzushigaku2.AutoSizeFromLength = false;
            this.txtNiniTmttknTorikuzushigaku2.DisplayLength = null;
            this.txtNiniTmttknTorikuzushigaku2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtNiniTmttknTorikuzushigaku2.ForeColor = System.Drawing.Color.Black;
            this.txtNiniTmttknTorikuzushigaku2.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtNiniTmttknTorikuzushigaku2.Location = new System.Drawing.Point(239, 43);
            this.txtNiniTmttknTorikuzushigaku2.MaxLength = 15;
            this.txtNiniTmttknTorikuzushigaku2.Name = "txtNiniTmttknTorikuzushigaku2";
            this.txtNiniTmttknTorikuzushigaku2.Size = new System.Drawing.Size(173, 20);
            this.txtNiniTmttknTorikuzushigaku2.TabIndex = 4;
            this.txtNiniTmttknTorikuzushigaku2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtNiniTmttknTorikuzushigaku2.Enter += new System.EventHandler(this.txtKingaku_Enter);
            this.txtNiniTmttknTorikuzushigaku2.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
            // 
            // txtNiniTmttknTorikuzushigaku1
            // 
            this.txtNiniTmttknTorikuzushigaku1.AutoSizeFromLength = false;
            this.txtNiniTmttknTorikuzushigaku1.DisplayLength = null;
            this.txtNiniTmttknTorikuzushigaku1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtNiniTmttknTorikuzushigaku1.ForeColor = System.Drawing.Color.Black;
            this.txtNiniTmttknTorikuzushigaku1.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtNiniTmttknTorikuzushigaku1.Location = new System.Drawing.Point(239, 23);
            this.txtNiniTmttknTorikuzushigaku1.MaxLength = 15;
            this.txtNiniTmttknTorikuzushigaku1.Name = "txtNiniTmttknTorikuzushigaku1";
            this.txtNiniTmttknTorikuzushigaku1.Size = new System.Drawing.Size(173, 20);
            this.txtNiniTmttknTorikuzushigaku1.TabIndex = 2;
            this.txtNiniTmttknTorikuzushigaku1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtNiniTmttknTorikuzushigaku1.Enter += new System.EventHandler(this.txtKingaku_Enter);
            this.txtNiniTmttknTorikuzushigaku1.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
            // 
            // txtKamokuBunruiNm30
            // 
            this.txtKamokuBunruiNm30.AutoSizeFromLength = false;
            this.txtKamokuBunruiNm30.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txtKamokuBunruiNm30.DisplayLength = null;
            this.txtKamokuBunruiNm30.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKamokuBunruiNm30.ForeColor = System.Drawing.Color.Black;
            this.txtKamokuBunruiNm30.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtKamokuBunruiNm30.Location = new System.Drawing.Point(3, 223);
            this.txtKamokuBunruiNm30.MaxLength = 30;
            this.txtKamokuBunruiNm30.Name = "txtKamokuBunruiNm30";
            this.txtKamokuBunruiNm30.Size = new System.Drawing.Size(236, 20);
            this.txtKamokuBunruiNm30.TabIndex = 21;
            this.txtKamokuBunruiNm30.Validating += new System.ComponentModel.CancelEventHandler(this.txtText_Validating);
            // 
            // txtKamokuBunruiNm20
            // 
            this.txtKamokuBunruiNm20.AutoSizeFromLength = false;
            this.txtKamokuBunruiNm20.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txtKamokuBunruiNm20.DisplayLength = null;
            this.txtKamokuBunruiNm20.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKamokuBunruiNm20.ForeColor = System.Drawing.Color.Black;
            this.txtKamokuBunruiNm20.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtKamokuBunruiNm20.Location = new System.Drawing.Point(3, 3);
            this.txtKamokuBunruiNm20.MaxLength = 30;
            this.txtKamokuBunruiNm20.Name = "txtKamokuBunruiNm20";
            this.txtKamokuBunruiNm20.Size = new System.Drawing.Size(236, 20);
            this.txtKamokuBunruiNm20.TabIndex = 0;
            this.txtKamokuBunruiNm20.Validating += new System.ComponentModel.CancelEventHandler(this.txtText_Validating);
            // 
            // txtKanjoKamokuNm90020_10
            // 
            this.txtKanjoKamokuNm90020_10.AutoSizeFromLength = false;
            this.txtKanjoKamokuNm90020_10.DisplayLength = null;
            this.txtKanjoKamokuNm90020_10.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKanjoKamokuNm90020_10.ForeColor = System.Drawing.Color.Black;
            this.txtKanjoKamokuNm90020_10.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtKanjoKamokuNm90020_10.Location = new System.Drawing.Point(3, 203);
            this.txtKanjoKamokuNm90020_10.MaxLength = 30;
            this.txtKanjoKamokuNm90020_10.Name = "txtKanjoKamokuNm90020_10";
            this.txtKanjoKamokuNm90020_10.Size = new System.Drawing.Size(236, 20);
            this.txtKanjoKamokuNm90020_10.TabIndex = 19;
            this.txtKanjoKamokuNm90020_10.Validating += new System.ComponentModel.CancelEventHandler(this.txtText_Validating);
            // 
            // txtKanjoKamokuNm90020_9
            // 
            this.txtKanjoKamokuNm90020_9.AutoSizeFromLength = false;
            this.txtKanjoKamokuNm90020_9.DisplayLength = null;
            this.txtKanjoKamokuNm90020_9.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKanjoKamokuNm90020_9.ForeColor = System.Drawing.Color.Black;
            this.txtKanjoKamokuNm90020_9.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtKanjoKamokuNm90020_9.Location = new System.Drawing.Point(3, 183);
            this.txtKanjoKamokuNm90020_9.MaxLength = 30;
            this.txtKanjoKamokuNm90020_9.Name = "txtKanjoKamokuNm90020_9";
            this.txtKanjoKamokuNm90020_9.Size = new System.Drawing.Size(236, 20);
            this.txtKanjoKamokuNm90020_9.TabIndex = 17;
            this.txtKanjoKamokuNm90020_9.Validating += new System.ComponentModel.CancelEventHandler(this.txtText_Validating);
            // 
            // txtKanjoKamokuNm90020_8
            // 
            this.txtKanjoKamokuNm90020_8.AutoSizeFromLength = false;
            this.txtKanjoKamokuNm90020_8.DisplayLength = null;
            this.txtKanjoKamokuNm90020_8.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKanjoKamokuNm90020_8.ForeColor = System.Drawing.Color.Black;
            this.txtKanjoKamokuNm90020_8.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtKanjoKamokuNm90020_8.Location = new System.Drawing.Point(3, 163);
            this.txtKanjoKamokuNm90020_8.MaxLength = 30;
            this.txtKanjoKamokuNm90020_8.Name = "txtKanjoKamokuNm90020_8";
            this.txtKanjoKamokuNm90020_8.Size = new System.Drawing.Size(236, 20);
            this.txtKanjoKamokuNm90020_8.TabIndex = 15;
            this.txtKanjoKamokuNm90020_8.Validating += new System.ComponentModel.CancelEventHandler(this.txtText_Validating);
            // 
            // txtKanjoKamokuNm90020_7
            // 
            this.txtKanjoKamokuNm90020_7.AutoSizeFromLength = false;
            this.txtKanjoKamokuNm90020_7.DisplayLength = null;
            this.txtKanjoKamokuNm90020_7.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKanjoKamokuNm90020_7.ForeColor = System.Drawing.Color.Black;
            this.txtKanjoKamokuNm90020_7.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtKanjoKamokuNm90020_7.Location = new System.Drawing.Point(3, 143);
            this.txtKanjoKamokuNm90020_7.MaxLength = 30;
            this.txtKanjoKamokuNm90020_7.Name = "txtKanjoKamokuNm90020_7";
            this.txtKanjoKamokuNm90020_7.Size = new System.Drawing.Size(236, 20);
            this.txtKanjoKamokuNm90020_7.TabIndex = 13;
            this.txtKanjoKamokuNm90020_7.Validating += new System.ComponentModel.CancelEventHandler(this.txtText_Validating);
            // 
            // txtKanjoKamokuNm90020_6
            // 
            this.txtKanjoKamokuNm90020_6.AutoSizeFromLength = false;
            this.txtKanjoKamokuNm90020_6.DisplayLength = null;
            this.txtKanjoKamokuNm90020_6.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKanjoKamokuNm90020_6.ForeColor = System.Drawing.Color.Black;
            this.txtKanjoKamokuNm90020_6.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtKanjoKamokuNm90020_6.Location = new System.Drawing.Point(3, 123);
            this.txtKanjoKamokuNm90020_6.MaxLength = 30;
            this.txtKanjoKamokuNm90020_6.Name = "txtKanjoKamokuNm90020_6";
            this.txtKanjoKamokuNm90020_6.Size = new System.Drawing.Size(236, 20);
            this.txtKanjoKamokuNm90020_6.TabIndex = 11;
            this.txtKanjoKamokuNm90020_6.Validating += new System.ComponentModel.CancelEventHandler(this.txtText_Validating);
            // 
            // txtKanjoKamokuNm90020_5
            // 
            this.txtKanjoKamokuNm90020_5.AutoSizeFromLength = false;
            this.txtKanjoKamokuNm90020_5.DisplayLength = null;
            this.txtKanjoKamokuNm90020_5.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKanjoKamokuNm90020_5.ForeColor = System.Drawing.Color.Black;
            this.txtKanjoKamokuNm90020_5.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtKanjoKamokuNm90020_5.Location = new System.Drawing.Point(3, 103);
            this.txtKanjoKamokuNm90020_5.MaxLength = 30;
            this.txtKanjoKamokuNm90020_5.Name = "txtKanjoKamokuNm90020_5";
            this.txtKanjoKamokuNm90020_5.Size = new System.Drawing.Size(236, 20);
            this.txtKanjoKamokuNm90020_5.TabIndex = 9;
            this.txtKanjoKamokuNm90020_5.Validating += new System.ComponentModel.CancelEventHandler(this.txtText_Validating);
            // 
            // txtKanjoKamokuNm90020_4
            // 
            this.txtKanjoKamokuNm90020_4.AutoSizeFromLength = false;
            this.txtKanjoKamokuNm90020_4.DisplayLength = null;
            this.txtKanjoKamokuNm90020_4.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKanjoKamokuNm90020_4.ForeColor = System.Drawing.Color.Black;
            this.txtKanjoKamokuNm90020_4.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtKanjoKamokuNm90020_4.Location = new System.Drawing.Point(3, 83);
            this.txtKanjoKamokuNm90020_4.MaxLength = 30;
            this.txtKanjoKamokuNm90020_4.Name = "txtKanjoKamokuNm90020_4";
            this.txtKanjoKamokuNm90020_4.Size = new System.Drawing.Size(236, 20);
            this.txtKanjoKamokuNm90020_4.TabIndex = 7;
            this.txtKanjoKamokuNm90020_4.Validating += new System.ComponentModel.CancelEventHandler(this.txtText_Validating);
            // 
            // txtKanjoKamokuNm90020_3
            // 
            this.txtKanjoKamokuNm90020_3.AutoSizeFromLength = false;
            this.txtKanjoKamokuNm90020_3.DisplayLength = null;
            this.txtKanjoKamokuNm90020_3.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKanjoKamokuNm90020_3.ForeColor = System.Drawing.Color.Black;
            this.txtKanjoKamokuNm90020_3.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtKanjoKamokuNm90020_3.Location = new System.Drawing.Point(3, 63);
            this.txtKanjoKamokuNm90020_3.MaxLength = 30;
            this.txtKanjoKamokuNm90020_3.Name = "txtKanjoKamokuNm90020_3";
            this.txtKanjoKamokuNm90020_3.Size = new System.Drawing.Size(236, 20);
            this.txtKanjoKamokuNm90020_3.TabIndex = 5;
            this.txtKanjoKamokuNm90020_3.Validating += new System.ComponentModel.CancelEventHandler(this.txtText_Validating);
            // 
            // txtKanjoKamokuNm90020_2
            // 
            this.txtKanjoKamokuNm90020_2.AutoSizeFromLength = false;
            this.txtKanjoKamokuNm90020_2.DisplayLength = null;
            this.txtKanjoKamokuNm90020_2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKanjoKamokuNm90020_2.ForeColor = System.Drawing.Color.Black;
            this.txtKanjoKamokuNm90020_2.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtKanjoKamokuNm90020_2.Location = new System.Drawing.Point(3, 43);
            this.txtKanjoKamokuNm90020_2.MaxLength = 30;
            this.txtKanjoKamokuNm90020_2.Name = "txtKanjoKamokuNm90020_2";
            this.txtKanjoKamokuNm90020_2.Size = new System.Drawing.Size(236, 20);
            this.txtKanjoKamokuNm90020_2.TabIndex = 3;
            this.txtKanjoKamokuNm90020_2.Validating += new System.ComponentModel.CancelEventHandler(this.txtText_Validating);
            // 
            // txtKanjoKamokuNm90020_1
            // 
            this.txtKanjoKamokuNm90020_1.AutoSizeFromLength = false;
            this.txtKanjoKamokuNm90020_1.DisplayLength = null;
            this.txtKanjoKamokuNm90020_1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKanjoKamokuNm90020_1.ForeColor = System.Drawing.Color.Black;
            this.txtKanjoKamokuNm90020_1.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtKanjoKamokuNm90020_1.Location = new System.Drawing.Point(3, 23);
            this.txtKanjoKamokuNm90020_1.MaxLength = 30;
            this.txtKanjoKamokuNm90020_1.Name = "txtKanjoKamokuNm90020_1";
            this.txtKanjoKamokuNm90020_1.Size = new System.Drawing.Size(236, 20);
            this.txtKanjoKamokuNm90020_1.TabIndex = 1;
            this.txtKanjoKamokuNm90020_1.Validating += new System.ComponentModel.CancelEventHandler(this.txtText_Validating);
            // 
            // pnl00
            // 
            this.pnl00.BackColor = System.Drawing.Color.LightGray;
            this.pnl00.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnl00.Controls.Add(this.txtKingaku1);
            this.pnl00.Controls.Add(this.txtKamokuBunruiNm10);
            this.pnl00.Location = new System.Drawing.Point(24, 29);
            this.pnl00.Name = "pnl00";
            this.pnl00.Size = new System.Drawing.Size(593, 30);
            this.pnl00.TabIndex = 0;
            // 
            // txtKingaku1
            // 
            this.txtKingaku1.AutoSizeFromLength = false;
            this.txtKingaku1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.txtKingaku1.DisplayLength = null;
            this.txtKingaku1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKingaku1.ForeColor = System.Drawing.Color.Black;
            this.txtKingaku1.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKingaku1.Location = new System.Drawing.Point(412, 4);
            this.txtKingaku1.MaxLength = 128;
            this.txtKingaku1.Name = "txtKingaku1";
            this.txtKingaku1.Size = new System.Drawing.Size(173, 20);
            this.txtKingaku1.TabIndex = 1;
            this.txtKingaku1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKingaku1.Enter += new System.EventHandler(this.txtKingaku_Enter);
            this.txtKingaku1.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
            // 
            // txtKamokuBunruiNm10
            // 
            this.txtKamokuBunruiNm10.AutoSizeFromLength = false;
            this.txtKamokuBunruiNm10.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txtKamokuBunruiNm10.DisplayLength = null;
            this.txtKamokuBunruiNm10.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKamokuBunruiNm10.ForeColor = System.Drawing.Color.Black;
            this.txtKamokuBunruiNm10.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtKamokuBunruiNm10.Location = new System.Drawing.Point(3, 3);
            this.txtKamokuBunruiNm10.MaxLength = 30;
            this.txtKamokuBunruiNm10.Name = "txtKamokuBunruiNm10";
            this.txtKamokuBunruiNm10.Size = new System.Drawing.Size(236, 20);
            this.txtKamokuBunruiNm10.TabIndex = 0;
            this.txtKamokuBunruiNm10.Validating += new System.ComponentModel.CancelEventHandler(this.txtText_Validating);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.LightGray;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.txtKingaku5);
            this.panel1.Controls.Add(this.txtKingaku4);
            this.panel1.Controls.Add(this.txtRiekikinShobungaku10);
            this.panel1.Controls.Add(this.txtRiekikinShobungaku9);
            this.panel1.Controls.Add(this.txtRiekikinShobungaku8);
            this.panel1.Controls.Add(this.txtRiekikinShobungaku7);
            this.panel1.Controls.Add(this.txtRiekikinShobungaku6);
            this.panel1.Controls.Add(this.txtRiekikinShobungaku5);
            this.panel1.Controls.Add(this.txtRiekikinShobungaku4);
            this.panel1.Controls.Add(this.txtRiekikinShobungaku3);
            this.panel1.Controls.Add(this.txtRiekikinShobungaku2);
            this.panel1.Controls.Add(this.txtRiekikinShobungaku1);
            this.panel1.Controls.Add(this.txtKamokuBunruiNm50);
            this.panel1.Controls.Add(this.txtKamokuBunruiNm40);
            this.panel1.Controls.Add(this.txtKanjoKamokuNm90040_10);
            this.panel1.Controls.Add(this.txtKanjoKamokuNm90040_9);
            this.panel1.Controls.Add(this.txtKanjoKamokuNm90040_8);
            this.panel1.Controls.Add(this.txtKanjoKamokuNm90040_7);
            this.panel1.Controls.Add(this.txtKanjoKamokuNm90040_6);
            this.panel1.Controls.Add(this.txtKanjoKamokuNm90040_5);
            this.panel1.Controls.Add(this.txtKanjoKamokuNm90040_4);
            this.panel1.Controls.Add(this.txtKanjoKamokuNm90040_3);
            this.panel1.Controls.Add(this.txtKanjoKamokuNm90040_2);
            this.panel1.Controls.Add(this.txtKanjoKamokuNm90040_1);
            this.panel1.Location = new System.Drawing.Point(24, 316);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(593, 249);
            this.panel1.TabIndex = 2;
            // 
            // txtKingaku5
            // 
            this.txtKingaku5.AutoSizeFromLength = false;
            this.txtKingaku5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.txtKingaku5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtKingaku5.DisplayLength = null;
            this.txtKingaku5.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKingaku5.ForeColor = System.Drawing.Color.Black;
            this.txtKingaku5.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKingaku5.Location = new System.Drawing.Point(412, 223);
            this.txtKingaku5.MaxLength = 128;
            this.txtKingaku5.Name = "txtKingaku5";
            this.txtKingaku5.ReadOnly = true;
            this.txtKingaku5.Size = new System.Drawing.Size(173, 20);
            this.txtKingaku5.TabIndex = 23;
            this.txtKingaku5.TabStop = false;
            this.txtKingaku5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtKingaku4
            // 
            this.txtKingaku4.AutoSizeFromLength = false;
            this.txtKingaku4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.txtKingaku4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtKingaku4.DisplayLength = null;
            this.txtKingaku4.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKingaku4.ForeColor = System.Drawing.Color.Black;
            this.txtKingaku4.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKingaku4.Location = new System.Drawing.Point(412, 203);
            this.txtKingaku4.MaxLength = 128;
            this.txtKingaku4.Name = "txtKingaku4";
            this.txtKingaku4.ReadOnly = true;
            this.txtKingaku4.Size = new System.Drawing.Size(173, 20);
            this.txtKingaku4.TabIndex = 22;
            this.txtKingaku4.TabStop = false;
            this.txtKingaku4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtRiekikinShobungaku10
            // 
            this.txtRiekikinShobungaku10.AutoSizeFromLength = false;
            this.txtRiekikinShobungaku10.DisplayLength = null;
            this.txtRiekikinShobungaku10.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtRiekikinShobungaku10.ForeColor = System.Drawing.Color.Black;
            this.txtRiekikinShobungaku10.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtRiekikinShobungaku10.Location = new System.Drawing.Point(239, 203);
            this.txtRiekikinShobungaku10.MaxLength = 15;
            this.txtRiekikinShobungaku10.Name = "txtRiekikinShobungaku10";
            this.txtRiekikinShobungaku10.Size = new System.Drawing.Size(173, 20);
            this.txtRiekikinShobungaku10.TabIndex = 20;
            this.txtRiekikinShobungaku10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtRiekikinShobungaku10.Enter += new System.EventHandler(this.txtKingaku_Enter);
            this.txtRiekikinShobungaku10.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
            // 
            // txtRiekikinShobungaku9
            // 
            this.txtRiekikinShobungaku9.AutoSizeFromLength = false;
            this.txtRiekikinShobungaku9.DisplayLength = null;
            this.txtRiekikinShobungaku9.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtRiekikinShobungaku9.ForeColor = System.Drawing.Color.Black;
            this.txtRiekikinShobungaku9.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtRiekikinShobungaku9.Location = new System.Drawing.Point(239, 183);
            this.txtRiekikinShobungaku9.MaxLength = 15;
            this.txtRiekikinShobungaku9.Name = "txtRiekikinShobungaku9";
            this.txtRiekikinShobungaku9.Size = new System.Drawing.Size(173, 20);
            this.txtRiekikinShobungaku9.TabIndex = 18;
            this.txtRiekikinShobungaku9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtRiekikinShobungaku9.Enter += new System.EventHandler(this.txtKingaku_Enter);
            this.txtRiekikinShobungaku9.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
            // 
            // txtRiekikinShobungaku8
            // 
            this.txtRiekikinShobungaku8.AutoSizeFromLength = false;
            this.txtRiekikinShobungaku8.DisplayLength = null;
            this.txtRiekikinShobungaku8.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtRiekikinShobungaku8.ForeColor = System.Drawing.Color.Black;
            this.txtRiekikinShobungaku8.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtRiekikinShobungaku8.Location = new System.Drawing.Point(239, 163);
            this.txtRiekikinShobungaku8.MaxLength = 15;
            this.txtRiekikinShobungaku8.Name = "txtRiekikinShobungaku8";
            this.txtRiekikinShobungaku8.Size = new System.Drawing.Size(173, 20);
            this.txtRiekikinShobungaku8.TabIndex = 16;
            this.txtRiekikinShobungaku8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtRiekikinShobungaku8.Enter += new System.EventHandler(this.txtKingaku_Enter);
            this.txtRiekikinShobungaku8.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
            // 
            // txtRiekikinShobungaku7
            // 
            this.txtRiekikinShobungaku7.AutoSizeFromLength = false;
            this.txtRiekikinShobungaku7.DisplayLength = null;
            this.txtRiekikinShobungaku7.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtRiekikinShobungaku7.ForeColor = System.Drawing.Color.Black;
            this.txtRiekikinShobungaku7.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtRiekikinShobungaku7.Location = new System.Drawing.Point(239, 143);
            this.txtRiekikinShobungaku7.MaxLength = 15;
            this.txtRiekikinShobungaku7.Name = "txtRiekikinShobungaku7";
            this.txtRiekikinShobungaku7.Size = new System.Drawing.Size(173, 20);
            this.txtRiekikinShobungaku7.TabIndex = 14;
            this.txtRiekikinShobungaku7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtRiekikinShobungaku7.Enter += new System.EventHandler(this.txtKingaku_Enter);
            this.txtRiekikinShobungaku7.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
            // 
            // txtRiekikinShobungaku6
            // 
            this.txtRiekikinShobungaku6.AutoSizeFromLength = false;
            this.txtRiekikinShobungaku6.DisplayLength = null;
            this.txtRiekikinShobungaku6.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtRiekikinShobungaku6.ForeColor = System.Drawing.Color.Black;
            this.txtRiekikinShobungaku6.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtRiekikinShobungaku6.Location = new System.Drawing.Point(239, 123);
            this.txtRiekikinShobungaku6.MaxLength = 15;
            this.txtRiekikinShobungaku6.Name = "txtRiekikinShobungaku6";
            this.txtRiekikinShobungaku6.Size = new System.Drawing.Size(173, 20);
            this.txtRiekikinShobungaku6.TabIndex = 12;
            this.txtRiekikinShobungaku6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtRiekikinShobungaku6.Enter += new System.EventHandler(this.txtKingaku_Enter);
            this.txtRiekikinShobungaku6.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
            // 
            // txtRiekikinShobungaku5
            // 
            this.txtRiekikinShobungaku5.AutoSizeFromLength = false;
            this.txtRiekikinShobungaku5.DisplayLength = null;
            this.txtRiekikinShobungaku5.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtRiekikinShobungaku5.ForeColor = System.Drawing.Color.Black;
            this.txtRiekikinShobungaku5.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtRiekikinShobungaku5.Location = new System.Drawing.Point(239, 103);
            this.txtRiekikinShobungaku5.MaxLength = 15;
            this.txtRiekikinShobungaku5.Name = "txtRiekikinShobungaku5";
            this.txtRiekikinShobungaku5.Size = new System.Drawing.Size(173, 20);
            this.txtRiekikinShobungaku5.TabIndex = 10;
            this.txtRiekikinShobungaku5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtRiekikinShobungaku5.Enter += new System.EventHandler(this.txtKingaku_Enter);
            this.txtRiekikinShobungaku5.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
            // 
            // txtRiekikinShobungaku4
            // 
            this.txtRiekikinShobungaku4.AutoSizeFromLength = false;
            this.txtRiekikinShobungaku4.DisplayLength = null;
            this.txtRiekikinShobungaku4.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtRiekikinShobungaku4.ForeColor = System.Drawing.Color.Black;
            this.txtRiekikinShobungaku4.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtRiekikinShobungaku4.Location = new System.Drawing.Point(239, 83);
            this.txtRiekikinShobungaku4.MaxLength = 15;
            this.txtRiekikinShobungaku4.Name = "txtRiekikinShobungaku4";
            this.txtRiekikinShobungaku4.Size = new System.Drawing.Size(173, 20);
            this.txtRiekikinShobungaku4.TabIndex = 8;
            this.txtRiekikinShobungaku4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtRiekikinShobungaku4.Enter += new System.EventHandler(this.txtKingaku_Enter);
            this.txtRiekikinShobungaku4.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
            // 
            // txtRiekikinShobungaku3
            // 
            this.txtRiekikinShobungaku3.AutoSizeFromLength = false;
            this.txtRiekikinShobungaku3.DisplayLength = null;
            this.txtRiekikinShobungaku3.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtRiekikinShobungaku3.ForeColor = System.Drawing.Color.Black;
            this.txtRiekikinShobungaku3.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtRiekikinShobungaku3.Location = new System.Drawing.Point(239, 63);
            this.txtRiekikinShobungaku3.MaxLength = 15;
            this.txtRiekikinShobungaku3.Name = "txtRiekikinShobungaku3";
            this.txtRiekikinShobungaku3.Size = new System.Drawing.Size(173, 20);
            this.txtRiekikinShobungaku3.TabIndex = 6;
            this.txtRiekikinShobungaku3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtRiekikinShobungaku3.Enter += new System.EventHandler(this.txtKingaku_Enter);
            this.txtRiekikinShobungaku3.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
            // 
            // txtRiekikinShobungaku2
            // 
            this.txtRiekikinShobungaku2.AutoSizeFromLength = false;
            this.txtRiekikinShobungaku2.DisplayLength = null;
            this.txtRiekikinShobungaku2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtRiekikinShobungaku2.ForeColor = System.Drawing.Color.Black;
            this.txtRiekikinShobungaku2.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtRiekikinShobungaku2.Location = new System.Drawing.Point(239, 43);
            this.txtRiekikinShobungaku2.MaxLength = 15;
            this.txtRiekikinShobungaku2.Name = "txtRiekikinShobungaku2";
            this.txtRiekikinShobungaku2.Size = new System.Drawing.Size(173, 20);
            this.txtRiekikinShobungaku2.TabIndex = 4;
            this.txtRiekikinShobungaku2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtRiekikinShobungaku2.Enter += new System.EventHandler(this.txtKingaku_Enter);
            this.txtRiekikinShobungaku2.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
            // 
            // txtRiekikinShobungaku1
            // 
            this.txtRiekikinShobungaku1.AutoSizeFromLength = false;
            this.txtRiekikinShobungaku1.DisplayLength = null;
            this.txtRiekikinShobungaku1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtRiekikinShobungaku1.ForeColor = System.Drawing.Color.Black;
            this.txtRiekikinShobungaku1.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtRiekikinShobungaku1.Location = new System.Drawing.Point(239, 23);
            this.txtRiekikinShobungaku1.MaxLength = 15;
            this.txtRiekikinShobungaku1.Name = "txtRiekikinShobungaku1";
            this.txtRiekikinShobungaku1.Size = new System.Drawing.Size(173, 20);
            this.txtRiekikinShobungaku1.TabIndex = 2;
            this.txtRiekikinShobungaku1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtRiekikinShobungaku1.Enter += new System.EventHandler(this.txtKingaku_Enter);
            this.txtRiekikinShobungaku1.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
            // 
            // txtKamokuBunruiNm50
            // 
            this.txtKamokuBunruiNm50.AutoSizeFromLength = false;
            this.txtKamokuBunruiNm50.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txtKamokuBunruiNm50.DisplayLength = null;
            this.txtKamokuBunruiNm50.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKamokuBunruiNm50.ForeColor = System.Drawing.Color.Black;
            this.txtKamokuBunruiNm50.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtKamokuBunruiNm50.Location = new System.Drawing.Point(3, 223);
            this.txtKamokuBunruiNm50.MaxLength = 30;
            this.txtKamokuBunruiNm50.Name = "txtKamokuBunruiNm50";
            this.txtKamokuBunruiNm50.Size = new System.Drawing.Size(236, 20);
            this.txtKamokuBunruiNm50.TabIndex = 21;
            this.txtKamokuBunruiNm50.Validating += new System.ComponentModel.CancelEventHandler(this.txtText_Validating);
            // 
            // txtKamokuBunruiNm40
            // 
            this.txtKamokuBunruiNm40.AutoSizeFromLength = false;
            this.txtKamokuBunruiNm40.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txtKamokuBunruiNm40.DisplayLength = null;
            this.txtKamokuBunruiNm40.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKamokuBunruiNm40.ForeColor = System.Drawing.Color.Black;
            this.txtKamokuBunruiNm40.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtKamokuBunruiNm40.Location = new System.Drawing.Point(3, 3);
            this.txtKamokuBunruiNm40.MaxLength = 30;
            this.txtKamokuBunruiNm40.Name = "txtKamokuBunruiNm40";
            this.txtKamokuBunruiNm40.Size = new System.Drawing.Size(236, 20);
            this.txtKamokuBunruiNm40.TabIndex = 0;
            this.txtKamokuBunruiNm40.Validating += new System.ComponentModel.CancelEventHandler(this.txtText_Validating);
            // 
            // txtKanjoKamokuNm90040_10
            // 
            this.txtKanjoKamokuNm90040_10.AutoSizeFromLength = false;
            this.txtKanjoKamokuNm90040_10.DisplayLength = null;
            this.txtKanjoKamokuNm90040_10.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKanjoKamokuNm90040_10.ForeColor = System.Drawing.Color.Black;
            this.txtKanjoKamokuNm90040_10.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtKanjoKamokuNm90040_10.Location = new System.Drawing.Point(3, 203);
            this.txtKanjoKamokuNm90040_10.MaxLength = 30;
            this.txtKanjoKamokuNm90040_10.Name = "txtKanjoKamokuNm90040_10";
            this.txtKanjoKamokuNm90040_10.Size = new System.Drawing.Size(236, 20);
            this.txtKanjoKamokuNm90040_10.TabIndex = 19;
            this.txtKanjoKamokuNm90040_10.Validating += new System.ComponentModel.CancelEventHandler(this.txtText_Validating);
            // 
            // txtKanjoKamokuNm90040_9
            // 
            this.txtKanjoKamokuNm90040_9.AutoSizeFromLength = false;
            this.txtKanjoKamokuNm90040_9.DisplayLength = null;
            this.txtKanjoKamokuNm90040_9.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKanjoKamokuNm90040_9.ForeColor = System.Drawing.Color.Black;
            this.txtKanjoKamokuNm90040_9.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtKanjoKamokuNm90040_9.Location = new System.Drawing.Point(3, 183);
            this.txtKanjoKamokuNm90040_9.MaxLength = 30;
            this.txtKanjoKamokuNm90040_9.Name = "txtKanjoKamokuNm90040_9";
            this.txtKanjoKamokuNm90040_9.Size = new System.Drawing.Size(236, 20);
            this.txtKanjoKamokuNm90040_9.TabIndex = 17;
            this.txtKanjoKamokuNm90040_9.Validating += new System.ComponentModel.CancelEventHandler(this.txtText_Validating);
            // 
            // txtKanjoKamokuNm90040_8
            // 
            this.txtKanjoKamokuNm90040_8.AutoSizeFromLength = false;
            this.txtKanjoKamokuNm90040_8.DisplayLength = null;
            this.txtKanjoKamokuNm90040_8.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKanjoKamokuNm90040_8.ForeColor = System.Drawing.Color.Black;
            this.txtKanjoKamokuNm90040_8.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtKanjoKamokuNm90040_8.Location = new System.Drawing.Point(3, 163);
            this.txtKanjoKamokuNm90040_8.MaxLength = 30;
            this.txtKanjoKamokuNm90040_8.Name = "txtKanjoKamokuNm90040_8";
            this.txtKanjoKamokuNm90040_8.Size = new System.Drawing.Size(236, 20);
            this.txtKanjoKamokuNm90040_8.TabIndex = 15;
            this.txtKanjoKamokuNm90040_8.Validating += new System.ComponentModel.CancelEventHandler(this.txtText_Validating);
            // 
            // txtKanjoKamokuNm90040_7
            // 
            this.txtKanjoKamokuNm90040_7.AutoSizeFromLength = false;
            this.txtKanjoKamokuNm90040_7.DisplayLength = null;
            this.txtKanjoKamokuNm90040_7.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKanjoKamokuNm90040_7.ForeColor = System.Drawing.Color.Black;
            this.txtKanjoKamokuNm90040_7.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtKanjoKamokuNm90040_7.Location = new System.Drawing.Point(3, 143);
            this.txtKanjoKamokuNm90040_7.MaxLength = 30;
            this.txtKanjoKamokuNm90040_7.Name = "txtKanjoKamokuNm90040_7";
            this.txtKanjoKamokuNm90040_7.Size = new System.Drawing.Size(236, 20);
            this.txtKanjoKamokuNm90040_7.TabIndex = 13;
            this.txtKanjoKamokuNm90040_7.Validating += new System.ComponentModel.CancelEventHandler(this.txtText_Validating);
            // 
            // txtKanjoKamokuNm90040_6
            // 
            this.txtKanjoKamokuNm90040_6.AutoSizeFromLength = false;
            this.txtKanjoKamokuNm90040_6.DisplayLength = null;
            this.txtKanjoKamokuNm90040_6.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKanjoKamokuNm90040_6.ForeColor = System.Drawing.Color.Black;
            this.txtKanjoKamokuNm90040_6.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtKanjoKamokuNm90040_6.Location = new System.Drawing.Point(3, 123);
            this.txtKanjoKamokuNm90040_6.MaxLength = 30;
            this.txtKanjoKamokuNm90040_6.Name = "txtKanjoKamokuNm90040_6";
            this.txtKanjoKamokuNm90040_6.Size = new System.Drawing.Size(236, 20);
            this.txtKanjoKamokuNm90040_6.TabIndex = 11;
            this.txtKanjoKamokuNm90040_6.Validating += new System.ComponentModel.CancelEventHandler(this.txtText_Validating);
            // 
            // txtKanjoKamokuNm90040_5
            // 
            this.txtKanjoKamokuNm90040_5.AutoSizeFromLength = false;
            this.txtKanjoKamokuNm90040_5.DisplayLength = null;
            this.txtKanjoKamokuNm90040_5.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKanjoKamokuNm90040_5.ForeColor = System.Drawing.Color.Black;
            this.txtKanjoKamokuNm90040_5.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtKanjoKamokuNm90040_5.Location = new System.Drawing.Point(3, 103);
            this.txtKanjoKamokuNm90040_5.MaxLength = 30;
            this.txtKanjoKamokuNm90040_5.Name = "txtKanjoKamokuNm90040_5";
            this.txtKanjoKamokuNm90040_5.Size = new System.Drawing.Size(236, 20);
            this.txtKanjoKamokuNm90040_5.TabIndex = 9;
            this.txtKanjoKamokuNm90040_5.Validating += new System.ComponentModel.CancelEventHandler(this.txtText_Validating);
            // 
            // txtKanjoKamokuNm90040_4
            // 
            this.txtKanjoKamokuNm90040_4.AutoSizeFromLength = false;
            this.txtKanjoKamokuNm90040_4.DisplayLength = null;
            this.txtKanjoKamokuNm90040_4.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKanjoKamokuNm90040_4.ForeColor = System.Drawing.Color.Black;
            this.txtKanjoKamokuNm90040_4.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtKanjoKamokuNm90040_4.Location = new System.Drawing.Point(3, 83);
            this.txtKanjoKamokuNm90040_4.MaxLength = 30;
            this.txtKanjoKamokuNm90040_4.Name = "txtKanjoKamokuNm90040_4";
            this.txtKanjoKamokuNm90040_4.Size = new System.Drawing.Size(236, 20);
            this.txtKanjoKamokuNm90040_4.TabIndex = 7;
            this.txtKanjoKamokuNm90040_4.Validating += new System.ComponentModel.CancelEventHandler(this.txtText_Validating);
            // 
            // txtKanjoKamokuNm90040_3
            // 
            this.txtKanjoKamokuNm90040_3.AutoSizeFromLength = false;
            this.txtKanjoKamokuNm90040_3.DisplayLength = null;
            this.txtKanjoKamokuNm90040_3.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKanjoKamokuNm90040_3.ForeColor = System.Drawing.Color.Black;
            this.txtKanjoKamokuNm90040_3.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtKanjoKamokuNm90040_3.Location = new System.Drawing.Point(3, 63);
            this.txtKanjoKamokuNm90040_3.MaxLength = 30;
            this.txtKanjoKamokuNm90040_3.Name = "txtKanjoKamokuNm90040_3";
            this.txtKanjoKamokuNm90040_3.Size = new System.Drawing.Size(236, 20);
            this.txtKanjoKamokuNm90040_3.TabIndex = 5;
            this.txtKanjoKamokuNm90040_3.Validating += new System.ComponentModel.CancelEventHandler(this.txtText_Validating);
            // 
            // txtKanjoKamokuNm90040_2
            // 
            this.txtKanjoKamokuNm90040_2.AutoSizeFromLength = false;
            this.txtKanjoKamokuNm90040_2.DisplayLength = null;
            this.txtKanjoKamokuNm90040_2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKanjoKamokuNm90040_2.ForeColor = System.Drawing.Color.Black;
            this.txtKanjoKamokuNm90040_2.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtKanjoKamokuNm90040_2.Location = new System.Drawing.Point(3, 43);
            this.txtKanjoKamokuNm90040_2.MaxLength = 30;
            this.txtKanjoKamokuNm90040_2.Name = "txtKanjoKamokuNm90040_2";
            this.txtKanjoKamokuNm90040_2.Size = new System.Drawing.Size(236, 20);
            this.txtKanjoKamokuNm90040_2.TabIndex = 3;
            this.txtKanjoKamokuNm90040_2.Validating += new System.ComponentModel.CancelEventHandler(this.txtText_Validating);
            // 
            // txtKanjoKamokuNm90040_1
            // 
            this.txtKanjoKamokuNm90040_1.AutoSizeFromLength = false;
            this.txtKanjoKamokuNm90040_1.DisplayLength = null;
            this.txtKanjoKamokuNm90040_1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKanjoKamokuNm90040_1.ForeColor = System.Drawing.Color.Black;
            this.txtKanjoKamokuNm90040_1.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtKanjoKamokuNm90040_1.Location = new System.Drawing.Point(3, 23);
            this.txtKanjoKamokuNm90040_1.MaxLength = 30;
            this.txtKanjoKamokuNm90040_1.Name = "txtKanjoKamokuNm90040_1";
            this.txtKanjoKamokuNm90040_1.Size = new System.Drawing.Size(236, 20);
            this.txtKanjoKamokuNm90040_1.TabIndex = 1;
            this.txtKanjoKamokuNm90040_1.Validating += new System.ComponentModel.CancelEventHandler(this.txtText_Validating);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.DarkGray;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Location = new System.Drawing.Point(24, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(241, 20);
            this.label1.TabIndex = 902;
            this.label1.Text = "科　　　　　目";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.DarkGray;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Location = new System.Drawing.Point(265, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(352, 20);
            this.label2.TabIndex = 903;
            this.label2.Text = "金　　　　　額";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ZMYR1015
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(630, 624);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pnl00);
            this.Controls.Add(this.pnl10);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "ZMYR1015";
            this.ShowFButton = true;
            this.Text = "";
            this.Controls.SetChildIndex(this.pnl10, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.pnl00, 0);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.pnlDebug.ResumeLayout(false);
            this.pnl10.ResumeLayout(false);
            this.pnl10.PerformLayout();
            this.pnl00.ResumeLayout(false);
            this.pnl00.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private jp.co.fsi.common.FsiPanel pnl10;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuNm90020_10;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuNm90020_9;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuNm90020_8;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuNm90020_7;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuNm90020_6;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuNm90020_5;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuNm90020_4;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuNm90020_3;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuNm90020_2;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuNm90020_1;
        private jp.co.fsi.common.controls.FsiTextBox txtKingaku3;
        private jp.co.fsi.common.controls.FsiTextBox txtKingaku2;
        private jp.co.fsi.common.controls.FsiTextBox txtNiniTmttknTorikuzushigaku10;
        private jp.co.fsi.common.controls.FsiTextBox txtNiniTmttknTorikuzushigaku9;
        private jp.co.fsi.common.controls.FsiTextBox txtNiniTmttknTorikuzushigaku8;
        private jp.co.fsi.common.controls.FsiTextBox txtNiniTmttknTorikuzushigaku7;
        private jp.co.fsi.common.controls.FsiTextBox txtNiniTmttknTorikuzushigaku6;
        private jp.co.fsi.common.controls.FsiTextBox txtNiniTmttknTorikuzushigaku5;
        private jp.co.fsi.common.controls.FsiTextBox txtNiniTmttknTorikuzushigaku4;
        private jp.co.fsi.common.controls.FsiTextBox txtNiniTmttknTorikuzushigaku3;
        private jp.co.fsi.common.controls.FsiTextBox txtNiniTmttknTorikuzushigaku2;
        private jp.co.fsi.common.controls.FsiTextBox txtNiniTmttknTorikuzushigaku1;
        private jp.co.fsi.common.controls.FsiTextBox txtKamokuBunruiNm30;
        private jp.co.fsi.common.controls.FsiTextBox txtKamokuBunruiNm20;
        private jp.co.fsi.common.FsiPanel pnl00;
        private jp.co.fsi.common.controls.FsiTextBox txtKingaku1;
        private jp.co.fsi.common.controls.FsiTextBox txtKamokuBunruiNm10;
        private jp.co.fsi.common.FsiPanel panel1;
        private jp.co.fsi.common.controls.FsiTextBox txtKingaku5;
        private jp.co.fsi.common.controls.FsiTextBox txtKingaku4;
        private jp.co.fsi.common.controls.FsiTextBox txtRiekikinShobungaku10;
        private jp.co.fsi.common.controls.FsiTextBox txtRiekikinShobungaku9;
        private jp.co.fsi.common.controls.FsiTextBox txtRiekikinShobungaku8;
        private jp.co.fsi.common.controls.FsiTextBox txtRiekikinShobungaku7;
        private jp.co.fsi.common.controls.FsiTextBox txtRiekikinShobungaku6;
        private jp.co.fsi.common.controls.FsiTextBox txtRiekikinShobungaku5;
        private jp.co.fsi.common.controls.FsiTextBox txtRiekikinShobungaku4;
        private jp.co.fsi.common.controls.FsiTextBox txtRiekikinShobungaku3;
        private jp.co.fsi.common.controls.FsiTextBox txtRiekikinShobungaku2;
        private jp.co.fsi.common.controls.FsiTextBox txtRiekikinShobungaku1;
        private jp.co.fsi.common.controls.FsiTextBox txtKamokuBunruiNm50;
        private jp.co.fsi.common.controls.FsiTextBox txtKamokuBunruiNm40;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuNm90040_10;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuNm90040_9;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuNm90040_8;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuNm90040_7;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuNm90040_6;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuNm90040_5;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuNm90040_4;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuNm90040_3;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuNm90040_2;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuNm90040_1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;





    }
}