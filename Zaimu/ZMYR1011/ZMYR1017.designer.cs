﻿namespace jp.co.fsi.zm.zmyr1011
{
    partial class ZMYR1017
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlTitle = new jp.co.fsi.common.FsiPanel();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtRiekiShobunanTitle = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtHanbaihiMeisaihyoTitle = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtSeizoGenkaTitle = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtSonekiKeisanshoTitle = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtTaishakuTaishohyoTitle = new jp.co.fsi.common.controls.FsiTextBox();
            this.pnlMinus = new jp.co.fsi.common.FsiPanel();
            this.rdoMinusKingaku1 = new System.Windows.Forms.RadioButton();
            this.rdoMinusKingaku0 = new System.Windows.Forms.RadioButton();
            this.label12 = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.pnlTitle.SuspendLayout();
            this.pnlMinus.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.BackColor = System.Drawing.Color.White;
            this.lblTitle.Location = new System.Drawing.Point(4, 9);
            this.lblTitle.Size = new System.Drawing.Size(463, 23);
            this.lblTitle.Text = "";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(5, 192);
            this.pnlDebug.Size = new System.Drawing.Size(480, 100);
            // 
            // pnlTitle
            // 
            this.pnlTitle.BackColor = System.Drawing.Color.Transparent;
            this.pnlTitle.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlTitle.Controls.Add(this.label6);
            this.pnlTitle.Controls.Add(this.label5);
            this.pnlTitle.Controls.Add(this.label4);
            this.pnlTitle.Controls.Add(this.label3);
            this.pnlTitle.Controls.Add(this.label2);
            this.pnlTitle.Controls.Add(this.label1);
            this.pnlTitle.Controls.Add(this.txtRiekiShobunanTitle);
            this.pnlTitle.Controls.Add(this.txtHanbaihiMeisaihyoTitle);
            this.pnlTitle.Controls.Add(this.txtSeizoGenkaTitle);
            this.pnlTitle.Controls.Add(this.txtSonekiKeisanshoTitle);
            this.pnlTitle.Controls.Add(this.txtTaishakuTaishohyoTitle);
            this.pnlTitle.Location = new System.Drawing.Point(12, 23);
            this.pnlTitle.Name = "pnlTitle";
            this.pnlTitle.Size = new System.Drawing.Size(450, 151);
            this.pnlTitle.TabIndex = 903;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.Silver;
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label6.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label6.Location = new System.Drawing.Point(24, 106);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(189, 20);
            this.label6.TabIndex = 11;
            this.label6.Text = "利益処分案";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Silver;
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label5.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label5.Location = new System.Drawing.Point(24, 86);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(189, 20);
            this.label5.TabIndex = 10;
            this.label5.Text = "販売費及び一般管理費";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Silver;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.Location = new System.Drawing.Point(24, 66);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(189, 20);
            this.label4.TabIndex = 9;
            this.label4.Text = "製造原価報告書";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Silver;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(24, 46);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(189, 20);
            this.label3.TabIndex = 8;
            this.label3.Text = "損益計算書";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Silver;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(24, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(189, 20);
            this.label2.TabIndex = 7;
            this.label2.Text = "貸借対照表";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(4, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "■帳票タイトル";
            // 
            // txtRiekiShobunanTitle
            // 
            this.txtRiekiShobunanTitle.AutoSizeFromLength = false;
            this.txtRiekiShobunanTitle.DisplayLength = null;
            this.txtRiekiShobunanTitle.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtRiekiShobunanTitle.ForeColor = System.Drawing.Color.Black;
            this.txtRiekiShobunanTitle.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtRiekiShobunanTitle.Location = new System.Drawing.Point(213, 106);
            this.txtRiekiShobunanTitle.MaxLength = 64;
            this.txtRiekiShobunanTitle.Name = "txtRiekiShobunanTitle";
            this.txtRiekiShobunanTitle.Size = new System.Drawing.Size(212, 20);
            this.txtRiekiShobunanTitle.TabIndex = 5;
            this.txtRiekiShobunanTitle.Validating += new System.ComponentModel.CancelEventHandler(this.txtTitle_Validating);
            // 
            // txtHanbaihiMeisaihyoTitle
            // 
            this.txtHanbaihiMeisaihyoTitle.AutoSizeFromLength = false;
            this.txtHanbaihiMeisaihyoTitle.DisplayLength = null;
            this.txtHanbaihiMeisaihyoTitle.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtHanbaihiMeisaihyoTitle.ForeColor = System.Drawing.Color.Black;
            this.txtHanbaihiMeisaihyoTitle.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtHanbaihiMeisaihyoTitle.Location = new System.Drawing.Point(213, 86);
            this.txtHanbaihiMeisaihyoTitle.MaxLength = 64;
            this.txtHanbaihiMeisaihyoTitle.Name = "txtHanbaihiMeisaihyoTitle";
            this.txtHanbaihiMeisaihyoTitle.Size = new System.Drawing.Size(212, 20);
            this.txtHanbaihiMeisaihyoTitle.TabIndex = 4;
            this.txtHanbaihiMeisaihyoTitle.Validating += new System.ComponentModel.CancelEventHandler(this.txtTitle_Validating);
            // 
            // txtSeizoGenkaTitle
            // 
            this.txtSeizoGenkaTitle.AutoSizeFromLength = false;
            this.txtSeizoGenkaTitle.DisplayLength = null;
            this.txtSeizoGenkaTitle.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSeizoGenkaTitle.ForeColor = System.Drawing.Color.Black;
            this.txtSeizoGenkaTitle.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtSeizoGenkaTitle.Location = new System.Drawing.Point(213, 66);
            this.txtSeizoGenkaTitle.MaxLength = 64;
            this.txtSeizoGenkaTitle.Name = "txtSeizoGenkaTitle";
            this.txtSeizoGenkaTitle.Size = new System.Drawing.Size(212, 20);
            this.txtSeizoGenkaTitle.TabIndex = 3;
            this.txtSeizoGenkaTitle.Validating += new System.ComponentModel.CancelEventHandler(this.txtTitle_Validating);
            // 
            // txtSonekiKeisanshoTitle
            // 
            this.txtSonekiKeisanshoTitle.AutoSizeFromLength = false;
            this.txtSonekiKeisanshoTitle.DisplayLength = null;
            this.txtSonekiKeisanshoTitle.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSonekiKeisanshoTitle.ForeColor = System.Drawing.Color.Black;
            this.txtSonekiKeisanshoTitle.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtSonekiKeisanshoTitle.Location = new System.Drawing.Point(213, 46);
            this.txtSonekiKeisanshoTitle.MaxLength = 64;
            this.txtSonekiKeisanshoTitle.Name = "txtSonekiKeisanshoTitle";
            this.txtSonekiKeisanshoTitle.Size = new System.Drawing.Size(212, 20);
            this.txtSonekiKeisanshoTitle.TabIndex = 2;
            this.txtSonekiKeisanshoTitle.Validating += new System.ComponentModel.CancelEventHandler(this.txtTitle_Validating);
            // 
            // txtTaishakuTaishohyoTitle
            // 
            this.txtTaishakuTaishohyoTitle.AutoSizeFromLength = false;
            this.txtTaishakuTaishohyoTitle.DisplayLength = null;
            this.txtTaishakuTaishohyoTitle.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTaishakuTaishohyoTitle.ForeColor = System.Drawing.Color.Black;
            this.txtTaishakuTaishohyoTitle.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtTaishakuTaishohyoTitle.Location = new System.Drawing.Point(213, 26);
            this.txtTaishakuTaishohyoTitle.MaxLength = 64;
            this.txtTaishakuTaishohyoTitle.Name = "txtTaishakuTaishohyoTitle";
            this.txtTaishakuTaishohyoTitle.Size = new System.Drawing.Size(212, 20);
            this.txtTaishakuTaishohyoTitle.TabIndex = 1;
            this.txtTaishakuTaishohyoTitle.Validating += new System.ComponentModel.CancelEventHandler(this.txtTitle_Validating);
            // 
            // pnlMinus
            // 
            this.pnlMinus.BackColor = System.Drawing.Color.Transparent;
            this.pnlMinus.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlMinus.Controls.Add(this.rdoMinusKingaku1);
            this.pnlMinus.Controls.Add(this.rdoMinusKingaku0);
            this.pnlMinus.Controls.Add(this.label12);
            this.pnlMinus.Location = new System.Drawing.Point(12, 180);
            this.pnlMinus.Name = "pnlMinus";
            this.pnlMinus.Size = new System.Drawing.Size(450, 52);
            this.pnlMinus.TabIndex = 904;
            // 
            // rdoMinusKingaku1
            // 
            this.rdoMinusKingaku1.AutoSize = true;
            this.rdoMinusKingaku1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoMinusKingaku1.Location = new System.Drawing.Point(213, 20);
            this.rdoMinusKingaku1.Name = "rdoMinusKingaku1";
            this.rdoMinusKingaku1.Size = new System.Drawing.Size(116, 17);
            this.rdoMinusKingaku1.TabIndex = 8;
            this.rdoMinusKingaku1.TabStop = true;
            this.rdoMinusKingaku1.Text = "△999,999,999";
            this.rdoMinusKingaku1.UseVisualStyleBackColor = true;
            // 
            // rdoMinusKingaku0
            // 
            this.rdoMinusKingaku0.AutoSize = true;
            this.rdoMinusKingaku0.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoMinusKingaku0.Location = new System.Drawing.Point(24, 20);
            this.rdoMinusKingaku0.Name = "rdoMinusKingaku0";
            this.rdoMinusKingaku0.Size = new System.Drawing.Size(109, 17);
            this.rdoMinusKingaku0.TabIndex = 7;
            this.rdoMinusKingaku0.TabStop = true;
            this.rdoMinusKingaku0.Text = "-999,999,999";
            this.rdoMinusKingaku0.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label12.Location = new System.Drawing.Point(4, 4);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(133, 13);
            this.label12.TabIndex = 6;
            this.label12.Text = "■マイナス金額表示";
            // 
            // ZMYR1017
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(472, 295);
            this.Controls.Add(this.pnlMinus);
            this.Controls.Add(this.pnlTitle);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "ZMYR1017";
            this.ShowFButton = true;
            this.Text = "";
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.pnlTitle, 0);
            this.Controls.SetChildIndex(this.pnlMinus, 0);
            this.pnlDebug.ResumeLayout(false);
            this.pnlTitle.ResumeLayout(false);
            this.pnlTitle.PerformLayout();
            this.pnlMinus.ResumeLayout(false);
            this.pnlMinus.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private jp.co.fsi.common.FsiPanel pnlTitle;
        private jp.co.fsi.common.controls.FsiTextBox txtRiekiShobunanTitle;
        private jp.co.fsi.common.controls.FsiTextBox txtHanbaihiMeisaihyoTitle;
        private jp.co.fsi.common.controls.FsiTextBox txtSeizoGenkaTitle;
        private jp.co.fsi.common.controls.FsiTextBox txtSonekiKeisanshoTitle;
        private jp.co.fsi.common.controls.FsiTextBox txtTaishakuTaishohyoTitle;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private jp.co.fsi.common.FsiPanel pnlMinus;
        private System.Windows.Forms.RadioButton rdoMinusKingaku1;
        private System.Windows.Forms.RadioButton rdoMinusKingaku0;
        private System.Windows.Forms.Label label12;





    }
}