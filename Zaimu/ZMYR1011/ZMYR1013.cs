﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

using systembase.table;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.constants;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.userinfo;
using jp.co.fsi.common.util;

namespace jp.co.fsi.zm.zmyr1011
{
    /// <summary>
    /// 決算報告書[要約設定](ZMYR1013)
    /// </summary>
    public partial class ZMYR1013 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// タブコントロールindex
        /// </summary>
        private const int LIST_F1 = 1;
        private const int LIST_F2 = 2;
        private const int LIST_F3 = 3;
        #endregion

        #region プロパティ
        /// <summary>
        /// F1対象勘定科目ListBox用データテーブル
        /// </summary>
        private DataTable _dtTaishoKamokuF1 = new DataTable();
        public DataTable TaishoKamokuF1
        {
            get
            {
                return this._dtTaishoKamokuF1;
            }
        }

        /// <summary>
        /// F1勘定科目一覧ListBox用データテーブル
        /// </summary>
        private DataTable _dtKamokuIchiranF1 = new DataTable();
        public DataTable KamokuIchiranF1
        {
            get
            {
                return this._dtKamokuIchiranF1;
            }
        }

        /// <summary>
        /// F2対象勘定科目ListBox用データテーブル
        /// </summary>
        private DataTable _dtTaishoKamokuF2 = new DataTable();
        public DataTable TaishoKamokuF2
        {
            get
            {
                return this._dtTaishoKamokuF2;
            }
        }

        /// <summary>
        /// F2勘定科目一覧ListBox用データテーブル
        /// </summary>
        private DataTable _dtKamokuIchiranF2 = new DataTable();
        public DataTable KamokuIchiranF2
        {
            get
            {
                return this._dtKamokuIchiranF2;
            }
        }

        /// <summary>
        /// F3対象勘定科目ListBox用データテーブル
        /// </summary>
        private DataTable _dtTaishoKamokuF3 = new DataTable();
        public DataTable TaishoKamokuF3
        {
            get
            {
                return this._dtTaishoKamokuF3;
            }
        }

        /// <summary>
        /// F3勘定科目一覧ListBox用データテーブル
        /// </summary>
        private DataTable _dtKamokuIchiranF3 = new DataTable();
        public DataTable KamokuIchiranF3
        {
            get
            {
                return this._dtKamokuIchiranF3;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ZMYR1013(ZMYR1011 frm)
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            InitializeComponent();

            // タイトル非表示
            this.lblTitle.Visible = false;

            // 画面の初期表示
            InitDetailArea();
            yoyakuSetteiArea();

            // フォーカス設定
            this.mtbListF1.Focus();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        public override void PressF1()
        {
            tabControl.SelectedTab = F1;
        }

        /// <summary>
        /// F2キー押下時処理
        /// </summary>
        public override void PressF2()
        {
            tabControl.SelectedTab = F2;
        }

        /// <summary>
        /// F3キー押下時処理
        /// </summary>
        public override void PressF3()
        {
            tabControl.SelectedTab = F3;
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        public override void PressF6()
        {
            // 確認メッセージを表示
            if (Msg.ConfYesNo("保存しますか？") == System.Windows.Forms.DialogResult.No)
            {
                return;
            }

            // データ保存処理
            gridDataInsert(LIST_F1);
            gridDataInsert(LIST_F2);
            gridDataInsert(LIST_F3);

            // 画面を閉じる
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// F7キー押下時処理
        /// </summary>
        public override void PressF7()
        {
            gridViewRowAdd();
        }

        /// <summary>
        /// F8キー押下時処理
        /// </summary>
        public override void PressF8()
        {
            gridViewRowDelete();
        }
        #endregion

        #region イベント
        /// <summary>
        /// GridViewセル選択時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int visibleSettei;
            string taishaku;
            string kakkoKubunNm;
            int kamokuCd;
            int gyoBango;
            int kakkoKubun;
            int kakkoHyoji;
            int mojiShubetsu;
            string joken;
            DataRow[] foundRows;
            int i = 0;

            // タブコントロール選択情報を取得
            // 可視判断用データを取得
            // 科目名を取得、設定
            // 貸借区分データを取得
            // 文字スタイルデータを取得
            // 科目コードを取得
            // 行番号を取得
            // 条件設定し、それを元に対象勘定科目テーブルからデータを取得
            if (tabControl.SelectedIndex == 0)
            {
                visibleSettei = Util.ToInt(this.mtbListF1[0, e.RowIndex].Value);
                this.txtKamokuNm.Text = Util.ToString(this.mtbListF1[2, e.RowIndex].Value);
                taishaku = Util.ToString(this.mtbListF1[4, e.RowIndex].Value);
                kakkoKubunNm = GetKakkoKubunNm(this.mtbListF1[8, e.RowIndex].Value);
                kamokuCd = Util.ToInt(this.mtbListF1[1, e.RowIndex].Value);
                gyoBango = Util.ToInt(this.mtbListF1[3, e.RowIndex].Value);
                kakkoKubun = Util.ToInt(Util.ToString(this.mtbListF1[8, e.RowIndex].Value));
                kakkoHyoji = Util.ToInt(Util.ToString(this.mtbListF1[9, e.RowIndex].Value));
                mojiShubetsu = Util.ToInt(Util.ToString(this.mtbListF1[10, e.RowIndex].Value));
                joken = "kamokuNo = " + kamokuCd + " AND gyoNo = " + gyoBango;
                foundRows = TaishoKamokuF1.Select(joken);
            }
            else if (tabControl.SelectedIndex == 1)
            {
                visibleSettei = Util.ToInt(this.mtbListF2[0, e.RowIndex].Value);
                this.txtKamokuNm.Text = Util.ToString(this.mtbListF2[2, e.RowIndex].Value);
                taishaku = Util.ToString(this.mtbListF2[4, e.RowIndex].Value);
                kakkoKubunNm = GetKakkoKubunNm(this.mtbListF2[8, e.RowIndex].Value);
                kamokuCd = Util.ToInt(this.mtbListF2[1, e.RowIndex].Value);
                gyoBango = Util.ToInt(this.mtbListF2[3, e.RowIndex].Value);
                kakkoKubun = Util.ToInt(Util.ToString(this.mtbListF2[8, e.RowIndex].Value));
                kakkoHyoji = Util.ToInt(Util.ToString(this.mtbListF2[9, e.RowIndex].Value));
                mojiShubetsu = Util.ToInt(Util.ToString(this.mtbListF2[10, e.RowIndex].Value));
                joken = "kamokuNo = " + kamokuCd + " AND gyoNo = " + gyoBango;
                foundRows = TaishoKamokuF2.Select(joken);
            }
            else
            {
                visibleSettei = Util.ToInt(this.mtbListF3[0, e.RowIndex].Value);
                this.txtKamokuNm.Text = Util.ToString(this.mtbListF3[2, e.RowIndex].Value);
                taishaku = Util.ToString(this.mtbListF3[4, e.RowIndex].Value);
                kakkoKubunNm = GetKakkoKubunNm(this.mtbListF3[8, e.RowIndex].Value);
                kamokuCd = Util.ToInt(this.mtbListF3[1, e.RowIndex].Value);
                gyoBango = Util.ToInt(this.mtbListF3[3, e.RowIndex].Value);
                kakkoKubun = Util.ToInt(Util.ToString(this.mtbListF3[8, e.RowIndex].Value));
                kakkoHyoji = Util.ToInt(Util.ToString(this.mtbListF3[9, e.RowIndex].Value));
                mojiShubetsu = Util.ToInt(Util.ToString(this.mtbListF3[10, e.RowIndex].Value));
                joken = "kamokuNo = " + kamokuCd + " AND gyoNo = " + gyoBango;
                foundRows = TaishoKamokuF3.Select(joken);
            }

            // 可視判断用データを設定
            if (visibleSettei == 0)
            {
                this.gbxKanjoKamoku.Visible = false;
                this.gbxMoji.Visible = true;
            }
            else
            {
                this.gbxKanjoKamoku.Visible = true;
                this.gbxMoji.Visible = false;
                this.lbxTaishoKanjoKamoku.Items.Clear();
            }

            // 貸借区分データを設定
            if (taishaku == "借方")
            {
                this.rdoKari.Checked = true;
            }
            else if (taishaku == "貸方")
            {
                this.rdoKashi.Checked = true;
            }
            else
            {
                this.rdoKari.Checked = false;
                this.rdoKashi.Checked = false;
            }

            // 括弧区分を設定
            this.txtKakkoKubun.Text = Util.ToString(kakkoKubun);

            // 括弧表示を設定
            this.txtKakkoHyoji.Text = Util.ToString(kakkoHyoji);

            // 文字スタイルデータを設定
            this.txtMojiShubetsu.Text = Util.ToString(mojiShubetsu);

            // 対象勘定科目テーブルから取得したデータを表示ListBoxに
            while (foundRows.Length > i)
            {
                this.lbxTaishoKanjoKamoku.Items.Add(foundRows[i].ItemArray[2] + "　" + foundRows[i].ItemArray[3]);
                i++;
            }
        }

        /// <summary>
        /// 選択した項目の移動処理(上→下)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDMove_Click(object sender, EventArgs e)
        {
            string taishoData;
            string[] stArrayData;
            DataRow rowDMove;
            string joken;
            DataRow[] foundRows;
            int[] selectNum = new int[lbxTaishoKanjoKamoku.SelectedIndices.Count];

            for (int i = 0; i < lbxTaishoKanjoKamoku.SelectedIndices.Count; i++)
            {
                selectNum[i] = lbxTaishoKanjoKamoku.SelectedIndices[i];
            }

            // 選択した項目の追加
            for (int i = 0; i < selectNum.Length; i++)
            {
                this.lbxKanjoKamokuIchiran.Items.Add(this.lbxTaishoKanjoKamoku.Items[selectNum[i]]);
            }

            // 選択した項目の削除
            for (int i = 0; i < selectNum.Length; i++)
            {
                taishoData = this.lbxTaishoKanjoKamoku.Items[selectNum[0]].ToString();
                stArrayData = taishoData.Split('　');

                // KamokuIchiranデータテーブルに登録
                // TaishoKamokuデータテーブルから削除
                if (tabControl.SelectedIndex == 0)
                {
                    joken = "taishoNo = " + stArrayData[0];
                    foundRows = TaishoKamokuF1.Select(joken);

                    foreach (DataGridViewRow r in mtbListF1.SelectedRows)
                    {
                        rowDMove = KamokuIchiranF1.NewRow();
                        rowDMove["kamokuNo"] = Util.ToInt(this.mtbListF1[1, r.Index].Value);
                        rowDMove["kamokuNm"] = this.mtbListF1[2, r.Index].Value;
                        rowDMove["taishoNo"] = Util.ToInt(stArrayData[0]);
                        rowDMove["taishoNm"] = stArrayData[1];
                        rowDMove["gyoNo"] = Util.ToInt(this.mtbListF1[3, r.Index].Value);
                        rowDMove["taishoTaishaku"] = Util.ToInt(foundRows[0].ItemArray[4]);
                        KamokuIchiranF1.Rows.Add(rowDMove);
                    }

                    Array.ForEach<DataRow>(foundRows, row => TaishoKamokuF1.Rows.Remove(row));
                }
                else if (tabControl.SelectedIndex == 1)
                {
                    joken = "taishoNo = " + stArrayData[0];
                    foundRows = TaishoKamokuF2.Select(joken);

                    foreach (DataGridViewRow r in mtbListF2.SelectedRows)
                    {
                        rowDMove = KamokuIchiranF2.NewRow();
                        rowDMove["kamokuNo"] = Util.ToInt(this.mtbListF2[1, r.Index].Value);
                        rowDMove["kamokuNm"] = this.mtbListF2[2, r.Index].Value;
                        rowDMove["taishoNo"] = Util.ToInt(stArrayData[0]);
                        rowDMove["taishoNm"] = stArrayData[1];
                        rowDMove["gyoNo"] = Util.ToInt(this.mtbListF2[3, r.Index].Value);
                        rowDMove["taishoTaishaku"] = Util.ToInt(foundRows[0].ItemArray[4]);
                        KamokuIchiranF2.Rows.Add(rowDMove);
                    }

                    Array.ForEach<DataRow>(foundRows, row => TaishoKamokuF2.Rows.Remove(row));
                }
                else
                {
                    joken = "taishoNo = " + stArrayData[0];
                    foundRows = TaishoKamokuF3.Select(joken);

                    foreach (DataGridViewRow r in mtbListF3.SelectedRows)
                    {
                        rowDMove = KamokuIchiranF3.NewRow();
                        rowDMove["kamokuNo"] = Util.ToInt(this.mtbListF3[1, r.Index].Value);
                        rowDMove["kamokuNm"] = this.mtbListF3[2, r.Index].Value;
                        rowDMove["taishoNo"] = Util.ToInt(stArrayData[0]);
                        rowDMove["taishoNm"] = stArrayData[1];
                        rowDMove["gyoNo"] = Util.ToInt(this.mtbListF3[3, r.Index].Value);
                        rowDMove["taishoTaishaku"] = Util.ToInt(foundRows[0].ItemArray[4]);
                        KamokuIchiranF3.Rows.Add(rowDMove);
                    }

                    Array.ForEach<DataRow>(foundRows, row => TaishoKamokuF3.Rows.Remove(row));
                }

                this.lbxTaishoKanjoKamoku.Items.Remove(this.lbxTaishoKanjoKamoku.Items[selectNum[0]]);
            }

            // 選択している対象勘定科目が0件になる場合、選択行を空表示にする
            if (this.lbxTaishoKanjoKamoku.Items.Count == 0)
            {
                if (tabControl.SelectedIndex == 0)
                {
                    foreach (DataGridViewRow r in mtbListF1.SelectedRows)
                    {
                        this.mtbListF1[2, r.Index].Value = "";
                        this.mtbListF1[4, r.Index].Value = "";
                        this.mtbListF1[5, r.Index].Value = "";
                    }
                }
                else if (tabControl.SelectedIndex == 1)
                {
                    foreach (DataGridViewRow r in mtbListF2.SelectedRows)
                    {
                        this.mtbListF2[2, r.Index].Value = "";
                        this.mtbListF2[4, r.Index].Value = "";
                        this.mtbListF2[5, r.Index].Value = "";
                    }
                }
                else
                {
                    foreach (DataGridViewRow r in mtbListF3.SelectedRows)
                    {
                        this.mtbListF3[2, r.Index].Value = "";
                        this.mtbListF3[4, r.Index].Value = "";
                        this.mtbListF3[5, r.Index].Value = "";
                    }
                }

                this.txtKamokuNm.Text = "";
                this.rdoKari.Checked = false;
                this.rdoKashi.Checked = false;
            }
        }

        /// <summary>
        /// 選択した項目の移動処理(下→上)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnUMove_Click(object sender, EventArgs e)
        {
            int[] selectNum = new int[lbxKanjoKamokuIchiran.SelectedIndices.Count];

            for (int i = 0; i < lbxKanjoKamokuIchiran.SelectedIndices.Count; i++)
            {
                selectNum[i] = lbxKanjoKamokuIchiran.SelectedIndices[i];
            }

            // 選択した項目の追加
            for (int i = 0; i < selectNum.Length; i++)
            {
                this.lbxTaishoKanjoKamoku.Items.Add(this.lbxKanjoKamokuIchiran.Items[selectNum[i]]);
            }

            // 選択した項目の削除
            for (int i = 0; i < selectNum.Length; i++)
            {
                string taishoData = this.lbxKanjoKamokuIchiran.Items[selectNum[0]].ToString();
                string[] stArrayData = taishoData.Split('　');

                string joken = "taishoNo = " + stArrayData[0];
                DataRow[] foundRows;

                // TaishoKamokuデータテーブルに登録
                // KamokuIchiranデータテーブルから削除
                if (tabControl.SelectedIndex == 0)
                {
                    foundRows = KamokuIchiranF1.Select(joken);
                    foreach (DataGridViewRow r in mtbListF1.SelectedRows)
                    {
                        DataRow rowUMove = TaishoKamokuF1.NewRow();
                        rowUMove["kamokuNo"] = Util.ToInt(this.mtbListF1[1, r.Index].Value);
                        rowUMove["kamokuNm"] = Util.ToString(foundRows[0].ItemArray[3]);
                        rowUMove["taishoNo"] = Util.ToInt(stArrayData[0]);
                        rowUMove["taishoNm"] = stArrayData[1];
                        rowUMove["gyoNo"] = Util.ToInt(this.mtbListF1[3, r.Index].Value);
                        rowUMove["taishoTaishaku"] = Util.ToInt(foundRows[0].ItemArray[4]);
                        TaishoKamokuF1.Rows.Add(rowUMove);
                    }

                    Array.ForEach<DataRow>(foundRows, row => KamokuIchiranF1.Rows.Remove(row));
                }
                else if (tabControl.SelectedIndex == 1)
                {
                    foundRows = KamokuIchiranF2.Select(joken);
                    foreach (DataGridViewRow r in mtbListF2.SelectedRows)
                    {
                        DataRow rowUMove = TaishoKamokuF2.NewRow();
                        rowUMove["kamokuNo"] = Util.ToInt(this.mtbListF2[1, r.Index].Value);
                        rowUMove["kamokuNm"] = Util.ToString(foundRows[0].ItemArray[3]);
                        rowUMove["taishoNo"] = Util.ToInt(stArrayData[0]);
                        rowUMove["taishoNm"] = stArrayData[1];
                        rowUMove["gyoNo"] = Util.ToInt(this.mtbListF2[3, r.Index].Value);
                        rowUMove["taishoTaishaku"] = Util.ToInt(foundRows[0].ItemArray[4]);
                        TaishoKamokuF2.Rows.Add(rowUMove);
                    }

                    Array.ForEach<DataRow>(foundRows, row => KamokuIchiranF2.Rows.Remove(row));
                }
                else
                {
                    foundRows = KamokuIchiranF3.Select(joken);
                    foreach (DataGridViewRow r in mtbListF3.SelectedRows)
                    {
                        DataRow rowUMove = TaishoKamokuF3.NewRow();
                        rowUMove["kamokuNo"] = Util.ToInt(this.mtbListF3[1, r.Index].Value);
                        rowUMove["kamokuNm"] = Util.ToString(foundRows[0].ItemArray[3]);
                        rowUMove["taishoNo"] = Util.ToInt(stArrayData[0]);
                        rowUMove["taishoNm"] = stArrayData[1];
                        rowUMove["gyoNo"] = Util.ToInt(this.mtbListF3[3, r.Index].Value);
                        rowUMove["taishoTaishaku"] = Util.ToInt(foundRows[0].ItemArray[4]);
                        TaishoKamokuF3.Rows.Add(rowUMove);
                    }

                    Array.ForEach<DataRow>(foundRows, row => KamokuIchiranF3.Rows.Remove(row));
                }

                this.lbxKanjoKamokuIchiran.Items.Remove(this.lbxKanjoKamokuIchiran.Items[selectNum[0]]);
            }

            // 選択している行が空表示の場合、選択行に対象勘定科目の１行目のデータを表示する
            // 対象勘定科目が0件であれば、実行しない
            if (tabControl.SelectedIndex == 0)
            {
                foreach (DataGridViewRow r in mtbListF1.SelectedRows)
                {
                    if (Util.ToString(this.mtbListF1[2, r.Index].Value) == "" && this.lbxTaishoKanjoKamoku.Items.Count != 0)
                    {
                        string taishoData = this.lbxTaishoKanjoKamoku.Items[0].ToString();
                        string[] stArrayData = taishoData.Split('　');

                        string joken = "taishoNo = " + stArrayData[0];
                        DataRow[] foundRows = TaishoKamokuF1.Select(joken);

                        this.mtbListF1[0, r.Index].Value = 1;
                        this.mtbListF1[2, r.Index].Value = stArrayData[1];
                        if (Util.ToInt(foundRows[0].ItemArray[4]) == 1)
                        {
                            this.mtbListF1[4, r.Index].Value = "借方";
                            this.rdoKari.Checked = true;
                        }
                        else
                        {
                            this.mtbListF1[4, r.Index].Value = "貸方";
                            this.rdoKashi.Checked = true;
                        }
                        this.mtbListF1[5, r.Index].Value = "";

                        this.txtKamokuNm.Text = stArrayData[1];
                    }
                }
            }
            else if (tabControl.SelectedIndex == 1)
            {
                foreach (DataGridViewRow r in mtbListF2.SelectedRows)
                {
                    if (Util.ToString(this.mtbListF2[2, r.Index].Value) == "" && this.lbxTaishoKanjoKamoku.Items.Count != 0)
                    {
                        string taishoData = this.lbxTaishoKanjoKamoku.Items[0].ToString();
                        string[] stArrayData = taishoData.Split('　');

                        string joken = "taishoNo = " + stArrayData[0];
                        DataRow[] foundRows = TaishoKamokuF2.Select(joken);

                        this.mtbListF2[0, r.Index].Value = 1;
                        this.mtbListF2[2, r.Index].Value = stArrayData[1];
                        if (Util.ToInt(foundRows[0].ItemArray[4]) == 1)
                        {
                            this.mtbListF2[4, r.Index].Value = "借方";
                            this.rdoKari.Checked = true;
                        }
                        else
                        {
                            this.mtbListF2[4, r.Index].Value = "貸方";
                            this.rdoKashi.Checked = true;
                        }
                        this.mtbListF2[5, r.Index].Value = "";

                        this.txtKamokuNm.Text = stArrayData[1];
                    }
                }
            }
            else
            {
                foreach (DataGridViewRow r in mtbListF3.SelectedRows)
                {
                    if (Util.ToString(this.mtbListF3[2, r.Index].Value) == "" && this.lbxTaishoKanjoKamoku.Items.Count != 0)
                    {
                        string taishoData = this.lbxTaishoKanjoKamoku.Items[0].ToString();
                        string[] stArrayData = taishoData.Split('　');

                        string joken = "taishoNo = " + stArrayData[0];
                        DataRow[] foundRows = TaishoKamokuF3.Select(joken);

                        this.mtbListF3[0, r.Index].Value = 1;
                        this.mtbListF3[2, r.Index].Value = stArrayData[1];
                        if (Util.ToInt(foundRows[0].ItemArray[4]) == 1)
                        {
                            this.mtbListF3[4, r.Index].Value = "借方";
                            this.rdoKari.Checked = true;
                        }
                        else
                        {
                            this.mtbListF3[4, r.Index].Value = "貸方";
                            this.rdoKashi.Checked = true;
                        }
                        this.mtbListF1[5, r.Index].Value = "";

                        this.txtKamokuNm.Text = stArrayData[1];
                    }
                }
            }
        }
        
        /// <summary>
        /// 科目名変更時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void kamokuNm_change(object sender, KeyEventArgs e)
        {
            string kamokuNm = Util.ToString(this.txtKamokuNm.Text);

            if (tabControl.SelectedIndex == 0)
            {
                foreach (DataGridViewRow r in mtbListF1.SelectedRows)
                {
                    this.mtbListF1[2, r.Index].Value = kamokuNm;
                }
            }
            else if (tabControl.SelectedIndex == 1)
            {
                foreach (DataGridViewRow r in mtbListF2.SelectedRows)
                {
                    this.mtbListF2[2, r.Index].Value = kamokuNm;
                }
            }
            else
            {
                foreach (DataGridViewRow r in mtbListF3.SelectedRows)
                {
                    this.mtbListF3[2, r.Index].Value = kamokuNm;
                }
            }
        }

        /// <summary>
        /// ラジオボタン(借方)押下時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rdoKari_Click(object sender, EventArgs e)
        {
            if (tabControl.SelectedIndex == 0)
            {
                foreach (DataGridViewRow r in mtbListF1.SelectedRows)
                {
                    this.mtbListF1[4, r.Index].Value = "借方";
                }
            }
            else if (tabControl.SelectedIndex == 1)
            {
                foreach (DataGridViewRow r in mtbListF2.SelectedRows)
                {
                    this.mtbListF2[4, r.Index].Value = "借方";
                }
            }
            else
            {
                foreach (DataGridViewRow r in mtbListF3.SelectedRows)
                {
                    this.mtbListF3[4, r.Index].Value = "借方";
                }
            }
        }
        
        /// <summary>
        /// ラジオボタン(貸方)押下時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rdoKashi_Click(object sender, EventArgs e)
        {
            if (tabControl.SelectedIndex == 0)
            {
                foreach (DataGridViewRow r in mtbListF1.SelectedRows)
                {
                    this.mtbListF1[4, r.Index].Value = "貸方";
                }
            }
            else if (tabControl.SelectedIndex == 1)
            {
                foreach (DataGridViewRow r in mtbListF2.SelectedRows)
                {
                    this.mtbListF2[4, r.Index].Value = "貸方";
                }
            }
            else
            {
                foreach (DataGridViewRow r in mtbListF3.SelectedRows)
                {
                    this.mtbListF3[4, r.Index].Value = "貸方";
                }
            }
        }

        /// <summary>
        /// 括弧区分Validating
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKakkoKubun_Validating(object sender, CancelEventArgs e)
        {
            // 入力値補正
            this.txtKakkoKubun.Text = this.FixDefinedCode(this.txtKakkoKubun.Text, 3);
        
            // グリッド列へセット・表示
            if (tabControl.SelectedIndex == 0)
            {
                foreach (DataGridViewRow r in mtbListF1.SelectedRows)
                {
                    this.mtbListF1[5, r.Index].Value = GetKakkoKubunNm(this.txtKakkoKubun.Text);
                    this.mtbListF1[8, r.Index].Value = this.txtKakkoKubun.Text;
                }
            }
            else if (tabControl.SelectedIndex == 1)
            {
                foreach (DataGridViewRow r in mtbListF2.SelectedRows)
                {
                    this.mtbListF2[5, r.Index].Value = GetKakkoKubunNm(this.txtKakkoKubun.Text);
                    this.mtbListF2[9, r.Index].Value = this.txtKakkoKubun.Text;
                }
            }
            else
            {
                foreach (DataGridViewRow r in mtbListF3.SelectedRows)
                {
                    this.mtbListF3[5, r.Index].Value = GetKakkoKubunNm(this.txtKakkoKubun.Text);
                    this.mtbListF3[10, r.Index].Value = this.txtKakkoKubun.Text;
                }
            }
        }

        /// <summary>
        /// 括弧表示Validating
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKakkoHyoji_Validating(object sender, CancelEventArgs e)
        {
            // 入力値補正
            this.txtKakkoHyoji.Text = this.FixDefinedCode(this.txtKakkoHyoji.Text, 1);
            
            // グリッド列へセット
            if (tabControl.SelectedIndex == 0)
            {
                foreach (DataGridViewRow r in mtbListF1.SelectedRows)
                {
                    this.mtbListF1[8, r.Index].Value = this.txtKakkoHyoji.Text;
                }
            }
            else if (tabControl.SelectedIndex == 1)
            {
                foreach (DataGridViewRow r in mtbListF2.SelectedRows)
                {
                    this.mtbListF2[8, r.Index].Value = this.txtKakkoHyoji.Text;
                }
            }
            else
            {
                foreach (DataGridViewRow r in mtbListF3.SelectedRows)
                {
                    this.mtbListF3[8, r.Index].Value = this.txtKakkoHyoji.Text;
                }
            }
        }

        /// <summary>
        /// 文字種別Validating
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMojiShubetsu_Validating(object sender, CancelEventArgs e)
        {
            // 入力値補正
            this.txtMojiShubetsu.Text = this.FixDefinedCode(this.txtMojiShubetsu.Text, 1);
            
            // グリッド列へセット
            if (tabControl.SelectedIndex == 0)
            {
                foreach (DataGridViewRow r in mtbListF1.SelectedRows)
                {
                    this.mtbListF1[9, r.Index].Value = this.txtMojiShubetsu.Text;
                }
            }
            else if (tabControl.SelectedIndex == 1)
            {
                foreach (DataGridViewRow r in mtbListF2.SelectedRows)
                {
                    this.mtbListF2[9, r.Index].Value = this.txtMojiShubetsu.Text;
                }
            }
            else
            {
                foreach (DataGridViewRow r in mtbListF3.SelectedRows)
                {
                    this.mtbListF3[9, r.Index].Value = this.txtMojiShubetsu.Text;
                }
            }
        }

        /// <summary>
        /// タブコントロール変更時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            int visibleSettei = 0;
            string taishaku = "";
            int kamokuCd = 0;
            int gyoBango = 0;
            int kakkoKubun = 0;
            int kakkoHyoji = 0;
            int mojiShubetsu = 0;
            string joken;
            DataRow[] foundRows;
            DataRow[] kamokuIchiranRows;
            int i = 0;

            this.lbxTaishoKanjoKamoku.Items.Clear();
            
            // 試算表⇒決算書の変更
            //this.lbxKanjoKamokuIchiran.Items.Clear();

            // タブコントロール選択情報を取得
            // 可視判断用データを取得
            // 科目名を取得、設定
            // 貸借区分データを取得
            // 文字スタイルデータを取得
            // 科目コードを取得
            // 行番号を取得
            // 条件設定し、それを元に対象勘定科目テーブルからデータを取得
            if (tabControl.SelectedIndex == 0)
            {
                foreach (DataGridViewRow r in mtbListF1.SelectedRows)
                {
                    visibleSettei = Util.ToInt(this.mtbListF1[0, r.Index].Value);
                    this.txtKamokuNm.Text = Util.ToString(this.mtbListF1[2, r.Index].Value);
                    taishaku = Util.ToString(this.mtbListF1[4, r.Index].Value);
                    kamokuCd = Util.ToInt(this.mtbListF1[1, r.Index].Value);
                    gyoBango = Util.ToInt(this.mtbListF1[3, r.Index].Value);
                    kakkoKubun = Util.ToInt(Util.ToString(this.mtbListF1[8, r.Index].Value));
                    kakkoHyoji = Util.ToInt(Util.ToString(this.mtbListF1[9, r.Index].Value));
                    mojiShubetsu = Util.ToInt(Util.ToString(this.mtbListF1[10, r.Index].Value));
                }
                joken = "kamokuNo = " + kamokuCd + " AND gyoNo = " + gyoBango;
                foundRows = TaishoKamokuF1.Select(joken);
                kamokuIchiranRows = KamokuIchiranF1.Select();
            }
            else if (tabControl.SelectedIndex == 1)
            {
                foreach (DataGridViewRow r in mtbListF2.SelectedRows)
                {
                    visibleSettei = Util.ToInt(this.mtbListF2[0, r.Index].Value);
                    this.txtKamokuNm.Text = Util.ToString(this.mtbListF2[2, r.Index].Value);
                    taishaku = Util.ToString(this.mtbListF2[4, r.Index].Value);
                    kamokuCd = Util.ToInt(this.mtbListF2[1, r.Index].Value);
                    gyoBango = Util.ToInt(this.mtbListF2[3, r.Index].Value);
                    kakkoKubun = Util.ToInt(Util.ToString(this.mtbListF2[8, r.Index].Value));
                    kakkoHyoji = Util.ToInt(Util.ToString(this.mtbListF2[9, r.Index].Value));
                    mojiShubetsu = Util.ToInt(Util.ToString(this.mtbListF2[10, r.Index].Value));
                }
                joken = "kamokuNo = " + kamokuCd + " AND gyoNo = " + gyoBango;
                foundRows = TaishoKamokuF2.Select(joken);
                kamokuIchiranRows = KamokuIchiranF2.Select();
            }
            else
            {
                foreach (DataGridViewRow r in mtbListF3.SelectedRows)
                {
                    visibleSettei = Util.ToInt(this.mtbListF3[0, r.Index].Value);
                    this.txtKamokuNm.Text = Util.ToString(this.mtbListF3[2, r.Index].Value);
                    taishaku = Util.ToString(this.mtbListF3[4, r.Index].Value);
                    kamokuCd = Util.ToInt(this.mtbListF3[1, r.Index].Value);
                    gyoBango = Util.ToInt(this.mtbListF3[3, r.Index].Value);
                    kakkoKubun = Util.ToInt(Util.ToString(this.mtbListF3[8, r.Index].Value));
                    kakkoHyoji = Util.ToInt(Util.ToString(this.mtbListF3[9, r.Index].Value));
                    mojiShubetsu = Util.ToInt(Util.ToString(this.mtbListF3[10, r.Index].Value));
                }
                joken = "kamokuNo = " + kamokuCd + " AND gyoNo = " + gyoBango;
                foundRows = TaishoKamokuF3.Select(joken);
                kamokuIchiranRows = KamokuIchiranF3.Select();
            }

            // 対象勘定科目テーブルから取得したデータを表示ListBoxに
            while (foundRows.Length > i)
            {
                this.lbxTaishoKanjoKamoku.Items.Add(foundRows[i].ItemArray[2] + "　" + foundRows[i].ItemArray[3]);
                i++;
            }

            // 2014.04.28試算表⇒決算書の変更
            //// 対象勘定科目テーブルから取得したデータを表示ListBoxに
            //i = 0;
            //while (kamokuIchiranRows.Length > i)
            //{
            //    this.lbxKanjoKamokuIchiran.Items.Add(kamokuIchiranRows[i].ItemArray[2] + "　" + kamokuIchiranRows[i].ItemArray[3]);
            //    i++;
            //}

            // 可視判断用データを設定
            if (visibleSettei == 0)
            {
                this.gbxKanjoKamoku.Visible = false;
                this.gbxMoji.Visible = true;
            }
            else
            {
                this.gbxKanjoKamoku.Visible = true;
                this.gbxMoji.Visible = false;
            }

            // 貸借区分データを設定
            if (taishaku == "借方")
            {
                this.rdoKari.Checked = true;
            }
            else if (taishaku == "貸方")
            {
                this.rdoKashi.Checked = true;
            }
            else
            {
                this.rdoKari.Checked = false;
                this.rdoKashi.Checked = false;
            }

            // 括弧区分データを設定
            this.txtKakkoKubun.Text = Util.ToString(kakkoKubun);

            // 括弧表示データを設定
            this.txtKakkoHyoji.Text = Util.ToString(kakkoHyoji);

            // 文字スタイルデータを設定
            this.txtMojiShubetsu.Text = Util.ToString(mojiShubetsu);
        }
        #endregion

        #region privateメソッド

        /// <summary>
        /// 括弧区分名を取得
        /// </summary>
        /// <param name="kakkoKubun">括弧区分</param>
        /// <returns>「()」「【】」「[]」ブランク</returns>
        private string GetKakkoKubunNm(object kakkoKubun)
        {
            string ret = "";

            switch (Util.ToString(kakkoKubun))
            {
                case "1":
                    ret = "()";
                    break;
                case "2":
                    ret = "【】";
                    break;
                case "3":
                    ret = "[]";
                    break;
                default:
                    break;
            }
            return ret;
        }

        /// <summary>
        /// コード項目の補正
        /// </summary>
        /// <param name="value">入力値</param>
        /// <param name="maxCode">定義上限値</param>
        /// <returns>下限0から上限値までの値</returns>
        private string FixDefinedCode(object value, int maxCode)
        {
            string ret = "0";

            if (value != null)
            {
                if (Util.ToInt(value) > maxCode)
                {
                    ret = Util.ToString(maxCode);
                }
                else
                {
                    ret = Util.ToString(value);
                }
            }
            return ret;
        }

        /// <summary>
        /// 明細部の初期化
        /// </summary>
        private void InitDetailArea()
        {
            ZMYR1011DA da = new ZMYR1011DA(this.UInfo, this.Dba, this.Config);

            // F1:貸借対照表要約設定データを取得
            DataTable yoyakuSetteiF1 = da.GetKamokuDaiKomoku(LIST_F1);
            // F2:損益計算書要約設定データを取得
            DataTable yoyakuSetteiF2 = da.GetKamokuDaiKomoku(LIST_F2);
            // F3:製造原価設定データを取得
            DataTable yoyakuSetteiF3 = da.GetKamokuDaiKomoku(LIST_F3);

            if (yoyakuSetteiF1.Rows.Count == 0 && yoyakuSetteiF2.Rows.Count == 0 && yoyakuSetteiF3.Rows.Count == 0)
            {
                Msg.Error("要約設定情報が読込みできません。");
                this.Close();
                return;
            }

            // セル内文字の配置設定
            this.mtbListF1.Columns["F1Taishaku"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.mtbListF1.Columns["F1Kakko"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.mtbListF2.Columns["F2Taishaku"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.mtbListF2.Columns["F2Kakko"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.mtbListF3.Columns["F3Taishaku"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.mtbListF3.Columns["F3Kakko"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            // 科目名で幅埋め
            this.mtbListF1.Columns["F1Taishaku"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            this.mtbListF2.Columns["F2Taishaku"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            this.mtbListF3.Columns["F3Taishaku"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            // 各対象勘定科目ListBox用データテーブルにカラムを6列ずつ定義
            TaishoKamokuF1.Columns.Add("kamokuNo", Type.GetType("System.Int32"));
            TaishoKamokuF1.Columns.Add("kamokuNm", Type.GetType("System.String"));
            TaishoKamokuF1.Columns.Add("taishoNo", Type.GetType("System.Int32"));
            TaishoKamokuF1.Columns.Add("taishoNm", Type.GetType("System.String"));
            TaishoKamokuF1.Columns.Add("gyoNo", Type.GetType("System.Int32"));
            TaishoKamokuF1.Columns.Add("taishoTaishaku", Type.GetType("System.Int32"));
            TaishoKamokuF2.Columns.Add("kamokuNo", Type.GetType("System.Int32"));
            TaishoKamokuF2.Columns.Add("kamokuNm", Type.GetType("System.String"));
            TaishoKamokuF2.Columns.Add("taishoNo", Type.GetType("System.Int32"));
            TaishoKamokuF2.Columns.Add("taishoNm", Type.GetType("System.String"));
            TaishoKamokuF2.Columns.Add("gyoNo", Type.GetType("System.Int32"));
            TaishoKamokuF2.Columns.Add("taishoTaishaku", Type.GetType("System.Int32"));
            TaishoKamokuF3.Columns.Add("kamokuNo", Type.GetType("System.Int32"));
            TaishoKamokuF3.Columns.Add("kamokuNm", Type.GetType("System.String"));
            TaishoKamokuF3.Columns.Add("taishoNo", Type.GetType("System.Int32"));
            TaishoKamokuF3.Columns.Add("taishoNm", Type.GetType("System.String"));
            TaishoKamokuF3.Columns.Add("gyoNo", Type.GetType("System.Int32"));
            TaishoKamokuF3.Columns.Add("taishoTaishaku", Type.GetType("System.Int32"));

            int i = 0; // ループ用カウント変数
            int j = 0; // サブループ用カウント変数
            int k = 0; // サブループ用カウント変数
            int gyoCount = 0; // 大項目毎行用カウント変数
            int colorCount = 0; // GridView行の色設定用カウント変数
            int dataCount = 0; // 科目分類毎空データ表示用カウント変数
            int kamokuBunruiBefore; // 前の科目分類コード変数
            int kamokuBunrui; // 科目分類コード変数
            int gyoBangoBefore; // 前の行番号変数
            int gyoBango; // 行番号変数
            int meisaiKomokuSu; // 明細項目数変数
            int taishakuKubunNo; // 貸借区分番号変数
            string taishakuKubun; // 貸借区分変数
            int kamokuBunruiCd; // 勘定科目コード変数
            string kamokuBunruiNm; // 勘定科目名変数
            int hyojiJuni;  // 表示順位変数
            int kakkoKubun; // 括弧区分変数
            int kakkoHyoji; // 括弧表示変数
            int mojiShubetsu; // 文字種別変数

            #region DataGridView にF1データを追加
            while (yoyakuSetteiF1.Rows.Count > i)
            {
                // 科目コードを取得
                kamokuBunruiCd = Util.ToInt(yoyakuSetteiF1.Rows[i]["KAMOKU_BUNRUI"]);
                // 科目名を取得
                kamokuBunruiNm = Util.ToString(yoyakuSetteiF1.Rows[i]["KAMOKU_BUNRUI_NM"]);
                // 貸借区分を取得、設定
                taishakuKubunNo = Util.ToInt(yoyakuSetteiF1.Rows[i]["TAISHAKU_KUBUN"]);
                if (taishakuKubunNo == 1)
                {
                    taishakuKubun = "借方";
                }
                else if (taishakuKubunNo == 2)
                {
                    taishakuKubun = "貸方";
                }
                else
                {
                    taishakuKubun = "";
                }
                // 括弧区分表示名称を取得
                string kakkoKubunNm = GetKakkoKubunNm(yoyakuSetteiF1.Rows[i]["KAKKO_KUBUN"]);
                // 表示順位・括弧区分・括弧表示・文字種別
                hyojiJuni = Util.ToInt(Util.ToString(yoyakuSetteiF1.Rows[i]["HYOJI_JUNI"]));
                kakkoKubun = Util.ToInt(Util.ToString(yoyakuSetteiF1.Rows[i]["KAKKO_KUBUN"]));
                kakkoHyoji = Util.ToInt(Util.ToString(yoyakuSetteiF1.Rows[i]["KAKKO_HYOJI"]));
                mojiShubetsu = Util.ToInt(Util.ToString(yoyakuSetteiF1.Rows[i]["MOJI_SHUBETSU"]));

                this.mtbListF1.Rows.Add("0", kamokuBunruiCd, kamokuBunruiNm, "", taishakuKubun, kakkoKubunNm, ""
                    , hyojiJuni, kakkoKubun, kakkoHyoji, mojiShubetsu);
                this.mtbListF1.Rows[colorCount].DefaultCellStyle.BackColor = Color.LightCyan;
                colorCount++;

                // 科目分類コードを取得
                kamokuBunrui = Util.ToInt(yoyakuSetteiF1.Rows[i]["KAMOKU_BUNRUI"]);
                // 明細項目数を取得
                meisaiKomokuSu = Util.ToInt(yoyakuSetteiF1.Rows[i]["MEISAI_KOMOKUSU"]);
                // F1:貸借対照表要約設定データを取得
                DataTable yoyakuSetteiData = da.GetKamokuShoKomoku(LIST_F1, kamokuBunrui);

                // 科目分類毎データを取得数分ループで表示
                /// 科目分類＞0を追加
                while (kamokuBunrui > 0 && yoyakuSetteiData.Rows.Count > j )
                {
                    // 勘定科目名を取得
                    kamokuBunruiNm = Util.ToString(yoyakuSetteiData.Rows[j]["KANJO_KAMOKU_NM"]);
                    // 科目分類コードを取得
                    kamokuBunrui = Util.ToInt(yoyakuSetteiData.Rows[j]["KAMOKU_BUNRUI"]);
                    // 行番号を取得、設定
                    gyoBango = Util.ToInt(yoyakuSetteiData.Rows[j]["GYO_BANGO"]);
                    // 貸借区分を取得、設定
                    taishakuKubunNo = Util.ToInt(yoyakuSetteiData.Rows[j]["TAISHAKU_KUBUN"]);

                    if (taishakuKubunNo == 1)
                    {
                        taishakuKubun = "借方";
                    }
                    else
                    {
                        taishakuKubun = "貸方";
                    }

                    // 前回と今回の科目分類コードが一致 && 前回と今回の行番号が不一致の場合、表示
                    if (j != 0)
                    {
                        // 前の科目分類コードを取得
                        kamokuBunruiBefore = Util.ToInt(yoyakuSetteiData.Rows[j - 1]["KAMOKU_BUNRUI"]);
                        // 前の行番号を取得
                        gyoBangoBefore = Util.ToInt(yoyakuSetteiData.Rows[j - 1]["GYO_BANGO"]);
                        if (kamokuBunruiBefore == kamokuBunrui && gyoBangoBefore != gyoBango)
                        {
                            this.mtbListF1.Rows.Add("1", kamokuBunrui, kamokuBunruiNm, meisaiKomokuSu - (gyoCount + 1), taishakuKubun, "", meisaiKomokuSu - (gyoCount + 1));
                            this.mtbListF1.Rows[colorCount].DefaultCellStyle.BackColor = Color.White;
                            colorCount++;
                            dataCount++;

                            // 対象勘定科目表示用データを格納
                            DataTable taishoData = da.GetYoyakuSetteiData(LIST_F1, kamokuBunrui, gyoBango);
                            k = 0;
                            while (taishoData.Rows.Count > k)
                            {
                                DataRow row = TaishoKamokuF1.NewRow();
                                row["kamokuNo"] = kamokuBunruiCd;
                                row["kamokuNm"] = kamokuBunruiNm;
                                row["taishoNo"] = Util.ToInt(taishoData.Rows[k]["KANJO_KAMOKU_CD"]);
                                row["taishoNm"] = Util.ToString(taishoData.Rows[k]["M_KANJO_KAMOKU_NM"]);
                                row["gyoNo"] = meisaiKomokuSu - (gyoCount + 1);
                                row["taishoTaishaku"] = Util.ToInt(taishoData.Rows[k]["TAISHAKU_KUBUN"]);
                                TaishoKamokuF1.Rows.Add(row);

                                k++;
                            }
                            gyoCount++;
                        }
                    }
                    else
                    {
                        this.mtbListF1.Rows.Add("1", kamokuBunrui, kamokuBunruiNm, meisaiKomokuSu - (gyoCount + 1), taishakuKubun, "", meisaiKomokuSu - (gyoCount + 1));
                        this.mtbListF1.Rows[colorCount].DefaultCellStyle.BackColor = Color.White;
                        colorCount++;
                        dataCount++;

                        // 対象勘定科目表示用データを格納
                        DataTable taishoData = da.GetYoyakuSetteiData(LIST_F1, kamokuBunrui, gyoBango);
                        k = 0;
                        while (taishoData.Rows.Count > k)
                        {
                            DataRow row = TaishoKamokuF1.NewRow();
                            row["kamokuNo"] = kamokuBunruiCd;
                            row["kamokuNm"] = kamokuBunruiNm;
                            row["taishoNo"] = Util.ToInt(taishoData.Rows[k]["KANJO_KAMOKU_CD"]);
                            row["taishoNm"] = Util.ToString(taishoData.Rows[k]["M_KANJO_KAMOKU_NM"]);
                            row["gyoNo"] = meisaiKomokuSu - (gyoCount + 1);
                            row["taishoTaishaku"] = Util.ToInt(taishoData.Rows[k]["TAISHAKU_KUBUN"]);
                            TaishoKamokuF1.Rows.Add(row);

                            k++;
                        }
                        gyoCount++;
                    }
                    j++;
                }
                j = 0;
                gyoCount = 0;

                // 勘定分類毎
                while (meisaiKomokuSu - dataCount > j)
                {
                    this.mtbListF1.Rows.Add("1", kamokuBunrui, "", meisaiKomokuSu - dataCount - (gyoCount + 1), "", "", meisaiKomokuSu - dataCount - (gyoCount + 1));
                    this.mtbListF1.Rows[colorCount].DefaultCellStyle.BackColor = Color.White;
                    colorCount++;
                    gyoCount++;
                    j++;
                }

                i++;
                j = 0;
                dataCount = 0;
                gyoCount = 0;
            }
            #endregion
            i = 0;
            colorCount = 0;
            dataCount = 0;
            gyoCount = 0;
            #region DataGridView にF2データを追加
            while (yoyakuSetteiF2.Rows.Count > i)
            {
                // 科目コードを取得
                kamokuBunruiCd = Util.ToInt(yoyakuSetteiF2.Rows[i]["KAMOKU_BUNRUI"]);
                // 科目名を取得
                kamokuBunruiNm = Util.ToString(yoyakuSetteiF2.Rows[i]["KAMOKU_BUNRUI_NM"]);
                // 貸借区分を取得、設定
                taishakuKubunNo = Util.ToInt(yoyakuSetteiF2.Rows[i]["TAISHAKU_KUBUN"]);
                if (taishakuKubunNo == 1)
                {
                    taishakuKubun = "借方";
                }
                else if (taishakuKubunNo == 2)
                {
                    taishakuKubun = "貸方";
                }
                else
                {
                    taishakuKubun = "";
                }
                // 括弧区分表示名称を取得
                string kakkoKubunNm = GetKakkoKubunNm(yoyakuSetteiF2.Rows[i]["KAKKO_KUBUN"]);
                // 表示順位・括弧区分・括弧表示・文字種別
                hyojiJuni = Util.ToInt(Util.ToString(yoyakuSetteiF2.Rows[i]["HYOJI_JUNI"]));
                kakkoKubun = Util.ToInt(Util.ToString(yoyakuSetteiF2.Rows[i]["KAKKO_KUBUN"]));
                kakkoHyoji = Util.ToInt(Util.ToString(yoyakuSetteiF2.Rows[i]["KAKKO_HYOJI"]));
                mojiShubetsu = Util.ToInt(Util.ToString(yoyakuSetteiF2.Rows[i]["MOJI_SHUBETSU"]));

                this.mtbListF2.Rows.Add("0", kamokuBunruiCd, kamokuBunruiNm, "", taishakuKubun, kakkoKubunNm, ""
                    ,hyojiJuni, kakkoKubun, kakkoHyoji, mojiShubetsu);
                this.mtbListF2.Rows[colorCount].DefaultCellStyle.BackColor = Color.LightCyan;
                colorCount++;

                // 科目分類コードを取得
                kamokuBunrui = Util.ToInt(yoyakuSetteiF2.Rows[i]["KAMOKU_BUNRUI"]);
                // 明細項目数を取得
                meisaiKomokuSu = Util.ToInt(yoyakuSetteiF2.Rows[i]["MEISAI_KOMOKUSU"]);
                // F2:損益計算書要約設定データを取得
                DataTable yoyakuSetteiData = da.GetKamokuShoKomoku(LIST_F2, kamokuBunrui);

                // 科目分類毎データを取得数分ループで表示
                while (yoyakuSetteiData.Rows.Count > j)
                {
                    // 勘定科目名を取得
                    kamokuBunruiNm = Util.ToString(yoyakuSetteiData.Rows[j]["KANJO_KAMOKU_NM"]);
                    // 科目分類コードを取得
                    kamokuBunrui = Util.ToInt(yoyakuSetteiData.Rows[j]["KAMOKU_BUNRUI"]);
                    // 行番号を取得、設定
                    gyoBango = Util.ToInt(yoyakuSetteiData.Rows[j]["GYO_BANGO"]);
                    // 貸借区分を取得、設定
                    taishakuKubunNo = Util.ToInt(yoyakuSetteiData.Rows[j]["TAISHAKU_KUBUN"]);

                    if (taishakuKubunNo == 1)
                    {
                        taishakuKubun = "借方";
                    }
                    else
                    {
                        taishakuKubun = "貸方";
                    }

                    // 前回と今回の科目分類コードが一致 && 前回と今回の行番号が不一致の場合、表示
                    if (j != 0)
                    {
                        // 前の科目分類コードを取得
                        kamokuBunruiBefore = Util.ToInt(yoyakuSetteiData.Rows[j - 1]["KAMOKU_BUNRUI"]);
                        // 前の行番号を取得
                        gyoBangoBefore = Util.ToInt(yoyakuSetteiData.Rows[j - 1]["GYO_BANGO"]);
                        if (kamokuBunruiBefore == kamokuBunrui && gyoBangoBefore != gyoBango)
                        {
                            this.mtbListF2.Rows.Add("1", kamokuBunrui, kamokuBunruiNm, meisaiKomokuSu - (gyoCount + 1), taishakuKubun, "", meisaiKomokuSu - (gyoCount + 1));
                            this.mtbListF2.Rows[colorCount].DefaultCellStyle.BackColor = Color.White;
                            colorCount++;
                            dataCount++;

                            // 対象勘定科目表示用データを格納
                            DataTable taishoData = da.GetYoyakuSetteiData(LIST_F2, kamokuBunrui, gyoBango);
                            k = 0;
                            while (taishoData.Rows.Count > k)
                            {
                                DataRow row = TaishoKamokuF2.NewRow();
                                row["kamokuNo"] = kamokuBunruiCd;
                                row["kamokuNm"] = kamokuBunruiNm;
                                row["taishoNo"] = Util.ToInt(taishoData.Rows[k]["KANJO_KAMOKU_CD"]);
                                row["taishoNm"] = Util.ToString(taishoData.Rows[k]["M_KANJO_KAMOKU_NM"]);
                                row["gyoNo"] = meisaiKomokuSu - (gyoCount + 1);
                                row["taishoTaishaku"] = Util.ToInt(taishoData.Rows[k]["TAISHAKU_KUBUN"]);
                                TaishoKamokuF2.Rows.Add(row);

                                k++;
                            }
                            gyoCount++;
                        }
                    }
                    else
                    {
                        this.mtbListF2.Rows.Add("1", kamokuBunrui, kamokuBunruiNm, meisaiKomokuSu - (gyoCount + 1), taishakuKubun, "", meisaiKomokuSu - (gyoCount + 1));
                        this.mtbListF2.Rows[colorCount].DefaultCellStyle.BackColor = Color.White;
                        colorCount++;
                        dataCount++;

                        // 対象勘定科目表示用データを格納
                        DataTable taishoData = da.GetYoyakuSetteiData(LIST_F2, kamokuBunrui, gyoBango);
                        k = 0;
                        while (taishoData.Rows.Count > k)
                        {
                            DataRow row = TaishoKamokuF2.NewRow();
                            row["kamokuNo"] = kamokuBunruiCd;
                            row["kamokuNm"] = kamokuBunruiNm;
                            row["taishoNo"] = Util.ToInt(taishoData.Rows[k]["KANJO_KAMOKU_CD"]);
                            row["taishoNm"] = Util.ToString(taishoData.Rows[k]["M_KANJO_KAMOKU_NM"]);
                            row["gyoNo"] = meisaiKomokuSu - (gyoCount + 1);
                            row["taishoTaishaku"] = Util.ToInt(taishoData.Rows[k]["TAISHAKU_KUBUN"]);
                            TaishoKamokuF2.Rows.Add(row);

                            k++;
                        }
                        gyoCount++;
                    }
                    j++;
                }
                j = 0;
                gyoCount = 0;

                // 勘定分類毎
                while (meisaiKomokuSu - dataCount > j)
                {
                    this.mtbListF2.Rows.Add("1", kamokuBunrui, "", meisaiKomokuSu - dataCount - (gyoCount + 1), "", "", meisaiKomokuSu - dataCount - (gyoCount + 1));
                    this.mtbListF2.Rows[colorCount].DefaultCellStyle.BackColor = Color.White;
                    colorCount++;
                    gyoCount++;
                    j++;
                }

                i++;
                j = 0;
                dataCount = 0;
                gyoCount = 0;
            }
            #endregion
            i = 0;
            colorCount = 0;
            dataCount = 0;
            gyoCount = 0;
            #region DataGridView にF3データを追加
            while (yoyakuSetteiF3.Rows.Count > i)
            {
                // 科目コードを取得
                kamokuBunruiCd = Util.ToInt(yoyakuSetteiF3.Rows[i]["KAMOKU_BUNRUI"]);
                // 科目名を取得
                kamokuBunruiNm = Util.ToString(yoyakuSetteiF3.Rows[i]["KAMOKU_BUNRUI_NM"]);
                // 貸借区分を取得、設定
                taishakuKubunNo = Util.ToInt(yoyakuSetteiF3.Rows[i]["TAISHAKU_KUBUN"]);
                if (taishakuKubunNo == 1)
                {
                    taishakuKubun = "借方";
                }
                else if (taishakuKubunNo == 2)
                {
                    taishakuKubun = "貸方";
                }
                else
                {
                    taishakuKubun = "";
                }
                // 括弧区分表示名称を取得
                string kakkoKubunNm = GetKakkoKubunNm(yoyakuSetteiF3.Rows[i]["KAKKO_KUBUN"]);
                // 表示順位・括弧区分・括弧表示・文字種別
                hyojiJuni = Util.ToInt(Util.ToString(yoyakuSetteiF3.Rows[i]["HYOJI_JUNI"]));
                kakkoKubun = Util.ToInt(Util.ToString(yoyakuSetteiF3.Rows[i]["KAKKO_KUBUN"]));
                kakkoHyoji = Util.ToInt(Util.ToString(yoyakuSetteiF3.Rows[i]["KAKKO_HYOJI"]));
                mojiShubetsu = Util.ToInt(Util.ToString(yoyakuSetteiF3.Rows[i]["MOJI_SHUBETSU"]));

                this.mtbListF3.Rows.Add("0", kamokuBunruiCd, kamokuBunruiNm, "", taishakuKubun, kakkoKubunNm, ""
                    ,hyojiJuni , kakkoKubun, kakkoHyoji, mojiShubetsu);
                this.mtbListF3.Rows[colorCount].DefaultCellStyle.BackColor = Color.LightCyan;
                colorCount++;

                // 科目分類コードを取得
                kamokuBunrui = Util.ToInt(yoyakuSetteiF3.Rows[i]["KAMOKU_BUNRUI"]);
                // 明細項目数を取得
                meisaiKomokuSu = Util.ToInt(yoyakuSetteiF3.Rows[i]["MEISAI_KOMOKUSU"]);
                // F3:製造原価要約設定データを取得
                DataTable yoyakuSetteiData = da.GetKamokuShoKomoku(LIST_F3, kamokuBunrui);

                // 科目分類毎データを取得数分ループで表示
                while (yoyakuSetteiData.Rows.Count > j)
                {
                    // 勘定科目名を取得
                    kamokuBunruiNm = Util.ToString(yoyakuSetteiData.Rows[j]["KANJO_KAMOKU_NM"]);
                    // 科目分類コードを取得
                    kamokuBunrui = Util.ToInt(yoyakuSetteiData.Rows[j]["KAMOKU_BUNRUI"]);
                    // 行番号を取得、設定
                    gyoBango = Util.ToInt(yoyakuSetteiData.Rows[j]["GYO_BANGO"]);
                    // 貸借区分を取得、設定
                    taishakuKubunNo = Util.ToInt(yoyakuSetteiData.Rows[j]["TAISHAKU_KUBUN"]);

                    if (taishakuKubunNo == 1)
                    {
                        taishakuKubun = "借方";
                    }
                    else
                    {
                        taishakuKubun = "貸方";
                    }

                    // 前回と今回の科目分類コードが一致 && 前回と今回の行番号が不一致の場合、表示
                    if (j != 0)
                    {
                        // 前の科目分類コードを取得
                        kamokuBunruiBefore = Util.ToInt(yoyakuSetteiData.Rows[j - 1]["KAMOKU_BUNRUI"]);
                        // 前の行番号を取得
                        gyoBangoBefore = Util.ToInt(yoyakuSetteiData.Rows[j - 1]["GYO_BANGO"]);
                        if (kamokuBunruiBefore == kamokuBunrui && gyoBangoBefore != gyoBango)
                        {
                            this.mtbListF3.Rows.Add("1", kamokuBunrui, kamokuBunruiNm, meisaiKomokuSu - (gyoCount + 1), taishakuKubun, "", meisaiKomokuSu - (gyoCount + 1));
                            this.mtbListF3.Rows[colorCount].DefaultCellStyle.BackColor = Color.White;
                            colorCount++;
                            dataCount++;

                            // 対象勘定科目表示用データを格納
                            DataTable taishoData = da.GetYoyakuSetteiData(LIST_F3, kamokuBunrui, gyoBango);
                            k = 0;
                            while (taishoData.Rows.Count > k)
                            {
                                DataRow row = TaishoKamokuF3.NewRow();
                                row["kamokuNo"] = kamokuBunruiCd;
                                row["kamokuNm"] = kamokuBunruiNm;
                                row["taishoNo"] = Util.ToInt(taishoData.Rows[k]["KANJO_KAMOKU_CD"]);
                                row["taishoNm"] = Util.ToString(taishoData.Rows[k]["M_KANJO_KAMOKU_NM"]);
                                row["gyoNo"] = meisaiKomokuSu - (gyoCount + 1);
                                row["taishoTaishaku"] = Util.ToInt(taishoData.Rows[k]["TAISHAKU_KUBUN"]);
                                TaishoKamokuF3.Rows.Add(row);

                                k++;
                            }
                            gyoCount++;
                        }
                    }
                    else
                    {
                        this.mtbListF3.Rows.Add("1", kamokuBunrui, kamokuBunruiNm, meisaiKomokuSu - (gyoCount + 1), taishakuKubun, "", meisaiKomokuSu - (gyoCount + 1));
                        this.mtbListF3.Rows[colorCount].DefaultCellStyle.BackColor = Color.White;
                        colorCount++;
                        dataCount++;

                        // 対象勘定科目表示用データを格納
                        DataTable taishoData = da.GetYoyakuSetteiData(LIST_F3, kamokuBunrui, gyoBango);
                        k = 0;
                        while (taishoData.Rows.Count > k)
                        {
                            DataRow row = TaishoKamokuF3.NewRow();
                            row["kamokuNo"] = kamokuBunruiCd;
                            row["kamokuNm"] = kamokuBunruiNm;
                            row["taishoNo"] = Util.ToInt(taishoData.Rows[k]["KANJO_KAMOKU_CD"]);
                            row["taishoNm"] = Util.ToString(taishoData.Rows[k]["M_KANJO_KAMOKU_NM"]);
                            row["gyoNo"] = meisaiKomokuSu - (gyoCount + 1);
                            row["taishoTaishaku"] = Util.ToInt(taishoData.Rows[k]["TAISHAKU_KUBUN"]);
                            TaishoKamokuF3.Rows.Add(row);

                            k++;
                        }
                        gyoCount++;
                    }
                    j++;
                }
                j = 0;
                gyoCount = 0;

                // 勘定分類毎
                while (meisaiKomokuSu - dataCount > j)
                {
                    this.mtbListF3.Rows.Add("1", kamokuBunrui, "", meisaiKomokuSu - dataCount - (gyoCount + 1), "", "", meisaiKomokuSu - dataCount - (gyoCount + 1));
                    this.mtbListF3.Rows[colorCount].DefaultCellStyle.BackColor = Color.White;
                    colorCount++;
                    gyoCount++;
                    j++;
                }

                i++;
                j = 0;
                dataCount = 0;
                gyoCount = 0;
            }
            #endregion

            //mtbListF1,mtbListF2,mtbListF3にユーザーが新しい行を追加できないようにする
            this.mtbListF1.AllowUserToAddRows = false;
            this.mtbListF2.AllowUserToAddRows = false;
            this.mtbListF3.AllowUserToAddRows = false;

            #region 要約設定
            // 科目名を取得
            kamokuBunruiNm = Util.ToString(yoyakuSetteiF1.Rows[0]["KAMOKU_BUNRUI_NM"]);
            // 貸借区分を取得、設定
            taishakuKubunNo = Util.ToInt(yoyakuSetteiF1.Rows[0]["TAISHAKU_KUBUN"]);
            // 括弧区分を取得、設定
            kakkoKubun = Util.ToInt(Util.ToString(yoyakuSetteiF1.Rows[0]["KAKKO_KUBUN"]));
            // 括弧表示を取得、設定
            kakkoHyoji = Util.ToInt(Util.ToString(yoyakuSetteiF1.Rows[0]["KAKKO_HYOJI"]));
            // 文字種別を取得、設定
            mojiShubetsu = Util.ToInt(Util.ToString(yoyakuSetteiF1.Rows[0]["MOJI_SHUBETSU"]));

            this.txtKamokuNm.Text = kamokuBunruiNm;
            if (taishakuKubunNo == 1)
            {
                this.rdoKari.Checked = true;
            }
            else
            {
                this.rdoKashi.Checked = true;
            }

            // 括弧区分を設定
            this.txtKakkoKubun.Text = Util.ToString(kakkoKubun);
            // 括弧表示を設定
            this.txtKakkoHyoji.Text = Util.ToString(kakkoHyoji);
            // 文字スタイルを設定
            this.txtMojiShubetsu.Text = Util.ToString(mojiShubetsu);
            // 勘定科目設定グループを非表示
            this.gbxKanjoKamoku.Visible = false;
            #endregion
        }

        /// <summary>
        /// 明細部の初期化
        /// </summary>
        private void yoyakuSetteiArea()
        {
            ZMYR1011DA da = new ZMYR1011DA(this.UInfo, this.Dba, this.Config);

            // F1:貸借対照表要約設定データを取得
            DataTable yoyakuSetteiF1 = da.GetKamokuDaiKomoku(LIST_F1);
            // F2:損益計算書要約設定データを取得
            DataTable yoyakuSetteiF2 = da.GetKamokuDaiKomoku(LIST_F2);
            // F3:製造原価要約設定データを取得
            DataTable yoyakuSetteiF3 = da.GetKamokuDaiKomoku(LIST_F3);

            // 各対象勘定科目ListBox用データテーブルにカラムを6列ずつ定義
            KamokuIchiranF1.Columns.Add("kamokuNo", Type.GetType("System.Int32"));
            KamokuIchiranF1.Columns.Add("kamokuNm", Type.GetType("System.String"));
            KamokuIchiranF1.Columns.Add("taishoNo", Type.GetType("System.Int32"));
            KamokuIchiranF1.Columns.Add("taishoNm", Type.GetType("System.String"));
            KamokuIchiranF1.Columns.Add("gyoNo", Type.GetType("System.Int32"));
            KamokuIchiranF1.Columns.Add("taishoTaishaku", Type.GetType("System.Int32"));
            KamokuIchiranF2.Columns.Add("kamokuNo", Type.GetType("System.Int32"));
            KamokuIchiranF2.Columns.Add("kamokuNm", Type.GetType("System.String"));
            KamokuIchiranF2.Columns.Add("taishoNo", Type.GetType("System.Int32"));
            KamokuIchiranF2.Columns.Add("taishoNm", Type.GetType("System.String"));
            KamokuIchiranF2.Columns.Add("gyoNo", Type.GetType("System.Int32"));
            KamokuIchiranF2.Columns.Add("taishoTaishaku", Type.GetType("System.Int32"));
            KamokuIchiranF3.Columns.Add("kamokuNo", Type.GetType("System.Int32"));
            KamokuIchiranF3.Columns.Add("kamokuNm", Type.GetType("System.String"));
            KamokuIchiranF3.Columns.Add("taishoNo", Type.GetType("System.Int32"));
            KamokuIchiranF3.Columns.Add("taishoNm", Type.GetType("System.String"));
            KamokuIchiranF3.Columns.Add("gyoNo", Type.GetType("System.Int32"));
            KamokuIchiranF3.Columns.Add("taishoTaishaku", Type.GetType("System.Int32"));

            int i = 0;
            int j = 0;
            int kamokuCd;
            int taishoCd;
            string taishoNm;
            int taishakuKubun;

            // 勘定科目一覧を取得
            #region F1:貸借対照表
            while (yoyakuSetteiF1.Rows.Count > i)
            {
                kamokuCd = Util.ToInt(yoyakuSetteiF1.Rows[i]["KAMOKU_BUNRUI"]);
                DataTable GetKanjoKamokuIchiran = da.GetKanjoKamokuIchiran(kamokuCd);

                while (GetKanjoKamokuIchiran.Rows.Count > j)
                {
                    taishoCd = Util.ToInt(GetKanjoKamokuIchiran.Rows[j]["KANJO_KAMOKU_CD"]);
                    taishoNm = Util.ToString(GetKanjoKamokuIchiran.Rows[j]["KANJO_KAMOKU_NM"]);
                    taishakuKubun = Util.ToInt(GetKanjoKamokuIchiran.Rows[j]["TAISHAKU_KUBUN"]);

                    DataRow row = KamokuIchiranF1.NewRow();
                    row["kamokuNo"] = kamokuCd;
                    row["kamokuNm"] = "";
                    row["taishoNo"] = taishoCd;
                    row["taishoNm"] = taishoNm;
                    row["gyoNo"] = 999;
                    row["taishoTaishaku"] = taishakuKubun;
                    KamokuIchiranF1.Rows.Add(row);

                    j++;
                }
                i++;
                j = 0;
            }
            #endregion
            i = 0;
            #region F2:損益計算書
            while (yoyakuSetteiF2.Rows.Count > i)
            {
                kamokuCd = Util.ToInt(yoyakuSetteiF2.Rows[i]["KAMOKU_BUNRUI"]);
                DataTable GetKanjoKamokuIchiran = da.GetKanjoKamokuIchiran(kamokuCd);

                while (GetKanjoKamokuIchiran.Rows.Count > j)
                {

                    taishoCd = Util.ToInt(GetKanjoKamokuIchiran.Rows[j]["KANJO_KAMOKU_CD"]);
                    taishoNm = Util.ToString(GetKanjoKamokuIchiran.Rows[j]["KANJO_KAMOKU_NM"]);
                    taishakuKubun = Util.ToInt(GetKanjoKamokuIchiran.Rows[j]["TAISHAKU_KUBUN"]);

                    DataRow row = KamokuIchiranF2.NewRow();
                    row["kamokuNo"] = kamokuCd;
                    row["kamokuNm"] = "";
                    row["taishoNo"] = taishoCd;
                    row["taishoNm"] = taishoNm;
                    row["gyoNo"] = 999;
                    row["taishoTaishaku"] = taishakuKubun;
                    KamokuIchiranF2.Rows.Add(row);

                    j++;
                }
                i++;
                j = 0;
            }
            #endregion
            i = 0;
            #region F3:製造原価
            while (yoyakuSetteiF3.Rows.Count > i)
            {
                kamokuCd = Util.ToInt(yoyakuSetteiF3.Rows[i]["KAMOKU_BUNRUI"]);
                DataTable GetKanjoKamokuIchiran = da.GetKanjoKamokuIchiran(kamokuCd);

                while (GetKanjoKamokuIchiran.Rows.Count > j)
                {

                    taishoCd = Util.ToInt(GetKanjoKamokuIchiran.Rows[j]["KANJO_KAMOKU_CD"]);
                    taishoNm = Util.ToString(GetKanjoKamokuIchiran.Rows[j]["KANJO_KAMOKU_NM"]);
                    taishakuKubun = Util.ToInt(GetKanjoKamokuIchiran.Rows[j]["TAISHAKU_KUBUN"]);

                    DataRow row = KamokuIchiranF3.NewRow();
                    row["kamokuNo"] = kamokuCd;
                    row["kamokuNm"] = "";
                    row["taishoNo"] = taishoCd;
                    row["taishoNm"] = taishoNm;
                    row["gyoNo"] = 999;
                    row["taishoTaishaku"] = taishakuKubun;
                    KamokuIchiranF3.Rows.Add(row);

                    j++;
                }
                i++;
                j = 0;
            }
            #endregion
            i = 0;

            // 勘定科目一覧から対象勘定科目を削除
            #region F1:貸借対照表
            while (yoyakuSetteiF1.Rows.Count > i)
            {
                kamokuCd = Util.ToInt(yoyakuSetteiF1.Rows[i]["KAMOKU_BUNRUI"]);
                DataTable GetYoyakuSetteiData = da.GetKamokuShoKomoku(LIST_F1, kamokuCd);

                while (GetYoyakuSetteiData.Rows.Count > j)
                {
                    kamokuCd = Util.ToInt(GetYoyakuSetteiData.Rows[j]["KANJO_KAMOKU_CD"]);

                    string joken = "taishoNo = " + kamokuCd;
                    DataRow[] foundRows = KamokuIchiranF1.Select(joken);
                    Array.ForEach<DataRow>(foundRows, row => KamokuIchiranF1.Rows.Remove(row));

                    j++;
                }

                i++;
                j = 0;
            }
            #endregion
            i = 0;
            #region F2:損益計算書
            while (yoyakuSetteiF2.Rows.Count > i)
            {
                kamokuCd = Util.ToInt(yoyakuSetteiF2.Rows[i]["KAMOKU_BUNRUI"]);
                DataTable GetYoyakuSetteiData = da.GetKamokuShoKomoku(LIST_F2, kamokuCd);

                while (GetYoyakuSetteiData.Rows.Count > j)
                {
                    kamokuCd = Util.ToInt(GetYoyakuSetteiData.Rows[j]["KANJO_KAMOKU_CD"]);

                    string joken = "taishoNo = " + kamokuCd;
                    DataRow[] foundRows = KamokuIchiranF2.Select(joken);
                    Array.ForEach<DataRow>(foundRows, row => KamokuIchiranF2.Rows.Remove(row));

                    j++;
                }

                i++;
                j = 0;
            }
            #endregion
            i = 0;
            #region F3:損益計算書
            while (yoyakuSetteiF3.Rows.Count > i)
            {
                kamokuCd = Util.ToInt(yoyakuSetteiF3.Rows[i]["KAMOKU_BUNRUI"]);
                DataTable GetYoyakuSetteiData = da.GetKamokuShoKomoku(LIST_F3, kamokuCd);

                while (GetYoyakuSetteiData.Rows.Count > j)
                {
                    kamokuCd = Util.ToInt(GetYoyakuSetteiData.Rows[j]["KANJO_KAMOKU_CD"]);

                    string joken = "taishoNo = " + kamokuCd;
                    DataRow[] foundRows = KamokuIchiranF3.Select(joken);
                    Array.ForEach<DataRow>(foundRows, row => KamokuIchiranF3.Rows.Remove(row));

                    j++;
                }

                i++;
                j = 0;
            }
            #endregion

            DataRow[] kamokuIchiranRowsF1 = KamokuIchiranF1.Select();

            // 2014.04.28試算表⇒決算書の変更
            //i = 0;
            //while (KamokuIchiranF1.Rows.Count > i)
            //{
            //    this.lbxKanjoKamokuIchiran.Items.Add(kamokuIchiranRowsF1[i].ItemArray[2] + "　" + kamokuIchiranRowsF1[i].ItemArray[3]);
            //    i++;
            //}
            // 決算書科目設定に未定義データを科目一覧へ追加
            DataTable GetIchiran = da.GetKanjoKamokuIchiran(0);
            foreach (DataRow dr in GetIchiran.Rows)
            {
                this.lbxKanjoKamokuIchiran.Items.Add(dr["KANJO_KAMOKU_CD"] + "　" + dr["KANJO_KAMOKU_NM"]);
            }
        }

        /// <summary>
        /// GridView選択行の削除
        /// </summary>
        private void gridViewRowDelete()
        {
            // 選択行情報を取得
            int visibleFlag = 0;
            if (tabControl.SelectedIndex == 0)
            {
                foreach (DataGridViewRow r in mtbListF1.SelectedRows)
                {
                    visibleFlag = Util.ToInt(this.mtbListF1[0, r.Index].Value);
                }
            }
            else if (tabControl.SelectedIndex == 1)
            {
                foreach (DataGridViewRow r in mtbListF2.SelectedRows)
                {
                    visibleFlag = Util.ToInt(this.mtbListF2[0, r.Index].Value);
                }
            }
            else
            {
                foreach (DataGridViewRow r in mtbListF3.SelectedRows)
                {
                    visibleFlag = Util.ToInt(this.mtbListF3[0, r.Index].Value);
                }
            }

            if (visibleFlag == 1)
            {
                // 対象勘定科目ListBox内の全データ情報を取得
                DataRow[] foundRows;
                int i = 0;
                while (lbxTaishoKanjoKamoku.Items.Count > i)
                {
                    // ListBox内データ情報を取得
                    string taishoData = Util.ToString(lbxTaishoKanjoKamoku.Items[i]);
                    string[] stArrayData = taishoData.Split('　');

                    // KamokuIchiranデータテーブルに登録
                    string joken = "taishoNo = " + stArrayData[0];
                    DataRow rowDMove;
                    if (tabControl.SelectedIndex == 0)
                    {
                        foundRows = TaishoKamokuF1.Select(joken);
                        rowDMove = KamokuIchiranF1.NewRow();
                        foreach (DataGridViewRow r in mtbListF1.SelectedRows)
                        {
                            rowDMove["kamokuNo"] = Util.ToInt(this.mtbListF1[1, r.Index].Value);
                            rowDMove["kamokuNm"] = "";
                            rowDMove["taishoNo"] = Util.ToInt(stArrayData[0]);
                            rowDMove["taishoNm"] = stArrayData[1];
                            rowDMove["gyoNo"] = Util.ToInt(this.mtbListF1[3, r.Index].Value);
                        }
                        rowDMove["taishoTaishaku"] = Util.ToInt(foundRows[0].ItemArray[4]);
                        KamokuIchiranF1.Rows.Add(rowDMove);

                        // TaishoKamokuデータテーブルから削除
                        Array.ForEach<DataRow>(foundRows, row => TaishoKamokuF1.Rows.Remove(row));
                    }
                    else if (tabControl.SelectedIndex == 1)
                    {
                        foundRows = TaishoKamokuF2.Select(joken);
                        rowDMove = KamokuIchiranF2.NewRow();
                        foreach (DataGridViewRow r in mtbListF2.SelectedRows)
                        {
                            rowDMove["kamokuNo"] = Util.ToInt(this.mtbListF2[1, r.Index].Value);
                            rowDMove["kamokuNm"] = "";
                            rowDMove["taishoNo"] = Util.ToInt(stArrayData[0]);
                            rowDMove["taishoNm"] = stArrayData[1];
                            rowDMove["gyoNo"] = Util.ToInt(this.mtbListF2[3, r.Index].Value);
                        }
                        rowDMove["taishoTaishaku"] = Util.ToInt(foundRows[0].ItemArray[4]);
                        KamokuIchiranF2.Rows.Add(rowDMove);

                        // TaishoKamokuデータテーブルから削除
                        Array.ForEach<DataRow>(foundRows, row => TaishoKamokuF2.Rows.Remove(row));
                    }
                    else
                    {
                        foundRows = TaishoKamokuF3.Select(joken);
                        rowDMove = KamokuIchiranF3.NewRow();
                        foreach (DataGridViewRow r in mtbListF3.SelectedRows)
                        {
                            rowDMove["kamokuNo"] = Util.ToInt(this.mtbListF3[1, r.Index].Value);
                            rowDMove["kamokuNm"] = "";
                            rowDMove["taishoNo"] = Util.ToInt(stArrayData[0]);
                            rowDMove["taishoNm"] = stArrayData[1];
                            rowDMove["gyoNo"] = Util.ToInt(this.mtbListF3[3, r.Index].Value);
                        }
                        rowDMove["taishoTaishaku"] = Util.ToInt(foundRows[0].ItemArray[4]);
                        KamokuIchiranF3.Rows.Add(rowDMove);

                        // TaishoKamokuデータテーブルから削除
                        Array.ForEach<DataRow>(foundRows, row => TaishoKamokuF3.Rows.Remove(row));
                    }

                    // 対象勘定科目ListBoxから勘定科目一覧ListBoxにデータを移動
                    this.lbxKanjoKamokuIchiran.Items.Add(this.lbxTaishoKanjoKamoku.Items[i]);

                    i++;
                }
                this.lbxTaishoKanjoKamoku.Items.Clear();

                // 選択行のデータを取得
                int kamokuNo = 0;
                int gyoNum = 0;
                int indexNum = 0;

                // GridViewから選択行を削除し、大項目内１番下に空行を追加
                if (tabControl.SelectedIndex == 0)
                {
                    foreach (DataGridViewRow r in mtbListF1.SelectedRows)
                    {
                        kamokuNo = Util.ToInt(this.mtbListF1[1, r.Index].Value);
                        gyoNum = Util.ToInt(this.mtbListF1[6, r.Index].Value);
                        indexNum = r.Index + 1;
                        if (!r.IsNewRow)
                        {
                            mtbListF1.Rows.Insert(indexNum + Util.ToInt(this.mtbListF1[6, r.Index].Value));
                            this.mtbListF1[0, indexNum + Util.ToInt(this.mtbListF1[6, r.Index].Value)].Value = "1";
                            this.mtbListF1[1, indexNum + Util.ToInt(this.mtbListF1[6, r.Index].Value)].Value = kamokuNo;
                            this.mtbListF1[2, indexNum + Util.ToInt(this.mtbListF1[6, r.Index].Value)].Value = "";
                            this.mtbListF1[3, indexNum + Util.ToInt(this.mtbListF1[6, r.Index].Value)].Value = "0";
                            this.mtbListF1[4, indexNum + Util.ToInt(this.mtbListF1[6, r.Index].Value)].Value = "";
                            this.mtbListF1[5, indexNum + Util.ToInt(this.mtbListF1[6, r.Index].Value)].Value = "";
                            this.mtbListF1[6, indexNum + Util.ToInt(this.mtbListF1[6, r.Index].Value)].Value = "0";
                            mtbListF1.Rows.Remove(r);
                        }
                    }
                }
                else if (tabControl.SelectedIndex == 1)
                {
                    foreach (DataGridViewRow r in mtbListF2.SelectedRows)
                    {
                        kamokuNo = Util.ToInt(this.mtbListF2[1, r.Index].Value);
                        gyoNum = Util.ToInt(this.mtbListF2[6, r.Index].Value);
                        indexNum = r.Index + 1;
                        if (!r.IsNewRow)
                        {
                            mtbListF2.Rows.Insert(indexNum + Util.ToInt(this.mtbListF2[6, r.Index].Value));
                            this.mtbListF2[0, indexNum + Util.ToInt(this.mtbListF2[6, r.Index].Value)].Value = "1";
                            this.mtbListF2[1, indexNum + Util.ToInt(this.mtbListF2[6, r.Index].Value)].Value = kamokuNo;
                            this.mtbListF2[2, indexNum + Util.ToInt(this.mtbListF2[6, r.Index].Value)].Value = "";
                            this.mtbListF2[3, indexNum + Util.ToInt(this.mtbListF2[6, r.Index].Value)].Value = "0";
                            this.mtbListF2[4, indexNum + Util.ToInt(this.mtbListF2[6, r.Index].Value)].Value = "";
                            this.mtbListF2[5, indexNum + Util.ToInt(this.mtbListF2[6, r.Index].Value)].Value = "";
                            this.mtbListF2[6, indexNum + Util.ToInt(this.mtbListF2[6, r.Index].Value)].Value = "0";
                            mtbListF2.Rows.Remove(r);
                        }
                    }
                }
                else
                {
                    foreach (DataGridViewRow r in mtbListF3.SelectedRows)
                    {
                        kamokuNo = Util.ToInt(this.mtbListF3[1, r.Index].Value);
                        gyoNum = Util.ToInt(this.mtbListF3[6, r.Index].Value);
                        indexNum = r.Index + 1;
                        if (!r.IsNewRow)
                        {
                            mtbListF3.Rows.Insert(indexNum + Util.ToInt(this.mtbListF3[6, r.Index].Value));
                            this.mtbListF3[0, indexNum + Util.ToInt(this.mtbListF3[6, r.Index].Value)].Value = "1";
                            this.mtbListF3[1, indexNum + Util.ToInt(this.mtbListF3[6, r.Index].Value)].Value = kamokuNo;
                            this.mtbListF3[2, indexNum + Util.ToInt(this.mtbListF3[6, r.Index].Value)].Value = "";
                            this.mtbListF3[3, indexNum + Util.ToInt(this.mtbListF3[6, r.Index].Value)].Value = "0";
                            this.mtbListF3[4, indexNum + Util.ToInt(this.mtbListF3[6, r.Index].Value)].Value = "";
                            this.mtbListF3[5, indexNum + Util.ToInt(this.mtbListF3[6, r.Index].Value)].Value = "";
                            this.mtbListF3[6, indexNum + Util.ToInt(this.mtbListF3[6, r.Index].Value)].Value = "0";
                            mtbListF3.Rows.Remove(r);
                        }
                    }
                }

                int kanjoKamoku;
                int gyoBango;
                string upDatejoken;
                int j;
                for (i = 0; gyoNum > i; i++)
                {
                    if (tabControl.SelectedIndex == 0)
                    {
                        kanjoKamoku = Util.ToInt(this.mtbListF1[1, indexNum + i - 1].Value);
                        gyoBango = Util.ToInt(this.mtbListF1[3, indexNum + i - 1].Value);
                        upDatejoken = "kamokuNo = " + kanjoKamoku + " AND gyoNo = " + gyoBango;
                        foundRows = TaishoKamokuF1.Select(upDatejoken);
                        // 選択した行の対象勘定科目データの行番号を書き換え
                        j = 0;
                        while (foundRows.Length > j)
                        {
                            foundRows[j]["gyoNo"] = gyoNum - i;
                            j++;
                        }

                        // 追加行以下の追加用行番号とデータテーブル検索用行番号を書き換える
                        this.mtbListF1[3, indexNum + i - 1].Value = gyoNum - i;
                        this.mtbListF1[6, indexNum + i - 1].Value = gyoNum - i;
                    }
                    else if (tabControl.SelectedIndex == 1)
                    {
                        kanjoKamoku = Util.ToInt(this.mtbListF2[1, indexNum + i - 1].Value);
                        gyoBango = Util.ToInt(this.mtbListF2[3, indexNum + i - 1].Value);
                        upDatejoken = "kamokuNo = " + kanjoKamoku + " AND gyoNo = " + gyoBango;
                        foundRows = TaishoKamokuF2.Select(upDatejoken);
                        // 選択した行の対象勘定科目データの行番号を書き換え
                        j = 0;
                        while (foundRows.Length > j)
                        {
                            foundRows[j]["gyoNo"] = gyoNum - i;
                            j++;
                        }

                        // 追加行以下の追加用行番号とデータテーブル検索用行番号を書き換える
                        this.mtbListF2[3, indexNum + i - 1].Value = gyoNum - i;
                        this.mtbListF2[6, indexNum + i - 1].Value = gyoNum - i;
                    }
                    else
                    {
                        kanjoKamoku = Util.ToInt(this.mtbListF3[1, indexNum + i - 1].Value);
                        gyoBango = Util.ToInt(this.mtbListF3[3, indexNum + i - 1].Value);
                        upDatejoken = "kamokuNo = " + kanjoKamoku + " AND gyoNo = " + gyoBango;
                        foundRows = TaishoKamokuF3.Select(upDatejoken);
                        // 選択した行の対象勘定科目データの行番号を書き換え
                        j = 0;
                        while (foundRows.Length > j)
                        {
                            foundRows[j]["gyoNo"] = gyoNum - i;
                            j++;
                        }

                        // 追加行以下の追加用行番号とデータテーブル検索用行番号を書き換える
                        this.mtbListF3[3, indexNum + i - 1].Value = gyoNum - i;
                        this.mtbListF3[6, indexNum + i - 1].Value = gyoNum - i;
                    }
                }
            }

            //選択されている行を表示
            if (tabControl.SelectedIndex == 0)
            {
                foreach (DataGridViewRow r in mtbListF1.SelectedRows)
                {
                    if (Util.ToString(this.mtbListF1[1, r.Index].Value) != "")
                    {
                        ZMYR1011DA da = new ZMYR1011DA(this.UInfo, this.Dba, this.Config);
                        // 科目コードを取得
                        int kamokuCd = Util.ToInt(this.mtbListF1[1, r.Index].Value);
                        // 行番号を取得
                        int gyoBango = Util.ToInt(this.mtbListF1[3, r.Index].Value);

                        string joken = "kamokuNo = " + kamokuCd + " AND gyoNo = " + gyoBango;
                        DataRow[] foundRows = TaishoKamokuF1.Select(joken);

                        int i = 0;
                        // 選択した行の対象勘定科目データを取得表示
                        while (foundRows.Length > i)
                        {
                            this.lbxTaishoKanjoKamoku.Items.Add(foundRows[i].ItemArray[2] + "　" + foundRows[i].ItemArray[3]);
                            i++;
                        }
                    }
                    this.txtKamokuNm.Text = Util.ToString(this.mtbListF1[2, r.Index].Value);
                    if (Util.ToString(this.mtbListF1[4, r.Index].Value) == "借方")
                    {
                        this.rdoKari.Checked = true;
                    }
                    else if (Util.ToString(this.mtbListF1[4, r.Index].Value) == "貸方")
                    {
                        this.rdoKashi.Checked = true;
                    }
                    else
                    {
                        this.rdoKari.Checked = false;
                        this.rdoKashi.Checked = false;
                    }
                }
            }
            else if (tabControl.SelectedIndex == 1)
            {
                foreach (DataGridViewRow r in mtbListF2.SelectedRows)
                {
                    if (Util.ToString(this.mtbListF2[1, r.Index].Value) != "")
                    {
                        ZMYR1011DA da = new ZMYR1011DA(this.UInfo, this.Dba, this.Config);
                        // 科目コードを取得
                        int kamokuCd = Util.ToInt(this.mtbListF2[1, r.Index].Value);
                        // 行番号を取得
                        int gyoBango = Util.ToInt(this.mtbListF2[3, r.Index].Value);

                        string joken = "kamokuNo = " + kamokuCd + " AND gyoNo = " + gyoBango;
                        DataRow[] foundRows = TaishoKamokuF2.Select(joken);

                        int i = 0;
                        // 選択した行の対象勘定科目データを取得表示
                        while (foundRows.Length > i)
                        {
                            this.lbxTaishoKanjoKamoku.Items.Add(foundRows[i].ItemArray[2] + "　" + foundRows[i].ItemArray[3]);
                            i++;
                        }
                    }
                    this.txtKamokuNm.Text = Util.ToString(this.mtbListF2[2, r.Index].Value);
                    if (Util.ToString(this.mtbListF2[4, r.Index].Value) == "借方")
                    {
                        this.rdoKari.Checked = true;
                    }
                    else if (Util.ToString(this.mtbListF2[4, r.Index].Value) == "貸方")
                    {
                        this.rdoKashi.Checked = true;
                    }
                    else
                    {
                        this.rdoKari.Checked = false;
                        this.rdoKashi.Checked = false;
                    }
                }
            }
            else
            {
                foreach (DataGridViewRow r in mtbListF3.SelectedRows)
                {
                    if (Util.ToString(this.mtbListF3[1, r.Index].Value) != "")
                    {
                        ZMYR1011DA da = new ZMYR1011DA(this.UInfo, this.Dba, this.Config);
                        // 科目コードを取得
                        int kamokuCd = Util.ToInt(this.mtbListF3[1, r.Index].Value);
                        // 行番号を取得
                        int gyoBango = Util.ToInt(this.mtbListF3[3, r.Index].Value);

                        string joken = "kamokuNo = " + kamokuCd + " AND gyoNo = " + gyoBango;
                        DataRow[] foundRows = TaishoKamokuF3.Select(joken);

                        int i = 0;
                        // 選択した行の対象勘定科目データを取得表示
                        while (foundRows.Length > i)
                        {
                            this.lbxTaishoKanjoKamoku.Items.Add(foundRows[i].ItemArray[2] + "　" + foundRows[i].ItemArray[3]);
                            i++;
                        }
                    }
                    this.txtKamokuNm.Text = Util.ToString(this.mtbListF3[2, r.Index].Value);
                    if (Util.ToString(this.mtbListF3[4, r.Index].Value) == "借方")
                    {
                        this.rdoKari.Checked = true;
                    }
                    else if (Util.ToString(this.mtbListF3[4, r.Index].Value) == "貸方")
                    {
                        this.rdoKashi.Checked = true;
                    }
                    else
                    {
                        this.rdoKari.Checked = false;
                        this.rdoKashi.Checked = false;
                    }
                }
            }
        }

        /// <summary>
        /// GridView行の追加
        /// </summary>
        private void gridViewRowAdd()
        {
            // 大項目１番下のデータを取得
            int kamokuNo = 0;
            int gyoNum = 0;
            int indexNum = 0;
            string lastDataNm = "";
            if (tabControl.SelectedIndex == 0)
            {
                foreach (DataGridViewRow r in mtbListF1.SelectedRows)
                {
                    kamokuNo = Util.ToInt(this.mtbListF1[1, r.Index].Value);
                    gyoNum = Util.ToInt(this.mtbListF1[6, r.Index].Value);
                    lastDataNm = Util.ToString(this.mtbListF1[2, r.Index + Util.ToInt(this.mtbListF1[6, r.Index].Value)].Value);
                }
            }
            else if (tabControl.SelectedIndex == 1)
            {
                foreach (DataGridViewRow r in mtbListF2.SelectedRows)
                {
                    kamokuNo = Util.ToInt(this.mtbListF2[1, r.Index].Value);
                    gyoNum = Util.ToInt(this.mtbListF2[6, r.Index].Value);
                    lastDataNm = Util.ToString(this.mtbListF2[2, r.Index + Util.ToInt(this.mtbListF2[6, r.Index].Value)].Value);
                }
            }
            else
            {
                foreach (DataGridViewRow r in mtbListF3.SelectedRows)
                {
                    kamokuNo = Util.ToInt(this.mtbListF3[1, r.Index].Value);
                    gyoNum = Util.ToInt(this.mtbListF3[6, r.Index].Value);
                    lastDataNm = Util.ToString(this.mtbListF3[2, r.Index + Util.ToInt(this.mtbListF3[6, r.Index].Value)].Value);
                }
            }

            // 大項目１番下が空データかつ、選択行が大項目１番下でなければ、選択行上に空行を追加
            if (lastDataNm == "" && gyoNum != 0)
            {
                int kanjoKamoku;
                int gyoBango;
                string upDatejoken;
                DataRow[] foundRows;
                if (tabControl.SelectedIndex == 0)
                {
                    foreach (DataGridViewRow r in mtbListF1.SelectedRows)
                    {
                        // 行を追加
                        mtbListF1.Rows.Insert(r.Index);
                        indexNum = r.Index - 1;
                        this.mtbListF1[0, indexNum].Value = "1";
                        this.mtbListF1[1, indexNum].Value = kamokuNo;
                        this.mtbListF1[2, indexNum].Value = "";
                        this.mtbListF1[3, indexNum].Value = "";
                        this.mtbListF1[4, indexNum].Value = "";
                        this.mtbListF1[5, indexNum].Value = "";
                        this.mtbListF1[6, indexNum].Value = gyoNum;
                        // 大項目１番下の行を削除
                        mtbListF1.Rows.RemoveAt(indexNum + Util.ToInt(this.mtbListF1[6, indexNum].Value) + 1);
                        // 追加した行を選択状態にする
                        mtbListF1.Rows[indexNum].Selected = true;
                        indexNum = indexNum + 1;
                        for (int i = gyoNum - 1; i >= 0; i--)
                        {
                            kanjoKamoku = Util.ToInt(this.mtbListF1[1, indexNum + i].Value);
                            gyoBango = Util.ToInt(this.mtbListF1[3, indexNum + i].Value);
                            upDatejoken = "kamokuNo = " + kanjoKamoku + " AND gyoNo = " + gyoBango;
                            foundRows = TaishoKamokuF1.Select(upDatejoken);
                            // 選択した行の対象勘定科目データの行番号を書き換え
                            int j = 0;
                            while (foundRows.Length > j)
                            {
                                foundRows[j]["gyoNo"] = gyoNum - (i + 1);
                                j++;
                            }
                            // 追加行以下の追加用行番号とデータテーブル検索用行番号を書き換える
                            this.mtbListF1[3, indexNum + i].Value = gyoNum - (i + 1);
                            this.mtbListF1[6, indexNum + i].Value = gyoNum - (i + 1);
                        }
                    }
                }
                else if (tabControl.SelectedIndex == 1)
                {
                    foreach (DataGridViewRow r in mtbListF2.SelectedRows)
                    {
                        // 行を追加
                        mtbListF2.Rows.Insert(r.Index);
                        indexNum = r.Index - 1;
                        this.mtbListF2[0, indexNum].Value = "1";
                        this.mtbListF2[1, indexNum].Value = kamokuNo;
                        this.mtbListF2[2, indexNum].Value = "";
                        this.mtbListF2[3, indexNum].Value = "";
                        this.mtbListF2[4, indexNum].Value = "";
                        this.mtbListF2[5, indexNum].Value = "";
                        this.mtbListF2[6, indexNum].Value = gyoNum;
                        // 大項目１番下の行を削除
                        mtbListF2.Rows.RemoveAt(indexNum + Util.ToInt(this.mtbListF2[6, indexNum].Value) + 1);
                        // 追加した行を選択状態にする
                        mtbListF2.Rows[indexNum].Selected = true;
                        indexNum = indexNum + 1;
                        for (int i = gyoNum - 1; i >= 0; i--)
                        {
                            kanjoKamoku = Util.ToInt(this.mtbListF2[1, indexNum + i].Value);
                            gyoBango = Util.ToInt(this.mtbListF2[3, indexNum + i].Value);
                            upDatejoken = "kamokuNo = " + kanjoKamoku + " AND gyoNo = " + gyoBango;
                            foundRows = TaishoKamokuF2.Select(upDatejoken);
                            // 選択した行の対象勘定科目データの行番号を書き換え
                            int j = 0;
                            while (foundRows.Length > j)
                            {
                                foundRows[j]["gyoNo"] = gyoNum - (i + 1);
                                j++;
                            }
                            // 追加行以下の追加用行番号とデータテーブル検索用行番号を書き換える
                            this.mtbListF2[3, indexNum + i].Value = gyoNum - (i + 1);
                            this.mtbListF2[6, indexNum + i].Value = gyoNum - (i + 1);
                        }
                    }
                }
                else
                {
                    foreach (DataGridViewRow r in mtbListF3.SelectedRows)
                    {
                        // 行を追加
                        mtbListF3.Rows.Insert(r.Index);
                        indexNum = r.Index - 1;
                        this.mtbListF3[0, indexNum].Value = "1";
                        this.mtbListF3[1, indexNum].Value = kamokuNo;
                        this.mtbListF3[2, indexNum].Value = "";
                        this.mtbListF3[3, indexNum].Value = "";
                        this.mtbListF3[4, indexNum].Value = "";
                        this.mtbListF3[5, indexNum].Value = "";
                        this.mtbListF3[6, indexNum].Value = gyoNum;
                        // 大項目１番下の行を削除
                        mtbListF3.Rows.RemoveAt(indexNum + Util.ToInt(this.mtbListF3[6, indexNum].Value) + 1);
                        // 追加した行を選択状態にする
                        mtbListF3.Rows[indexNum].Selected = true;
                        indexNum = indexNum + 1;
                        for (int i = gyoNum - 1; i >= 0; i--)
                        {
                            kanjoKamoku = Util.ToInt(this.mtbListF3[1, indexNum + i].Value);
                            gyoBango = Util.ToInt(this.mtbListF3[3, indexNum + i].Value);
                            upDatejoken = "kamokuNo = " + kanjoKamoku + " AND gyoNo = " + gyoBango;
                            foundRows = TaishoKamokuF3.Select(upDatejoken);
                            // 選択した行の対象勘定科目データの行番号を書き換え
                            int j = 0;
                            while (foundRows.Length > j)
                            {
                                foundRows[j]["gyoNo"] = gyoNum - (i + 1);
                                j++;
                            }
                            // 追加行以下の追加用行番号とデータテーブル検索用行番号を書き換える
                            this.mtbListF3[3, indexNum + i].Value = gyoNum - (i + 1);
                            this.mtbListF3[6, indexNum + i].Value = gyoNum - (i + 1);
                        }
                    }
                }
            }
        }
        
        /// <summary>
        /// GridViewデータの登録
        /// </summary>
        /// <param name="chohyoBango">帳票番号</param>
        private void gridDataInsert(int chohyoBango)
        {
            ZMYR1011DA da = new ZMYR1011DA(this.UInfo, this.Dba, this.Config);

            int handanFlag;
            int kamokuBunrui;
            int gyoBango;
            string kamokuNm;
            int taishakuNo;
            int kakkoKubun;
            int kakkoHyoji;
            int mojiShubetsu;
            int hyojiJuni;
            int gyoBangoCount = 1;
            int allDataCount;
            DataTable upDataMotoJoho;
            DataRow[] foundRows;
            ArrayList delParams;

            DataGridView mtbList;
            DataTable taishoKamoku;

            // 対象DataGridViewをセット
            if (chohyoBango == LIST_F1)
            {
                mtbList = this.mtbListF1;
                taishoKamoku = this.TaishoKamokuF1;
            }
            else if (chohyoBango == LIST_F2)
            {
                mtbList = this.mtbListF2;
                taishoKamoku = this.TaishoKamokuF2;
            }
            else
            {
                mtbList = this.mtbListF3;
                taishoKamoku = this.TaishoKamokuF3;
            }
            allDataCount = mtbList.RowCount;

            try
            {
                this.Dba.BeginTransaction();
                for (int i = 0; allDataCount > i; i++)
                {
                    handanFlag = Util.ToInt(mtbList[0, i].Value);

                    if (handanFlag == 0)
                    {
                        #region 大項目の場合
                        kamokuBunrui = Util.ToInt(mtbList[1, i].Value);
                        kamokuNm = Util.ToString(mtbList[2, i].Value);
                        if (Util.ToString(mtbList[4, i].Value) == "借方")
                        {
                            taishakuNo = 1;
                        }
                        else
                        {
                            taishakuNo = 2;
                        }
                        hyojiJuni = Util.ToInt(Util.ToString(mtbList[7, i].Value));
                        kakkoKubun = Util.ToInt(Util.ToString(mtbList[8, i].Value));
                        kakkoHyoji = Util.ToInt(Util.ToString(mtbList[9, i].Value));
                        mojiShubetsu = Util.ToInt(Util.ToString(mtbList[10, i].Value));

                        upDataMotoJoho = da.GetUpDateMotoJoho(chohyoBango, hyojiJuni , kamokuBunrui);
                        // 入力値をバインドパラメータとしてセットする
                        delParams = SetDelParams(chohyoBango, kamokuBunrui);

                        // 入力値をバインドパラメータとしてセットする
                        ArrayList upDataWhereParams = UpDataWhereParams(chohyoBango, hyojiJuni, kamokuBunrui);
                        ArrayList upDataSetParams = UpDataSetParams(upDataMotoJoho, kamokuNm, taishakuNo, kakkoKubun
                            , kakkoHyoji, mojiShubetsu);

                        // データUpData
                        this.Dba.Update("TB_ZM_KESSANSHO_KAMOKU_BUNRUI",
                            (DbParamCollection)upDataSetParams[0],
                            "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = 0 AND CHOHYO_BUNRUI = @CHOHYO_BUNRUI AND HYOJI_JUNI = @HYOJI_JUNI AND KAMOKU_BUNRUI = @KAMOKU_BUNRUI AND KAIKEI_NENDO = @KAIKEI_NENDO",
                            (DbParamCollection)upDataWhereParams[0]);

                        // データDelete
                        this.Dba.Delete("TB_ZM_KESSANSHO_KAMOKU_SETTEI",
                            "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = 0 AND CHOHYO_BUNRUI = @CHOHYO_BUNRUI AND KAMOKU_BUNRUI = @KAMOKU_BUNRUI AND KAIKEI_NENDO = @KAIKEI_NENDO",
                            (DbParamCollection)delParams[0]);

                        gyoBangoCount = 1;
                        #endregion

                    }
                    else
                    {
                        #region 小項目の場合
                        // GridViewの行データを元に、対象勘定科目データテーブルから値を取得
                        kamokuBunrui = Util.ToInt(mtbList[1, i].Value);
                        kamokuNm = Util.ToString(mtbList[2, i].Value);
                        gyoBango = Util.ToInt(mtbList[3, i].Value);
                        if (Util.ToString(mtbList[4, i].Value) == "借方")
                        {
                            taishakuNo = 1;
                        }
                        else
                        {
                            taishakuNo = 2;
                        }
                        string joken = "kamokuNo = " + kamokuBunrui + " AND gyoNo = " + gyoBango;
                        foundRows = taishoKamoku.Select(joken);

                        int j = 0;
                        // 選択した行の対象勘定科目データを取得表示
                        while (foundRows.Length > j)
                        {
                            if (Util.ToInt(foundRows[j].ItemArray[2]) == 100100)
                            {
                                Console.WriteLine("debug");
                            }

                            // 入力値をバインドパラメータとしてセットする
                            ArrayList insertParams =
                                SetInsertParams(chohyoBango, kamokuBunrui, gyoBangoCount, Util.ToInt(foundRows[j].ItemArray[2]),
                                    kamokuNm, taishakuNo);

                            // データInsert
                            this.Dba.Insert("TB_ZM_KESSANSHO_KAMOKU_SETTEI",
                                (DbParamCollection)insertParams[0]);

                            j++;
                        }
                        gyoBangoCount++;
                        #endregion
                    }
                }

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

        }

        /// <summary>
        /// 大項目データをTB_ZM_KESSANSHO_KAMOKU_BUNRUIにUPDATA
        /// </summary>
        /// <returns>
        /// </returns>
        private ArrayList UpDataSetParams(DataTable upDataMotoJoho, string kamokuNm, int taishakuNo, int kakkoKubun, int kakkoHyoji, int mojiShubetsu)
        {
            int meisaiKamoku = Util.ToInt(upDataMotoJoho.Rows[0]["MEISAI_KOMOKUSU"]);
            int meisaiKubun = Util.ToInt(upDataMotoJoho.Rows[0]["MEISAI_KUBUN"]);
            int shukeiKubun = Util.ToInt(upDataMotoJoho.Rows[0]["SHUKEI_KUBUN"]);
            string shukeiKeisanShiki = Util.ToString(upDataMotoJoho.Rows[0]["SHUKEI_KEISANSHIKI"]);
            int bunruiKubun = Util.ToInt(upDataMotoJoho.Rows[0]["BUNRUI_KUBUN"]);

            // 帳票番号、表示順位、科目分類番号をSetパラメータに設定
            ArrayList alParams = new ArrayList();
            DbParamCollection dpcSet = new DbParamCollection();
            dpcSet.SetParam("@KAMOKU_BUNRUI_NM", SqlDbType.VarChar, 64, kamokuNm);
            dpcSet.SetParam("@TAISHAKU_KUBUN", SqlDbType.Decimal, 1, taishakuNo);
            dpcSet.SetParam("@MEISAI_KOMOKUSU", SqlDbType.Decimal, 2, meisaiKamoku);
            dpcSet.SetParam("@MEISAI_KUBUN", SqlDbType.Decimal, 1, meisaiKubun);
            dpcSet.SetParam("@SHUKEI_KUBUN", SqlDbType.Decimal, 1, shukeiKubun);
            dpcSet.SetParam("@SHUKEI_KEISANSHIKI", SqlDbType.VarChar, 250, shukeiKeisanShiki);
            dpcSet.SetParam("@BUNRUI_KUBUN", SqlDbType.Decimal, 2, bunruiKubun);
            dpcSet.SetParam("@KAKKO_KUBUN", SqlDbType.Decimal, 1, kakkoKubun);
            dpcSet.SetParam("@KAKKO_HYOJI", SqlDbType.Decimal, 1, kakkoHyoji);
            dpcSet.SetParam("@MOJI_SHUBETSU", SqlDbType.Decimal, 1, mojiShubetsu);
            dpcSet.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWDATE");

            alParams.Add(dpcSet);

            return alParams;
        }

        /// <summary>
        /// 大項目データをTB_ZM_KESSANSHO_KAMOKU_BUNRUIにUPDATA
        /// </summary>
        /// <returns>
        /// </returns>
        private ArrayList UpDataWhereParams(int chohyoBango, int hyojiJuni, int kamokuBunrui)
        {
            // 帳票番号、表示順位、科目分類番号をWhereパラメータに設定
            ArrayList alParams = new ArrayList();
            DbParamCollection dpcWhere = new DbParamCollection();
            dpcWhere.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpcWhere.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, 0);

            dpcWhere.SetParam("@CHOHYO_BUNRUI", SqlDbType.Decimal, 2, chohyoBango);
            dpcWhere.SetParam("@HYOJI_JUNI", SqlDbType.Decimal, 4, hyojiJuni);
            dpcWhere.SetParam("@KAMOKU_BUNRUI", SqlDbType.Decimal, 5, kamokuBunrui);
            dpcWhere.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);

            alParams.Add(dpcWhere);

            return alParams;
        }

        /// <summary>
        /// TB_ZM_KESSANSHO_KAMOKU_SETTEIからデータを削除
        /// </summary>
        /// <returns>
        /// </returns>
        private ArrayList SetDelParams(int chohyoBunrui, int kamokuBunrui)
        {
            // 帳票分類コード、科目分類コード、会計年度を削除パラメータに設定
            ArrayList alParams = new ArrayList();
            DbParamCollection dpcDel = new DbParamCollection();
            dpcDel.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpcDel.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, 0);

            dpcDel.SetParam("@CHOHYO_BUNRUI", SqlDbType.Decimal, 2, chohyoBunrui);
            dpcDel.SetParam("@KAMOKU_BUNRUI", SqlDbType.Decimal, 5, kamokuBunrui);
            dpcDel.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);

            alParams.Add(dpcDel);

            return alParams;
        }

        /// <summary>
        /// TB_ZM_KESSANSHO_KAMOKU_SETTEIにデータをインサート
        /// </summary>
        /// <returns>
        /// </returns>
        private ArrayList SetInsertParams(int chohyoBunrui, int kamokuBunrui, int gyoBango, int kanjoKamokuCd, string kanjoKamokuNm, int taishakuKubun)
        {
            // 帳票分類コード、科目分類コード、会計年度を削除パラメータに設定
            ArrayList alParams = new ArrayList();
            DbParamCollection dpcInsert = new DbParamCollection();
            dpcInsert.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpcInsert.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, 0);

            dpcInsert.SetParam("@CHOHYO_BUNRUI", SqlDbType.Decimal, 2, chohyoBunrui);
            dpcInsert.SetParam("@KAMOKU_BUNRUI", SqlDbType.Decimal, 5, kamokuBunrui);
            dpcInsert.SetParam("@GYO_BANGO", SqlDbType.Decimal, 2, gyoBango);
            dpcInsert.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, kanjoKamokuCd);
            dpcInsert.SetParam("@KANJO_KAMOKU_NM", SqlDbType.VarChar, 64, kanjoKamokuNm);
            dpcInsert.SetParam("@TAISHAKU_KUBUN", SqlDbType.Decimal, 1, taishakuKubun);
            dpcInsert.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpcInsert.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWDATE");

            alParams.Add(dpcInsert);

            return alParams;
        }

        #endregion

    }
}
