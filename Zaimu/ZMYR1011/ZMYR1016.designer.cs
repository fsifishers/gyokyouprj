﻿namespace jp.co.fsi.zm.zmyr1011
{
    partial class ZMYR1016
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlPage1 = new jp.co.fsi.common.FsiPanel();
            this.txtRiekiShobunanChuki20 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtRiekiShobunanChuki19 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtRiekiShobunanChuki18 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtRiekiShobunanChuki17 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtRiekiShobunanChuki16 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtRiekiShobunanChuki15 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtRiekiShobunanChuki14 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtRiekiShobunanChuki13 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtRiekiShobunanChuki12 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtRiekiShobunanChuki11 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtRiekiShobunanChuki10 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtRiekiShobunanChuki9 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtRiekiShobunanChuki8 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtRiekiShobunanChuki7 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtRiekiShobunanChuki6 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtRiekiShobunanChuki5 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtRiekiShobunanChuki4 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtRiekiShobunanChuki3 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtRiekiShobunanChuki2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtRiekiShobunanChuki1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.pnlDebug.SuspendLayout();
            this.pnlPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.BackColor = System.Drawing.Color.White;
            this.lblTitle.Location = new System.Drawing.Point(4, 9);
            this.lblTitle.Size = new System.Drawing.Size(651, 23);
            this.lblTitle.Text = "";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(5, 395);
            this.pnlDebug.Size = new System.Drawing.Size(668, 100);
            // 
            // pnlPage1
            // 
            this.pnlPage1.BackColor = System.Drawing.Color.Transparent;
            this.pnlPage1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlPage1.Controls.Add(this.txtRiekiShobunanChuki20);
            this.pnlPage1.Controls.Add(this.txtRiekiShobunanChuki19);
            this.pnlPage1.Controls.Add(this.txtRiekiShobunanChuki18);
            this.pnlPage1.Controls.Add(this.txtRiekiShobunanChuki17);
            this.pnlPage1.Controls.Add(this.txtRiekiShobunanChuki16);
            this.pnlPage1.Controls.Add(this.txtRiekiShobunanChuki15);
            this.pnlPage1.Controls.Add(this.txtRiekiShobunanChuki14);
            this.pnlPage1.Controls.Add(this.txtRiekiShobunanChuki13);
            this.pnlPage1.Controls.Add(this.txtRiekiShobunanChuki12);
            this.pnlPage1.Controls.Add(this.txtRiekiShobunanChuki11);
            this.pnlPage1.Controls.Add(this.txtRiekiShobunanChuki10);
            this.pnlPage1.Controls.Add(this.txtRiekiShobunanChuki9);
            this.pnlPage1.Controls.Add(this.txtRiekiShobunanChuki8);
            this.pnlPage1.Controls.Add(this.txtRiekiShobunanChuki7);
            this.pnlPage1.Controls.Add(this.txtRiekiShobunanChuki6);
            this.pnlPage1.Controls.Add(this.txtRiekiShobunanChuki5);
            this.pnlPage1.Controls.Add(this.txtRiekiShobunanChuki4);
            this.pnlPage1.Controls.Add(this.txtRiekiShobunanChuki3);
            this.pnlPage1.Controls.Add(this.txtRiekiShobunanChuki2);
            this.pnlPage1.Controls.Add(this.txtRiekiShobunanChuki1);
            this.pnlPage1.Location = new System.Drawing.Point(24, 23);
            this.pnlPage1.Name = "pnlPage1";
            this.pnlPage1.Size = new System.Drawing.Size(625, 410);
            this.pnlPage1.TabIndex = 903;
            // 
            // txtRiekiShobunanChuki20
            // 
            this.txtRiekiShobunanChuki20.AutoSizeFromLength = false;
            this.txtRiekiShobunanChuki20.DisplayLength = null;
            this.txtRiekiShobunanChuki20.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtRiekiShobunanChuki20.ForeColor = System.Drawing.Color.Black;
            this.txtRiekiShobunanChuki20.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtRiekiShobunanChuki20.Location = new System.Drawing.Point(3, 383);
            this.txtRiekiShobunanChuki20.MaxLength = 128;
            this.txtRiekiShobunanChuki20.Name = "txtRiekiShobunanChuki20";
            this.txtRiekiShobunanChuki20.Size = new System.Drawing.Size(615, 20);
            this.txtRiekiShobunanChuki20.TabIndex = 20;
            this.txtRiekiShobunanChuki20.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtRiekiShobunanChuki19
            // 
            this.txtRiekiShobunanChuki19.AutoSizeFromLength = false;
            this.txtRiekiShobunanChuki19.DisplayLength = null;
            this.txtRiekiShobunanChuki19.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtRiekiShobunanChuki19.ForeColor = System.Drawing.Color.Black;
            this.txtRiekiShobunanChuki19.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtRiekiShobunanChuki19.Location = new System.Drawing.Point(3, 363);
            this.txtRiekiShobunanChuki19.MaxLength = 128;
            this.txtRiekiShobunanChuki19.Name = "txtRiekiShobunanChuki19";
            this.txtRiekiShobunanChuki19.Size = new System.Drawing.Size(615, 20);
            this.txtRiekiShobunanChuki19.TabIndex = 19;
            this.txtRiekiShobunanChuki19.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtRiekiShobunanChuki18
            // 
            this.txtRiekiShobunanChuki18.AutoSizeFromLength = false;
            this.txtRiekiShobunanChuki18.DisplayLength = null;
            this.txtRiekiShobunanChuki18.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtRiekiShobunanChuki18.ForeColor = System.Drawing.Color.Black;
            this.txtRiekiShobunanChuki18.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtRiekiShobunanChuki18.Location = new System.Drawing.Point(3, 343);
            this.txtRiekiShobunanChuki18.MaxLength = 128;
            this.txtRiekiShobunanChuki18.Name = "txtRiekiShobunanChuki18";
            this.txtRiekiShobunanChuki18.Size = new System.Drawing.Size(615, 20);
            this.txtRiekiShobunanChuki18.TabIndex = 18;
            this.txtRiekiShobunanChuki18.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtRiekiShobunanChuki17
            // 
            this.txtRiekiShobunanChuki17.AutoSizeFromLength = false;
            this.txtRiekiShobunanChuki17.DisplayLength = null;
            this.txtRiekiShobunanChuki17.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtRiekiShobunanChuki17.ForeColor = System.Drawing.Color.Black;
            this.txtRiekiShobunanChuki17.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtRiekiShobunanChuki17.Location = new System.Drawing.Point(3, 323);
            this.txtRiekiShobunanChuki17.MaxLength = 128;
            this.txtRiekiShobunanChuki17.Name = "txtRiekiShobunanChuki17";
            this.txtRiekiShobunanChuki17.Size = new System.Drawing.Size(615, 20);
            this.txtRiekiShobunanChuki17.TabIndex = 17;
            this.txtRiekiShobunanChuki17.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtRiekiShobunanChuki16
            // 
            this.txtRiekiShobunanChuki16.AutoSizeFromLength = false;
            this.txtRiekiShobunanChuki16.DisplayLength = null;
            this.txtRiekiShobunanChuki16.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtRiekiShobunanChuki16.ForeColor = System.Drawing.Color.Black;
            this.txtRiekiShobunanChuki16.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtRiekiShobunanChuki16.Location = new System.Drawing.Point(3, 303);
            this.txtRiekiShobunanChuki16.MaxLength = 128;
            this.txtRiekiShobunanChuki16.Name = "txtRiekiShobunanChuki16";
            this.txtRiekiShobunanChuki16.Size = new System.Drawing.Size(615, 20);
            this.txtRiekiShobunanChuki16.TabIndex = 16;
            this.txtRiekiShobunanChuki16.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtRiekiShobunanChuki15
            // 
            this.txtRiekiShobunanChuki15.AutoSizeFromLength = false;
            this.txtRiekiShobunanChuki15.DisplayLength = null;
            this.txtRiekiShobunanChuki15.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtRiekiShobunanChuki15.ForeColor = System.Drawing.Color.Black;
            this.txtRiekiShobunanChuki15.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtRiekiShobunanChuki15.Location = new System.Drawing.Point(3, 283);
            this.txtRiekiShobunanChuki15.MaxLength = 128;
            this.txtRiekiShobunanChuki15.Name = "txtRiekiShobunanChuki15";
            this.txtRiekiShobunanChuki15.Size = new System.Drawing.Size(615, 20);
            this.txtRiekiShobunanChuki15.TabIndex = 15;
            this.txtRiekiShobunanChuki15.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtRiekiShobunanChuki14
            // 
            this.txtRiekiShobunanChuki14.AutoSizeFromLength = false;
            this.txtRiekiShobunanChuki14.DisplayLength = null;
            this.txtRiekiShobunanChuki14.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtRiekiShobunanChuki14.ForeColor = System.Drawing.Color.Black;
            this.txtRiekiShobunanChuki14.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtRiekiShobunanChuki14.Location = new System.Drawing.Point(3, 263);
            this.txtRiekiShobunanChuki14.MaxLength = 128;
            this.txtRiekiShobunanChuki14.Name = "txtRiekiShobunanChuki14";
            this.txtRiekiShobunanChuki14.Size = new System.Drawing.Size(615, 20);
            this.txtRiekiShobunanChuki14.TabIndex = 14;
            this.txtRiekiShobunanChuki14.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtRiekiShobunanChuki13
            // 
            this.txtRiekiShobunanChuki13.AutoSizeFromLength = false;
            this.txtRiekiShobunanChuki13.DisplayLength = null;
            this.txtRiekiShobunanChuki13.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtRiekiShobunanChuki13.ForeColor = System.Drawing.Color.Black;
            this.txtRiekiShobunanChuki13.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtRiekiShobunanChuki13.Location = new System.Drawing.Point(3, 243);
            this.txtRiekiShobunanChuki13.MaxLength = 128;
            this.txtRiekiShobunanChuki13.Name = "txtRiekiShobunanChuki13";
            this.txtRiekiShobunanChuki13.Size = new System.Drawing.Size(615, 20);
            this.txtRiekiShobunanChuki13.TabIndex = 13;
            this.txtRiekiShobunanChuki13.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtRiekiShobunanChuki12
            // 
            this.txtRiekiShobunanChuki12.AutoSizeFromLength = false;
            this.txtRiekiShobunanChuki12.DisplayLength = null;
            this.txtRiekiShobunanChuki12.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtRiekiShobunanChuki12.ForeColor = System.Drawing.Color.Black;
            this.txtRiekiShobunanChuki12.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtRiekiShobunanChuki12.Location = new System.Drawing.Point(3, 223);
            this.txtRiekiShobunanChuki12.MaxLength = 128;
            this.txtRiekiShobunanChuki12.Name = "txtRiekiShobunanChuki12";
            this.txtRiekiShobunanChuki12.Size = new System.Drawing.Size(615, 20);
            this.txtRiekiShobunanChuki12.TabIndex = 12;
            this.txtRiekiShobunanChuki12.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtRiekiShobunanChuki11
            // 
            this.txtRiekiShobunanChuki11.AutoSizeFromLength = false;
            this.txtRiekiShobunanChuki11.DisplayLength = null;
            this.txtRiekiShobunanChuki11.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtRiekiShobunanChuki11.ForeColor = System.Drawing.Color.Black;
            this.txtRiekiShobunanChuki11.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtRiekiShobunanChuki11.Location = new System.Drawing.Point(3, 203);
            this.txtRiekiShobunanChuki11.MaxLength = 128;
            this.txtRiekiShobunanChuki11.Name = "txtRiekiShobunanChuki11";
            this.txtRiekiShobunanChuki11.Size = new System.Drawing.Size(615, 20);
            this.txtRiekiShobunanChuki11.TabIndex = 11;
            this.txtRiekiShobunanChuki11.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtRiekiShobunanChuki10
            // 
            this.txtRiekiShobunanChuki10.AutoSizeFromLength = false;
            this.txtRiekiShobunanChuki10.DisplayLength = null;
            this.txtRiekiShobunanChuki10.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtRiekiShobunanChuki10.ForeColor = System.Drawing.Color.Black;
            this.txtRiekiShobunanChuki10.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtRiekiShobunanChuki10.Location = new System.Drawing.Point(3, 183);
            this.txtRiekiShobunanChuki10.MaxLength = 128;
            this.txtRiekiShobunanChuki10.Name = "txtRiekiShobunanChuki10";
            this.txtRiekiShobunanChuki10.Size = new System.Drawing.Size(615, 20);
            this.txtRiekiShobunanChuki10.TabIndex = 10;
            this.txtRiekiShobunanChuki10.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtRiekiShobunanChuki9
            // 
            this.txtRiekiShobunanChuki9.AutoSizeFromLength = false;
            this.txtRiekiShobunanChuki9.DisplayLength = null;
            this.txtRiekiShobunanChuki9.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtRiekiShobunanChuki9.ForeColor = System.Drawing.Color.Black;
            this.txtRiekiShobunanChuki9.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtRiekiShobunanChuki9.Location = new System.Drawing.Point(3, 163);
            this.txtRiekiShobunanChuki9.MaxLength = 128;
            this.txtRiekiShobunanChuki9.Name = "txtRiekiShobunanChuki9";
            this.txtRiekiShobunanChuki9.Size = new System.Drawing.Size(615, 20);
            this.txtRiekiShobunanChuki9.TabIndex = 9;
            this.txtRiekiShobunanChuki9.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtRiekiShobunanChuki8
            // 
            this.txtRiekiShobunanChuki8.AutoSizeFromLength = false;
            this.txtRiekiShobunanChuki8.DisplayLength = null;
            this.txtRiekiShobunanChuki8.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtRiekiShobunanChuki8.ForeColor = System.Drawing.Color.Black;
            this.txtRiekiShobunanChuki8.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtRiekiShobunanChuki8.Location = new System.Drawing.Point(3, 143);
            this.txtRiekiShobunanChuki8.MaxLength = 128;
            this.txtRiekiShobunanChuki8.Name = "txtRiekiShobunanChuki8";
            this.txtRiekiShobunanChuki8.Size = new System.Drawing.Size(615, 20);
            this.txtRiekiShobunanChuki8.TabIndex = 8;
            this.txtRiekiShobunanChuki8.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtRiekiShobunanChuki7
            // 
            this.txtRiekiShobunanChuki7.AutoSizeFromLength = false;
            this.txtRiekiShobunanChuki7.DisplayLength = null;
            this.txtRiekiShobunanChuki7.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtRiekiShobunanChuki7.ForeColor = System.Drawing.Color.Black;
            this.txtRiekiShobunanChuki7.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtRiekiShobunanChuki7.Location = new System.Drawing.Point(3, 123);
            this.txtRiekiShobunanChuki7.MaxLength = 128;
            this.txtRiekiShobunanChuki7.Name = "txtRiekiShobunanChuki7";
            this.txtRiekiShobunanChuki7.Size = new System.Drawing.Size(615, 20);
            this.txtRiekiShobunanChuki7.TabIndex = 7;
            this.txtRiekiShobunanChuki7.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtRiekiShobunanChuki6
            // 
            this.txtRiekiShobunanChuki6.AutoSizeFromLength = false;
            this.txtRiekiShobunanChuki6.DisplayLength = null;
            this.txtRiekiShobunanChuki6.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtRiekiShobunanChuki6.ForeColor = System.Drawing.Color.Black;
            this.txtRiekiShobunanChuki6.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtRiekiShobunanChuki6.Location = new System.Drawing.Point(3, 103);
            this.txtRiekiShobunanChuki6.MaxLength = 128;
            this.txtRiekiShobunanChuki6.Name = "txtRiekiShobunanChuki6";
            this.txtRiekiShobunanChuki6.Size = new System.Drawing.Size(615, 20);
            this.txtRiekiShobunanChuki6.TabIndex = 6;
            this.txtRiekiShobunanChuki6.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtRiekiShobunanChuki5
            // 
            this.txtRiekiShobunanChuki5.AutoSizeFromLength = false;
            this.txtRiekiShobunanChuki5.DisplayLength = null;
            this.txtRiekiShobunanChuki5.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtRiekiShobunanChuki5.ForeColor = System.Drawing.Color.Black;
            this.txtRiekiShobunanChuki5.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtRiekiShobunanChuki5.Location = new System.Drawing.Point(3, 83);
            this.txtRiekiShobunanChuki5.MaxLength = 128;
            this.txtRiekiShobunanChuki5.Name = "txtRiekiShobunanChuki5";
            this.txtRiekiShobunanChuki5.Size = new System.Drawing.Size(615, 20);
            this.txtRiekiShobunanChuki5.TabIndex = 5;
            this.txtRiekiShobunanChuki5.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtRiekiShobunanChuki4
            // 
            this.txtRiekiShobunanChuki4.AutoSizeFromLength = false;
            this.txtRiekiShobunanChuki4.DisplayLength = null;
            this.txtRiekiShobunanChuki4.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtRiekiShobunanChuki4.ForeColor = System.Drawing.Color.Black;
            this.txtRiekiShobunanChuki4.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtRiekiShobunanChuki4.Location = new System.Drawing.Point(3, 63);
            this.txtRiekiShobunanChuki4.MaxLength = 128;
            this.txtRiekiShobunanChuki4.Name = "txtRiekiShobunanChuki4";
            this.txtRiekiShobunanChuki4.Size = new System.Drawing.Size(615, 20);
            this.txtRiekiShobunanChuki4.TabIndex = 4;
            this.txtRiekiShobunanChuki4.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtRiekiShobunanChuki3
            // 
            this.txtRiekiShobunanChuki3.AutoSizeFromLength = false;
            this.txtRiekiShobunanChuki3.DisplayLength = null;
            this.txtRiekiShobunanChuki3.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtRiekiShobunanChuki3.ForeColor = System.Drawing.Color.Black;
            this.txtRiekiShobunanChuki3.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtRiekiShobunanChuki3.Location = new System.Drawing.Point(3, 43);
            this.txtRiekiShobunanChuki3.MaxLength = 128;
            this.txtRiekiShobunanChuki3.Name = "txtRiekiShobunanChuki3";
            this.txtRiekiShobunanChuki3.Size = new System.Drawing.Size(615, 20);
            this.txtRiekiShobunanChuki3.TabIndex = 3;
            this.txtRiekiShobunanChuki3.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtRiekiShobunanChuki2
            // 
            this.txtRiekiShobunanChuki2.AutoSizeFromLength = false;
            this.txtRiekiShobunanChuki2.DisplayLength = null;
            this.txtRiekiShobunanChuki2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtRiekiShobunanChuki2.ForeColor = System.Drawing.Color.Black;
            this.txtRiekiShobunanChuki2.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtRiekiShobunanChuki2.Location = new System.Drawing.Point(3, 23);
            this.txtRiekiShobunanChuki2.MaxLength = 128;
            this.txtRiekiShobunanChuki2.Name = "txtRiekiShobunanChuki2";
            this.txtRiekiShobunanChuki2.Size = new System.Drawing.Size(615, 20);
            this.txtRiekiShobunanChuki2.TabIndex = 2;
            this.txtRiekiShobunanChuki2.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // txtRiekiShobunanChuki1
            // 
            this.txtRiekiShobunanChuki1.AutoSizeFromLength = false;
            this.txtRiekiShobunanChuki1.DisplayLength = null;
            this.txtRiekiShobunanChuki1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtRiekiShobunanChuki1.ForeColor = System.Drawing.Color.Black;
            this.txtRiekiShobunanChuki1.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtRiekiShobunanChuki1.Location = new System.Drawing.Point(3, 3);
            this.txtRiekiShobunanChuki1.MaxLength = 128;
            this.txtRiekiShobunanChuki1.Name = "txtRiekiShobunanChuki1";
            this.txtRiekiShobunanChuki1.Size = new System.Drawing.Size(615, 20);
            this.txtRiekiShobunanChuki1.TabIndex = 1;
            this.txtRiekiShobunanChuki1.Validating += new System.ComponentModel.CancelEventHandler(this.txtChuki_Validating);
            // 
            // ZMYR1016
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(660, 498);
            this.Controls.Add(this.pnlPage1);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "ZMYR1016";
            this.ShowFButton = true;
            this.Text = "";
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.pnlPage1, 0);
            this.pnlDebug.ResumeLayout(false);
            this.pnlPage1.ResumeLayout(false);
            this.pnlPage1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private jp.co.fsi.common.FsiPanel pnlPage1;
        private jp.co.fsi.common.controls.FsiTextBox txtRiekiShobunanChuki20;
        private jp.co.fsi.common.controls.FsiTextBox txtRiekiShobunanChuki19;
        private jp.co.fsi.common.controls.FsiTextBox txtRiekiShobunanChuki18;
        private jp.co.fsi.common.controls.FsiTextBox txtRiekiShobunanChuki17;
        private jp.co.fsi.common.controls.FsiTextBox txtRiekiShobunanChuki16;
        private jp.co.fsi.common.controls.FsiTextBox txtRiekiShobunanChuki15;
        private jp.co.fsi.common.controls.FsiTextBox txtRiekiShobunanChuki14;
        private jp.co.fsi.common.controls.FsiTextBox txtRiekiShobunanChuki13;
        private jp.co.fsi.common.controls.FsiTextBox txtRiekiShobunanChuki12;
        private jp.co.fsi.common.controls.FsiTextBox txtRiekiShobunanChuki11;
        private jp.co.fsi.common.controls.FsiTextBox txtRiekiShobunanChuki10;
        private jp.co.fsi.common.controls.FsiTextBox txtRiekiShobunanChuki9;
        private jp.co.fsi.common.controls.FsiTextBox txtRiekiShobunanChuki8;
        private jp.co.fsi.common.controls.FsiTextBox txtRiekiShobunanChuki7;
        private jp.co.fsi.common.controls.FsiTextBox txtRiekiShobunanChuki6;
        private jp.co.fsi.common.controls.FsiTextBox txtRiekiShobunanChuki5;
        private jp.co.fsi.common.controls.FsiTextBox txtRiekiShobunanChuki4;
        private jp.co.fsi.common.controls.FsiTextBox txtRiekiShobunanChuki3;
        private jp.co.fsi.common.controls.FsiTextBox txtRiekiShobunanChuki2;
        private jp.co.fsi.common.controls.FsiTextBox txtRiekiShobunanChuki1;





    }
}