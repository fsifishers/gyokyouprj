﻿namespace jp.co.fsi.zm.zmmr1071
{
    /// <summary>
    /// ZMMR10711R の概要の説明です。
    /// </summary>
    partial class ZMMR107111R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(ZMMR107111R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.textBox6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtToday = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCompanyName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtHyojiDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtPageCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.reportInfo1 = new GrapeCity.ActiveReports.SectionReportModel.ReportInfo();
            this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtTitle02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.shape1 = new GrapeCity.ActiveReports.SectionReportModel.Shape();
            this.txtIrisu = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSouko = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtShohinNm = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtKikaku = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtShohinCd = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.textBox7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.reportHeader1 = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.reportFooter1 = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.groupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.groupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHyojiDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportInfo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIrisu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSouko)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShohinNm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKikaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShohinCd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.textBox6,
            this.txtToday,
            this.txtCompanyName,
            this.label1,
            this.txtHyojiDate,
            this.label3,
            this.txtPageCount,
            this.reportInfo1,
            this.textBox1,
            this.textBox2,
            this.label2,
            this.label4,
            this.label5,
            this.txtTitle02,
            this.txtTitle03,
            this.textBox3,
            this.textBox4,
            this.textBox5,
            this.line3,
            this.line4,
            this.line7,
            this.line1,
            this.line9,
            this.line6,
            this.line10,
            this.line5,
            this.line8});
            this.pageHeader.Height = 0.8858268F;
            this.pageHeader.Name = "pageHeader";
            this.pageHeader.Format += new System.EventHandler(this.pageHeader_Format);
            // 
            // textBox6
            // 
            this.textBox6.DataField = "ITEM09";
            this.textBox6.Height = 0.2421261F;
            this.textBox6.Left = 5.31496F;
            this.textBox6.MultiLine = false;
            this.textBox6.Name = "textBox6";
            this.textBox6.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold;" +
    " text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox6.Text = "9 2016 年度";
            this.textBox6.Top = 0.6425197F;
            this.textBox6.Width = 1.023622F;
            // 
            // txtToday
            // 
            this.txtToday.DataField = "ITEM17";
            this.txtToday.Height = 0.196752F;
            this.txtToday.Left = 5.314961F;
            this.txtToday.Name = "txtToday";
            this.txtToday.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtToday.Text = "txtToday";
            this.txtToday.Top = 0.04881889F;
            this.txtToday.Width = 2.012205F;
            // 
            // txtCompanyName
            // 
            this.txtCompanyName.Height = 0.1968504F;
            this.txtCompanyName.Left = 2.592914F;
            this.txtCompanyName.MultiLine = false;
            this.txtCompanyName.Name = "txtCompanyName";
            this.txtCompanyName.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.txtCompanyName.Text = null;
            this.txtCompanyName.Top = 0.1980315F;
            this.txtCompanyName.Width = 1.149213F;
            // 
            // label1
            // 
            this.label1.DataField = "ITEM02";
            this.label1.Height = 0.2F;
            this.label1.HyperLink = null;
            this.label1.Left = 0.01574803F;
            this.label1.Name = "label1";
            this.label1.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; text-justify: auto; vert" +
    "ical-align: bottom; ddo-char-set: 1";
            this.label1.Text = "02【全社】";
            this.label1.Top = 0.2456693F;
            this.label1.Width = 1.940158F;
            // 
            // txtHyojiDate
            // 
            this.txtHyojiDate.DataField = "ITEM04";
            this.txtHyojiDate.Height = 0.2F;
            this.txtHyojiDate.Left = 0.01574803F;
            this.txtHyojiDate.Name = "txtHyojiDate";
            this.txtHyojiDate.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: left; ver" +
    "tical-align: bottom; ddo-char-set: 1";
            this.txtHyojiDate.Text = "4";
            this.txtHyojiDate.Top = 0.4456693F;
            this.txtHyojiDate.Width = 2.477953F;
            // 
            // label3
            // 
            this.label3.Height = 0.2F;
            this.label3.HyperLink = null;
            this.label3.Left = 7.105906F;
            this.label3.Name = "label3";
            this.label3.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.label3.Text = "頁";
            this.label3.Top = 0.2456693F;
            this.label3.Width = 0.2212596F;
            // 
            // txtPageCount
            // 
            this.txtPageCount.Height = 0.2F;
            this.txtPageCount.Left = 6.862599F;
            this.txtPageCount.MultiLine = false;
            this.txtPageCount.Name = "txtPageCount";
            this.txtPageCount.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.txtPageCount.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtPageCount.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.txtPageCount.Text = "999";
            this.txtPageCount.Top = 0.2456693F;
            this.txtPageCount.Width = 0.2433071F;
            // 
            // reportInfo1
            // 
            this.reportInfo1.FormatString = "{RunDateTime:yyyy/MM/dd}";
            this.reportInfo1.Height = 0.2F;
            this.reportInfo1.Left = 2.96063F;
            this.reportInfo1.Name = "reportInfo1";
            this.reportInfo1.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; text-align: right; ve" +
    "rtical-align: top; ddo-char-set: 1";
            this.reportInfo1.Top = 0.4539371F;
            this.reportInfo1.Visible = false;
            this.reportInfo1.Width = 2.110237F;
            // 
            // textBox1
            // 
            this.textBox1.DataField = "ITEM05";
            this.textBox1.Height = 0.2F;
            this.textBox1.Left = 6.625985F;
            this.textBox1.MultiLine = false;
            this.textBox1.Name = "textBox1";
            this.textBox1.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: normal; vertical-align: middl" +
    "e; ddo-char-set: 1";
            this.textBox1.Text = "05【税込み】";
            this.textBox1.Top = 0.4456693F;
            this.textBox1.Width = 0.8580713F;
            // 
            // textBox2
            // 
            this.textBox2.DataField = "ITEM03";
            this.textBox2.Height = 0.1968504F;
            this.textBox2.Left = 3.921654F;
            this.textBox2.MultiLine = false;
            this.textBox2.Name = "textBox2";
            this.textBox2.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.textBox2.Text = "03平成27年12月20日";
            this.textBox2.Top = 0.2011811F;
            this.textBox2.Width = 1.366929F;
            // 
            // label2
            // 
            this.label2.Height = 0.2F;
            this.label2.HyperLink = null;
            this.label2.Left = 2.434253F;
            this.label2.Name = "label2";
            this.label2.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.label2.Text = "自";
            this.label2.Top = 0.1937008F;
            this.label2.Visible = false;
            this.label2.Width = 0.1586607F;
            // 
            // label4
            // 
            this.label4.Height = 0.2F;
            this.label4.HyperLink = null;
            this.label4.Left = 3.742127F;
            this.label4.Name = "label4";
            this.label4.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.label4.Text = "至";
            this.label4.Top = 0.1980315F;
            this.label4.Visible = false;
            this.label4.Width = 0.1795263F;
            // 
            // label5
            // 
            this.label5.DataField = "ITEM01";
            this.label5.Height = 0.1979167F;
            this.label5.HyperLink = null;
            this.label5.Left = 2.278347F;
            this.label5.Name = "label5";
            this.label5.Style = "font-family: ＭＳ 明朝; font-size: 14.25pt; font-weight: bold; text-align: center; ve" +
    "rtical-align: middle; ddo-char-set: 1";
            this.label5.Text = " 01五期比較諸表　　 ";
            this.label5.Top = 0F;
            this.label5.Width = 3.010237F;
            // 
            // txtTitle02
            // 
            this.txtTitle02.Height = 0.2421261F;
            this.txtTitle02.Left = 0F;
            this.txtTitle02.MultiLine = false;
            this.txtTitle02.Name = "txtTitle02";
            this.txtTitle02.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold;" +
    " text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.txtTitle02.Text = "科  目  名";
            this.txtTitle02.Top = 0.6425197F;
            this.txtTitle02.Width = 2.361418F;
            // 
            // txtTitle03
            // 
            this.txtTitle03.DataField = "ITEM06";
            this.txtTitle03.Height = 0.242126F;
            this.txtTitle03.Left = 2.362205F;
            this.txtTitle03.MultiLine = false;
            this.txtTitle03.Name = "txtTitle03";
            this.txtTitle03.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold;" +
    " text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.txtTitle03.Text = "6 2013 年度";
            this.txtTitle03.Top = 0.6425197F;
            this.txtTitle03.Width = 0.984252F;
            // 
            // textBox3
            // 
            this.textBox3.DataField = "ITEM08";
            this.textBox3.Height = 0.242126F;
            this.textBox3.Left = 4.330709F;
            this.textBox3.MultiLine = false;
            this.textBox3.Name = "textBox3";
            this.textBox3.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold;" +
    " text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox3.Text = "8 2015 年度";
            this.textBox3.Top = 0.6425197F;
            this.textBox3.Width = 0.984252F;
            // 
            // textBox4
            // 
            this.textBox4.Height = 0.2421261F;
            this.textBox4.Left = 6.299212F;
            this.textBox4.MultiLine = false;
            this.textBox4.Name = "textBox4";
            this.textBox4.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold;" +
    " text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox4.Text = "当   期";
            this.textBox4.Top = 0.6425197F;
            this.textBox4.Width = 1.172047F;
            // 
            // textBox5
            // 
            this.textBox5.DataField = "ITEM07";
            this.textBox5.Height = 0.242126F;
            this.textBox5.Left = 3.346457F;
            this.textBox5.MultiLine = false;
            this.textBox5.Name = "textBox5";
            this.textBox5.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold;" +
    " text-align: center; vertical-align: bottom; ddo-char-set: 1";
            this.textBox5.Text = "7 2014 年度";
            this.textBox5.Top = 0.6425197F;
            this.textBox5.Width = 0.984252F;
            // 
            // line3
            // 
            this.line3.Height = 0.2421261F;
            this.line3.Left = 3.346457F;
            this.line3.LineWeight = 2F;
            this.line3.Name = "line3";
            this.line3.Top = 0.6425197F;
            this.line3.Width = 0F;
            this.line3.X1 = 3.346457F;
            this.line3.X2 = 3.346457F;
            this.line3.Y1 = 0.6425197F;
            this.line3.Y2 = 0.8846458F;
            // 
            // line4
            // 
            this.line4.Height = 0.2421261F;
            this.line4.Left = 4.330709F;
            this.line4.LineWeight = 2F;
            this.line4.Name = "line4";
            this.line4.Top = 0.653937F;
            this.line4.Width = 0F;
            this.line4.X1 = 4.330709F;
            this.line4.X2 = 4.330709F;
            this.line4.Y1 = 0.653937F;
            this.line4.Y2 = 0.8960631F;
            // 
            // line7
            // 
            this.line7.Height = 0.2421261F;
            this.line7.Left = 2.362205F;
            this.line7.LineWeight = 2F;
            this.line7.Name = "line7";
            this.line7.Top = 0.653937F;
            this.line7.Width = 0F;
            this.line7.X1 = 2.362205F;
            this.line7.X2 = 2.362205F;
            this.line7.Y1 = 0.653937F;
            this.line7.Y2 = 0.8960631F;
            // 
            // line1
            // 
            this.line1.Height = 0F;
            this.line1.Left = 0F;
            this.line1.LineWeight = 2F;
            this.line1.Name = "line1";
            this.line1.Top = 0.6456693F;
            this.line1.Width = 7.48622F;
            this.line1.X1 = 0F;
            this.line1.X2 = 7.48622F;
            this.line1.Y1 = 0.6456693F;
            this.line1.Y2 = 0.6456693F;
            // 
            // line9
            // 
            this.line9.Height = 1.072884E-06F;
            this.line9.Left = -1.862645E-09F;
            this.line9.LineWeight = 2F;
            this.line9.Name = "line9";
            this.line9.Top = 0.8877953F;
            this.line9.Width = 7.483859F;
            this.line9.X1 = -1.862645E-09F;
            this.line9.X2 = 7.483859F;
            this.line9.Y1 = 0.8877953F;
            this.line9.Y2 = 0.8877964F;
            // 
            // line6
            // 
            this.line6.Height = 0.242126F;
            this.line6.Left = 7.484252F;
            this.line6.LineWeight = 2F;
            this.line6.Name = "line6";
            this.line6.Top = 0.6456693F;
            this.line6.Width = 0F;
            this.line6.X1 = 7.484252F;
            this.line6.X2 = 7.484252F;
            this.line6.Y1 = 0.6456693F;
            this.line6.Y2 = 0.8877953F;
            // 
            // line10
            // 
            this.line10.Height = 0.242126F;
            this.line10.Left = 0.003937008F;
            this.line10.LineWeight = 2F;
            this.line10.Name = "line10";
            this.line10.Top = 0.6456693F;
            this.line10.Width = 0F;
            this.line10.X1 = 0.003937008F;
            this.line10.X2 = 0.003937008F;
            this.line10.Y1 = 0.6456693F;
            this.line10.Y2 = 0.8877953F;
            // 
            // line5
            // 
            this.line5.Height = 0.242126F;
            this.line5.Left = 5.31496F;
            this.line5.LineWeight = 2F;
            this.line5.Name = "line5";
            this.line5.Top = 0.6425197F;
            this.line5.Width = 0F;
            this.line5.X1 = 5.31496F;
            this.line5.X2 = 5.31496F;
            this.line5.Y1 = 0.6425197F;
            this.line5.Y2 = 0.8846457F;
            // 
            // line8
            // 
            this.line8.Height = 0.242126F;
            this.line8.Left = 6.299212F;
            this.line8.LineWeight = 2F;
            this.line8.Name = "line8";
            this.line8.Top = 0.6539371F;
            this.line8.Width = 0F;
            this.line8.X1 = 6.299212F;
            this.line8.X2 = 6.299212F;
            this.line8.Y1 = 0.6539371F;
            this.line8.Y2 = 0.8960631F;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.shape1,
            this.txtIrisu,
            this.txtSouko,
            this.txtShohinNm,
            this.txtKikaku,
            this.txtShohinCd,
            this.line2,
            this.line11,
            this.line12,
            this.line13,
            this.line14,
            this.line15,
            this.line16,
            this.line17,
            this.textBox7,
            this.textBox8});
            this.detail.Height = 0.1692913F;
            this.detail.Name = "detail";
            this.detail.Format += new System.EventHandler(this.detail_Format);
            // 
            // shape1
            // 
            this.shape1.BackColor = System.Drawing.Color.LightCyan;
            this.shape1.Height = 0.1653543F;
            this.shape1.Left = 0F;
            this.shape1.LineColor = System.Drawing.Color.LightCyan;
            this.shape1.LineWeight = 0.1F;
            this.shape1.Name = "shape1";
            this.shape1.RoundingRadius = 9.999999F;
            this.shape1.Top = 0F;
            this.shape1.Visible = false;
            this.shape1.Width = 7.484252F;
            // 
            // txtIrisu
            // 
            this.txtIrisu.DataField = "ITEM13";
            this.txtIrisu.Height = 0.1653543F;
            this.txtIrisu.Left = 4.330709F;
            this.txtIrisu.MultiLine = false;
            this.txtIrisu.Name = "txtIrisu";
            this.txtIrisu.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.txtIrisu.Text = "13";
            this.txtIrisu.Top = 0F;
            this.txtIrisu.Width = 0.984252F;
            // 
            // txtSouko
            // 
            this.txtSouko.DataField = "ITEM14";
            this.txtSouko.Height = 0.1653543F;
            this.txtSouko.Left = 5.31496F;
            this.txtSouko.Name = "txtSouko";
            this.txtSouko.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.txtSouko.Text = "14";
            this.txtSouko.Top = 0F;
            this.txtSouko.Width = 0.984252F;
            // 
            // txtShohinNm
            // 
            this.txtShohinNm.DataField = "ITEM11";
            this.txtShohinNm.Height = 0.1653543F;
            this.txtShohinNm.Left = 2.362205F;
            this.txtShohinNm.Name = "txtShohinNm";
            this.txtShohinNm.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtShohinNm.Text = "23,456,789,123";
            this.txtShohinNm.Top = 0F;
            this.txtShohinNm.Width = 0.984252F;
            // 
            // txtKikaku
            // 
            this.txtKikaku.DataField = "ITEM12";
            this.txtKikaku.Height = 0.1653543F;
            this.txtKikaku.Left = 3.346457F;
            this.txtKikaku.Name = "txtKikaku";
            this.txtKikaku.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtKikaku.Text = "12";
            this.txtKikaku.Top = 0F;
            this.txtKikaku.Width = 0.984252F;
            // 
            // txtShohinCd
            // 
            this.txtShohinCd.DataField = "ITEM10";
            this.txtShohinCd.Height = 0.1653543F;
            this.txtShohinCd.Left = 0.01574802F;
            this.txtShohinCd.MultiLine = false;
            this.txtShohinCd.Name = "txtShohinCd";
            this.txtShohinCd.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; ddo-char-set: 1";
            this.txtShohinCd.Text = "10";
            this.txtShohinCd.Top = 0.003937008F;
            this.txtShohinCd.Width = 2.262599F;
            // 
            // line2
            // 
            this.line2.Height = 0F;
            this.line2.Left = 0F;
            this.line2.LineWeight = 2F;
            this.line2.Name = "line2";
            this.line2.Top = 0.1653543F;
            this.line2.Width = 7.484248F;
            this.line2.X1 = 7.484248F;
            this.line2.X2 = 0F;
            this.line2.Y1 = 0.1653543F;
            this.line2.Y2 = 0.1653543F;
            // 
            // line11
            // 
            this.line11.Height = 0.1692911F;
            this.line11.Left = 0.003937008F;
            this.line11.LineWeight = 2F;
            this.line11.Name = "line11";
            this.line11.Top = 0F;
            this.line11.Width = 0F;
            this.line11.X1 = 0.003937008F;
            this.line11.X2 = 0.003937008F;
            this.line11.Y1 = 0F;
            this.line11.Y2 = 0.1692911F;
            // 
            // line12
            // 
            this.line12.Height = 0.1692911F;
            this.line12.Left = 7.484252F;
            this.line12.LineWeight = 2F;
            this.line12.Name = "line12";
            this.line12.Top = 0F;
            this.line12.Width = 0F;
            this.line12.X1 = 7.484252F;
            this.line12.X2 = 7.484252F;
            this.line12.Y1 = 0F;
            this.line12.Y2 = 0.1692911F;
            // 
            // line13
            // 
            this.line13.Height = 0.1692911F;
            this.line13.Left = 6.299212F;
            this.line13.LineWeight = 2F;
            this.line13.Name = "line13";
            this.line13.Top = 0F;
            this.line13.Width = 0F;
            this.line13.X1 = 6.299212F;
            this.line13.X2 = 6.299212F;
            this.line13.Y1 = 0F;
            this.line13.Y2 = 0.1692911F;
            // 
            // line14
            // 
            this.line14.Height = 0.1692911F;
            this.line14.Left = 3.346457F;
            this.line14.LineWeight = 2F;
            this.line14.Name = "line14";
            this.line14.Top = 0F;
            this.line14.Width = 0F;
            this.line14.X1 = 3.346457F;
            this.line14.X2 = 3.346457F;
            this.line14.Y1 = 0F;
            this.line14.Y2 = 0.1692911F;
            // 
            // line15
            // 
            this.line15.Height = 0.1692911F;
            this.line15.Left = 2.361418F;
            this.line15.LineWeight = 2F;
            this.line15.Name = "line15";
            this.line15.Top = 0F;
            this.line15.Width = 0F;
            this.line15.X1 = 2.361418F;
            this.line15.X2 = 2.361418F;
            this.line15.Y1 = 0F;
            this.line15.Y2 = 0.1692911F;
            // 
            // line16
            // 
            this.line16.Height = 0.1692911F;
            this.line16.Left = 5.31496F;
            this.line16.LineWeight = 2F;
            this.line16.Name = "line16";
            this.line16.Top = 0F;
            this.line16.Width = 0F;
            this.line16.X1 = 5.31496F;
            this.line16.X2 = 5.31496F;
            this.line16.Y1 = 0F;
            this.line16.Y2 = 0.1692911F;
            // 
            // line17
            // 
            this.line17.Height = 0.1692911F;
            this.line17.Left = 4.330709F;
            this.line17.LineWeight = 2F;
            this.line17.Name = "line17";
            this.line17.Top = 0F;
            this.line17.Width = 0F;
            this.line17.X1 = 4.330709F;
            this.line17.X2 = 4.330709F;
            this.line17.Y1 = 0F;
            this.line17.Y2 = 0.1692911F;
            // 
            // textBox7
            // 
            this.textBox7.DataField = "ITEM15";
            this.textBox7.Height = 0.1653543F;
            this.textBox7.Left = 6.468111F;
            this.textBox7.Name = "textBox7";
            this.textBox7.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.textBox7.Text = "15";
            this.textBox7.Top = 0F;
            this.textBox7.Width = 0.9842521F;
            // 
            // textBox8
            // 
            this.textBox8.DataField = "ITEM18";
            this.textBox8.Height = 0.1653543F;
            this.textBox8.Left = 2.088189F;
            this.textBox8.MultiLine = false;
            this.textBox8.Name = "textBox8";
            this.textBox8.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; ddo-char-set: 1";
            this.textBox8.Text = "18";
            this.textBox8.Top = 0F;
            this.textBox8.Visible = false;
            this.textBox8.Width = 0.2732283F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            this.pageFooter.Visible = false;
            // 
            // reportHeader1
            // 
            this.reportHeader1.Height = 0F;
            this.reportHeader1.Name = "reportHeader1";
            // 
            // reportFooter1
            // 
            this.reportFooter1.Height = 0F;
            this.reportFooter1.Name = "reportFooter1";
            this.reportFooter1.Visible = false;
            // 
            // groupHeader1
            // 
            this.groupHeader1.DataField = "ITEM16";
            this.groupHeader1.Height = 0F;
            this.groupHeader1.Name = "groupHeader1";
            this.groupHeader1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
            this.groupHeader1.UnderlayNext = true;
            // 
            // groupFooter1
            // 
            this.groupFooter1.Height = 0F;
            this.groupFooter1.Name = "groupFooter1";
            // 
            // ZMMR107111R
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.7874016F;
            this.PageSettings.Margins.Left = 0.3937008F;
            this.PageSettings.Margins.Right = 0.3937008F;
            this.PageSettings.Margins.Top = 0.5905512F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.486221F;
            this.Sections.Add(this.reportHeader1);
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.groupHeader1);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.groupFooter1);
            this.Sections.Add(this.pageFooter);
            this.Sections.Add(this.reportFooter1);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHyojiDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportInfo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIrisu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSouko)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShohinNm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKikaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShohinCd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCompanyName;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHyojiDate;
        private GrapeCity.ActiveReports.SectionReportModel.Label label3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPageCount;
        private GrapeCity.ActiveReports.SectionReportModel.ReportInfo reportInfo1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox2;
        private GrapeCity.ActiveReports.SectionReportModel.Label label2;
        private GrapeCity.ActiveReports.SectionReportModel.Label label4;
        private GrapeCity.ActiveReports.SectionReportModel.Label label5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox5;
        private GrapeCity.ActiveReports.SectionReportModel.Line line9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtShohinCd;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtIrisu;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSouko;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtShohinNm;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKikaku;
        private GrapeCity.ActiveReports.SectionReportModel.ReportHeader reportHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter reportFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.Line line3;
        private GrapeCity.ActiveReports.SectionReportModel.Line line4;
        private GrapeCity.ActiveReports.SectionReportModel.Line line5;
        private GrapeCity.ActiveReports.SectionReportModel.Line line6;
        private GrapeCity.ActiveReports.SectionReportModel.Line line7;
        private GrapeCity.ActiveReports.SectionReportModel.Line line8;
        private GrapeCity.ActiveReports.SectionReportModel.Line line10;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.Line line11;
        private GrapeCity.ActiveReports.SectionReportModel.Line line12;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader groupHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter groupFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtToday;
        private GrapeCity.ActiveReports.SectionReportModel.Line line13;
        private GrapeCity.ActiveReports.SectionReportModel.Line line14;
        private GrapeCity.ActiveReports.SectionReportModel.Line line15;
        private GrapeCity.ActiveReports.SectionReportModel.Line line16;
        private GrapeCity.ActiveReports.SectionReportModel.Line line17;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox8;
        private GrapeCity.ActiveReports.SectionReportModel.Shape shape1;
    }
}
