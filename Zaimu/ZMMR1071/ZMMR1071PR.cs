﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Windows.Forms;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.userinfo;
using jp.co.fsi.common.util;
using jp.co.fsi.common.forms;

using ClosedXML.Excel;

namespace jp.co.fsi.zm.zmmr1071
{
    /// <summary>
    /// モジュール全体で使用するデータアクセスクラスです。
    /// </summary>
    public class ZMMR1071PR
    {
        #region private変数
        /// <summary>
        /// ZAMR2031(条件画面)のオブジェクト(設定内容の取得のため)
        /// </summary>
        ZMMR1071 _pForm;

        /// <summary>
        /// ユーザー情報
        /// </summary>
        UserInfo _uInfo;

        /// <summary>
        /// データアクセスオブジェクト
        /// </summary>
        DbAccess _dba;

        /// <summary>
        /// 設定ファイルアクセスオブジェクト
        /// </summary>
        ConfigLoader _config;

        /// <summary>
        /// ユニークID
        /// </summary>
        string _unqId;


        private ZMMR1071DA _da;
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="uInfo">操作中のユーザーの情報</param>
        /// <param name="dba">呼び出し元で保持するデータアクセスオブジェクト</param>
        /// <param name="config">呼び出し元で保持する設定ファイルアクセスオブジェクト</param>
        public ZMMR1071PR(UserInfo uInfo, DbAccess dba, ConfigLoader config, string unuqId, ZMMR1071 frm)
        {
            this._uInfo = uInfo;
            this._dba = dba;
            this._config = config;
            this._unqId = unuqId;
            this._pForm = frm;
        }
        #endregion

        #region publicメソッド
        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        public bool DoPrint(bool isPreview, bool isPdf = false, bool isExcel = false, bool isCsv = false)
        {
            if (!DispSetKikan())
            {
                return false;
            }

            bool dataFlag;
            ZMMR1074 msgFrm = new ZMMR1074();
            try
            {
                // 処理中メッセージ表示
                msgFrm.Show();
                msgFrm.Refresh();

                this._dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                dataFlag = MakeWkData();

                // 帳票出力
                if (dataFlag)
                {
                    #region 取得列の定義
                    StringBuilder cols = new StringBuilder();
                    cols.Append("  ITEM01");
                    cols.Append(" ,ITEM02");
                    cols.Append(" ,ITEM03");
                    cols.Append(" ,ITEM04");
                    cols.Append(" ,ITEM05");
                    cols.Append(" ,ITEM06");
                    cols.Append(" ,ITEM07");
                    cols.Append(" ,ITEM08");
                    cols.Append(" ,ITEM09");
                    cols.Append(" ,ITEM10");
                    cols.Append(" ,ITEM11");
                    cols.Append(" ,ITEM12");
                    cols.Append(" ,ITEM13");
                    cols.Append(" ,ITEM14");
                    cols.Append(" ,ITEM15");
                    cols.Append(" ,ITEM16");
                    cols.Append(" ,ITEM17");
                    cols.Append(" ,ITEM18");
                    cols.Append(" ,ITEM19");
                    cols.Append(" ,ITEM20");
                    cols.Append(" ,ITEM21");
                    cols.Append(" ,ITEM22");
                    cols.Append(" ,ITEM23");
                    cols.Append(" ,ITEM24");
                    cols.Append(" ,ITEM25");
                    cols.Append(" ,ITEM26");
                    cols.Append(" ,ITEM27");
                    cols.Append(" ,ITEM28");
                    cols.Append(" ,ITEM29");
                    cols.Append(" ,ITEM30");
                    cols.Append(" ,ITEM31");
                    cols.Append(" ,ITEM32");
                    cols.Append(" ,ITEM33");
                    cols.Append(" ,ITEM34");
                    cols.Append(" ,ITEM35");
                    cols.Append(" ,ITEM36");
                    cols.Append(" ,ITEM37");
                    cols.Append(" ,ITEM38");
                    cols.Append(" ,ITEM39");
                    cols.Append(" ,ITEM40");
                    cols.Append(" ,ITEM41");
                    cols.Append(" ,ITEM42");
                    cols.Append(" ,ITEM43");
                    cols.Append(" ,ITEM44");
                    cols.Append(" ,ITEM45");
                    cols.Append(" ,ITEM46");
                    cols.Append(" ,ITEM47");
                    cols.Append(" ,ITEM48");
                    cols.Append(" ,ITEM49");
                    cols.Append(" ,ITEM50");
                    cols.Append(" ,ITEM51");
                    cols.Append(" ,ITEM52");
                    cols.Append(" ,ITEM53");
                    cols.Append(" ,ITEM54");
                    cols.Append(" ,ITEM55");
                    cols.Append(" ,ITEM56");
                    cols.Append(" ,ITEM57");
                    cols.Append(" ,ITEM58");
                    cols.Append(" ,ITEM59");
                    cols.Append(" ,ITEM60");
                    cols.Append(" ,ITEM61");
                    cols.Append(" ,ITEM62");
                    cols.Append(" ,ITEM63");
                    cols.Append(" ,ITEM64");
                    cols.Append(" ,ITEM65");
                    cols.Append(" ,ITEM66");
                    cols.Append(" ,ITEM67");
                    cols.Append(" ,ITEM68");
                    cols.Append(" ,ITEM69");
                    cols.Append(" ,ITEM70");
                    cols.Append(" ,ITEM71");
                    cols.Append(" ,ITEM72");
                    cols.Append(" ,ITEM73");
                    cols.Append(" ,ITEM74");
                    cols.Append(" ,ITEM75");
                    cols.Append(" ,ITEM76");
                    cols.Append(" ,ITEM77");
                    cols.Append(" ,ITEM78");
                    cols.Append(" ,ITEM79");
                    cols.Append(" ,ITEM80");
                    cols.Append(" ,ITEM81");
                    cols.Append(" ,ITEM82");
                    cols.Append(" ,ITEM83");
                    cols.Append(" ,ITEM84");
                    cols.Append(" ,ITEM85");
                    cols.Append(" ,ITEM86");
                    cols.Append(" ,ITEM87");
                    cols.Append(" ,ITEM88");
                    cols.Append(" ,ITEM89");
                    cols.Append(" ,ITEM90");
                    cols.Append(" ,ITEM91");
                    cols.Append(" ,ITEM92");
                    cols.Append(" ,ITEM93");
                    cols.Append(" ,ITEM94");
                    cols.Append(" ,ITEM95");
                    cols.Append(" ,ITEM96");
                    cols.Append(" ,ITEM97");
                    cols.Append(" ,ITEM98");
                    cols.Append(" ,ITEM99");
                    cols.Append(" ,ITEM100");
                    cols.Append(" ,ITEM101");
                    cols.Append(" ,ITEM102");
                    cols.Append(" ,ITEM103");
                    cols.Append(" ,ITEM104");
                    cols.Append(" ,ITEM105");
                    cols.Append(" ,ITEM106");
                    cols.Append(" ,ITEM107");
                    cols.Append(" ,ITEM108");
                    cols.Append(" ,ITEM109");
                    cols.Append(" ,ITEM110");
                    cols.Append(" ,ITEM111");
                    cols.Append(" ,ITEM112");
                    cols.Append(" ,ITEM113");
                    cols.Append(" ,ITEM114");
                    cols.Append(" ,ITEM115");
                    cols.Append(" ,ITEM116");
                    cols.Append(" ,ITEM117");
                    cols.Append(" ,ITEM118");
                    cols.Append(" ,ITEM119");
                    cols.Append(" ,ITEM120");
                    cols.Append(" ,ITEM121");
                    cols.Append(" ,ITEM122");
                    cols.Append(" ,ITEM123");
                    cols.Append(" ,ITEM124");
                    cols.Append(" ,ITEM125");
                    cols.Append(" ,ITEM126");
                    cols.Append(" ,ITEM127");
                    cols.Append(" ,ITEM128");
                    cols.Append(" ,ITEM129");
                    cols.Append(" ,ITEM130");
                    cols.Append(" ,ITEM131");
                    cols.Append(" ,ITEM132");
                    cols.Append(" ,ITEM133");
                    cols.Append(" ,ITEM134");
                    cols.Append(" ,ITEM135");
                    cols.Append(" ,ITEM136");
                    cols.Append(" ,ITEM137");
                    cols.Append(" ,ITEM138");
                    cols.Append(" ,ITEM139");
                    cols.Append(" ,ITEM140");
                    cols.Append(" ,ITEM141");
                    cols.Append(" ,ITEM142");
                    cols.Append(" ,ITEM143");
                    cols.Append(" ,ITEM144");
                    cols.Append(" ,ITEM145");
                    cols.Append(" ,ITEM146");
                    cols.Append(" ,ITEM147");
                    cols.Append(" ,ITEM148");
                    cols.Append(" ,ITEM149");
                    cols.Append(" ,ITEM150");
                    cols.Append(" ,ITEM151");
                    cols.Append(" ,ITEM152");
                    cols.Append(" ,ITEM153");
                    cols.Append(" ,ITEM154");
                    cols.Append(" ,ITEM155");
                    cols.Append(" ,ITEM156");
                    cols.Append(" ,ITEM157");
                    cols.Append(" ,ITEM158");
                    cols.Append(" ,ITEM159");
                    cols.Append(" ,ITEM160");
                    cols.Append(" ,ITEM161");
                    cols.Append(" ,ITEM162");
                    cols.Append(" ,ITEM163");
                    cols.Append(" ,ITEM164");
                    cols.Append(" ,ITEM165");
                    cols.Append(" ,ITEM166");
                    cols.Append(" ,ITEM167");
                    cols.Append(" ,ITEM168");
                    cols.Append(" ,ITEM169");
                    cols.Append(" ,ITEM170");
                    cols.Append(" ,ITEM171");
                    cols.Append(" ,ITEM172");
                    cols.Append(" ,ITEM173");
                    cols.Append(" ,ITEM174");
                    cols.Append(" ,ITEM175");
                    cols.Append(" ,ITEM176");
                    cols.Append(" ,ITEM177");
                    cols.Append(" ,ITEM178");
                    cols.Append(" ,ITEM179");
                    cols.Append(" ,ITEM180");
                    cols.Append(" ,ITEM181");
                    cols.Append(" ,ITEM182");
                    cols.Append(" ,ITEM183");
                    cols.Append(" ,ITEM184");
                    cols.Append(" ,ITEM185");
                    cols.Append(" ,ITEM186");
                    cols.Append(" ,ITEM187");
                    cols.Append(" ,ITEM188");
                    cols.Append(" ,ITEM189");
                    cols.Append(" ,ITEM190");
                    cols.Append(" ,ITEM191");
                    cols.Append(" ,ITEM192");
                    cols.Append(" ,ITEM193");
                    cols.Append(" ,ITEM194");
                    cols.Append(" ,ITEM195");
                    cols.Append(" ,ITEM196");
                    cols.Append(" ,ITEM197");
                    cols.Append(" ,ITEM198");
                    cols.Append(" ,ITEM199");
                    cols.Append(" ,ITEM200");
                    cols.Append(" ,ITEM201");
                    cols.Append(" ,ITEM202");
                    cols.Append(" ,ITEM203");
                    cols.Append(" ,ITEM204");
                    cols.Append(" ,ITEM205");
                    cols.Append(" ,ITEM206");
                    cols.Append(" ,ITEM207");
                    cols.Append(" ,ITEM208");
                    cols.Append(" ,ITEM209");
                    cols.Append(" ,ITEM210");
                    cols.Append(" ,ITEM211");
                    cols.Append(" ,ITEM212");
                    cols.Append(" ,ITEM213");
                    cols.Append(" ,ITEM214");
                    cols.Append(" ,ITEM215");
                    cols.Append(" ,ITEM216");
                    cols.Append(" ,ITEM217");
                    cols.Append(" ,ITEM218");
                    cols.Append(" ,ITEM219");
                    cols.Append(" ,ITEM220");
                    cols.Append(" ,ITEM221");
                    cols.Append(" ,ITEM222");
                    cols.Append(" ,ITEM223");
                    cols.Append(" ,ITEM224");
                    cols.Append(" ,ITEM225");
                    cols.Append(" ,ITEM226");
                    cols.Append(" ,ITEM227");
                    cols.Append(" ,ITEM228");
                    cols.Append(" ,ITEM229");
                    cols.Append(" ,ITEM230");
                    cols.Append(" ,ITEM231");
                    cols.Append(" ,ITEM232");
                    cols.Append(" ,ITEM233");
                    cols.Append(" ,ITEM234");
                    cols.Append(" ,ITEM235");
                    cols.Append(" ,ITEM236");
                    cols.Append(" ,ITEM237");
                    cols.Append(" ,ITEM238");
                    cols.Append(" ,ITEM239");
                    cols.Append(" ,ITEM240");
                    cols.Append(" ,ITEM241");
                    cols.Append(" ,ITEM242");
                    cols.Append(" ,ITEM243");
                    cols.Append(" ,ITEM244");
                    cols.Append(" ,ITEM245");
                    cols.Append(" ,ITEM246");
                    cols.Append(" ,ITEM247");
                    cols.Append(" ,ITEM248");
                    cols.Append(" ,ITEM249");
                    cols.Append(" ,ITEM250");
                    cols.Append(" ,ITEM251");
                    cols.Append(" ,ITEM252");
                    cols.Append(" ,ITEM253");
                    cols.Append(" ,ITEM254");
                    cols.Append(" ,ITEM255");
                    cols.Append(" ,ITEM256");
                    cols.Append(" ,ITEM257");
                    cols.Append(" ,ITEM258");
                    cols.Append(" ,ITEM259");
                    cols.Append(" ,ITEM260");
                    cols.Append(" ,ITEM261");
                    cols.Append(" ,ITEM262");
                    cols.Append(" ,ITEM263");
                    cols.Append(" ,ITEM264");
                    cols.Append(" ,ITEM265");
                    cols.Append(" ,ITEM266");
                    cols.Append(" ,ITEM267");
                    cols.Append(" ,ITEM268");
                    cols.Append(" ,ITEM269");
                    cols.Append(" ,ITEM270");
                    cols.Append(" ,ITEM271");
                    cols.Append(" ,ITEM272");
                    cols.Append(" ,ITEM273");
                    cols.Append(" ,ITEM274");
                    cols.Append(" ,ITEM275");
                    cols.Append(" ,ITEM276");
                    cols.Append(" ,ITEM277");
                    cols.Append(" ,ITEM278");
                    cols.Append(" ,ITEM279");
                    cols.Append(" ,ITEM280");
                    cols.Append(" ,ITEM281");
                    cols.Append(" ,ITEM282");
                    cols.Append(" ,ITEM283");
                    cols.Append(" ,ITEM284");
                    cols.Append(" ,ITEM285");
                    cols.Append(" ,ITEM286");
                    cols.Append(" ,ITEM287");
                    cols.Append(" ,ITEM288");
                    cols.Append(" ,ITEM289");
                    cols.Append(" ,ITEM290");
                    cols.Append(" ,ITEM291");
                    cols.Append(" ,ITEM292");
                    cols.Append(" ,ITEM293");
                    cols.Append(" ,ITEM294");
                    cols.Append(" ,ITEM295");
                    cols.Append(" ,ITEM296");
                    cols.Append(" ,ITEM297");
                    cols.Append(" ,ITEM298");
                    cols.Append(" ,ITEM299");
                    cols.Append(" ,ITEM300");
                    cols.Append(" ,ITEM301");
                    cols.Append(" ,ITEM302");
                    cols.Append(" ,ITEM303");
                    cols.Append(" ,ITEM304");
                    cols.Append(" ,ITEM305");
                    cols.Append(" ,ITEM306");
                    cols.Append(" ,ITEM307");
                    cols.Append(" ,ITEM308");
                    cols.Append(" ,ITEM309");
                    cols.Append(" ,ITEM310");
                    cols.Append(" ,ITEM311");
                    cols.Append(" ,ITEM312");
                    cols.Append(" ,ITEM313");
                    cols.Append(" ,ITEM314");
                    cols.Append(" ,ITEM315");
                    cols.Append(" ,ITEM316");
                    cols.Append(" ,ITEM317");
                    cols.Append(" ,ITEM318");
                    cols.Append(" ,ITEM319");
                    cols.Append(" ,ITEM320");
                    cols.Append(" ,ITEM321");
                    cols.Append(" ,ITEM322");
                    cols.Append(" ,ITEM323");
                    cols.Append(" ,ITEM324");
                    cols.Append(" ,ITEM325");
                    cols.Append(" ,ITEM326");
                    cols.Append(" ,ITEM327");
                    cols.Append(" ,ITEM328");
                    cols.Append(" ,ITEM329");
                    cols.Append(" ,ITEM330");
                    cols.Append(" ,ITEM331");
                    cols.Append(" ,ITEM332");
                    cols.Append(" ,ITEM333");
                    cols.Append(" ,ITEM334");
                    cols.Append(" ,ITEM335");
                    cols.Append(" ,ITEM336");
                    cols.Append(" ,ITEM337");
                    cols.Append(" ,ITEM338");
                    cols.Append(" ,ITEM339");
                    cols.Append(" ,ITEM340");
                    cols.Append(" ,ITEM341");
                    cols.Append(" ,ITEM342");
                    cols.Append(" ,ITEM343");
                    cols.Append(" ,ITEM344");
                    cols.Append(" ,ITEM345");
                    cols.Append(" ,ITEM346");
                    cols.Append(" ,ITEM347");
                    cols.Append(" ,ITEM348");
                    cols.Append(" ,ITEM349");
                    cols.Append(" ,ITEM350");
                    cols.Append(" ,ITEM351");
                    cols.Append(" ,ITEM352");
                    cols.Append(" ,ITEM353");
                    cols.Append(" ,ITEM354");
                    cols.Append(" ,ITEM355");
                    cols.Append(" ,ITEM356");
                    cols.Append(" ,ITEM357");
                    cols.Append(" ,ITEM358");
                    cols.Append(" ,ITEM359");
                    cols.Append(" ,ITEM360");
                    cols.Append(" ,ITEM361");
                    cols.Append(" ,ITEM362");
                    cols.Append(" ,ITEM363");
                    cols.Append(" ,ITEM364");
                    cols.Append(" ,ITEM365");
                    cols.Append(" ,ITEM366");
                    cols.Append(" ,ITEM367");
                    cols.Append(" ,ITEM368");
                    cols.Append(" ,ITEM369");
                    cols.Append(" ,ITEM370");
                    cols.Append(" ,ITEM371");
                    cols.Append(" ,ITEM372");
                    cols.Append(" ,ITEM373");
                    cols.Append(" ,ITEM374");
                    cols.Append(" ,ITEM375");
                    cols.Append(" ,ITEM376");
                    cols.Append(" ,ITEM377");
                    // 名護へ合わせ
                    cols.Append(" ,ITEM378");
                    cols.Append(" ,ITEM379");
                    cols.Append(" ,ITEM380");
                    cols.Append(" ,ITEM381");
                    cols.Append(" ,ITEM382");
                    #endregion

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this._unqId);

                    // データの取得
                    DataTable dtOutput = this._dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_ZM_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    ZMMR10711R rpt = null;
                    // 帳票オブジェクトをインスタンス化
                    if (!isExcel)
                    {
                        rpt = new ZMMR10711R(dtOutput);

                        rpt.Document.Printer.DocumentName = Util.ToString(this._pForm.Condition["ReportName"]);
                        rpt.Document.Name = Util.ToString(this._pForm.Condition["ReportName"]);
                    }

                    if (isExcel)
                    {
                        // エクセル出力はレポート機能を使わない方式

                        //GrapeCity.ActiveReports.Export.Excel.Section.XlsExport xlsExport1 = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
                        ////SetExcelSetting(xlsExport1);
                        //rpt.Run();
                        //string saveFileName = Util.GetSavePath(Constants.SubSys.Zai, rpt.Document.Name, 2);
                        //if (!ValChk.IsEmpty(saveFileName))
                        //{
                        //    xlsExport1.Export(rpt.Document, saveFileName);
                        //    Msg.InfoNm("EXCEL出力", "保存しました。");
                        //    Util.OpenFolder(saveFileName);
                        //}
                        if (dtOutput.Rows.Count > 0)
                        {
                            List<string> title = new List<string>();
                            title.Add("科　目　名"); // タイトル1
                            title.Add(Util.ToString(dtOutput.Rows[0]["ITEM06"])); // タイトル2
                            title.Add(Util.ToString(dtOutput.Rows[0]["ITEM07"])); // タイトル3
                            title.Add(Util.ToString(dtOutput.Rows[0]["ITEM08"])); // タイトル4
                            title.Add(Util.ToString(dtOutput.Rows[0]["ITEM09"])); // タイトル5
                            title.Add("当　期"); // タイトル6

                            string sheetName = "";
                            int docNo = 1;
                            var workbook = new XLWorkbook();
                            IXLWorksheet ws = null;
                            int lin = 1;
                            foreach (DataRow dr in dtOutput.Rows)
                            {
                                // 帳票タイトルでシート分け
                                if (sheetName != Util.ToString(dr["ITEM01"]))
                                {
                                    // 見出しの設定
                                    lin = 1;
                                    sheetName = "五期比較諸表" + docNo.ToString();
                                    ws = workbook.Worksheets.Add(sheetName);
                                    sheetName = Util.ToString(dr["ITEM01"]);
                                    docNo++;
                                    // タイトル
                                    ws.Cell(lin, 1).Value = sheetName;
                                    // 出力日
                                    ws.Cell(lin, 5).Value = Util.ToString(dr["ITEM17"]);
                                    lin++;
                                    // 部門条件
                                    ws.Cell(lin, 1).Value = Util.ToString(dr["ITEM02"]);
                                    // 日付最終
                                    ws.Cell(lin, 5).Value = Util.ToString(dr["ITEM03"]);
                                    lin++;
                                    // 会社名
                                    ws.Cell(lin, 1).Value = Util.ToString(dr["ITEM04"]);
                                    // 消費税処理
                                    ws.Cell(lin, 5).Value = Util.ToString(dr["ITEM05"]);
                                    lin++;
                                    for (int i = 0; i < title.Count; i++)
                                    {
                                        ws.Cell(lin, (i + 1)).Value = title[i];
                                    }
                                    lin++;
                                }

                                // 明細部の設定

                                // 科目名
                                if (Util.ToString(dr["ITEM18"]) == "1")
                                    ws.Cell(lin, 1).Style.Font.Bold = true;
                                ws.Cell(lin, 1).Value = Util.ToString(dr["ITEM10"]);
                                // 金額１
                                ws.Cell(lin, 2).Style.NumberFormat.Format = "#,###";
                                ws.Cell(lin, 2).Value = Util.ToString(dr["ITEM11"]);
                                // 金額２
                                ws.Cell(lin, 3).Style.NumberFormat.Format = "#,###";
                                ws.Cell(lin, 3).Value = Util.ToString(dr["ITEM12"]);
                                // 金額３
                                ws.Cell(lin, 4).Style.NumberFormat.Format = "#,###";
                                ws.Cell(lin, 4).Value = Util.ToString(dr["ITEM13"]);
                                // 金額４
                                ws.Cell(lin, 5).Style.NumberFormat.Format = "#,###";
                                ws.Cell(lin, 5).Value = Util.ToString(dr["ITEM14"]);
                                // 金額５
                                ws.Cell(lin, 6).Style.NumberFormat.Format = "#,###";
                                ws.Cell(lin, 6).Value = Util.ToString(dr["ITEM15"]);

                                lin++;
                            }

                            // 保存先の指定
                            string saveFileName = GetSavePath("五期比較諸表", 2);
                            if (!ValChk.IsEmpty(saveFileName))
                            {
                                workbook.SaveAs(saveFileName);

                                Msg.InfoNm("EXCEL出力", "保存しました。");
                                Util.OpenFolder(saveFileName);
                            }
                        }
                    }
                    else if (isPdf)
                    {
                        GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport p = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Zai, rpt.Document.Name, 1);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            p.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("PDF出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPreview)
                    {
                        // プレビュー画面表示
                        PreviewForm pFrm = new PreviewForm(rpt, this._unqId);
                        pFrm.WindowState = FormWindowState.Maximized;
                        pFrm.Show();
                    }
                    else
                    {
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
            }
            finally
            {
                this._dba.Rollback();

                msgFrm.Close();
            }
            if (dataFlag)
                return true;
            else
                return false;
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 保持用データをセット
        /// </summary>
        private Boolean DispSetKikan()
        {
            // 該当会計年度の会計期間開始日を取得
            DataTable dt = Util.GetKaikeiKikan(this._uInfo.KaikeiNendo, this._dba);
   
            if (dt.Rows.Count == 0)
            {
                Msg.Info("該当会計年度の情報が取得できませんでした。");
                return false;
            }

            //this._dtKikanKaishibi = Util.ToDate(dt.Rows[0]["KAIKEI_KIKAN_KAISHIBI"]);

            return true;
        }

        #region コメント
        ///// <summary>
        ///// データを格納
        ///// </summary>
        //private void DataSet()
        //{
        //    // 対象データテーブルにカラムを5列ずつ定義
        //    KanjoKamokuKingaku.Columns.Add("shukeiKeisanShiki", Type.GetType("System.String"));
        //    KanjoKamokuKingaku.Columns.Add("zandakaKingaku", Type.GetType("System.Int64"));
        //    KanjoKamokuKingaku.Columns.Add("karikataKingaku", Type.GetType("System.Int64"));
        //    KanjoKamokuKingaku.Columns.Add("kashikataKingaku", Type.GetType("System.Int64"));
        //    KanjoKamokuKingaku.Columns.Add("touzanKingaku", Type.GetType("System.Int64"));

        //    ZMMR1031DA da = new ZMMR1031DA(this._uInfo, this._dba, this._config);

        //    // F6:貸借対照表、F7:損益計算書、F8:製造原価の大項目名を取得
        //    this._dtTaishakuTaishohyo = da.GetKamokuDaiKomoku(LIST_F1);
        //    this._dtSonekiKeisansho = da.GetKamokuDaiKomoku(LIST_F2);
        //    this._dtSeizoGenka = da.GetKamokuDaiKomoku(LIST_F3);

        //    DataTable kanjoKamokuIchiran;
        //    DataTable kingakuData;
        //    DataRow row;
        //    int shukeiKubun;
        //    string kamokuNm;
        //    int kamokuBunrui;
        //    int meisaiKubun;
        //    int mojiShubetsu;
        //    int daiKomokuTaishakuKubun;
        //    int shoKomokuTaishakuKubun;
        //    string shukeiKeisanShiki;
        //    decimal zenZanKingaku;
        //    decimal karikataKingaku;
        //    decimal kashikataKingaku;
        //    decimal zandakaKingaku;
        //    int taishoNo;
        //    string taishoNm;
        //    int gyoBango;
        //    string jokenPlus = "";
        //    string jokenMinus = "";
        //    int i = 0;
        //    int j = 0;

        //    // 法定準備金の金額を保持
        //    // 金額データを取得
        //    DataTable dt = da.GetHoteJunbiki(kikanKaishibi, this._pForm.Condition);
        //    //if (dt.Rows.Count > 0)
        //    if (dt.Rows.Count > 0 && Util.ToInt(dt.Rows[0]["JUNBIKIN"]) > 0)
        //    {
        //        this._dtHoteJunbikin = Util.ToDecimal(dt.Rows[0]["JUNBIKIN"]);
        //    }
        //    // 当期未処分剰余金の金額を保持
        //    dt = da.GetTokiMishobunJoyokin(this._pForm.Condition);
        //    //if (dt.Rows.Count > 0)
        //    if (dt.Rows.Count > 0 && Util.ToDecimal(dt.Rows[0]["KINGAKU"]) != 0)
        //    {
        //        this._dtTokiMishobunJoyokin = Util.ToDecimal(dt.Rows[0]["KINGAKU"]);
        //    }

        //    // 貸借対照表、損益計算書、製造原価の大項目名毎の小項目データを取得し、格納
        //    #region 製造原価
        //    while (SeizoGenka.Rows.Count > i)
        //    {
        //        meisaiKubun = Util.ToInt(SeizoGenka.Rows[i]["MEISAI_KUBUN"]);
        //        // 明細区分が0以外のデータのみ処理
        //        if (meisaiKubun != 0)
        //        {
        //            kamokuNm = Util.ToString(SeizoGenka.Rows[i]["KAMOKU_BUNRUI_NM"]);
        //            shukeiKeisanShiki = Util.ToString(SeizoGenka.Rows[i]["SHUKEI_KEISANSHIKI"]);
        //            mojiShubetsu = Util.ToInt(SeizoGenka.Rows[i]["MOJI_SHUBETSU"]);
        //            daiKomokuTaishakuKubun = Util.ToInt(SeizoGenka.Rows[i]["TAISHAKU_KUBUN"]);
        //            kamokuBunrui = Util.ToInt(SeizoGenka.Rows[i]["KAMOKU_BUNRUI"]);
        //            kanjoKamokuIchiran = da.GetKamokuShoKomoku(LIST_F3, kamokuBunrui);

        //            // 小項目データを取得
        //            while (kanjoKamokuIchiran.Rows.Count > j)
        //            {
        //                zenZanKingaku = 0;
        //                karikataKingaku = 0;
        //                kashikataKingaku = 0;
        //                zandakaKingaku = 0;
        //                taishoNo = Util.ToInt(kanjoKamokuIchiran.Rows[j]["KANJO_KAMOKU_CD"]);
        //                taishoNm = Util.ToString(kanjoKamokuIchiran.Rows[j]["KANJO_KAMOKU_NM"]);
        //                gyoBango = Util.ToInt(kanjoKamokuIchiran.Rows[j]["GYO_BANGO"]);
        //                shoKomokuTaishakuKubun = Util.ToInt(kanjoKamokuIchiran.Rows[j]["TAISHAKU_KUBUN"]);

        //                // 金額データを取得
        //                kingakuData = da.GetKamokuKingaku(taishoNo, this._pForm.Condition);
        //                if (kingakuData.Rows.Count != 0)
        //                {
        //                    zenZanKingaku = Util.ToDecimal(kingakuData.Rows[0]["ZEN_ZAN"]);
        //                    karikataKingaku = Util.ToDecimal(kingakuData.Rows[0]["KARIKATA"]);
        //                    kashikataKingaku = Util.ToDecimal(kingakuData.Rows[0]["KASHIKATA"]);
        //                    zandakaKingaku = Util.ToDecimal(kingakuData.Rows[0]["ZANDAKA"]);
        //                }

        //                // 金額をデータテーブルに格納
        //                row = KanjoKamokuKingaku.NewRow();
        //                row["shukeiKeisanShiki"] = shukeiKeisanShiki;
        //                // 小項目の貸借区分が貸方の場合、-1を掛ける
        //                if (shoKomokuTaishakuKubun == 1)
        //                {
        //                    row["zandakaKingaku"] = zenZanKingaku;
        //                    row["karikataKingaku"] = karikataKingaku;
        //                    row["kashikataKingaku"] = kashikataKingaku;
        //                    row["touzanKingaku"] = zandakaKingaku;
        //                }
        //                else
        //                {
        //                    row["zandakaKingaku"] = zenZanKingaku * -1;
        //                    row["karikataKingaku"] = karikataKingaku * -1;
        //                    row["kashikataKingaku"] = kashikataKingaku * -1;
        //                    row["touzanKingaku"] = zandakaKingaku * -1;
        //                }
        //                KanjoKamokuKingaku.Rows.Add(row);
        //                j++;
        //            }
        //        }
        //        j = 0;
        //        i++;
        //    }
        //    #endregion
        //    i = 0;
        //    #region 損益計算書
        //    while (SonekiKeisansho.Rows.Count > i)
        //    {
        //        shukeiKubun = Util.ToInt(SonekiKeisansho.Rows[i]["SHUKEI_KUBUN"]);
        //        //集計区分が2のデータのみ処理
        //        if (shukeiKubun == 2)
        //        {
        //            kamokuNm = Util.ToString(SonekiKeisansho.Rows[i]["KAMOKU_BUNRUI_NM"]);
        //            meisaiKubun = Util.ToInt(SonekiKeisansho.Rows[i]["MEISAI_KUBUN"]);
        //            shukeiKeisanShiki = Util.ToString(SonekiKeisansho.Rows[i]["SHUKEI_KEISANSHIKI"]);
        //            mojiShubetsu = Util.ToInt(SonekiKeisansho.Rows[i]["MOJI_SHUBETSU"]);
        //            daiKomokuTaishakuKubun = Util.ToInt(SonekiKeisansho.Rows[i]["TAISHAKU_KUBUN"]);
        //            kamokuBunrui = Util.ToInt(SonekiKeisansho.Rows[i]["KAMOKU_BUNRUI"]);
        //            kanjoKamokuIchiran = da.GetKamokuShoKomoku(LIST_F2, kamokuBunrui);

        //            // 小項目データを取得
        //            while (kanjoKamokuIchiran.Rows.Count > j)
        //            {
        //                zenZanKingaku = 0;
        //                karikataKingaku = 0;
        //                kashikataKingaku = 0;
        //                zandakaKingaku = 0;
        //                taishoNo = Util.ToInt(kanjoKamokuIchiran.Rows[j]["KANJO_KAMOKU_CD"]);
        //                taishoNm = Util.ToString(kanjoKamokuIchiran.Rows[j]["KANJO_KAMOKU_NM"]);
        //                gyoBango = Util.ToInt(kanjoKamokuIchiran.Rows[j]["GYO_BANGO"]);
        //                shoKomokuTaishakuKubun = Util.ToInt(kanjoKamokuIchiran.Rows[j]["TAISHAKU_KUBUN"]);

        //                // 金額データを取得
        //                kingakuData = da.GetKamokuKingaku(taishoNo, this._pForm.Condition);
        //                if (kingakuData.Rows.Count != 0)
        //                {
        //                    zenZanKingaku = Util.ToDecimal(kingakuData.Rows[0]["ZEN_ZAN"]);
        //                    karikataKingaku = Util.ToDecimal(kingakuData.Rows[0]["KARIKATA"]);
        //                    kashikataKingaku = Util.ToDecimal(kingakuData.Rows[0]["KASHIKATA"]);
        //                    zandakaKingaku = Util.ToDecimal(kingakuData.Rows[0]["ZANDAKA"]);
        //                }

        //                // 金額をデータテーブルに格納
        //                row = KanjoKamokuKingaku.NewRow();
        //                row["shukeiKeisanShiki"] = shukeiKeisanShiki;
        //                // 小項目の貸借区分が貸方の場合、-1を掛ける
        //                if (shoKomokuTaishakuKubun == 1)
        //                {
        //                    row["zandakaKingaku"] = zenZanKingaku;
        //                    row["karikataKingaku"] = karikataKingaku;
        //                    row["kashikataKingaku"] = kashikataKingaku;
        //                    row["touzanKingaku"] = zandakaKingaku;
        //                }
        //                else
        //                {
        //                    row["zandakaKingaku"] = zenZanKingaku * -1;
        //                    row["karikataKingaku"] = karikataKingaku * -1;
        //                    row["kashikataKingaku"] = kashikataKingaku * -1;
        //                    row["touzanKingaku"] = zandakaKingaku * -1;
        //                }
        //                KanjoKamokuKingaku.Rows.Add(row);
        //                j++;
        //            }
        //        }
        //        j = 0;
        //        i++;
        //    }
        //    #endregion
        //    i = 0;
        //    #region 貸借対照表
        //    while (TaishakuTaishohyo.Rows.Count > i)
        //    {
        //        meisaiKubun = Util.ToInt(TaishakuTaishohyo.Rows[i]["MEISAI_KUBUN"]);
        //        kamokuBunrui = Util.ToInt(TaishakuTaishohyo.Rows[i]["KAMOKU_BUNRUI"]);
        //        kamokuNm = Util.ToString(TaishakuTaishohyo.Rows[i]["KAMOKU_BUNRUI_NM"]);
        //        shukeiKeisanShiki = Util.ToString(TaishakuTaishohyo.Rows[i]["SHUKEI_KEISANSHIKI"]);
        //        mojiShubetsu = Util.ToInt(TaishakuTaishohyo.Rows[i]["MOJI_SHUBETSU"]);
        //        daiKomokuTaishakuKubun = Util.ToInt(TaishakuTaishohyo.Rows[i]["TAISHAKU_KUBUN"]);
        //        // 明細区分が0以外のデータのみ処理
        //        if (meisaiKubun != 0)
        //        {
        //            kanjoKamokuIchiran = da.GetKamokuShoKomoku(LIST_F1, kamokuBunrui);

        //            // 小項目データを取得、格納
        //            while (kanjoKamokuIchiran.Rows.Count > j)
        //            {
        //                zenZanKingaku = 0;
        //                karikataKingaku = 0;
        //                kashikataKingaku = 0;
        //                zandakaKingaku = 0;
        //                taishoNo = Util.ToInt(kanjoKamokuIchiran.Rows[j]["KANJO_KAMOKU_CD"]);
        //                taishoNm = Util.ToString(kanjoKamokuIchiran.Rows[j]["KANJO_KAMOKU_NM"]);
        //                gyoBango = Util.ToInt(kanjoKamokuIchiran.Rows[j]["GYO_BANGO"]);
        //                shoKomokuTaishakuKubun = Util.ToInt(kanjoKamokuIchiran.Rows[j]["TAISHAKU_KUBUN"]);

        //                // 金額データを取得
        //                kingakuData = da.GetKamokuKingaku(taishoNo, this._pForm.Condition);
        //                if (kingakuData.Rows.Count != 0)
        //                {
        //                    zenZanKingaku = Util.ToDecimal(kingakuData.Rows[0]["ZEN_ZAN"]);
        //                    karikataKingaku = Util.ToDecimal(kingakuData.Rows[0]["KARIKATA"]);
        //                    kashikataKingaku = Util.ToDecimal(kingakuData.Rows[0]["KASHIKATA"]);
        //                    zandakaKingaku = Util.ToDecimal(kingakuData.Rows[0]["ZANDAKA"]);
        //                }

        //                // 金額をデータテーブルに格納
        //                row = KanjoKamokuKingaku.NewRow();
        //                row["shukeiKeisanShiki"] = kamokuBunrui;
        //                // 小項目の貸借区分が貸方の場合、-1を掛ける
        //                if (shoKomokuTaishakuKubun == 1)
        //                {
        //                    row["zandakaKingaku"] = zenZanKingaku;
        //                    row["karikataKingaku"] = karikataKingaku;
        //                    row["kashikataKingaku"] = kashikataKingaku;
        //                    row["touzanKingaku"] = zandakaKingaku;
        //                }
        //                else
        //                {
        //                    row["zandakaKingaku"] = zenZanKingaku * -1;
        //                    row["karikataKingaku"] = karikataKingaku * -1;
        //                    row["kashikataKingaku"] = kashikataKingaku * -1;
        //                    row["touzanKingaku"] = zandakaKingaku * -1;
        //                }
        //                KanjoKamokuKingaku.Rows.Add(row);
        //                j++;
        //            }
        //        }
        //        // 明細区分が0で、科目分類が0以上のデータのみ処理
        //        if (meisaiKubun == 0 && kamokuBunrui > 0)
        //        {
        //            // 集計計算式にて計算
        //            ArrayList stTargetPlus = new ArrayList();
        //            ArrayList stTargetMinus = new ArrayList();
        //            object sumZandakaKingakuPlus;
        //            object sumKarikataKingakuPlus;
        //            object sumKashikataKingakuPlus;
        //            object sumTouzanKingakuPlus;
        //            object sumZandakaKingakuMinus;
        //            object sumKarikataKingakuMinus;
        //            object sumKashikataKingakuMinus;
        //            object sumTouzanKingakuMinus;
        //            int findPlus;
        //            int findMinus;
        //            int flag = 0;
        //            int first = 0;
        //            int plusMinusCheckFlag = 0;
        //            int deleteCount;

        //            #region 集計計算式の"+"と"-"を分別
        //            while (flag == 0)
        //            {
        //                // "+"と"-"の文字を検索
        //                findPlus = shukeiKeisanShiki.IndexOf("+");
        //                findMinus = shukeiKeisanShiki.IndexOf("-");
        //                // "+"も"-"も無ければ、処理終了
        //                if (findPlus <= 0 && findMinus <= 0)
        //                {
        //                    if (first == 0)
        //                    {
        //                        stTargetPlus.Add(shukeiKeisanShiki);
        //                    }
        //                    else
        //                    {
        //                        if (plusMinusCheckFlag == 0)
        //                        {
        //                            stTargetPlus.Add(shukeiKeisanShiki);
        //                        }
        //                        else if (plusMinusCheckFlag == 1)
        //                        {
        //                            stTargetMinus.Add(shukeiKeisanShiki);
        //                        }
        //                    }
        //                    flag = 1;
        //                }
        //                // "+"又は"-"があれば分けて格納
        //                else
        //                {
        //                    // 最初の処理 1つ目の項目を格納し、文字列から削除
        //                    if (first == 0)
        //                    {
        //                        // "+"の時の処理
        //                        if ((findMinus > findPlus && findPlus > 0) || (findPlus > findMinus && findMinus <= 0))
        //                        {
        //                            stTargetPlus.Add(shukeiKeisanShiki.Substring(0, findPlus));
        //                            shukeiKeisanShiki = shukeiKeisanShiki.Substring(findPlus);
        //                        }
        //                        // "-"の時の処理
        //                        else if ((findPlus > findMinus && findMinus > 0) || (findMinus > findPlus && findPlus <= 0))
        //                        {
        //                            stTargetPlus.Add(shukeiKeisanShiki.Substring(0, findMinus));
        //                            shukeiKeisanShiki = shukeiKeisanShiki.Substring(findMinus);
        //                        }
        //                    }
        //                    // 2つ目以降の処理 項目を順番に格納し、文字列から削除
        //                    else
        //                    {
        //                        // 削除する文字数を取得
        //                        if ((findPlus > findMinus && findMinus > 0) || (findMinus > findPlus && findPlus <= 0))
        //                        {
        //                            deleteCount = findMinus;
        //                        }
        //                        else
        //                        {
        //                            deleteCount = findPlus;
        //                        }
        //                        // 項目を格納
        //                        if (plusMinusCheckFlag == 0)
        //                        {
        //                            stTargetPlus.Add(shukeiKeisanShiki.Substring(0, deleteCount));
        //                            shukeiKeisanShiki = shukeiKeisanShiki.Substring(deleteCount);
        //                        }
        //                        else if (plusMinusCheckFlag == 1)
        //                        {
        //                            stTargetMinus.Add(shukeiKeisanShiki.Substring(0, deleteCount));
        //                            shukeiKeisanShiki = shukeiKeisanShiki.Substring(deleteCount);
        //                        }
        //                    }
        //                    // "+"と"-"の文字を検索
        //                    findPlus = shukeiKeisanShiki.IndexOf("+");
        //                    findMinus = shukeiKeisanShiki.IndexOf("-");
        //                    // 次の項目がが"+"か"-"なのかの判断フラグを設定
        //                    if (findPlus == 0)
        //                    {
        //                        plusMinusCheckFlag = 0;
        //                    }
        //                    else if (findMinus == 0)
        //                    {
        //                        plusMinusCheckFlag = 1;
        //                    }
        //                    // "+"又は"-"の文字を削除
        //                    shukeiKeisanShiki = shukeiKeisanShiki.Substring(1);
        //                }
        //                first = 1;
        //            }
        //            #endregion

        //            #region 条件式の作成
        //            // "+"の項目の条件式を作成
        //            j = 0;
        //            while (stTargetPlus.Count > j)
        //            {
        //                if (j == 0)
        //                {
        //                    jokenPlus = "shukeiKeisanShiki = " + stTargetPlus[j];
        //                }
        //                else
        //                {
        //                    jokenPlus += " OR shukeiKeisanShiki = " + stTargetPlus[j];
        //                }

        //                j++;
        //            }
        //            // "-"の項目の条件式を作成
        //            j = 0;
        //            while (stTargetMinus.Count > j)
        //            {
        //                if (j == 0)
        //                {
        //                    jokenMinus = "shukeiKeisanShiki = " + stTargetMinus[j];
        //                }
        //                else
        //                {
        //                    jokenMinus += " OR shukeiKeisanShiki = " + stTargetMinus[j];
        //                }

        //                j++;
        //            }
        //            #endregion

        //            #region 残高金額、借方金額、貸方金額、当残金額の合計値を計算
        //            if (jokenPlus != "")
        //            {
        //                sumZandakaKingakuPlus = KanjoKamokuKingaku.Compute("SUM(zandakaKingaku)", jokenPlus);
        //                sumKarikataKingakuPlus = KanjoKamokuKingaku.Compute("SUM(karikataKingaku)", jokenPlus);
        //                sumKashikataKingakuPlus = KanjoKamokuKingaku.Compute("SUM(kashikataKingaku)", jokenPlus);
        //                sumTouzanKingakuPlus = KanjoKamokuKingaku.Compute("SUM(touzanKingaku)", jokenPlus);
        //            }
        //            else
        //            {
        //                sumZandakaKingakuPlus = 0;
        //                sumKarikataKingakuPlus = 0;
        //                sumKashikataKingakuPlus = 0;
        //                sumTouzanKingakuPlus = 0;
        //            }
        //            if (jokenMinus != "")
        //            {
        //                sumZandakaKingakuMinus = KanjoKamokuKingaku.Compute("SUM(zandakaKingaku)", jokenMinus);
        //                sumKarikataKingakuMinus = KanjoKamokuKingaku.Compute("SUM(karikataKingaku)", jokenMinus);
        //                sumKashikataKingakuMinus = KanjoKamokuKingaku.Compute("SUM(kashikataKingaku)", jokenMinus);
        //                sumTouzanKingakuMinus = KanjoKamokuKingaku.Compute("SUM(touzanKingaku)", jokenMinus);
        //            }
        //            else
        //            {
        //                sumZandakaKingakuMinus = 0;
        //                sumKarikataKingakuMinus = 0;
        //                sumKashikataKingakuMinus = 0;
        //                sumTouzanKingakuMinus = 0;
        //            }
        //            zenZanKingaku = Util.ToDecimal(sumZandakaKingakuPlus) + Util.ToDecimal(sumZandakaKingakuMinus);
        //            zandakaKingaku = Util.ToDecimal(sumTouzanKingakuPlus) + Util.ToDecimal(sumTouzanKingakuMinus);

        //            // 貸借区分が借方の場合
        //            if (daiKomokuTaishakuKubun == 1)
        //            {
        //                karikataKingaku = 0;
        //                kashikataKingaku = (zandakaKingaku + zenZanKingaku) * -1;
        //            }
        //            else
        //            {
        //                karikataKingaku = (zandakaKingaku - zenZanKingaku) * -1;
        //                kashikataKingaku = 0;
        //            }
        //            #endregion

        //            row = KanjoKamokuKingaku.NewRow();
        //            row["shukeiKeisanShiki"] = kamokuBunrui;
        //            row["zandakaKingaku"] = zenZanKingaku;
        //            row["karikataKingaku"] = karikataKingaku;
        //            row["kashikataKingaku"] = kashikataKingaku;
        //            row["touzanKingaku"] = zandakaKingaku;
        //            KanjoKamokuKingaku.Rows.Add(row);
        //        }
        //        j = 0;
        //        i++;
        //    }
        //    #endregion
        //}
        #endregion

        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {
            // 期間
            string kikan = "自 ";
            string[] aryJpDateFr = Util.ConvJpDate(Util.ToDate(this._pForm.Condition["DtFr"]), this._dba);
            kikan += aryJpDateFr[5];
            string[] aryJpDateTo = Util.ConvJpDate(Util.ToDate(this._pForm.Condition["DtTo"]), this._dba);
            //kikan += " 至 " + aryJpDateTo[5];
            kikan = "至 " + aryJpDateTo[5];

            // 出力日付
            string[] aryJpDateOt = Util.ConvJpDate(Util.ToDate(this._pForm.Condition["ShurutyokuDt"]), this._dba);
            string outPutDate = "出力日付 " + aryJpDateOt[5]; // Util.ToString(this._pForm.Condition["ShurutyokuDt"]);

            // 部門範囲を表示 ※開始と終了が未設定の場合は、【全社】
            string bumonRange;
            string bumonNmFr = Util.ToString(this._pForm.Condition["BumonNmFr"]);
            string bumonNmTo = Util.ToString(this._pForm.Condition["BumonNmTo"]);
            if (bumonNmFr == "先　頭" && bumonNmTo == "最　後")
            {
                bumonRange = "【全社】";
            }
            else
            {
                bumonRange = bumonNmFr + "　～　" + bumonNmTo;
            }

            // 残高タイトル
            // 現在設定されている会計年度を取得
            DateTime kaikeiNendoKaishiBi = Util.ToDate(this._uInfo.KaikeiSettings["KAIKEI_KIKAN_KAISHIBI"]);
            // 取得したデータを和暦に変換
            string[] warekiKaikeiNendoKaishiBiDate = Util.ConvJpDate(kaikeiNendoKaishiBi, this._dba);
            // 期間の開始日付が設定会計年度開始日付と一致する場合、期首残高を設定
            string zandakaNm;
            if (aryJpDateFr[6] == warekiKaikeiNendoKaishiBiDate[6])
            {
                zandakaNm = "期首残高";
            }
            // 期間の開始日が1日の場合、前月残高を設定
            else if (aryJpDateFr[4] == "1")
            {
                zandakaNm = "前月残高";
            }
            // 期間の開始日付が上記以外だった場合、前日残高を設定
            else
            {
                zandakaNm = "前日残高";
            }

            // 集計処理
            this._da = new ZMMR1071DA(this._uInfo, this._dba, this._config);
            this._da.Summary(this._pForm.Condition, this._pForm.UnqId);

            // データ登録
            decimal dbSORT = 1;
            DataView dv = null;
            // 貸借対照
            dv = new DataView(this._da.TaishakuTaishohyo.Copy());
            dv.RowFilter = "KAMOKU_BUNRUI_NM <> ''";
            this.MakeWkData_insertTable(ref dbSORT, "五期比較諸表［貸借対照表］", kikan, bumonRange, outPutDate, zandakaNm, dv.ToTable().Copy());
            // 損益計算
            dv = new DataView(this._da.SonekiKeisansho.Copy());
            dv.RowFilter = "KAMOKU_BUNRUI_NM <> ''";
            this.MakeWkData_insertTable(ref dbSORT, "五期比較諸表［損益計算書］", kikan, bumonRange, outPutDate, zandakaNm, dv.ToTable().Copy());

            return true;
        }

        /// <summary>
        /// 印刷データの登録処理。
        /// </summary>
        private void MakeWkData_insertTable(ref decimal dbSORT, string title, string kikan, string bumonRange, string outPutDate, string zandakaNm, DataTable dt)
        {
            #region 前準備
            int kamokuBunrui;

            decimal zenZanKingaku1;
            decimal zenZanKingaku2;
            decimal zenZanKingaku3;

            decimal zenZanKingaku;
            decimal karikataKingaku;
            decimal kashikataKingaku;
            decimal zandakaKingaku;

            String tmpKanjoKamokuNm;
            String tmpZenZanKingaku1;
            String tmpZenZanKingaku2;
            String tmpZenZanKingaku3;
            String tmpZenZanKingaku;
            String tmpKarikataKingaku;
            String tmpKashikataKingaku;
            String tmpZandakaKingaku;
            String moji_shubetsu;
            List<string> nendo;
            #endregion

            if (dt.Rows.Count > 0)
            {
                // 年度タイトル設定
                nendo = new List<string>();
                nendo.Add(this._da.KAIKEI_NENDO1);
                nendo.Add(this._da.KAIKEI_NENDO2);
                nendo.Add(this._da.KAIKEI_NENDO3);
                nendo.Add(this._da.KAIKEI_NENDO4);
                string[] aryJpDateNd;
                for (int n = 0; n < 4; n++)
                {
                    if (nendo[n] != "")
                    {
                        aryJpDateNd = Util.ConvJpDate(Util.ToDate(nendo[n] + "/01/01"), this._dba);
                        nendo[n] = aryJpDateNd[0] + aryJpDateNd[2] + " 年度";
                    }
                }

                DbParamCollection dpc = new DbParamCollection();
                StringBuilder Sql = new StringBuilder();
                foreach (DataRow r in dt.Rows)
                {
                    // 一旦金額を初期化
                    zenZanKingaku1 = 0;
                    zenZanKingaku2 = 0;
                    zenZanKingaku3 = 0;
                    zenZanKingaku = 0;
                    karikataKingaku = 0;
                    kashikataKingaku = 0;
                    zandakaKingaku = 0;

                    zenZanKingaku1 = Util.ToDecimal(r["ZANDAKA_KINGAKUZEN1"]);
                    zenZanKingaku2 = Util.ToDecimal(r["ZANDAKA_KINGAKUZEN2"]);
                    zenZanKingaku3 = Util.ToDecimal(r["ZANDAKA_KINGAKUZEN3"]);

                    zenZanKingaku = Util.ToDecimal(r["ZANDAKA_KINGAKU"]);
                    karikataKingaku = Util.ToDecimal(r["KARIKATA_KINGAKU"]);
                    kashikataKingaku = Util.ToDecimal(r["KASHIKATA_KINGAKU"]);
                    zandakaKingaku = Util.ToDecimal(r["TOUZAN_KINGAKU"]);

                    kamokuBunrui = Util.ToInt(r["KAMOKU_BUNRUI"]);

                    moji_shubetsu = Util.ToString(r["MOJI_SHUBETSU"]);

                    #region 全て同じ扱いに
                    if (zenZanKingaku1 != 0)
                    {
                        tmpZenZanKingaku1 = Util.FormatNum(Util.ToString(zenZanKingaku1));
                    }
                    else
                    {
                        tmpZenZanKingaku1 = "";
                    }
                    if (zenZanKingaku2 != 0)
                    {
                        tmpZenZanKingaku2 = Util.FormatNum(Util.ToString(zenZanKingaku2));
                    }
                    else
                    {
                        tmpZenZanKingaku2 = "";
                    }
                    if (zenZanKingaku3 != 0)
                    {
                        tmpZenZanKingaku3 = Util.FormatNum(Util.ToString(zenZanKingaku3));
                    }
                    else
                    {
                        tmpZenZanKingaku3 = "";
                    }

                    if (zenZanKingaku != 0)//  < 0
                    {
                        tmpZenZanKingaku = Util.FormatNum(Util.ToString(zenZanKingaku));
                    }
                    else
                    {
                        tmpZenZanKingaku = "";
                    }

                    if (karikataKingaku != 0)
                    {
                        tmpKarikataKingaku = Util.FormatNum(Util.ToString(karikataKingaku));
                    }
                    else
                    {
                        tmpKarikataKingaku = "";
                    }

                    if (kashikataKingaku != 0)
                    {
                        tmpKashikataKingaku = Util.FormatNum(Util.ToString(kashikataKingaku));
                    }
                    else
                    {
                        tmpKashikataKingaku = "";
                    }

                    if (zandakaKingaku != 0)
                    {
                        tmpZandakaKingaku = Util.FormatNum(Util.ToString(zandakaKingaku));
                    }
                    else
                    {
                        tmpZandakaKingaku = "";
                    }
                    #endregion

                    Sql = new StringBuilder();
                    dpc = new DbParamCollection();
                    Sql.Append("INSERT INTO PR_ZM_TBL(");
                    Sql.Append("  GUID");
                    Sql.Append(" ,SORT");
                    Sql.Append(" ,ITEM01");
                    Sql.Append(" ,ITEM02");
                    Sql.Append(" ,ITEM03");
                    Sql.Append(" ,ITEM04");
                    Sql.Append(" ,ITEM05");
                    Sql.Append(" ,ITEM06");
                    Sql.Append(" ,ITEM07");
                    Sql.Append(" ,ITEM08");
                    Sql.Append(" ,ITEM09");
                    Sql.Append(" ,ITEM10");
                    Sql.Append(" ,ITEM11");
                    Sql.Append(" ,ITEM12");
                    Sql.Append(" ,ITEM13");
                    Sql.Append(" ,ITEM14");
                    Sql.Append(" ,ITEM15");
                    Sql.Append(" ,ITEM16");
                    Sql.Append(" ,ITEM17");
                    Sql.Append(" ,ITEM18");
                    Sql.Append(") ");
                    Sql.Append("VALUES(");
                    Sql.Append("  @GUID");
                    Sql.Append(" ,@SORT");
                    Sql.Append(" ,@ITEM01");
                    Sql.Append(" ,@ITEM02");
                    Sql.Append(" ,@ITEM03");
                    Sql.Append(" ,@ITEM04");
                    Sql.Append(" ,@ITEM05");
                    Sql.Append(" ,@ITEM06");
                    Sql.Append(" ,@ITEM07");
                    Sql.Append(" ,@ITEM08");
                    Sql.Append(" ,@ITEM09");
                    Sql.Append(" ,@ITEM10");
                    Sql.Append(" ,@ITEM11");
                    Sql.Append(" ,@ITEM12");
                    Sql.Append(" ,@ITEM13");
                    Sql.Append(" ,@ITEM14");
                    Sql.Append(" ,@ITEM15");
                    Sql.Append(" ,@ITEM16");
                    Sql.Append(" ,@ITEM17");
                    Sql.Append(" ,@ITEM18");
                    Sql.Append(") ");

                    // 金額がｾﾞﾛの科目を印字する場合、あるいは金額がｾﾞﾛの科目を印字しない場合かつ金額が0でない場合
                    if ((Util.ToString(this._pForm.Condition["Inji"]) == "yes")
                        || (tmpZenZanKingaku1 != "" || tmpZenZanKingaku2 != "" || tmpZenZanKingaku3 != "" || tmpZenZanKingaku != "" || tmpKarikataKingaku != "" || tmpKashikataKingaku != "" || tmpZandakaKingaku != ""))
                    {

                        dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this._unqId);
                        dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
                        dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, title); // タイトル
                        dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, bumonRange); // 部門名
                        dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, kikan); // 期間
                        dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, this._uInfo.KaishaNm); // 会社名
                        dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, Util.ToString(this._pForm.Condition["ShohizeiShori"])); // 税抜きor税込み

                        dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, nendo[0]); // タイトル1
                        dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, nendo[1]); // タイトル2
                        dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, nendo[2]); // タイトル3
                        dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, nendo[3]); // タイトル4

                        tmpKanjoKamokuNm = Util.ToString(r["KAMOKU_BUNRUI_NM"]);

                        dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, tmpKanjoKamokuNm); // 科目名

                        dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, tmpZenZanKingaku1); // 前前前前期残高
                        dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, tmpZenZanKingaku2); // 前前前期残高
                        dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200, tmpZenZanKingaku3); // 前前期残高

                        dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, tmpZenZanKingaku); // 前期残高
                        dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200, tmpZandakaKingaku); // 当期

                        dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, title); // 改ページ

                        dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, outPutDate); // 出力日付
                        dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, moji_shubetsu); // 文字種別

                        this._dba.ModifyBySql(Util.ToString(Sql), dpc);

                        dbSORT++;
                    }
                }
            }
        }

        /// <summary>
        /// ファイル保存先の表示
        /// </summary>
        /// <param name="fName"></param>
        /// <param name="filetype"></param>
        /// <returns></returns>
        private string GetSavePath(string fName, decimal filetype)
        {
            SaveFileDialog sfd = new SaveFileDialog();

            sfd.FileName = fName;
            string fType = "";
            if (filetype == 1)
            {
                fType = "PDF";
            }
            else
            {
                fType = "EXCEL";
            }
            string folderName = @"C:\";
            try
            {
                folderName = this._config.LoadPgConfig(Constants.SubSys.Han, "OUTPUT", fType, "PATH");
            }
            catch (Exception)
            {
                folderName = @"C:\";
            }
            if (ValChk.IsEmpty(folderName))
            {
                folderName = @"C:\";
            }
            sfd.InitialDirectory = folderName;

            sfd.AddExtension = true;

            if (filetype == 1)
            {
                sfd.Title = "PDFファイルの保存";
                sfd.Filter = "全てのファイル(*.*)|*.*|" +
                             "ＰＤＦファイル(*.pdf)|*.pdf";
            }
            else
            {
                sfd.Title = "EXCELファイルの保存";
                sfd.Filter = "全てのファイル(*.*)|*.*|" +
                             "Excelファイル(*.xlsx)|*.xlsx";
            }
            sfd.FilterIndex = 2;
            DialogResult ret = sfd.ShowDialog();
            if (ret == DialogResult.OK)
            {
                folderName = System.IO.Path.GetDirectoryName(sfd.FileName);
                try
                {
                    this._config.SetPgConfig(Constants.SubSys.Han, "OUTPUT", fType, "PATH", folderName);
                    this._config.SaveConfig();
                }
                catch (Exception) { }
                return sfd.FileName;
            }
            else
            {
                return "";
            }
        }

        #region コメント
        ///// <summary>
        ///// インサートテーブルを作成します。
        ///// </summary>
        //private StringBuilder MakeWkData_insertTable(StringBuilder Sql, int maxNo)
        //{
        //    Sql.Append("INSERT INTO PR_ZM_TBL(");
        //    Sql.Append("  GUID");
        //    Sql.Append(" ,SORT");
        //    Sql.Append(" ,ITEM01");
        //    Sql.Append(" ,ITEM02");
        //    Sql.Append(" ,ITEM03");
        //    Sql.Append(" ,ITEM04");
        //    Sql.Append(" ,ITEM05");
        //    Sql.Append(" ,ITEM06");
        //    Sql.Append(" ,ITEM07");
        //    Sql.Append(" ,ITEM08");
        //    Sql.Append(" ,ITEM09");
        //    for (int itemNo = 10; itemNo <= maxNo; itemNo++)
        //    {
        //        Sql.Append(" ,ITEM" + itemNo);
        //    }
        //    Sql.Append(") ");
        //    Sql.Append("VALUES(");
        //    Sql.Append("  @GUID");
        //    Sql.Append(" ,@SORT");
        //    Sql.Append(" ,@ITEM01");
        //    Sql.Append(" ,@ITEM02");
        //    Sql.Append(" ,@ITEM03");
        //    Sql.Append(" ,@ITEM04");
        //    Sql.Append(" ,@ITEM05");
        //    Sql.Append(" ,@ITEM06");
        //    Sql.Append(" ,@ITEM07");
        //    Sql.Append(" ,@ITEM08");
        //    Sql.Append(" ,@ITEM09");
        //    for (int itemNo = 10; itemNo <= maxNo; itemNo++)
        //    {
        //        Sql.Append(" ,@ITEM" + itemNo);
        //    }
        //    Sql.Append(") ");

        //    return Sql;
        //}

        ///// <summary>
        ///// 貸借対照表のデータを作成します。
        ///// </summary>
        //private void MakeWkData_taishaku(string kikan, string bumonRange, string outPutDate, string zandakaNm)
        //{
        //    #region 前準備
        //    int dbSORT = 0;
        //    StringBuilder Sql = new StringBuilder();
        //    DbParamCollection dpc = new DbParamCollection();
        //    ZMMR1031DA da = new ZMMR1031DA(this._uInfo, this._dba, this._config);

        //    int addRowCount = 5;

        //    String itemNo01 = "08";
        //    String itemNo02 = "09";
        //    String itemNo03 = "10";
        //    String itemNo04 = "11";
        //    String itemNo05 = "12";

        //    String itemNm = "@ITEM";

        //    DataTable kanjoKamokuIchiran;
        //    DataRow[] dataRows;
        //    Decimal[] kingakuDate;
        //    String joken;
        //    int kamokuBunrui;
        //    int meisaiKomokuSu;
        //    int cnt;
        //    int meisaiCnt;
        //    int dataCnt = 1;
        //    //int handHouteiJunbi = 13320;
        //    int handHouteiJunbi = this._pForm.KAMOKU_BUNRUI_HOTE_JUNBIKIN;

        //    decimal zenZanKingaku;
        //    decimal karikataKingaku;
        //    decimal kashikataKingaku;
        //    decimal zandakaKingaku;
        //    decimal houteiJunbiKin = 0;

        //    decimal toukiZandaka = 0;
        //    decimal toukiKarikata = 0;
        //    decimal toukiKashikata = 0;
        //    decimal toukiTouzan = 0;
        //    String tmpKanjoKamokuNm;
        //    String tmpZenZanKingaku;
        //    String tmpKarikataKingaku;
        //    String tmpKashikataKingaku;
        //    String tmpZandakaKingaku;

        //    // ページ最終表示順位番号
        //    String hyojiNo_taishaku01 = "220";
        //    String hyojiNo_taishaku02 = "420";
        //    #endregion

        //    /* 貸借対照表(P1)を準備 */
        //    Sql = MakeWkData_insertTable(new StringBuilder(), taishaku01);
        //    dpc = MakeWkData_setDpc(dbSORT, kikan, bumonRange, outPutDate, zandakaNm);

        //    foreach (DataRow date in TaishakuTaishohyo.Rows)
        //    {
        //        // 一旦金額を初期化
        //        zenZanKingaku = 0;
        //        karikataKingaku = 0;
        //        kashikataKingaku = 0;
        //        zandakaKingaku = 0;

        //        // 明細項目数を取得
        //        meisaiKomokuSu = Util.ToInt(date["MEISAI_KOMOKUSU"]);
        //        kamokuBunrui = Util.ToInt(date["KAMOKU_BUNRUI"]);
        //        kanjoKamokuIchiran = da.GetKamokuShoKomoku(LIST_F1, kamokuBunrui);
        //        joken = "shukeiKeisanShiki = " + kamokuBunrui;
        //        dataRows = KanjoKamokuKingaku.Select(joken);

        //        #region 小項目データをワークテーブルに登録
        //        cnt = 0;
        //        meisaiCnt = 0;
        //        foreach (DataRow subDate in kanjoKamokuIchiran.Rows)
        //        {
        //            int kanjoKamokuCd = Util.ToInt(kanjoKamokuIchiran.Rows[cnt]["KANJO_KAMOKU_CD"]);
        //            if (meisaiKomokuSu <= meisaiCnt)
        //            {
        //                break;
        //            }

        //            #region 金額データを取得
        //            // 現在の行番号と次の行番号が同じ場合
        //            try
        //            {
        //                if (Util.ToInt(subDate["GYO_BANGO"]) == Util.ToInt(kanjoKamokuIchiran.Rows[cnt + 1]["GYO_BANGO"]))
        //                {
        //                    zenZanKingaku += Util.ToDecimal(dataRows[cnt]["zandakaKingaku"]);
        //                    karikataKingaku += Util.ToDecimal(dataRows[cnt]["karikataKingaku"]);
        //                    kashikataKingaku += Util.ToDecimal(dataRows[cnt]["kashikataKingaku"]);
        //                    zandakaKingaku += Util.ToDecimal(dataRows[cnt]["touzanKingaku"]);
        //                    cnt++;
        //                    continue;
        //                }
        //            }
        //            catch (Exception)
        //            {
        //            }

        //            zenZanKingaku += Util.ToDecimal(dataRows[cnt]["zandakaKingaku"]);
        //            karikataKingaku += Util.ToDecimal(dataRows[cnt]["karikataKingaku"]);
        //            kashikataKingaku += Util.ToDecimal(dataRows[cnt]["kashikataKingaku"]);
        //            zandakaKingaku += Util.ToDecimal(dataRows[cnt]["touzanKingaku"]);

        //            //勘定科目CD141以上147以下
        //            //if (141 <= kanjoKamokuCd && kanjoKamokuCd <= 147)
        //            if (this._pForm.KAMOKU_BUNRUI_KEIZAI_JIGYO_MISHUKIN == kamokuBunrui)
        //            {
        //                #region 科目分類11133のみ
        //                if (zenZanKingaku != 0)//  < 0
        //                {
        //                    tmpZenZanKingaku = Util.FormatNum(Util.ToString(zenZanKingaku));
        //                }
        //                else
        //                {
        //                    tmpZenZanKingaku = "";
        //                }

        //                if (karikataKingaku != 0)
        //                {
        //                    tmpKarikataKingaku = Util.FormatNum(Util.ToString(karikataKingaku));
        //                }
        //                else
        //                {
        //                    tmpKarikataKingaku = "";
        //                }

        //                if (kashikataKingaku != 0)
        //                {
        //                    tmpKashikataKingaku = Util.FormatNum(Util.ToString(kashikataKingaku));
        //                }
        //                else
        //                {
        //                    tmpKashikataKingaku = "";
        //                }

        //                if (zandakaKingaku != 0)
        //                {
        //                    tmpZandakaKingaku = Util.FormatNum(Util.ToString(zandakaKingaku));
        //                }
        //                else
        //                {
        //                    tmpZandakaKingaku = "";
        //                }
        //                #endregion
        //            }
        //            else
        //            {
        //                #region それ以外
        //                if (zenZanKingaku > 0)
        //                {
        //                    tmpZenZanKingaku = Util.FormatNum(Util.ToString(zenZanKingaku));
        //                }
        //                else if (zenZanKingaku < 0)
        //                {
        //                    tmpZenZanKingaku = Util.FormatNum(Util.ToString(zenZanKingaku * -1));
        //                }
        //                else
        //                {
        //                    tmpZenZanKingaku = "";
        //                }
        //                if (karikataKingaku > 0)
        //                {
        //                    tmpKarikataKingaku = Util.FormatNum(Util.ToString(karikataKingaku));
        //                }
        //                else if (karikataKingaku < 0)
        //                {
        //                    tmpKarikataKingaku = Util.FormatNum(Util.ToString(karikataKingaku * -1));
        //                }
        //                else
        //                {
        //                    tmpKarikataKingaku = "";
        //                }
        //                if (kashikataKingaku > 0)
        //                {
        //                    tmpKashikataKingaku = Util.FormatNum(Util.ToString(kashikataKingaku));
        //                }
        //                else if (kashikataKingaku < 0)
        //                {
        //                    tmpKashikataKingaku = Util.FormatNum(Util.ToString(kashikataKingaku * -1));
        //                }
        //                else
        //                {
        //                    tmpKashikataKingaku = "";
        //                }
        //                if (zandakaKingaku > 0)
        //                {
        //                    tmpZandakaKingaku = Util.FormatNum(Util.ToString(zandakaKingaku));
        //                }
        //                else if (zandakaKingaku < 0)
        //                {
        //                    tmpZandakaKingaku = Util.FormatNum(Util.ToString(zandakaKingaku * -1));
        //                }
        //                else
        //                {
        //                    tmpZandakaKingaku = "";
        //                }
        //                #endregion
        //            }
        //            #endregion
        //            // 金額がｾﾞﾛの科目を印字する場合、あるいは金額がｾﾞﾛの科目を印字しない場合かつ金額が0でない場合
        //            if ((Util.ToString(this._pForm.Condition["Inji"]) == "yes")
        //                || (tmpZenZanKingaku != "" || tmpKarikataKingaku != "" || tmpKashikataKingaku != "" || tmpZandakaKingaku != ""))
        //            {
        //                tmpKanjoKamokuNm = Util.ToString(subDate["KANJO_KAMOKU_NM"]);

        //                dpc.SetParam(itemNm + itemNo01, SqlDbType.VarChar, 200, tmpKanjoKamokuNm); // 科目名
        //                dpc.SetParam(itemNm + itemNo02, SqlDbType.VarChar, 200, tmpZenZanKingaku); // 前日残高
        //                dpc.SetParam(itemNm + itemNo03, SqlDbType.VarChar, 200, tmpKarikataKingaku); // 貸方
        //                dpc.SetParam(itemNm + itemNo04, SqlDbType.VarChar, 200, tmpKashikataKingaku); // 借方
        //                dpc.SetParam(itemNm + itemNo05, SqlDbType.VarChar, 200, tmpZandakaKingaku); // 当残

        //                // ITEM名称をCOUNTUP
        //                itemNo01 = Util.ToString(Util.ToDecimal(itemNo01) + addRowCount);
        //                itemNo02 = Util.ToString(Util.ToDecimal(itemNo02) + addRowCount);
        //                itemNo03 = Util.ToString(Util.ToDecimal(itemNo03) + addRowCount);
        //                itemNo04 = Util.ToString(Util.ToDecimal(itemNo04) + addRowCount);
        //                itemNo05 = Util.ToString(Util.ToDecimal(itemNo05) + addRowCount);

        //                meisaiCnt++;
        //            }

        //            // 現在の行番号と次の行番号が一致しない場合
        //            try
        //            {
        //                if (Util.ToInt(subDate["GYO_BANGO"]) != Util.ToInt(kanjoKamokuIchiran.Rows[cnt + 1]["GYO_BANGO"]))
        //                {
        //                    zenZanKingaku = 0;
        //                    karikataKingaku = 0;
        //                    kashikataKingaku = 0;
        //                    zandakaKingaku = 0;
        //                }
        //            }
        //            catch (Exception)
        //            {
        //            }

        //            cnt++;
        //        }

        //        // 指定明細数に満たない場合、空行をセット
        //        while (meisaiKomokuSu > meisaiCnt)
        //        {
        //            dpc.SetParam(itemNm + itemNo01, SqlDbType.VarChar, 200, ""); // 科目名
        //            dpc.SetParam(itemNm + itemNo02, SqlDbType.VarChar, 200, ""); // 前日残高
        //            dpc.SetParam(itemNm + itemNo03, SqlDbType.VarChar, 200, ""); // 貸方
        //            dpc.SetParam(itemNm + itemNo04, SqlDbType.VarChar, 200, ""); // 借方
        //            dpc.SetParam(itemNm + itemNo05, SqlDbType.VarChar, 200, ""); // 当残

        //            // ITEM名称をCOUNTUP
        //            itemNo01 = Util.ToString(Util.ToDecimal(itemNo01) + addRowCount);
        //            itemNo02 = Util.ToString(Util.ToDecimal(itemNo02) + addRowCount);
        //            itemNo03 = Util.ToString(Util.ToDecimal(itemNo03) + addRowCount);
        //            itemNo04 = Util.ToString(Util.ToDecimal(itemNo04) + addRowCount);
        //            itemNo05 = Util.ToString(Util.ToDecimal(itemNo05) + addRowCount);

        //            meisaiCnt++;
        //        }
        //        #endregion

        //        #region 大項目データをワークテーブルに登録
        //        // 明細区分が0以外の場合
        //        if (Util.ToInt(date["MEISAI_KUBUN"]) != 0)
        //        {
        //            kingakuDate = getKingakuDate01(date);
        //        }
        //        // 明細区分が0の場合
        //        else
        //        {
        //            kingakuDate = getKingakuDate02(date);
        //        }
        //        // 勘定科目が法定準備金の場合金額を保持
        //        if (Util.ToInt(date["KAMOKU_BUNRUI"]) == handHouteiJunbi)
        //        {
        //            houteiJunbiKin = kingakuDate[4];
        //            TaishakuHoteiJunbikin[0] = houteiJunbiKin;
        //        }

        //        #region 勘定科目が"当期未処分剰余金"、"(うち当期剰余金)"の場合
        //        // 名護へ合わせ
        //        //if (TaishakuTaishohyo.Rows.Count - 3 == dataCnt || TaishakuTaishohyo.Rows.Count - 2 == dataCnt)
        //        if (TaishakuTaishohyo.Rows.Count - 4 == dataCnt || TaishakuTaishohyo.Rows.Count - 3 == dataCnt)
        //        {
        //            // 対象区分が借方の場合
        //            if (Util.ToInt(date["TAISHAKU_KUBUN"]) == 1)
        //            {
        //                kingakuDate[1] = kingakuDate[3] - kingakuDate[0];
        //                kingakuDate[2] = kingakuDate[3] + kingakuDate[0];
        //                // 当残が0未満の場合
        //                if (kingakuDate[3] < 0)
        //                {
        //                    // 借方発生
        //                    kingakuDate[1] = 0;
        //                }
        //            }
        //            // 対象区分が貸方の場合
        //            else
        //            {
        //                //kingakuDate[0] = kingakuDate[0] * -1;
        //                kingakuDate[1] = kingakuDate[3] + kingakuDate[0];
        //                //kingakuDate[3] = kingakuDate[3] * -1;
        //                kingakuDate[2] = kingakuDate[3] - kingakuDate[0];
        //                // 勘定科目が"(うち当期剰余金)"の場合
        //                // 名護へ合わせ
        //                //if (TaishakuTaishohyo.Rows.Count - 3 != dataCnt)
        //                if (TaishakuTaishohyo.Rows.Count - 4 != dataCnt)
        //                {
        //                    // 当残が0以上の場合
        //                    if (kingakuDate[3] >= 0)
        //                    {
        //                        // 借方発生
        //                        kingakuDate[1] = 0;
        //                    }
        //                    // 0未満の場合
        //                    else
        //                    {
        //                        // 貸方発生
        //                        kingakuDate[2] = 0;
        //                    }
        //                }
        //                else
        //                {
        //                    // 当残が0以上の場合
        //                    if (kingakuDate[3] >= 0)
        //                    {
        //                        // 借方発生
        //                        kingakuDate[1] = houteiJunbiKin;
        //                        kingakuDate[3] = kingakuDate[0] + kingakuDate[2] - kingakuDate[1];
        //                    }
        //                    // 0以下の場合
        //                    else
        //                    {
        //                        // 貸方発生
        //                        kingakuDate[2] = 0;
        //                    }
        //                }
        //            }

        //            // 名護へ合わせ
        //            //if (TaishakuTaishohyo.Rows.Count - 3 == dataCnt)// 当期未処分剰余金を保持
        //            if (TaishakuTaishohyo.Rows.Count - 4 == dataCnt)// 当期未処分剰余金を保持
        //            {
        //                toukiZandaka = kingakuDate[0];
        //                toukiKarikata = kingakuDate[1];
        //                toukiKashikata = kingakuDate[2];
        //                toukiTouzan = kingakuDate[3];
        //            }
        //        }
        //        else if (TaishakuTaishohyo.Rows.Count - 1 == dataCnt || TaishakuTaishohyo.Rows.Count - 0 == dataCnt)// "資本の部合計"、"負債・資本の部合計"の場合
        //        {
        //            kingakuDate[0] = kingakuDate[0] + toukiZandaka;
        //            kingakuDate[1] = kingakuDate[1] + toukiKarikata;
        //            kingakuDate[2] = kingakuDate[2] + toukiKashikata;
        //            kingakuDate[3] = kingakuDate[3] + toukiTouzan;
        //        }

        //        // 2016-05-11 追記 kisemori
        //        // 法定準備金と当期未処分剰余金を計算
        //        // 名護へ合わせ
        //        //if (TaishakuTaishohyo.Rows.Count - 3 == dataCnt || TaishakuTaishohyo.Rows.Count - 1 == dataCnt || TaishakuTaishohyo.Rows.Count == dataCnt)
        //        if (TaishakuTaishohyo.Rows.Count - 4 == dataCnt || TaishakuTaishohyo.Rows.Count - 1 == dataCnt || TaishakuTaishohyo.Rows.Count == dataCnt)
        //        {
        //            kingakuDate[0] = kingakuDate[0] - hoteJunbikin + tokiMishobunJoyokin;
        //            kingakuDate[3] = kingakuDate[3] - hoteJunbikin + tokiMishobunJoyokin;
        //        }
        //        #endregion

        //        // 金額がｾﾞﾛの科目を印字しない場合
        //        if (Util.ToString(this._pForm.Condition["Inji"]) == "no" &&
        //            kingakuDate[0] == 0 && kingakuDate[1] == 0 && kingakuDate[2] == 0 && kingakuDate[3] == 0)
        //        {
        //            tmpKanjoKamokuNm = "";
        //            tmpZenZanKingaku = "";
        //            tmpKarikataKingaku = "";
        //            tmpKashikataKingaku = "";
        //            tmpZandakaKingaku = "";
        //        }
        //        else
        //        {

        //            tmpKanjoKamokuNm = Util.ToString(date["KAMOKU_BUNRUI_NM"]);
        //            tmpZenZanKingaku = Util.FormatNum(kingakuDate[0]);
        //            tmpKarikataKingaku = Util.FormatNum(kingakuDate[1]);
        //            tmpKashikataKingaku = Util.FormatNum(kingakuDate[2]);
        //            tmpZandakaKingaku = Util.FormatNum(kingakuDate[3]);
        //        }

        //        dpc.SetParam(itemNm + itemNo01, SqlDbType.VarChar, 200, tmpKanjoKamokuNm); // 科目名
        //        dpc.SetParam(itemNm + itemNo02, SqlDbType.VarChar, 200, tmpZenZanKingaku); // 前日残高
        //        dpc.SetParam(itemNm + itemNo03, SqlDbType.VarChar, 200, tmpKarikataKingaku); // 貸方
        //        dpc.SetParam(itemNm + itemNo04, SqlDbType.VarChar, 200, tmpKashikataKingaku); // 借方
        //        dpc.SetParam(itemNm + itemNo05, SqlDbType.VarChar, 200, tmpZandakaKingaku); // 当残

        //        // ITEM名称をCOUNTUP
        //        itemNo01 = Util.ToString(Util.ToDecimal(itemNo01) + addRowCount);
        //        itemNo02 = Util.ToString(Util.ToDecimal(itemNo02) + addRowCount);
        //        itemNo03 = Util.ToString(Util.ToDecimal(itemNo03) + addRowCount);
        //        itemNo04 = Util.ToString(Util.ToDecimal(itemNo04) + addRowCount);
        //        itemNo05 = Util.ToString(Util.ToDecimal(itemNo05) + addRowCount);
        //        #endregion

        //        // 1P目の最後の項目でインサート.
        //        if (Util.ToString(date["HYOJI_JUNI"]) == hyojiNo_taishaku01)
        //        {
        //            // インサート処理を実行
        //            this._dba.ModifyBySql(Util.ToString(Sql), dpc);

        //            /* 貸借対照表(P2)を準備*/
        //            dbSORT++;
        //            Sql = MakeWkData_insertTable(new StringBuilder(), taishaku02);
        //            dpc = MakeWkData_setDpc(dbSORT, kikan, bumonRange, outPutDate, zandakaNm);

        //            itemNo01 = "08";
        //            itemNo02 = "09";
        //            itemNo03 = "10";
        //            itemNo04 = "11";
        //            itemNo05 = "12";
        //        }
        //        // 2P目の最後の項目でインサート.
        //        else if (Util.ToString(date["HYOJI_JUNI"]) == hyojiNo_taishaku02)
        //        {
        //            // インサート処理を実行
        //            this._dba.ModifyBySql(Util.ToString(Sql), dpc);
        //        }

        //        dataCnt++;
        //    }
        //}

        ///// <summary>
        ///// 損益計算書のデータを作成します。
        ///// </summary>
        //private void MakeWkData_soneki(string kikan, string bumonRange, string outPutDate, string zandakaNm)
        //{
        //    #region 前準備
        //    int dbSORT = 2;
        //    StringBuilder Sql = new StringBuilder();
        //    DbParamCollection dpc = new DbParamCollection();
        //    ZMMR1031DA da = new ZMMR1031DA(this._uInfo, this._dba, this._config);

        //    int addRowCount = 5;

        //    String itemNo01 = "08";
        //    String itemNo02 = "09";
        //    String itemNo03 = "10";
        //    String itemNo04 = "11";
        //    String itemNo05 = "12";

        //    String itemNm = "@ITEM";

        //    DataTable kanjoKamokuIchiran;
        //    DataRow[] dataRows;
        //    Decimal[] kingakuDate;
        //    String joken;
        //    int kamokuBunrui;
        //    int meisaiKomokuSu;
        //    int cnt;
        //    int meisaicnt;
        //    int dataCnt = 1;

        //    decimal zenZanKingakuData;
        //    decimal karikataKingakuData;
        //    decimal kashikataKingakuData;
        //    decimal zandakaKingakuData;

        //    String tmpZenZanKingaku;
        //    String tmpKarikataKingaku;
        //    String tmpKashikataKingaku;
        //    String tmpZandakaKingaku;

        //    // ページ最終表示順位番号
        //    String hyojiNo_soneki01 = "150";
        //    String hyojiNo_soneki02 = "350";
        //    String hyojiNo_soneki03 = "440";
        //    String hyojiNo_soneki04 = "510";
        //    #endregion

        //    /* 損益計算書(P1) */
        //    Sql = MakeWkData_insertTable(new StringBuilder(), soneki01);
        //    dpc = MakeWkData_setDpc(dbSORT, kikan, bumonRange, outPutDate, zandakaNm);

        //    foreach (DataRow date in SonekiKeisansho.Rows)
        //    {
        //        // 一旦金額を初期化
        //        zenZanKingakuData = 0;
        //        karikataKingakuData = 0;
        //        kashikataKingakuData = 0;
        //        zandakaKingakuData = 0;

        //        // 明細項目数を取得
        //        meisaiKomokuSu = Util.ToInt(date["MEISAI_KOMOKUSU"]);
        //        kamokuBunrui = Util.ToInt(date["KAMOKU_BUNRUI"]);
        //        kanjoKamokuIchiran = da.GetKamokuShoKomoku(LIST_F2, kamokuBunrui);
        //        joken = "shukeiKeisanShiki = " + kamokuBunrui;
        //        dataRows = KanjoKamokuKingaku.Select(joken);

        //        #region 小項目データをワークテーブルに登録
        //        cnt = 0;
        //        meisaicnt = 0;
        //        foreach (DataRow subDate in kanjoKamokuIchiran.Rows)
        //        {
        //            if (meisaiKomokuSu <= meisaicnt)
        //            {
        //                break;
        //            }

        //            #region 金額データを取得
        //            // 現在の行番号と次の行番号が同じ場合
        //            try
        //            {
        //                if (Util.ToInt(subDate["GYO_BANGO"]) == Util.ToInt(kanjoKamokuIchiran.Rows[cnt + 1]["GYO_BANGO"]))
        //                {
        //                    zenZanKingakuData += Util.ToDecimal(dataRows[cnt]["zandakaKingaku"]);
        //                    karikataKingakuData += Util.ToDecimal(dataRows[cnt]["karikataKingaku"]);
        //                    kashikataKingakuData += Util.ToDecimal(dataRows[cnt]["kashikataKingaku"]);
        //                    zandakaKingakuData += Util.ToDecimal(dataRows[cnt]["touzanKingaku"]);
        //                    cnt++;
        //                    continue;
        //                }
        //            }
        //            catch (Exception)
        //            {
        //            }

        //            zenZanKingakuData += Util.ToDecimal(dataRows[cnt]["zandakaKingaku"]);
        //            karikataKingakuData += Util.ToDecimal(dataRows[cnt]["karikataKingaku"]);
        //            kashikataKingakuData += Util.ToDecimal(dataRows[cnt]["kashikataKingaku"]);
        //            zandakaKingakuData += Util.ToDecimal(dataRows[cnt]["touzanKingaku"]);

        //            if (zenZanKingakuData < 0)
        //            {
        //                zenZanKingakuData = zenZanKingakuData * -1;
        //            }
        //            if (karikataKingakuData < 0)
        //            {
        //                karikataKingakuData = karikataKingakuData * -1;
        //            }
        //            if (kashikataKingakuData < 0)
        //            {
        //                kashikataKingakuData = kashikataKingakuData * -1;
        //            }
        //            if (zandakaKingakuData < 0)
        //            {
        //                zandakaKingakuData = zandakaKingakuData * -1;
        //            }
        //            #endregion

        //            // 金額がｾﾞﾛの科目を印字する場合、あるいは金額がｾﾞﾛの科目を印字しない場合かつ金額が0でない場合
        //            if (Util.ToString(this._pForm.Condition["Inji"]) == "yes" ||
        //                    zenZanKingakuData != 0 || karikataKingakuData != 0 || kashikataKingakuData != 0 || zandakaKingakuData != 0)
        //            {
        //                tmpZenZanKingaku = Util.FormatNum(zenZanKingakuData);
        //                tmpKarikataKingaku = Util.FormatNum(karikataKingakuData);
        //                tmpKashikataKingaku = Util.FormatNum(kashikataKingakuData);
        //                tmpZandakaKingaku = Util.FormatNum(zandakaKingakuData);

        //                dpc.SetParam(itemNm + itemNo01, SqlDbType.VarChar, 200, Util.ToString(subDate["KANJO_KAMOKU_NM"])); // 科目名
        //                dpc.SetParam(itemNm + itemNo02, SqlDbType.VarChar, 200, tmpZenZanKingaku); // 前日残高
        //                dpc.SetParam(itemNm + itemNo03, SqlDbType.VarChar, 200, tmpKarikataKingaku); // 貸方
        //                dpc.SetParam(itemNm + itemNo04, SqlDbType.VarChar, 200, tmpKashikataKingaku); // 借方
        //                dpc.SetParam(itemNm + itemNo05, SqlDbType.VarChar, 200, tmpZandakaKingaku); // 当残

        //                // ITEM名称をCOUNTUP
        //                itemNo01 = Util.ToString(Util.ToDecimal(itemNo01) + addRowCount);
        //                itemNo02 = Util.ToString(Util.ToDecimal(itemNo02) + addRowCount);
        //                itemNo03 = Util.ToString(Util.ToDecimal(itemNo03) + addRowCount);
        //                itemNo04 = Util.ToString(Util.ToDecimal(itemNo04) + addRowCount);
        //                itemNo05 = Util.ToString(Util.ToDecimal(itemNo05) + addRowCount);

        //                meisaicnt++;
        //            }

        //            // 現在の行番号と次の行番号が一致しない場合
        //            try
        //            {
        //                if (Util.ToInt(subDate["GYO_BANGO"]) != Util.ToInt(kanjoKamokuIchiran.Rows[cnt + 1]["GYO_BANGO"]))
        //                {
        //                    zenZanKingakuData = 0;
        //                    karikataKingakuData = 0;
        //                    kashikataKingakuData = 0;
        //                    zandakaKingakuData = 0;
        //                }
        //            }
        //            catch (Exception)
        //            {
        //            }

        //            cnt++;
        //        }

        //        // 指定明細数に満たない場合、空行をセット
        //        while (meisaiKomokuSu > meisaicnt)
        //        {
        //            dpc.SetParam(itemNm + itemNo01, SqlDbType.VarChar, 200, ""); // 科目名
        //            dpc.SetParam(itemNm + itemNo02, SqlDbType.VarChar, 200, ""); // 前日残高
        //            dpc.SetParam(itemNm + itemNo03, SqlDbType.VarChar, 200, ""); // 貸方
        //            dpc.SetParam(itemNm + itemNo04, SqlDbType.VarChar, 200, ""); // 借方
        //            dpc.SetParam(itemNm + itemNo05, SqlDbType.VarChar, 200, ""); // 当残

        //            // ITEM名称をCOUNTUP
        //            itemNo01 = Util.ToString(Util.ToDecimal(itemNo01) + addRowCount);
        //            itemNo02 = Util.ToString(Util.ToDecimal(itemNo02) + addRowCount);
        //            itemNo03 = Util.ToString(Util.ToDecimal(itemNo03) + addRowCount);
        //            itemNo04 = Util.ToString(Util.ToDecimal(itemNo04) + addRowCount);
        //            itemNo05 = Util.ToString(Util.ToDecimal(itemNo05) + addRowCount);

        //            meisaicnt++;
        //        }
        //        #endregion

        //        #region 大項目データをワークテーブルに登録
        //        // 集計区分が2の場合
        //        if (Util.ToInt(date["SHUKEI_KUBUN"]) == 2)
        //        {
        //            kingakuDate = getKingakuDate03(date);
        //            //if(Util.ToInt(date["KAMOKU_BUNRUI"]) == 22400)
        //            if (Util.ToInt(date["KAMOKU_BUNRUI"]) == this._pForm.KAMOKU_BUNRUI_TOKI_MISYOBUN_JOYOKIN)
        //            {
        //                kingakuDate[0] = kingakuDate[0] - hoteJunbikin + tokiMishobunJoyokin;
        //                kingakuDate[1] = TaishakuHoteiJunbikin[0];
        //                kingakuDate[3] = kingakuDate[0] - kingakuDate[1] + kingakuDate[2];
        //                kingakuDate[3] = kingakuDate[3] - hoteJunbikin;// - tokiMishobunJoyokin;
        //                tokiMishobunzenZanKingakuSoneki[0] = kingakuDate[3];
        //            }
        //        }
        //        // 集計区分が3の場合
        //        else if (Util.ToInt(date["SHUKEI_KUBUN"]) == 3)
        //        {
        //            kingakuDate = getKingakuDate04(date);
        //            // 勘定科目が"【経常利益】"の場合
        //            if (SonekiKeisansho.Rows.Count - 7 == dataCnt)
        //            {
        //                //kingakuDate[0] = kingakuDate[0] * -1;
        //                //kingakuDate[3] = kingakuDate[3] * -1;
        //                kingakuDate[0] = kingakuDate[0];
        //                kingakuDate[3] = kingakuDate[3];
        //            }
        //            // 勘定科目が"【未処分剰余金】"の場合
        //            else if (SonekiKeisansho.Rows.Count == dataCnt)
        //            {
        //                // 対象区分が借方の場合
        //                if (Util.ToInt(date["TAISHAKU_KUBUN"]) == 1)
        //                {
        //                    kingakuDate[0] = tokiMishobunzenZanKingakuSoneki[0];// + hoteJunbikin - tokiMishobunJoyokin;
        //                    kingakuDate[3] = kingakuDate[0] + kingakuDate[1];// + hoteJunbikin - tokiMishobunJoyokin;
        //                }
        //                // 対象区分が貸方の場合
        //                else
        //                {
        //                    kingakuDate[0] = tokiMishobunzenZanKingakuSoneki[0];// + hoteJunbikin - tokiMishobunJoyokin;
        //                    kingakuDate[3] = kingakuDate[0] + kingakuDate[2];// + hoteJunbikin - tokiMishobunJoyokin;
        //                }
        //            }
        //        }
        //        else
        //        {
        //            continue;
        //        }

        //        tmpZenZanKingaku = Util.FormatNum(kingakuDate[0]);
        //        tmpKarikataKingaku = Util.FormatNum(kingakuDate[1]);
        //        tmpKashikataKingaku = Util.FormatNum(kingakuDate[2]);
        //        tmpZandakaKingaku = Util.FormatNum(kingakuDate[3]);

        //        // 金額がｾﾞﾛの科目を印字しない場合
        //        if (Util.ToString(this._pForm.Condition["Inji"]) == "no")
        //        {
        //            if (kingakuDate[0] == 0 && kingakuDate[1] == 0 && kingakuDate[2] == 0 && kingakuDate[3] == 0)
        //            {
        //                tmpZenZanKingaku = "";
        //                tmpKarikataKingaku = "";
        //                tmpKashikataKingaku = "";
        //                tmpZandakaKingaku = "";
        //            }
        //        }

        //        dpc.SetParam(itemNm + itemNo01, SqlDbType.VarChar, 200, date["KAMOKU_BUNRUI_NM"]); // 科目名
        //        dpc.SetParam(itemNm + itemNo02, SqlDbType.VarChar, 200, tmpZenZanKingaku); // 前日残高
        //        dpc.SetParam(itemNm + itemNo03, SqlDbType.VarChar, 200, tmpKarikataKingaku); // 貸方
        //        dpc.SetParam(itemNm + itemNo04, SqlDbType.VarChar, 200, tmpKashikataKingaku); // 借方
        //        dpc.SetParam(itemNm + itemNo05, SqlDbType.VarChar, 200, tmpZandakaKingaku); // 当残

        //        // ITEM名称をCOUNTUP
        //        itemNo01 = Util.ToString(Util.ToDecimal(itemNo01) + addRowCount);
        //        itemNo02 = Util.ToString(Util.ToDecimal(itemNo02) + addRowCount);
        //        itemNo03 = Util.ToString(Util.ToDecimal(itemNo03) + addRowCount);
        //        itemNo04 = Util.ToString(Util.ToDecimal(itemNo04) + addRowCount);
        //        itemNo05 = Util.ToString(Util.ToDecimal(itemNo05) + addRowCount);
        //        #endregion

        //        // 1P目の最後の項目でインサート.
        //        if (Util.ToString(date["HYOJI_JUNI"]) == hyojiNo_soneki01)
        //        {
        //            // インサート処理を実行
        //            this._dba.ModifyBySql(Util.ToString(Sql), dpc);

        //            /* 損益計算書(P2)を準備*/
        //            dbSORT++;
        //            Sql = MakeWkData_insertTable(new StringBuilder(), soneki02);
        //            dpc = MakeWkData_setDpc(dbSORT, kikan, bumonRange, outPutDate, zandakaNm);

        //            itemNo01 = "08";
        //            itemNo02 = "09";
        //            itemNo03 = "10";
        //            itemNo04 = "11";
        //            itemNo05 = "12";
        //        }
        //        // 2P目の最後の項目でインサート.
        //        else if (Util.ToString(date["HYOJI_JUNI"]) == hyojiNo_soneki02)
        //        {
        //            // インサート処理を実行
        //            this._dba.ModifyBySql(Util.ToString(Sql), dpc);

        //            /* 損益計算書(P3)を準備*/
        //            dbSORT++;
        //            Sql = MakeWkData_insertTable(new StringBuilder(), soneki03);
        //            dpc = MakeWkData_setDpc(dbSORT, kikan, bumonRange, outPutDate, zandakaNm);

        //            itemNo01 = "08";
        //            itemNo02 = "09";
        //            itemNo03 = "10";
        //            itemNo04 = "11";
        //            itemNo05 = "12";
        //        }
        //        // 3P目の最後の項目でインサート.
        //        else if (Util.ToString(date["HYOJI_JUNI"]) == hyojiNo_soneki03)
        //        {
        //            // インサート処理を実行
        //            this._dba.ModifyBySql(Util.ToString(Sql), dpc);

        //            /* 損益計算書(P4)を準備*/
        //            dbSORT++;
        //            Sql = MakeWkData_insertTable(new StringBuilder(), soneki04);
        //            dpc = MakeWkData_setDpc(dbSORT, kikan, bumonRange, outPutDate, zandakaNm);

        //            itemNo01 = "08";
        //            itemNo02 = "09";
        //            itemNo03 = "10";
        //            itemNo04 = "11";
        //            itemNo05 = "12";
        //        }
        //        // 4P目の最後の項目でインサート.
        //        else if (Util.ToString(date["HYOJI_JUNI"]) == hyojiNo_soneki04)
        //        {
        //            // インサート処理を実行
        //            this._dba.ModifyBySql(Util.ToString(Sql), dpc);
        //        }

        //        dataCnt++;
        //    }
        //}

        ///// <summary>
        ///// 共通パラム
        ///// </summary>
        //private DbParamCollection MakeWkData_setDpc(int dbSORT, String kikan, string bumonRange, string outPutDate, string zandakaNm)
        //{
        //    DbParamCollection dpc = new DbParamCollection();
        //    /* ▼▼▼ 共通 ▼▼▼ */
        //    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this._unqId);
        //    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, dbSORT);
        //    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, bumonRange); // 部門名
        //    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, this._uInfo.KaishaNm); // 会社名
        //    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, kikan); // 期間
        //    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, Util.ToString(this._pForm.Condition["ShohizeiShori"])); // 税抜きor税込み
        //    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, outPutDate); // 出力日付
        //    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, dbSORT); // ページ切り替え用
        //    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, zandakaNm); // 項目タイトル
        //    /* ▲▲▲ 共通 ▲▲▲ */

        //    return dpc;
        //}
        #endregion

        #region コメント
        ///// <summary>
        ///// 大項目の金額を取得
        ///// 明細区分が0以外の場合
        ///// </summary>
        //private Decimal[] getKingakuDate01(DataRow date)
        //{
        //    #region 金額データを取得
        //    String joken = "shukeiKeisanShiki = " + Util.ToString(date["SHUKEI_KEISANSHIKI"]);
        //    decimal zenZanKingaku;
        //    decimal karikataKingaku;
        //    decimal kashikataKingaku;
        //    decimal zandakaKingaku;
        //    decimal houteiJunbiKin = 0;

        //    // 残高金額、借方金額、貸方金額、当残金額の合計値を計算
        //    // 貸借区分が借方の場合
        //    if (Util.ToInt(date["TAISHAKU_KUBUN"]) == 1)
        //    {
        //        zenZanKingaku = Util.ToDecimal(KanjoKamokuKingaku.Compute("SUM(zandakaKingaku)", joken));
        //        karikataKingaku = Util.ToDecimal(KanjoKamokuKingaku.Compute("SUM(karikataKingaku)", joken));
        //        kashikataKingaku = Util.ToDecimal(KanjoKamokuKingaku.Compute("SUM(kashikataKingaku)", joken));
        //        zandakaKingaku = Util.ToDecimal(KanjoKamokuKingaku.Compute("SUM(touzanKingaku)", joken));
        //    }
        //    // 貸借区分が貸方の場合
        //    else
        //    {
        //        zenZanKingaku = Util.ToDecimal(KanjoKamokuKingaku.Compute("SUM(zandakaKingaku)", joken)) * -1;
        //        karikataKingaku = Util.ToDecimal(KanjoKamokuKingaku.Compute("SUM(karikataKingaku)", joken)) * -1;
        //        kashikataKingaku = Util.ToDecimal(KanjoKamokuKingaku.Compute("SUM(kashikataKingaku)", joken)) * -1;
        //        zandakaKingaku = Util.ToDecimal(KanjoKamokuKingaku.Compute("SUM(touzanKingaku)", joken)) * -1;

        //        // 勘定科目が法定準備金の場合金額を保持
        //        //if (Util.ToInt(date["KAMOKU_BUNRUI"]) == 13320)
        //        if (Util.ToInt(date["KAMOKU_BUNRUI"]) == this._pForm.KAMOKU_BUNRUI_HOTE_JUNBIKIN)
        //        {
        //            houteiJunbiKin = kashikataKingaku;
        //        }
        //    }
        //    #endregion

        //    Decimal[] kingakuDate = new Decimal[5] { zenZanKingaku, karikataKingaku, kashikataKingaku, zandakaKingaku, houteiJunbiKin };

        //    return kingakuDate;
        //}

        ///// <summary>
        ///// 大項目の金額を取得
        ///// 明細区分が0の場合
        ///// </summary>
        //private Decimal[] getKingakuDate02(DataRow date)
        //{
        //    int first = 0;
        //    bool flag = false;
        //    ArrayList stTargetPlus = new ArrayList();
        //    ArrayList stTargetMinus = new ArrayList();
        //    bool plusMinusCheckFlag = false;
        //    int deleteCount;
        //    String shukeiKeisanShiki = Util.ToString(date["SHUKEI_KEISANSHIKI"]);
        //    int daiKomokuTaishakuKubun = Util.ToInt(date["TAISHAKU_KUBUN"]);

        //    #region 集計計算式の"+"と"-"を分別
        //    while (!flag)
        //    {
        //        // "+"と"-"の文字を検索
        //        int findPlus = shukeiKeisanShiki.IndexOf("+");
        //        int findMinus = shukeiKeisanShiki.IndexOf("-");
        //        // "+"も"-"も無ければ、処理終了
        //        if (findPlus <= 0 && findMinus <= 0)
        //        {
        //            if (first == 0)
        //            {
        //                stTargetPlus.Add(shukeiKeisanShiki);
        //            }
        //            else
        //            {
        //                if (!plusMinusCheckFlag)
        //                {
        //                    stTargetPlus.Add(shukeiKeisanShiki);
        //                }
        //                else if (plusMinusCheckFlag)
        //                {
        //                    stTargetMinus.Add(shukeiKeisanShiki);
        //                }
        //            }
        //            flag = true;
        //        }
        //        // "+"又は"-"があれば分けて格納
        //        else
        //        {
        //            // 最初の処理 1つ目の項目を格納し、文字列から削除
        //            if (first == 0)
        //            {
        //                // "+"の時の処理
        //                if ((findMinus > findPlus && findPlus > 0) || (findPlus > findMinus && findMinus <= 0))
        //                {
        //                    stTargetPlus.Add(shukeiKeisanShiki.Substring(0, findPlus));
        //                    shukeiKeisanShiki = shukeiKeisanShiki.Substring(findPlus);
        //                }
        //                // "-"の時の処理
        //                else if ((findPlus > findMinus && findMinus > 0) || (findMinus > findPlus && findPlus <= 0))
        //                {
        //                    stTargetPlus.Add(shukeiKeisanShiki.Substring(0, findMinus));
        //                    shukeiKeisanShiki = shukeiKeisanShiki.Substring(findMinus);
        //                }
        //            }
        //            // 2つ目以降の処理 項目を順番に格納し、文字列から削除
        //            else
        //            {
        //                // 削除する文字数を取得
        //                if ((findPlus > findMinus && findMinus > 0) || (findMinus > findPlus && findPlus <= 0))
        //                {
        //                    deleteCount = findMinus;
        //                }
        //                else
        //                {
        //                    deleteCount = findPlus;
        //                }
        //                // 項目を格納
        //                if (!plusMinusCheckFlag)
        //                {
        //                    stTargetPlus.Add(shukeiKeisanShiki.Substring(0, deleteCount));
        //                    shukeiKeisanShiki = shukeiKeisanShiki.Substring(deleteCount);
        //                }
        //                else if (plusMinusCheckFlag)
        //                {
        //                    stTargetMinus.Add(shukeiKeisanShiki.Substring(0, deleteCount));
        //                    shukeiKeisanShiki = shukeiKeisanShiki.Substring(deleteCount);
        //                }
        //            }
        //            // "+"と"-"の文字を検索
        //            findPlus = shukeiKeisanShiki.IndexOf("+");
        //            findMinus = shukeiKeisanShiki.IndexOf("-");
        //            // 次の項目がが"+"か"-"なのかの判断フラグを設定
        //            if (findPlus == 0)
        //            {
        //                plusMinusCheckFlag = false;
        //            }
        //            else if (findMinus == 0)
        //            {
        //                plusMinusCheckFlag = true;
        //            }
        //            // "+"又は"-"の文字を削除
        //            shukeiKeisanShiki = shukeiKeisanShiki.Substring(1);
        //        }
        //        first = 1;
        //    }
        //    #endregion

        //    #region 条件式の作成
        //    // "+"の項目の条件式を作成
        //    int cnt = 0;
        //    string jokenPlus = "";
        //    while (stTargetPlus.Count > cnt)
        //    {
        //        if (cnt == 0)
        //        {
        //            jokenPlus = "shukeiKeisanShiki = " + stTargetPlus[cnt];
        //        }
        //        else
        //        {
        //            jokenPlus += " OR shukeiKeisanShiki = " + stTargetPlus[cnt];
        //        }

        //        cnt++;
        //    }
        //    // "-"の項目の条件式を作成
        //    cnt = 0;
        //    string jokenMinus = "";
        //    while (stTargetMinus.Count > cnt)
        //    {
        //        if (cnt == 0)
        //        {
        //            jokenMinus = "shukeiKeisanShiki = " + stTargetMinus[cnt];
        //        }
        //        else
        //        {
        //            jokenMinus += " OR shukeiKeisanShiki = " + stTargetMinus[cnt];
        //        }

        //        cnt++;
        //    }
        //    #endregion

        //    #region 残高金額、借方金額、貸方金額、当残金額の合計値を計算
        //    object sumZandakaKingakuPlus;
        //    object sumKarikataKingakuPlus;
        //    object sumKashikataKingakuPlus;
        //    object sumTouzanKingakuPlus;
        //    object sumZandakaKingakuMinus;
        //    object sumKarikataKingakuMinus;
        //    object sumKashikataKingakuMinus;
        //    object sumTouzanKingakuMinus;
        //    decimal sumZandakaKingaku;
        //    decimal sumKarikataKingaku;
        //    decimal sumKashikataKingaku;
        //    decimal sumTouzanKingaku;
        //    if (jokenPlus != "")
        //    {
        //        sumZandakaKingakuPlus = KanjoKamokuKingaku.Compute("SUM(zandakaKingaku)", jokenPlus);
        //        sumKarikataKingakuPlus = KanjoKamokuKingaku.Compute("SUM(karikataKingaku)", jokenPlus);
        //        sumKashikataKingakuPlus = KanjoKamokuKingaku.Compute("SUM(kashikataKingaku)", jokenPlus);
        //        sumTouzanKingakuPlus = KanjoKamokuKingaku.Compute("SUM(touzanKingaku)", jokenPlus);
        //    }
        //    else
        //    {
        //        sumZandakaKingakuPlus = 0;
        //        sumKarikataKingakuPlus = 0;
        //        sumKashikataKingakuPlus = 0;
        //        sumTouzanKingakuPlus = 0;
        //    }
        //    if (jokenMinus != "")
        //    {
        //        sumZandakaKingakuMinus = KanjoKamokuKingaku.Compute("SUM(zandakaKingaku)", jokenMinus);
        //        sumKarikataKingakuMinus = KanjoKamokuKingaku.Compute("SUM(karikataKingaku)", jokenMinus);
        //        sumKashikataKingakuMinus = KanjoKamokuKingaku.Compute("SUM(kashikataKingaku)", jokenMinus);
        //        sumTouzanKingakuMinus = KanjoKamokuKingaku.Compute("SUM(touzanKingaku)", jokenMinus);
        //    }
        //    else
        //    {
        //        sumZandakaKingakuMinus = 0;
        //        sumKarikataKingakuMinus = 0;
        //        sumKashikataKingakuMinus = 0;
        //        sumTouzanKingakuMinus = 0;
        //    }
        //    sumZandakaKingaku = Util.ToDecimal(sumZandakaKingakuPlus) + Util.ToDecimal(sumZandakaKingakuMinus);
        //    sumKarikataKingaku = Util.ToDecimal(sumKarikataKingakuPlus) + Util.ToDecimal(sumKarikataKingakuMinus);
        //    sumKashikataKingaku = Util.ToDecimal(sumKashikataKingakuPlus) + Util.ToDecimal(sumKashikataKingakuMinus);
        //    sumTouzanKingaku = Util.ToDecimal(sumTouzanKingakuPlus) + Util.ToDecimal(sumTouzanKingakuMinus);

        //    // 貸借区分が貸方の場合
        //    if (daiKomokuTaishakuKubun == 2)
        //    {
        //        sumZandakaKingaku = sumZandakaKingaku * -1;
        //        sumKarikataKingaku = sumKarikataKingaku * -1;
        //        sumKashikataKingaku = sumKashikataKingaku * -1;
        //        sumTouzanKingaku = sumTouzanKingaku * -1;
        //    }
        //    #endregion

        //    Decimal[] kingakuDate = new Decimal[4] { sumZandakaKingaku, sumKarikataKingaku, sumKashikataKingaku, sumTouzanKingaku };

        //    return kingakuDate;
        //}

        ///// <summary>
        ///// 大項目の金額を取得
        ///// 明細区分が0以外の場合　集計区分が2の場合
        ///// </summary>
        //private Decimal[] getKingakuDate03(DataRow date)
        //{
        //    #region 金額データを取得
        //    String joken = "shukeiKeisanShiki = " + Util.ToString(date["SHUKEI_KEISANSHIKI"]);
        //    decimal zenZanKingakuData;
        //    decimal karikataKingakuData;
        //    decimal kashikataKingakuData;
        //    decimal zandakaKingakuData;

        //    // 残高金額、借方金額、貸方金額、当残金額の合計値を計算
        //    // 貸借区分が借方の場合
        //    if (Util.ToInt(date["TAISHAKU_KUBUN"]) == 1)
        //    {
        //        zenZanKingakuData = Util.ToDecimal(KanjoKamokuKingaku.Compute("SUM(zandakaKingaku)", joken));
        //        karikataKingakuData = Util.ToDecimal(KanjoKamokuKingaku.Compute("SUM(karikataKingaku)", joken));
        //        kashikataKingakuData = Util.ToDecimal(KanjoKamokuKingaku.Compute("SUM(kashikataKingaku)", joken));
        //        zandakaKingakuData = Util.ToDecimal(KanjoKamokuKingaku.Compute("SUM(touzanKingaku)", joken));
        //    }
        //    // 貸借区分が貸方の場合
        //    else
        //    {
        //        zenZanKingakuData = Util.ToDecimal(KanjoKamokuKingaku.Compute("SUM(zandakaKingaku)", joken)) * -1;
        //        karikataKingakuData = Util.ToDecimal(KanjoKamokuKingaku.Compute("SUM(karikataKingaku)", joken)) * -1;
        //        kashikataKingakuData = Util.ToDecimal(KanjoKamokuKingaku.Compute("SUM(kashikataKingaku)", joken)) * -1;
        //        zandakaKingakuData = Util.ToDecimal(KanjoKamokuKingaku.Compute("SUM(touzanKingaku)", joken)) * -1;
        //    }
        //    #endregion

        //    Decimal[] kingakuDate = new Decimal[4] { zenZanKingakuData, karikataKingakuData, kashikataKingakuData, zandakaKingakuData };

        //    return kingakuDate;
        //}

        ///// <summary>
        ///// 大項目の金額を取得
        ///// 明細区分が0の場合 集計区分が3の場合
        ///// </summary>
        //private Decimal[] getKingakuDate04(DataRow date)
        //{
        //    int first = 0;
        //    bool flag = false;
        //    ArrayList stTargetPlus = new ArrayList();
        //    ArrayList stTargetMinus = new ArrayList();
        //    bool plusMinusCheckFlag = false;
        //    int deleteCount;
        //    String shukeiKeisanShiki = Util.ToString(date["SHUKEI_KEISANSHIKI"]);
        //    int daiKomokuTaishakuKubun = Util.ToInt(date["TAISHAKU_KUBUN"]);

        //    #region 集計計算式の"+"と"-"を分別
        //    while (!flag)
        //    {
        //        // "+"と"-"の文字を検索
        //        int findPlus = shukeiKeisanShiki.IndexOf("+");
        //        int findMinus = shukeiKeisanShiki.IndexOf("-");
        //        // "+"も"-"も無ければ、処理終了
        //        if (findPlus <= 0 && findMinus <= 0)
        //        {
        //            if (first == 0)
        //            {
        //                stTargetPlus.Add(shukeiKeisanShiki);
        //            }
        //            else
        //            {
        //                if (!plusMinusCheckFlag)
        //                {
        //                    stTargetPlus.Add(shukeiKeisanShiki);
        //                }
        //                else if (plusMinusCheckFlag)
        //                {
        //                    stTargetMinus.Add(shukeiKeisanShiki);
        //                }
        //            }
        //            flag = true;
        //        }
        //        // "+"又は"-"があれば分けて格納
        //        else
        //        {
        //            // 最初の処理 1つ目の項目を格納し、文字列から削除
        //            if (first == 0)
        //            {
        //                // "+"の時の処理
        //                if ((findMinus > findPlus && findPlus > 0) || (findPlus > findMinus && findMinus <= 0))
        //                {
        //                    stTargetPlus.Add(shukeiKeisanShiki.Substring(0, findPlus));
        //                    shukeiKeisanShiki = shukeiKeisanShiki.Substring(findPlus);
        //                }
        //                // "-"の時の処理
        //                else if ((findPlus > findMinus && findMinus > 0) || (findMinus > findPlus && findPlus <= 0))
        //                {
        //                    stTargetPlus.Add(shukeiKeisanShiki.Substring(0, findMinus));
        //                    shukeiKeisanShiki = shukeiKeisanShiki.Substring(findMinus);
        //                }
        //            }
        //            // 2つ目以降の処理 項目を順番に格納し、文字列から削除
        //            else
        //            {
        //                // 削除する文字数を取得
        //                if ((findPlus > findMinus && findMinus > 0) || (findMinus > findPlus && findPlus <= 0))
        //                {
        //                    deleteCount = findMinus;
        //                }
        //                else
        //                {
        //                    deleteCount = findPlus;
        //                }
        //                // 項目を格納
        //                if (!plusMinusCheckFlag)
        //                {
        //                    stTargetPlus.Add(shukeiKeisanShiki.Substring(0, deleteCount));
        //                    shukeiKeisanShiki = shukeiKeisanShiki.Substring(deleteCount);
        //                }
        //                else if (plusMinusCheckFlag)
        //                {
        //                    stTargetMinus.Add(shukeiKeisanShiki.Substring(0, deleteCount));
        //                    shukeiKeisanShiki = shukeiKeisanShiki.Substring(deleteCount);
        //                }
        //            }
        //            // "+"と"-"の文字を検索
        //            findPlus = shukeiKeisanShiki.IndexOf("+");
        //            findMinus = shukeiKeisanShiki.IndexOf("-");
        //            // 次の項目がが"+"か"-"なのかの判断フラグを設定
        //            if (findPlus == 0)
        //            {
        //                plusMinusCheckFlag = false;
        //            }
        //            else if (findMinus == 0)
        //            {
        //                plusMinusCheckFlag = true;
        //            }
        //            // "+"又は"-"の文字を削除
        //            shukeiKeisanShiki = shukeiKeisanShiki.Substring(1);
        //        }
        //        first = 1;
        //    }
        //    #endregion

        //    #region 条件式の作成
        //    // "+"の項目の条件式を作成
        //    int cnt = 0;
        //    string jokenPlus = "";
        //    while (stTargetPlus.Count > cnt)
        //    {
        //        if (cnt == 0)
        //        {
        //            jokenPlus = "shukeiKeisanShiki = " + stTargetPlus[cnt];
        //        }
        //        else
        //        {
        //            jokenPlus += " OR shukeiKeisanShiki = " + stTargetPlus[cnt];
        //        }

        //        cnt++;
        //    }
        //    // "-"の項目の条件式を作成
        //    cnt = 0;
        //    string jokenMinus = "";
        //    while (stTargetMinus.Count > cnt)
        //    {
        //        if (cnt == 0)
        //        {
        //            jokenMinus = "shukeiKeisanShiki = " + stTargetMinus[cnt];
        //        }
        //        else
        //        {
        //            jokenMinus += " OR shukeiKeisanShiki = " + stTargetMinus[cnt];
        //        }

        //        cnt++;
        //    }
        //    #endregion

        //    #region 残高金額、借方金額、貸方金額、当残金額の合計値を計算
        //    object sumZandakaKingakuPlus;
        //    object sumKarikataKingakuPlus;
        //    object sumKashikataKingakuPlus;
        //    object sumTouzanKingakuPlus;
        //    object sumZandakaKingakuMinus;
        //    object sumKarikataKingakuMinus;
        //    object sumKashikataKingakuMinus;
        //    object sumTouzanKingakuMinus;
        //    decimal sumZandakaKingaku;
        //    decimal sumKarikataKingaku;
        //    decimal sumKashikataKingaku;
        //    decimal sumTouzanKingaku;
        //    if (jokenPlus != "")
        //    {
        //        sumZandakaKingakuPlus = KanjoKamokuKingaku.Compute("SUM(zandakaKingaku)", jokenPlus);
        //        sumKarikataKingakuPlus = KanjoKamokuKingaku.Compute("SUM(karikataKingaku)", jokenPlus);
        //        sumKashikataKingakuPlus = KanjoKamokuKingaku.Compute("SUM(kashikataKingaku)", jokenPlus);
        //        sumTouzanKingakuPlus = KanjoKamokuKingaku.Compute("SUM(touzanKingaku)", jokenPlus);
        //    }
        //    else
        //    {
        //        sumZandakaKingakuPlus = 0;
        //        sumKarikataKingakuPlus = 0;
        //        sumKashikataKingakuPlus = 0;
        //        sumTouzanKingakuPlus = 0;
        //    }
        //    if (jokenMinus != "")
        //    {
        //        sumZandakaKingakuMinus = KanjoKamokuKingaku.Compute("SUM(zandakaKingaku)", jokenMinus);
        //        sumKarikataKingakuMinus = KanjoKamokuKingaku.Compute("SUM(karikataKingaku)", jokenMinus);
        //        sumKashikataKingakuMinus = KanjoKamokuKingaku.Compute("SUM(kashikataKingaku)", jokenMinus);
        //        sumTouzanKingakuMinus = KanjoKamokuKingaku.Compute("SUM(touzanKingaku)", jokenMinus);
        //    }
        //    else
        //    {
        //        sumZandakaKingakuMinus = 0;
        //        sumKarikataKingakuMinus = 0;
        //        sumKashikataKingakuMinus = 0;
        //        sumTouzanKingakuMinus = 0;
        //    }
        //    sumZandakaKingaku = Util.ToDecimal(sumZandakaKingakuPlus) + Util.ToDecimal(sumZandakaKingakuMinus);
        //    sumKarikataKingaku = Util.ToDecimal(sumKarikataKingakuMinus) - Util.ToDecimal(sumKarikataKingakuPlus);
        //    sumKashikataKingaku = Util.ToDecimal(sumKashikataKingakuMinus) - Util.ToDecimal(sumKashikataKingakuPlus);
        //    sumTouzanKingaku = Util.ToDecimal(sumTouzanKingakuPlus) + Util.ToDecimal(sumTouzanKingakuMinus);

        //    // 貸借区分が借方の場合
        //    if (daiKomokuTaishakuKubun == 1)
        //    {
        //        // 借方>貸方の場合
        //        if (sumKarikataKingaku > sumKashikataKingaku)
        //        {
        //            // 貸方発生
        //            sumKashikataKingaku = (sumKashikataKingaku - sumKarikataKingaku) * -1;
        //            // 借方発生
        //            sumKarikataKingaku = 0;
        //        }
        //        // 借方<貸方の場合
        //        else if (sumKarikataKingaku < sumKashikataKingaku)
        //        {
        //            // 借方発生
        //            sumKarikataKingaku = (sumKarikataKingaku - sumKashikataKingaku) * -1;
        //            // 貸方発生
        //            sumKashikataKingaku = 0;
        //        }
        //        // 借方==貸方の場合
        //        else
        //        {
        //            // 借方発生
        //            sumKarikataKingaku = 0;
        //            // 貸方発生
        //            sumKashikataKingaku = 0;
        //        }
        //    }
        //    // 貸借区分が貸方の場合
        //    else
        //    {
        //        sumZandakaKingaku = sumZandakaKingaku * -1;
        //        sumTouzanKingaku = sumTouzanKingaku * -1;
        //        // 借方>貸方の場合
        //        if (sumKarikataKingaku > sumKashikataKingaku)
        //        {
        //            // 借方発生
        //            sumKarikataKingaku = sumKarikataKingaku - sumKashikataKingaku;
        //            // 貸方発生
        //            sumKashikataKingaku = 0;
        //        }
        //        // 借方<貸方の場合
        //        else if (sumKarikataKingaku < sumKashikataKingaku)
        //        {
        //            // 貸方発生
        //            sumKashikataKingaku = sumKashikataKingaku - sumKarikataKingaku;
        //            // 借方発生
        //            sumKarikataKingaku = 0;
        //        }
        //        // 借方==貸方の場合
        //        else
        //        {
        //            // 借方発生
        //            sumKarikataKingaku = 0;
        //            // 貸方発生
        //            sumKashikataKingaku = 0;
        //        }
        //    }
        //    #endregion

        //    Decimal[] kingakuDate = new Decimal[4] { sumZandakaKingaku, sumKarikataKingaku, sumKashikataKingaku, sumTouzanKingaku };

        //    return kingakuDate;
        //}
        #endregion

        #endregion
    }
}
