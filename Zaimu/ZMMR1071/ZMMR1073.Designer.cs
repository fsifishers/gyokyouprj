﻿namespace jp.co.fsi.zm.zmmr1071
{
    partial class ZMMR1073
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.F1 = new System.Windows.Forms.TabPage();
            this.mtbListF1 = new System.Windows.Forms.DataGridView();
            this.F1VisibleHandan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F1kamokuCd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F1KamokuNm = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F1gyoBango = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F1Taishaku = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F1Moji = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F1deleteAndAddNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F1hyojiJuni = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F2 = new System.Windows.Forms.TabPage();
            this.mtbListF2 = new System.Windows.Forms.DataGridView();
            this.F2VisibleHandan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F2kamokuCd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F2KamokuNm = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F2gyoBango = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F2Taishaku = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F2Moji = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F2deleteAndAddNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F2hyojiJuni = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F3 = new System.Windows.Forms.TabPage();
            this.mtbListF3 = new System.Windows.Forms.DataGridView();
            this.F3VisibleHandan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F3kamokuCd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F3KamokuNm = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F3gyoBango = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F3Taishaku = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F3Moji = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F3deleteAndAddNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F3hyojiJuni = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gbxYoyaku = new System.Windows.Forms.GroupBox();
            this.gbxMoji = new System.Windows.Forms.GroupBox();
            this.lblMemo = new System.Windows.Forms.Label();
            this.txtMoji = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMoji = new System.Windows.Forms.Label();
            this.gbxKanjoKamoku = new System.Windows.Forms.GroupBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnDel = new System.Windows.Forms.Button();
            this.gbxKanjoKamokuIchiran = new System.Windows.Forms.GroupBox();
            this.lbxKanjoKamokuIchiran = new System.Windows.Forms.ListBox();
            this.gbxTaishoKanjoKamoku = new System.Windows.Forms.GroupBox();
            this.lbxTaishoKanjoKamoku = new System.Windows.Forms.ListBox();
            this.gbxTaishaku = new System.Windows.Forms.GroupBox();
            this.rdoKashi = new System.Windows.Forms.RadioButton();
            this.rdoKari = new System.Windows.Forms.RadioButton();
            this.txtKamokuNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.pnlDebug.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.F1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mtbListF1)).BeginInit();
            this.F2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mtbListF2)).BeginInit();
            this.F3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mtbListF3)).BeginInit();
            this.gbxYoyaku.SuspendLayout();
            this.gbxMoji.SuspendLayout();
            this.gbxKanjoKamoku.SuspendLayout();
            this.gbxKanjoKamokuIchiran.SuspendLayout();
            this.gbxTaishoKanjoKamoku.SuspendLayout();
            this.gbxTaishaku.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(641, 23);
            this.lblTitle.Text = "総勘定元帳 [印字設定]";
            this.lblTitle.Visible = false;
            // 
            // btnEsc
            // 
            this.btnEsc.Location = new System.Drawing.Point(3, 49);
            // 
            // btnF1
            // 
            this.btnF1.Visible = false;
            // 
            // btnF2
            // 
            this.btnF2.Visible = false;
            // 
            // btnF3
            // 
            this.btnF3.Visible = false;
            // 
            // btnF4
            // 
            this.btnF4.Visible = false;
            // 
            // btnF5
            // 
            this.btnF5.Visible = false;
            // 
            // btnF7
            // 
            this.btnF7.Visible = false;
            // 
            // btnF6
            // 
            this.btnF6.Location = new System.Drawing.Point(67, 49);
            // 
            // btnF8
            // 
            this.btnF8.Visible = false;
            // 
            // btnF9
            // 
            this.btnF9.Visible = false;
            // 
            // btnF12
            // 
            this.btnF12.Visible = false;
            // 
            // btnF11
            // 
            this.btnF11.Visible = false;
            // 
            // btnF10
            // 
            this.btnF10.Visible = false;
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(5, 487);
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.F1);
            this.tabControl.Controls.Add(this.F2);
            this.tabControl.Controls.Add(this.F3);
            this.tabControl.Location = new System.Drawing.Point(6, 12);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(384, 517);
            this.tabControl.TabIndex = 1;
            this.tabControl.SelectedIndexChanged += new System.EventHandler(this.tabControl_SelectedIndexChanged);
            // 
            // F1
            // 
            this.F1.Controls.Add(this.mtbListF1);
            this.F1.Location = new System.Drawing.Point(4, 22);
            this.F1.Name = "F1";
            this.F1.Padding = new System.Windows.Forms.Padding(3);
            this.F1.Size = new System.Drawing.Size(376, 491);
            this.F1.TabIndex = 0;
            this.F1.Text = "F1：貸借対照表";
            this.F1.UseVisualStyleBackColor = true;
            // 
            // mtbListF1
            // 
            this.mtbListF1.AllowUserToAddRows = false;
            this.mtbListF1.AllowUserToDeleteRows = false;
            this.mtbListF1.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gray;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.mtbListF1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.mtbListF1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.mtbListF1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.F1VisibleHandan,
            this.F1kamokuCd,
            this.F1KamokuNm,
            this.F1gyoBango,
            this.F1Taishaku,
            this.F1Moji,
            this.F1deleteAndAddNo,
            this.F1hyojiJuni});
            this.mtbListF1.Location = new System.Drawing.Point(6, 6);
            this.mtbListF1.MultiSelect = false;
            this.mtbListF1.Name = "mtbListF1";
            this.mtbListF1.ReadOnly = true;
            this.mtbListF1.RowHeadersVisible = false;
            this.mtbListF1.RowTemplate.Height = 21;
            this.mtbListF1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.mtbListF1.Size = new System.Drawing.Size(364, 480);
            this.mtbListF1.TabIndex = 3;
            this.mtbListF1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.CellClick);
            // 
            // F1VisibleHandan
            // 
            this.F1VisibleHandan.HeaderText = "Visible判断";
            this.F1VisibleHandan.Name = "F1VisibleHandan";
            this.F1VisibleHandan.ReadOnly = true;
            this.F1VisibleHandan.Visible = false;
            // 
            // F1kamokuCd
            // 
            this.F1kamokuCd.HeaderText = "科目コード";
            this.F1kamokuCd.Name = "F1kamokuCd";
            this.F1kamokuCd.ReadOnly = true;
            this.F1kamokuCd.Visible = false;
            // 
            // F1KamokuNm
            // 
            this.F1KamokuNm.HeaderText = "科　目　名";
            this.F1KamokuNm.Name = "F1KamokuNm";
            this.F1KamokuNm.ReadOnly = true;
            this.F1KamokuNm.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.F1KamokuNm.Width = 200;
            // 
            // F1gyoBango
            // 
            this.F1gyoBango.HeaderText = "行番号";
            this.F1gyoBango.Name = "F1gyoBango";
            this.F1gyoBango.ReadOnly = true;
            this.F1gyoBango.Visible = false;
            // 
            // F1Taishaku
            // 
            this.F1Taishaku.HeaderText = "貸借";
            this.F1Taishaku.Name = "F1Taishaku";
            this.F1Taishaku.ReadOnly = true;
            this.F1Taishaku.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.F1Taishaku.Width = 70;
            // 
            // F1Moji
            // 
            this.F1Moji.HeaderText = "文字";
            this.F1Moji.Name = "F1Moji";
            this.F1Moji.ReadOnly = true;
            this.F1Moji.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.F1Moji.Width = 70;
            // 
            // F1deleteAndAddNo
            // 
            this.F1deleteAndAddNo.HeaderText = "削除と追加用行番号";
            this.F1deleteAndAddNo.Name = "F1deleteAndAddNo";
            this.F1deleteAndAddNo.ReadOnly = true;
            this.F1deleteAndAddNo.Visible = false;
            // 
            // F1hyojiJuni
            // 
            this.F1hyojiJuni.HeaderText = "表示順位";
            this.F1hyojiJuni.Name = "F1hyojiJuni";
            this.F1hyojiJuni.ReadOnly = true;
            this.F1hyojiJuni.Visible = false;
            // 
            // F2
            // 
            this.F2.Controls.Add(this.mtbListF2);
            this.F2.Location = new System.Drawing.Point(4, 22);
            this.F2.Name = "F2";
            this.F2.Padding = new System.Windows.Forms.Padding(3);
            this.F2.Size = new System.Drawing.Size(376, 491);
            this.F2.TabIndex = 1;
            this.F2.Text = "F2：損益計算書";
            this.F2.UseVisualStyleBackColor = true;
            // 
            // mtbListF2
            // 
            this.mtbListF2.AllowUserToAddRows = false;
            this.mtbListF2.AllowUserToDeleteRows = false;
            this.mtbListF2.AllowUserToResizeRows = false;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Gray;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.mtbListF2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.mtbListF2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.mtbListF2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.F2VisibleHandan,
            this.F2kamokuCd,
            this.F2KamokuNm,
            this.F2gyoBango,
            this.F2Taishaku,
            this.F2Moji,
            this.F2deleteAndAddNo,
            this.F2hyojiJuni});
            this.mtbListF2.Location = new System.Drawing.Point(6, 6);
            this.mtbListF2.MultiSelect = false;
            this.mtbListF2.Name = "mtbListF2";
            this.mtbListF2.ReadOnly = true;
            this.mtbListF2.RowHeadersVisible = false;
            this.mtbListF2.RowTemplate.Height = 21;
            this.mtbListF2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.mtbListF2.Size = new System.Drawing.Size(364, 480);
            this.mtbListF2.TabIndex = 4;
            this.mtbListF2.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.CellClick);
            // 
            // F2VisibleHandan
            // 
            this.F2VisibleHandan.HeaderText = "Visible判断";
            this.F2VisibleHandan.Name = "F2VisibleHandan";
            this.F2VisibleHandan.ReadOnly = true;
            this.F2VisibleHandan.Visible = false;
            // 
            // F2kamokuCd
            // 
            this.F2kamokuCd.HeaderText = "科目コード";
            this.F2kamokuCd.Name = "F2kamokuCd";
            this.F2kamokuCd.ReadOnly = true;
            this.F2kamokuCd.Visible = false;
            // 
            // F2KamokuNm
            // 
            this.F2KamokuNm.HeaderText = "科　目　名";
            this.F2KamokuNm.Name = "F2KamokuNm";
            this.F2KamokuNm.ReadOnly = true;
            this.F2KamokuNm.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.F2KamokuNm.Width = 200;
            // 
            // F2gyoBango
            // 
            this.F2gyoBango.HeaderText = "行番号";
            this.F2gyoBango.Name = "F2gyoBango";
            this.F2gyoBango.ReadOnly = true;
            this.F2gyoBango.Visible = false;
            // 
            // F2Taishaku
            // 
            this.F2Taishaku.HeaderText = "貸借";
            this.F2Taishaku.Name = "F2Taishaku";
            this.F2Taishaku.ReadOnly = true;
            this.F2Taishaku.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.F2Taishaku.Width = 70;
            // 
            // F2Moji
            // 
            this.F2Moji.HeaderText = "文字";
            this.F2Moji.Name = "F2Moji";
            this.F2Moji.ReadOnly = true;
            this.F2Moji.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.F2Moji.Width = 70;
            // 
            // F2deleteAndAddNo
            // 
            this.F2deleteAndAddNo.HeaderText = "削除と追加用行番号";
            this.F2deleteAndAddNo.Name = "F2deleteAndAddNo";
            this.F2deleteAndAddNo.ReadOnly = true;
            this.F2deleteAndAddNo.Visible = false;
            // 
            // F2hyojiJuni
            // 
            this.F2hyojiJuni.HeaderText = "表示順位";
            this.F2hyojiJuni.Name = "F2hyojiJuni";
            this.F2hyojiJuni.ReadOnly = true;
            this.F2hyojiJuni.Visible = false;
            // 
            // F3
            // 
            this.F3.Controls.Add(this.mtbListF3);
            this.F3.Location = new System.Drawing.Point(4, 22);
            this.F3.Name = "F3";
            this.F3.Size = new System.Drawing.Size(376, 491);
            this.F3.TabIndex = 2;
            this.F3.Text = "F3：製造原価";
            this.F3.UseVisualStyleBackColor = true;
            // 
            // mtbListF3
            // 
            this.mtbListF3.AllowUserToAddRows = false;
            this.mtbListF3.AllowUserToDeleteRows = false;
            this.mtbListF3.AllowUserToResizeRows = false;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.Gray;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.mtbListF3.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.mtbListF3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.mtbListF3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.F3VisibleHandan,
            this.F3kamokuCd,
            this.F3KamokuNm,
            this.F3gyoBango,
            this.F3Taishaku,
            this.F3Moji,
            this.F3deleteAndAddNo,
            this.F3hyojiJuni});
            this.mtbListF3.Location = new System.Drawing.Point(6, 6);
            this.mtbListF3.MultiSelect = false;
            this.mtbListF3.Name = "mtbListF3";
            this.mtbListF3.ReadOnly = true;
            this.mtbListF3.RowHeadersVisible = false;
            this.mtbListF3.RowTemplate.Height = 21;
            this.mtbListF3.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.mtbListF3.Size = new System.Drawing.Size(364, 480);
            this.mtbListF3.TabIndex = 5;
            this.mtbListF3.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.CellClick);
            // 
            // F3VisibleHandan
            // 
            this.F3VisibleHandan.HeaderText = "Visible判断";
            this.F3VisibleHandan.Name = "F3VisibleHandan";
            this.F3VisibleHandan.ReadOnly = true;
            this.F3VisibleHandan.Visible = false;
            // 
            // F3kamokuCd
            // 
            this.F3kamokuCd.HeaderText = "科目コード";
            this.F3kamokuCd.Name = "F3kamokuCd";
            this.F3kamokuCd.ReadOnly = true;
            this.F3kamokuCd.Visible = false;
            // 
            // F3KamokuNm
            // 
            this.F3KamokuNm.HeaderText = "科　目　名";
            this.F3KamokuNm.Name = "F3KamokuNm";
            this.F3KamokuNm.ReadOnly = true;
            this.F3KamokuNm.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.F3KamokuNm.Width = 200;
            // 
            // F3gyoBango
            // 
            this.F3gyoBango.HeaderText = "行番号";
            this.F3gyoBango.Name = "F3gyoBango";
            this.F3gyoBango.ReadOnly = true;
            this.F3gyoBango.Visible = false;
            // 
            // F3Taishaku
            // 
            this.F3Taishaku.HeaderText = "貸借";
            this.F3Taishaku.Name = "F3Taishaku";
            this.F3Taishaku.ReadOnly = true;
            this.F3Taishaku.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.F3Taishaku.Width = 70;
            // 
            // F3Moji
            // 
            this.F3Moji.HeaderText = "文字";
            this.F3Moji.Name = "F3Moji";
            this.F3Moji.ReadOnly = true;
            this.F3Moji.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.F3Moji.Width = 70;
            // 
            // F3deleteAndAddNo
            // 
            this.F3deleteAndAddNo.HeaderText = "削除と追加用行番号";
            this.F3deleteAndAddNo.Name = "F3deleteAndAddNo";
            this.F3deleteAndAddNo.ReadOnly = true;
            this.F3deleteAndAddNo.Visible = false;
            // 
            // F3hyojiJuni
            // 
            this.F3hyojiJuni.HeaderText = "表示順位";
            this.F3hyojiJuni.Name = "F3hyojiJuni";
            this.F3hyojiJuni.ReadOnly = true;
            this.F3hyojiJuni.Visible = false;
            // 
            // gbxYoyaku
            // 
            this.gbxYoyaku.Controls.Add(this.gbxMoji);
            this.gbxYoyaku.Controls.Add(this.gbxKanjoKamoku);
            this.gbxYoyaku.Controls.Add(this.gbxTaishaku);
            this.gbxYoyaku.Controls.Add(this.txtKamokuNm);
            this.gbxYoyaku.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxYoyaku.ForeColor = System.Drawing.Color.Black;
            this.gbxYoyaku.Location = new System.Drawing.Point(396, 12);
            this.gbxYoyaku.Name = "gbxYoyaku";
            this.gbxYoyaku.Size = new System.Drawing.Size(415, 517);
            this.gbxYoyaku.TabIndex = 0;
            this.gbxYoyaku.TabStop = false;
            this.gbxYoyaku.Text = "要約設定";
            // 
            // gbxMoji
            // 
            this.gbxMoji.Controls.Add(this.lblMemo);
            this.gbxMoji.Controls.Add(this.txtMoji);
            this.gbxMoji.Controls.Add(this.lblMoji);
            this.gbxMoji.Location = new System.Drawing.Point(9, 453);
            this.gbxMoji.Name = "gbxMoji";
            this.gbxMoji.Size = new System.Drawing.Size(395, 56);
            this.gbxMoji.TabIndex = 3;
            this.gbxMoji.TabStop = false;
            // 
            // lblMemo
            // 
            this.lblMemo.BackColor = System.Drawing.Color.Silver;
            this.lblMemo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMemo.ForeColor = System.Drawing.Color.Black;
            this.lblMemo.Location = new System.Drawing.Point(157, 19);
            this.lblMemo.Name = "lblMemo";
            this.lblMemo.Size = new System.Drawing.Size(230, 27);
            this.lblMemo.TabIndex = 2;
            this.lblMemo.Text = "0：標準　1：太字";
            this.lblMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtMoji
            // 
            this.txtMoji.AutoSizeFromLength = false;
            this.txtMoji.DisplayLength = null;
            this.txtMoji.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMoji.ForeColor = System.Drawing.Color.Black;
            this.txtMoji.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtMoji.Location = new System.Drawing.Point(120, 23);
            this.txtMoji.MaxLength = 2;
            this.txtMoji.Name = "txtMoji";
            this.txtMoji.Size = new System.Drawing.Size(30, 20);
            this.txtMoji.TabIndex = 1;
            this.txtMoji.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMoji.Enter += new System.EventHandler(this.moji_Enter);
            this.txtMoji.KeyUp += new System.Windows.Forms.KeyEventHandler(this.moji_change);
            // 
            // lblMoji
            // 
            this.lblMoji.BackColor = System.Drawing.Color.Silver;
            this.lblMoji.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMoji.ForeColor = System.Drawing.Color.Black;
            this.lblMoji.Location = new System.Drawing.Point(10, 19);
            this.lblMoji.Name = "lblMoji";
            this.lblMoji.Size = new System.Drawing.Size(143, 27);
            this.lblMoji.TabIndex = 0;
            this.lblMoji.Text = "文字スタイル";
            this.lblMoji.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gbxKanjoKamoku
            // 
            this.gbxKanjoKamoku.Controls.Add(this.btnAdd);
            this.gbxKanjoKamoku.Controls.Add(this.btnDel);
            this.gbxKanjoKamoku.Controls.Add(this.gbxKanjoKamokuIchiran);
            this.gbxKanjoKamoku.Controls.Add(this.gbxTaishoKanjoKamoku);
            this.gbxKanjoKamoku.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxKanjoKamoku.ForeColor = System.Drawing.Color.Black;
            this.gbxKanjoKamoku.Location = new System.Drawing.Point(9, 61);
            this.gbxKanjoKamoku.Name = "gbxKanjoKamoku";
            this.gbxKanjoKamoku.Size = new System.Drawing.Size(396, 390);
            this.gbxKanjoKamoku.TabIndex = 2;
            this.gbxKanjoKamoku.TabStop = false;
            this.gbxKanjoKamoku.Text = "勘定科目設定";
            // 
            // btnAdd
            // 
            this.btnAdd.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnAdd.Location = new System.Drawing.Point(204, 184);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(40, 40);
            this.btnAdd.TabIndex = 3;
            this.btnAdd.Text = "▲";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnUMove_Click);
            // 
            // btnDel
            // 
            this.btnDel.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnDel.Location = new System.Drawing.Point(149, 184);
            this.btnDel.Name = "btnDel";
            this.btnDel.Size = new System.Drawing.Size(40, 40);
            this.btnDel.TabIndex = 2;
            this.btnDel.Text = "▼";
            this.btnDel.UseVisualStyleBackColor = true;
            this.btnDel.Click += new System.EventHandler(this.btnDMove_Click);
            // 
            // gbxKanjoKamokuIchiran
            // 
            this.gbxKanjoKamokuIchiran.Controls.Add(this.lbxKanjoKamokuIchiran);
            this.gbxKanjoKamokuIchiran.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxKanjoKamokuIchiran.ForeColor = System.Drawing.Color.Black;
            this.gbxKanjoKamokuIchiran.Location = new System.Drawing.Point(9, 231);
            this.gbxKanjoKamokuIchiran.Name = "gbxKanjoKamokuIchiran";
            this.gbxKanjoKamokuIchiran.Size = new System.Drawing.Size(378, 150);
            this.gbxKanjoKamokuIchiran.TabIndex = 1;
            this.gbxKanjoKamokuIchiran.TabStop = false;
            this.gbxKanjoKamokuIchiran.Text = "勘定科目一覧";
            // 
            // lbxKanjoKamokuIchiran
            // 
            this.lbxKanjoKamokuIchiran.FormattingEnabled = true;
            this.lbxKanjoKamokuIchiran.Location = new System.Drawing.Point(9, 19);
            this.lbxKanjoKamokuIchiran.Name = "lbxKanjoKamokuIchiran";
            this.lbxKanjoKamokuIchiran.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.lbxKanjoKamokuIchiran.Size = new System.Drawing.Size(360, 121);
            this.lbxKanjoKamokuIchiran.TabIndex = 0;
            // 
            // gbxTaishoKanjoKamoku
            // 
            this.gbxTaishoKanjoKamoku.Controls.Add(this.lbxTaishoKanjoKamoku);
            this.gbxTaishoKanjoKamoku.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxTaishoKanjoKamoku.ForeColor = System.Drawing.Color.Black;
            this.gbxTaishoKanjoKamoku.Location = new System.Drawing.Point(9, 22);
            this.gbxTaishoKanjoKamoku.Name = "gbxTaishoKanjoKamoku";
            this.gbxTaishoKanjoKamoku.Size = new System.Drawing.Size(378, 150);
            this.gbxTaishoKanjoKamoku.TabIndex = 0;
            this.gbxTaishoKanjoKamoku.TabStop = false;
            this.gbxTaishoKanjoKamoku.Text = "対象勘定科目";
            // 
            // lbxTaishoKanjoKamoku
            // 
            this.lbxTaishoKanjoKamoku.FormattingEnabled = true;
            this.lbxTaishoKanjoKamoku.Location = new System.Drawing.Point(9, 19);
            this.lbxTaishoKanjoKamoku.Name = "lbxTaishoKanjoKamoku";
            this.lbxTaishoKanjoKamoku.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.lbxTaishoKanjoKamoku.Size = new System.Drawing.Size(360, 121);
            this.lbxTaishoKanjoKamoku.TabIndex = 0;
            // 
            // gbxTaishaku
            // 
            this.gbxTaishaku.Controls.Add(this.rdoKashi);
            this.gbxTaishaku.Controls.Add(this.rdoKari);
            this.gbxTaishaku.Location = new System.Drawing.Point(277, 12);
            this.gbxTaishaku.Name = "gbxTaishaku";
            this.gbxTaishaku.Size = new System.Drawing.Size(128, 43);
            this.gbxTaishaku.TabIndex = 1;
            this.gbxTaishaku.TabStop = false;
            // 
            // rdoKashi
            // 
            this.rdoKashi.AutoSize = true;
            this.rdoKashi.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoKashi.ForeColor = System.Drawing.Color.Black;
            this.rdoKashi.Location = new System.Drawing.Point(74, 16);
            this.rdoKashi.Name = "rdoKashi";
            this.rdoKashi.Size = new System.Drawing.Size(53, 17);
            this.rdoKashi.TabIndex = 1;
            this.rdoKashi.TabStop = true;
            this.rdoKashi.Text = "貸方";
            this.rdoKashi.UseVisualStyleBackColor = true;
            this.rdoKashi.Click += new System.EventHandler(this.rdoKashi_Click);
            // 
            // rdoKari
            // 
            this.rdoKari.AutoSize = true;
            this.rdoKari.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoKari.ForeColor = System.Drawing.Color.Black;
            this.rdoKari.Location = new System.Drawing.Point(10, 16);
            this.rdoKari.Name = "rdoKari";
            this.rdoKari.Size = new System.Drawing.Size(53, 17);
            this.rdoKari.TabIndex = 0;
            this.rdoKari.TabStop = true;
            this.rdoKari.Text = "借方";
            this.rdoKari.UseVisualStyleBackColor = true;
            this.rdoKari.Click += new System.EventHandler(this.rdoKari_Click);
            // 
            // txtKamokuNm
            // 
            this.txtKamokuNm.AutoSizeFromLength = false;
            this.txtKamokuNm.DisplayLength = null;
            this.txtKamokuNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKamokuNm.ForeColor = System.Drawing.Color.Black;
            this.txtKamokuNm.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtKamokuNm.Location = new System.Drawing.Point(9, 28);
            this.txtKamokuNm.MaxLength = 30;
            this.txtKamokuNm.Name = "txtKamokuNm";
            this.txtKamokuNm.Size = new System.Drawing.Size(262, 20);
            this.txtKamokuNm.TabIndex = 0;
            this.txtKamokuNm.KeyUp += new System.Windows.Forms.KeyEventHandler(this.kamokuNm_change);
            // 
            // ZMMR1073
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 590);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.gbxYoyaku);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "ZMMR1073";
            this.ShowFButton = true;
            this.Text = "";
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.gbxYoyaku, 0);
            this.Controls.SetChildIndex(this.tabControl, 0);
            this.pnlDebug.ResumeLayout(false);
            this.tabControl.ResumeLayout(false);
            this.F1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mtbListF1)).EndInit();
            this.F2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mtbListF2)).EndInit();
            this.F3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mtbListF3)).EndInit();
            this.gbxYoyaku.ResumeLayout(false);
            this.gbxYoyaku.PerformLayout();
            this.gbxMoji.ResumeLayout(false);
            this.gbxMoji.PerformLayout();
            this.gbxKanjoKamoku.ResumeLayout(false);
            this.gbxKanjoKamokuIchiran.ResumeLayout(false);
            this.gbxTaishoKanjoKamoku.ResumeLayout(false);
            this.gbxTaishaku.ResumeLayout(false);
            this.gbxTaishaku.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage F1;
        private System.Windows.Forms.TabPage F2;
        private System.Windows.Forms.TabPage F3;
        private System.Windows.Forms.GroupBox gbxYoyaku;
        private System.Windows.Forms.GroupBox gbxTaishaku;
        private System.Windows.Forms.RadioButton rdoKashi;
        private System.Windows.Forms.RadioButton rdoKari;
        private jp.co.fsi.common.controls.FsiTextBox txtKamokuNm;
        private System.Windows.Forms.GroupBox gbxKanjoKamoku;
        private System.Windows.Forms.GroupBox gbxTaishoKanjoKamoku;
        private System.Windows.Forms.GroupBox gbxKanjoKamokuIchiran;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnDel;
        private System.Windows.Forms.GroupBox gbxMoji;
        private System.Windows.Forms.Label lblMemo;
        private jp.co.fsi.common.controls.FsiTextBox txtMoji;
        private System.Windows.Forms.Label lblMoji;
        private System.Windows.Forms.ListBox lbxKanjoKamokuIchiran;
        private System.Windows.Forms.ListBox lbxTaishoKanjoKamoku;
        private System.Windows.Forms.DataGridView mtbListF1;
        private System.Windows.Forms.DataGridView mtbListF2;
        private System.Windows.Forms.DataGridView mtbListF3;
        private System.Windows.Forms.DataGridViewTextBoxColumn F1VisibleHandan;
        private System.Windows.Forms.DataGridViewTextBoxColumn F1kamokuCd;
        private System.Windows.Forms.DataGridViewTextBoxColumn F1KamokuNm;
        private System.Windows.Forms.DataGridViewTextBoxColumn F1gyoBango;
        private System.Windows.Forms.DataGridViewTextBoxColumn F1Taishaku;
        private System.Windows.Forms.DataGridViewTextBoxColumn F1Moji;
        private System.Windows.Forms.DataGridViewTextBoxColumn F1deleteAndAddNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn F1hyojiJuni;
        private System.Windows.Forms.DataGridViewTextBoxColumn F2VisibleHandan;
        private System.Windows.Forms.DataGridViewTextBoxColumn F2kamokuCd;
        private System.Windows.Forms.DataGridViewTextBoxColumn F2KamokuNm;
        private System.Windows.Forms.DataGridViewTextBoxColumn F2gyoBango;
        private System.Windows.Forms.DataGridViewTextBoxColumn F2Taishaku;
        private System.Windows.Forms.DataGridViewTextBoxColumn F2Moji;
        private System.Windows.Forms.DataGridViewTextBoxColumn F2deleteAndAddNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn F2hyojiJuni;
        private System.Windows.Forms.DataGridViewTextBoxColumn F3VisibleHandan;
        private System.Windows.Forms.DataGridViewTextBoxColumn F3kamokuCd;
        private System.Windows.Forms.DataGridViewTextBoxColumn F3KamokuNm;
        private System.Windows.Forms.DataGridViewTextBoxColumn F3gyoBango;
        private System.Windows.Forms.DataGridViewTextBoxColumn F3Taishaku;
        private System.Windows.Forms.DataGridViewTextBoxColumn F3Moji;
        private System.Windows.Forms.DataGridViewTextBoxColumn F3deleteAndAddNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn F3hyojiJuni;






    }
}