﻿namespace jp.co.fsi.zm.zmcm1031
{
    partial class ZMCM1035
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbxKamokuBunruiCd = new System.Windows.Forms.GroupBox();
            this.lblKamokuBunruiCdTo = new System.Windows.Forms.Label();
            this.lblKamokuBunruiCdFr = new System.Windows.Forms.Label();
            this.txtKamokuBunruiCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKamokuBunruiCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblCodeBet1 = new System.Windows.Forms.Label();
            this.gbxKanjoKamokuCd = new System.Windows.Forms.GroupBox();
            this.lblKanjoKamokuCdTo = new System.Windows.Forms.Label();
            this.lblKanjoKamokuCdFr = new System.Windows.Forms.Label();
            this.txtKanjoKamokuCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKanjoKamokuCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblCodeBet2 = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.gbxKamokuBunruiCd.SuspendLayout();
            this.gbxKanjoKamokuCd.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(505, 23);
            this.lblTitle.Text = "勘定科目の登録";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(5, 96);
            this.pnlDebug.Size = new System.Drawing.Size(538, 100);
            // 
            // gbxKamokuBunruiCd
            // 
            this.gbxKamokuBunruiCd.Controls.Add(this.lblKamokuBunruiCdTo);
            this.gbxKamokuBunruiCd.Controls.Add(this.lblKamokuBunruiCdFr);
            this.gbxKamokuBunruiCd.Controls.Add(this.txtKamokuBunruiCdTo);
            this.gbxKamokuBunruiCd.Controls.Add(this.txtKamokuBunruiCdFr);
            this.gbxKamokuBunruiCd.Controls.Add(this.lblCodeBet1);
            this.gbxKamokuBunruiCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxKamokuBunruiCd.Location = new System.Drawing.Point(12, 12);
            this.gbxKamokuBunruiCd.Name = "gbxKamokuBunruiCd";
            this.gbxKamokuBunruiCd.Size = new System.Drawing.Size(509, 60);
            this.gbxKamokuBunruiCd.TabIndex = 1;
            this.gbxKamokuBunruiCd.TabStop = false;
            this.gbxKamokuBunruiCd.Text = "科目分類コード範囲";
            // 
            // lblKamokuBunruiCdTo
            // 
            this.lblKamokuBunruiCdTo.BackColor = System.Drawing.Color.Silver;
            this.lblKamokuBunruiCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKamokuBunruiCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKamokuBunruiCdTo.Location = new System.Drawing.Point(318, 24);
            this.lblKamokuBunruiCdTo.Name = "lblKamokuBunruiCdTo";
            this.lblKamokuBunruiCdTo.Size = new System.Drawing.Size(180, 20);
            this.lblKamokuBunruiCdTo.TabIndex = 4;
            this.lblKamokuBunruiCdTo.Text = "最　後";
            this.lblKamokuBunruiCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKamokuBunruiCdFr
            // 
            this.lblKamokuBunruiCdFr.BackColor = System.Drawing.Color.Silver;
            this.lblKamokuBunruiCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKamokuBunruiCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKamokuBunruiCdFr.Location = new System.Drawing.Point(61, 24);
            this.lblKamokuBunruiCdFr.Name = "lblKamokuBunruiCdFr";
            this.lblKamokuBunruiCdFr.Size = new System.Drawing.Size(180, 20);
            this.lblKamokuBunruiCdFr.TabIndex = 1;
            this.lblKamokuBunruiCdFr.Text = "先　頭";
            this.lblKamokuBunruiCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKamokuBunruiCdTo
            // 
            this.txtKamokuBunruiCdTo.AutoSizeFromLength = true;
            this.txtKamokuBunruiCdTo.DisplayLength = null;
            this.txtKamokuBunruiCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKamokuBunruiCdTo.Location = new System.Drawing.Point(267, 24);
            this.txtKamokuBunruiCdTo.MaxLength = 5;
            this.txtKamokuBunruiCdTo.Name = "txtKamokuBunruiCdTo";
            this.txtKamokuBunruiCdTo.Size = new System.Drawing.Size(48, 20);
            this.txtKamokuBunruiCdTo.TabIndex = 3;
            this.txtKamokuBunruiCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKamokuBunruiCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtKamokuBunruiCdTo_Validating);
            // 
            // txtKamokuBunruiCdFr
            // 
            this.txtKamokuBunruiCdFr.AutoSizeFromLength = true;
            this.txtKamokuBunruiCdFr.DisplayLength = null;
            this.txtKamokuBunruiCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKamokuBunruiCdFr.Location = new System.Drawing.Point(10, 24);
            this.txtKamokuBunruiCdFr.MaxLength = 5;
            this.txtKamokuBunruiCdFr.Name = "txtKamokuBunruiCdFr";
            this.txtKamokuBunruiCdFr.Size = new System.Drawing.Size(48, 20);
            this.txtKamokuBunruiCdFr.TabIndex = 0;
            this.txtKamokuBunruiCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKamokuBunruiCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtKamokuBunruiCdFr_Validating);
            // 
            // lblCodeBet1
            // 
            this.lblCodeBet1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCodeBet1.Location = new System.Drawing.Point(243, 24);
            this.lblCodeBet1.Name = "lblCodeBet1";
            this.lblCodeBet1.Size = new System.Drawing.Size(18, 20);
            this.lblCodeBet1.TabIndex = 2;
            this.lblCodeBet1.Text = "～";
            this.lblCodeBet1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gbxKanjoKamokuCd
            // 
            this.gbxKanjoKamokuCd.Controls.Add(this.lblKanjoKamokuCdTo);
            this.gbxKanjoKamokuCd.Controls.Add(this.lblKanjoKamokuCdFr);
            this.gbxKanjoKamokuCd.Controls.Add(this.txtKanjoKamokuCdTo);
            this.gbxKanjoKamokuCd.Controls.Add(this.txtKanjoKamokuCdFr);
            this.gbxKanjoKamokuCd.Controls.Add(this.lblCodeBet2);
            this.gbxKanjoKamokuCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxKanjoKamokuCd.Location = new System.Drawing.Point(12, 78);
            this.gbxKanjoKamokuCd.Name = "gbxKanjoKamokuCd";
            this.gbxKanjoKamokuCd.Size = new System.Drawing.Size(509, 60);
            this.gbxKanjoKamokuCd.TabIndex = 2;
            this.gbxKanjoKamokuCd.TabStop = false;
            this.gbxKanjoKamokuCd.Text = "勘定科目コード範囲";
            // 
            // lblKanjoKamokuCdTo
            // 
            this.lblKanjoKamokuCdTo.BackColor = System.Drawing.Color.Silver;
            this.lblKanjoKamokuCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKanjoKamokuCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKanjoKamokuCdTo.Location = new System.Drawing.Point(318, 24);
            this.lblKanjoKamokuCdTo.Name = "lblKanjoKamokuCdTo";
            this.lblKanjoKamokuCdTo.Size = new System.Drawing.Size(180, 20);
            this.lblKanjoKamokuCdTo.TabIndex = 4;
            this.lblKanjoKamokuCdTo.Text = "最　後";
            this.lblKanjoKamokuCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKanjoKamokuCdFr
            // 
            this.lblKanjoKamokuCdFr.BackColor = System.Drawing.Color.Silver;
            this.lblKanjoKamokuCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKanjoKamokuCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKanjoKamokuCdFr.Location = new System.Drawing.Point(61, 24);
            this.lblKanjoKamokuCdFr.Name = "lblKanjoKamokuCdFr";
            this.lblKanjoKamokuCdFr.Size = new System.Drawing.Size(180, 20);
            this.lblKanjoKamokuCdFr.TabIndex = 1;
            this.lblKanjoKamokuCdFr.Text = "先　頭";
            this.lblKanjoKamokuCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKanjoKamokuCdTo
            // 
            this.txtKanjoKamokuCdTo.AutoSizeFromLength = true;
            this.txtKanjoKamokuCdTo.DisplayLength = null;
            this.txtKanjoKamokuCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKanjoKamokuCdTo.Location = new System.Drawing.Point(267, 24);
            this.txtKanjoKamokuCdTo.MaxLength = 6;
            this.txtKanjoKamokuCdTo.Name = "txtKanjoKamokuCdTo";
            this.txtKanjoKamokuCdTo.Size = new System.Drawing.Size(48, 20);
            this.txtKanjoKamokuCdTo.TabIndex = 3;
            this.txtKanjoKamokuCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKanjoKamokuCdTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtKanjoKamokuCdTo_KeyDown);
            this.txtKanjoKamokuCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtKanjoKamokuCdTo_Validating);
            // 
            // txtKanjoKamokuCdFr
            // 
            this.txtKanjoKamokuCdFr.AutoSizeFromLength = true;
            this.txtKanjoKamokuCdFr.DisplayLength = null;
            this.txtKanjoKamokuCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKanjoKamokuCdFr.Location = new System.Drawing.Point(10, 24);
            this.txtKanjoKamokuCdFr.MaxLength = 6;
            this.txtKanjoKamokuCdFr.Name = "txtKanjoKamokuCdFr";
            this.txtKanjoKamokuCdFr.Size = new System.Drawing.Size(48, 20);
            this.txtKanjoKamokuCdFr.TabIndex = 0;
            this.txtKanjoKamokuCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKanjoKamokuCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtKanjoKamokuCdFr_Validating);
            // 
            // lblCodeBet2
            // 
            this.lblCodeBet2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCodeBet2.Location = new System.Drawing.Point(243, 24);
            this.lblCodeBet2.Name = "lblCodeBet2";
            this.lblCodeBet2.Size = new System.Drawing.Size(18, 20);
            this.lblCodeBet2.TabIndex = 2;
            this.lblCodeBet2.Text = "～";
            this.lblCodeBet2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ZMCM1035
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(530, 199);
            this.Controls.Add(this.gbxKanjoKamokuCd);
            this.Controls.Add(this.gbxKamokuBunruiCd);
            this.Name = "ZMCM1035";
            this.ShowFButton = true;
            this.Text = "勘定科目の印刷";
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.gbxKamokuBunruiCd, 0);
            this.Controls.SetChildIndex(this.gbxKanjoKamokuCd, 0);
            this.pnlDebug.ResumeLayout(false);
            this.gbxKamokuBunruiCd.ResumeLayout(false);
            this.gbxKamokuBunruiCd.PerformLayout();
            this.gbxKanjoKamokuCd.ResumeLayout(false);
            this.gbxKanjoKamokuCd.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbxKamokuBunruiCd;
        private System.Windows.Forms.Label lblKamokuBunruiCdTo;
        private System.Windows.Forms.Label lblKamokuBunruiCdFr;
        private jp.co.fsi.common.controls.FsiTextBox txtKamokuBunruiCdTo;
        private jp.co.fsi.common.controls.FsiTextBox txtKamokuBunruiCdFr;
        private System.Windows.Forms.Label lblCodeBet1;
        private System.Windows.Forms.GroupBox gbxKanjoKamokuCd;
        private System.Windows.Forms.Label lblKanjoKamokuCdTo;
        private System.Windows.Forms.Label lblKanjoKamokuCdFr;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuCdTo;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuCdFr;
        private System.Windows.Forms.Label lblCodeBet2;

    }
}