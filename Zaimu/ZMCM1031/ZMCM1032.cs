﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Reflection;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.zm.zmcm1031
{
    /// <summary>
    /// 勘定科目の登録(ZMCM1032)
    /// </summary>
    public partial class ZMCM1032 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(新規)
        /// </summary>
        private const string MODE_NEW = "1";

        /// <summary>
        /// モード(編集)
        /// </summary>
        private const string MODE_EDIT = "2";
        #endregion

        #region 変数
        private int _swkUmu = 0;                    // 仕訳データ有無
        private int _hjkUmu = 0;                    // 補助科目データ有無
        private int _hjkKbn = 0;                    // 補助使用区分
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ZMCM1032()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="par1">引数1</param>
        public ZMCM1032(string par1)
            : base(par1)
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 引数：Par1／モード(1:新規、2:変更)、InData：勘定科目コード
            if (MODE_NEW.Equals(this.Par1))
            {
                // 新規モードの初期表示
                InitDispOnNew();
                this.btnF3.Enabled = false;
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 編集モードの初期表示
                InitDispOnEdit();
            }
            else
            {
                // 不正な起動として閉じる
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }

            // タイトルは非表示
            this.lblTitle.Visible = false;
            // EscapeとF1のみ表示
            this.ShowFButton = true;
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF1.Location = this.btnF2.Location;
            this.btnF6.Location = this.btnF4.Location;
            this.btnF2.Visible = false;
            this.btnF3.Visible = true;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF6.Visible = true;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;

            this._swkUmu = 0;
            this._hjkUmu = 0;
            this._hjkKbn = 0;
    }

    /// <summary>
    /// フォーカス移動時処理
    /// </summary>
    protected override void OnMoveFocus()
        {
            // 科目分類コード、科目区分、借方税区分、貸方税区分に
            // フォーカス時のみF1を有効にする
            switch (this.ActiveCtlNm)
            {
                case "txtKamokuBnCd":
                case "txtKamokuKubn":
                case "txtKarikataKubn":
                case "txtKashikataKubn":
                    this.btnF1.Enabled = true;
                    break;

                default:
                    this.btnF1.Enabled = false;
                    break;
            }

            switch (this.ActiveCtlNm)
            {
                case "txtkanjoNm":
                    this.txtkanjoNm.ImeMode = ImeMode.Katakana;
                    break;
                case "txtkanjoKanaNm":
                    this.txtkanjoNm.ImeMode = ImeMode.KatakanaHalf;
                    break;
                case "txtKamokuBnCd":
                case "txtKamokuKubn":
                case "txtTaishakuKubn":
                case "txtKarikataKubn":
                case "txtKashikataKubn":
                    this.txtkanjoNm.ImeMode = ImeMode.Off;
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }
        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;

            switch (this.ActiveCtlNm)
            {
                case "txtKamokuBnCd":
                    #region 科目分類
                    // アセンブリのロード
                    //asm = System.Reflection.Assembly.LoadFrom("COMC8011.exe");
                    asm = Assembly.LoadFrom("CMCM1041.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.com.comc8011.COMC8011");
                    t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "TB_ZM_F_KAMOKU_BUNRUI";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.txtKamokuBnCd.Text = result[0];
                                //this.lblKamokuBnCd.Text = result[1];
                                this.txtKamokuBnNm.Text = result[1];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtKamokuKubn":
                    #region 科目区分
                    // アセンブリのロード
                    //asm = System.Reflection.Assembly.LoadFrom("COMC8011.exe");
                    asm = Assembly.LoadFrom("CMCM1041.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.com.comc8011.COMC8011");
                    t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "TB_ZM_F_KAMOKU_KUBUN";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.txtKamokuKubn.Text = result[0];
                                //this.lblKamokuKubn.Text = result[1];
                                this.txtKamokuKubnNm.Text = result[1];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtKarikataKubn":
                    #region 借方税区分
                    // アセンブリのロード
                    //asm = System.Reflection.Assembly.LoadFrom("COMC8011.exe");
                    asm = Assembly.LoadFrom("CMCM1041.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.com.comc8011.COMC8011");
                    t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "TB_ZM_F_ZEI_KUBUN";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.txtKarikataKubn.Text = result[0];
                                this.lblkari.Text = result[1];
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtKashikataKubn":
                    #region 貸方税区分
                    // アセンブリのロード
                    //asm = System.Reflection.Assembly.LoadFrom("COMC8011.exe");
                    asm = Assembly.LoadFrom("CMCM1041.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.com.comc8011.COMC8011");
                    t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "TB_ZM_F_ZEI_KUBUN";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.txtKashikataKubn.Text = result[0];
                                this.lblkashi.Text = result[1];
                            }
                        }
                    }
                    #endregion
                    break;

                default:
                    break;
            }
        }
        /// <summary>
        /// F3キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF3();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF3()
        {
            if (!this.btnF3.Enabled)
                return;

            // 新規モードの場合は処理させない
            if (this.Par1.Equals(MODE_NEW)) return;
            // 削除する前に再度コードの存在チェック
            if (IsCodeUsing())
            {
                Msg.Error("勘定科目コードが使用されているため、削除できません。");
                return;
            }
            if (Msg.ConfYesNo("削除しますか？") == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }
            // 削除処理
            try
            {
                // データ削除
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                dpc.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, this.InData);
                this.Dba.Delete("TB_ZM_KANJO_KAMOKU", "KAISHA_CD = @KAISHA_CD AND KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD", dpc);

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF6();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF6()
        {
            if (!this.btnF6.Enabled)
                return;

            // 全項目を再度入力値チェック
            if (!ValidateAll(true))
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // 確認メッセージを表示
            string msg = (MODE_NEW.Equals(this.Par1) ? "登録" : "更新") + "しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                this.txtShukeihoho.Focus();
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 入力値をバインドパラメータとしてセットする
            ArrayList alParamsZmkanjo = SetZmKanjoParams();

            try
            {
                this.Dba.BeginTransaction();

                if (MODE_NEW.Equals(this.Par1))
                {
                    if (IsCodeExists())
                    {
                        Msg.Error("勘定科目情報が登録済みです。");
                        return;
                    }

                    // データ登録
                    // 共通.取引先マスタ
                    this.Dba.Insert("TB_ZM_KANJO_KAMOKU", (DbParamCollection)alParamsZmkanjo[0]);
                }
                else if (MODE_EDIT.Equals(this.Par1))
                {

                    // データ更新
                    // 共通.取引先マスタ
                    this.Dba.Update("TB_ZM_KANJO_KAMOKU",
                        (DbParamCollection)alParamsZmkanjo[1],
                        "KAISHA_CD = @KAISHA_CD AND KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD AND KAIKEI_NENDO = @KAIKEI_NENDO",
                        (DbParamCollection)alParamsZmkanjo[0]);
                }

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();

        }
        #endregion

        #region イベント
        /// <summary>
        /// 勘定科目コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtkanjoCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidkanjoCd())
            {
                e.Cancel = true;
                this.txtkanjoCd.SelectAll();
            }
        }

        /// <summary>
        /// 勘定科目名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtkanjoNm_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidkanjoKanaNm())
            {
                e.Cancel = true;
                this.txtkanjoNm.SelectAll();
            }
        }

        /// <summary>
        /// 勘定科目カナ名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtkanjoKanaNm_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidkanjoKanaNm())
            {
                e.Cancel = true;
                this.txtkanjoKanaNm.SelectAll();
            }
        }

        /// <summary>
        /// 科目分類コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKamokuBnCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidKamokuBnCd())
            {
                e.Cancel = true;
                this.txtKamokuBnCd.SelectAll();
            }
        }

        /// <summary>
        /// 科目区分の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKamokuKubn_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidKamokuKubn())
            {
                e.Cancel = true;
                this.txtKamokuKubn.SelectAll();
            }
        }

        /// <summary>
        /// 貸借区分の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTaishakuKubn_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidTaishakuKubn())
            {
                e.Cancel = true;
                this.txtTaishakuKubn.SelectAll();
            }
        }

        /// <summary>
        /// 借方税区分の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKarikataKubn_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidKarikataKubn())
            {
                e.Cancel = true;
                this.txtKarikataKubn.SelectAll();
            }
        }

        /// <summary>
        /// 貸方税区分の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKashikataKubn_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidKashikataKubn())
            {
                e.Cancel = true;
                this.txtKashikataKubn.SelectAll();
            }
        }

        /// <summary>
        /// 部門管理の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBumonKnri_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidBumonKnri())
            {
                e.Cancel = true;
                this.txtBumonKnri.SelectAll();
            }
        }

        /// <summary>
        /// 補助科目管理の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtHojoKamokuKnri_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidHojoKamokuKnri(false))
            {
                e.Cancel = true;
                this.txtHojoKamokuKnri.SelectAll();
            }
        }

        /// <summary>
        /// 補助使用区分の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtHojoShiyoKubn_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidHojoShiyoKubn())
            {
                e.Cancel = true;
                this.txtHojoShiyoKubn.SelectAll();
            }
        }

        /// <summary>
        /// 工事管理の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKojiKnri_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidKojiKnri())
            {
                e.Cancel = true;
                this.txtKojiKnri.SelectAll();
            }
        }

        /// <summary>
        /// 集計コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShukeiCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidHojoShiyoKubn())
            {
                e.Cancel = true;
                this.txtShukeiCd.SelectAll();
            }
        }

        /// <summary>
        /// 合計計算区分の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGokeiKeisanKubn_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidGokeiKeisanKubn())
            {
                e.Cancel = true;
                this.txtGokeiKeisanKubn.SelectAll();
            }
        }

        /// <summary>
        /// 集計方法の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShukeihoho_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShukeihoho())
            {
                e.Cancel = true;
                this.txtShukeihoho.SelectAll();
            }
        }

        /// <summary>
        /// 集計方法Enter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShukeihoho_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.PressF6();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 新規モードの初期表示
        /// </summary>
        private void InitDispOnNew()
        {
            // 初期値、入力制御を実装

            // 勘定科目コードの初期値を取得
            // 勘定科目の中でのMAX+1を初期表示する
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            StringBuilder where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND KAISHA_CD = @KAISHA_CD");
            DataTable dtMaxShrSk =
                this.Dba.GetDataTableByConditionWithParams("MAX(KANJO_KAMOKU_CD) AS MAX_CD",
                    "TB_ZM_KANJO_KAMOKU", Util.ToString(where), dpc);
            if (dtMaxShrSk.Rows.Count > 0 && !ValChk.IsEmpty(dtMaxShrSk.Rows[0]["MAX_CD"]))
            {
                this.txtkanjoCd.Text = Util.ToString(Util.ToInt(dtMaxShrSk.Rows[0]["MAX_CD"]) + 1);
            }
            else
            {
                this.txtkanjoCd.Text = "100";
            }
            // 各項目の初期値を設定
            // 勘定科目コード0
            this.txtKamokuBnCd.Text = "0";
            // 科目区分0
            this.txtKamokuKubn.Text = "0";
            // 貸借区分0
            this.txtTaishakuKubn.Text = "1";
            // 借方税区分0
            this.txtKarikataKubn.Text = "0";
            // 貸方税区分0
            this.txtKashikataKubn.Text = "0";
            // 部門管理0
            this.txtBumonKnri.Text = "0";
            // 補助科目管理0
            this.txtHojoKamokuKnri.Text = "0";
            // 補助使用区分1
            this.txtHojoShiyoKubn.Text = "0";
            // 工事管理0
            this.txtKojiKnri.Text = "0";
            // 勘定科目名に初期フォーカス
            this.ActiveControl = this.txtkanjoNm;
            this.txtkanjoNm.Focus();
            ////補助使用区分は入力不可
            if (this.txtHojoKamokuKnri.Text == "1")
            {
                this.txtHojoShiyoKubn.Enabled = true;
            }
            else
            {
                this.txtHojoShiyoKubn.Enabled = false;
            }
        }

        /// <summary>
        /// 編集モードの初期表示
        /// </summary>
        private void InitDispOnEdit()
        {
            // 現在DBに登録されている値、入力制御を実装
            StringBuilder cols = new StringBuilder();
            cols.Append("A.KAISHA_CD");
            cols.Append(" ,A.KANJO_KAMOKU_CD");
            cols.Append(" ,A.KANJO_KAMOKU_NM");
            cols.Append(" ,A.KANJO_KAMOKU_KANA_NM");
            cols.Append(" ,A.KAMOKU_BUNRUI_CD");
            cols.Append(" ,A.KAMOKU_KUBUN");
            cols.Append(" ,A.TAISHAKU_KUBUN");
            cols.Append(" ,A.KARIKATA_ZEI_KUBUN");
            cols.Append(" ,A.KASHIKATA_ZEI_KUBUN");
            cols.Append(" ,A.BUMON_UMU");
            cols.Append(" ,A.HOJO_KAMOKU_UMU");
            cols.Append(" ,A.HOJO_SHIYO_KUBUN");
            cols.Append(" ,A.KOJI_UMU");
            cols.Append(" ,A.SHUKEI_CD");
            cols.Append(" ,A.SHUKEI_KEISAN_KUBUN");

            StringBuilder from = new StringBuilder();
            from.Append("TB_ZM_KANJO_KAMOKU AS A");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, Util.ToString(this.InData));
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 6, this.UInfo.KaikeiNendo);

            DataTable dtDispData =
                this.Dba.GetDataTableByConditionWithParams(
                    Util.ToString(cols), Util.ToString(from),
                    "A.KAISHA_CD = @KAISHA_CD AND A.KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD AND A.KAIKEI_NENDO = @KAIKEI_NENDO",
                    dpc);

            if (dtDispData.Rows.Count == 0)
            {
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }

            // 取得した内容を表示
            DataRow drDispData = dtDispData.Rows[0];
            this.txtkanjoCd.Text = Util.ToString(drDispData["KANJO_KAMOKU_CD"]);
            this.txtkanjoNm.Text = Util.ToString(drDispData["KANJO_KAMOKU_NM"]);
            this.txtkanjoKanaNm.Text = Util.ToString(drDispData["KANJO_KAMOKU_KANA_NM"]);
            this.txtKamokuBnCd.Text = Util.ToString(drDispData["KAMOKU_BUNRUI_CD"]);
            this.txtKamokuKubn.Text = Util.ToString(drDispData["KAMOKU_KUBUN"]);
            this.txtTaishakuKubn.Text = Util.ToString(drDispData["TAISHAKU_KUBUN"]);
            this.txtKarikataKubn.Text = Util.ToString(drDispData["KARIKATA_ZEI_KUBUN"]);
            this.txtKashikataKubn.Text = Util.ToString(drDispData["KASHIKATA_ZEI_KUBUN"]);
            this.txtBumonKnri.Text = Util.ToString(drDispData["BUMON_UMU"]);
            this.txtHojoKamokuKnri.Text = Util.ToString(drDispData["HOJO_KAMOKU_UMU"]);
            this.txtHojoShiyoKubn.Text = Util.ToString(drDispData["HOJO_SHIYO_KUBUN"]);
            this.txtKojiKnri.Text = Util.ToString(drDispData["KOJI_UMU"]);
            if (Util.ToString(drDispData["SHUKEI_CD"]).Length == 3)
            {
                this.txtShukeihoho.Text = Util.ToString(drDispData["SHUKEI_CD"]).Substring(2, 1);

                if (Util.ToString(drDispData["SHUKEI_CD"]).Substring(1, 1) == " ")
                {
                    this.txtShukeiCd.Text = Util.ToString(drDispData["SHUKEI_CD"]).Substring(0, 1);
                }
                else
                {
                    this.txtShukeiCd.Text = Util.ToString(drDispData["SHUKEI_CD"]).Substring(0, 2);
                }
            }
            else
            {
                this.txtShukeiCd.Text = Util.ToString(drDispData["SHUKEI_CD"]);
            }
            
            this.txtGokeiKeisanKubn.Text = Util.ToString(drDispData["SHUKEI_KEISAN_KUBUN"]);
            this.txtKamokuKubnNm.Text = this.Dba.GetName(this.UInfo, "TB_ZM_F_KAMOKU_KUBUN", this.UInfo.ShishoCd, this.txtKamokuKubn.Text);
            this.lblkari.Text = this.Dba.GetName(this.UInfo, "TB_ZM_F_ZEI_KUBUN", this.UInfo.ShishoCd, txtKarikataKubn.Text);
            this.lblkashi.Text = this.Dba.GetName(this.UInfo, "TB_ZM_F_ZEI_KUBUN", this.UInfo.ShishoCd, txtKashikataKubn.Text);
            this.txtKamokuBnNm.Text = this.Dba.GetName(this.UInfo, "TB_ZM_F_KAMOKU_BUNRUI", this.UInfo.ShishoCd, this.txtKamokuBnCd.Text);
            //　勘定科目コード入力不可　
            this.txtkanjoCd.Enabled = false;
            //this.txtBumonKnri.Enabled = false;
            //this.txtKojiKnri.Enabled = false;

            this.txtHojoKamokuKnri.Enabled = true;
            if (Util.ToInt(drDispData["HOJO_KAMOKU_UMU"]) == 1)
            {
                this.txtHojoShiyoKubn.Enabled = true;
            }
            else
            {
                this.txtHojoShiyoKubn.Enabled = false;
            }

            if (IsCodeUsing())
            {
                // 使用されているので削除不可
                this.btnF3.Enabled = false;
            }
            else
            {
                // 使用されていないので削除OK
                this.btnF3.Enabled = true;
            }

            this._hjkKbn = Util.ToInt(Util.ToString(drDispData["HOJO_SHIYO_KUBUN"]));
            // 実績がある場合
            if (this._swkUmu == 1)
            {
                this.txtKamokuBnCd.Enabled = false;
                this.txtTaishakuKubn.Enabled = false;
                this.txtHojoKamokuKnri.Enabled = false;
                this.txtBumonKnri.Enabled = false;
                this.txtKojiKnri.Enabled = false;
                this.txtHojoShiyoKubn.Enabled = false;
            }
        }
        /// <summary>
        /// 勘定科目コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidkanjoCd()
        {
            // 入力が空の場合にエラー表示
            if (ValChk.IsEmpty(this.txtkanjoCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtkanjoCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 既に存在するコードを入力した場合はエラーとする
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, this.txtkanjoCd.Text);
            StringBuilder where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD");
            DataTable dtkanjo =
                this.Dba.GetDataTableByConditionWithParams("KANJO_KAMOKU_CD",
                    "TB_ZM_KANJO_KAMOKU", Util.ToString(where), dpc);
            if (dtkanjo.Rows.Count > 0)
            {
                Msg.Error("既に存在する勘定科目コードと重複しています。");
                return false;
            }
            return true;
        }

        /// <summary>
        /// 勘定科目名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidkanjoNm()
        {
            // 40バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtkanjoNm.Text, this.txtkanjoNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 勘定科目カナ名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidkanjoKanaNm()
        {
            // 30バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtkanjoKanaNm.Text, this.txtkanjoKanaNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 科目分類コード入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidKamokuBnCd()
        {
            // 入力が空の場合にエラー表示
            if (ValChk.IsEmpty(this.txtKamokuBnCd.Text) || this.txtKamokuBnCd.Text == "0")
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtKamokuBnCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            this.txtKamokuBnNm.Text = this.Dba.GetName(this.UInfo, "TB_ZM_F_KAMOKU_BUNRUI", this.UInfo.ShishoCd, this.txtKamokuBnCd.Text);
            // マスタに無い場合
            if (ValChk.IsEmpty(this.txtKamokuBnNm.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }
        /// <summary>
        /// 科目区分の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidKamokuKubn()
        {
            // 入力が空の場合にエラー表示
            if (ValChk.IsEmpty(this.txtKamokuKubn.Text) || this.txtKamokuKubn.Text == "0")
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtKamokuKubn.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            
            this.txtKamokuKubnNm.Text = this.Dba.GetName(this.UInfo, "TB_ZM_F_KAMOKU_KUBUN", this.UInfo.ShishoCd, this.txtKamokuKubn.Text);
            // マスタに無い場合
            if (ValChk.IsEmpty(this.txtKamokuKubnNm.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 貸借区分の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidTaishakuKubn()
        {
            // 入力が空の場合にエラー表示
            if (ValChk.IsEmpty(this.txtTaishakuKubn.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            if (!(this.txtTaishakuKubn.Text == "1" || this.txtTaishakuKubn.Text == "2"))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtTaishakuKubn.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }
        /// <summary>
        /// 借方税区分の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidKarikataKubn()
        {
            // 入力が空の場合にエラー表示
            if (ValChk.IsEmpty(this.txtKarikataKubn.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtKarikataKubn.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            this.lblkari.Text = this.Dba.GetName(this.UInfo, "TB_ZM_F_ZEI_KUBUN", this.UInfo.ShishoCd, txtKarikataKubn.Text);
            // マスタに無い場合
            if (ValChk.IsEmpty(this.lblkari.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 貸方税区分の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidKashikataKubn()
        {
            // 入力が空の場合にエラー表示
            if (ValChk.IsEmpty(this.txtKashikataKubn.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtKashikataKubn.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            this.lblkashi.Text = this.Dba.GetName(this.UInfo, "TB_ZM_F_ZEI_KUBUN", this.UInfo.ShishoCd, txtKashikataKubn.Text);
            // マスタに無い場合
            if (ValChk.IsEmpty(this.lblkashi.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }
        /// <summary>
        /// 部門管理の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidBumonKnri()
        {
            // 入力が空の場合にエラー表示
            if (ValChk.IsEmpty(this.txtBumonKnri.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            if (!(this.txtBumonKnri.Text == "0" || this.txtBumonKnri.Text == "1"))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtBumonKnri.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }
        /// <summary>
        /// 補助科目管理の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidHojoKamokuKnri(bool allCheck)
        {
            // 入力が空の場合にエラー表示
            if (ValChk.IsEmpty(this.txtHojoKamokuKnri.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            if (!(this.txtHojoKamokuKnri.Text == "0" || this.txtHojoKamokuKnri.Text == "1"))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 補助使用区分入力制御
            if (this.txtHojoKamokuKnri.Text.Equals("1"))
            {
                this.txtHojoShiyoKubn.Enabled = true;
                if (!allCheck) this.txtHojoShiyoKubn.Focus();
            }
            else
            {
                this.txtHojoShiyoKubn.Text = "0";
                this.txtHojoShiyoKubn.Enabled = false;
                if (!allCheck) this.txtShukeiCd.Focus();
            }

            return true;
        }
        /// <summary>
        /// 補助使用区分の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidHojoShiyoKubn()
        {

            // 補助科目管理が0の場合補助使用区分に0を入れる
            if (this.txtHojoKamokuKnri.Text.Equals("0"))
            {
                if (!(this.txtHojoShiyoKubn.Text == "0"))
                {
                    this.txtHojoShiyoKubn.Text = "0";
                    return false;
                }
            }
            // 補助使用区分入力制御
            if (this.txtHojoKamokuKnri.Text.Equals("0"))
            {
                this.txtHojoShiyoKubn.Enabled = false;
            }
            // 入力が空の場合にエラー表示
            if (ValChk.IsEmpty(this.txtHojoShiyoKubn.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 補助科目管理するの場合に補助使用がゼロの場合は入力を促す
            if (this.txtHojoKamokuKnri.Text.Equals("1") && this.txtHojoShiyoKubn.Text == "0")
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            if (!(this.txtHojoShiyoKubn.Text == "0" || this.txtHojoShiyoKubn.Text == "1" ||this.txtHojoShiyoKubn.Text == "2"))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            return true;
        }
        /// <summary>
        /// 工事管理の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidKojiKnri()
        {
            // 入力が空の場合にエラー表示
            if (ValChk.IsEmpty(this.txtKojiKnri.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            if (!(this.txtKojiKnri.Text == "0" || this.txtKojiKnri.Text == "1"))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtKojiKnri.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }
        /// <summary>
        /// 集計コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShukeiCd()
        {
            // 10バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtkanjoNm.Text, this.txtkanjoNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }
        /// <summary>
        /// 合計計算区分の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidGokeiKeisanKubn()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtGokeiKeisanKubn.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }
        /// <summary>
        /// 集計方法の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShukeihoho()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtShukeihoho.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            if (this.txtShukeihoho.Text == "1"||ValChk.IsEmpty(this.txtShukeihoho.Text))
            {
                return true;
            }
            Msg.Error("入力に誤りがあります。");
            return false;

        }
        
        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll(bool allCheck)
        {
            if (MODE_NEW.Equals(this.Par1))
            {
                // 勘定科目コードのチェック
                if (!IsValidkanjoCd())
                {
                    this.txtkanjoCd.Focus();
                    return false;
                }
            }

            // 勘定科目名のチェック
            if (!IsValidkanjoNm())
            {
                this.txtkanjoNm.Focus();
                return false;
            }

            // 勘定科目カナ名のチェック
            if (!IsValidkanjoKanaNm())
            {
                this.txtkanjoKanaNm.Focus();
                return false;
            }

            // 科目分類コードのチェック
            if (!IsValidKamokuBnCd())
            {
                this.txtKamokuBnCd.Focus();
                return false;
            }

            // 科目区分のチェック
            if (!IsValidKamokuKubn())
            {
                this.txtKamokuKubn.Focus();
                return false;
            }

            // 貸借区分のチェック
            if (!IsValidTaishakuKubn())
            {
                this.txtTaishakuKubn.Focus();
                return false;
            }

            // 借方税区分のチェック
            if (!IsValidKarikataKubn())
            {
                this.txtKarikataKubn.Focus();
                return false;
            }

            // 貸方税区分のチェック
            if (!IsValidKashikataKubn())
            {
                this.txtKashikataKubn.Focus();
                return false;
            }

            // 部門管理のチェック
            if (!IsValidBumonKnri())
            {
                this.txtBumonKnri.Focus();
                return false;
            }

            // 補助科目管理のチェック
            if (!IsValidHojoKamokuKnri(allCheck))
            {
                this.txtHojoKamokuKnri.Focus();
                return false;
            }
            //// 工事管理のチェック
            //if (!IsValidKojiKnri())
            //{
            //    this.txtKojiKnri.Focus();
            //    return false;
            //}

            // 補助使用区分のチェック
            if (!IsValidHojoShiyoKubn())
            {
                this.txtHojoShiyoKubn.Focus();
                return false;
            }
            // 集計コードのチェック
            if (!IsValidShukeiCd())
            {
                this.txtShukeiCd.Focus();
                return false;
            }

            // 合計計算区分のチェック
            if (!IsValidGokeiKeisanKubn())
            {
                this.txtGokeiKeisanKubn.Focus();
                return false;
            }

            // 集計方法のチェック
            if (!IsValidShukeihoho())
            {
                this.txtShukeihoho.Focus();
                return false;
            }

            return true;
        }
        /// <summary>
        /// TB_ZM_KANJO_KAMOKUに更新するためのパラメータ設定をします。
        /// </summary>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// 更新処理：DbParamCollection*2(Where句,Set句)
        /// </returns>
        private ArrayList SetZmKanjoParams()
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            if (MODE_NEW.Equals(this.Par1))
            {
                // 会社コードと勘定科目コードと会計年度を更新パラメータに設定
                updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                updParam.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, this.txtkanjoCd.Text);
                updParam.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 6, this.UInfo.KaikeiNendo);
                // 登録日
                updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWTIME");
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 会社コードと勘定科目コードをWhere句のパラメータに設定
                DbParamCollection whereParam = new DbParamCollection();
                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                whereParam.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, this.txtkanjoCd.Text);
                whereParam.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 6, this.UInfo.KaikeiNendo);
                alParams.Add(whereParam);
            }

            // 勘定科目名
            updParam.SetParam("@KANJO_KAMOKU_NM", SqlDbType.VarChar, 40, this.txtkanjoNm.Text);
            // 勘定科目カナ名
            updParam.SetParam("@KANJO_KAMOKU_KANA_NM", SqlDbType.VarChar, 40, this.txtkanjoKanaNm.Text);
            // 科目分類コード
            updParam.SetParam("@KAMOKU_BUNRUI_CD", SqlDbType.VarChar, 40, this.txtKamokuBnCd.Text);
            // 科目区分
            updParam.SetParam("@KAMOKU_KUBUN", SqlDbType.VarChar, 40, this.txtKamokuKubn.Text);
            // 貸借区分
            updParam.SetParam("@TAISHAKU_KUBUN", SqlDbType.VarChar, 40, this.txtTaishakuKubn.Text);
            // 借方税区分
            updParam.SetParam("@KARIKATA_ZEI_KUBUN", SqlDbType.VarChar, 40, this.txtKarikataKubn.Text);
            // 貸方税区分
            updParam.SetParam("@KASHIKATA_ZEI_KUBUN", SqlDbType.VarChar, 40, this.txtKashikataKubn.Text);
            // 部門管理
            updParam.SetParam("@BUMON_UMU", SqlDbType.VarChar, 40, this.txtBumonKnri.Text);
            // 補助科目管理
            updParam.SetParam("@HOJO_KAMOKU_UMU", SqlDbType.VarChar, 40, this.txtHojoKamokuKnri.Text);
            // 補助使用区分
            updParam.SetParam("@HOJO_SHIYO_KUBUN", SqlDbType.VarChar, 40, this.txtHojoShiyoKubn.Text);
            //// 工事管理
            //updParam.SetParam("@KOJI_UMU", SqlDbType.VarChar, 40, this.txtKojiKnri.Text);
            string shukeicd ="";
            if (this.txtShukeiCd.Text.Length != 0)
            {
                shukeicd = this.txtShukeiCd.Text;
            }
            if (this.txtShukeihoho.Text.Length == this.txtShukeihoho.MaxLength)
            {
                if (this.txtShukeiCd.Text.Length == this.txtShukeiCd.MaxLength)
                {
                    shukeicd += this.txtShukeihoho.Text;
                }
                else if (this.txtShukeiCd.Text.Length == 1)
                {
                    shukeicd += " " + this.txtShukeihoho.Text;
                }
                else
                {
                    shukeicd += "  " + this.txtShukeihoho.Text;
                }
            }
            // 集計コード
            updParam.SetParam("@SHUKEI_CD", SqlDbType.VarChar, 40, shukeicd);
            // 合計計算区分
            updParam.SetParam("@SHUKEI_KEISAN_KUBUN", SqlDbType.VarChar, 40, this.txtGokeiKeisanKubn.Text);
            // 仕様未使用
            updParam.SetParam("@SHIYO_MISHIYO", SqlDbType.VarChar, 40, "1");
            // 更新日
            updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWTIME");
            alParams.Add(updParam);

            return alParams;
        }
        /// <summary>
        /// 対象の勘定科目コードがマスタ登録されているかどうかをチェックします。
        /// </summary>
        /// <returns>true:使用されている/false:使用されていない</returns>
        private bool IsCodeExists()
        {
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, this.txtkanjoCd.Text);
            DataTable dtZmShiwakemeisai = this.Dba.GetDataTableByConditionWithParams("*", "TB_ZM_SHIWAKE_MEISAI", "KAISHA_CD = @KAISHA_CD AND KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD", dpc);
            DataTable dtViHojokamoku = this.Dba.GetDataTableByConditionWithParams("*", "VI_ZM_HOJO_KAMOKU", "KAISHA_CD = @KAISHA_CD AND KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD AND HOJO_KAMOKU_CD IS NOT NULL", dpc);

            if (dtZmShiwakemeisai.Rows.Count > 0 || dtViHojokamoku.Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 対象の勘定科目コードがトランテーブルで使用されているかどうかをチェックします。
        /// </summary>
        /// <returns>true:使用されている/false:使用されていない</returns>
        private bool IsCodeUsing()
        {
            // 削除可否チェック
            // 指定した取引先コードが使用されているかどうかをチェック
            // 仕訳明細
            StringBuilder from = new StringBuilder();
            from.Append("TB_ZM_SHIWAKE_MEISAI AS A");
            from.Append(" LEFT OUTER JOIN");
            from.Append(" TB_ZM_KANJO_KAMOKU AS B");
            from.Append(" ON A.KAISHA_CD = B.KAISHA_CD");
            from.Append(" AND A.KAIKEI_NENDO = B.KAIKEI_NENDO");
            from.Append(" AND A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD");

            StringBuilder where = new StringBuilder();
            where.Append("A.KAISHA_CD = @KAISHA_CD");
            where.Append(" AND A.KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD");
            where.Append(" AND A.KAIKEI_NENDO = @KAIKEI_NENDO");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, Util.ToString(this.InData));
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);

            DataTable dtSwkMsi = this.Dba.GetDataTableByConditionWithParams("A.KANJO_KAMOKU_CD", from.ToString(), where.ToString(), dpc);

            // 補助科目
            from = new StringBuilder();
            from.Append("VI_ZM_HOJO_KAMOKU");

            where = new StringBuilder();
            where.Append("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD");
            where.Append(" AND KAIKEI_NENDO = @KAIKEI_NENDO");
            where.Append(" AND HOJO_KAMOKU_CD IS NOT NULL");
            where.Append(" AND (SHISHO_CD = @SHISHO_CD or SHISHO_CD is null)");

            dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.UInfo.ShishoCd);
            dpc.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, Util.ToString(this.InData));
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);

            DataTable dtHojokamk = this.Dba.GetDataTableByConditionWithParams("*", from.ToString(), where.ToString(), dpc);

            this._swkUmu = dtSwkMsi.Rows.Count > 0 ? 1 : 0;
            this._hjkUmu = dtHojokamk.Rows.Count > 0 ? 1 : 0;

            if (dtSwkMsi.Rows.Count > 0 || dtHojokamk.Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion
    }
}
        