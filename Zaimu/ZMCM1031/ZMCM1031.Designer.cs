﻿namespace jp.co.fsi.zm.zmcm1031
{
    partial class ZMCM1031
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblKanaName = new System.Windows.Forms.Label();
            this.txtKanaName = new System.Windows.Forms.TextBox();
            this.dgvList = new System.Windows.Forms.DataGridView();
            this.txtKamokubn = new System.Windows.Forms.TextBox();
            this.lblKamokubn = new System.Windows.Forms.Label();
            this.lblkamokubnrui = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Text = "勘定科目の登録";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            // 
            // lblKanaName
            // 
            this.lblKanaName.BackColor = System.Drawing.Color.Silver;
            this.lblKanaName.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKanaName.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKanaName.Location = new System.Drawing.Point(17, 44);
            this.lblKanaName.Name = "lblKanaName";
            this.lblKanaName.Size = new System.Drawing.Size(267, 25);
            this.lblKanaName.TabIndex = 1;
            this.lblKanaName.Text = "カ　ナ　名";
            this.lblKanaName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKanaName
            // 
            this.txtKanaName.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKanaName.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtKanaName.Location = new System.Drawing.Point(101, 46);
            this.txtKanaName.MaxLength = 30;
            this.txtKanaName.Name = "txtKanaName";
            this.txtKanaName.Size = new System.Drawing.Size(180, 20);
            this.txtKanaName.TabIndex = 2;
            this.txtKanaName.Validating += new System.ComponentModel.CancelEventHandler(this.txtKanaName_Validating);
            // 
            // dgvList
            // 
            this.dgvList.AllowUserToAddRows = false;
            this.dgvList.AllowUserToDeleteRows = false;
            this.dgvList.AllowUserToResizeColumns = false;
            this.dgvList.AllowUserToResizeRows = false;
            this.dgvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvList.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.dgvList.Location = new System.Drawing.Point(17, 74);
            this.dgvList.MultiSelect = false;
            this.dgvList.Name = "dgvList";
            this.dgvList.ReadOnly = true;
            this.dgvList.RowHeadersVisible = false;
            this.dgvList.RowTemplate.Height = 21;
            this.dgvList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvList.Size = new System.Drawing.Size(794, 359);
            this.dgvList.TabIndex = 3;
            this.dgvList.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvList_CellDoubleClick);
            this.dgvList.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvList_KeyDown);
            // 
            // txtKamokubn
            // 
            this.txtKamokubn.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKamokubn.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtKamokubn.Location = new System.Drawing.Point(372, 46);
            this.txtKamokubn.MaxLength = 5;
            this.txtKamokubn.Name = "txtKamokubn";
            this.txtKamokubn.Size = new System.Drawing.Size(78, 20);
            this.txtKamokubn.TabIndex = 903;
            this.txtKamokubn.Validating += new System.ComponentModel.CancelEventHandler(this.txtKamokubn_Validating);
            // 
            // lblKamokubn
            // 
            this.lblKamokubn.BackColor = System.Drawing.Color.Silver;
            this.lblKamokubn.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKamokubn.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKamokubn.Location = new System.Drawing.Point(287, 44);
            this.lblKamokubn.Name = "lblKamokubn";
            this.lblKamokubn.Size = new System.Drawing.Size(166, 25);
            this.lblKamokubn.TabIndex = 902;
            this.lblKamokubn.Text = "科 目 分 類";
            this.lblKamokubn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblkamokubnrui
            // 
            this.lblkamokubnrui.BackColor = System.Drawing.Color.Silver;
            this.lblkamokubnrui.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblkamokubnrui.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblkamokubnrui.Location = new System.Drawing.Point(451, 44);
            this.lblkamokubnrui.Name = "lblkamokubnrui";
            this.lblkamokubnrui.Size = new System.Drawing.Size(223, 25);
            this.lblkamokubnrui.TabIndex = 904;
            this.lblkamokubnrui.Text = "全て";
            this.lblkamokubnrui.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ZMCM1031
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.lblkamokubnrui);
            this.Controls.Add(this.txtKamokubn);
            this.Controls.Add(this.lblKamokubn);
            this.Controls.Add(this.dgvList);
            this.Controls.Add(this.txtKanaName);
            this.Controls.Add(this.lblKanaName);
            this.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.Name = "ZMCM1031";
            this.Text = "勘定科目の登録";
            this.Shown += new System.EventHandler(this.frm_Shown);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.lblKanaName, 0);
            this.Controls.SetChildIndex(this.txtKanaName, 0);
            this.Controls.SetChildIndex(this.dgvList, 0);
            this.Controls.SetChildIndex(this.lblKamokubn, 0);
            this.Controls.SetChildIndex(this.txtKamokubn, 0);
            this.Controls.SetChildIndex(this.lblkamokubnrui, 0);
            this.pnlDebug.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblKanaName;
        private System.Windows.Forms.TextBox txtKanaName;
        private System.Windows.Forms.DataGridView dgvList;
        private System.Windows.Forms.TextBox txtKamokubn;
        private System.Windows.Forms.Label lblKamokubn;
        private System.Windows.Forms.Label lblkamokubnrui;

    }
}