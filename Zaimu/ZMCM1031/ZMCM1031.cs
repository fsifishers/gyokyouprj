﻿using System.ComponentModel;
using System;
using System.Data;
using System.Drawing;
using System.Text;
using System.Reflection;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.zm.zmcm1031
{
    /// <summary>
    /// 勘定科目の登録(ZMCM1031)
    /// </summary>
    public partial class ZMCM1031 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(コード検索)
        /// </summary>
        private const string MODE_CD_SRC = "1";
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ZMCM1031()
        {
            InitializeComponent();
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.InitForm();は呼び出さなくて構いません。
        /// また、このメソッド内の処理を外出しでこのクラス内にメソッド化するのは構いませんが、
        /// 原則、独自で起動時のイベント処理を実装することは禁じます。
        /// </remarks>
        protected override void InitForm()
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                // Par1が"1"の場合、コード検索画面としての挙動をする
                // EscapeとF1のみ表示
                this.ShowFButton = true;
                this.btnEsc.Location = this.btnF1.Location;
                this.btnF1.Location = this.btnF2.Location;
                this.btnF2.Visible = false;
                this.btnF3.Visible = false;
                this.btnF4.Visible = false;
                this.btnF5.Visible = false;
                this.btnF6.Visible = false;
                this.btnF7.Visible = false;
                this.btnF8.Visible = false;
                this.btnF9.Visible = false;
                this.btnF10.Visible = false;
                this.btnF11.Visible = false;
                this.btnF12.Visible = false;
            }

            // まずデータが存在し得ない検索条件で検索をし、結果をバインドすることで、
            // 初期状態を作り出す
            SearchData(true);

            // カナ名にフォーカス
            this.txtKanaName.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtKanaName.Focus();
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                // Par1が"1"の場合、ダイアログとしての処理結果を返却する
                this.DialogResult = DialogResult.Cancel;
            }
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            Assembly asm;
            Type t;

            switch (this.ActiveCtlNm)
            {
                case "txtKamokubn":
                    #region 科目分類
                    // アセンブリのロード
                    //asm = System.Reflection.Assembly.LoadFrom("COMC8011.exe");
                    asm = Assembly.LoadFrom("CMCM1041.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.com.comc8011.COMC8011");
                    t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "TB_ZM_F_KAMOKU_BUNRUI";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.txtKamokubn.Text = result[0];
                                this.lblkamokubnrui.Text = result[1];
                            }
                        }
                    }
                    #endregion
                    break;

                default:
                    break;
            }
        
            // カナ名にフォーカスを戻す
            this.txtKanaName.Focus();
            this.txtKanaName.SelectAll();
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF4();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF4()
        {
            // メンテ機能で立ち上げている場合のみ勘定科目登録画面を立ち上げる
            if (ValChk.IsEmpty(this.Par1))
            {
                // 勘定科目登録画面の起動
                EditKanjo(string.Empty);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF5();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF5()
        {
            // 勘定科目の印刷画面を起動
            ZMCM1035 frm = new ZMCM1035();
            DialogResult result = frm.ShowDialog(this);
        }
        #endregion

        #region イベント
        /// <summary>
        /// カナ名検証時の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKanaName_Validating(object sender, CancelEventArgs e)
        {
            //TODO:何かチェックが必要なのかもしれない
                // 入力された情報を元に検索する
                SearchData(false);
        }
        /// <summary>
        /// 科目分類検証時の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKamokubn_Validating(object sender, CancelEventArgs e)
        {
            ////TODO:何かチェックが必要なのかもしれない
            //    // 入力された情報を元に検索する
            //    SearchData(false);
            if (!IsValidKamokuBnCd())
            {
                e.Cancel = true;
                this.txtKamokubn.SelectAll();
            }
            else
            {
                // 入力された情報を元に検索する
                SearchData(false);
            }
        }

        /// <summary>
        /// グリッドでのキーダウン処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (MODE_CD_SRC.Equals(this.Par1))
                {
                    ReturnVal();
                }
                else
                {
                    try
                    {
                        EditKanjo(Util.ToString(this.dgvList.SelectedRows[0].Cells["コード"].Value));
                        e.Handled = true;
                    }
                    catch (Exception) { }
                }
            }
        }

        /// <summary>
        /// グリッドのセルダブルクリック時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (MODE_CD_SRC.Equals(this.Par1))
            {
                ReturnVal();
            }
            else
            {
                EditKanjo(Util.ToString(this.dgvList.SelectedRows[0].Cells["コード"].Value));
            }
        }

        /// <summary>
        /// フォーム表示後の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frm_Shown(object sender, EventArgs e)
        {
            if (this.dgvList.RowCount == 0)
            {
                if (Msg.ConfYesNo( "該当データがありません、登録しますか？") == DialogResult.Yes)
                {
                    this.PressF4();
                }
            }
            else
            {
                ActiveControl = this.dgvList;
                this.dgvList.Rows[0].Selected = true;
                this.dgvList.CurrentCell = this.dgvList[0, 0];
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 科目分類コード入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidKamokuBnCd()
        {
            // 入力が空の場合にエラー表示
            if (ValChk.IsEmpty(this.txtKamokubn.Text) || this.txtKamokubn.Text == "0")
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtKamokubn.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            this.lblkamokubnrui.Text = this.Dba.GetName(this.UInfo, "TB_ZM_F_KAMOKU_BUNRUI", this.UInfo.ShishoCd, this.txtKamokubn.Text);
            // マスタに無い場合
            if (ValChk.IsEmpty(this.lblkamokubnrui.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// データを検索する
        /// </summary>
        /// <param name="isInitial">初期処理であるかどうか</param>
        private void SearchData(bool isInitial)
        {
            // 勘定科目ビューからデータを取得して表示
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 6, this.UInfo.KaikeiNendo);

            StringBuilder where = new StringBuilder("ZM.KAISHA_CD = @KAISHA_CD");
            where.Append(" AND ZM.KAIKEI_NENDO = @KAIKEI_NENDO");
            if (!isInitial)
            {
                // 初期処理でない場合、入力されたカナ名から検索する
                if (!ValChk.IsEmpty(this.txtKanaName.Text))
                {
                    where.Append(" AND ZM.KANJO_KAMOKU_KANA_NM LIKE @KANJO_KAMOKU_KANA_NM");
                    
                    // NOTICE!:部分一致検索をする場合、"%"の分桁数に2バイトを足すこと
                    dpc.SetParam("@KANJO_KAMOKU_KANA_NM", SqlDbType.VarChar, 32, "%" + this.txtKanaName.Text + "%");
                }
                if (txtKamokubn.Text != "0" && !ValChk.IsEmpty(txtKamokubn.Text))
                {
                    where.Append(" AND ZM.KAMOKU_BUNRUI_CD = @KAMOKU_BUNRUI_CD");
                    dpc.SetParam("@KAMOKU_BUNRUI_CD", SqlDbType.Decimal, 7, Util.ToDecimal(this.txtKamokubn.Text));
                    this.lblkamokubnrui.Text = this.Dba.GetName(this.UInfo, "TB_ZM_F_KAMOKU_BUNRUI", this.UInfo.ShishoCd, this.txtKamokubn.Text);
                }
                else this.lblkamokubnrui.Text = "全て";
            }
            string cols = "ZM.KANJO_KAMOKU_CD AS コード";
            cols += ", ZM.KANJO_KAMOKU_NM AS 勘定科目名";
            cols += ", ZM.KANJO_KAMOKU_KANA_NM AS カナ名";
            cols += ", ZM.KAMOKU_BUNRUI_NM AS 科目分類";
            cols += ", ZM.TAISHAKU_KUBUN_NM AS 貸借区分";
            cols += ", ZM.KARIKATA_ZEI_KUBUN AS 借方税区分";
            cols += ", ZM.KASHIKATA_ZEI_KUBUN AS 貸方税区分";

            string from = "VI_ZM_KANJO_KAMOKU AS ZM";

            DataTable dtkanjo =
                this.Dba.GetDataTableByConditionWithParams(cols, from,
                    Util.ToString(where), "ZM.KANJO_KAMOKU_CD", dpc);

            
                
            // 初期処理以外の場合、該当データがなければエラーメッセージを表示
            if (dtkanjo.Rows.Count == 0)
            {
                if (!isInitial)
                {
                    Msg.Info("該当データがありません。");
                }

                //dtkanjo.Rows.Add(dtkanjo.NewRow());
            }

            this.dgvList.Enabled = true;
            this.dgvList.DataSource = dtkanjo;

            if (dtkanjo.Rows.Count == 0)
                this.dgvList.Enabled = false;

            // ユーザーによるソートを禁止させる
            foreach (DataGridViewColumn c in this.dgvList.Columns)
                c.SortMode = DataGridViewColumnSortMode.NotSortable;

            // フォントを設定する
            //this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F, FontStyle.Bold);
            this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F, FontStyle.Regular);
            this.dgvList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            this.dgvList.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F);

            // 列幅を設定する
            this.dgvList.Columns[0].Width = 60;
            this.dgvList.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvList.Columns[1].Width = 180;
            this.dgvList.Columns[2].Width = 150;
            this.dgvList.Columns[3].Width = 124;
            this.dgvList.Columns[4].Width = 80;
            this.dgvList.Columns[5].Width = 90;
            this.dgvList.Columns[6].Width = 90;
        }

        /// <summary>
        /// 勘定科目を追加編集する
        /// </summary>
        /// <param name="code">勘定科目コード(空：新規登録、以外：編集)</param>
        private void EditKanjo(string code)
        {
            ZMCM1032 frm;

            if (ValChk.IsEmpty(code))
            {
                // 新規登録モードで登録画面を起動
                frm = new ZMCM1032("1");
            }
            else
            {
                // 編集モードで登録画面を起動
                frm = new ZMCM1032("2");
                frm.InData = code;
            }

            DialogResult result = frm.ShowDialog(this);

            if (result == DialogResult.OK)
            {
                // データを再検索する
                SearchData(false);
                // 元々選択していたデータを選択
                for (int i = 0; i < this.dgvList.Rows.Count; i++)
                {
                    if (code.Equals(Util.ToString(this.dgvList.Rows[i].Cells["コード"].Value)))
                    {
                        this.dgvList.FirstDisplayedScrollingRowIndex = i;
                        this.dgvList.Rows[i].Selected = true;
                        break;
                    }
                }
                // Gridに再度フォーカスをセット
                this.ActiveControl = this.dgvList;
                this.dgvList.Focus();
            }
        }

        /// <summary>
        /// 呼び出し元に戻り値を返す
        /// </summary>
        private void ReturnVal()
        {
            this.OutData = new string[7] { 
                Util.ToString(this.dgvList.SelectedRows[0].Cells["コード"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["勘定科目名"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["カナ名"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["科目分類"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["貸借区分"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["借方税区分"].Value),
                Util.ToString(this.dgvList.SelectedRows[0].Cells["貸方税区分"].Value)
            };
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion
    }
}
