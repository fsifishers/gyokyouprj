﻿namespace jp.co.fsi.zm.zmdr1011
{
    partial class ZMDR1011
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblFr = new System.Windows.Forms.Label();
            this.txtMonthFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtYearFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDayFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblYearFr = new System.Windows.Forms.Label();
            this.lblMonthFr = new System.Windows.Forms.Label();
            this.lblDayFr = new System.Windows.Forms.Label();
            this.gbxDate = new System.Windows.Forms.GroupBox();
            this.lblGengoTo = new System.Windows.Forms.Label();
            this.lblCodeBetDate = new System.Windows.Forms.Label();
            this.lblGengoFr = new System.Windows.Forms.Label();
            this.lblDayTo = new System.Windows.Forms.Label();
            this.lblMonthTo = new System.Windows.Forms.Label();
            this.lblYearTo = new System.Windows.Forms.Label();
            this.txtDayTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtYearTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtMonthTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblTo = new System.Windows.Forms.Label();
            this.gbxShohizeiShori = new System.Windows.Forms.GroupBox();
            this.rdoZeikomi = new System.Windows.Forms.RadioButton();
            this.rdoZeinuki = new System.Windows.Forms.RadioButton();
            this.gbxMizuageShisho = new System.Windows.Forms.GroupBox();
            this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMizuageShishoNm = new System.Windows.Forms.Label();
            this.lblMizuageShisho = new System.Windows.Forms.Label();
            this.gbxShiwakeShurui = new System.Windows.Forms.GroupBox();
            this.rdoZenShiwake = new System.Windows.Forms.RadioButton();
            this.rdoTujoShiwake = new System.Windows.Forms.RadioButton();
            this.rdoKessanShiwake = new System.Windows.Forms.RadioButton();
            this.pnlDebug.SuspendLayout();
            this.gbxDate.SuspendLayout();
            this.gbxShohizeiShori.SuspendLayout();
            this.gbxMizuageShisho.SuspendLayout();
            this.gbxShiwakeShurui.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Text = "";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            // 
            // lblFr
            // 
            this.lblFr.BackColor = System.Drawing.Color.Silver;
            this.lblFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFr.Location = new System.Drawing.Point(26, 28);
            this.lblFr.Name = "lblFr";
            this.lblFr.Size = new System.Drawing.Size(199, 29);
            this.lblFr.TabIndex = 2;
            // 
            // txtMonthFr
            // 
            this.txtMonthFr.AutoSizeFromLength = false;
            this.txtMonthFr.DisplayLength = null;
            this.txtMonthFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMonthFr.Location = new System.Drawing.Point(121, 32);
            this.txtMonthFr.MaxLength = 2;
            this.txtMonthFr.Name = "txtMonthFr";
            this.txtMonthFr.Size = new System.Drawing.Size(30, 20);
            this.txtMonthFr.TabIndex = 3;
            this.txtMonthFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonthFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthFr_Validating);
            // 
            // txtYearFr
            // 
            this.txtYearFr.AutoSizeFromLength = false;
            this.txtYearFr.DisplayLength = null;
            this.txtYearFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtYearFr.Location = new System.Drawing.Point(71, 32);
            this.txtYearFr.MaxLength = 2;
            this.txtYearFr.Name = "txtYearFr";
            this.txtYearFr.Size = new System.Drawing.Size(30, 20);
            this.txtYearFr.TabIndex = 1;
            this.txtYearFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtYearFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtYearFr_Validating);
            // 
            // txtDayFr
            // 
            this.txtDayFr.AutoSizeFromLength = false;
            this.txtDayFr.DisplayLength = null;
            this.txtDayFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDayFr.Location = new System.Drawing.Point(171, 32);
            this.txtDayFr.MaxLength = 2;
            this.txtDayFr.Name = "txtDayFr";
            this.txtDayFr.Size = new System.Drawing.Size(30, 20);
            this.txtDayFr.TabIndex = 5;
            this.txtDayFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDayFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDayFr_Validating);
            // 
            // lblYearFr
            // 
            this.lblYearFr.BackColor = System.Drawing.Color.Silver;
            this.lblYearFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYearFr.Location = new System.Drawing.Point(103, 32);
            this.lblYearFr.Name = "lblYearFr";
            this.lblYearFr.Size = new System.Drawing.Size(17, 21);
            this.lblYearFr.TabIndex = 2;
            this.lblYearFr.Text = "年";
            this.lblYearFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMonthFr
            // 
            this.lblMonthFr.BackColor = System.Drawing.Color.Silver;
            this.lblMonthFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMonthFr.Location = new System.Drawing.Point(153, 32);
            this.lblMonthFr.Name = "lblMonthFr";
            this.lblMonthFr.Size = new System.Drawing.Size(15, 19);
            this.lblMonthFr.TabIndex = 4;
            this.lblMonthFr.Text = "月";
            this.lblMonthFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDayFr
            // 
            this.lblDayFr.BackColor = System.Drawing.Color.Silver;
            this.lblDayFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDayFr.Location = new System.Drawing.Point(203, 32);
            this.lblDayFr.Name = "lblDayFr";
            this.lblDayFr.Size = new System.Drawing.Size(20, 18);
            this.lblDayFr.TabIndex = 6;
            this.lblDayFr.Text = "日";
            this.lblDayFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gbxDate
            // 
            this.gbxDate.Controls.Add(this.lblGengoTo);
            this.gbxDate.Controls.Add(this.lblCodeBetDate);
            this.gbxDate.Controls.Add(this.lblGengoFr);
            this.gbxDate.Controls.Add(this.lblDayTo);
            this.gbxDate.Controls.Add(this.lblMonthTo);
            this.gbxDate.Controls.Add(this.lblYearTo);
            this.gbxDate.Controls.Add(this.txtDayTo);
            this.gbxDate.Controls.Add(this.txtYearTo);
            this.gbxDate.Controls.Add(this.txtMonthTo);
            this.gbxDate.Controls.Add(this.lblTo);
            this.gbxDate.Controls.Add(this.lblDayFr);
            this.gbxDate.Controls.Add(this.lblMonthFr);
            this.gbxDate.Controls.Add(this.lblYearFr);
            this.gbxDate.Controls.Add(this.txtDayFr);
            this.gbxDate.Controls.Add(this.txtYearFr);
            this.gbxDate.Controls.Add(this.txtMonthFr);
            this.gbxDate.Controls.Add(this.lblFr);
            this.gbxDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxDate.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gbxDate.Location = new System.Drawing.Point(12, 140);
            this.gbxDate.Name = "gbxDate";
            this.gbxDate.Size = new System.Drawing.Size(477, 77);
            this.gbxDate.TabIndex = 2;
            this.gbxDate.TabStop = false;
            this.gbxDate.Text = "日付";
            // 
            // lblGengoTo
            // 
            this.lblGengoTo.BackColor = System.Drawing.Color.Silver;
            this.lblGengoTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengoTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengoTo.ForeColor = System.Drawing.Color.Black;
            this.lblGengoTo.Location = new System.Drawing.Point(258, 32);
            this.lblGengoTo.Name = "lblGengoTo";
            this.lblGengoTo.Size = new System.Drawing.Size(41, 21);
            this.lblGengoTo.TabIndex = 8;
            this.lblGengoTo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCodeBetDate
            // 
            this.lblCodeBetDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCodeBetDate.Location = new System.Drawing.Point(229, 32);
            this.lblCodeBetDate.Name = "lblCodeBetDate";
            this.lblCodeBetDate.Size = new System.Drawing.Size(17, 22);
            this.lblCodeBetDate.TabIndex = 7;
            this.lblCodeBetDate.Text = "～";
            this.lblCodeBetDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblGengoFr
            // 
            this.lblGengoFr.BackColor = System.Drawing.Color.Silver;
            this.lblGengoFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengoFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengoFr.ForeColor = System.Drawing.Color.Black;
            this.lblGengoFr.Location = new System.Drawing.Point(29, 32);
            this.lblGengoFr.Name = "lblGengoFr";
            this.lblGengoFr.Size = new System.Drawing.Size(41, 21);
            this.lblGengoFr.TabIndex = 0;
            this.lblGengoFr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDayTo
            // 
            this.lblDayTo.BackColor = System.Drawing.Color.Silver;
            this.lblDayTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDayTo.Location = new System.Drawing.Point(432, 32);
            this.lblDayTo.Name = "lblDayTo";
            this.lblDayTo.Size = new System.Drawing.Size(20, 18);
            this.lblDayTo.TabIndex = 14;
            this.lblDayTo.Text = "日";
            this.lblDayTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMonthTo
            // 
            this.lblMonthTo.BackColor = System.Drawing.Color.Silver;
            this.lblMonthTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMonthTo.Location = new System.Drawing.Point(382, 32);
            this.lblMonthTo.Name = "lblMonthTo";
            this.lblMonthTo.Size = new System.Drawing.Size(15, 19);
            this.lblMonthTo.TabIndex = 12;
            this.lblMonthTo.Text = "月";
            this.lblMonthTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblYearTo
            // 
            this.lblYearTo.BackColor = System.Drawing.Color.Silver;
            this.lblYearTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYearTo.Location = new System.Drawing.Point(332, 32);
            this.lblYearTo.Name = "lblYearTo";
            this.lblYearTo.Size = new System.Drawing.Size(17, 21);
            this.lblYearTo.TabIndex = 10;
            this.lblYearTo.Text = "年";
            this.lblYearTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDayTo
            // 
            this.txtDayTo.AutoSizeFromLength = false;
            this.txtDayTo.DisplayLength = null;
            this.txtDayTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDayTo.Location = new System.Drawing.Point(400, 32);
            this.txtDayTo.MaxLength = 2;
            this.txtDayTo.Name = "txtDayTo";
            this.txtDayTo.Size = new System.Drawing.Size(30, 20);
            this.txtDayTo.TabIndex = 13;
            this.txtDayTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDayTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDayTo_Validating);
            // 
            // txtYearTo
            // 
            this.txtYearTo.AutoSizeFromLength = false;
            this.txtYearTo.DisplayLength = null;
            this.txtYearTo.Enabled = false;
            this.txtYearTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtYearTo.Location = new System.Drawing.Point(300, 32);
            this.txtYearTo.MaxLength = 2;
            this.txtYearTo.Name = "txtYearTo";
            this.txtYearTo.Size = new System.Drawing.Size(30, 20);
            this.txtYearTo.TabIndex = 9;
            this.txtYearTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtMonthTo
            // 
            this.txtMonthTo.AutoSizeFromLength = false;
            this.txtMonthTo.DisplayLength = null;
            this.txtMonthTo.Enabled = false;
            this.txtMonthTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMonthTo.Location = new System.Drawing.Point(350, 32);
            this.txtMonthTo.MaxLength = 2;
            this.txtMonthTo.Name = "txtMonthTo";
            this.txtMonthTo.Size = new System.Drawing.Size(30, 20);
            this.txtMonthTo.TabIndex = 11;
            this.txtMonthTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblTo
            // 
            this.lblTo.BackColor = System.Drawing.Color.Silver;
            this.lblTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTo.Location = new System.Drawing.Point(255, 28);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(199, 29);
            this.lblTo.TabIndex = 11;
            // 
            // gbxShohizeiShori
            // 
            this.gbxShohizeiShori.Controls.Add(this.rdoZeikomi);
            this.gbxShohizeiShori.Controls.Add(this.rdoZeinuki);
            this.gbxShohizeiShori.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxShohizeiShori.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gbxShohizeiShori.Location = new System.Drawing.Point(300, 231);
            this.gbxShohizeiShori.Name = "gbxShohizeiShori";
            this.gbxShohizeiShori.Size = new System.Drawing.Size(201, 59);
            this.gbxShohizeiShori.TabIndex = 4;
            this.gbxShohizeiShori.TabStop = false;
            this.gbxShohizeiShori.Text = "消費税処理";
            // 
            // rdoZeikomi
            // 
            this.rdoZeikomi.AutoSize = true;
            this.rdoZeikomi.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoZeikomi.Location = new System.Drawing.Point(20, 25);
            this.rdoZeikomi.Name = "rdoZeikomi";
            this.rdoZeikomi.Size = new System.Drawing.Size(53, 17);
            this.rdoZeikomi.TabIndex = 0;
            this.rdoZeikomi.TabStop = true;
            this.rdoZeikomi.Text = "税込";
            this.rdoZeikomi.UseVisualStyleBackColor = true;
            this.rdoZeikomi.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rdoZeikomi_KeyDown);
            // 
            // rdoZeinuki
            // 
            this.rdoZeinuki.AutoSize = true;
            this.rdoZeinuki.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoZeinuki.Location = new System.Drawing.Point(113, 25);
            this.rdoZeinuki.Name = "rdoZeinuki";
            this.rdoZeinuki.Size = new System.Drawing.Size(53, 17);
            this.rdoZeinuki.TabIndex = 1;
            this.rdoZeinuki.TabStop = true;
            this.rdoZeinuki.Text = "税抜";
            this.rdoZeinuki.UseVisualStyleBackColor = true;
            this.rdoZeinuki.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rdoZeinuki_KeyDown);
            // 
            // gbxMizuageShisho
            // 
            this.gbxMizuageShisho.Controls.Add(this.txtMizuageShishoCd);
            this.gbxMizuageShisho.Controls.Add(this.lblMizuageShishoNm);
            this.gbxMizuageShisho.Controls.Add(this.lblMizuageShisho);
            this.gbxMizuageShisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.gbxMizuageShisho.ForeColor = System.Drawing.Color.Black;
            this.gbxMizuageShisho.Location = new System.Drawing.Point(12, 49);
            this.gbxMizuageShisho.Name = "gbxMizuageShisho";
            this.gbxMizuageShisho.Size = new System.Drawing.Size(404, 77);
            this.gbxMizuageShisho.TabIndex = 1;
            this.gbxMizuageShisho.TabStop = false;
            this.gbxMizuageShisho.Text = "支所";
            // 
            // txtMizuageShishoCd
            // 
            this.txtMizuageShishoCd.AutoSizeFromLength = true;
            this.txtMizuageShishoCd.DisplayLength = null;
            this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtMizuageShishoCd.Location = new System.Drawing.Point(71, 33);
            this.txtMizuageShishoCd.MaxLength = 4;
            this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
            this.txtMizuageShishoCd.Size = new System.Drawing.Size(34, 20);
            this.txtMizuageShishoCd.TabIndex = 1;
            this.txtMizuageShishoCd.TabStop = false;
            this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
            // 
            // lblMizuageShishoNm
            // 
            this.lblMizuageShishoNm.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMizuageShishoNm.Location = new System.Drawing.Point(107, 33);
            this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
            this.lblMizuageShishoNm.Size = new System.Drawing.Size(270, 20);
            this.lblMizuageShishoNm.TabIndex = 2;
            this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMizuageShisho
            // 
            this.lblMizuageShisho.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblMizuageShisho.Location = new System.Drawing.Point(27, 31);
            this.lblMizuageShisho.Name = "lblMizuageShisho";
            this.lblMizuageShisho.Size = new System.Drawing.Size(354, 25);
            this.lblMizuageShisho.TabIndex = 0;
            this.lblMizuageShisho.Text = "支所";
            this.lblMizuageShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gbxShiwakeShurui
            // 
            this.gbxShiwakeShurui.Controls.Add(this.rdoZenShiwake);
            this.gbxShiwakeShurui.Controls.Add(this.rdoTujoShiwake);
            this.gbxShiwakeShurui.Controls.Add(this.rdoKessanShiwake);
            this.gbxShiwakeShurui.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxShiwakeShurui.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gbxShiwakeShurui.Location = new System.Drawing.Point(12, 231);
            this.gbxShiwakeShurui.Name = "gbxShiwakeShurui";
            this.gbxShiwakeShurui.Size = new System.Drawing.Size(282, 59);
            this.gbxShiwakeShurui.TabIndex = 3;
            this.gbxShiwakeShurui.TabStop = false;
            this.gbxShiwakeShurui.Text = "仕訳種類";
            // 
            // rdoZenShiwake
            // 
            this.rdoZenShiwake.AutoSize = true;
            this.rdoZenShiwake.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoZenShiwake.Location = new System.Drawing.Point(192, 25);
            this.rdoZenShiwake.Name = "rdoZenShiwake";
            this.rdoZenShiwake.Size = new System.Drawing.Size(67, 17);
            this.rdoZenShiwake.TabIndex = 2;
            this.rdoZenShiwake.TabStop = true;
            this.rdoZenShiwake.Text = "全仕訳";
            this.rdoZenShiwake.UseVisualStyleBackColor = true;
            // 
            // rdoTujoShiwake
            // 
            this.rdoTujoShiwake.AutoSize = true;
            this.rdoTujoShiwake.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoTujoShiwake.Location = new System.Drawing.Point(15, 25);
            this.rdoTujoShiwake.Name = "rdoTujoShiwake";
            this.rdoTujoShiwake.Size = new System.Drawing.Size(81, 17);
            this.rdoTujoShiwake.TabIndex = 0;
            this.rdoTujoShiwake.TabStop = true;
            this.rdoTujoShiwake.Text = "通常仕訳";
            this.rdoTujoShiwake.UseVisualStyleBackColor = true;
            // 
            // rdoKessanShiwake
            // 
            this.rdoKessanShiwake.AutoSize = true;
            this.rdoKessanShiwake.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoKessanShiwake.Location = new System.Drawing.Point(102, 25);
            this.rdoKessanShiwake.Name = "rdoKessanShiwake";
            this.rdoKessanShiwake.Size = new System.Drawing.Size(81, 17);
            this.rdoKessanShiwake.TabIndex = 1;
            this.rdoKessanShiwake.TabStop = true;
            this.rdoKessanShiwake.Text = "決算仕訳";
            this.rdoKessanShiwake.UseVisualStyleBackColor = true;
            // 
            // ZMDR1011
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.gbxShiwakeShurui);
            this.Controls.Add(this.gbxMizuageShisho);
            this.Controls.Add(this.gbxShohizeiShori);
            this.Controls.Add(this.gbxDate);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "ZMDR1011";
            this.Text = "";
            this.Controls.SetChildIndex(this.gbxDate, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.gbxShohizeiShori, 0);
            this.Controls.SetChildIndex(this.gbxMizuageShisho, 0);
            this.Controls.SetChildIndex(this.gbxShiwakeShurui, 0);
            this.pnlDebug.ResumeLayout(false);
            this.gbxDate.ResumeLayout(false);
            this.gbxDate.PerformLayout();
            this.gbxShohizeiShori.ResumeLayout(false);
            this.gbxShohizeiShori.PerformLayout();
            this.gbxMizuageShisho.ResumeLayout(false);
            this.gbxMizuageShisho.PerformLayout();
            this.gbxShiwakeShurui.ResumeLayout(false);
            this.gbxShiwakeShurui.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblFr;
        private jp.co.fsi.common.controls.FsiTextBox txtMonthFr;
        private jp.co.fsi.common.controls.FsiTextBox txtYearFr;
        private jp.co.fsi.common.controls.FsiTextBox txtDayFr;
        private System.Windows.Forms.Label lblYearFr;
        private System.Windows.Forms.Label lblMonthFr;
        private System.Windows.Forms.Label lblDayFr;
        private System.Windows.Forms.GroupBox gbxDate;
        private System.Windows.Forms.Label lblCodeBetDate;
        private System.Windows.Forms.Label lblDayTo;
        private System.Windows.Forms.Label lblMonthTo;
        private System.Windows.Forms.Label lblYearTo;
        private jp.co.fsi.common.controls.FsiTextBox txtDayTo;
        private jp.co.fsi.common.controls.FsiTextBox txtYearTo;
        private jp.co.fsi.common.controls.FsiTextBox txtMonthTo;
        private System.Windows.Forms.Label lblTo;
        private System.Windows.Forms.GroupBox gbxShohizeiShori;
        private System.Windows.Forms.RadioButton rdoZeikomi;
        private System.Windows.Forms.RadioButton rdoZeinuki;
        private System.Windows.Forms.Label lblGengoTo;
        private System.Windows.Forms.Label lblGengoFr;
        private System.Windows.Forms.GroupBox gbxMizuageShisho;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.Label lblMizuageShisho;
        private System.Windows.Forms.GroupBox gbxShiwakeShurui;
        private System.Windows.Forms.RadioButton rdoZenShiwake;
        private System.Windows.Forms.RadioButton rdoTujoShiwake;
        private System.Windows.Forms.RadioButton rdoKessanShiwake;
    }
}