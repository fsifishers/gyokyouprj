﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Reflection;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;
using jp.co.fsi.common.constants;

namespace jp.co.fsi.zm.zmde1011
{
    /// <summary>
    /// 入力範囲指定(ZMDE1012)
    /// </summary>
    public partial class ZMDE1012 : BasePgForm
    {
        #region 変数
        private bool _isInitial;                    // 初回起動判定
        // 入力範囲指定
        private string _tantoshaCd;                 // 担当者コード
        private string _kanjoKamokuCd;              // 勘定科目コード
        private string _hojoKamokuCd;               // 補助科目コード
        private string _bumonCd;                    // 部門コード
        private DateTime _denpyoDateFr;             // 伝票日付(自)
        private DateTime _denpyoDateTo;             // 伝票日付(至)
        private bool _isPressF10 = false;           // F10押下フラグ

        private int _shishoCode;                    // 支所コード
        private string _genkin;                     // 科目分類（現金）11131
        private string _yokin;                      // 科目分類（預け金）11132
        private bool _dtFlg = new bool();
        #endregion

        #region プロパティ

        /// <summary>
        /// 初回起動判定
        /// </summary>
        public bool IsInitial
        {
            get
            {
                return _isInitial;
            }
            set
            {
                _isInitial = value;
            }
        }

        /// <summary>
        /// 担当者コード
        /// </summary>
        public string TantoshaCd
        {
            get
            {
                return _tantoshaCd;
            }
            set
            {
                _tantoshaCd = Util.ToString(value);
            }
        }

        /// <summary>
        /// 勘定科目コード
        /// </summary>
        public string KanjoKamokuCd
        {
            get
            {
                return _kanjoKamokuCd;
            }
            set
            {
                _kanjoKamokuCd = Util.ToString(value);
            }
        }

        /// <summary>
        /// 補助科目コード
        /// </summary>
        public string HojoKamokuCd
        {
            get
            {
                return _hojoKamokuCd;
            }
            set
            {
                _hojoKamokuCd = Util.ToString(value);
            }
        }

        /// <summary>
        /// 部門科目コード
        /// </summary>
        public string BumonCd
        {
            get
            {
                return _bumonCd;
            }
            set
            {
                _bumonCd = Util.ToString(value);
            }
        }

        /// <summary>
        /// 伝票日付(自)
        /// </summary>
        public DateTime DenpyoDateFr
        {
            get
            {
                return _denpyoDateFr;
            }
            set
            {
                _denpyoDateFr = Util.ToDate(value);
            }
        }

        /// <summary>
        /// 伝票日付(至)
        /// </summary>
        public DateTime DenpyoDateTo
        {
            get
            {
                return _denpyoDateTo;
            }
            set
            {
                _denpyoDateTo = Util.ToDate(value);
            }
        }

        /// <summary>
        /// F10押下フラグ
        /// </summary>
        public bool IsPressF10
        {
            get
            {
                return this._isPressF10;
            }
        }

        public int ShishoCode
        {
            get
            {
                return this._shishoCode;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ZMDE1012()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)

        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // サイズを縮める
            this.Size = new Size(720, 500);

            // Escape 使用可否
            this.btnEsc.Enabled = !this.IsInitial;

            // Escape F1 F6 F10のみ表示
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF10.Location = this.btnF4.Location;
            this.btnF6.Location = this.btnF3.Location;
            this.btnF1.Location = this.btnF2.Location;
            this.btnF2.Visible = false;
            this.btnF3.Visible = false;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;
            this.ShowFButton = true;

            // 表示位置設定
            this.StartPosition = FormStartPosition.Manual;
            this.Left = this.Left + (this.Owner.Width - this.Width) / 2;
            this.Top = this.Top + (this.Owner.Height - this.Height) / 2;

            // 水揚支所
            this.txtMizuageShishoCd.Text = this.UInfo.ShishoCd;
            this.lblMizuageShishoNm.Text = this.UInfo.ShishoNm;
            if (Util.ToInt(this.txtMizuageShishoCd.Text) == 0)
            {
                DataRow r = GetPersonInfo(this.UInfo.UserCd);
                if (r != null)
                {
                    this.UInfo.ShishoCd = r["SHISHO_CD"].ToString();
                    this.UInfo.ShishoNm = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.UInfo.ShishoCd, this.UInfo.ShishoCd);
                    this.txtMizuageShishoCd.Text = this.UInfo.ShishoCd;
                    this.lblMizuageShishoNm.Text = this.UInfo.ShishoNm;
                }
            }
            // 取り合えず入力、更新系は触れない様に
            txtMizuageShishoCd.Enabled = false;
            // 科目分類コード設定（設定から取得）
            this._genkin = Util.ToString(this.Config.LoadPgConfig(Constants.SubSys.Zai, "ZMDE1011", "Setting", "bunruiGenkin"));
            this._yokin = Util.ToString(this.Config.LoadPgConfig(Constants.SubSys.Zai, "ZMDE1011", "Setting", "bunruiYokin"));
            // Enter処理を無効化
            this._dtFlg = false;

            // 起動時の各項目値セット
            if (!ValChk.IsEmpty(this.TantoshaCd))
            {
                this.txtTantoshaCd.Text = this.TantoshaCd;
                this.lblTantoshaNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_TANTOSHA", this.txtMizuageShishoCd.Text, this.TantoshaCd);
            }
            if (!ValChk.IsEmpty(this.KanjoKamokuCd))
            {
                this.txtKanjoKamokuCd.Text = this.KanjoKamokuCd;
                this.lblKanjoKamokuNm.Text = this.Dba.GetName(this.UInfo, "TB_ZM_KANJO_KAMOKU", this.txtMizuageShishoCd.Text, this.KanjoKamokuCd);

                // 補助科目コード・部門コード入力制御
                int[] umu = GetUmuInfo(this.txtKanjoKamokuCd.Text);
                this.txtHojoKamokuCd.Enabled = (umu[0] == 1);
                this.txtBumonCd.Enabled = (umu[1] == 1);
            }
            SetJpFr(Util.ConvJpDate(this.DenpyoDateFr, this.Dba));
            SetJpTo(Util.ConvJpDate(this.DenpyoDateTo, this.Dba));

            // 起動時のフォーカス位置
            this.txtKanjoKamokuCd.Focus();
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // F1使用可否制御
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd":
                case "txtTantoshaCd":
                case "txtGengoYearDenpyoDateFr":
                case "txtGengoYearDenpyoDateTo":
                case "txtKanjoKamokuCd":
                case "txtHojoKamokuCd":
                case "txtBumonCd":
                    btnF1.Enabled = true;
                    break;
                default:
                    btnF1.Enabled = false;
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // ダイアログキャンセルで終了
            this.DialogResult = DialogResult.Cancel;
            this.Close();       // 終了
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        public override void PressF1()
        {
            Assembly asm;
            Type t;

            //アクティブコントロールごとの処理
            switch (this.ActiveCtlNm)
            {
                case "txtMizuageShishoCd":
                    #region 水揚支所
                    asm = Assembly.LoadFrom("CMCM2031.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2031.CMCM2031");
                    if (t != null)
                    {
                        Object obj = Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] outData = (string[])frm.OutData;
                                this.txtMizuageShishoCd.Text = outData[0];
                                this.lblMizuageShishoNm.Text = outData[1];
                            }
                        }
                    }
                    #endregion
                    break;
                case "txtGengoYearDenpyoDateFr":
                case "txtGengoYearDenpyoDateTo":
                    #region 元号検索
                    // アセンブリのロード
                    //asm = System.Reflection.Assembly.LoadFrom("COMC9011.exe");
                    asm = Assembly.LoadFrom("CMCM1021.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.com.comc9011.COMC9011");
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            if (this.ActiveCtlNm == "txtGengoYearDenpyoDateFr")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.InData = this.lblGengoDenpyoDateFr.Text;
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.lblGengoDenpyoDateFr.Text = result[1];

                                    // 存在しない日付の場合、補正して存在する日付に戻す
                                    SetJpFr();
                                }
                            }
                            if (this.ActiveCtlNm == "txtGengoYearDenpyoDateTo")
                            {
                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.InData = this.lblGengoDenpyoDateFr.Text;
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    string[] result = (string[])frm.OutData;
                                    this.lblGengoDenpyoDateTo.Text = result[1];

                                    // 存在しない日付の場合、補正して存在する日付に戻す
                                    SetJpTo();
                                }
                            }
                        }
                    }
                    #endregion
                    break;
                case "txtTantoshaCd":
                    #region 担当者検索
                    // アセンブリのロード
                    //asm = System.Reflection.Assembly.LoadFrom("KOBC9041.exe");
                    asm = System.Reflection.Assembly.LoadFrom("CMCM2021.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.kob.kobc9041.KOBC9041");
                    t = asm.GetType("jp.co.fsi.cm.cmcm2021.CMCM2021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.txtTantoshaCd.Text = result[0];
                                this.lblTantoshaNm.Text = result[1];
                            }
                        }
                    }
                    #endregion
                    break;
                case "txtKanjoKamokuCd":
                    #region 勘定科目検索
                    // アセンブリのロード
                    //asm = System.Reflection.Assembly.LoadFrom("ZAMC9011.exe");
                    asm = Assembly.LoadFrom("ZMCM1031.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.zam.zamc9011.ZAMC9014");
                    t = asm.GetType("jp.co.fsi.zm.zmcm1031.ZMCM1034");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            if (this.Par1.Equals(this._genkin))
                                frm.Par1 = "1";
                            else if (this.Par1.Equals(this._yokin))
                                frm.Par1 = "2";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.txtKanjoKamokuCd.Text = result[0];
                                this.lblKanjoKamokuNm.Text = result[1];
                                // 補助科目コード・部門コード入力制御
                                int[] umu = GetUmuInfo(this.txtKanjoKamokuCd.Text);
                                this.txtHojoKamokuCd.Enabled = (umu[0] == 1);
                                this.txtBumonCd.Enabled = (umu[1] == 1);
                                this.txtHojoKamokuCd.Focus();
                            }
                        }
                    }
                    #endregion
                    break;
                case "txtHojoKamokuCd":
                    #region 補助科目検索
                    // アセンブリのロード
                    //asm = System.Reflection.Assembly.LoadFrom("ZAMC9021.exe");
                    asm = Assembly.LoadFrom("ZMCM1041.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.zam.zamc9021.ZAMC9024");
                    t = asm.GetType("jp.co.fsi.zm.zmcm1041.ZMCM1044");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.txtKanjoKamokuCd.Text;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.txtHojoKamokuCd.Text = result[0];
                                this.lblHojoKamokuNm.Text = result[1];
                            }
                        }
                    }
                    #endregion
                    break;
                case "txtBumonCd":
                    #region 部門検索
                    // アセンブリのロード
                    //asm = System.Reflection.Assembly.LoadFrom("COMC8011.exe");  // -> ZAMC9011
                    //asm = System.Reflection.Assembly.LoadFrom("CMCM2041.exe");
                    asm = Assembly.LoadFrom("CMCM1041.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.com.comc8011.COMC8011");
                    //t = asm.GetType("jp.co.fsi.cm.cmcm2041.CMCM2041");
                    t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            //frm.Par1 = "1";
                            frm.Par1 = "TB_CM_BUMON";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.txtBumonCd.Text = result[0];
                                this.lblBumonNm.Text = result[1];
                            }
                        }
                    }
                    #endregion
                    break;
            }
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        public override void PressF6()
        {
            if (ValidateAll())
            {
                //　ダイアログOKで終了
                this._shishoCode = Util.ToInt(this.txtMizuageShishoCd.Text);

                this.TantoshaCd = this.txtTantoshaCd.Text;
                this.KanjoKamokuCd = this.txtKanjoKamokuCd.Text;
                this.HojoKamokuCd = txtHojoKamokuCd.Text;
                this.BumonCd = this.txtBumonCd.Text;
                this.DenpyoDateFr = Util.ConvAdDate(this.lblGengoDenpyoDateFr.Text, this.txtGengoYearDenpyoDateFr.Text,
                                                        this.txtMonthDenpyoDateFr.Text, this.txtDayDenpyoDateFr.Text, this.Dba);
                this.DenpyoDateTo = Util.ConvAdDate(this.lblGengoDenpyoDateTo.Text, this.txtGengoYearDenpyoDateTo.Text,
                                                        this.txtMonthDenpyoDateTo.Text, this.txtDayDenpyoDateTo.Text, this.Dba);
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        /// <summary>
        /// F10キー押下時処理
        /// </summary>
        public override void PressF10()
        {
            this._isPressF10 = true;
            this.PressEsc();
        }

        #endregion

        #region イベント

        /// <summary>
        /// 水揚支所入力チェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMizuageShishoCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                e.Cancel = true;
                this.txtMizuageShishoCd.SelectAll();
                this.txtMizuageShishoCd.Focus();
            }
        }

        /// <summary>
        /// 担当者コードのチェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTantoshaCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidTantoshaCd())
            {
                e.Cancel = true;
                this.txtTantoshaCd.SelectAll();
                return;
            }
            
            if (this.txtTantoshaCd.Modified)
            {
                // 担当者名セット
                this.lblTantoshaNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_TANTOSHA", this.txtMizuageShishoCd.Text, this.txtTantoshaCd.Text);

                this.txtTantoshaCd.Modified = false;
            }
        }

        /// <summary>
        /// 勘定科目コードのチェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKanjoKamokuCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidKanjoKamokuCd())
            {
                e.Cancel = true;
                this.txtKanjoKamokuCd.SelectAll();
                return;
            }

            if (this.txtKanjoKamokuCd.Modified)
            {
                if (ValChk.IsEmpty(this.txtKanjoKamokuCd.Text))
                {
                    Msg.Error("入力に誤りがあります。");
                    e.Cancel = true;
                    return;
                }
                else
                {
                    // 勘定科目名
                    this.lblKanjoKamokuNm.Text = this.Dba.GetName(this.UInfo, "TB_ZM_KANJO_KAMOKU", this.txtMizuageShishoCd.Text, this.txtKanjoKamokuCd.Text);
                    // 補助科目、部門情報クリア            
                    this.txtHojoKamokuCd.Text = "";
                    this.lblHojoKamokuNm.Text = "";
                    this.txtBumonCd.Text = "";
                    this.lblBumonNm.Text = "";
                    // 補助科目コード・部門コード入力制御
                    int[] umu = GetUmuInfo(this.txtKanjoKamokuCd.Text);
                    this.txtHojoKamokuCd.Enabled = (umu[0] == 1);
                    this.txtBumonCd.Enabled = (umu[1] == 1);
                    this.txtHojoKamokuCd.Focus();
                    this.txtKanjoKamokuCd.Modified = false;
                }
            }
        }

        /// <summary>
        /// 補助科目コードのチェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtHojoKamokuCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidHojoKamokuCd())
            {
                e.Cancel = true;
                this.txtHojoKamokuCd.SelectAll();
                return;
            }

            if (this.txtHojoKamokuCd.Modified)
            {
                // 補助科目名セット
                this.lblHojoKamokuNm.Text =
                    this.GetHojoKmkNm(this.txtMizuageShishoCd.Text, this.txtKanjoKamokuCd.Text, this.txtHojoKamokuCd.Text);

                this.txtHojoKamokuCd.Modified = false;
            }
        }

        /// <summary>
        /// 部門コードのチェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBumonCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidBumonCd())
            {
                e.Cancel = true;
                this.txtBumonCd.SelectAll();
                return;
            }
            
            if (this.txtBumonCd.Modified)
            {
                // 部門名セット
                this.lblBumonNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_BUMON", this.txtMizuageShishoCd.Text, this.txtBumonCd.Text);

                this.txtBumonCd.Modified = false;
            }
        }

        /// <summary>
        /// 伝票日付(自)・年値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGengoYearDenpyoDateFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtGengoYearDenpyoDateFr.Text, this.txtGengoYearDenpyoDateFr.MaxLength))
            {
                e.Cancel = true;
                this.txtGengoYearDenpyoDateFr.SelectAll();
            }
            else
            {
                this.txtGengoYearDenpyoDateFr.Text = Util.ToString(IsValid.SetYear(this.txtGengoYearDenpyoDateFr.Text));
                CheckJpFr();
                SetJpFr();
            }
        }

        /// <summary>
        /// 伝票日付(自)・月の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonthDenpyoDateFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtMonthDenpyoDateFr.Text, this.txtMonthDenpyoDateFr.MaxLength))
            {
                e.Cancel = true;
                this.txtMonthDenpyoDateFr.SelectAll();
            }
            else
            {
                this.txtMonthDenpyoDateFr.Text = Util.ToString(IsValid.SetMonth(this.txtMonthDenpyoDateFr.Text));
                CheckJpFr();
                SetJpFr();
            }
        }

        /// <summary>
        /// 伝票日付(自)・日の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDayDenpyoDateFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtDayDenpyoDateFr.Text, this.txtDayDenpyoDateFr.MaxLength))
            {
                e.Cancel = true;
                this.txtDayDenpyoDateFr.SelectAll();
            }
            else
            {
                this.txtDayDenpyoDateFr.Text = Util.ToString(IsValid.SetDay(this.txtDayDenpyoDateFr.Text));
                CheckJpFr();
                SetJpFr();
            }
        }

        /// <summary>
        /// 伝票日付(至)・年値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGengoYearDenpyoDateTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtGengoYearDenpyoDateTo.Text, this.txtGengoYearDenpyoDateTo.MaxLength))
            {
                e.Cancel = true;
                this.txtGengoYearDenpyoDateTo.SelectAll();
            }
            else
            {
                this.txtGengoYearDenpyoDateTo.Text = Util.ToString(IsValid.SetYear(this.txtGengoYearDenpyoDateTo.Text));
                CheckJpTo();
                SetJpTo();
            }
        }

        /// <summary>
        /// 伝票日付(至)・月の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonthDenpyoDateTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtMonthDenpyoDateTo.Text, this.txtMonthDenpyoDateTo.MaxLength))
            {
                e.Cancel = true;
                this.txtMonthDenpyoDateTo.SelectAll();
            }
            else
            {
                this.txtMonthDenpyoDateTo.Text = Util.ToString(IsValid.SetMonth(this.txtMonthDenpyoDateTo.Text));
                CheckJpTo();
                SetJpTo();
            }
        }

        /// <summary>
        /// 伝票日付(至)・日の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDayDenpyoDateTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtDayDenpyoDateTo.Text, this.txtDayDenpyoDateTo.MaxLength))
            {
                e.Cancel = true;
                this.txtDayDenpyoDateTo.SelectAll();
                // Enter処理を無効化
                this._dtFlg = false;
            }
            else
            {
                this.txtDayDenpyoDateTo.Text = Util.ToString(IsValid.SetDay(this.txtDayDenpyoDateTo.Text));
                CheckJpTo();
                SetJpTo();
                // Enter処理を有効化
                this._dtFlg = true;
            }
        }

        /// <summary>
        /// 伝票日付(至)のEnter押下時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDayDenpyoDateTo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this._dtFlg)
            {
                // Enter処理を無効化
                this._dtFlg = false;
                // 実行処理の呼び出し
                this.btnF6.PerformClick();
            }
        }

        private void ZMDE1012_Shown(object sender, EventArgs e)
        {
            if (!ValChk.IsEmpty(this.txtTantoshaCd.Text))
            {
                // 起動時のフォーカス位置
                this.txtKanjoKamokuCd.Focus();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 年月日(自)の月末入力チェック
        /// </summary>
        /// 
        private void CheckJpFr()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblGengoDenpyoDateFr.Text, this.txtGengoYearDenpyoDateFr.Text,
                this.txtMonthDenpyoDateFr.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDayDenpyoDateFr.Text) > lastDayInMonth)
            {
                this.txtDayDenpyoDateFr.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(自)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJpFr()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpFr(Util.ConvJpDate(
                    ZaUtil.FixNendoDate(
                    Util.ConvAdDate(this.lblGengoDenpyoDateFr.Text, 
                                    this.txtGengoYearDenpyoDateFr.Text,
                                    this.txtMonthDenpyoDateFr.Text,
                                    this.txtDayDenpyoDateFr.Text, this.Dba), this.UInfo), this.Dba));
        }

        /// <summary>
        /// 配列に格納された伝票日付(自)和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpFr(string[] arrJpDate)
        {
            this.lblGengoDenpyoDateFr.Text = arrJpDate[0];
            this.txtGengoYearDenpyoDateFr.Text = arrJpDate[2];
            this.txtMonthDenpyoDateFr.Text = arrJpDate[3];
            this.txtDayDenpyoDateFr.Text = arrJpDate[4];
        }

        /// <summary>
        /// 年月日(至)の月末入力チェック
        /// </summary>
        /// 
        private void CheckJpTo()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblGengoDenpyoDateTo.Text, this.txtGengoYearDenpyoDateTo.Text,
                this.txtMonthDenpyoDateTo.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDayDenpyoDateTo.Text) > lastDayInMonth)
            {
                this.txtDayDenpyoDateTo.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(至)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJpTo()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            SetJpTo(Util.ConvJpDate(
                    ZaUtil.FixNendoDate(
                    Util.ConvAdDate(this.lblGengoDenpyoDateTo.Text, 
                                    this.txtGengoYearDenpyoDateTo.Text,
                                    this.txtMonthDenpyoDateTo.Text,
                                    this.txtDayDenpyoDateTo.Text, this.Dba), this.UInfo), this.Dba));
        }

        /// <summary>
        /// 配列に格納された伝票日付(至)和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpTo(string[] arrJpDate)
        {
            this.lblGengoDenpyoDateTo.Text = arrJpDate[0];
            this.txtGengoYearDenpyoDateTo.Text = arrJpDate[2];
            this.txtMonthDenpyoDateTo.Text = arrJpDate[3];
            this.txtDayDenpyoDateTo.Text = arrJpDate[4];
        }

        /// <summary>
        /// 水揚支所の値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool IsValidMizuageShishoCd()
        {
            // 空 又は 0入力の場合
            if (ValChk.IsEmpty(this.txtMizuageShishoCd.Text) || Equals(this.txtMizuageShishoCd.Text, "0"))
            {
                // 伝票入力時は必須
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            // 水揚支所名称を表示する
            this.lblMizuageShishoNm.Text = this.Dba.GetName(this.UInfo, "TB_CM_SHISHO", this.txtMizuageShishoCd.Text, this.txtMizuageShishoCd.Text);

            if (ValChk.IsEmpty(this.lblMizuageShishoNm.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            // 支所の切り替え
            if (Util.ToInt(this.UInfo.ShishoCd) != Util.ToInt(this.txtMizuageShishoCd.Text))
            {
                this.UInfo.ShishoCd = this.txtMizuageShishoCd.Text;
                this.UInfo.ShishoNm = this.lblMizuageShishoNm.Text;
            }

            return true;
        }

        /// <summary>
        /// 担当者コードの入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidTantoshaCd()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtTantoshaCd.Text) || ValChk.IsEmpty(this.txtTantoshaCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // レコード存在チェック
            string name = this.Dba.GetName(this.UInfo, "TB_CM_TANTOSHA", this.txtMizuageShishoCd.Text, this.txtTantoshaCd.Text);
            if (ValChk.IsEmpty(name))
            {
                Msg.Error("担当者コードが存在しません。"); 
                return false;
            }

            return true;
        }

        /// <summary>
        /// 勘定科目コードの入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidKanjoKamokuCd()
        {
            // 数字のみの入力を許可　未入力はNG
            //if (!ValChk.IsNumber(this.txtKanjoKamokuCd.Text) || ValChk.IsEmpty(this.txtKanjoKamokuCd.Text))
            if (!ValChk.IsNumber(this.txtKanjoKamokuCd.Text) || ValChk.IsEmpty(this.txtKanjoKamokuCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            if (!ValChk.IsEmpty(this.txtKanjoKamokuCd.Text))
            {
                // 範囲データ存在確認
                if (!IsExistKanjyoKamoku())
                {
                    Msg.Error("勘定科目コードの入力誤りです。");
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 補助科目コードの入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidHojoKamokuCd()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtHojoKamokuCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 補助科目利用ありの場合
            if (this.txtHojoKamokuCd.Enabled)
            {
                // 未入力はNG
                if (ValChk.IsEmpty(this.txtHojoKamokuCd.Text))
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }
                else
                {
                    // レコード存在チェック
                    string name = this.GetHojoKmkNm(this.txtMizuageShishoCd.Text, this.txtKanjoKamokuCd.Text, this.txtHojoKamokuCd.Text);
                    if (ValChk.IsEmpty(name))
                    {
                        Msg.Error("補助科目コードが存在しません。");
                        return false;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// 部門コードの入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidBumonCd()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtBumonCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 部門利用ありの場合
            if (this.txtBumonCd.Enabled)
            {
                // 未入力はNG
                if (ValChk.IsEmpty(this.txtBumonCd.Text))
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }
                else
                {
                    // レコード存在チェック
                    string name = this.Dba.GetName(this.UInfo, "TB_CM_BUMON", this.txtMizuageShishoCd.Text, this.txtBumonCd.Text);
                    if (ValChk.IsEmpty(name))
                    {
                        Msg.Error("部門コードが存在しません。"); //  TB_CM_BUMON
                        return false;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 水揚支所の入力チェック
            if (!IsValid.IsValidShishoCd(this.txtMizuageShishoCd.Text, this.lblMizuageShishoNm.Text, this.txtMizuageShishoCd.MaxLength) || !IsValidMizuageShishoCd())
            {
                this.txtMizuageShishoCd.Focus();
                this.txtMizuageShishoCd.SelectAll();
                return false;
            }

            // 担当者コードのチェック
            if (!IsValidTantoshaCd())
            {
                this.txtTantoshaCd.Focus();
                return false;
            }

            //// 勘定科目コードの必須入力チェック
            //if (ValChk.IsEmpty(this.txtKanjoKamokuCd.Text))
            //{
            //    Msg.Error("入力に誤りがあります。");
            //    return false;
            //}

            // 勘定科目コードのチェック
            if (!IsValidKanjoKamokuCd())
            {
                this.txtKanjoKamokuCd.Focus();
                return false;
            }

            // 補助コードのチェック
            if (!IsValidHojoKamokuCd())
            {
                this.txtHojoKamokuCd.Focus();
                return false;
            }

            // 部門コードのチェック
            if (!IsValidBumonCd())
            {
                this.txtBumonCd.Focus();
                return false;
            }

            // 日付の範囲大小チェック
            DateTime dateFr = Util.ConvAdDate(this.lblGengoDenpyoDateFr.Text, this.txtGengoYearDenpyoDateFr.Text,
                                                    this.txtMonthDenpyoDateFr.Text, this.txtDayDenpyoDateFr.Text, this.Dba);
            DateTime dateTo = Util.ConvAdDate(this.lblGengoDenpyoDateTo.Text, this.txtGengoYearDenpyoDateTo.Text,
                                                    this.txtMonthDenpyoDateTo.Text, this.txtDayDenpyoDateTo.Text, this.Dba);
            if (dateFr > dateTo)
            {
                Msg.Error("入力に誤りがあります。");
                this.txtGengoYearDenpyoDateFr.Focus();
                this.txtGengoYearDenpyoDateFr.SelectAll();
                return false;
            }

            return true;
        }

        /// <summary>
        /// 勘定科目コードの存在チェック
        /// </summary>
        private bool IsExistKanjyoKamoku()
        {
            // 勘定科目テーブルの存在確認
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, this.txtKanjoKamokuCd.Text);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@KAMOKU_BUNRUI_CD", SqlDbType.Decimal, 5, Util.ToDecimal(Util.ToString(this.Par1)));
            StringBuilder cols = new StringBuilder();
            cols.Append("KANJO_KAMOKU_CD");
            StringBuilder where = new StringBuilder();
            where.Append("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND KAIKEI_NENDO = @KAIKEI_NENDO");
            where.Append(" AND KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD");
            if (this.Par1 != null)
                where.Append(" AND KAMOKU_BUNRUI_CD = @KAMOKU_BUNRUI_CD");
            DataTable dt = this.Dba.GetDataTableByConditionWithParams(Util.ToString(cols)
                            , "TB_ZM_KANJO_KAMOKU"
                            , Util.ToString(where), dpc);
            return (dt.Rows.Count > 0);
        }

        /// <summary>
        /// 補助科目有無・部門有無情報の取得
        /// </summary>
        /// <param name="kanjoKamokuCd">勘定科目コード</param>
        private int[] GetUmuInfo(string kanjoKamokuCd)
        {
            int[] ret = new int[2]{0, 0};

            // 勘定科目テーブルの存在確認
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, this.txtKanjoKamokuCd.Text);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            StringBuilder cols = new StringBuilder();
            cols.Append("HOJO_KAMOKU_UMU");
            cols.Append(", BUMON_UMU");
            StringBuilder where = new StringBuilder();
            where.Append("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND KAIKEI_NENDO = @KAIKEI_NENDO");
            where.Append(" AND KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD");
            DataTable dt = this.Dba.GetDataTableByConditionWithParams(Util.ToString(cols)
                            , "TB_ZM_KANJO_KAMOKU"
                            , Util.ToString(where), dpc);
            if (dt.Rows.Count > 0)
            {
                ret[0] = Util.ToInt(dt.Rows[0]["HOJO_KAMOKU_UMU"]);
                ret[1] = Util.ToInt(dt.Rows[0]["BUMON_UMU"]);
            }

            return ret;
        }

        /// <summary>
        /// 補助科目名取得
        /// </summary>
        /// <param name="shishoCd">支所コード</param>
        /// <param name="kanjoKmkCd">勘定科目コード</param>
        /// <param name="code">補助科目コード</param>
        /// <returns>補助科目名</returns>
        private string GetHojoKmkNm(string shishoCd, string kanjoKmkCd, string code)
        {
            string name = null;

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, kanjoKmkCd);
            dpc.SetParam("@HOJO_KAMOKU_CD", SqlDbType.Decimal, 10, code);
            StringBuilder where = new StringBuilder();
            where.Append("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND (SHISHO_CD = @SHISHO_CD or SHISHO_CD is null)");
            where.Append(" AND KAIKEI_NENDO = @KAIKEI_NENDO");
            where.Append(" AND KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD");
            where.Append(" AND HOJO_KAMOKU_CD = @HOJO_KAMOKU_CD");

            DataTable dt = this.Dba.GetDataTableByConditionWithParams(
                "*",
                "VI_ZM_HOJO_KAMOKU",
                where.ToString(),
                dpc);
            if (dt.Rows.Count != 0)
            {
                name = Util.ToString(dt.Rows[0]["HOJO_KAMOKU_NM"]);
            }
            return name;
        }

        /// <summary>
        /// 担当者DatRowの取得
        /// </summary>
        /// <param name="code">担当者コード</param>
        /// <returns></returns>
        private DataRow GetPersonInfo(string code)
        {
            DataRow r = null;
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.VarChar, 6, this.UInfo.KaishaCd);
            dpc.SetParam("@TANTOSHA_CD", SqlDbType.Decimal, 4, Util.ToDecimal(code));
            DataTable dt = this.Dba.GetDataTableByConditionWithParams(
                "*",
                "TB_CM_TANTOSHA",
                "KAISHA_CD = @KAISHA_CD AND TANTOSHA_CD = @TANTOSHA_CD ",
                dpc);
            if (dt.Rows.Count != 0)
            {
                r = dt.Rows[0];
            }
            return r;
        }
        #endregion
    }
}
