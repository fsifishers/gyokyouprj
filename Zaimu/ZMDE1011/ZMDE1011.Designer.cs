﻿namespace jp.co.fsi.zm.zmde1011
{
    partial class ZMDE1011
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mtbList = new jp.co.fsi.common.controls.SjMultiTable();
            this.panel4 = new jp.co.fsi.common.FsiPanel();
            this.lblShukkin = new System.Windows.Forms.Label();
            this.lblGokeiZandaka = new System.Windows.Forms.Label();
            this.lblNyukin = new System.Windows.Forms.Label();
            this.lblGokei = new System.Windows.Forms.Label();
            this.lblTantosha = new System.Windows.Forms.Label();
            this.lblTantoshaNm = new System.Windows.Forms.Label();
            this.txtTantoshaCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblDate = new System.Windows.Forms.Label();
            this.lblBumonNm = new System.Windows.Forms.Label();
            this.lblBumonCd = new System.Windows.Forms.Label();
            this.txtBumonCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblZandaka = new System.Windows.Forms.Label();
            this.txtZandaka = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKanjoKamokuNm = new System.Windows.Forms.Label();
            this.lblKanjoKamokuCd = new System.Windows.Forms.Label();
            this.txtKanjoKamokuCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblHojoKamokuNm = new System.Windows.Forms.Label();
            this.lblHojoKamokuCd = new System.Windows.Forms.Label();
            this.txtHojoKamokuCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblBakKanjoKamoku = new System.Windows.Forms.Label();
            this.lblBakHojoKamokuCd = new System.Windows.Forms.Label();
            this.lblBakBumonCd = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Text = "出納帳入力";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            // 
            // mtbList
            // 
            this.mtbList.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.mtbList.FixedCols = 0;
            this.mtbList.FocusField = null;
            this.mtbList.Location = new System.Drawing.Point(12, 109);
            this.mtbList.Name = "mtbList";
            this.mtbList.NotSelectableCols = 0;
            this.mtbList.SelectRange = null;
            this.mtbList.Size = new System.Drawing.Size(799, 377);
            this.mtbList.TabIndex = 0;
            this.mtbList.Text = "SjMultiTable1";
            this.mtbList.UndoBufferEnabled = false;
            this.mtbList.RecordEnter += new systembase.table.UTable.RecordEnterEventHandler(this.mtbList_RecordEnter);
            this.mtbList.FieldValidating += new systembase.table.UTable.FieldValidatingEventHandler(this.mtbList_FieldValidating);
            this.mtbList.FieldEnter += new systembase.table.UTable.FieldEnterEventHandler(this.mtbList_FieldEnter);
            this.mtbList.EditStart += new systembase.table.UTable.EditStartEventHandler(this.mtbList_EditStart);
            this.mtbList.InitializeEditor += new systembase.table.UTable.InitializeEditorEventHandler(this.mtbList_InitializeEditor);
            this.mtbList.Leave += new System.EventHandler(this.mtbList_Leave);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Silver;
            this.panel4.Controls.Add(this.lblShukkin);
            this.panel4.Controls.Add(this.lblGokeiZandaka);
            this.panel4.Controls.Add(this.lblNyukin);
            this.panel4.Controls.Add(this.lblGokei);
            this.panel4.Location = new System.Drawing.Point(17, 491);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(789, 24);
            this.panel4.TabIndex = 910;
            // 
            // lblShukkin
            // 
            this.lblShukkin.BackColor = System.Drawing.Color.Transparent;
            this.lblShukkin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblShukkin.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShukkin.Location = new System.Drawing.Point(528, 4);
            this.lblShukkin.Name = "lblShukkin";
            this.lblShukkin.Size = new System.Drawing.Size(80, 16);
            this.lblShukkin.TabIndex = 914;
            this.lblShukkin.Text = "　";
            this.lblShukkin.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblGokeiZandaka
            // 
            this.lblGokeiZandaka.BackColor = System.Drawing.Color.Transparent;
            this.lblGokeiZandaka.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblGokeiZandaka.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGokeiZandaka.Location = new System.Drawing.Point(608, 4);
            this.lblGokeiZandaka.Name = "lblGokeiZandaka";
            this.lblGokeiZandaka.Size = new System.Drawing.Size(80, 16);
            this.lblGokeiZandaka.TabIndex = 913;
            this.lblGokeiZandaka.Text = "　";
            this.lblGokeiZandaka.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblNyukin
            // 
            this.lblNyukin.BackColor = System.Drawing.Color.Transparent;
            this.lblNyukin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNyukin.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblNyukin.Location = new System.Drawing.Point(448, 4);
            this.lblNyukin.Name = "lblNyukin";
            this.lblNyukin.Size = new System.Drawing.Size(80, 16);
            this.lblNyukin.TabIndex = 912;
            this.lblNyukin.Text = "　";
            this.lblNyukin.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblGokei
            // 
            this.lblGokei.BackColor = System.Drawing.Color.Silver;
            this.lblGokei.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGokei.Location = new System.Drawing.Point(240, 5);
            this.lblGokei.Name = "lblGokei";
            this.lblGokei.Size = new System.Drawing.Size(100, 14);
            this.lblGokei.TabIndex = 910;
            this.lblGokei.Text = "合　　計";
            this.lblGokei.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblTantosha
            // 
            this.lblTantosha.AutoSize = true;
            this.lblTantosha.BackColor = System.Drawing.Color.Silver;
            this.lblTantosha.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTantosha.Location = new System.Drawing.Point(543, 51);
            this.lblTantosha.Name = "lblTantosha";
            this.lblTantosha.Size = new System.Drawing.Size(49, 13);
            this.lblTantosha.TabIndex = 5;
            this.lblTantosha.Text = "担当者";
            // 
            // lblTantoshaNm
            // 
            this.lblTantoshaNm.BackColor = System.Drawing.Color.Silver;
            this.lblTantoshaNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTantoshaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTantoshaNm.Location = new System.Drawing.Point(648, 47);
            this.lblTantoshaNm.Name = "lblTantoshaNm";
            this.lblTantoshaNm.Size = new System.Drawing.Size(150, 20);
            this.lblTantoshaNm.TabIndex = 0;
            this.lblTantoshaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTantoshaCd
            // 
            this.txtTantoshaCd.AutoSizeFromLength = false;
            this.txtTantoshaCd.BackColor = System.Drawing.SystemColors.Window;
            this.txtTantoshaCd.DisplayLength = null;
            this.txtTantoshaCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTantoshaCd.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtTantoshaCd.Location = new System.Drawing.Point(593, 46);
            this.txtTantoshaCd.MaxLength = 4;
            this.txtTantoshaCd.Name = "txtTantoshaCd";
            this.txtTantoshaCd.ReadOnly = true;
            this.txtTantoshaCd.Size = new System.Drawing.Size(54, 23);
            this.txtTantoshaCd.TabIndex = 1;
            this.txtTantoshaCd.TabStop = false;
            this.txtTantoshaCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblDate
            // 
            this.lblDate.BackColor = System.Drawing.Color.Silver;
            this.lblDate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDate.Location = new System.Drawing.Point(539, 79);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(260, 21);
            this.lblDate.TabIndex = 5;
            this.lblDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblBumonNm
            // 
            this.lblBumonNm.BackColor = System.Drawing.Color.Silver;
            this.lblBumonNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBumonNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBumonNm.Location = new System.Drawing.Point(445, 47);
            this.lblBumonNm.Name = "lblBumonNm";
            this.lblBumonNm.Size = new System.Drawing.Size(88, 21);
            this.lblBumonNm.TabIndex = 7;
            this.lblBumonNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblBumonCd
            // 
            this.lblBumonCd.AutoSize = true;
            this.lblBumonCd.BackColor = System.Drawing.Color.Silver;
            this.lblBumonCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBumonCd.Location = new System.Drawing.Point(339, 51);
            this.lblBumonCd.Name = "lblBumonCd";
            this.lblBumonCd.Size = new System.Drawing.Size(35, 13);
            this.lblBumonCd.TabIndex = 5;
            this.lblBumonCd.Text = "部門";
            // 
            // txtBumonCd
            // 
            this.txtBumonCd.AutoSizeFromLength = false;
            this.txtBumonCd.BackColor = System.Drawing.SystemColors.Window;
            this.txtBumonCd.DisplayLength = null;
            this.txtBumonCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtBumonCd.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtBumonCd.Location = new System.Drawing.Point(403, 46);
            this.txtBumonCd.MaxLength = 4;
            this.txtBumonCd.Name = "txtBumonCd";
            this.txtBumonCd.ReadOnly = true;
            this.txtBumonCd.Size = new System.Drawing.Size(41, 23);
            this.txtBumonCd.TabIndex = 1;
            this.txtBumonCd.TabStop = false;
            this.txtBumonCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblZandaka
            // 
            this.lblZandaka.AutoSize = true;
            this.lblZandaka.BackColor = System.Drawing.Color.Silver;
            this.lblZandaka.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblZandaka.Location = new System.Drawing.Point(337, 83);
            this.lblZandaka.Name = "lblZandaka";
            this.lblZandaka.Size = new System.Drawing.Size(63, 13);
            this.lblZandaka.TabIndex = 5;
            this.lblZandaka.Text = "繰越残高";
            // 
            // txtZandaka
            // 
            this.txtZandaka.AutoSizeFromLength = false;
            this.txtZandaka.BackColor = System.Drawing.SystemColors.Window;
            this.txtZandaka.DisplayLength = null;
            this.txtZandaka.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtZandaka.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtZandaka.Location = new System.Drawing.Point(403, 78);
            this.txtZandaka.MaxLength = 4;
            this.txtZandaka.Name = "txtZandaka";
            this.txtZandaka.ReadOnly = true;
            this.txtZandaka.Size = new System.Drawing.Size(131, 23);
            this.txtZandaka.TabIndex = 1;
            this.txtZandaka.TabStop = false;
            this.txtZandaka.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblKanjoKamokuNm
            // 
            this.lblKanjoKamokuNm.BackColor = System.Drawing.Color.Silver;
            this.lblKanjoKamokuNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKanjoKamokuNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKanjoKamokuNm.Location = new System.Drawing.Point(135, 47);
            this.lblKanjoKamokuNm.Name = "lblKanjoKamokuNm";
            this.lblKanjoKamokuNm.Size = new System.Drawing.Size(190, 21);
            this.lblKanjoKamokuNm.TabIndex = 7;
            this.lblKanjoKamokuNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKanjoKamokuCd
            // 
            this.lblKanjoKamokuCd.AutoSize = true;
            this.lblKanjoKamokuCd.BackColor = System.Drawing.Color.Silver;
            this.lblKanjoKamokuCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKanjoKamokuCd.Location = new System.Drawing.Point(17, 51);
            this.lblKanjoKamokuCd.Name = "lblKanjoKamokuCd";
            this.lblKanjoKamokuCd.Size = new System.Drawing.Size(63, 13);
            this.lblKanjoKamokuCd.TabIndex = 5;
            this.lblKanjoKamokuCd.Text = "勘定科目";
            // 
            // txtKanjoKamokuCd
            // 
            this.txtKanjoKamokuCd.AutoSizeFromLength = false;
            this.txtKanjoKamokuCd.BackColor = System.Drawing.SystemColors.Window;
            this.txtKanjoKamokuCd.DisplayLength = null;
            this.txtKanjoKamokuCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKanjoKamokuCd.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKanjoKamokuCd.Location = new System.Drawing.Point(81, 46);
            this.txtKanjoKamokuCd.MaxLength = 4;
            this.txtKanjoKamokuCd.Name = "txtKanjoKamokuCd";
            this.txtKanjoKamokuCd.ReadOnly = true;
            this.txtKanjoKamokuCd.Size = new System.Drawing.Size(53, 23);
            this.txtKanjoKamokuCd.TabIndex = 1;
            this.txtKanjoKamokuCd.TabStop = false;
            this.txtKanjoKamokuCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblHojoKamokuNm
            // 
            this.lblHojoKamokuNm.BackColor = System.Drawing.Color.Silver;
            this.lblHojoKamokuNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblHojoKamokuNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblHojoKamokuNm.Location = new System.Drawing.Point(135, 79);
            this.lblHojoKamokuNm.Name = "lblHojoKamokuNm";
            this.lblHojoKamokuNm.Size = new System.Drawing.Size(190, 21);
            this.lblHojoKamokuNm.TabIndex = 7;
            this.lblHojoKamokuNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblHojoKamokuCd
            // 
            this.lblHojoKamokuCd.AutoSize = true;
            this.lblHojoKamokuCd.BackColor = System.Drawing.Color.Silver;
            this.lblHojoKamokuCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblHojoKamokuCd.Location = new System.Drawing.Point(17, 83);
            this.lblHojoKamokuCd.Name = "lblHojoKamokuCd";
            this.lblHojoKamokuCd.Size = new System.Drawing.Size(63, 13);
            this.lblHojoKamokuCd.TabIndex = 5;
            this.lblHojoKamokuCd.Text = "補助科目";
            // 
            // txtHojoKamokuCd
            // 
            this.txtHojoKamokuCd.AutoSizeFromLength = false;
            this.txtHojoKamokuCd.BackColor = System.Drawing.SystemColors.Window;
            this.txtHojoKamokuCd.DisplayLength = null;
            this.txtHojoKamokuCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtHojoKamokuCd.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtHojoKamokuCd.Location = new System.Drawing.Point(81, 78);
            this.txtHojoKamokuCd.MaxLength = 4;
            this.txtHojoKamokuCd.Name = "txtHojoKamokuCd";
            this.txtHojoKamokuCd.ReadOnly = true;
            this.txtHojoKamokuCd.Size = new System.Drawing.Size(53, 23);
            this.txtHojoKamokuCd.TabIndex = 1;
            this.txtHojoKamokuCd.TabStop = false;
            this.txtHojoKamokuCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblBakKanjoKamoku
            // 
            this.lblBakKanjoKamoku.BackColor = System.Drawing.Color.Silver;
            this.lblBakKanjoKamoku.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBakKanjoKamoku.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblBakKanjoKamoku.Location = new System.Drawing.Point(12, 41);
            this.lblBakKanjoKamoku.Name = "lblBakKanjoKamoku";
            this.lblBakKanjoKamoku.Size = new System.Drawing.Size(318, 32);
            this.lblBakKanjoKamoku.TabIndex = 917;
            this.lblBakKanjoKamoku.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblBakHojoKamokuCd
            // 
            this.lblBakHojoKamokuCd.BackColor = System.Drawing.Color.Silver;
            this.lblBakHojoKamokuCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBakHojoKamokuCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblBakHojoKamokuCd.Location = new System.Drawing.Point(12, 74);
            this.lblBakHojoKamokuCd.Name = "lblBakHojoKamokuCd";
            this.lblBakHojoKamokuCd.Size = new System.Drawing.Size(318, 32);
            this.lblBakHojoKamokuCd.TabIndex = 918;
            this.lblBakHojoKamokuCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblBakBumonCd
            // 
            this.lblBakBumonCd.BackColor = System.Drawing.Color.Silver;
            this.lblBakBumonCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBakBumonCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblBakBumonCd.Location = new System.Drawing.Point(331, 41);
            this.lblBakBumonCd.Name = "lblBakBumonCd";
            this.lblBakBumonCd.Size = new System.Drawing.Size(206, 32);
            this.lblBakBumonCd.TabIndex = 919;
            this.lblBakBumonCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Silver;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.label2.Location = new System.Drawing.Point(331, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(475, 32);
            this.label2.TabIndex = 920;
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Silver;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.label1.Location = new System.Drawing.Point(538, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(268, 32);
            this.label1.TabIndex = 921;
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Silver;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.label3.Location = new System.Drawing.Point(12, 490);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(799, 27);
            this.label3.TabIndex = 922;
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ZMDE1011
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblTantosha);
            this.Controls.Add(this.lblTantoshaNm);
            this.Controls.Add(this.txtTantoshaCd);
            this.Controls.Add(this.lblDate);
            this.Controls.Add(this.lblZandaka);
            this.Controls.Add(this.txtZandaka);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblBumonNm);
            this.Controls.Add(this.lblBumonCd);
            this.Controls.Add(this.txtBumonCd);
            this.Controls.Add(this.lblBakBumonCd);
            this.Controls.Add(this.lblHojoKamokuNm);
            this.Controls.Add(this.lblHojoKamokuCd);
            this.Controls.Add(this.txtHojoKamokuCd);
            this.Controls.Add(this.lblBakHojoKamokuCd);
            this.Controls.Add(this.lblKanjoKamokuNm);
            this.Controls.Add(this.lblKanjoKamokuCd);
            this.Controls.Add(this.txtKanjoKamokuCd);
            this.Controls.Add(this.lblBakKanjoKamoku);
            this.Controls.Add(this.mtbList);
            this.Controls.Add(this.label1);
            this.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.Name = "ZMDE1011";
            this.Text = "出納帳入力";
            this.Shown += new System.EventHandler(this.ZMDE1011_Shown);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.mtbList, 0);
            this.Controls.SetChildIndex(this.lblBakKanjoKamoku, 0);
            this.Controls.SetChildIndex(this.txtKanjoKamokuCd, 0);
            this.Controls.SetChildIndex(this.lblKanjoKamokuCd, 0);
            this.Controls.SetChildIndex(this.lblKanjoKamokuNm, 0);
            this.Controls.SetChildIndex(this.lblBakHojoKamokuCd, 0);
            this.Controls.SetChildIndex(this.txtHojoKamokuCd, 0);
            this.Controls.SetChildIndex(this.lblHojoKamokuCd, 0);
            this.Controls.SetChildIndex(this.lblHojoKamokuNm, 0);
            this.Controls.SetChildIndex(this.lblBakBumonCd, 0);
            this.Controls.SetChildIndex(this.txtBumonCd, 0);
            this.Controls.SetChildIndex(this.lblBumonCd, 0);
            this.Controls.SetChildIndex(this.lblBumonNm, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.txtZandaka, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.lblZandaka, 0);
            this.Controls.SetChildIndex(this.lblDate, 0);
            this.Controls.SetChildIndex(this.txtTantoshaCd, 0);
            this.Controls.SetChildIndex(this.lblTantoshaNm, 0);
            this.Controls.SetChildIndex(this.lblTantosha, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.Controls.SetChildIndex(this.panel4, 0);
            this.pnlDebug.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private jp.co.fsi.common.controls.SjMultiTable mtbList;
        private jp.co.fsi.common.FsiPanel panel4;
        private System.Windows.Forms.Label lblShukkin;
        private System.Windows.Forms.Label lblGokeiZandaka;
        private System.Windows.Forms.Label lblNyukin;
        private System.Windows.Forms.Label lblGokei;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.Label lblBumonNm;
        private System.Windows.Forms.Label lblBumonCd;
        private jp.co.fsi.common.controls.FsiTextBox txtBumonCd;
        private System.Windows.Forms.Label lblZandaka;
        private jp.co.fsi.common.controls.FsiTextBox txtZandaka;
        private System.Windows.Forms.Label lblKanjoKamokuNm;
        private System.Windows.Forms.Label lblKanjoKamokuCd;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuCd;
        private System.Windows.Forms.Label lblHojoKamokuNm;
        private System.Windows.Forms.Label lblHojoKamokuCd;
        private jp.co.fsi.common.controls.FsiTextBox txtHojoKamokuCd;
        private System.Windows.Forms.Label lblTantosha;
        private System.Windows.Forms.Label lblTantoshaNm;
        private jp.co.fsi.common.controls.FsiTextBox txtTantoshaCd;
        private System.Windows.Forms.Label lblBakKanjoKamoku;
        private System.Windows.Forms.Label lblBakHojoKamokuCd;
        private System.Windows.Forms.Label lblBakBumonCd;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
    }
}