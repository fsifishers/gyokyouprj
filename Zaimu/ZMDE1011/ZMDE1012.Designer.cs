﻿namespace jp.co.fsi.zm.zmde1011
{
    partial class ZMDE1012
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblDayDenpyoDateTo = new System.Windows.Forms.Label();
            this.txtDayDenpyoDateTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblDenpyoDateTo = new System.Windows.Forms.Label();
            this.lblMonthDenpyoDateTo = new System.Windows.Forms.Label();
            this.lblYearDenpyoDateTo = new System.Windows.Forms.Label();
            this.txtMonthDenpyoDateTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGengoDenpyoDateTo = new System.Windows.Forms.Label();
            this.txtGengoYearDenpyoDateTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblDayDenpyoDateFr = new System.Windows.Forms.Label();
            this.txtDayDenpyoDateFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblDenpyoDateFr = new System.Windows.Forms.Label();
            this.lblMonthDenpyoDateFr = new System.Windows.Forms.Label();
            this.lblYearDenpyoDateFr = new System.Windows.Forms.Label();
            this.txtMonthDenpyoDateFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGengoDenpyoDateFr = new System.Windows.Forms.Label();
            this.txtGengoYearDenpyoDateFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblHojoKamokuNm = new System.Windows.Forms.Label();
            this.lblHojoKamokuCd = new System.Windows.Forms.Label();
            this.txtHojoKamokuCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKanjoKamokuNm = new System.Windows.Forms.Label();
            this.lblKanjoKamokuCd = new System.Windows.Forms.Label();
            this.txtKanjoKamokuCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblBumonNm = new System.Windows.Forms.Label();
            this.lblBumonCd = new System.Windows.Forms.Label();
            this.txtBumonCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblTantosha = new System.Windows.Forms.Label();
            this.lblTantoshaNm = new System.Windows.Forms.Label();
            this.txtTantoshaCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblBakMizuageShisho = new System.Windows.Forms.Label();
            this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMizuageShisho = new System.Windows.Forms.Label();
            this.lblMizuageShishoNm = new System.Windows.Forms.Label();
            this.lblBakTantosha = new System.Windows.Forms.Label();
            this.lblBakKanjoKamoku = new System.Windows.Forms.Label();
            this.lblBakHojoKamokuCd = new System.Windows.Forms.Label();
            this.lblBakBumonCd = new System.Windows.Forms.Label();
            this.lblBakDenpyoDate = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Text = "入力範囲指定";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            // 
            // lblDayDenpyoDateTo
            // 
            this.lblDayDenpyoDateTo.AutoSize = true;
            this.lblDayDenpyoDateTo.BackColor = System.Drawing.Color.Silver;
            this.lblDayDenpyoDateTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDayDenpyoDateTo.Location = new System.Drawing.Point(477, 230);
            this.lblDayDenpyoDateTo.Name = "lblDayDenpyoDateTo";
            this.lblDayDenpyoDateTo.Size = new System.Drawing.Size(21, 13);
            this.lblDayDenpyoDateTo.TabIndex = 36;
            this.lblDayDenpyoDateTo.Text = "日";
            // 
            // txtDayDenpyoDateTo
            // 
            this.txtDayDenpyoDateTo.AutoSizeFromLength = false;
            this.txtDayDenpyoDateTo.DisplayLength = null;
            this.txtDayDenpyoDateTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDayDenpyoDateTo.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtDayDenpyoDateTo.Location = new System.Drawing.Point(451, 227);
            this.txtDayDenpyoDateTo.MaxLength = 2;
            this.txtDayDenpyoDateTo.Name = "txtDayDenpyoDateTo";
            this.txtDayDenpyoDateTo.Size = new System.Drawing.Size(22, 20);
            this.txtDayDenpyoDateTo.TabIndex = 35;
            this.txtDayDenpyoDateTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDayDenpyoDateTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDayDenpyoDateTo_KeyDown);
            this.txtDayDenpyoDateTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDayDenpyoDateTo_Validating);
            // 
            // lblDenpyoDateTo
            // 
            this.lblDenpyoDateTo.AutoSize = true;
            this.lblDenpyoDateTo.BackColor = System.Drawing.Color.Silver;
            this.lblDenpyoDateTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDenpyoDateTo.Location = new System.Drawing.Point(283, 230);
            this.lblDenpyoDateTo.Name = "lblDenpyoDateTo";
            this.lblDenpyoDateTo.Size = new System.Drawing.Size(21, 13);
            this.lblDenpyoDateTo.TabIndex = 29;
            this.lblDenpyoDateTo.Text = "～";
            // 
            // lblMonthDenpyoDateTo
            // 
            this.lblMonthDenpyoDateTo.AutoSize = true;
            this.lblMonthDenpyoDateTo.BackColor = System.Drawing.Color.Silver;
            this.lblMonthDenpyoDateTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMonthDenpyoDateTo.Location = new System.Drawing.Point(428, 230);
            this.lblMonthDenpyoDateTo.Name = "lblMonthDenpyoDateTo";
            this.lblMonthDenpyoDateTo.Size = new System.Drawing.Size(21, 13);
            this.lblMonthDenpyoDateTo.TabIndex = 34;
            this.lblMonthDenpyoDateTo.Text = "月";
            // 
            // lblYearDenpyoDateTo
            // 
            this.lblYearDenpyoDateTo.AutoSize = true;
            this.lblYearDenpyoDateTo.BackColor = System.Drawing.Color.Silver;
            this.lblYearDenpyoDateTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYearDenpyoDateTo.Location = new System.Drawing.Point(380, 230);
            this.lblYearDenpyoDateTo.Name = "lblYearDenpyoDateTo";
            this.lblYearDenpyoDateTo.Size = new System.Drawing.Size(21, 13);
            this.lblYearDenpyoDateTo.TabIndex = 32;
            this.lblYearDenpyoDateTo.Text = "年";
            // 
            // txtMonthDenpyoDateTo
            // 
            this.txtMonthDenpyoDateTo.AutoSizeFromLength = false;
            this.txtMonthDenpyoDateTo.DisplayLength = null;
            this.txtMonthDenpyoDateTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMonthDenpyoDateTo.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtMonthDenpyoDateTo.Location = new System.Drawing.Point(403, 227);
            this.txtMonthDenpyoDateTo.MaxLength = 2;
            this.txtMonthDenpyoDateTo.Name = "txtMonthDenpyoDateTo";
            this.txtMonthDenpyoDateTo.Size = new System.Drawing.Size(22, 20);
            this.txtMonthDenpyoDateTo.TabIndex = 33;
            this.txtMonthDenpyoDateTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonthDenpyoDateTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthDenpyoDateTo_Validating);
            // 
            // lblGengoDenpyoDateTo
            // 
            this.lblGengoDenpyoDateTo.BackColor = System.Drawing.Color.Silver;
            this.lblGengoDenpyoDateTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengoDenpyoDateTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengoDenpyoDateTo.Location = new System.Drawing.Point(316, 227);
            this.lblGengoDenpyoDateTo.Name = "lblGengoDenpyoDateTo";
            this.lblGengoDenpyoDateTo.Size = new System.Drawing.Size(40, 20);
            this.lblGengoDenpyoDateTo.TabIndex = 30;
            this.lblGengoDenpyoDateTo.Text = "平成";
            this.lblGengoDenpyoDateTo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtGengoYearDenpyoDateTo
            // 
            this.txtGengoYearDenpyoDateTo.AutoSizeFromLength = false;
            this.txtGengoYearDenpyoDateTo.DisplayLength = null;
            this.txtGengoYearDenpyoDateTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGengoYearDenpyoDateTo.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtGengoYearDenpyoDateTo.Location = new System.Drawing.Point(358, 227);
            this.txtGengoYearDenpyoDateTo.MaxLength = 2;
            this.txtGengoYearDenpyoDateTo.Name = "txtGengoYearDenpyoDateTo";
            this.txtGengoYearDenpyoDateTo.Size = new System.Drawing.Size(22, 20);
            this.txtGengoYearDenpyoDateTo.TabIndex = 31;
            this.txtGengoYearDenpyoDateTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGengoYearDenpyoDateTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtGengoYearDenpyoDateTo_Validating);
            // 
            // lblDayDenpyoDateFr
            // 
            this.lblDayDenpyoDateFr.AutoSize = true;
            this.lblDayDenpyoDateFr.BackColor = System.Drawing.Color.Silver;
            this.lblDayDenpyoDateFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDayDenpyoDateFr.Location = new System.Drawing.Point(256, 230);
            this.lblDayDenpyoDateFr.Name = "lblDayDenpyoDateFr";
            this.lblDayDenpyoDateFr.Size = new System.Drawing.Size(21, 13);
            this.lblDayDenpyoDateFr.TabIndex = 28;
            this.lblDayDenpyoDateFr.Text = "日";
            // 
            // txtDayDenpyoDateFr
            // 
            this.txtDayDenpyoDateFr.AutoSizeFromLength = false;
            this.txtDayDenpyoDateFr.DisplayLength = null;
            this.txtDayDenpyoDateFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDayDenpyoDateFr.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtDayDenpyoDateFr.Location = new System.Drawing.Point(230, 227);
            this.txtDayDenpyoDateFr.MaxLength = 2;
            this.txtDayDenpyoDateFr.Name = "txtDayDenpyoDateFr";
            this.txtDayDenpyoDateFr.Size = new System.Drawing.Size(22, 20);
            this.txtDayDenpyoDateFr.TabIndex = 27;
            this.txtDayDenpyoDateFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDayDenpyoDateFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDayDenpyoDateFr_Validating);
            // 
            // lblDenpyoDateFr
            // 
            this.lblDenpyoDateFr.AutoSize = true;
            this.lblDenpyoDateFr.BackColor = System.Drawing.Color.Silver;
            this.lblDenpyoDateFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDenpyoDateFr.Location = new System.Drawing.Point(23, 230);
            this.lblDenpyoDateFr.Name = "lblDenpyoDateFr";
            this.lblDenpyoDateFr.Size = new System.Drawing.Size(63, 13);
            this.lblDenpyoDateFr.TabIndex = 21;
            this.lblDenpyoDateFr.Text = "伝票日付";
            // 
            // lblMonthDenpyoDateFr
            // 
            this.lblMonthDenpyoDateFr.AutoSize = true;
            this.lblMonthDenpyoDateFr.BackColor = System.Drawing.Color.Silver;
            this.lblMonthDenpyoDateFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMonthDenpyoDateFr.Location = new System.Drawing.Point(207, 230);
            this.lblMonthDenpyoDateFr.Name = "lblMonthDenpyoDateFr";
            this.lblMonthDenpyoDateFr.Size = new System.Drawing.Size(21, 13);
            this.lblMonthDenpyoDateFr.TabIndex = 26;
            this.lblMonthDenpyoDateFr.Text = "月";
            // 
            // lblYearDenpyoDateFr
            // 
            this.lblYearDenpyoDateFr.AutoSize = true;
            this.lblYearDenpyoDateFr.BackColor = System.Drawing.Color.Silver;
            this.lblYearDenpyoDateFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYearDenpyoDateFr.Location = new System.Drawing.Point(159, 230);
            this.lblYearDenpyoDateFr.Name = "lblYearDenpyoDateFr";
            this.lblYearDenpyoDateFr.Size = new System.Drawing.Size(21, 13);
            this.lblYearDenpyoDateFr.TabIndex = 24;
            this.lblYearDenpyoDateFr.Text = "年";
            // 
            // txtMonthDenpyoDateFr
            // 
            this.txtMonthDenpyoDateFr.AutoSizeFromLength = false;
            this.txtMonthDenpyoDateFr.DisplayLength = null;
            this.txtMonthDenpyoDateFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMonthDenpyoDateFr.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtMonthDenpyoDateFr.Location = new System.Drawing.Point(182, 227);
            this.txtMonthDenpyoDateFr.MaxLength = 2;
            this.txtMonthDenpyoDateFr.Name = "txtMonthDenpyoDateFr";
            this.txtMonthDenpyoDateFr.Size = new System.Drawing.Size(22, 20);
            this.txtMonthDenpyoDateFr.TabIndex = 25;
            this.txtMonthDenpyoDateFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonthDenpyoDateFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthDenpyoDateFr_Validating);
            // 
            // lblGengoDenpyoDateFr
            // 
            this.lblGengoDenpyoDateFr.BackColor = System.Drawing.Color.Silver;
            this.lblGengoDenpyoDateFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengoDenpyoDateFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengoDenpyoDateFr.Location = new System.Drawing.Point(95, 227);
            this.lblGengoDenpyoDateFr.Name = "lblGengoDenpyoDateFr";
            this.lblGengoDenpyoDateFr.Size = new System.Drawing.Size(40, 20);
            this.lblGengoDenpyoDateFr.TabIndex = 22;
            this.lblGengoDenpyoDateFr.Text = "平成";
            this.lblGengoDenpyoDateFr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtGengoYearDenpyoDateFr
            // 
            this.txtGengoYearDenpyoDateFr.AutoSizeFromLength = false;
            this.txtGengoYearDenpyoDateFr.DisplayLength = null;
            this.txtGengoYearDenpyoDateFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGengoYearDenpyoDateFr.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtGengoYearDenpyoDateFr.Location = new System.Drawing.Point(137, 227);
            this.txtGengoYearDenpyoDateFr.MaxLength = 2;
            this.txtGengoYearDenpyoDateFr.Name = "txtGengoYearDenpyoDateFr";
            this.txtGengoYearDenpyoDateFr.Size = new System.Drawing.Size(22, 20);
            this.txtGengoYearDenpyoDateFr.TabIndex = 23;
            this.txtGengoYearDenpyoDateFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGengoYearDenpyoDateFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtGengoYearDenpyoDateFr_Validating);
            // 
            // lblHojoKamokuNm
            // 
            this.lblHojoKamokuNm.BackColor = System.Drawing.Color.Silver;
            this.lblHojoKamokuNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblHojoKamokuNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblHojoKamokuNm.Location = new System.Drawing.Point(148, 162);
            this.lblHojoKamokuNm.Name = "lblHojoKamokuNm";
            this.lblHojoKamokuNm.Size = new System.Drawing.Size(356, 21);
            this.lblHojoKamokuNm.TabIndex = 16;
            this.lblHojoKamokuNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblHojoKamokuCd
            // 
            this.lblHojoKamokuCd.AutoSize = true;
            this.lblHojoKamokuCd.BackColor = System.Drawing.Color.Silver;
            this.lblHojoKamokuCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblHojoKamokuCd.Location = new System.Drawing.Point(22, 166);
            this.lblHojoKamokuCd.Name = "lblHojoKamokuCd";
            this.lblHojoKamokuCd.Size = new System.Drawing.Size(63, 13);
            this.lblHojoKamokuCd.TabIndex = 5;
            this.lblHojoKamokuCd.Text = "補助科目";
            // 
            // txtHojoKamokuCd
            // 
            this.txtHojoKamokuCd.AutoSizeFromLength = false;
            this.txtHojoKamokuCd.BackColor = System.Drawing.SystemColors.Window;
            this.txtHojoKamokuCd.DisplayLength = null;
            this.txtHojoKamokuCd.Enabled = false;
            this.txtHojoKamokuCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtHojoKamokuCd.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtHojoKamokuCd.Location = new System.Drawing.Point(94, 161);
            this.txtHojoKamokuCd.MaxLength = 4;
            this.txtHojoKamokuCd.Name = "txtHojoKamokuCd";
            this.txtHojoKamokuCd.Size = new System.Drawing.Size(53, 23);
            this.txtHojoKamokuCd.TabIndex = 15;
            this.txtHojoKamokuCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtHojoKamokuCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtHojoKamokuCd_Validating);
            // 
            // lblKanjoKamokuNm
            // 
            this.lblKanjoKamokuNm.BackColor = System.Drawing.Color.Silver;
            this.lblKanjoKamokuNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKanjoKamokuNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKanjoKamokuNm.Location = new System.Drawing.Point(148, 129);
            this.lblKanjoKamokuNm.Name = "lblKanjoKamokuNm";
            this.lblKanjoKamokuNm.Size = new System.Drawing.Size(356, 21);
            this.lblKanjoKamokuNm.TabIndex = 12;
            this.lblKanjoKamokuNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKanjoKamokuCd
            // 
            this.lblKanjoKamokuCd.AutoSize = true;
            this.lblKanjoKamokuCd.BackColor = System.Drawing.Color.Silver;
            this.lblKanjoKamokuCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKanjoKamokuCd.Location = new System.Drawing.Point(22, 133);
            this.lblKanjoKamokuCd.Name = "lblKanjoKamokuCd";
            this.lblKanjoKamokuCd.Size = new System.Drawing.Size(63, 13);
            this.lblKanjoKamokuCd.TabIndex = 5;
            this.lblKanjoKamokuCd.Text = "勘定科目";
            // 
            // txtKanjoKamokuCd
            // 
            this.txtKanjoKamokuCd.AutoSizeFromLength = false;
            this.txtKanjoKamokuCd.BackColor = System.Drawing.SystemColors.Window;
            this.txtKanjoKamokuCd.DisplayLength = null;
            this.txtKanjoKamokuCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKanjoKamokuCd.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKanjoKamokuCd.Location = new System.Drawing.Point(94, 128);
            this.txtKanjoKamokuCd.MaxLength = 6;
            this.txtKanjoKamokuCd.Name = "txtKanjoKamokuCd";
            this.txtKanjoKamokuCd.Size = new System.Drawing.Size(53, 23);
            this.txtKanjoKamokuCd.TabIndex = 11;
            this.txtKanjoKamokuCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKanjoKamokuCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtKanjoKamokuCd_Validating);
            // 
            // lblBumonNm
            // 
            this.lblBumonNm.BackColor = System.Drawing.Color.Silver;
            this.lblBumonNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBumonNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBumonNm.Location = new System.Drawing.Point(148, 194);
            this.lblBumonNm.Name = "lblBumonNm";
            this.lblBumonNm.Size = new System.Drawing.Size(356, 21);
            this.lblBumonNm.TabIndex = 19;
            this.lblBumonNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblBumonCd
            // 
            this.lblBumonCd.AutoSize = true;
            this.lblBumonCd.BackColor = System.Drawing.Color.Silver;
            this.lblBumonCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBumonCd.Location = new System.Drawing.Point(21, 198);
            this.lblBumonCd.Name = "lblBumonCd";
            this.lblBumonCd.Size = new System.Drawing.Size(35, 13);
            this.lblBumonCd.TabIndex = 5;
            this.lblBumonCd.Text = "部門";
            // 
            // txtBumonCd
            // 
            this.txtBumonCd.AutoSizeFromLength = false;
            this.txtBumonCd.BackColor = System.Drawing.SystemColors.Window;
            this.txtBumonCd.DisplayLength = null;
            this.txtBumonCd.Enabled = false;
            this.txtBumonCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtBumonCd.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtBumonCd.Location = new System.Drawing.Point(93, 193);
            this.txtBumonCd.MaxLength = 4;
            this.txtBumonCd.Name = "txtBumonCd";
            this.txtBumonCd.Size = new System.Drawing.Size(53, 23);
            this.txtBumonCd.TabIndex = 18;
            this.txtBumonCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBumonCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtBumonCd_Validating);
            // 
            // lblTantosha
            // 
            this.lblTantosha.AutoSize = true;
            this.lblTantosha.BackColor = System.Drawing.Color.Silver;
            this.lblTantosha.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTantosha.Location = new System.Drawing.Point(23, 100);
            this.lblTantosha.Name = "lblTantosha";
            this.lblTantosha.Size = new System.Drawing.Size(49, 13);
            this.lblTantosha.TabIndex = 6;
            this.lblTantosha.Text = "担当者";
            // 
            // lblTantoshaNm
            // 
            this.lblTantoshaNm.BackColor = System.Drawing.Color.Silver;
            this.lblTantoshaNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTantoshaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTantoshaNm.Location = new System.Drawing.Point(148, 96);
            this.lblTantoshaNm.Name = "lblTantoshaNm";
            this.lblTantoshaNm.Size = new System.Drawing.Size(356, 20);
            this.lblTantoshaNm.TabIndex = 8;
            this.lblTantoshaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTantoshaCd
            // 
            this.txtTantoshaCd.AutoSizeFromLength = false;
            this.txtTantoshaCd.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtTantoshaCd.DisplayLength = null;
            this.txtTantoshaCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTantoshaCd.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtTantoshaCd.Location = new System.Drawing.Point(94, 95);
            this.txtTantoshaCd.MaxLength = 4;
            this.txtTantoshaCd.Name = "txtTantoshaCd";
            this.txtTantoshaCd.Size = new System.Drawing.Size(53, 23);
            this.txtTantoshaCd.TabIndex = 7;
            this.txtTantoshaCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTantoshaCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtTantoshaCd_Validating);
            // 
            // lblBakMizuageShisho
            // 
            this.lblBakMizuageShisho.BackColor = System.Drawing.Color.Silver;
            this.lblBakMizuageShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBakMizuageShisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblBakMizuageShisho.Location = new System.Drawing.Point(17, 57);
            this.lblBakMizuageShisho.Name = "lblBakMizuageShisho";
            this.lblBakMizuageShisho.Size = new System.Drawing.Size(494, 32);
            this.lblBakMizuageShisho.TabIndex = 2;
            this.lblBakMizuageShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtMizuageShishoCd
            // 
            this.txtMizuageShishoCd.AutoSizeFromLength = false;
            this.txtMizuageShishoCd.BackColor = System.Drawing.SystemColors.Window;
            this.txtMizuageShishoCd.DisplayLength = null;
            this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtMizuageShishoCd.Location = new System.Drawing.Point(94, 62);
            this.txtMizuageShishoCd.MaxLength = 4;
            this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
            this.txtMizuageShishoCd.Size = new System.Drawing.Size(53, 23);
            this.txtMizuageShishoCd.TabIndex = 3;
            this.txtMizuageShishoCd.TabStop = false;
            this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
            // 
            // lblMizuageShisho
            // 
            this.lblMizuageShisho.AutoSize = true;
            this.lblMizuageShisho.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMizuageShisho.Location = new System.Drawing.Point(23, 67);
            this.lblMizuageShisho.Name = "lblMizuageShisho";
            this.lblMizuageShisho.Size = new System.Drawing.Size(35, 13);
            this.lblMizuageShisho.TabIndex = 5;
            this.lblMizuageShisho.Text = "支所";
            // 
            // lblMizuageShishoNm
            // 
            this.lblMizuageShishoNm.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMizuageShishoNm.Location = new System.Drawing.Point(148, 63);
            this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
            this.lblMizuageShishoNm.Size = new System.Drawing.Size(356, 20);
            this.lblMizuageShishoNm.TabIndex = 4;
            this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblBakTantosha
            // 
            this.lblBakTantosha.BackColor = System.Drawing.Color.Silver;
            this.lblBakTantosha.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBakTantosha.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblBakTantosha.Location = new System.Drawing.Point(17, 90);
            this.lblBakTantosha.Name = "lblBakTantosha";
            this.lblBakTantosha.Size = new System.Drawing.Size(494, 32);
            this.lblBakTantosha.TabIndex = 5;
            this.lblBakTantosha.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblBakKanjoKamoku
            // 
            this.lblBakKanjoKamoku.BackColor = System.Drawing.Color.Silver;
            this.lblBakKanjoKamoku.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBakKanjoKamoku.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblBakKanjoKamoku.Location = new System.Drawing.Point(17, 123);
            this.lblBakKanjoKamoku.Name = "lblBakKanjoKamoku";
            this.lblBakKanjoKamoku.Size = new System.Drawing.Size(494, 32);
            this.lblBakKanjoKamoku.TabIndex = 10;
            this.lblBakKanjoKamoku.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblBakHojoKamokuCd
            // 
            this.lblBakHojoKamokuCd.BackColor = System.Drawing.Color.Silver;
            this.lblBakHojoKamokuCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBakHojoKamokuCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblBakHojoKamokuCd.Location = new System.Drawing.Point(17, 156);
            this.lblBakHojoKamokuCd.Name = "lblBakHojoKamokuCd";
            this.lblBakHojoKamokuCd.Size = new System.Drawing.Size(494, 32);
            this.lblBakHojoKamokuCd.TabIndex = 14;
            this.lblBakHojoKamokuCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblBakBumonCd
            // 
            this.lblBakBumonCd.BackColor = System.Drawing.Color.Silver;
            this.lblBakBumonCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBakBumonCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblBakBumonCd.Location = new System.Drawing.Point(17, 189);
            this.lblBakBumonCd.Name = "lblBakBumonCd";
            this.lblBakBumonCd.Size = new System.Drawing.Size(494, 32);
            this.lblBakBumonCd.TabIndex = 17;
            this.lblBakBumonCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblBakDenpyoDate
            // 
            this.lblBakDenpyoDate.BackColor = System.Drawing.Color.Silver;
            this.lblBakDenpyoDate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBakDenpyoDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblBakDenpyoDate.Location = new System.Drawing.Point(17, 222);
            this.lblBakDenpyoDate.Name = "lblBakDenpyoDate";
            this.lblBakDenpyoDate.Size = new System.Drawing.Size(494, 32);
            this.lblBakDenpyoDate.TabIndex = 20;
            this.lblBakDenpyoDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ZMDE1012
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.lblDayDenpyoDateTo);
            this.Controls.Add(this.txtDayDenpyoDateTo);
            this.Controls.Add(this.lblBumonNm);
            this.Controls.Add(this.lblDenpyoDateTo);
            this.Controls.Add(this.lblBumonCd);
            this.Controls.Add(this.lblMonthDenpyoDateTo);
            this.Controls.Add(this.txtBumonCd);
            this.Controls.Add(this.lblYearDenpyoDateTo);
            this.Controls.Add(this.lblBakBumonCd);
            this.Controls.Add(this.txtMonthDenpyoDateTo);
            this.Controls.Add(this.lblHojoKamokuNm);
            this.Controls.Add(this.lblGengoDenpyoDateTo);
            this.Controls.Add(this.lblHojoKamokuCd);
            this.Controls.Add(this.txtGengoYearDenpyoDateTo);
            this.Controls.Add(this.txtHojoKamokuCd);
            this.Controls.Add(this.lblDayDenpyoDateFr);
            this.Controls.Add(this.txtDayDenpyoDateFr);
            this.Controls.Add(this.lblBakHojoKamokuCd);
            this.Controls.Add(this.lblDenpyoDateFr);
            this.Controls.Add(this.lblKanjoKamokuNm);
            this.Controls.Add(this.lblMonthDenpyoDateFr);
            this.Controls.Add(this.lblKanjoKamokuCd);
            this.Controls.Add(this.lblYearDenpyoDateFr);
            this.Controls.Add(this.txtKanjoKamokuCd);
            this.Controls.Add(this.txtMonthDenpyoDateFr);
            this.Controls.Add(this.lblTantosha);
            this.Controls.Add(this.lblGengoDenpyoDateFr);
            this.Controls.Add(this.lblTantoshaNm);
            this.Controls.Add(this.txtGengoYearDenpyoDateFr);
            this.Controls.Add(this.txtTantoshaCd);
            this.Controls.Add(this.lblMizuageShishoNm);
            this.Controls.Add(this.lblMizuageShisho);
            this.Controls.Add(this.txtMizuageShishoCd);
            this.Controls.Add(this.lblBakMizuageShisho);
            this.Controls.Add(this.lblBakTantosha);
            this.Controls.Add(this.lblBakKanjoKamoku);
            this.Controls.Add(this.lblBakDenpyoDate);
            this.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.Name = "ZMDE1012";
            this.Text = "入力範囲指定";
            this.Shown += new System.EventHandler(this.ZMDE1012_Shown);
            this.Controls.SetChildIndex(this.lblBakDenpyoDate, 0);
            this.Controls.SetChildIndex(this.lblBakKanjoKamoku, 0);
            this.Controls.SetChildIndex(this.lblBakTantosha, 0);
            this.Controls.SetChildIndex(this.lblBakMizuageShisho, 0);
            this.Controls.SetChildIndex(this.txtMizuageShishoCd, 0);
            this.Controls.SetChildIndex(this.lblMizuageShisho, 0);
            this.Controls.SetChildIndex(this.lblMizuageShishoNm, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.txtTantoshaCd, 0);
            this.Controls.SetChildIndex(this.txtGengoYearDenpyoDateFr, 0);
            this.Controls.SetChildIndex(this.lblTantoshaNm, 0);
            this.Controls.SetChildIndex(this.lblGengoDenpyoDateFr, 0);
            this.Controls.SetChildIndex(this.lblTantosha, 0);
            this.Controls.SetChildIndex(this.txtMonthDenpyoDateFr, 0);
            this.Controls.SetChildIndex(this.txtKanjoKamokuCd, 0);
            this.Controls.SetChildIndex(this.lblYearDenpyoDateFr, 0);
            this.Controls.SetChildIndex(this.lblKanjoKamokuCd, 0);
            this.Controls.SetChildIndex(this.lblMonthDenpyoDateFr, 0);
            this.Controls.SetChildIndex(this.lblKanjoKamokuNm, 0);
            this.Controls.SetChildIndex(this.lblDenpyoDateFr, 0);
            this.Controls.SetChildIndex(this.lblBakHojoKamokuCd, 0);
            this.Controls.SetChildIndex(this.txtDayDenpyoDateFr, 0);
            this.Controls.SetChildIndex(this.lblDayDenpyoDateFr, 0);
            this.Controls.SetChildIndex(this.txtHojoKamokuCd, 0);
            this.Controls.SetChildIndex(this.txtGengoYearDenpyoDateTo, 0);
            this.Controls.SetChildIndex(this.lblHojoKamokuCd, 0);
            this.Controls.SetChildIndex(this.lblGengoDenpyoDateTo, 0);
            this.Controls.SetChildIndex(this.lblHojoKamokuNm, 0);
            this.Controls.SetChildIndex(this.txtMonthDenpyoDateTo, 0);
            this.Controls.SetChildIndex(this.lblBakBumonCd, 0);
            this.Controls.SetChildIndex(this.lblYearDenpyoDateTo, 0);
            this.Controls.SetChildIndex(this.txtBumonCd, 0);
            this.Controls.SetChildIndex(this.lblMonthDenpyoDateTo, 0);
            this.Controls.SetChildIndex(this.lblBumonCd, 0);
            this.Controls.SetChildIndex(this.lblDenpyoDateTo, 0);
            this.Controls.SetChildIndex(this.lblBumonNm, 0);
            this.Controls.SetChildIndex(this.txtDayDenpyoDateTo, 0);
            this.Controls.SetChildIndex(this.lblDayDenpyoDateTo, 0);
            this.pnlDebug.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblDayDenpyoDateTo;
        private jp.co.fsi.common.controls.FsiTextBox txtDayDenpyoDateTo;
        private System.Windows.Forms.Label lblDenpyoDateTo;
        private System.Windows.Forms.Label lblMonthDenpyoDateTo;
        private System.Windows.Forms.Label lblYearDenpyoDateTo;
        private jp.co.fsi.common.controls.FsiTextBox txtMonthDenpyoDateTo;
        private System.Windows.Forms.Label lblGengoDenpyoDateTo;
        private jp.co.fsi.common.controls.FsiTextBox txtGengoYearDenpyoDateTo;
        private System.Windows.Forms.Label lblDayDenpyoDateFr;
        private jp.co.fsi.common.controls.FsiTextBox txtDayDenpyoDateFr;
        private System.Windows.Forms.Label lblDenpyoDateFr;
        private System.Windows.Forms.Label lblMonthDenpyoDateFr;
        private System.Windows.Forms.Label lblYearDenpyoDateFr;
        private jp.co.fsi.common.controls.FsiTextBox txtMonthDenpyoDateFr;
        private System.Windows.Forms.Label lblGengoDenpyoDateFr;
        private jp.co.fsi.common.controls.FsiTextBox txtGengoYearDenpyoDateFr;
        private System.Windows.Forms.Label lblHojoKamokuNm;
        private System.Windows.Forms.Label lblHojoKamokuCd;
        private jp.co.fsi.common.controls.FsiTextBox txtHojoKamokuCd;
        private System.Windows.Forms.Label lblKanjoKamokuNm;
        private System.Windows.Forms.Label lblKanjoKamokuCd;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuCd;
        private System.Windows.Forms.Label lblBumonNm;
        private System.Windows.Forms.Label lblBumonCd;
        private jp.co.fsi.common.controls.FsiTextBox txtBumonCd;
        private System.Windows.Forms.Label lblTantosha;
        private System.Windows.Forms.Label lblTantoshaNm;
        private jp.co.fsi.common.controls.FsiTextBox txtTantoshaCd;
        private System.Windows.Forms.Label lblBakMizuageShisho;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShisho;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.Label lblBakTantosha;
        private System.Windows.Forms.Label lblBakKanjoKamoku;
        private System.Windows.Forms.Label lblBakHojoKamokuCd;
        private System.Windows.Forms.Label lblBakBumonCd;
        private System.Windows.Forms.Label lblBakDenpyoDate;
    }
}