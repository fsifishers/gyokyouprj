﻿namespace jp.co.fsi.zm.zmyr1031
{
    /// <summary>
    /// ZMYR1031R の概要の説明です。
    /// </summary>
    partial class ZMYR1031R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(ZMYR1031R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.txtToday = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtPageCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtRitsu5Shohizei = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtRitsu8Shohizei = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtRitsu5Zeinuki = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtRitsu8Zeinuki = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtRitsu10Zeinuki = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtRitsu10Shohizei = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtRitsu5Kei = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtRitsu10Kei = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtRitsu8Kei = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtRitsu0Zeinuki = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtRitsu3Shohizei = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtRitsu0Kei = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtRitsu3Kei = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtRitsu0Shohizei = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtRitsu3Zeinuki = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtRitsu10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtRitsu5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtRitsu8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtRitsu0 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtRitsu3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtZeiKubunNo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line19 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line20 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line21 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line22 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtZeiKubunNm = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.txtTitle02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle04 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle05 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle06 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.groupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.groupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRitsu5Shohizei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRitsu8Shohizei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRitsu5Zeinuki)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRitsu8Zeinuki)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRitsu10Zeinuki)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRitsu10Shohizei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRitsu5Kei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRitsu10Kei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRitsu8Kei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRitsu0Zeinuki)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRitsu3Shohizei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRitsu0Kei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRitsu3Kei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRitsu0Shohizei)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRitsu3Zeinuki)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRitsu10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRitsu5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRitsu8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRitsu0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRitsu3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZeiKubunNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZeiKubunNm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle04)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle05)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle06)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtToday,
            this.lblPage,
            this.txtPageCount,
            this.txtTitle,
            this.txtDate,
            this.line1});
            this.pageHeader.Height = 0.4897308F;
            this.pageHeader.Name = "pageHeader";
            this.pageHeader.Format += new System.EventHandler(this.pageHeader_Format);
            // 
            // txtToday
            // 
            this.txtToday.Height = 0.1968504F;
            this.txtToday.Left = 9.351969F;
            this.txtToday.MultiLine = false;
            this.txtToday.Name = "txtToday";
            this.txtToday.OutputFormat = resources.GetString("txtToday.OutputFormat");
            this.txtToday.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle";
            this.txtToday.Text = "yyyy-MM-dd";
            this.txtToday.Top = 0.2893701F;
            this.txtToday.Width = 0.816535F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.1968504F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 10.22677F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle";
            this.lblPage.Text = "Page:";
            this.lblPage.Top = 0.2893701F;
            this.lblPage.Width = 0.4299212F;
            // 
            // txtPageCount
            // 
            this.txtPageCount.Height = 0.1968504F;
            this.txtPageCount.Left = 10.71063F;
            this.txtPageCount.MultiLine = false;
            this.txtPageCount.Name = "txtPageCount";
            this.txtPageCount.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle";
            this.txtPageCount.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtPageCount.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.txtPageCount.Text = "1";
            this.txtPageCount.Top = 0.2893701F;
            this.txtPageCount.Width = 0.1909451F;
            // 
            // txtTitle
            // 
            this.txtTitle.DataField = "ITEM01";
            this.txtTitle.Height = 0.25F;
            this.txtTitle.Left = 4.473622F;
            this.txtTitle.MultiLine = false;
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Style = "font-family: ＭＳ ゴシック; font-size: 14.25pt; font-weight: normal; text-align: center" +
    "; vertical-align: middle";
            this.txtTitle.Text = null;
            this.txtTitle.Top = 0F;
            this.txtTitle.Width = 2.100394F;
            // 
            // txtDate
            // 
            this.txtDate.DataField = "ITEM02";
            this.txtDate.Height = 0.1968504F;
            this.txtDate.Left = 0F;
            this.txtDate.MultiLine = false;
            this.txtDate.Name = "txtDate";
            this.txtDate.Style = "font-family: ＭＳ 明朝; font-size: 9pt; font-weight: normal; text-align: left; vertic" +
    "al-align: middle";
            this.txtDate.Text = null;
            this.txtDate.Top = 0.2893701F;
            this.txtDate.Width = 3.579528F;
            // 
            // line1
            // 
            this.line1.Height = 0F;
            this.line1.Left = 4.473622F;
            this.line1.LineWeight = 1F;
            this.line1.Name = "line1";
            this.line1.Top = 0.2480315F;
            this.line1.Width = 2.101181F;
            this.line1.X1 = 4.473622F;
            this.line1.X2 = 6.574803F;
            this.line1.Y1 = 0.2480315F;
            this.line1.Y2 = 0.2480315F;
            // 
            // detail
            // 
            this.detail.ColumnCount = 2;
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtRitsu5Shohizei,
            this.txtRitsu8Shohizei,
            this.txtRitsu5Zeinuki,
            this.txtRitsu8Zeinuki,
            this.txtRitsu10Zeinuki,
            this.txtRitsu10Shohizei,
            this.txtRitsu5Kei,
            this.txtRitsu10Kei,
            this.txtRitsu8Kei,
            this.txtRitsu0Zeinuki,
            this.txtRitsu3Shohizei,
            this.txtRitsu0Kei,
            this.txtRitsu3Kei,
            this.txtRitsu0Shohizei,
            this.txtRitsu3Zeinuki,
            this.txtRitsu10,
            this.txtRitsu5,
            this.txtRitsu8,
            this.txtRitsu0,
            this.txtRitsu3,
            this.txtZeiKubunNo,
            this.line10,
            this.line12,
            this.line13,
            this.line14,
            this.line15,
            this.line18,
            this.line19,
            this.line20,
            this.line21,
            this.line22,
            this.line2,
            this.txtZeiKubunNm,
            this.line11});
            this.detail.Height = 0.7893702F;
            this.detail.Name = "detail";
            // 
            // txtRitsu5Shohizei
            // 
            this.txtRitsu5Shohizei.DataField = "ITEM15";
            this.txtRitsu5Shohizei.Height = 0.1574803F;
            this.txtRitsu5Shohizei.Left = 3.643307F;
            this.txtRitsu5Shohizei.Name = "txtRitsu5Shohizei";
            this.txtRitsu5Shohizei.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; " +
    "vertical-align: middle";
            this.txtRitsu5Shohizei.Text = null;
            this.txtRitsu5Shohizei.Top = 0.3149607F;
            this.txtRitsu5Shohizei.Width = 0.8228357F;
            // 
            // txtRitsu8Shohizei
            // 
            this.txtRitsu8Shohizei.DataField = "ITEM19";
            this.txtRitsu8Shohizei.Height = 0.1574803F;
            this.txtRitsu8Shohizei.Left = 3.643307F;
            this.txtRitsu8Shohizei.Name = "txtRitsu8Shohizei";
            this.txtRitsu8Shohizei.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; " +
    "vertical-align: middle";
            this.txtRitsu8Shohizei.Text = null;
            this.txtRitsu8Shohizei.Top = 0.472441F;
            this.txtRitsu8Shohizei.Width = 0.8228357F;
            // 
            // txtRitsu5Zeinuki
            // 
            this.txtRitsu5Zeinuki.DataField = "ITEM14";
            this.txtRitsu5Zeinuki.Height = 0.1574803F;
            this.txtRitsu5Zeinuki.Left = 2.705906F;
            this.txtRitsu5Zeinuki.Name = "txtRitsu5Zeinuki";
            this.txtRitsu5Zeinuki.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; " +
    "vertical-align: middle";
            this.txtRitsu5Zeinuki.Text = null;
            this.txtRitsu5Zeinuki.Top = 0.3149607F;
            this.txtRitsu5Zeinuki.Width = 0.8228362F;
            // 
            // txtRitsu8Zeinuki
            // 
            this.txtRitsu8Zeinuki.DataField = "ITEM18";
            this.txtRitsu8Zeinuki.Height = 0.1574803F;
            this.txtRitsu8Zeinuki.Left = 2.705906F;
            this.txtRitsu8Zeinuki.Name = "txtRitsu8Zeinuki";
            this.txtRitsu8Zeinuki.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; " +
    "vertical-align: middle";
            this.txtRitsu8Zeinuki.Text = null;
            this.txtRitsu8Zeinuki.Top = 0.472441F;
            this.txtRitsu8Zeinuki.Width = 0.8228362F;
            // 
            // txtRitsu10Zeinuki
            // 
            this.txtRitsu10Zeinuki.DataField = "ITEM22";
            this.txtRitsu10Zeinuki.Height = 0.1574803F;
            this.txtRitsu10Zeinuki.Left = 2.705906F;
            this.txtRitsu10Zeinuki.Name = "txtRitsu10Zeinuki";
            this.txtRitsu10Zeinuki.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; " +
    "vertical-align: middle";
            this.txtRitsu10Zeinuki.Text = null;
            this.txtRitsu10Zeinuki.Top = 0.6299213F;
            this.txtRitsu10Zeinuki.Width = 0.8228362F;
            // 
            // txtRitsu10Shohizei
            // 
            this.txtRitsu10Shohizei.DataField = "ITEM23";
            this.txtRitsu10Shohizei.Height = 0.1574803F;
            this.txtRitsu10Shohizei.Left = 3.643307F;
            this.txtRitsu10Shohizei.Name = "txtRitsu10Shohizei";
            this.txtRitsu10Shohizei.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; " +
    "vertical-align: middle";
            this.txtRitsu10Shohizei.Text = null;
            this.txtRitsu10Shohizei.Top = 0.6299213F;
            this.txtRitsu10Shohizei.Width = 0.8228357F;
            // 
            // txtRitsu5Kei
            // 
            this.txtRitsu5Kei.DataField = "ITEM16";
            this.txtRitsu5Kei.Height = 0.1574803F;
            this.txtRitsu5Kei.Left = 4.54252F;
            this.txtRitsu5Kei.Name = "txtRitsu5Kei";
            this.txtRitsu5Kei.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; " +
    "vertical-align: middle";
            this.txtRitsu5Kei.Text = null;
            this.txtRitsu5Kei.Top = 0.3149607F;
            this.txtRitsu5Kei.Width = 0.8228359F;
            // 
            // txtRitsu10Kei
            // 
            this.txtRitsu10Kei.DataField = "ITEM24";
            this.txtRitsu10Kei.Height = 0.1574803F;
            this.txtRitsu10Kei.Left = 4.54252F;
            this.txtRitsu10Kei.Name = "txtRitsu10Kei";
            this.txtRitsu10Kei.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; " +
    "vertical-align: middle";
            this.txtRitsu10Kei.Text = null;
            this.txtRitsu10Kei.Top = 0.6318898F;
            this.txtRitsu10Kei.Width = 0.8228359F;
            // 
            // txtRitsu8Kei
            // 
            this.txtRitsu8Kei.DataField = "ITEM20";
            this.txtRitsu8Kei.Height = 0.1574803F;
            this.txtRitsu8Kei.Left = 4.54252F;
            this.txtRitsu8Kei.Name = "txtRitsu8Kei";
            this.txtRitsu8Kei.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; " +
    "vertical-align: middle";
            this.txtRitsu8Kei.Text = null;
            this.txtRitsu8Kei.Top = 0.4744095F;
            this.txtRitsu8Kei.Width = 0.8228359F;
            // 
            // txtRitsu0Zeinuki
            // 
            this.txtRitsu0Zeinuki.DataField = "ITEM06";
            this.txtRitsu0Zeinuki.Height = 0.1574803F;
            this.txtRitsu0Zeinuki.Left = 2.705906F;
            this.txtRitsu0Zeinuki.Name = "txtRitsu0Zeinuki";
            this.txtRitsu0Zeinuki.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; " +
    "vertical-align: middle";
            this.txtRitsu0Zeinuki.Text = null;
            this.txtRitsu0Zeinuki.Top = 0F;
            this.txtRitsu0Zeinuki.Width = 0.8228362F;
            // 
            // txtRitsu3Shohizei
            // 
            this.txtRitsu3Shohizei.DataField = "ITEM11";
            this.txtRitsu3Shohizei.Height = 0.1574803F;
            this.txtRitsu3Shohizei.Left = 3.643307F;
            this.txtRitsu3Shohizei.Name = "txtRitsu3Shohizei";
            this.txtRitsu3Shohizei.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; " +
    "vertical-align: middle";
            this.txtRitsu3Shohizei.Text = null;
            this.txtRitsu3Shohizei.Top = 0.1574803F;
            this.txtRitsu3Shohizei.Width = 0.8228357F;
            // 
            // txtRitsu0Kei
            // 
            this.txtRitsu0Kei.DataField = "ITEM08";
            this.txtRitsu0Kei.Height = 0.1574803F;
            this.txtRitsu0Kei.Left = 4.54252F;
            this.txtRitsu0Kei.Name = "txtRitsu0Kei";
            this.txtRitsu0Kei.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; " +
    "vertical-align: middle";
            this.txtRitsu0Kei.Text = null;
            this.txtRitsu0Kei.Top = 0F;
            this.txtRitsu0Kei.Width = 0.8228346F;
            // 
            // txtRitsu3Kei
            // 
            this.txtRitsu3Kei.DataField = "ITEM12";
            this.txtRitsu3Kei.Height = 0.1574803F;
            this.txtRitsu3Kei.Left = 4.54252F;
            this.txtRitsu3Kei.Name = "txtRitsu3Kei";
            this.txtRitsu3Kei.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; " +
    "vertical-align: middle";
            this.txtRitsu3Kei.Text = null;
            this.txtRitsu3Kei.Top = 0.1574803F;
            this.txtRitsu3Kei.Width = 0.8228359F;
            // 
            // txtRitsu0Shohizei
            // 
            this.txtRitsu0Shohizei.DataField = "ITEM07";
            this.txtRitsu0Shohizei.Height = 0.1574803F;
            this.txtRitsu0Shohizei.Left = 3.643307F;
            this.txtRitsu0Shohizei.Name = "txtRitsu0Shohizei";
            this.txtRitsu0Shohizei.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; " +
    "vertical-align: middle";
            this.txtRitsu0Shohizei.Text = null;
            this.txtRitsu0Shohizei.Top = 0F;
            this.txtRitsu0Shohizei.Width = 0.8228346F;
            // 
            // txtRitsu3Zeinuki
            // 
            this.txtRitsu3Zeinuki.DataField = "ITEM10";
            this.txtRitsu3Zeinuki.Height = 0.1574803F;
            this.txtRitsu3Zeinuki.Left = 2.705906F;
            this.txtRitsu3Zeinuki.Name = "txtRitsu3Zeinuki";
            this.txtRitsu3Zeinuki.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; " +
    "vertical-align: middle";
            this.txtRitsu3Zeinuki.Text = null;
            this.txtRitsu3Zeinuki.Top = 0.1574803F;
            this.txtRitsu3Zeinuki.Width = 0.8228362F;
            // 
            // txtRitsu10
            // 
            this.txtRitsu10.DataField = "ITEM21";
            this.txtRitsu10.Height = 0.1574803F;
            this.txtRitsu10.Left = 2.325197F;
            this.txtRitsu10.Name = "txtRitsu10";
            this.txtRitsu10.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; " +
    "vertical-align: middle";
            this.txtRitsu10.Text = null;
            this.txtRitsu10.Top = 0.6318898F;
            this.txtRitsu10.Width = 0.3334644F;
            // 
            // txtRitsu5
            // 
            this.txtRitsu5.DataField = "ITEM13";
            this.txtRitsu5.Height = 0.1574803F;
            this.txtRitsu5.Left = 2.325197F;
            this.txtRitsu5.Name = "txtRitsu5";
            this.txtRitsu5.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; " +
    "vertical-align: middle";
            this.txtRitsu5.Text = null;
            this.txtRitsu5.Top = 0.3149607F;
            this.txtRitsu5.Width = 0.3334644F;
            // 
            // txtRitsu8
            // 
            this.txtRitsu8.DataField = "ITEM17";
            this.txtRitsu8.Height = 0.1574803F;
            this.txtRitsu8.Left = 2.325197F;
            this.txtRitsu8.Name = "txtRitsu8";
            this.txtRitsu8.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; " +
    "vertical-align: middle";
            this.txtRitsu8.Text = null;
            this.txtRitsu8.Top = 0.472441F;
            this.txtRitsu8.Width = 0.3334644F;
            // 
            // txtRitsu0
            // 
            this.txtRitsu0.DataField = "ITEM05";
            this.txtRitsu0.Height = 0.1574803F;
            this.txtRitsu0.Left = 2.325197F;
            this.txtRitsu0.Name = "txtRitsu0";
            this.txtRitsu0.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; " +
    "vertical-align: middle";
            this.txtRitsu0.Text = null;
            this.txtRitsu0.Top = 0F;
            this.txtRitsu0.Width = 0.3334646F;
            // 
            // txtRitsu3
            // 
            this.txtRitsu3.DataField = "ITEM09";
            this.txtRitsu3.Height = 0.1574803F;
            this.txtRitsu3.Left = 2.325197F;
            this.txtRitsu3.Name = "txtRitsu3";
            this.txtRitsu3.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: right; " +
    "vertical-align: middle";
            this.txtRitsu3.Text = null;
            this.txtRitsu3.Top = 0.1574803F;
            this.txtRitsu3.Width = 0.3334644F;
            // 
            // txtZeiKubunNo
            // 
            this.txtZeiKubunNo.DataField = "ITEM03";
            this.txtZeiKubunNo.Height = 0.7874016F;
            this.txtZeiKubunNo.Left = 0F;
            this.txtZeiKubunNo.MultiLine = false;
            this.txtZeiKubunNo.Name = "txtZeiKubunNo";
            this.txtZeiKubunNo.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: center;" +
    " vertical-align: middle";
            this.txtZeiKubunNo.Text = null;
            this.txtZeiKubunNo.Top = 0F;
            this.txtZeiKubunNo.Width = 0.2877953F;
            // 
            // line10
            // 
            this.line10.Height = 0.7874016F;
            this.line10.Left = 0.2877951F;
            this.line10.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dot;
            this.line10.LineWeight = 1F;
            this.line10.Name = "line10";
            this.line10.Top = 0F;
            this.line10.Width = 0F;
            this.line10.X1 = 0.2877951F;
            this.line10.X2 = 0.2877951F;
            this.line10.Y1 = 0F;
            this.line10.Y2 = 0.7874016F;
            // 
            // line12
            // 
            this.line12.Height = 0.7874016F;
            this.line12.Left = 2.679528F;
            this.line12.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dot;
            this.line12.LineWeight = 1F;
            this.line12.Name = "line12";
            this.line12.Top = 0F;
            this.line12.Width = 0F;
            this.line12.X1 = 2.679528F;
            this.line12.X2 = 2.679528F;
            this.line12.Y1 = 0F;
            this.line12.Y2 = 0.7874016F;
            // 
            // line13
            // 
            this.line13.Height = 0.7874016F;
            this.line13.Left = 3.579528F;
            this.line13.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dot;
            this.line13.LineWeight = 1F;
            this.line13.Name = "line13";
            this.line13.Top = 0F;
            this.line13.Width = 0F;
            this.line13.X1 = 3.579528F;
            this.line13.X2 = 3.579528F;
            this.line13.Y1 = 0F;
            this.line13.Y2 = 0.7874016F;
            // 
            // line14
            // 
            this.line14.Height = 0.7874016F;
            this.line14.Left = 4.48504F;
            this.line14.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dot;
            this.line14.LineWeight = 1F;
            this.line14.Name = "line14";
            this.line14.Top = 0F;
            this.line14.Width = 0F;
            this.line14.X1 = 4.48504F;
            this.line14.X2 = 4.48504F;
            this.line14.Y1 = 0F;
            this.line14.Y2 = 0.7874016F;
            // 
            // line15
            // 
            this.line15.Height = 0.7874016F;
            this.line15.Left = 5.390552F;
            this.line15.LineWeight = 1F;
            this.line15.Name = "line15";
            this.line15.Top = 0F;
            this.line15.Width = 0F;
            this.line15.X1 = 5.390552F;
            this.line15.X2 = 5.390552F;
            this.line15.Y1 = 0F;
            this.line15.Y2 = 0.7874016F;
            // 
            // line18
            // 
            this.line18.Height = 0.7874016F;
            this.line18.Left = 0F;
            this.line18.LineWeight = 1F;
            this.line18.Name = "line18";
            this.line18.Top = 0F;
            this.line18.Width = 0F;
            this.line18.X1 = 0F;
            this.line18.X2 = 0F;
            this.line18.Y1 = 0F;
            this.line18.Y2 = 0.7874016F;
            // 
            // line19
            // 
            this.line19.Height = 0F;
            this.line19.Left = 2.30315F;
            this.line19.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dot;
            this.line19.LineWeight = 1F;
            this.line19.Name = "line19";
            this.line19.Top = 0.1574803F;
            this.line19.Width = 3.087403F;
            this.line19.X1 = 2.30315F;
            this.line19.X2 = 5.390553F;
            this.line19.Y1 = 0.1574803F;
            this.line19.Y2 = 0.1574803F;
            // 
            // line20
            // 
            this.line20.Height = 0F;
            this.line20.Left = 2.294095F;
            this.line20.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dot;
            this.line20.LineWeight = 1F;
            this.line20.Name = "line20";
            this.line20.Top = 0.3149607F;
            this.line20.Width = 3.096458F;
            this.line20.X1 = 2.294095F;
            this.line20.X2 = 5.390553F;
            this.line20.Y1 = 0.3149607F;
            this.line20.Y2 = 0.3149607F;
            // 
            // line21
            // 
            this.line21.Height = 0.001968503F;
            this.line21.Left = 2.294095F;
            this.line21.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dot;
            this.line21.LineWeight = 1F;
            this.line21.Name = "line21";
            this.line21.Top = 0.472441F;
            this.line21.Width = 3.096458F;
            this.line21.X1 = 2.294095F;
            this.line21.X2 = 5.390553F;
            this.line21.Y1 = 0.4744095F;
            this.line21.Y2 = 0.472441F;
            // 
            // line22
            // 
            this.line22.Height = 0F;
            this.line22.Left = 2.294095F;
            this.line22.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dot;
            this.line22.LineWeight = 1F;
            this.line22.Name = "line22";
            this.line22.Top = 0.6299213F;
            this.line22.Width = 3.096458F;
            this.line22.X1 = 2.294095F;
            this.line22.X2 = 5.390553F;
            this.line22.Y1 = 0.6299213F;
            this.line22.Y2 = 0.6299213F;
            // 
            // line2
            // 
            this.line2.Height = 0F;
            this.line2.Left = 0F;
            this.line2.LineWeight = 1F;
            this.line2.Name = "line2";
            this.line2.Top = 0.7874016F;
            this.line2.Width = 5.374016F;
            this.line2.X1 = 0F;
            this.line2.X2 = 5.374016F;
            this.line2.Y1 = 0.7874016F;
            this.line2.Y2 = 0.7874016F;
            // 
            // txtZeiKubunNm
            // 
            this.txtZeiKubunNm.DataField = "ITEM04";
            this.txtZeiKubunNm.Height = 0.6299213F;
            this.txtZeiKubunNm.Left = 0.346063F;
            this.txtZeiKubunNm.Name = "txtZeiKubunNm";
            this.txtZeiKubunNm.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: normal; text-align: left; v" +
    "ertical-align: top";
            this.txtZeiKubunNm.Text = null;
            this.txtZeiKubunNm.Top = 0.1574803F;
            this.txtZeiKubunNm.Width = 1.948032F;
            // 
            // line11
            // 
            this.line11.Height = 0.7874016F;
            this.line11.Left = 2.294095F;
            this.line11.LineWeight = 1F;
            this.line11.Name = "line11";
            this.line11.Top = 0F;
            this.line11.Width = 0F;
            this.line11.X1 = 2.294095F;
            this.line11.X2 = 2.294095F;
            this.line11.Y1 = 0F;
            this.line11.Y2 = 0.7874016F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            this.pageFooter.Visible = false;
            // 
            // txtTitle02
            // 
            this.txtTitle02.Height = 0.3149606F;
            this.txtTitle02.Left = 0.2877953F;
            this.txtTitle02.MultiLine = false;
            this.txtTitle02.Name = "txtTitle02";
            this.txtTitle02.Style = "background-color: Cyan; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: nor" +
    "mal; text-align: center; vertical-align: middle";
            this.txtTitle02.Text = "税 区 分";
            this.txtTitle02.Top = 0F;
            this.txtTitle02.Width = 2.006299F;
            // 
            // txtTitle01
            // 
            this.txtTitle01.Height = 0.3149606F;
            this.txtTitle01.Left = 0F;
            this.txtTitle01.MultiLine = false;
            this.txtTitle01.Name = "txtTitle01";
            this.txtTitle01.Style = "background-color: Cyan; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: nor" +
    "mal; text-align: center; vertical-align: middle";
            this.txtTitle01.Text = null;
            this.txtTitle01.Top = 0F;
            this.txtTitle01.Width = 0.2877953F;
            // 
            // txtTitle03
            // 
            this.txtTitle03.Height = 0.3149606F;
            this.txtTitle03.Left = 2.294095F;
            this.txtTitle03.MultiLine = false;
            this.txtTitle03.Name = "txtTitle03";
            this.txtTitle03.Style = "background-color: Cyan; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: nor" +
    "mal; text-align: center; vertical-align: middle";
            this.txtTitle03.Text = "率(%)";
            this.txtTitle03.Top = 0F;
            this.txtTitle03.Width = 0.3854332F;
            // 
            // txtTitle04
            // 
            this.txtTitle04.Height = 0.3149606F;
            this.txtTitle04.Left = 2.674016F;
            this.txtTitle04.MultiLine = false;
            this.txtTitle04.Name = "txtTitle04";
            this.txtTitle04.Style = "background-color: Cyan; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: nor" +
    "mal; text-align: center; vertical-align: middle";
            this.txtTitle04.Text = "税抜";
            this.txtTitle04.Top = 0F;
            this.txtTitle04.Width = 0.9055118F;
            // 
            // txtTitle05
            // 
            this.txtTitle05.Height = 0.3149606F;
            this.txtTitle05.Left = 3.579528F;
            this.txtTitle05.MultiLine = false;
            this.txtTitle05.Name = "txtTitle05";
            this.txtTitle05.Style = "background-color: Cyan; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: nor" +
    "mal; text-align: center; vertical-align: middle";
            this.txtTitle05.Text = "消費税";
            this.txtTitle05.Top = 0F;
            this.txtTitle05.Width = 0.9055118F;
            // 
            // txtTitle06
            // 
            this.txtTitle06.Height = 0.3149606F;
            this.txtTitle06.Left = 4.48504F;
            this.txtTitle06.MultiLine = false;
            this.txtTitle06.Name = "txtTitle06";
            this.txtTitle06.Style = "background-color: Cyan; font-family: ＭＳ ゴシック; font-size: 9.75pt; font-weight: nor" +
    "mal; text-align: center; vertical-align: middle";
            this.txtTitle06.Text = "計";
            this.txtTitle06.Top = 0F;
            this.txtTitle06.Width = 0.9055118F;
            // 
            // groupHeader1
            // 
            this.groupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtTitle02,
            this.txtTitle01,
            this.txtTitle03,
            this.txtTitle04,
            this.txtTitle05,
            this.txtTitle06,
            this.line3,
            this.line4,
            this.line5,
            this.line6,
            this.line7,
            this.line8,
            this.line9,
            this.line16,
            this.line17});
            this.groupHeader1.Height = 0.3220472F;
            this.groupHeader1.Name = "groupHeader1";
            this.groupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnColumn;
            // 
            // line3
            // 
            this.line3.Height = 0.3149606F;
            this.line3.Left = 0F;
            this.line3.LineWeight = 1F;
            this.line3.Name = "line3";
            this.line3.Top = 0F;
            this.line3.Width = 0F;
            this.line3.X1 = 0F;
            this.line3.X2 = 0F;
            this.line3.Y1 = 0F;
            this.line3.Y2 = 0.3149606F;
            // 
            // line4
            // 
            this.line4.Height = 0.3149606F;
            this.line4.Left = 0.2877951F;
            this.line4.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dot;
            this.line4.LineWeight = 1F;
            this.line4.Name = "line4";
            this.line4.Top = 0F;
            this.line4.Width = 0F;
            this.line4.X1 = 0.2877951F;
            this.line4.X2 = 0.2877951F;
            this.line4.Y1 = 0F;
            this.line4.Y2 = 0.3149606F;
            // 
            // line5
            // 
            this.line5.Height = 0.3149606F;
            this.line5.Left = 2.294095F;
            this.line5.LineWeight = 1F;
            this.line5.Name = "line5";
            this.line5.Top = 0.007086615F;
            this.line5.Width = 0F;
            this.line5.X1 = 2.294095F;
            this.line5.X2 = 2.294095F;
            this.line5.Y1 = 0.007086615F;
            this.line5.Y2 = 0.3220472F;
            // 
            // line6
            // 
            this.line6.Height = 0.3149606F;
            this.line6.Left = 2.679528F;
            this.line6.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dot;
            this.line6.LineWeight = 1F;
            this.line6.Name = "line6";
            this.line6.Top = 0F;
            this.line6.Width = 0F;
            this.line6.X1 = 2.679528F;
            this.line6.X2 = 2.679528F;
            this.line6.Y1 = 0F;
            this.line6.Y2 = 0.3149606F;
            // 
            // line7
            // 
            this.line7.Height = 0.3149606F;
            this.line7.Left = 3.579528F;
            this.line7.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dot;
            this.line7.LineWeight = 1F;
            this.line7.Name = "line7";
            this.line7.Top = 0F;
            this.line7.Width = 0F;
            this.line7.X1 = 3.579528F;
            this.line7.X2 = 3.579528F;
            this.line7.Y1 = 0F;
            this.line7.Y2 = 0.3149606F;
            // 
            // line8
            // 
            this.line8.Height = 0.3149606F;
            this.line8.Left = 4.48504F;
            this.line8.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dot;
            this.line8.LineWeight = 1F;
            this.line8.Name = "line8";
            this.line8.Top = 0.007086615F;
            this.line8.Width = 0F;
            this.line8.X1 = 4.48504F;
            this.line8.X2 = 4.48504F;
            this.line8.Y1 = 0.007086615F;
            this.line8.Y2 = 0.3220472F;
            // 
            // line9
            // 
            this.line9.Height = 0.3149606F;
            this.line9.Left = 5.390552F;
            this.line9.LineWeight = 1F;
            this.line9.Name = "line9";
            this.line9.Top = 0F;
            this.line9.Width = 0F;
            this.line9.X1 = 5.390552F;
            this.line9.X2 = 5.390552F;
            this.line9.Y1 = 0F;
            this.line9.Y2 = 0.3149606F;
            // 
            // line16
            // 
            this.line16.Height = 0F;
            this.line16.Left = 0F;
            this.line16.LineWeight = 1F;
            this.line16.Name = "line16";
            this.line16.Top = 0F;
            this.line16.Width = 5.374016F;
            this.line16.X1 = 0F;
            this.line16.X2 = 5.374016F;
            this.line16.Y1 = 0F;
            this.line16.Y2 = 0F;
            // 
            // line17
            // 
            this.line17.Height = 0F;
            this.line17.Left = 0F;
            this.line17.LineWeight = 1F;
            this.line17.Name = "line17";
            this.line17.Top = 0.3149606F;
            this.line17.Width = 5.374016F;
            this.line17.X1 = 0F;
            this.line17.X2 = 5.374016F;
            this.line17.Y1 = 0.3149606F;
            this.line17.Y2 = 0.3149606F;
            // 
            // groupFooter1
            // 
            this.groupFooter1.Height = 0F;
            this.groupFooter1.Name = "groupFooter1";
            this.groupFooter1.Visible = false;
            // 
            // ZMYR1031R
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.5905512F;
            this.PageSettings.Margins.Left = 0.3937008F;
            this.PageSettings.Margins.Right = 0.3937008F;
            this.PageSettings.Margins.Top = 0.3937008F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 10.90158F;
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.groupHeader1);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.groupFooter1);
            this.Sections.Add(this.pageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRitsu5Shohizei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRitsu8Shohizei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRitsu5Zeinuki)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRitsu8Zeinuki)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRitsu10Zeinuki)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRitsu10Shohizei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRitsu5Kei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRitsu10Kei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRitsu8Kei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRitsu0Zeinuki)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRitsu3Shohizei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRitsu0Kei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRitsu3Kei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRitsu0Shohizei)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRitsu3Zeinuki)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRitsu10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRitsu5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRitsu8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRitsu0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRitsu3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZeiKubunNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZeiKubunNm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle04)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle05)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle06)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtToday;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPageCount;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZeiKubunNo;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZeiKubunNm;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle04;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle05;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle06;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader groupHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter groupFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.Line line10;
        private GrapeCity.ActiveReports.SectionReportModel.Line line11;
        private GrapeCity.ActiveReports.SectionReportModel.Line line12;
        private GrapeCity.ActiveReports.SectionReportModel.Line line13;
        private GrapeCity.ActiveReports.SectionReportModel.Line line14;
        private GrapeCity.ActiveReports.SectionReportModel.Line line15;
        private GrapeCity.ActiveReports.SectionReportModel.Line line3;
        private GrapeCity.ActiveReports.SectionReportModel.Line line4;
        private GrapeCity.ActiveReports.SectionReportModel.Line line5;
        private GrapeCity.ActiveReports.SectionReportModel.Line line6;
        private GrapeCity.ActiveReports.SectionReportModel.Line line7;
        private GrapeCity.ActiveReports.SectionReportModel.Line line8;
        private GrapeCity.ActiveReports.SectionReportModel.Line line9;
        private GrapeCity.ActiveReports.SectionReportModel.Line line16;
        private GrapeCity.ActiveReports.SectionReportModel.Line line17;
        private GrapeCity.ActiveReports.SectionReportModel.Line line18;
        private GrapeCity.ActiveReports.SectionReportModel.Line line19;
        private GrapeCity.ActiveReports.SectionReportModel.Line line20;
        private GrapeCity.ActiveReports.SectionReportModel.Line line21;
        private GrapeCity.ActiveReports.SectionReportModel.Line line22;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRitsu0Zeinuki;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRitsu0;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRitsu3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRitsu5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRitsu8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRitsu10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRitsu3Zeinuki;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRitsu5Zeinuki;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRitsu8Zeinuki;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRitsu10Zeinuki;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRitsu0Shohizei;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRitsu3Shohizei;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRitsu5Shohizei;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRitsu8Shohizei;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRitsu10Shohizei;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRitsu0Kei;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRitsu3Kei;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRitsu5Kei;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRitsu8Kei;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRitsu10Kei;
    }
}
