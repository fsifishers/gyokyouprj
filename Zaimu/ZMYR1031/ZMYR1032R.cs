﻿using System;
using System.Data;

using jp.co.fsi.common.report;
using jp.co.fsi.common.util;

namespace jp.co.fsi.zm.zmyr1031
{
    /// <summary>
    /// ZAMR3062Rの帳票
    /// </summary>
    public partial class ZMYR1032R : BaseReport
    {
        public ZMYR1032R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }

        public ZMYR1032R(DataTable tgtData, string repID) : base(tgtData, repID)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }

        /// <summary>
        /// ページヘッダーの設定
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pageHeader_Format(object sender, EventArgs e)
        {
            txtToday.Text = DateTime.Now.ToString("yyyy-M-d");
        }

        /// <summary>
        /// ページフッターの設定
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void reportFooter1_Format(object sender, EventArgs e)
        {
            // 値が0となる場合は、空白にする
            if (txtZeinukiKingakuTotal.Text == "0")
            {
                txtZeinukiKingakuTotal.Text = "";
            }
            if (txtShohizeiTotal.Text == "0")
            {
                txtShohizeiTotal.Text = "";
            }
            if (txtKeiTotal.Text == "0")
            {
                txtKeiTotal.Text = "";
            }
            if (txtValue01Total.Text == "0")
            {
                txtValue01Total.Text = "";
            }
            if (txtValue02Total.Text == "0")
            {
                txtValue02Total.Text = "";
            }
            if (txtValue03Total.Text == "0")
            {
                txtValue03Total.Text = "";
            }
            if (txtValue04Total.Text == "0")
            {
                txtValue04Total.Text = "";
            }
            if (txtValue05Total.Text == "0")
            {
                txtValue05Total.Text = "";
            }
            if (txtValue06Total.Text == "0")
            {
                txtValue06Total.Text = "";
            }
            if (txtValue07Total.Text == "0")
            {
                txtValue07Total.Text = "";
            }
            if (txtValue08Total.Text == "0")
            {
                txtValue08Total.Text = "";
            }
            if (txtValue09Total.Text == "0")
            {
                txtValue09Total.Text = "";
            }
        }
    }
}
