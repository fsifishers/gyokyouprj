﻿namespace jp.co.fsi.zm.zmmr1061
{
    partial class ZMMR1061
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbxDate = new System.Windows.Forms.GroupBox();
            this.lblKikanCodeBet = new System.Windows.Forms.Label();
            this.lblDayTo = new System.Windows.Forms.Label();
            this.lblMonthTo = new System.Windows.Forms.Label();
            this.lblYearTo = new System.Windows.Forms.Label();
            this.txtDayTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtYearTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtMonthTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGengoTo = new System.Windows.Forms.Label();
            this.lblTo = new System.Windows.Forms.Label();
            this.lblDayFr = new System.Windows.Forms.Label();
            this.lblMonthFr = new System.Windows.Forms.Label();
            this.lblYearFr = new System.Windows.Forms.Label();
            this.txtDayFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtYearFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtMonthFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGengoFr = new System.Windows.Forms.Label();
            this.lblFr = new System.Windows.Forms.Label();
            this.gbxKanjoKamoku = new System.Windows.Forms.GroupBox();
            this.lblKanjoKamokuTo = new System.Windows.Forms.Label();
            this.lblKanjoKamokuCodeBet = new System.Windows.Forms.Label();
            this.txtKanjoKamokuFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKanjoKamokuFr = new System.Windows.Forms.Label();
            this.txtKanjoKamokuTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.gbxShohizeiShori = new System.Windows.Forms.GroupBox();
            this.rdoZeinuki = new System.Windows.Forms.RadioButton();
            this.rdoZeikomi = new System.Windows.Forms.RadioButton();
            this.gbxBumon = new System.Windows.Forms.GroupBox();
            this.lblBumonTo = new System.Windows.Forms.Label();
            this.lblBumonBet = new System.Windows.Forms.Label();
            this.txtBumonFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblBumonFr = new System.Windows.Forms.Label();
            this.txtBumonTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.gbxInji = new System.Windows.Forms.GroupBox();
            this.rdoYes = new System.Windows.Forms.RadioButton();
            this.rdoNo = new System.Windows.Forms.RadioButton();
            this.gbxShiwakeShurui = new System.Windows.Forms.GroupBox();
            this.rdoZenbu = new System.Windows.Forms.RadioButton();
            this.rdoKessan = new System.Windows.Forms.RadioButton();
            this.rdoTsujo = new System.Windows.Forms.RadioButton();
            this.gbxMizuageShisho = new System.Windows.Forms.GroupBox();
            this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMizuageShishoNm = new System.Windows.Forms.Label();
            this.lblMizuageShisho = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.gbxDate.SuspendLayout();
            this.gbxKanjoKamoku.SuspendLayout();
            this.gbxShohizeiShori.SuspendLayout();
            this.gbxBumon.SuspendLayout();
            this.gbxInji.SuspendLayout();
            this.gbxShiwakeShurui.SuspendLayout();
            this.gbxMizuageShisho.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Text = "";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            // 
            // gbxDate
            // 
            this.gbxDate.Controls.Add(this.lblKikanCodeBet);
            this.gbxDate.Controls.Add(this.lblDayTo);
            this.gbxDate.Controls.Add(this.lblMonthTo);
            this.gbxDate.Controls.Add(this.lblYearTo);
            this.gbxDate.Controls.Add(this.txtDayTo);
            this.gbxDate.Controls.Add(this.txtYearTo);
            this.gbxDate.Controls.Add(this.txtMonthTo);
            this.gbxDate.Controls.Add(this.lblGengoTo);
            this.gbxDate.Controls.Add(this.lblTo);
            this.gbxDate.Controls.Add(this.lblDayFr);
            this.gbxDate.Controls.Add(this.lblMonthFr);
            this.gbxDate.Controls.Add(this.lblYearFr);
            this.gbxDate.Controls.Add(this.txtDayFr);
            this.gbxDate.Controls.Add(this.txtYearFr);
            this.gbxDate.Controls.Add(this.txtMonthFr);
            this.gbxDate.Controls.Add(this.lblGengoFr);
            this.gbxDate.Controls.Add(this.lblFr);
            this.gbxDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxDate.ForeColor = System.Drawing.Color.Black;
            this.gbxDate.Location = new System.Drawing.Point(12, 196);
            this.gbxDate.Name = "gbxDate";
            this.gbxDate.Size = new System.Drawing.Size(573, 74);
            this.gbxDate.TabIndex = 4;
            this.gbxDate.TabStop = false;
            this.gbxDate.Text = "期間";
            // 
            // lblKikanCodeBet
            // 
            this.lblKikanCodeBet.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKikanCodeBet.ForeColor = System.Drawing.Color.Black;
            this.lblKikanCodeBet.Location = new System.Drawing.Point(277, 29);
            this.lblKikanCodeBet.Name = "lblKikanCodeBet";
            this.lblKikanCodeBet.Size = new System.Drawing.Size(18, 20);
            this.lblKikanCodeBet.TabIndex = 8;
            this.lblKikanCodeBet.Text = "～";
            this.lblKikanCodeBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDayTo
            // 
            this.lblDayTo.BackColor = System.Drawing.Color.Silver;
            this.lblDayTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDayTo.ForeColor = System.Drawing.Color.Black;
            this.lblDayTo.Location = new System.Drawing.Point(525, 29);
            this.lblDayTo.Name = "lblDayTo";
            this.lblDayTo.Size = new System.Drawing.Size(20, 18);
            this.lblDayTo.TabIndex = 16;
            this.lblDayTo.Text = "日";
            this.lblDayTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMonthTo
            // 
            this.lblMonthTo.BackColor = System.Drawing.Color.Silver;
            this.lblMonthTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMonthTo.ForeColor = System.Drawing.Color.Black;
            this.lblMonthTo.Location = new System.Drawing.Point(470, 29);
            this.lblMonthTo.Name = "lblMonthTo";
            this.lblMonthTo.Size = new System.Drawing.Size(15, 19);
            this.lblMonthTo.TabIndex = 14;
            this.lblMonthTo.Text = "月";
            this.lblMonthTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblYearTo
            // 
            this.lblYearTo.BackColor = System.Drawing.Color.Silver;
            this.lblYearTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYearTo.ForeColor = System.Drawing.Color.Black;
            this.lblYearTo.Location = new System.Drawing.Point(417, 29);
            this.lblYearTo.Name = "lblYearTo";
            this.lblYearTo.Size = new System.Drawing.Size(17, 21);
            this.lblYearTo.TabIndex = 12;
            this.lblYearTo.Text = "年";
            this.lblYearTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDayTo
            // 
            this.txtDayTo.AutoSizeFromLength = false;
            this.txtDayTo.DisplayLength = null;
            this.txtDayTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDayTo.ForeColor = System.Drawing.Color.Black;
            this.txtDayTo.Location = new System.Drawing.Point(492, 29);
            this.txtDayTo.MaxLength = 2;
            this.txtDayTo.Name = "txtDayTo";
            this.txtDayTo.Size = new System.Drawing.Size(30, 20);
            this.txtDayTo.TabIndex = 15;
            this.txtDayTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDayTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateDayTo_Validating);
            // 
            // txtYearTo
            // 
            this.txtYearTo.AutoSizeFromLength = false;
            this.txtYearTo.DisplayLength = null;
            this.txtYearTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtYearTo.ForeColor = System.Drawing.Color.Black;
            this.txtYearTo.Location = new System.Drawing.Point(385, 29);
            this.txtYearTo.MaxLength = 2;
            this.txtYearTo.Name = "txtYearTo";
            this.txtYearTo.Size = new System.Drawing.Size(30, 20);
            this.txtYearTo.TabIndex = 11;
            this.txtYearTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtYearTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateYearTo_Validating);
            // 
            // txtMonthTo
            // 
            this.txtMonthTo.AutoSizeFromLength = false;
            this.txtMonthTo.DisplayLength = null;
            this.txtMonthTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMonthTo.ForeColor = System.Drawing.Color.Black;
            this.txtMonthTo.Location = new System.Drawing.Point(438, 29);
            this.txtMonthTo.MaxLength = 2;
            this.txtMonthTo.Name = "txtMonthTo";
            this.txtMonthTo.Size = new System.Drawing.Size(30, 20);
            this.txtMonthTo.TabIndex = 13;
            this.txtMonthTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonthTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateMonthTo_Validating);
            // 
            // lblGengoTo
            // 
            this.lblGengoTo.BackColor = System.Drawing.Color.Silver;
            this.lblGengoTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengoTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengoTo.ForeColor = System.Drawing.Color.Black;
            this.lblGengoTo.Location = new System.Drawing.Point(340, 29);
            this.lblGengoTo.Name = "lblGengoTo";
            this.lblGengoTo.Size = new System.Drawing.Size(41, 21);
            this.lblGengoTo.TabIndex = 10;
            this.lblGengoTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTo
            // 
            this.lblTo.BackColor = System.Drawing.Color.Silver;
            this.lblTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTo.ForeColor = System.Drawing.Color.Black;
            this.lblTo.Location = new System.Drawing.Point(337, 26);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(214, 27);
            this.lblTo.TabIndex = 9;
            this.lblTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDayFr
            // 
            this.lblDayFr.BackColor = System.Drawing.Color.Silver;
            this.lblDayFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDayFr.ForeColor = System.Drawing.Color.Black;
            this.lblDayFr.Location = new System.Drawing.Point(209, 29);
            this.lblDayFr.Name = "lblDayFr";
            this.lblDayFr.Size = new System.Drawing.Size(20, 18);
            this.lblDayFr.TabIndex = 7;
            this.lblDayFr.Text = "日";
            this.lblDayFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMonthFr
            // 
            this.lblMonthFr.BackColor = System.Drawing.Color.Silver;
            this.lblMonthFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMonthFr.ForeColor = System.Drawing.Color.Black;
            this.lblMonthFr.Location = new System.Drawing.Point(154, 29);
            this.lblMonthFr.Name = "lblMonthFr";
            this.lblMonthFr.Size = new System.Drawing.Size(15, 19);
            this.lblMonthFr.TabIndex = 5;
            this.lblMonthFr.Text = "月";
            this.lblMonthFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblYearFr
            // 
            this.lblYearFr.BackColor = System.Drawing.Color.Silver;
            this.lblYearFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYearFr.ForeColor = System.Drawing.Color.Black;
            this.lblYearFr.Location = new System.Drawing.Point(101, 29);
            this.lblYearFr.Name = "lblYearFr";
            this.lblYearFr.Size = new System.Drawing.Size(17, 21);
            this.lblYearFr.TabIndex = 3;
            this.lblYearFr.Text = "年";
            this.lblYearFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDayFr
            // 
            this.txtDayFr.AutoSizeFromLength = false;
            this.txtDayFr.DisplayLength = null;
            this.txtDayFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDayFr.ForeColor = System.Drawing.Color.Black;
            this.txtDayFr.Location = new System.Drawing.Point(176, 29);
            this.txtDayFr.MaxLength = 2;
            this.txtDayFr.Name = "txtDayFr";
            this.txtDayFr.Size = new System.Drawing.Size(30, 20);
            this.txtDayFr.TabIndex = 6;
            this.txtDayFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDayFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateDayFr_Validating);
            // 
            // txtYearFr
            // 
            this.txtYearFr.AutoSizeFromLength = false;
            this.txtYearFr.DisplayLength = null;
            this.txtYearFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtYearFr.ForeColor = System.Drawing.Color.Black;
            this.txtYearFr.Location = new System.Drawing.Point(69, 29);
            this.txtYearFr.MaxLength = 2;
            this.txtYearFr.Name = "txtYearFr";
            this.txtYearFr.Size = new System.Drawing.Size(30, 20);
            this.txtYearFr.TabIndex = 2;
            this.txtYearFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtYearFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateYearFr_Validating);
            // 
            // txtMonthFr
            // 
            this.txtMonthFr.AutoSizeFromLength = false;
            this.txtMonthFr.DisplayLength = null;
            this.txtMonthFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMonthFr.ForeColor = System.Drawing.Color.Black;
            this.txtMonthFr.Location = new System.Drawing.Point(122, 29);
            this.txtMonthFr.MaxLength = 2;
            this.txtMonthFr.Name = "txtMonthFr";
            this.txtMonthFr.Size = new System.Drawing.Size(30, 20);
            this.txtMonthFr.TabIndex = 4;
            this.txtMonthFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonthFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateMonthFr_Validating);
            // 
            // lblGengoFr
            // 
            this.lblGengoFr.BackColor = System.Drawing.Color.Silver;
            this.lblGengoFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengoFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengoFr.ForeColor = System.Drawing.Color.Black;
            this.lblGengoFr.Location = new System.Drawing.Point(24, 29);
            this.lblGengoFr.Name = "lblGengoFr";
            this.lblGengoFr.Size = new System.Drawing.Size(41, 21);
            this.lblGengoFr.TabIndex = 1;
            this.lblGengoFr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblFr
            // 
            this.lblFr.BackColor = System.Drawing.Color.Silver;
            this.lblFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFr.ForeColor = System.Drawing.Color.Black;
            this.lblFr.Location = new System.Drawing.Point(21, 26);
            this.lblFr.Name = "lblFr";
            this.lblFr.Size = new System.Drawing.Size(214, 27);
            this.lblFr.TabIndex = 0;
            this.lblFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gbxKanjoKamoku
            // 
            this.gbxKanjoKamoku.Controls.Add(this.lblKanjoKamokuTo);
            this.gbxKanjoKamoku.Controls.Add(this.lblKanjoKamokuCodeBet);
            this.gbxKanjoKamoku.Controls.Add(this.txtKanjoKamokuFr);
            this.gbxKanjoKamoku.Controls.Add(this.lblKanjoKamokuFr);
            this.gbxKanjoKamoku.Controls.Add(this.txtKanjoKamokuTo);
            this.gbxKanjoKamoku.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxKanjoKamoku.ForeColor = System.Drawing.Color.Black;
            this.gbxKanjoKamoku.Location = new System.Drawing.Point(12, 276);
            this.gbxKanjoKamoku.Name = "gbxKanjoKamoku";
            this.gbxKanjoKamoku.Size = new System.Drawing.Size(572, 74);
            this.gbxKanjoKamoku.TabIndex = 5;
            this.gbxKanjoKamoku.TabStop = false;
            this.gbxKanjoKamoku.Text = "勘定科目";
            // 
            // lblKanjoKamokuTo
            // 
            this.lblKanjoKamokuTo.BackColor = System.Drawing.Color.Silver;
            this.lblKanjoKamokuTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKanjoKamokuTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKanjoKamokuTo.ForeColor = System.Drawing.Color.Black;
            this.lblKanjoKamokuTo.Location = new System.Drawing.Point(384, 30);
            this.lblKanjoKamokuTo.Name = "lblKanjoKamokuTo";
            this.lblKanjoKamokuTo.Size = new System.Drawing.Size(170, 20);
            this.lblKanjoKamokuTo.TabIndex = 4;
            this.lblKanjoKamokuTo.Text = "最　後";
            this.lblKanjoKamokuTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKanjoKamokuCodeBet
            // 
            this.lblKanjoKamokuCodeBet.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKanjoKamokuCodeBet.ForeColor = System.Drawing.Color.Black;
            this.lblKanjoKamokuCodeBet.Location = new System.Drawing.Point(272, 30);
            this.lblKanjoKamokuCodeBet.Name = "lblKanjoKamokuCodeBet";
            this.lblKanjoKamokuCodeBet.Size = new System.Drawing.Size(18, 20);
            this.lblKanjoKamokuCodeBet.TabIndex = 2;
            this.lblKanjoKamokuCodeBet.Text = "～";
            this.lblKanjoKamokuCodeBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKanjoKamokuFr
            // 
            this.txtKanjoKamokuFr.AutoSizeFromLength = false;
            this.txtKanjoKamokuFr.DisplayLength = null;
            this.txtKanjoKamokuFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKanjoKamokuFr.ForeColor = System.Drawing.Color.Black;
            this.txtKanjoKamokuFr.Location = new System.Drawing.Point(22, 30);
            this.txtKanjoKamokuFr.MaxLength = 6;
            this.txtKanjoKamokuFr.Name = "txtKanjoKamokuFr";
            this.txtKanjoKamokuFr.Size = new System.Drawing.Size(50, 20);
            this.txtKanjoKamokuFr.TabIndex = 0;
            this.txtKanjoKamokuFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKanjoKamokuFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtKnjoKamokuFr_Validating);
            // 
            // lblKanjoKamokuFr
            // 
            this.lblKanjoKamokuFr.BackColor = System.Drawing.Color.Silver;
            this.lblKanjoKamokuFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKanjoKamokuFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKanjoKamokuFr.ForeColor = System.Drawing.Color.Black;
            this.lblKanjoKamokuFr.Location = new System.Drawing.Point(73, 30);
            this.lblKanjoKamokuFr.Name = "lblKanjoKamokuFr";
            this.lblKanjoKamokuFr.Size = new System.Drawing.Size(170, 20);
            this.lblKanjoKamokuFr.TabIndex = 1;
            this.lblKanjoKamokuFr.Text = "先　頭";
            this.lblKanjoKamokuFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKanjoKamokuTo
            // 
            this.txtKanjoKamokuTo.AutoSizeFromLength = false;
            this.txtKanjoKamokuTo.DisplayLength = null;
            this.txtKanjoKamokuTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKanjoKamokuTo.ForeColor = System.Drawing.Color.Black;
            this.txtKanjoKamokuTo.Location = new System.Drawing.Point(332, 30);
            this.txtKanjoKamokuTo.MaxLength = 6;
            this.txtKanjoKamokuTo.Name = "txtKanjoKamokuTo";
            this.txtKanjoKamokuTo.Size = new System.Drawing.Size(50, 20);
            this.txtKanjoKamokuTo.TabIndex = 3;
            this.txtKanjoKamokuTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKanjoKamokuTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtKnjoKamokuTo_Validating);
            // 
            // gbxShohizeiShori
            // 
            this.gbxShohizeiShori.Controls.Add(this.rdoZeinuki);
            this.gbxShohizeiShori.Controls.Add(this.rdoZeikomi);
            this.gbxShohizeiShori.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxShohizeiShori.ForeColor = System.Drawing.Color.Black;
            this.gbxShohizeiShori.Location = new System.Drawing.Point(328, 116);
            this.gbxShohizeiShori.Name = "gbxShohizeiShori";
            this.gbxShohizeiShori.Size = new System.Drawing.Size(212, 60);
            this.gbxShohizeiShori.TabIndex = 3;
            this.gbxShohizeiShori.TabStop = false;
            this.gbxShohizeiShori.Text = "消費税処理";
            // 
            // rdoZeinuki
            // 
            this.rdoZeinuki.AutoSize = true;
            this.rdoZeinuki.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoZeinuki.ForeColor = System.Drawing.Color.Black;
            this.rdoZeinuki.Location = new System.Drawing.Point(126, 25);
            this.rdoZeinuki.Name = "rdoZeinuki";
            this.rdoZeinuki.Size = new System.Drawing.Size(53, 17);
            this.rdoZeinuki.TabIndex = 1;
            this.rdoZeinuki.TabStop = true;
            this.rdoZeinuki.Text = "税抜";
            this.rdoZeinuki.UseVisualStyleBackColor = true;
            // 
            // rdoZeikomi
            // 
            this.rdoZeikomi.AutoSize = true;
            this.rdoZeikomi.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoZeikomi.ForeColor = System.Drawing.Color.Black;
            this.rdoZeikomi.Location = new System.Drawing.Point(29, 25);
            this.rdoZeikomi.Name = "rdoZeikomi";
            this.rdoZeikomi.Size = new System.Drawing.Size(53, 17);
            this.rdoZeikomi.TabIndex = 0;
            this.rdoZeikomi.TabStop = true;
            this.rdoZeikomi.Text = "税込";
            this.rdoZeikomi.UseVisualStyleBackColor = true;
            // 
            // gbxBumon
            // 
            this.gbxBumon.Controls.Add(this.lblBumonTo);
            this.gbxBumon.Controls.Add(this.lblBumonBet);
            this.gbxBumon.Controls.Add(this.txtBumonFr);
            this.gbxBumon.Controls.Add(this.lblBumonFr);
            this.gbxBumon.Controls.Add(this.txtBumonTo);
            this.gbxBumon.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxBumon.ForeColor = System.Drawing.Color.Black;
            this.gbxBumon.Location = new System.Drawing.Point(12, 358);
            this.gbxBumon.Name = "gbxBumon";
            this.gbxBumon.Size = new System.Drawing.Size(572, 74);
            this.gbxBumon.TabIndex = 7;
            this.gbxBumon.TabStop = false;
            this.gbxBumon.Text = "部門範囲";
            // 
            // lblBumonTo
            // 
            this.lblBumonTo.BackColor = System.Drawing.Color.Silver;
            this.lblBumonTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBumonTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBumonTo.ForeColor = System.Drawing.Color.Black;
            this.lblBumonTo.Location = new System.Drawing.Point(384, 30);
            this.lblBumonTo.Name = "lblBumonTo";
            this.lblBumonTo.Size = new System.Drawing.Size(170, 20);
            this.lblBumonTo.TabIndex = 4;
            this.lblBumonTo.Text = "最　後";
            this.lblBumonTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblBumonBet
            // 
            this.lblBumonBet.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBumonBet.ForeColor = System.Drawing.Color.Black;
            this.lblBumonBet.Location = new System.Drawing.Point(272, 30);
            this.lblBumonBet.Name = "lblBumonBet";
            this.lblBumonBet.Size = new System.Drawing.Size(18, 20);
            this.lblBumonBet.TabIndex = 2;
            this.lblBumonBet.Text = "～";
            this.lblBumonBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtBumonFr
            // 
            this.txtBumonFr.AutoSizeFromLength = false;
            this.txtBumonFr.DisplayLength = null;
            this.txtBumonFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtBumonFr.ForeColor = System.Drawing.Color.Black;
            this.txtBumonFr.Location = new System.Drawing.Point(22, 30);
            this.txtBumonFr.MaxLength = 4;
            this.txtBumonFr.Name = "txtBumonFr";
            this.txtBumonFr.Size = new System.Drawing.Size(50, 20);
            this.txtBumonFr.TabIndex = 0;
            this.txtBumonFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBumonFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtBumonFr_Validating);
            // 
            // lblBumonFr
            // 
            this.lblBumonFr.BackColor = System.Drawing.Color.Silver;
            this.lblBumonFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBumonFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBumonFr.ForeColor = System.Drawing.Color.Black;
            this.lblBumonFr.Location = new System.Drawing.Point(73, 30);
            this.lblBumonFr.Name = "lblBumonFr";
            this.lblBumonFr.Size = new System.Drawing.Size(170, 20);
            this.lblBumonFr.TabIndex = 1;
            this.lblBumonFr.Text = "先　頭";
            this.lblBumonFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtBumonTo
            // 
            this.txtBumonTo.AutoSizeFromLength = false;
            this.txtBumonTo.DisplayLength = null;
            this.txtBumonTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtBumonTo.ForeColor = System.Drawing.Color.Black;
            this.txtBumonTo.Location = new System.Drawing.Point(332, 30);
            this.txtBumonTo.MaxLength = 4;
            this.txtBumonTo.Name = "txtBumonTo";
            this.txtBumonTo.Size = new System.Drawing.Size(50, 20);
            this.txtBumonTo.TabIndex = 3;
            this.txtBumonTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBumonTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtBumonTo_Validating);
            // 
            // gbxInji
            // 
            this.gbxInji.Controls.Add(this.rdoYes);
            this.gbxInji.Controls.Add(this.rdoNo);
            this.gbxInji.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxInji.ForeColor = System.Drawing.Color.Black;
            this.gbxInji.Location = new System.Drawing.Point(12, 438);
            this.gbxInji.Name = "gbxInji";
            this.gbxInji.Size = new System.Drawing.Size(212, 74);
            this.gbxInji.TabIndex = 8;
            this.gbxInji.TabStop = false;
            this.gbxInji.Text = "金額がｾﾞﾛの科目を印字";
            // 
            // rdoYes
            // 
            this.rdoYes.AutoSize = true;
            this.rdoYes.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoYes.ForeColor = System.Drawing.Color.Black;
            this.rdoYes.Location = new System.Drawing.Point(126, 33);
            this.rdoYes.Name = "rdoYes";
            this.rdoYes.Size = new System.Drawing.Size(53, 17);
            this.rdoYes.TabIndex = 1;
            this.rdoYes.TabStop = true;
            this.rdoYes.Text = "する";
            this.rdoYes.UseVisualStyleBackColor = true;
            this.rdoYes.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.rdo_KeyPress);
            // 
            // rdoNo
            // 
            this.rdoNo.AutoSize = true;
            this.rdoNo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoNo.ForeColor = System.Drawing.Color.Black;
            this.rdoNo.Location = new System.Drawing.Point(26, 33);
            this.rdoNo.Name = "rdoNo";
            this.rdoNo.Size = new System.Drawing.Size(67, 17);
            this.rdoNo.TabIndex = 0;
            this.rdoNo.TabStop = true;
            this.rdoNo.Text = "しない";
            this.rdoNo.UseVisualStyleBackColor = true;
            this.rdoNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.rdo_KeyPress);
            // 
            // gbxShiwakeShurui
            // 
            this.gbxShiwakeShurui.Controls.Add(this.rdoZenbu);
            this.gbxShiwakeShurui.Controls.Add(this.rdoKessan);
            this.gbxShiwakeShurui.Controls.Add(this.rdoTsujo);
            this.gbxShiwakeShurui.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxShiwakeShurui.ForeColor = System.Drawing.Color.Black;
            this.gbxShiwakeShurui.Location = new System.Drawing.Point(12, 116);
            this.gbxShiwakeShurui.Name = "gbxShiwakeShurui";
            this.gbxShiwakeShurui.Size = new System.Drawing.Size(304, 60);
            this.gbxShiwakeShurui.TabIndex = 2;
            this.gbxShiwakeShurui.TabStop = false;
            this.gbxShiwakeShurui.Text = "仕訳種類";
            // 
            // rdoZenbu
            // 
            this.rdoZenbu.AutoSize = true;
            this.rdoZenbu.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoZenbu.ForeColor = System.Drawing.Color.Black;
            this.rdoZenbu.Location = new System.Drawing.Point(217, 25);
            this.rdoZenbu.Name = "rdoZenbu";
            this.rdoZenbu.Size = new System.Drawing.Size(67, 17);
            this.rdoZenbu.TabIndex = 2;
            this.rdoZenbu.TabStop = true;
            this.rdoZenbu.Text = "全仕訳";
            this.rdoZenbu.UseVisualStyleBackColor = true;
            // 
            // rdoKessan
            // 
            this.rdoKessan.AutoSize = true;
            this.rdoKessan.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoKessan.ForeColor = System.Drawing.Color.Black;
            this.rdoKessan.Location = new System.Drawing.Point(115, 25);
            this.rdoKessan.Name = "rdoKessan";
            this.rdoKessan.Size = new System.Drawing.Size(81, 17);
            this.rdoKessan.TabIndex = 1;
            this.rdoKessan.TabStop = true;
            this.rdoKessan.Text = "決算仕訳";
            this.rdoKessan.UseVisualStyleBackColor = true;
            // 
            // rdoTsujo
            // 
            this.rdoTsujo.AutoSize = true;
            this.rdoTsujo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoTsujo.ForeColor = System.Drawing.Color.Black;
            this.rdoTsujo.Location = new System.Drawing.Point(18, 25);
            this.rdoTsujo.Name = "rdoTsujo";
            this.rdoTsujo.Size = new System.Drawing.Size(81, 17);
            this.rdoTsujo.TabIndex = 0;
            this.rdoTsujo.TabStop = true;
            this.rdoTsujo.Text = "通常仕訳";
            this.rdoTsujo.UseVisualStyleBackColor = true;
            // 
            // gbxMizuageShisho
            // 
            this.gbxMizuageShisho.Controls.Add(this.txtMizuageShishoCd);
            this.gbxMizuageShisho.Controls.Add(this.lblMizuageShishoNm);
            this.gbxMizuageShisho.Controls.Add(this.lblMizuageShisho);
            this.gbxMizuageShisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.gbxMizuageShisho.ForeColor = System.Drawing.Color.Black;
            this.gbxMizuageShisho.Location = new System.Drawing.Point(12, 49);
            this.gbxMizuageShisho.Name = "gbxMizuageShisho";
            this.gbxMizuageShisho.Size = new System.Drawing.Size(404, 59);
            this.gbxMizuageShisho.TabIndex = 1;
            this.gbxMizuageShisho.TabStop = false;
            this.gbxMizuageShisho.Text = "支所";
            // 
            // txtMizuageShishoCd
            // 
            this.txtMizuageShishoCd.AutoSizeFromLength = true;
            this.txtMizuageShishoCd.DisplayLength = null;
            this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtMizuageShishoCd.Location = new System.Drawing.Point(71, 22);
            this.txtMizuageShishoCd.MaxLength = 4;
            this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
            this.txtMizuageShishoCd.Size = new System.Drawing.Size(34, 20);
            this.txtMizuageShishoCd.TabIndex = 1;
            this.txtMizuageShishoCd.TabStop = false;
            this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
            // 
            // lblMizuageShishoNm
            // 
            this.lblMizuageShishoNm.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMizuageShishoNm.Location = new System.Drawing.Point(107, 22);
            this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
            this.lblMizuageShishoNm.Size = new System.Drawing.Size(270, 20);
            this.lblMizuageShishoNm.TabIndex = 2;
            this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMizuageShisho
            // 
            this.lblMizuageShisho.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblMizuageShisho.Location = new System.Drawing.Point(27, 20);
            this.lblMizuageShisho.Name = "lblMizuageShisho";
            this.lblMizuageShisho.Size = new System.Drawing.Size(354, 25);
            this.lblMizuageShisho.TabIndex = 0;
            this.lblMizuageShisho.Text = "支所";
            this.lblMizuageShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ZMMR1061
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.gbxMizuageShisho);
            this.Controls.Add(this.gbxShiwakeShurui);
            this.Controls.Add(this.gbxInji);
            this.Controls.Add(this.gbxBumon);
            this.Controls.Add(this.gbxShohizeiShori);
            this.Controls.Add(this.gbxKanjoKamoku);
            this.Controls.Add(this.gbxDate);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "ZMMR1061";
            this.Text = "";
            this.Controls.SetChildIndex(this.gbxDate, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.gbxKanjoKamoku, 0);
            this.Controls.SetChildIndex(this.gbxShohizeiShori, 0);
            this.Controls.SetChildIndex(this.gbxBumon, 0);
            this.Controls.SetChildIndex(this.gbxInji, 0);
            this.Controls.SetChildIndex(this.gbxShiwakeShurui, 0);
            this.Controls.SetChildIndex(this.gbxMizuageShisho, 0);
            this.pnlDebug.ResumeLayout(false);
            this.gbxDate.ResumeLayout(false);
            this.gbxDate.PerformLayout();
            this.gbxKanjoKamoku.ResumeLayout(false);
            this.gbxKanjoKamoku.PerformLayout();
            this.gbxShohizeiShori.ResumeLayout(false);
            this.gbxShohizeiShori.PerformLayout();
            this.gbxBumon.ResumeLayout(false);
            this.gbxBumon.PerformLayout();
            this.gbxInji.ResumeLayout(false);
            this.gbxInji.PerformLayout();
            this.gbxShiwakeShurui.ResumeLayout(false);
            this.gbxShiwakeShurui.PerformLayout();
            this.gbxMizuageShisho.ResumeLayout(false);
            this.gbxMizuageShisho.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbxDate;
        private jp.co.fsi.common.controls.FsiTextBox txtYearFr;
        private System.Windows.Forms.Label lblGengoFr;
        private System.Windows.Forms.Label lblFr;
        private System.Windows.Forms.Label lblDayFr;
        private System.Windows.Forms.Label lblMonthFr;
        private System.Windows.Forms.Label lblYearFr;
        private jp.co.fsi.common.controls.FsiTextBox txtDayFr;
        private jp.co.fsi.common.controls.FsiTextBox txtMonthFr;
        private System.Windows.Forms.GroupBox gbxKanjoKamoku;
        private System.Windows.Forms.Label lblKanjoKamokuTo;
        private System.Windows.Forms.Label lblKanjoKamokuCodeBet;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuFr;
        private System.Windows.Forms.Label lblKanjoKamokuFr;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuTo;
        private System.Windows.Forms.GroupBox gbxShohizeiShori;
        private System.Windows.Forms.RadioButton rdoZeinuki;
        private System.Windows.Forms.RadioButton rdoZeikomi;
        private System.Windows.Forms.Label lblKikanCodeBet;
        private System.Windows.Forms.Label lblDayTo;
        private System.Windows.Forms.Label lblMonthTo;
        private System.Windows.Forms.Label lblYearTo;
        private jp.co.fsi.common.controls.FsiTextBox txtDayTo;
        private jp.co.fsi.common.controls.FsiTextBox txtYearTo;
        private jp.co.fsi.common.controls.FsiTextBox txtMonthTo;
        private System.Windows.Forms.Label lblGengoTo;
        private System.Windows.Forms.Label lblTo;
        private System.Windows.Forms.GroupBox gbxBumon;
        private System.Windows.Forms.Label lblBumonTo;
        private System.Windows.Forms.Label lblBumonBet;
        private jp.co.fsi.common.controls.FsiTextBox txtBumonFr;
        private System.Windows.Forms.Label lblBumonFr;
        private jp.co.fsi.common.controls.FsiTextBox txtBumonTo;
        private System.Windows.Forms.GroupBox gbxInji;
        private System.Windows.Forms.RadioButton rdoYes;
        private System.Windows.Forms.RadioButton rdoNo;
        private System.Windows.Forms.GroupBox gbxShiwakeShurui;
        private System.Windows.Forms.RadioButton rdoKessan;
        private System.Windows.Forms.RadioButton rdoTsujo;
        private System.Windows.Forms.RadioButton rdoZenbu;
        private System.Windows.Forms.GroupBox gbxMizuageShisho;
        private common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.Label lblMizuageShisho;
    }
}