﻿using System.Data;
using System.Text;
using System.Windows.Forms;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.userinfo;
using jp.co.fsi.common.util;
using jp.co.fsi.common.forms;

namespace jp.co.fsi.zm.zmmr1061
{
    /// <summary>
    /// モジュール全体で使用するデータアクセスクラスです。
    /// </summary>
    public class ZMMR1061PR
    {
        #region private変数
        /// <summary>
        /// ZAMR2041(条件画面)のオブジェクト(設定内容の取得のため)
        /// </summary>
        ZMMR1061 _pForm;

        /// <summary>
        /// ユーザー情報
        /// </summary>
        UserInfo _uInfo;

        /// <summary>
        /// データアクセスオブジェクト
        /// </summary>
        DbAccess _dba;

        /// <summary>
        /// ユニークID
        /// </summary>
        string _unqId;
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="uInfo">操作中のユーザーの情報</param>
        /// <param name="dba">呼び出し元で保持するデータアクセスオブジェクト</param>
        public ZMMR1061PR(UserInfo uInfo, DbAccess dba, string unuqId, ZMMR1061 frm)
        {
            this._uInfo = uInfo;
            this._dba = dba;
            this._unqId = unuqId;
            this._pForm = frm;
        }
        #endregion

        #region publicメソッド
        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        public void DoPrint(bool isPreview, bool isPdf = false, bool isExcel = false, bool isCsv = false)
        {

            try
            {
                bool dataFlag;

                this._dba.BeginTransaction();

                //// 帳票出力用にワークテーブルにデータを作成
                dataFlag = MakeWkData();
                //DataTable dtOutput = MakeWkData();
                //dataFlag = InsertWkData(dtOutput);

                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    StringBuilder cols = new StringBuilder();
                    cols.Append("  ITEM01");
                    cols.Append(" ,ITEM02");
                    cols.Append(" ,ITEM03");
                    cols.Append(" ,ITEM04");
                    cols.Append(" ,ITEM05");
                    cols.Append(" ,ITEM06");
                    cols.Append(" ,ITEM07");
                    cols.Append(" ,ITEM08");
                    cols.Append(" ,ITEM09");
                    cols.Append(" ,ITEM10");
                    cols.Append(" ,ITEM11");
                    cols.Append(" ,ITEM12");
                    cols.Append(" ,ITEM13");
                    cols.Append(" ,ITEM14");

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this._unqId);

                    // データの取得
                    DataTable dtOutput = this._dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_ZM_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    // 帳票オブジェクトをインスタンス化
                    ZMMR10611R rpt = new ZMMR10611R(dtOutput);

                    rpt.Document.Printer.DocumentName = Util.ToString(this._pForm.Condition["ReportName"]);
                    rpt.Document.Name = Util.ToString(this._pForm.Condition["ReportName"]);

                    if (isExcel)
                    {
                        GrapeCity.ActiveReports.Export.Excel.Section.XlsExport xlsExport1 = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
                        //SetExcelSetting(xlsExport1);
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Zai, rpt.Document.Name, 2);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            xlsExport1.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("EXCEL出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPdf)
                    {
                        GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport p = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Zai, rpt.Document.Name, 1);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            p.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("PDF出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPreview)
                    {
                        // プレビュー画面表示
                        PreviewForm pFrm = new PreviewForm(rpt, this._unqId);
                        pFrm.WindowState = FormWindowState.Maximized;
                        pFrm.Show();
                    }
                    else
                    {
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
            }
            /*bool dataFlag;
            try
            {
                this._dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                dataFlag = MakeWkData();
                this._dba.Commit();
            }
            finally
            {
                this._dba.Rollback();
            }

            // 帳票出力
            if (dataFlag)
            {
                Report rpt = new Report();
                rpt.OutputReport(Path.Combine(Util.GetPath(), Constants.REP_DIR, "ZAMR2041.mdb"), "R_ZAMR2041", this._unqId, isPreview);
            }

            // ワークテーブルに作成したデータを削除
            try
            {
                this._dba.BeginTransaction();

                // 帳票出力用に作成したデータを削除
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this._unqId);
                this._dba.Delete("PR_ZM_TBL", "GUID = @GUID", dpc);
                this._dba.Commit();
            }*/
            finally
            {
                this._dba.Rollback();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {
            DataTable tmpHojoData = this._pForm.BumonData;

            // 仕訳対象データが存在する場合
            if (this._pForm.BumonData.Rows.Count > 0)
            {
                int i = 0; // ループカウント変数
                string zeiHyojiJoho = Util.ToString(this._pForm.Condition["ShohizeiShori"]); // 税表示情報を取得
                // 期間の表示
                string[] tmpDateFr = Util.ConvJpDate(Util.ToDate(this._pForm.Condition["DtFr"]), this._dba);
                string[] tmpDateTo = Util.ConvJpDate(Util.ToDate(this._pForm.Condition["DtTo"]), this._dba);
                // 出力日付
                string[] tmpnowDate = Util.ConvJpDate(System.DateTime.Today, this._dba);

                // 部門範囲を表示 ※開始と終了が未設定の場合は、【全社】
                string bumonRange;
                string bumonNmFr = Util.ToString(this._pForm.Condition["BumonNmFr"]);
                string bumonNmTo = Util.ToString(this._pForm.Condition["BumonNmTo"]);
                if (bumonNmFr == "先　頭" && bumonNmTo == "最　後")
                {
                    bumonRange = "【全社】";
                }
                else
                {
                    bumonRange = bumonNmFr + "　～　" + bumonNmTo;
                }

                #region データを印刷ワークテーブルに登録
                DbParamCollection dpc = new DbParamCollection();
                StringBuilder Sql = new StringBuilder();
                foreach (DataRow dr in tmpHojoData.Rows)
                {
                    Sql = new StringBuilder();
                    dpc = new DbParamCollection();
                    Sql.Append("INSERT INTO PR_ZM_TBL(");
                    Sql.Append("  GUID");
                    Sql.Append(" ,SORT");
                    Sql.Append(" ,ITEM01");
                    Sql.Append(" ,ITEM02");
                    Sql.Append(" ,ITEM03");
                    Sql.Append(" ,ITEM04");
                    Sql.Append(" ,ITEM05");
                    Sql.Append(" ,ITEM06");
                    Sql.Append(" ,ITEM07");
                    Sql.Append(" ,ITEM08");
                    Sql.Append(" ,ITEM09");
                    Sql.Append(" ,ITEM10");
                    Sql.Append(" ,ITEM11");
                    Sql.Append(" ,ITEM12");
                    Sql.Append(" ,ITEM13");
                    Sql.Append(" ,ITEM14");
                    Sql.Append(") ");
                    Sql.Append("VALUES(");
                    Sql.Append("  @GUID");
                    Sql.Append(" ,@SORT");
                    Sql.Append(" ,@ITEM01");
                    Sql.Append(" ,@ITEM02");
                    Sql.Append(" ,@ITEM03");
                    Sql.Append(" ,@ITEM04");
                    Sql.Append(" ,@ITEM05");
                    Sql.Append(" ,@ITEM06");
                    Sql.Append(" ,@ITEM07");
                    Sql.Append(" ,@ITEM08");
                    Sql.Append(" ,@ITEM09");
                    Sql.Append(" ,@ITEM10");
                    Sql.Append(" ,@ITEM11");
                    Sql.Append(" ,@ITEM12");
                    Sql.Append(" ,@ITEM13");
                    Sql.Append(" ,@ITEM14");
                    Sql.Append(") ");

                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this._unqId);
                    dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, tmpDateFr[5]);
                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, tmpDateTo[5]);
                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, this._uInfo.KaishaNm);
                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, zeiHyojiJoho);
                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, tmpHojoData.Rows[i]["コード"]);
                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, tmpHojoData.Rows[i]["勘定科目名"]);
                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, Util.FormatNum(tmpHojoData.Rows[i]["前月残高"]));
                    dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, Util.FormatNum(tmpHojoData.Rows[i]["借方"]));
                    dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, Util.FormatNum(tmpHojoData.Rows[i]["貸方"]));
                    dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, Util.FormatNum(tmpHojoData.Rows[i]["残高"]));
                    if (Util.ToString(tmpHojoData.Rows[i]["FLG"]) == "1")
                    {
                        dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, "1");
                    }
                    else
                    {
                        dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, "");
                    }
                    // 出力日付
                    dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, tmpnowDate[5]);
                    // 支所コード
                    dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200, tmpHojoData.Rows[i]["SHISHO_CD"]);
                    // 部門条件
                    dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, bumonRange);

                    this._dba.ModifyBySql(Util.ToString(Sql), dpc);
                    i++;
                }
            }
                #endregion
            return true;
        }
        #endregion
    }
}

