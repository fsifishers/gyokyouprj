﻿namespace jp.co.fsi.zm.zmmr1061
{
    partial class ZMMR1062
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvList = new System.Windows.Forms.DataGridView();
            this.lblCdKei = new System.Windows.Forms.Label();
            this.lblSoGokei = new System.Windows.Forms.Label();
            this.lblZenZanKei = new System.Windows.Forms.Label();
            this.lblKarikataKei = new System.Windows.Forms.Label();
            this.lblKashikataKei = new System.Windows.Forms.Label();
            this.lblZandakaKei = new System.Windows.Forms.Label();
            this.lblJp = new System.Windows.Forms.Label();
            this.lblZei = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(800, 23);
            this.lblTitle.Text = "";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(5, 431);
            this.pnlDebug.Size = new System.Drawing.Size(833, 100);
            // 
            // dgvList
            // 
            this.dgvList.AllowUserToAddRows = false;
            this.dgvList.AllowUserToDeleteRows = false;
            this.dgvList.AllowUserToResizeColumns = false;
            this.dgvList.AllowUserToResizeRows = false;
            this.dgvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvList.Location = new System.Drawing.Point(12, 50);
            this.dgvList.MultiSelect = false;
            this.dgvList.Name = "dgvList";
            this.dgvList.ReadOnly = true;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.MenuHighlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvList.RowHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvList.RowHeadersVisible = false;
            this.dgvList.RowTemplate.Height = 21;
            this.dgvList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvList.Size = new System.Drawing.Size(799, 402);
            this.dgvList.TabIndex = 1;
            // 
            // lblCdKei
            // 
            this.lblCdKei.BackColor = System.Drawing.Color.Silver;
            this.lblCdKei.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCdKei.Location = new System.Drawing.Point(12, 451);
            this.lblCdKei.Name = "lblCdKei";
            this.lblCdKei.Size = new System.Drawing.Size(62, 20);
            this.lblCdKei.TabIndex = 2;
            // 
            // lblSoGokei
            // 
            this.lblSoGokei.BackColor = System.Drawing.Color.Silver;
            this.lblSoGokei.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblSoGokei.Font = new System.Drawing.Font("ＭＳ ゴシック", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSoGokei.Location = new System.Drawing.Point(73, 451);
            this.lblSoGokei.Name = "lblSoGokei";
            this.lblSoGokei.Size = new System.Drawing.Size(236, 20);
            this.lblSoGokei.TabIndex = 3;
            this.lblSoGokei.Text = "総 合 計";
            this.lblSoGokei.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblZenZanKei
            // 
            this.lblZenZanKei.BackColor = System.Drawing.Color.Transparent;
            this.lblZenZanKei.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblZenZanKei.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblZenZanKei.Location = new System.Drawing.Point(308, 451);
            this.lblZenZanKei.Name = "lblZenZanKei";
            this.lblZenZanKei.Size = new System.Drawing.Size(121, 20);
            this.lblZenZanKei.TabIndex = 4;
            this.lblZenZanKei.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblKarikataKei
            // 
            this.lblKarikataKei.BackColor = System.Drawing.Color.Transparent;
            this.lblKarikataKei.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblKarikataKei.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKarikataKei.Location = new System.Drawing.Point(428, 451);
            this.lblKarikataKei.Name = "lblKarikataKei";
            this.lblKarikataKei.Size = new System.Drawing.Size(121, 20);
            this.lblKarikataKei.TabIndex = 5;
            this.lblKarikataKei.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblKashikataKei
            // 
            this.lblKashikataKei.BackColor = System.Drawing.Color.Transparent;
            this.lblKashikataKei.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblKashikataKei.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKashikataKei.Location = new System.Drawing.Point(548, 451);
            this.lblKashikataKei.Name = "lblKashikataKei";
            this.lblKashikataKei.Size = new System.Drawing.Size(121, 20);
            this.lblKashikataKei.TabIndex = 6;
            this.lblKashikataKei.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblZandakaKei
            // 
            this.lblZandakaKei.BackColor = System.Drawing.Color.Transparent;
            this.lblZandakaKei.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblZandakaKei.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblZandakaKei.Location = new System.Drawing.Point(668, 451);
            this.lblZandakaKei.Name = "lblZandakaKei";
            this.lblZandakaKei.Size = new System.Drawing.Size(121, 20);
            this.lblZandakaKei.TabIndex = 7;
            this.lblZandakaKei.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblJp
            // 
            this.lblJp.BackColor = System.Drawing.Color.Transparent;
            this.lblJp.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJp.ForeColor = System.Drawing.Color.Black;
            this.lblJp.Location = new System.Drawing.Point(412, 14);
            this.lblJp.Name = "lblJp";
            this.lblJp.Size = new System.Drawing.Size(303, 20);
            this.lblJp.TabIndex = 908;
            this.lblJp.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblZei
            // 
            this.lblZei.BackColor = System.Drawing.Color.Transparent;
            this.lblZei.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblZei.ForeColor = System.Drawing.Color.Black;
            this.lblZei.Location = new System.Drawing.Point(721, 14);
            this.lblZei.Name = "lblZei";
            this.lblZei.Size = new System.Drawing.Size(78, 20);
            this.lblZei.TabIndex = 913;
            this.lblZei.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ZMMR1062
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(825, 534);
            this.Controls.Add(this.lblZandakaKei);
            this.Controls.Add(this.lblKashikataKei);
            this.Controls.Add(this.lblKarikataKei);
            this.Controls.Add(this.lblZenZanKei);
            this.Controls.Add(this.lblSoGokei);
            this.Controls.Add(this.dgvList);
            this.Controls.Add(this.lblCdKei);
            this.Controls.Add(this.lblZei);
            this.Controls.Add(this.lblJp);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "ZMMR1062";
            this.ShowFButton = true;
            this.Text = "";
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblJp, 0);
            this.Controls.SetChildIndex(this.lblZei, 0);
            this.Controls.SetChildIndex(this.lblCdKei, 0);
            this.Controls.SetChildIndex(this.dgvList, 0);
            this.Controls.SetChildIndex(this.lblSoGokei, 0);
            this.Controls.SetChildIndex(this.lblZenZanKei, 0);
            this.Controls.SetChildIndex(this.lblKarikataKei, 0);
            this.Controls.SetChildIndex(this.lblKashikataKei, 0);
            this.Controls.SetChildIndex(this.lblZandakaKei, 0);
            this.pnlDebug.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvList;
        private System.Windows.Forms.Label lblCdKei;
        private System.Windows.Forms.Label lblSoGokei;
        private System.Windows.Forms.Label lblZenZanKei;
        private System.Windows.Forms.Label lblKarikataKei;
        private System.Windows.Forms.Label lblKashikataKei;
        private System.Windows.Forms.Label lblZandakaKei;
        private System.Windows.Forms.Label lblJp;
        private System.Windows.Forms.Label lblZei;



    }
}