﻿namespace jp.co.fsi.zm.zmmr1051
{
    partial class ZMMR1053
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.gbxYoyaku = new System.Windows.Forms.GroupBox();
            this.gbxMoji = new System.Windows.Forms.GroupBox();
            this.lblMemo = new System.Windows.Forms.Label();
            this.txtMoji = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMoji = new System.Windows.Forms.Label();
            this.gbxKanjoKamoku = new System.Windows.Forms.GroupBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnDel = new System.Windows.Forms.Button();
            this.gbxKanjoKamokuIchiran = new System.Windows.Forms.GroupBox();
            this.lbxKanjoKamokuIchiran = new System.Windows.Forms.ListBox();
            this.gbxTaishoKanjoKamoku = new System.Windows.Forms.GroupBox();
            this.lbxTaishoKanjoKamoku = new System.Windows.Forms.ListBox();
            this.gbxTaishaku = new System.Windows.Forms.GroupBox();
            this.rdoKashi = new System.Windows.Forms.RadioButton();
            this.rdoKari = new System.Windows.Forms.RadioButton();
            this.txtKamokuNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.gbxYoyakuList = new System.Windows.Forms.GroupBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colBS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colKMKb = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colBSKbn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlDebug.SuspendLayout();
            this.gbxYoyaku.SuspendLayout();
            this.gbxMoji.SuspendLayout();
            this.gbxKanjoKamoku.SuspendLayout();
            this.gbxKanjoKamokuIchiran.SuspendLayout();
            this.gbxTaishoKanjoKamoku.SuspendLayout();
            this.gbxTaishaku.SuspendLayout();
            this.gbxYoyakuList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(641, 23);
            this.lblTitle.Text = "総勘定元帳 [印字設定]";
            this.lblTitle.Visible = false;
            // 
            // btnEsc
            // 
            this.btnEsc.Location = new System.Drawing.Point(3, 49);
            // 
            // btnF1
            // 
            this.btnF1.Visible = false;
            // 
            // btnF2
            // 
            this.btnF2.Visible = false;
            // 
            // btnF3
            // 
            this.btnF3.Visible = false;
            // 
            // btnF4
            // 
            this.btnF4.Visible = false;
            // 
            // btnF5
            // 
            this.btnF5.Visible = false;
            // 
            // btnF6
            // 
            this.btnF6.Location = new System.Drawing.Point(67, 49);
            // 
            // btnF9
            // 
            this.btnF9.Visible = false;
            // 
            // btnF12
            // 
            this.btnF12.Visible = false;
            // 
            // btnF11
            // 
            this.btnF11.Visible = false;
            // 
            // btnF10
            // 
            this.btnF10.Visible = false;
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(5, 487);
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            // 
            // gbxYoyaku
            // 
            this.gbxYoyaku.Controls.Add(this.gbxMoji);
            this.gbxYoyaku.Controls.Add(this.gbxKanjoKamoku);
            this.gbxYoyaku.Controls.Add(this.gbxTaishaku);
            this.gbxYoyaku.Controls.Add(this.txtKamokuNm);
            this.gbxYoyaku.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxYoyaku.ForeColor = System.Drawing.Color.Black;
            this.gbxYoyaku.Location = new System.Drawing.Point(396, 12);
            this.gbxYoyaku.Name = "gbxYoyaku";
            this.gbxYoyaku.Size = new System.Drawing.Size(415, 517);
            this.gbxYoyaku.TabIndex = 0;
            this.gbxYoyaku.TabStop = false;
            this.gbxYoyaku.Text = "要約設定";
            // 
            // gbxMoji
            // 
            this.gbxMoji.Controls.Add(this.lblMemo);
            this.gbxMoji.Controls.Add(this.txtMoji);
            this.gbxMoji.Controls.Add(this.lblMoji);
            this.gbxMoji.Location = new System.Drawing.Point(9, 453);
            this.gbxMoji.Name = "gbxMoji";
            this.gbxMoji.Size = new System.Drawing.Size(395, 56);
            this.gbxMoji.TabIndex = 3;
            this.gbxMoji.TabStop = false;
            this.gbxMoji.Visible = false;
            // 
            // lblMemo
            // 
            this.lblMemo.BackColor = System.Drawing.Color.Silver;
            this.lblMemo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMemo.ForeColor = System.Drawing.Color.Black;
            this.lblMemo.Location = new System.Drawing.Point(157, 19);
            this.lblMemo.Name = "lblMemo";
            this.lblMemo.Size = new System.Drawing.Size(230, 27);
            this.lblMemo.TabIndex = 2;
            this.lblMemo.Text = "0：標準　1：太字";
            this.lblMemo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtMoji
            // 
            this.txtMoji.AutoSizeFromLength = false;
            this.txtMoji.DisplayLength = null;
            this.txtMoji.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMoji.ForeColor = System.Drawing.Color.Black;
            this.txtMoji.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtMoji.Location = new System.Drawing.Point(120, 23);
            this.txtMoji.MaxLength = 2;
            this.txtMoji.Name = "txtMoji";
            this.txtMoji.Size = new System.Drawing.Size(30, 20);
            this.txtMoji.TabIndex = 1;
            this.txtMoji.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMoji.Enter += new System.EventHandler(this.moji_Enter);
            this.txtMoji.KeyUp += new System.Windows.Forms.KeyEventHandler(this.moji_change);
            // 
            // lblMoji
            // 
            this.lblMoji.BackColor = System.Drawing.Color.Silver;
            this.lblMoji.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMoji.ForeColor = System.Drawing.Color.Black;
            this.lblMoji.Location = new System.Drawing.Point(10, 19);
            this.lblMoji.Name = "lblMoji";
            this.lblMoji.Size = new System.Drawing.Size(143, 27);
            this.lblMoji.TabIndex = 0;
            this.lblMoji.Text = "文字スタイル";
            this.lblMoji.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gbxKanjoKamoku
            // 
            this.gbxKanjoKamoku.Controls.Add(this.btnAdd);
            this.gbxKanjoKamoku.Controls.Add(this.btnDel);
            this.gbxKanjoKamoku.Controls.Add(this.gbxKanjoKamokuIchiran);
            this.gbxKanjoKamoku.Controls.Add(this.gbxTaishoKanjoKamoku);
            this.gbxKanjoKamoku.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxKanjoKamoku.ForeColor = System.Drawing.Color.Black;
            this.gbxKanjoKamoku.Location = new System.Drawing.Point(9, 61);
            this.gbxKanjoKamoku.Name = "gbxKanjoKamoku";
            this.gbxKanjoKamoku.Size = new System.Drawing.Size(396, 390);
            this.gbxKanjoKamoku.TabIndex = 2;
            this.gbxKanjoKamoku.TabStop = false;
            this.gbxKanjoKamoku.Text = "勘定科目設定";
            // 
            // btnAdd
            // 
            this.btnAdd.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnAdd.Location = new System.Drawing.Point(204, 184);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(40, 40);
            this.btnAdd.TabIndex = 3;
            this.btnAdd.Text = "▲";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnUMove_Click);
            // 
            // btnDel
            // 
            this.btnDel.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnDel.Location = new System.Drawing.Point(149, 184);
            this.btnDel.Name = "btnDel";
            this.btnDel.Size = new System.Drawing.Size(40, 40);
            this.btnDel.TabIndex = 2;
            this.btnDel.Text = "▼";
            this.btnDel.UseVisualStyleBackColor = true;
            this.btnDel.Click += new System.EventHandler(this.btnDMove_Click);
            // 
            // gbxKanjoKamokuIchiran
            // 
            this.gbxKanjoKamokuIchiran.Controls.Add(this.lbxKanjoKamokuIchiran);
            this.gbxKanjoKamokuIchiran.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxKanjoKamokuIchiran.ForeColor = System.Drawing.Color.Black;
            this.gbxKanjoKamokuIchiran.Location = new System.Drawing.Point(9, 231);
            this.gbxKanjoKamokuIchiran.Name = "gbxKanjoKamokuIchiran";
            this.gbxKanjoKamokuIchiran.Size = new System.Drawing.Size(378, 150);
            this.gbxKanjoKamokuIchiran.TabIndex = 1;
            this.gbxKanjoKamokuIchiran.TabStop = false;
            this.gbxKanjoKamokuIchiran.Text = "勘定科目一覧";
            // 
            // lbxKanjoKamokuIchiran
            // 
            this.lbxKanjoKamokuIchiran.FormattingEnabled = true;
            this.lbxKanjoKamokuIchiran.Location = new System.Drawing.Point(9, 19);
            this.lbxKanjoKamokuIchiran.Name = "lbxKanjoKamokuIchiran";
            this.lbxKanjoKamokuIchiran.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.lbxKanjoKamokuIchiran.Size = new System.Drawing.Size(360, 121);
            this.lbxKanjoKamokuIchiran.TabIndex = 0;
            // 
            // gbxTaishoKanjoKamoku
            // 
            this.gbxTaishoKanjoKamoku.Controls.Add(this.lbxTaishoKanjoKamoku);
            this.gbxTaishoKanjoKamoku.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxTaishoKanjoKamoku.ForeColor = System.Drawing.Color.Black;
            this.gbxTaishoKanjoKamoku.Location = new System.Drawing.Point(9, 22);
            this.gbxTaishoKanjoKamoku.Name = "gbxTaishoKanjoKamoku";
            this.gbxTaishoKanjoKamoku.Size = new System.Drawing.Size(378, 150);
            this.gbxTaishoKanjoKamoku.TabIndex = 0;
            this.gbxTaishoKanjoKamoku.TabStop = false;
            this.gbxTaishoKanjoKamoku.Text = "対象勘定科目";
            // 
            // lbxTaishoKanjoKamoku
            // 
            this.lbxTaishoKanjoKamoku.FormattingEnabled = true;
            this.lbxTaishoKanjoKamoku.Location = new System.Drawing.Point(9, 19);
            this.lbxTaishoKanjoKamoku.Name = "lbxTaishoKanjoKamoku";
            this.lbxTaishoKanjoKamoku.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.lbxTaishoKanjoKamoku.Size = new System.Drawing.Size(360, 121);
            this.lbxTaishoKanjoKamoku.TabIndex = 0;
            // 
            // gbxTaishaku
            // 
            this.gbxTaishaku.Controls.Add(this.rdoKashi);
            this.gbxTaishaku.Controls.Add(this.rdoKari);
            this.gbxTaishaku.Location = new System.Drawing.Point(277, 12);
            this.gbxTaishaku.Name = "gbxTaishaku";
            this.gbxTaishaku.Size = new System.Drawing.Size(128, 43);
            this.gbxTaishaku.TabIndex = 1;
            this.gbxTaishaku.TabStop = false;
            // 
            // rdoKashi
            // 
            this.rdoKashi.AutoSize = true;
            this.rdoKashi.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoKashi.ForeColor = System.Drawing.Color.Black;
            this.rdoKashi.Location = new System.Drawing.Point(74, 16);
            this.rdoKashi.Name = "rdoKashi";
            this.rdoKashi.Size = new System.Drawing.Size(53, 17);
            this.rdoKashi.TabIndex = 1;
            this.rdoKashi.TabStop = true;
            this.rdoKashi.Text = "貸方";
            this.rdoKashi.UseVisualStyleBackColor = true;
            this.rdoKashi.CheckedChanged += new System.EventHandler(this.rdo_CheckedChanged);
            this.rdoKashi.Click += new System.EventHandler(this.rdoKashi_Click);
            // 
            // rdoKari
            // 
            this.rdoKari.AutoSize = true;
            this.rdoKari.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoKari.ForeColor = System.Drawing.Color.Black;
            this.rdoKari.Location = new System.Drawing.Point(10, 16);
            this.rdoKari.Name = "rdoKari";
            this.rdoKari.Size = new System.Drawing.Size(53, 17);
            this.rdoKari.TabIndex = 0;
            this.rdoKari.TabStop = true;
            this.rdoKari.Text = "借方";
            this.rdoKari.UseVisualStyleBackColor = true;
            this.rdoKari.CheckedChanged += new System.EventHandler(this.rdo_CheckedChanged);
            this.rdoKari.Click += new System.EventHandler(this.rdoKari_Click);
            // 
            // txtKamokuNm
            // 
            this.txtKamokuNm.AutoSizeFromLength = false;
            this.txtKamokuNm.DisplayLength = null;
            this.txtKamokuNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKamokuNm.ForeColor = System.Drawing.Color.Black;
            this.txtKamokuNm.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtKamokuNm.Location = new System.Drawing.Point(9, 28);
            this.txtKamokuNm.MaxLength = 30;
            this.txtKamokuNm.Name = "txtKamokuNm";
            this.txtKamokuNm.Size = new System.Drawing.Size(262, 20);
            this.txtKamokuNm.TabIndex = 0;
            this.txtKamokuNm.KeyUp += new System.Windows.Forms.KeyEventHandler(this.kamokuNm_change);
            // 
            // gbxYoyakuList
            // 
            this.gbxYoyakuList.Controls.Add(this.dataGridView1);
            this.gbxYoyakuList.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxYoyakuList.Location = new System.Drawing.Point(5, 12);
            this.gbxYoyakuList.Name = "gbxYoyakuList";
            this.gbxYoyakuList.Size = new System.Drawing.Size(378, 517);
            this.gbxYoyakuList.TabIndex = 902;
            this.gbxYoyakuList.TabStop = false;
            this.gbxYoyakuList.Text = "要約設定";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gray;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.colCode,
            this.colName,
            this.dataGridViewTextBoxColumn4,
            this.colBS,
            this.colKMKb,
            this.colBSKbn,
            this.dataGridViewTextBoxColumn8});
            this.dataGridView1.Location = new System.Drawing.Point(5, 25);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowTemplate.Height = 21;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(364, 480);
            this.dataGridView1.TabIndex = 4;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            this.dataGridView1.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellEnter);
            this.dataGridView1.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridView1_CellFormatting);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Visible判断";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // colCode
            // 
            this.colCode.DataPropertyName = "Code";
            this.colCode.HeaderText = "科目コード";
            this.colCode.Name = "colCode";
            this.colCode.ReadOnly = true;
            this.colCode.Visible = false;
            // 
            // colName
            // 
            this.colName.DataPropertyName = "Name";
            this.colName.HeaderText = "科　目　名";
            this.colName.Name = "colName";
            this.colName.ReadOnly = true;
            this.colName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.colName.Width = 270;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "行番号";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Visible = false;
            // 
            // colBS
            // 
            this.colBS.DataPropertyName = "BS";
            this.colBS.HeaderText = "貸借";
            this.colBS.Name = "colBS";
            this.colBS.ReadOnly = true;
            this.colBS.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.colBS.Width = 70;
            // 
            // colKMKb
            // 
            this.colKMKb.DataPropertyName = "KMKb";
            this.colKMKb.HeaderText = "文字";
            this.colKMKb.Name = "colKMKb";
            this.colKMKb.ReadOnly = true;
            this.colKMKb.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.colKMKb.Visible = false;
            this.colKMKb.Width = 70;
            // 
            // colBSKbn
            // 
            this.colBSKbn.DataPropertyName = "BSKbn";
            this.colBSKbn.HeaderText = "削除と追加用行番号";
            this.colBSKbn.Name = "colBSKbn";
            this.colBSKbn.ReadOnly = true;
            this.colBSKbn.Visible = false;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.HeaderText = "表示順位";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.Visible = false;
            // 
            // ZMMR1053
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 590);
            this.Controls.Add(this.gbxYoyakuList);
            this.Controls.Add(this.gbxYoyaku);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "ZMMR1053";
            this.ShowFButton = true;
            this.Text = "";
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.gbxYoyaku, 0);
            this.Controls.SetChildIndex(this.gbxYoyakuList, 0);
            this.pnlDebug.ResumeLayout(false);
            this.gbxYoyaku.ResumeLayout(false);
            this.gbxYoyaku.PerformLayout();
            this.gbxMoji.ResumeLayout(false);
            this.gbxMoji.PerformLayout();
            this.gbxKanjoKamoku.ResumeLayout(false);
            this.gbxKanjoKamokuIchiran.ResumeLayout(false);
            this.gbxTaishoKanjoKamoku.ResumeLayout(false);
            this.gbxTaishaku.ResumeLayout(false);
            this.gbxTaishaku.PerformLayout();
            this.gbxYoyakuList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox gbxYoyaku;
        private System.Windows.Forms.GroupBox gbxTaishaku;
        private System.Windows.Forms.RadioButton rdoKashi;
        private System.Windows.Forms.RadioButton rdoKari;
        private jp.co.fsi.common.controls.FsiTextBox txtKamokuNm;
        private System.Windows.Forms.GroupBox gbxKanjoKamoku;
        private System.Windows.Forms.GroupBox gbxTaishoKanjoKamoku;
        private System.Windows.Forms.GroupBox gbxKanjoKamokuIchiran;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnDel;
        private System.Windows.Forms.GroupBox gbxMoji;
        private System.Windows.Forms.Label lblMemo;
        private jp.co.fsi.common.controls.FsiTextBox txtMoji;
        private System.Windows.Forms.Label lblMoji;
        private System.Windows.Forms.ListBox lbxKanjoKamokuIchiran;
        private System.Windows.Forms.ListBox lbxTaishoKanjoKamoku;
        private System.Windows.Forms.GroupBox gbxYoyakuList;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn colName;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn colBS;
        private System.Windows.Forms.DataGridViewTextBoxColumn colKMKb;
        private System.Windows.Forms.DataGridViewTextBoxColumn colBSKbn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
    }
}