﻿using System;
using System.Data;

using jp.co.fsi.common.report;
using jp.co.fsi.common.util;

namespace jp.co.fsi.zm.zmmr1051
{
    /// <summary>
    /// ZMMR10511Rの帳票
    /// </summary>
    public partial class ZMMR10511R : BaseReport
    {
        public ZMMR10511R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }

        /// <summary>
        /// ページヘッダーの設定
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pageHeader_Format(object sender, EventArgs e)
        {
            txtToday.Text = DateTime.Now.ToString("yyyy-M-d");
        }

        /// <summary>
        /// ページフッターの設定
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void reportFooter1_Format(object sender, EventArgs e)
        {
            //// 値が0となる場合は、空白にする
            //if (txtZeinukiKingakuTotal.Text == "0")
            //{
            //    txtZeinukiKingakuTotal.Text = "";
            //}
            //if (txtShohizeiTotal.Text == "0")
            //{
            //    txtShohizeiTotal.Text = "";
            //}
            //if (txtKeiTotal.Text == "0")
            //{
            //    txtKeiTotal.Text = "";
            //}
            //if (txtValue01Total.Text == "0")
            //{
            //    txtValue01Total.Text = "";
            //}
            //if (txtValue02Total.Text == "0")
            //{
            //    txtValue02Total.Text = "";
            //}
            //if (txtValue03Total.Text == "0")
            //{
            //    txtValue03Total.Text = "";
            //}
            //if (txtValue04Total.Text == "0")
            //{
            //    txtValue04Total.Text = "";
            //}
            //if (txtValue05Total.Text == "0")
            //{
            //    txtValue05Total.Text = "";
            //}
            //if (txtValue06Total.Text == "0")
            //{
            //    txtValue06Total.Text = "";
            //}
            //if (txtValue07Total.Text == "0")
            //{
            //    txtValue07Total.Text = "";
            //}
            //if (txtValue08Total.Text == "0")
            //{
            //    txtValue08Total.Text = "";
            //}
            //if (txtValue09Total.Text == "0")
            //{
            //    txtValue09Total.Text = "";
            //}

            //try
            //{
            //    // 値が0となる場合は、空白にする
            //    if (TaxUtil.ToDecimal(txtKei.Text) == 0)
            //        txtKei.Text = "";
            //    else
            //        txtKei.Text = Util.FormatNum(TaxUtil.ToDecimal(txtKei.Text), 1);
            //    if (TaxUtil.ToDecimal(textBox2.Text) == 0)
            //        textBox2.Text = "";
            //    else
            //        textBox2.Text = Util.FormatNum(TaxUtil.ToDecimal(textBox2.Text), 1);
            //    if (TaxUtil.ToDecimal(textBox21.Text) == 0)
            //        textBox21.Text = "";
            //    else
            //        textBox21.Text = Util.FormatNum(TaxUtil.ToDecimal(textBox21.Text), 1);
            //    if (TaxUtil.ToDecimal(textBox22.Text) == 0)
            //        textBox22.Text = "";
            //    else
            //        textBox22.Text = Util.FormatNum(TaxUtil.ToDecimal(textBox22.Text), 1);
            //    if (TaxUtil.ToDecimal(textBox25.Text) == 0)
            //        textBox25.Text = "";
            //    else
            //        textBox25.Text = Util.FormatNum(TaxUtil.ToDecimal(textBox25.Text), 1);
            //    if (TaxUtil.ToDecimal(textBox26.Text) == 0)
            //        textBox26.Text = "";
            //    else
            //        textBox26.Text = Util.FormatNum(TaxUtil.ToDecimal(textBox26.Text), 1);
            //    if (TaxUtil.ToDecimal(textBox29.Text) == 0)
            //        textBox29.Text = "";
            //    else
            //        textBox29.Text = Util.FormatNum(TaxUtil.ToDecimal(textBox29.Text), 1);
            //    if (TaxUtil.ToDecimal(textBox30.Text) == 0)
            //        textBox30.Text = "";
            //    else
            //        textBox30.Text = Util.FormatNum(TaxUtil.ToDecimal(textBox30.Text), 1);
            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine(ex.Message);
            //}
        }

        private void detail_Format(object sender, EventArgs e)
        {
            if (txtKamokuNo.Text == "0")
            {
                shape1.Visible = true;
            }
            else
            {
                shape1.Visible = false;
            }
        }
    }
}
