﻿namespace jp.co.fsi.zm.zmmr1051
{
    partial class ZMMR1052
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.lblKaishaSettei = new System.Windows.Forms.Label();
            this.lblJp = new System.Windows.Forms.Label();
            this.lblZei = new System.Windows.Forms.Label();
            this.lblGridViewTitle = new System.Windows.Forms.Label();
            this.dgvInputList = new System.Windows.Forms.DataGridView();
            this.txtGridEdit = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtFunanushiCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.KamokuNm = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Zandaka = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KarikataHassei = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KashikataHassei = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TouZan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlDebug.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInputList)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.BackColor = System.Drawing.Color.White;
            this.lblTitle.Size = new System.Drawing.Size(743, 23);
            this.lblTitle.Text = "";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(5, 489);
            this.pnlDebug.Size = new System.Drawing.Size(760, 100);
            // 
            // lblKaishaSettei
            // 
            this.lblKaishaSettei.BackColor = System.Drawing.Color.Transparent;
            this.lblKaishaSettei.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKaishaSettei.ForeColor = System.Drawing.Color.Black;
            this.lblKaishaSettei.Location = new System.Drawing.Point(14, 13);
            this.lblKaishaSettei.Name = "lblKaishaSettei";
            this.lblKaishaSettei.Size = new System.Drawing.Size(141, 20);
            this.lblKaishaSettei.TabIndex = 0;
            this.lblKaishaSettei.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblJp
            // 
            this.lblJp.BackColor = System.Drawing.Color.Transparent;
            this.lblJp.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJp.ForeColor = System.Drawing.Color.Black;
            this.lblJp.Location = new System.Drawing.Point(356, 14);
            this.lblJp.Name = "lblJp";
            this.lblJp.Size = new System.Drawing.Size(303, 20);
            this.lblJp.TabIndex = 4;
            this.lblJp.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblZei
            // 
            this.lblZei.BackColor = System.Drawing.Color.Transparent;
            this.lblZei.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblZei.ForeColor = System.Drawing.Color.Black;
            this.lblZei.Location = new System.Drawing.Point(661, 14);
            this.lblZei.Name = "lblZei";
            this.lblZei.Size = new System.Drawing.Size(78, 20);
            this.lblZei.TabIndex = 5;
            this.lblZei.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblGridViewTitle
            // 
            this.lblGridViewTitle.BackColor = System.Drawing.Color.Transparent;
            this.lblGridViewTitle.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGridViewTitle.ForeColor = System.Drawing.Color.Black;
            this.lblGridViewTitle.Location = new System.Drawing.Point(239, 13);
            this.lblGridViewTitle.Name = "lblGridViewTitle";
            this.lblGridViewTitle.Size = new System.Drawing.Size(138, 20);
            this.lblGridViewTitle.TabIndex = 3;
            this.lblGridViewTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dgvInputList
            // 
            this.dgvInputList.AllowUserToAddRows = false;
            this.dgvInputList.AllowUserToDeleteRows = false;
            this.dgvInputList.AllowUserToResizeColumns = false;
            this.dgvInputList.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gray;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvInputList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvInputList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvInputList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.KamokuNm,
            this.Zandaka,
            this.KarikataHassei,
            this.KashikataHassei,
            this.TouZan});
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvInputList.DefaultCellStyle = dataGridViewCellStyle7;
            this.dgvInputList.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvInputList.Location = new System.Drawing.Point(13, 39);
            this.dgvInputList.MultiSelect = false;
            this.dgvInputList.Name = "dgvInputList";
            this.dgvInputList.RowHeadersVisible = false;
            this.dgvInputList.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvInputList.RowTemplate.Height = 21;
            this.dgvInputList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvInputList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvInputList.Size = new System.Drawing.Size(726, 493);
            this.dgvInputList.TabIndex = 902;
            this.dgvInputList.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvInputList_CellEnter);
            this.dgvInputList.Scroll += new System.Windows.Forms.ScrollEventHandler(this.dgvInputList_Scroll);
            this.dgvInputList.Enter += new System.EventHandler(this.dgvInputList_Enter);
            // 
            // txtGridEdit
            // 
            this.txtGridEdit.AutoSizeFromLength = true;
            this.txtGridEdit.BackColor = System.Drawing.Color.White;
            this.txtGridEdit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtGridEdit.DisplayLength = null;
            this.txtGridEdit.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGridEdit.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtGridEdit.Location = new System.Drawing.Point(445, 13);
            this.txtGridEdit.MaxLength = 10;
            this.txtGridEdit.Name = "txtGridEdit";
            this.txtGridEdit.Size = new System.Drawing.Size(76, 20);
            this.txtGridEdit.TabIndex = 904;
            this.txtGridEdit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGridEdit.Enter += new System.EventHandler(this.txtGridEdit_Enter);
            this.txtGridEdit.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtGridEdit_KeyDown);
            // 
            // txtFunanushiCd
            // 
            this.txtFunanushiCd.AutoSizeFromLength = true;
            this.txtFunanushiCd.DisplayLength = null;
            this.txtFunanushiCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFunanushiCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtFunanushiCd.Location = new System.Drawing.Point(359, 13);
            this.txtFunanushiCd.MaxLength = 4;
            this.txtFunanushiCd.Name = "txtFunanushiCd";
            this.txtFunanushiCd.Size = new System.Drawing.Size(0, 20);
            this.txtFunanushiCd.TabIndex = 905;
            this.txtFunanushiCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // KamokuNm
            // 
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            this.KamokuNm.DefaultCellStyle = dataGridViewCellStyle2;
            this.KamokuNm.HeaderText = "ｺｰﾄﾞ";
            this.KamokuNm.Name = "KamokuNm";
            this.KamokuNm.ReadOnly = true;
            this.KamokuNm.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.KamokuNm.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.KamokuNm.Width = 50;
            // 
            // Zandaka
            // 
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            this.Zandaka.DefaultCellStyle = dataGridViewCellStyle3;
            this.Zandaka.HeaderText = "勘 定 科 目 名";
            this.Zandaka.Name = "Zandaka";
            this.Zandaka.ReadOnly = true;
            this.Zandaka.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Zandaka.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Zandaka.Width = 190;
            // 
            // KarikataHassei
            // 
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            this.KarikataHassei.DefaultCellStyle = dataGridViewCellStyle4;
            this.KarikataHassei.HeaderText = "科 目 分 類 名";
            this.KarikataHassei.Name = "KarikataHassei";
            this.KarikataHassei.ReadOnly = true;
            this.KarikataHassei.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.KarikataHassei.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.KarikataHassei.Width = 190;
            // 
            // KashikataHassei
            // 
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            this.KashikataHassei.DefaultCellStyle = dataGridViewCellStyle5;
            this.KashikataHassei.HeaderText = "ｺｰﾄﾞ";
            this.KashikataHassei.Name = "KashikataHassei";
            this.KashikataHassei.ReadOnly = true;
            this.KashikataHassei.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.KashikataHassei.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.KashikataHassei.Width = 50;
            // 
            // TouZan
            // 
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            this.TouZan.DefaultCellStyle = dataGridViewCellStyle6;
            this.TouZan.HeaderText = "部　門　名";
            this.TouZan.Name = "TouZan";
            this.TouZan.ReadOnly = true;
            this.TouZan.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.TouZan.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.TouZan.Width = 190;
            // 
            // ZMMR1052
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(752, 592);
            this.Controls.Add(this.txtFunanushiCd);
            this.Controls.Add(this.txtGridEdit);
            this.Controls.Add(this.lblGridViewTitle);
            this.Controls.Add(this.dgvInputList);
            this.Controls.Add(this.lblZei);
            this.Controls.Add(this.lblJp);
            this.Controls.Add(this.lblKaishaSettei);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "ZMMR1052";
            this.ShowFButton = true;
            this.Text = "";
            this.Shown += new System.EventHandler(this.ZAMR9012_Shown);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblKaishaSettei, 0);
            this.Controls.SetChildIndex(this.lblJp, 0);
            this.Controls.SetChildIndex(this.lblZei, 0);
            this.Controls.SetChildIndex(this.dgvInputList, 0);
            this.Controls.SetChildIndex(this.lblGridViewTitle, 0);
            this.Controls.SetChildIndex(this.txtGridEdit, 0);
            this.Controls.SetChildIndex(this.txtFunanushiCd, 0);
            this.pnlDebug.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvInputList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblKaishaSettei;
        private System.Windows.Forms.Label lblJp;
        private System.Windows.Forms.Label lblZei;
        private System.Windows.Forms.Label lblGridViewTitle;
        private System.Windows.Forms.DataGridView dgvInputList;
        private jp.co.fsi.common.controls.FsiTextBox txtGridEdit;
        private jp.co.fsi.common.controls.FsiTextBox txtFunanushiCd;
        private System.Windows.Forms.DataGridViewTextBoxColumn KamokuNm;
        private System.Windows.Forms.DataGridViewTextBoxColumn Zandaka;
        private System.Windows.Forms.DataGridViewTextBoxColumn KarikataHassei;
        private System.Windows.Forms.DataGridViewTextBoxColumn KashikataHassei;
        private System.Windows.Forms.DataGridViewTextBoxColumn TouZan;
    }
}