﻿namespace jp.co.fsi.zm.zmde1031
{
    partial class ZMDE1032
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rdoKessanKubunAll = new System.Windows.Forms.RadioButton();
            this.rdoKessanKubun1 = new System.Windows.Forms.RadioButton();
            this.rdoKessanKubun0 = new System.Windows.Forms.RadioButton();
            this.lblDayDenpyoDateTo = new System.Windows.Forms.Label();
            this.txtDayDenpyoDateTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblDenpyoDateTo = new System.Windows.Forms.Label();
            this.lblMonthDenpyoDateTo = new System.Windows.Forms.Label();
            this.lblYearDenpyoDateTo = new System.Windows.Forms.Label();
            this.txtMonthDenpyoDateTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGengoDenpyoDateTo = new System.Windows.Forms.Label();
            this.txtGengoYearDenpyoDateTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblDayDenpyoDateFr = new System.Windows.Forms.Label();
            this.txtDayDenpyoDateFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblDenpyoDateFr = new System.Windows.Forms.Label();
            this.lblMonthDenpyoDateFr = new System.Windows.Forms.Label();
            this.lblYearDenpyoDateFr = new System.Windows.Forms.Label();
            this.txtMonthDenpyoDateFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGengoDenpyoDateFr = new System.Windows.Forms.Label();
            this.txtGengoYearDenpyoDateFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.panel3 = new jp.co.fsi.common.FsiPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.panel2 = new jp.co.fsi.common.FsiPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.mtbList = new jp.co.fsi.common.controls.SjMultiTable();
            this.lblShohyoBango = new System.Windows.Forms.Label();
            this.txtShohyoBango = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKingaku = new System.Windows.Forms.Label();
            this.txtKingaku = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblTekiyo = new System.Windows.Forms.Label();
            this.txtTekiyo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblBumonCdTo = new System.Windows.Forms.Label();
            this.txtBumonCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblBumonCdFr = new System.Windows.Forms.Label();
            this.txtBumonCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKojiCdTo = new System.Windows.Forms.Label();
            this.txtKojiCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKojiCdFr = new System.Windows.Forms.Label();
            this.txtKojiCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKanjoKamokuCdTo = new System.Windows.Forms.Label();
            this.txtKanjoKamokuCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKanjoKamokuCdFr = new System.Windows.Forms.Label();
            this.txtKanjoKamokuCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblHojoKamokuCdTo = new System.Windows.Forms.Label();
            this.txtHojoKamokuCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblHojoKamokuCdFo = new System.Windows.Forms.Label();
            this.txtHojoKamokuCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblZeiKubunTo = new System.Windows.Forms.Label();
            this.txtZeiKubunTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblZeiKubunFr = new System.Windows.Forms.Label();
            this.txtZeiKubunFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblCondition = new System.Windows.Forms.Label();
            this.txtZeiRt = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblZeiRtPer = new System.Windows.Forms.Label();
            this.lblZeiRt = new System.Windows.Forms.Label();
            this.lblBakDenpyoDate = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Text = "伝票検索";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            // 
            // rdoKessanKubunAll
            // 
            this.rdoKessanKubunAll.AutoSize = true;
            this.rdoKessanKubunAll.BackColor = System.Drawing.Color.Silver;
            this.rdoKessanKubunAll.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoKessanKubunAll.Location = new System.Drawing.Point(735, 45);
            this.rdoKessanKubunAll.Name = "rdoKessanKubunAll";
            this.rdoKessanKubunAll.Size = new System.Drawing.Size(67, 17);
            this.rdoKessanKubunAll.TabIndex = 58;
            this.rdoKessanKubunAll.Text = "全仕訳";
            this.rdoKessanKubunAll.UseVisualStyleBackColor = false;
            // 
            // rdoKessanKubun1
            // 
            this.rdoKessanKubun1.AutoSize = true;
            this.rdoKessanKubun1.BackColor = System.Drawing.Color.Silver;
            this.rdoKessanKubun1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoKessanKubun1.Location = new System.Drawing.Point(638, 45);
            this.rdoKessanKubun1.Name = "rdoKessanKubun1";
            this.rdoKessanKubun1.Size = new System.Drawing.Size(81, 17);
            this.rdoKessanKubun1.TabIndex = 57;
            this.rdoKessanKubun1.Text = "決算仕訳";
            this.rdoKessanKubun1.UseVisualStyleBackColor = false;
            // 
            // rdoKessanKubun0
            // 
            this.rdoKessanKubun0.AutoSize = true;
            this.rdoKessanKubun0.BackColor = System.Drawing.Color.Silver;
            this.rdoKessanKubun0.Checked = true;
            this.rdoKessanKubun0.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoKessanKubun0.Location = new System.Drawing.Point(542, 45);
            this.rdoKessanKubun0.Name = "rdoKessanKubun0";
            this.rdoKessanKubun0.Size = new System.Drawing.Size(81, 17);
            this.rdoKessanKubun0.TabIndex = 56;
            this.rdoKessanKubun0.TabStop = true;
            this.rdoKessanKubun0.Text = "通常仕訳";
            this.rdoKessanKubun0.UseVisualStyleBackColor = false;
            // 
            // lblDayDenpyoDateTo
            // 
            this.lblDayDenpyoDateTo.AutoSize = true;
            this.lblDayDenpyoDateTo.BackColor = System.Drawing.Color.Silver;
            this.lblDayDenpyoDateTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDayDenpyoDateTo.Location = new System.Drawing.Point(470, 47);
            this.lblDayDenpyoDateTo.Name = "lblDayDenpyoDateTo";
            this.lblDayDenpyoDateTo.Size = new System.Drawing.Size(21, 13);
            this.lblDayDenpyoDateTo.TabIndex = 17;
            this.lblDayDenpyoDateTo.Text = "日";
            // 
            // txtDayDenpyoDateTo
            // 
            this.txtDayDenpyoDateTo.AutoSizeFromLength = false;
            this.txtDayDenpyoDateTo.DisplayLength = null;
            this.txtDayDenpyoDateTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDayDenpyoDateTo.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtDayDenpyoDateTo.Location = new System.Drawing.Point(444, 44);
            this.txtDayDenpyoDateTo.MaxLength = 2;
            this.txtDayDenpyoDateTo.Name = "txtDayDenpyoDateTo";
            this.txtDayDenpyoDateTo.Size = new System.Drawing.Size(22, 20);
            this.txtDayDenpyoDateTo.TabIndex = 16;
            this.txtDayDenpyoDateTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDayDenpyoDateTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDayDenpyoDateTo_Validating);
            // 
            // lblDenpyoDateTo
            // 
            this.lblDenpyoDateTo.AutoSize = true;
            this.lblDenpyoDateTo.BackColor = System.Drawing.Color.Silver;
            this.lblDenpyoDateTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDenpyoDateTo.Location = new System.Drawing.Point(276, 47);
            this.lblDenpyoDateTo.Name = "lblDenpyoDateTo";
            this.lblDenpyoDateTo.Size = new System.Drawing.Size(21, 13);
            this.lblDenpyoDateTo.TabIndex = 10;
            this.lblDenpyoDateTo.Text = "～";
            // 
            // lblMonthDenpyoDateTo
            // 
            this.lblMonthDenpyoDateTo.AutoSize = true;
            this.lblMonthDenpyoDateTo.BackColor = System.Drawing.Color.Silver;
            this.lblMonthDenpyoDateTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMonthDenpyoDateTo.Location = new System.Drawing.Point(421, 47);
            this.lblMonthDenpyoDateTo.Name = "lblMonthDenpyoDateTo";
            this.lblMonthDenpyoDateTo.Size = new System.Drawing.Size(21, 13);
            this.lblMonthDenpyoDateTo.TabIndex = 15;
            this.lblMonthDenpyoDateTo.Text = "月";
            // 
            // lblYearDenpyoDateTo
            // 
            this.lblYearDenpyoDateTo.AutoSize = true;
            this.lblYearDenpyoDateTo.BackColor = System.Drawing.Color.Silver;
            this.lblYearDenpyoDateTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYearDenpyoDateTo.Location = new System.Drawing.Point(373, 47);
            this.lblYearDenpyoDateTo.Name = "lblYearDenpyoDateTo";
            this.lblYearDenpyoDateTo.Size = new System.Drawing.Size(21, 13);
            this.lblYearDenpyoDateTo.TabIndex = 13;
            this.lblYearDenpyoDateTo.Text = "年";
            // 
            // txtMonthDenpyoDateTo
            // 
            this.txtMonthDenpyoDateTo.AutoSizeFromLength = false;
            this.txtMonthDenpyoDateTo.DisplayLength = null;
            this.txtMonthDenpyoDateTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMonthDenpyoDateTo.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtMonthDenpyoDateTo.Location = new System.Drawing.Point(396, 44);
            this.txtMonthDenpyoDateTo.MaxLength = 2;
            this.txtMonthDenpyoDateTo.Name = "txtMonthDenpyoDateTo";
            this.txtMonthDenpyoDateTo.Size = new System.Drawing.Size(22, 20);
            this.txtMonthDenpyoDateTo.TabIndex = 14;
            this.txtMonthDenpyoDateTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonthDenpyoDateTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthDenpyoDateTo_Validating);
            // 
            // lblGengoDenpyoDateTo
            // 
            this.lblGengoDenpyoDateTo.BackColor = System.Drawing.Color.Silver;
            this.lblGengoDenpyoDateTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengoDenpyoDateTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengoDenpyoDateTo.Location = new System.Drawing.Point(309, 44);
            this.lblGengoDenpyoDateTo.Name = "lblGengoDenpyoDateTo";
            this.lblGengoDenpyoDateTo.Size = new System.Drawing.Size(40, 20);
            this.lblGengoDenpyoDateTo.TabIndex = 11;
            this.lblGengoDenpyoDateTo.Text = "平成";
            this.lblGengoDenpyoDateTo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtGengoYearDenpyoDateTo
            // 
            this.txtGengoYearDenpyoDateTo.AutoSizeFromLength = false;
            this.txtGengoYearDenpyoDateTo.DisplayLength = null;
            this.txtGengoYearDenpyoDateTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGengoYearDenpyoDateTo.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtGengoYearDenpyoDateTo.Location = new System.Drawing.Point(351, 44);
            this.txtGengoYearDenpyoDateTo.MaxLength = 2;
            this.txtGengoYearDenpyoDateTo.Name = "txtGengoYearDenpyoDateTo";
            this.txtGengoYearDenpyoDateTo.Size = new System.Drawing.Size(22, 20);
            this.txtGengoYearDenpyoDateTo.TabIndex = 12;
            this.txtGengoYearDenpyoDateTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGengoYearDenpyoDateTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtGengoYearDenpyoDateTo_Validating);
            // 
            // lblDayDenpyoDateFr
            // 
            this.lblDayDenpyoDateFr.AutoSize = true;
            this.lblDayDenpyoDateFr.BackColor = System.Drawing.Color.Silver;
            this.lblDayDenpyoDateFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDayDenpyoDateFr.Location = new System.Drawing.Point(249, 47);
            this.lblDayDenpyoDateFr.Name = "lblDayDenpyoDateFr";
            this.lblDayDenpyoDateFr.Size = new System.Drawing.Size(21, 13);
            this.lblDayDenpyoDateFr.TabIndex = 9;
            this.lblDayDenpyoDateFr.Text = "日";
            // 
            // txtDayDenpyoDateFr
            // 
            this.txtDayDenpyoDateFr.AutoSizeFromLength = false;
            this.txtDayDenpyoDateFr.DisplayLength = null;
            this.txtDayDenpyoDateFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDayDenpyoDateFr.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtDayDenpyoDateFr.Location = new System.Drawing.Point(223, 44);
            this.txtDayDenpyoDateFr.MaxLength = 2;
            this.txtDayDenpyoDateFr.Name = "txtDayDenpyoDateFr";
            this.txtDayDenpyoDateFr.Size = new System.Drawing.Size(22, 20);
            this.txtDayDenpyoDateFr.TabIndex = 8;
            this.txtDayDenpyoDateFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDayDenpyoDateFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDayDenpyoDateFr_Validating);
            // 
            // lblDenpyoDateFr
            // 
            this.lblDenpyoDateFr.AutoSize = true;
            this.lblDenpyoDateFr.BackColor = System.Drawing.Color.Silver;
            this.lblDenpyoDateFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDenpyoDateFr.Location = new System.Drawing.Point(16, 47);
            this.lblDenpyoDateFr.Name = "lblDenpyoDateFr";
            this.lblDenpyoDateFr.Size = new System.Drawing.Size(63, 13);
            this.lblDenpyoDateFr.TabIndex = 2;
            this.lblDenpyoDateFr.Text = "伝票日付";
            // 
            // lblMonthDenpyoDateFr
            // 
            this.lblMonthDenpyoDateFr.AutoSize = true;
            this.lblMonthDenpyoDateFr.BackColor = System.Drawing.Color.Silver;
            this.lblMonthDenpyoDateFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMonthDenpyoDateFr.Location = new System.Drawing.Point(200, 47);
            this.lblMonthDenpyoDateFr.Name = "lblMonthDenpyoDateFr";
            this.lblMonthDenpyoDateFr.Size = new System.Drawing.Size(21, 13);
            this.lblMonthDenpyoDateFr.TabIndex = 7;
            this.lblMonthDenpyoDateFr.Text = "月";
            // 
            // lblYearDenpyoDateFr
            // 
            this.lblYearDenpyoDateFr.AutoSize = true;
            this.lblYearDenpyoDateFr.BackColor = System.Drawing.Color.Silver;
            this.lblYearDenpyoDateFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYearDenpyoDateFr.Location = new System.Drawing.Point(152, 47);
            this.lblYearDenpyoDateFr.Name = "lblYearDenpyoDateFr";
            this.lblYearDenpyoDateFr.Size = new System.Drawing.Size(21, 13);
            this.lblYearDenpyoDateFr.TabIndex = 5;
            this.lblYearDenpyoDateFr.Text = "年";
            // 
            // txtMonthDenpyoDateFr
            // 
            this.txtMonthDenpyoDateFr.AutoSizeFromLength = false;
            this.txtMonthDenpyoDateFr.DisplayLength = null;
            this.txtMonthDenpyoDateFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMonthDenpyoDateFr.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtMonthDenpyoDateFr.Location = new System.Drawing.Point(175, 44);
            this.txtMonthDenpyoDateFr.MaxLength = 2;
            this.txtMonthDenpyoDateFr.Name = "txtMonthDenpyoDateFr";
            this.txtMonthDenpyoDateFr.Size = new System.Drawing.Size(22, 20);
            this.txtMonthDenpyoDateFr.TabIndex = 6;
            this.txtMonthDenpyoDateFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonthDenpyoDateFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthDenpyoDateFr_Validating);
            // 
            // lblGengoDenpyoDateFr
            // 
            this.lblGengoDenpyoDateFr.BackColor = System.Drawing.Color.Silver;
            this.lblGengoDenpyoDateFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengoDenpyoDateFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengoDenpyoDateFr.Location = new System.Drawing.Point(88, 44);
            this.lblGengoDenpyoDateFr.Name = "lblGengoDenpyoDateFr";
            this.lblGengoDenpyoDateFr.Size = new System.Drawing.Size(40, 20);
            this.lblGengoDenpyoDateFr.TabIndex = 3;
            this.lblGengoDenpyoDateFr.Text = "平成";
            this.lblGengoDenpyoDateFr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtGengoYearDenpyoDateFr
            // 
            this.txtGengoYearDenpyoDateFr.AutoSizeFromLength = false;
            this.txtGengoYearDenpyoDateFr.DisplayLength = null;
            this.txtGengoYearDenpyoDateFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGengoYearDenpyoDateFr.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtGengoYearDenpyoDateFr.Location = new System.Drawing.Point(130, 44);
            this.txtGengoYearDenpyoDateFr.MaxLength = 2;
            this.txtGengoYearDenpyoDateFr.Name = "txtGengoYearDenpyoDateFr";
            this.txtGengoYearDenpyoDateFr.Size = new System.Drawing.Size(22, 20);
            this.txtGengoYearDenpyoDateFr.TabIndex = 4;
            this.txtGengoYearDenpyoDateFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGengoYearDenpyoDateFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtGengoYearDenpyoDateFr_Validating);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Transparent;
            this.panel3.Controls.Add(this.label2);
            this.panel3.Location = new System.Drawing.Point(469, 169);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(250, 19);
            this.panel3.TabIndex = 915;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(87, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 13);
            this.label2.TabIndex = 910;
            this.label2.Text = "貸　　　方";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.Controls.Add(this.label1);
            this.panel2.Location = new System.Drawing.Point(39, 169);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(250, 19);
            this.panel2.TabIndex = 914;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(87, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 13);
            this.label1.TabIndex = 910;
            this.label1.Text = "借　　　方";
            // 
            // mtbList
            // 
            this.mtbList.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.mtbList.FixedCols = 0;
            this.mtbList.FocusField = null;
            this.mtbList.Location = new System.Drawing.Point(12, 188);
            this.mtbList.Name = "mtbList";
            this.mtbList.NotSelectableCols = 0;
            this.mtbList.SelectRange = null;
            this.mtbList.Size = new System.Drawing.Size(816, 336);
            this.mtbList.TabIndex = 59;
            this.mtbList.Text = "sjMultiTable1";
            this.mtbList.UndoBufferEnabled = false;
            this.mtbList.RecordEnter += new systembase.table.UTable.RecordEnterEventHandler(this.mtbList_RecordEnter);
            this.mtbList.DoubleClick += new System.EventHandler(this.mtbList_DoubleClick);
            this.mtbList.Leave += new System.EventHandler(this.mtbList_Leave);
            this.mtbList.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.mtbList_PreviewKeyDown);
            // 
            // lblShohyoBango
            // 
            this.lblShohyoBango.AutoSize = true;
            this.lblShohyoBango.BackColor = System.Drawing.Color.Silver;
            this.lblShohyoBango.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohyoBango.Location = new System.Drawing.Point(545, 80);
            this.lblShohyoBango.Name = "lblShohyoBango";
            this.lblShohyoBango.Size = new System.Drawing.Size(63, 13);
            this.lblShohyoBango.TabIndex = 48;
            this.lblShohyoBango.Text = "証憑番号";
            // 
            // txtShohyoBango
            // 
            this.txtShohyoBango.AutoSizeFromLength = false;
            this.txtShohyoBango.DisplayLength = null;
            this.txtShohyoBango.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShohyoBango.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtShohyoBango.Location = new System.Drawing.Point(614, 75);
            this.txtShohyoBango.MaxLength = 10;
            this.txtShohyoBango.Name = "txtShohyoBango";
            this.txtShohyoBango.Size = new System.Drawing.Size(119, 23);
            this.txtShohyoBango.TabIndex = 49;
            this.txtShohyoBango.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohyoBango_Validating);
            // 
            // lblKingaku
            // 
            this.lblKingaku.AutoSize = true;
            this.lblKingaku.BackColor = System.Drawing.Color.Silver;
            this.lblKingaku.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKingaku.Location = new System.Drawing.Point(545, 112);
            this.lblKingaku.Name = "lblKingaku";
            this.lblKingaku.Size = new System.Drawing.Size(63, 13);
            this.lblKingaku.TabIndex = 51;
            this.lblKingaku.Text = "金　　額";
            // 
            // txtKingaku
            // 
            this.txtKingaku.AutoSizeFromLength = false;
            this.txtKingaku.DisplayLength = null;
            this.txtKingaku.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKingaku.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKingaku.Location = new System.Drawing.Point(614, 107);
            this.txtKingaku.MaxLength = 10;
            this.txtKingaku.Name = "txtKingaku";
            this.txtKingaku.Size = new System.Drawing.Size(119, 23);
            this.txtKingaku.TabIndex = 52;
            this.txtKingaku.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKingaku.Validating += new System.ComponentModel.CancelEventHandler(this.txtKingaku_Validating);
            // 
            // lblTekiyo
            // 
            this.lblTekiyo.AutoSize = true;
            this.lblTekiyo.BackColor = System.Drawing.Color.Silver;
            this.lblTekiyo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTekiyo.Location = new System.Drawing.Point(545, 145);
            this.lblTekiyo.Name = "lblTekiyo";
            this.lblTekiyo.Size = new System.Drawing.Size(63, 13);
            this.lblTekiyo.TabIndex = 54;
            this.lblTekiyo.Text = "摘　　要";
            // 
            // txtTekiyo
            // 
            this.txtTekiyo.AutoSizeFromLength = false;
            this.txtTekiyo.DisplayLength = null;
            this.txtTekiyo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTekiyo.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtTekiyo.Location = new System.Drawing.Point(614, 140);
            this.txtTekiyo.MaxLength = 40;
            this.txtTekiyo.Name = "txtTekiyo";
            this.txtTekiyo.Size = new System.Drawing.Size(184, 23);
            this.txtTekiyo.TabIndex = 55;
            this.txtTekiyo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtTekiyo_KeyDown);
            this.txtTekiyo.Validating += new System.ComponentModel.CancelEventHandler(this.txtTekiyo_Validating);
            // 
            // lblBumonCdTo
            // 
            this.lblBumonCdTo.AutoSize = true;
            this.lblBumonCdTo.BackColor = System.Drawing.Color.Silver;
            this.lblBumonCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBumonCdTo.Location = new System.Drawing.Point(450, 80);
            this.lblBumonCdTo.Name = "lblBumonCdTo";
            this.lblBumonCdTo.Size = new System.Drawing.Size(21, 13);
            this.lblBumonCdTo.TabIndex = 33;
            this.lblBumonCdTo.Text = "～";
            // 
            // txtBumonCdTo
            // 
            this.txtBumonCdTo.AutoSizeFromLength = false;
            this.txtBumonCdTo.DisplayLength = null;
            this.txtBumonCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtBumonCdTo.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtBumonCdTo.Location = new System.Drawing.Point(479, 75);
            this.txtBumonCdTo.MaxLength = 4;
            this.txtBumonCdTo.Name = "txtBumonCdTo";
            this.txtBumonCdTo.Size = new System.Drawing.Size(51, 23);
            this.txtBumonCdTo.TabIndex = 34;
            this.txtBumonCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBumonCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtBumonCdTo_Validating);
            // 
            // lblBumonCdFr
            // 
            this.lblBumonCdFr.AutoSize = true;
            this.lblBumonCdFr.BackColor = System.Drawing.Color.Silver;
            this.lblBumonCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBumonCdFr.Location = new System.Drawing.Point(311, 80);
            this.lblBumonCdFr.Name = "lblBumonCdFr";
            this.lblBumonCdFr.Size = new System.Drawing.Size(77, 13);
            this.lblBumonCdFr.TabIndex = 31;
            this.lblBumonCdFr.Text = "部門コード";
            // 
            // txtBumonCdFr
            // 
            this.txtBumonCdFr.AutoSizeFromLength = false;
            this.txtBumonCdFr.DisplayLength = null;
            this.txtBumonCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtBumonCdFr.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtBumonCdFr.Location = new System.Drawing.Point(391, 75);
            this.txtBumonCdFr.MaxLength = 4;
            this.txtBumonCdFr.Name = "txtBumonCdFr";
            this.txtBumonCdFr.Size = new System.Drawing.Size(51, 23);
            this.txtBumonCdFr.TabIndex = 32;
            this.txtBumonCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBumonCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtBumonCdFr_Validating);
            // 
            // lblKojiCdTo
            // 
            this.lblKojiCdTo.AutoSize = true;
            this.lblKojiCdTo.BackColor = System.Drawing.Color.Silver;
            this.lblKojiCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKojiCdTo.Location = new System.Drawing.Point(450, 112);
            this.lblKojiCdTo.Name = "lblKojiCdTo";
            this.lblKojiCdTo.Size = new System.Drawing.Size(21, 13);
            this.lblKojiCdTo.TabIndex = 38;
            this.lblKojiCdTo.Text = "～";
            // 
            // txtKojiCdTo
            // 
            this.txtKojiCdTo.AutoSizeFromLength = false;
            this.txtKojiCdTo.DisplayLength = null;
            this.txtKojiCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKojiCdTo.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKojiCdTo.Location = new System.Drawing.Point(479, 107);
            this.txtKojiCdTo.MaxLength = 4;
            this.txtKojiCdTo.Name = "txtKojiCdTo";
            this.txtKojiCdTo.Size = new System.Drawing.Size(51, 23);
            this.txtKojiCdTo.TabIndex = 39;
            this.txtKojiCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKojiCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtKojiCdTo_Validating);
            // 
            // lblKojiCdFr
            // 
            this.lblKojiCdFr.AutoSize = true;
            this.lblKojiCdFr.BackColor = System.Drawing.Color.Silver;
            this.lblKojiCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKojiCdFr.Location = new System.Drawing.Point(311, 112);
            this.lblKojiCdFr.Name = "lblKojiCdFr";
            this.lblKojiCdFr.Size = new System.Drawing.Size(77, 13);
            this.lblKojiCdFr.TabIndex = 36;
            this.lblKojiCdFr.Text = "工事コード";
            // 
            // txtKojiCdFr
            // 
            this.txtKojiCdFr.AutoSizeFromLength = false;
            this.txtKojiCdFr.DisplayLength = null;
            this.txtKojiCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKojiCdFr.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKojiCdFr.Location = new System.Drawing.Point(391, 107);
            this.txtKojiCdFr.MaxLength = 4;
            this.txtKojiCdFr.Name = "txtKojiCdFr";
            this.txtKojiCdFr.Size = new System.Drawing.Size(51, 23);
            this.txtKojiCdFr.TabIndex = 37;
            this.txtKojiCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKojiCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtKojiCdFr_Validating);
            // 
            // lblKanjoKamokuCdTo
            // 
            this.lblKanjoKamokuCdTo.AutoSize = true;
            this.lblKanjoKamokuCdTo.BackColor = System.Drawing.Color.Silver;
            this.lblKanjoKamokuCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKanjoKamokuCdTo.Location = new System.Drawing.Point(216, 80);
            this.lblKanjoKamokuCdTo.Name = "lblKanjoKamokuCdTo";
            this.lblKanjoKamokuCdTo.Size = new System.Drawing.Size(21, 13);
            this.lblKanjoKamokuCdTo.TabIndex = 23;
            this.lblKanjoKamokuCdTo.Text = "～";
            // 
            // txtKanjoKamokuCdTo
            // 
            this.txtKanjoKamokuCdTo.AutoSizeFromLength = false;
            this.txtKanjoKamokuCdTo.DisplayLength = null;
            this.txtKanjoKamokuCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKanjoKamokuCdTo.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKanjoKamokuCdTo.Location = new System.Drawing.Point(245, 75);
            this.txtKanjoKamokuCdTo.MaxLength = 6;
            this.txtKanjoKamokuCdTo.Name = "txtKanjoKamokuCdTo";
            this.txtKanjoKamokuCdTo.Size = new System.Drawing.Size(53, 23);
            this.txtKanjoKamokuCdTo.TabIndex = 24;
            this.txtKanjoKamokuCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKanjoKamokuCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtKanjoKamokuCdTo_Validating);
            // 
            // lblKanjoKamokuCdFr
            // 
            this.lblKanjoKamokuCdFr.AutoSize = true;
            this.lblKanjoKamokuCdFr.BackColor = System.Drawing.Color.Silver;
            this.lblKanjoKamokuCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKanjoKamokuCdFr.Location = new System.Drawing.Point(47, 80);
            this.lblKanjoKamokuCdFr.Name = "lblKanjoKamokuCdFr";
            this.lblKanjoKamokuCdFr.Size = new System.Drawing.Size(105, 13);
            this.lblKanjoKamokuCdFr.TabIndex = 20;
            this.lblKanjoKamokuCdFr.Text = "勘定科目コード";
            // 
            // txtKanjoKamokuCdFr
            // 
            this.txtKanjoKamokuCdFr.AutoSizeFromLength = false;
            this.txtKanjoKamokuCdFr.DisplayLength = null;
            this.txtKanjoKamokuCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKanjoKamokuCdFr.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtKanjoKamokuCdFr.Location = new System.Drawing.Point(157, 75);
            this.txtKanjoKamokuCdFr.MaxLength = 6;
            this.txtKanjoKamokuCdFr.Name = "txtKanjoKamokuCdFr";
            this.txtKanjoKamokuCdFr.Size = new System.Drawing.Size(53, 23);
            this.txtKanjoKamokuCdFr.TabIndex = 22;
            this.txtKanjoKamokuCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKanjoKamokuCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtKanjoKamokuCdFr_Validating);
            // 
            // lblHojoKamokuCdTo
            // 
            this.lblHojoKamokuCdTo.AutoSize = true;
            this.lblHojoKamokuCdTo.BackColor = System.Drawing.Color.Silver;
            this.lblHojoKamokuCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblHojoKamokuCdTo.Location = new System.Drawing.Point(216, 113);
            this.lblHojoKamokuCdTo.Name = "lblHojoKamokuCdTo";
            this.lblHojoKamokuCdTo.Size = new System.Drawing.Size(21, 13);
            this.lblHojoKamokuCdTo.TabIndex = 28;
            this.lblHojoKamokuCdTo.Text = "～";
            // 
            // txtHojoKamokuCdTo
            // 
            this.txtHojoKamokuCdTo.AutoSizeFromLength = false;
            this.txtHojoKamokuCdTo.DisplayLength = null;
            this.txtHojoKamokuCdTo.Enabled = false;
            this.txtHojoKamokuCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtHojoKamokuCdTo.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtHojoKamokuCdTo.Location = new System.Drawing.Point(245, 108);
            this.txtHojoKamokuCdTo.MaxLength = 4;
            this.txtHojoKamokuCdTo.Name = "txtHojoKamokuCdTo";
            this.txtHojoKamokuCdTo.Size = new System.Drawing.Size(51, 23);
            this.txtHojoKamokuCdTo.TabIndex = 29;
            this.txtHojoKamokuCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtHojoKamokuCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtHojoKamokuCdTo_Validating);
            // 
            // lblHojoKamokuCdFo
            // 
            this.lblHojoKamokuCdFo.AutoSize = true;
            this.lblHojoKamokuCdFo.BackColor = System.Drawing.Color.Silver;
            this.lblHojoKamokuCdFo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblHojoKamokuCdFo.Location = new System.Drawing.Point(47, 113);
            this.lblHojoKamokuCdFo.Name = "lblHojoKamokuCdFo";
            this.lblHojoKamokuCdFo.Size = new System.Drawing.Size(105, 13);
            this.lblHojoKamokuCdFo.TabIndex = 26;
            this.lblHojoKamokuCdFo.Text = "補助科目コード";
            // 
            // txtHojoKamokuCdFr
            // 
            this.txtHojoKamokuCdFr.AutoSizeFromLength = false;
            this.txtHojoKamokuCdFr.DisplayLength = null;
            this.txtHojoKamokuCdFr.Enabled = false;
            this.txtHojoKamokuCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtHojoKamokuCdFr.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtHojoKamokuCdFr.Location = new System.Drawing.Point(157, 108);
            this.txtHojoKamokuCdFr.MaxLength = 4;
            this.txtHojoKamokuCdFr.Name = "txtHojoKamokuCdFr";
            this.txtHojoKamokuCdFr.Size = new System.Drawing.Size(51, 23);
            this.txtHojoKamokuCdFr.TabIndex = 27;
            this.txtHojoKamokuCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtHojoKamokuCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtHojoKamokuCdFr_Validating);
            // 
            // lblZeiKubunTo
            // 
            this.lblZeiKubunTo.AutoSize = true;
            this.lblZeiKubunTo.BackColor = System.Drawing.Color.Silver;
            this.lblZeiKubunTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblZeiKubunTo.Location = new System.Drawing.Point(216, 147);
            this.lblZeiKubunTo.Name = "lblZeiKubunTo";
            this.lblZeiKubunTo.Size = new System.Drawing.Size(21, 13);
            this.lblZeiKubunTo.TabIndex = 42;
            this.lblZeiKubunTo.Text = "～";
            // 
            // txtZeiKubunTo
            // 
            this.txtZeiKubunTo.AutoSizeFromLength = false;
            this.txtZeiKubunTo.DisplayLength = null;
            this.txtZeiKubunTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtZeiKubunTo.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtZeiKubunTo.Location = new System.Drawing.Point(245, 141);
            this.txtZeiKubunTo.MaxLength = 2;
            this.txtZeiKubunTo.Name = "txtZeiKubunTo";
            this.txtZeiKubunTo.Size = new System.Drawing.Size(51, 23);
            this.txtZeiKubunTo.TabIndex = 43;
            this.txtZeiKubunTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtZeiKubunTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtZeiKubunTo_Validating);
            // 
            // lblZeiKubunFr
            // 
            this.lblZeiKubunFr.AutoSize = true;
            this.lblZeiKubunFr.BackColor = System.Drawing.Color.Silver;
            this.lblZeiKubunFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblZeiKubunFr.Location = new System.Drawing.Point(47, 146);
            this.lblZeiKubunFr.Name = "lblZeiKubunFr";
            this.lblZeiKubunFr.Size = new System.Drawing.Size(91, 13);
            this.lblZeiKubunFr.TabIndex = 40;
            this.lblZeiKubunFr.Text = "税区分コード";
            // 
            // txtZeiKubunFr
            // 
            this.txtZeiKubunFr.AutoSizeFromLength = false;
            this.txtZeiKubunFr.DisplayLength = null;
            this.txtZeiKubunFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtZeiKubunFr.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtZeiKubunFr.Location = new System.Drawing.Point(157, 141);
            this.txtZeiKubunFr.MaxLength = 2;
            this.txtZeiKubunFr.Name = "txtZeiKubunFr";
            this.txtZeiKubunFr.Size = new System.Drawing.Size(51, 23);
            this.txtZeiKubunFr.TabIndex = 41;
            this.txtZeiKubunFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtZeiKubunFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtZeiKubunFr_Validating);
            // 
            // lblCondition
            // 
            this.lblCondition.AutoSize = true;
            this.lblCondition.BackColor = System.Drawing.Color.Silver;
            this.lblCondition.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCondition.Location = new System.Drawing.Point(14, 85);
            this.lblCondition.Name = "lblCondition";
            this.lblCondition.Size = new System.Drawing.Size(21, 13);
            this.lblCondition.TabIndex = 19;
            this.lblCondition.Text = "検";
            // 
            // txtZeiRt
            // 
            this.txtZeiRt.AutoSizeFromLength = false;
            this.txtZeiRt.DisplayLength = null;
            this.txtZeiRt.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtZeiRt.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtZeiRt.Location = new System.Drawing.Point(391, 141);
            this.txtZeiRt.MaxLength = 2;
            this.txtZeiRt.Name = "txtZeiRt";
            this.txtZeiRt.Size = new System.Drawing.Size(51, 23);
            this.txtZeiRt.TabIndex = 46;
            this.txtZeiRt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtZeiRt.Validating += new System.ComponentModel.CancelEventHandler(this.txtZeiRt_Validating);
            // 
            // lblZeiRtPer
            // 
            this.lblZeiRtPer.AutoSize = true;
            this.lblZeiRtPer.BackColor = System.Drawing.Color.Silver;
            this.lblZeiRtPer.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblZeiRtPer.Location = new System.Drawing.Point(450, 146);
            this.lblZeiRtPer.Name = "lblZeiRtPer";
            this.lblZeiRtPer.Size = new System.Drawing.Size(21, 13);
            this.lblZeiRtPer.TabIndex = 2;
            this.lblZeiRtPer.Text = "％";
            // 
            // lblZeiRt
            // 
            this.lblZeiRt.AutoSize = true;
            this.lblZeiRt.BackColor = System.Drawing.Color.Silver;
            this.lblZeiRt.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblZeiRt.Location = new System.Drawing.Point(311, 146);
            this.lblZeiRt.Name = "lblZeiRt";
            this.lblZeiRt.Size = new System.Drawing.Size(77, 13);
            this.lblZeiRt.TabIndex = 45;
            this.lblZeiRt.Text = "税      率";
            // 
            // lblBakDenpyoDate
            // 
            this.lblBakDenpyoDate.BackColor = System.Drawing.Color.Silver;
            this.lblBakDenpyoDate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBakDenpyoDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblBakDenpyoDate.Location = new System.Drawing.Point(12, 38);
            this.lblBakDenpyoDate.Name = "lblBakDenpyoDate";
            this.lblBakDenpyoDate.Size = new System.Drawing.Size(494, 32);
            this.lblBakDenpyoDate.TabIndex = 1;
            this.lblBakDenpyoDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Silver;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.label3.Location = new System.Drawing.Point(39, 71);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(262, 32);
            this.label3.TabIndex = 21;
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Silver;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.label4.Location = new System.Drawing.Point(39, 104);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(262, 32);
            this.label4.TabIndex = 25;
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Silver;
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label5.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.label5.Location = new System.Drawing.Point(39, 137);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(262, 32);
            this.label5.TabIndex = 29;
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.Silver;
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label6.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.label6.Location = new System.Drawing.Point(302, 71);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(232, 32);
            this.label6.TabIndex = 30;
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.Silver;
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label7.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.label7.Location = new System.Drawing.Point(302, 104);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(232, 32);
            this.label7.TabIndex = 35;
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.Silver;
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label8.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.label8.Location = new System.Drawing.Point(302, 137);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(232, 32);
            this.label8.TabIndex = 44;
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.Silver;
            this.label9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label9.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.label9.Location = new System.Drawing.Point(535, 38);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(273, 32);
            this.label9.TabIndex = 923;
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.Color.Silver;
            this.label10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label10.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.label10.Location = new System.Drawing.Point(12, 71);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(27, 99);
            this.label10.TabIndex = 18;
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label11
            // 
            this.label11.BackColor = System.Drawing.Color.Silver;
            this.label11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label11.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.label11.Location = new System.Drawing.Point(535, 71);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(273, 32);
            this.label11.TabIndex = 47;
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label12
            // 
            this.label12.BackColor = System.Drawing.Color.Silver;
            this.label12.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label12.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.label12.Location = new System.Drawing.Point(535, 104);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(273, 32);
            this.label12.TabIndex = 50;
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label13
            // 
            this.label13.BackColor = System.Drawing.Color.Silver;
            this.label13.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label13.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.label13.Location = new System.Drawing.Point(535, 137);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(273, 32);
            this.label13.TabIndex = 53;
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ZMDE1032
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.lblTekiyo);
            this.Controls.Add(this.txtTekiyo);
            this.Controls.Add(this.lblKingaku);
            this.Controls.Add(this.txtKingaku);
            this.Controls.Add(this.lblShohyoBango);
            this.Controls.Add(this.txtShohyoBango);
            this.Controls.Add(this.rdoKessanKubunAll);
            this.Controls.Add(this.lblCondition);
            this.Controls.Add(this.rdoKessanKubun1);
            this.Controls.Add(this.rdoKessanKubun0);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtZeiRt);
            this.Controls.Add(this.lblZeiRtPer);
            this.Controls.Add(this.lblZeiRt);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.lblKojiCdTo);
            this.Controls.Add(this.txtKojiCdTo);
            this.Controls.Add(this.lblKojiCdFr);
            this.Controls.Add(this.lblBumonCdTo);
            this.Controls.Add(this.txtKojiCdFr);
            this.Controls.Add(this.txtBumonCdTo);
            this.Controls.Add(this.lblBumonCdFr);
            this.Controls.Add(this.lblZeiKubunTo);
            this.Controls.Add(this.txtBumonCdFr);
            this.Controls.Add(this.txtZeiKubunTo);
            this.Controls.Add(this.lblHojoKamokuCdTo);
            this.Controls.Add(this.lblZeiKubunFr);
            this.Controls.Add(this.txtHojoKamokuCdTo);
            this.Controls.Add(this.txtZeiKubunFr);
            this.Controls.Add(this.lblHojoKamokuCdFo);
            this.Controls.Add(this.lblKanjoKamokuCdTo);
            this.Controls.Add(this.txtHojoKamokuCdFr);
            this.Controls.Add(this.txtKanjoKamokuCdTo);
            this.Controls.Add(this.lblKanjoKamokuCdFr);
            this.Controls.Add(this.lblDayDenpyoDateTo);
            this.Controls.Add(this.txtKanjoKamokuCdFr);
            this.Controls.Add(this.txtDayDenpyoDateTo);
            this.Controls.Add(this.lblDenpyoDateTo);
            this.Controls.Add(this.lblMonthDenpyoDateTo);
            this.Controls.Add(this.lblYearDenpyoDateTo);
            this.Controls.Add(this.txtMonthDenpyoDateTo);
            this.Controls.Add(this.lblGengoDenpyoDateTo);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.txtGengoYearDenpyoDateTo);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.lblDayDenpyoDateFr);
            this.Controls.Add(this.mtbList);
            this.Controls.Add(this.txtDayDenpyoDateFr);
            this.Controls.Add(this.lblGengoDenpyoDateFr);
            this.Controls.Add(this.lblDenpyoDateFr);
            this.Controls.Add(this.txtGengoYearDenpyoDateFr);
            this.Controls.Add(this.lblMonthDenpyoDateFr);
            this.Controls.Add(this.txtMonthDenpyoDateFr);
            this.Controls.Add(this.lblYearDenpyoDateFr);
            this.Controls.Add(this.lblBakDenpyoDate);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label13);
            this.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.Name = "ZMDE1032";
            this.Text = "伝票検索";
            this.Controls.SetChildIndex(this.label13, 0);
            this.Controls.SetChildIndex(this.label12, 0);
            this.Controls.SetChildIndex(this.label11, 0);
            this.Controls.SetChildIndex(this.label7, 0);
            this.Controls.SetChildIndex(this.label6, 0);
            this.Controls.SetChildIndex(this.label5, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.Controls.SetChildIndex(this.lblBakDenpyoDate, 0);
            this.Controls.SetChildIndex(this.lblYearDenpyoDateFr, 0);
            this.Controls.SetChildIndex(this.txtMonthDenpyoDateFr, 0);
            this.Controls.SetChildIndex(this.lblMonthDenpyoDateFr, 0);
            this.Controls.SetChildIndex(this.txtGengoYearDenpyoDateFr, 0);
            this.Controls.SetChildIndex(this.lblDenpyoDateFr, 0);
            this.Controls.SetChildIndex(this.lblGengoDenpyoDateFr, 0);
            this.Controls.SetChildIndex(this.txtDayDenpyoDateFr, 0);
            this.Controls.SetChildIndex(this.mtbList, 0);
            this.Controls.SetChildIndex(this.lblDayDenpyoDateFr, 0);
            this.Controls.SetChildIndex(this.panel2, 0);
            this.Controls.SetChildIndex(this.txtGengoYearDenpyoDateTo, 0);
            this.Controls.SetChildIndex(this.panel3, 0);
            this.Controls.SetChildIndex(this.lblGengoDenpyoDateTo, 0);
            this.Controls.SetChildIndex(this.txtMonthDenpyoDateTo, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.lblYearDenpyoDateTo, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblMonthDenpyoDateTo, 0);
            this.Controls.SetChildIndex(this.lblDenpyoDateTo, 0);
            this.Controls.SetChildIndex(this.txtDayDenpyoDateTo, 0);
            this.Controls.SetChildIndex(this.txtKanjoKamokuCdFr, 0);
            this.Controls.SetChildIndex(this.lblDayDenpyoDateTo, 0);
            this.Controls.SetChildIndex(this.lblKanjoKamokuCdFr, 0);
            this.Controls.SetChildIndex(this.txtKanjoKamokuCdTo, 0);
            this.Controls.SetChildIndex(this.txtHojoKamokuCdFr, 0);
            this.Controls.SetChildIndex(this.lblKanjoKamokuCdTo, 0);
            this.Controls.SetChildIndex(this.lblHojoKamokuCdFo, 0);
            this.Controls.SetChildIndex(this.txtZeiKubunFr, 0);
            this.Controls.SetChildIndex(this.txtHojoKamokuCdTo, 0);
            this.Controls.SetChildIndex(this.lblZeiKubunFr, 0);
            this.Controls.SetChildIndex(this.lblHojoKamokuCdTo, 0);
            this.Controls.SetChildIndex(this.txtZeiKubunTo, 0);
            this.Controls.SetChildIndex(this.txtBumonCdFr, 0);
            this.Controls.SetChildIndex(this.lblZeiKubunTo, 0);
            this.Controls.SetChildIndex(this.lblBumonCdFr, 0);
            this.Controls.SetChildIndex(this.txtBumonCdTo, 0);
            this.Controls.SetChildIndex(this.txtKojiCdFr, 0);
            this.Controls.SetChildIndex(this.lblBumonCdTo, 0);
            this.Controls.SetChildIndex(this.lblKojiCdFr, 0);
            this.Controls.SetChildIndex(this.txtKojiCdTo, 0);
            this.Controls.SetChildIndex(this.lblKojiCdTo, 0);
            this.Controls.SetChildIndex(this.label8, 0);
            this.Controls.SetChildIndex(this.lblZeiRt, 0);
            this.Controls.SetChildIndex(this.lblZeiRtPer, 0);
            this.Controls.SetChildIndex(this.txtZeiRt, 0);
            this.Controls.SetChildIndex(this.label9, 0);
            this.Controls.SetChildIndex(this.label10, 0);
            this.Controls.SetChildIndex(this.rdoKessanKubun0, 0);
            this.Controls.SetChildIndex(this.rdoKessanKubun1, 0);
            this.Controls.SetChildIndex(this.lblCondition, 0);
            this.Controls.SetChildIndex(this.rdoKessanKubunAll, 0);
            this.Controls.SetChildIndex(this.txtShohyoBango, 0);
            this.Controls.SetChildIndex(this.lblShohyoBango, 0);
            this.Controls.SetChildIndex(this.txtKingaku, 0);
            this.Controls.SetChildIndex(this.lblKingaku, 0);
            this.Controls.SetChildIndex(this.txtTekiyo, 0);
            this.Controls.SetChildIndex(this.lblTekiyo, 0);
            this.pnlDebug.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.RadioButton rdoKessanKubun1;
        private System.Windows.Forms.RadioButton rdoKessanKubun0;
        private System.Windows.Forms.Label lblDayDenpyoDateTo;
        private jp.co.fsi.common.controls.FsiTextBox txtDayDenpyoDateTo;
        private System.Windows.Forms.Label lblDenpyoDateTo;
        private System.Windows.Forms.Label lblMonthDenpyoDateTo;
        private System.Windows.Forms.Label lblYearDenpyoDateTo;
        private jp.co.fsi.common.controls.FsiTextBox txtMonthDenpyoDateTo;
        private System.Windows.Forms.Label lblGengoDenpyoDateTo;
        private jp.co.fsi.common.controls.FsiTextBox txtGengoYearDenpyoDateTo;
        private System.Windows.Forms.Label lblDayDenpyoDateFr;
        private jp.co.fsi.common.controls.FsiTextBox txtDayDenpyoDateFr;
        private System.Windows.Forms.Label lblDenpyoDateFr;
        private System.Windows.Forms.Label lblMonthDenpyoDateFr;
        private System.Windows.Forms.Label lblYearDenpyoDateFr;
        private jp.co.fsi.common.controls.FsiTextBox txtMonthDenpyoDateFr;
        private System.Windows.Forms.Label lblGengoDenpyoDateFr;
        private jp.co.fsi.common.controls.FsiTextBox txtGengoYearDenpyoDateFr;
        private jp.co.fsi.common.FsiPanel panel3;
        private System.Windows.Forms.Label label2;
        private jp.co.fsi.common.FsiPanel panel2;
        private System.Windows.Forms.Label label1;
        private jp.co.fsi.common.controls.SjMultiTable mtbList;
        private System.Windows.Forms.Label lblKanjoKamokuCdTo;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuCdTo;
        private System.Windows.Forms.Label lblKanjoKamokuCdFr;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuCdFr;
        private System.Windows.Forms.Label lblCondition;
        private System.Windows.Forms.RadioButton rdoKessanKubunAll;
        private System.Windows.Forms.Label lblShohyoBango;
        private jp.co.fsi.common.controls.FsiTextBox txtShohyoBango;
        private System.Windows.Forms.Label lblKingaku;
        private jp.co.fsi.common.controls.FsiTextBox txtKingaku;
        private System.Windows.Forms.Label lblTekiyo;
        private jp.co.fsi.common.controls.FsiTextBox txtTekiyo;
        private System.Windows.Forms.Label lblBumonCdTo;
        private jp.co.fsi.common.controls.FsiTextBox txtBumonCdTo;
        private System.Windows.Forms.Label lblBumonCdFr;
        private jp.co.fsi.common.controls.FsiTextBox txtBumonCdFr;
        private System.Windows.Forms.Label lblKojiCdTo;
        private jp.co.fsi.common.controls.FsiTextBox txtKojiCdTo;
        private System.Windows.Forms.Label lblKojiCdFr;
        private jp.co.fsi.common.controls.FsiTextBox txtKojiCdFr;
        private System.Windows.Forms.Label lblHojoKamokuCdTo;
        private jp.co.fsi.common.controls.FsiTextBox txtHojoKamokuCdTo;
        private System.Windows.Forms.Label lblHojoKamokuCdFo;
        private jp.co.fsi.common.controls.FsiTextBox txtHojoKamokuCdFr;
        private System.Windows.Forms.Label lblZeiKubunTo;
        private jp.co.fsi.common.controls.FsiTextBox txtZeiKubunTo;
        private System.Windows.Forms.Label lblZeiKubunFr;
        private jp.co.fsi.common.controls.FsiTextBox txtZeiKubunFr;
        private common.controls.FsiTextBox txtZeiRt;
        private System.Windows.Forms.Label lblZeiRtPer;
        private System.Windows.Forms.Label lblZeiRt;
        private System.Windows.Forms.Label lblBakDenpyoDate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
    }
}