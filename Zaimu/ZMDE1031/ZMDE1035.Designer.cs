﻿namespace jp.co.fsi.zm.zmde1031
{
    partial class ZMDE1035
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpTekiyo = new System.Windows.Forms.GroupBox();
            this.txtTekiyoKanaNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtTekiyoNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtTekiyoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKanaName = new System.Windows.Forms.Label();
            this.grpShiwake = new System.Windows.Forms.GroupBox();
            this.txtShiwakeNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtShiwakeCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.grpOpt = new System.Windows.Forms.GroupBox();
            this.chkOpt3 = new System.Windows.Forms.CheckBox();
            this.chkOpt2 = new System.Windows.Forms.CheckBox();
            this.chkOpt1 = new System.Windows.Forms.CheckBox();
            this.pnlDebug.SuspendLayout();
            this.grpTekiyo.SuspendLayout();
            this.grpShiwake.SuspendLayout();
            this.grpOpt.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Text = "仕訳事例の登録";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            // 
            // grpTekiyo
            // 
            this.grpTekiyo.Controls.Add(this.txtTekiyoKanaNm);
            this.grpTekiyo.Controls.Add(this.label2);
            this.grpTekiyo.Controls.Add(this.txtTekiyoNm);
            this.grpTekiyo.Controls.Add(this.label1);
            this.grpTekiyo.Controls.Add(this.txtTekiyoCd);
            this.grpTekiyo.Controls.Add(this.lblKanaName);
            this.grpTekiyo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.grpTekiyo.Location = new System.Drawing.Point(12, 51);
            this.grpTekiyo.Name = "grpTekiyo";
            this.grpTekiyo.Size = new System.Drawing.Size(411, 94);
            this.grpTekiyo.TabIndex = 0;
            this.grpTekiyo.TabStop = false;
            this.grpTekiyo.Text = "摘要";
            // 
            // txtTekiyoKanaNm
            // 
            this.txtTekiyoKanaNm.AutoSizeFromLength = false;
            this.txtTekiyoKanaNm.DisplayLength = null;
            this.txtTekiyoKanaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTekiyoKanaNm.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtTekiyoKanaNm.Location = new System.Drawing.Point(99, 62);
            this.txtTekiyoKanaNm.MaxLength = 30;
            this.txtTekiyoKanaNm.Name = "txtTekiyoKanaNm";
            this.txtTekiyoKanaNm.Size = new System.Drawing.Size(222, 20);
            this.txtTekiyoKanaNm.TabIndex = 8;
            this.txtTekiyoKanaNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtTekiyoKanaNm_Validating);
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Silver;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(15, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 20);
            this.label2.TabIndex = 7;
            this.label2.Text = "摘要カナ名";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTekiyoNm
            // 
            this.txtTekiyoNm.AutoSizeFromLength = false;
            this.txtTekiyoNm.DisplayLength = null;
            this.txtTekiyoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTekiyoNm.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtTekiyoNm.Location = new System.Drawing.Point(99, 39);
            this.txtTekiyoNm.MaxLength = 40;
            this.txtTekiyoNm.Name = "txtTekiyoNm";
            this.txtTekiyoNm.Size = new System.Drawing.Size(296, 20);
            this.txtTekiyoNm.TabIndex = 6;
            this.txtTekiyoNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtTekiyoNm_Validating);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Silver;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(15, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 20);
            this.label1.TabIndex = 5;
            this.label1.Text = "摘　要　名";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTekiyoCd
            // 
            this.txtTekiyoCd.AutoSizeFromLength = false;
            this.txtTekiyoCd.DisplayLength = null;
            this.txtTekiyoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTekiyoCd.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtTekiyoCd.Location = new System.Drawing.Point(99, 16);
            this.txtTekiyoCd.MaxLength = 4;
            this.txtTekiyoCd.Name = "txtTekiyoCd";
            this.txtTekiyoCd.Size = new System.Drawing.Size(36, 20);
            this.txtTekiyoCd.TabIndex = 4;
            this.txtTekiyoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTekiyoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtTekiyoCd_Validating);
            // 
            // lblKanaName
            // 
            this.lblKanaName.BackColor = System.Drawing.Color.Silver;
            this.lblKanaName.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKanaName.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKanaName.Location = new System.Drawing.Point(15, 16);
            this.lblKanaName.Name = "lblKanaName";
            this.lblKanaName.Size = new System.Drawing.Size(85, 20);
            this.lblKanaName.TabIndex = 3;
            this.lblKanaName.Text = "摘要コード";
            this.lblKanaName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // grpShiwake
            // 
            this.grpShiwake.Controls.Add(this.txtShiwakeNm);
            this.grpShiwake.Controls.Add(this.label4);
            this.grpShiwake.Controls.Add(this.txtShiwakeCd);
            this.grpShiwake.Controls.Add(this.label5);
            this.grpShiwake.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.grpShiwake.Location = new System.Drawing.Point(12, 151);
            this.grpShiwake.Name = "grpShiwake";
            this.grpShiwake.Size = new System.Drawing.Size(411, 69);
            this.grpShiwake.TabIndex = 1;
            this.grpShiwake.TabStop = false;
            this.grpShiwake.Text = "仕訳";
            // 
            // txtShiwakeNm
            // 
            this.txtShiwakeNm.AutoSizeFromLength = false;
            this.txtShiwakeNm.DisplayLength = null;
            this.txtShiwakeNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShiwakeNm.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtShiwakeNm.Location = new System.Drawing.Point(99, 39);
            this.txtShiwakeNm.MaxLength = 40;
            this.txtShiwakeNm.Name = "txtShiwakeNm";
            this.txtShiwakeNm.Size = new System.Drawing.Size(296, 20);
            this.txtShiwakeNm.TabIndex = 6;
            this.txtShiwakeNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtShiwakeNm_Validating);
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Silver;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.Location = new System.Drawing.Point(15, 39);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 20);
            this.label4.TabIndex = 5;
            this.label4.Text = "仕訳名称";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtShiwakeCd
            // 
            this.txtShiwakeCd.AutoSizeFromLength = false;
            this.txtShiwakeCd.DisplayLength = null;
            this.txtShiwakeCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShiwakeCd.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtShiwakeCd.Location = new System.Drawing.Point(99, 16);
            this.txtShiwakeCd.MaxLength = 4;
            this.txtShiwakeCd.Name = "txtShiwakeCd";
            this.txtShiwakeCd.Size = new System.Drawing.Size(36, 20);
            this.txtShiwakeCd.TabIndex = 4;
            this.txtShiwakeCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShiwakeCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtShiwakeCd_Validating);
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Silver;
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label5.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label5.Location = new System.Drawing.Point(15, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(85, 20);
            this.label5.TabIndex = 3;
            this.label5.Text = "仕訳コード";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // grpOpt
            // 
            this.grpOpt.Controls.Add(this.chkOpt3);
            this.grpOpt.Controls.Add(this.chkOpt2);
            this.grpOpt.Controls.Add(this.chkOpt1);
            this.grpOpt.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.grpOpt.Location = new System.Drawing.Point(12, 226);
            this.grpOpt.Name = "grpOpt";
            this.grpOpt.Size = new System.Drawing.Size(411, 49);
            this.grpOpt.TabIndex = 2;
            this.grpOpt.TabStop = false;
            this.grpOpt.Text = "保存指定";
            // 
            // chkOpt3
            // 
            this.chkOpt3.AutoSize = true;
            this.chkOpt3.Location = new System.Drawing.Point(275, 20);
            this.chkOpt3.Name = "chkOpt3";
            this.chkOpt3.Size = new System.Drawing.Size(124, 17);
            this.chkOpt3.TabIndex = 2;
            this.chkOpt3.Text = "金額を保存する";
            this.chkOpt3.UseVisualStyleBackColor = true;
            // 
            // chkOpt2
            // 
            this.chkOpt2.AutoSize = true;
            this.chkOpt2.Location = new System.Drawing.Point(145, 20);
            this.chkOpt2.Name = "chkOpt2";
            this.chkOpt2.Size = new System.Drawing.Size(124, 17);
            this.chkOpt2.TabIndex = 1;
            this.chkOpt2.Text = "補助を保存する";
            this.chkOpt2.UseVisualStyleBackColor = true;
            // 
            // chkOpt1
            // 
            this.chkOpt1.AutoSize = true;
            this.chkOpt1.Location = new System.Drawing.Point(15, 20);
            this.chkOpt1.Name = "chkOpt1";
            this.chkOpt1.Size = new System.Drawing.Size(124, 17);
            this.chkOpt1.TabIndex = 0;
            this.chkOpt1.Text = "部門を保存する";
            this.chkOpt1.UseVisualStyleBackColor = true;
            // 
            // ZMDE1035
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.grpOpt);
            this.Controls.Add(this.grpShiwake);
            this.Controls.Add(this.grpTekiyo);
            this.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.Name = "ZMDE1035";
            this.ShowFButton = true;
            this.Text = "仕訳事例の登録";
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.grpTekiyo, 0);
            this.Controls.SetChildIndex(this.grpShiwake, 0);
            this.Controls.SetChildIndex(this.grpOpt, 0);
            this.pnlDebug.ResumeLayout(false);
            this.grpTekiyo.ResumeLayout(false);
            this.grpTekiyo.PerformLayout();
            this.grpShiwake.ResumeLayout(false);
            this.grpShiwake.PerformLayout();
            this.grpOpt.ResumeLayout(false);
            this.grpOpt.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpTekiyo;
        private jp.co.fsi.common.controls.FsiTextBox txtTekiyoKanaNm;
        private System.Windows.Forms.Label label2;
        private jp.co.fsi.common.controls.FsiTextBox txtTekiyoNm;
        private System.Windows.Forms.Label label1;
        private jp.co.fsi.common.controls.FsiTextBox txtTekiyoCd;
        private System.Windows.Forms.Label lblKanaName;
        private System.Windows.Forms.GroupBox grpShiwake;
        private jp.co.fsi.common.controls.FsiTextBox txtShiwakeNm;
        private System.Windows.Forms.Label label4;
        private jp.co.fsi.common.controls.FsiTextBox txtShiwakeCd;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox grpOpt;
        private System.Windows.Forms.CheckBox chkOpt3;
        private System.Windows.Forms.CheckBox chkOpt2;
        private System.Windows.Forms.CheckBox chkOpt1;


    }
}