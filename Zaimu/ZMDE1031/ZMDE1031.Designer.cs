﻿namespace jp.co.fsi.zm.zmde1031
{
    partial class ZMDE1031
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblDayDenpyoDate = new System.Windows.Forms.Label();
            this.txtDayDenpyoDate = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblDenpyoDate = new System.Windows.Forms.Label();
            this.lblMonthDenpyoDate = new System.Windows.Forms.Label();
            this.lblYearDenpyoDate = new System.Windows.Forms.Label();
            this.txtMonthDenpyoDate = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGengoDenpyoDate = new System.Windows.Forms.Label();
            this.txtGengoYearDenpyoDate = new jp.co.fsi.common.controls.FsiTextBox();
            this.mtbList = new jp.co.fsi.common.controls.SjMultiTable();
            this.panel2 = new jp.co.fsi.common.FsiPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel3 = new jp.co.fsi.common.FsiPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.panel4 = new jp.co.fsi.common.FsiPanel();
            this.lblCrShohizeiKingaku = new System.Windows.Forms.Label();
            this.lblCrDenpyoKingaku = new System.Windows.Forms.Label();
            this.lblDrShohizeiKingaku = new System.Windows.Forms.Label();
            this.lblDrDenpyoKingaku = new System.Windows.Forms.Label();
            this.lblInfo = new System.Windows.Forms.Label();
            this.lblGokei = new System.Windows.Forms.Label();
            this.lblTantosha = new System.Windows.Forms.Label();
            this.lblTantoshaNm = new System.Windows.Forms.Label();
            this.txtTantoshaCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblDenpyoBango = new System.Windows.Forms.Label();
            this.txtDenpyoBango = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblShohyoBango = new System.Windows.Forms.Label();
            this.txtShohyoBango = new jp.co.fsi.common.controls.FsiTextBox();
            this.rdoKessanKubun1 = new System.Windows.Forms.RadioButton();
            this.rdoKessanKubun0 = new System.Windows.Forms.RadioButton();
            this.lblMode = new System.Windows.Forms.Label();
            this.lblMessage = new System.Windows.Forms.Label();
            this.txtMizuageShishoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMizuageShishoNm = new System.Windows.Forms.Label();
            this.lblMizuageShisho = new System.Windows.Forms.Label();
            this.btnShiftF4 = new System.Windows.Forms.Button();
            this.lblBiko = new System.Windows.Forms.Label();
            this.lblBakDenpyoDate = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Text = "振替伝票入力";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Controls.Add(this.lblBiko);
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            this.pnlDebug.Controls.SetChildIndex(this.btnF6, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF7, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF5, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF8, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF4, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF9, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF3, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF10, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF2, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF11, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF1, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF12, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnEsc, 0);
            this.pnlDebug.Controls.SetChildIndex(this.lblBiko, 0);
            // 
            // lblDayDenpyoDate
            // 
            this.lblDayDenpyoDate.AutoSize = true;
            this.lblDayDenpyoDate.BackColor = System.Drawing.Color.Silver;
            this.lblDayDenpyoDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDayDenpyoDate.Location = new System.Drawing.Point(394, 87);
            this.lblDayDenpyoDate.Name = "lblDayDenpyoDate";
            this.lblDayDenpyoDate.Size = new System.Drawing.Size(21, 13);
            this.lblDayDenpyoDate.TabIndex = 16;
            this.lblDayDenpyoDate.Text = "日";
            // 
            // txtDayDenpyoDate
            // 
            this.txtDayDenpyoDate.AutoSizeFromLength = false;
            this.txtDayDenpyoDate.DisplayLength = null;
            this.txtDayDenpyoDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDayDenpyoDate.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtDayDenpyoDate.Location = new System.Drawing.Point(368, 84);
            this.txtDayDenpyoDate.MaxLength = 2;
            this.txtDayDenpyoDate.Name = "txtDayDenpyoDate";
            this.txtDayDenpyoDate.Size = new System.Drawing.Size(22, 20);
            this.txtDayDenpyoDate.TabIndex = 15;
            this.txtDayDenpyoDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDayDenpyoDate.Validating += new System.ComponentModel.CancelEventHandler(this.txtDayDenpyoDate_Validating);
            // 
            // lblDenpyoDate
            // 
            this.lblDenpyoDate.AutoSize = true;
            this.lblDenpyoDate.BackColor = System.Drawing.Color.Silver;
            this.lblDenpyoDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDenpyoDate.Location = new System.Drawing.Point(161, 87);
            this.lblDenpyoDate.Name = "lblDenpyoDate";
            this.lblDenpyoDate.Size = new System.Drawing.Size(63, 13);
            this.lblDenpyoDate.TabIndex = 9;
            this.lblDenpyoDate.Text = "伝票日付";
            // 
            // lblMonthDenpyoDate
            // 
            this.lblMonthDenpyoDate.AutoSize = true;
            this.lblMonthDenpyoDate.BackColor = System.Drawing.Color.Silver;
            this.lblMonthDenpyoDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMonthDenpyoDate.Location = new System.Drawing.Point(345, 87);
            this.lblMonthDenpyoDate.Name = "lblMonthDenpyoDate";
            this.lblMonthDenpyoDate.Size = new System.Drawing.Size(21, 13);
            this.lblMonthDenpyoDate.TabIndex = 14;
            this.lblMonthDenpyoDate.Text = "月";
            // 
            // lblYearDenpyoDate
            // 
            this.lblYearDenpyoDate.AutoSize = true;
            this.lblYearDenpyoDate.BackColor = System.Drawing.Color.Silver;
            this.lblYearDenpyoDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYearDenpyoDate.Location = new System.Drawing.Point(297, 87);
            this.lblYearDenpyoDate.Name = "lblYearDenpyoDate";
            this.lblYearDenpyoDate.Size = new System.Drawing.Size(21, 13);
            this.lblYearDenpyoDate.TabIndex = 12;
            this.lblYearDenpyoDate.Text = "年";
            // 
            // txtMonthDenpyoDate
            // 
            this.txtMonthDenpyoDate.AutoSizeFromLength = false;
            this.txtMonthDenpyoDate.DisplayLength = null;
            this.txtMonthDenpyoDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMonthDenpyoDate.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtMonthDenpyoDate.Location = new System.Drawing.Point(320, 84);
            this.txtMonthDenpyoDate.MaxLength = 2;
            this.txtMonthDenpyoDate.Name = "txtMonthDenpyoDate";
            this.txtMonthDenpyoDate.Size = new System.Drawing.Size(22, 20);
            this.txtMonthDenpyoDate.TabIndex = 13;
            this.txtMonthDenpyoDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonthDenpyoDate.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthDenpyoDate_Validating);
            // 
            // lblGengoDenpyoDate
            // 
            this.lblGengoDenpyoDate.BackColor = System.Drawing.Color.Silver;
            this.lblGengoDenpyoDate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengoDenpyoDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengoDenpyoDate.Location = new System.Drawing.Point(233, 84);
            this.lblGengoDenpyoDate.Name = "lblGengoDenpyoDate";
            this.lblGengoDenpyoDate.Size = new System.Drawing.Size(40, 20);
            this.lblGengoDenpyoDate.TabIndex = 10;
            this.lblGengoDenpyoDate.Text = "平成";
            this.lblGengoDenpyoDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtGengoYearDenpyoDate
            // 
            this.txtGengoYearDenpyoDate.AutoSizeFromLength = false;
            this.txtGengoYearDenpyoDate.DisplayLength = null;
            this.txtGengoYearDenpyoDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGengoYearDenpyoDate.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtGengoYearDenpyoDate.Location = new System.Drawing.Point(275, 84);
            this.txtGengoYearDenpyoDate.MaxLength = 2;
            this.txtGengoYearDenpyoDate.Name = "txtGengoYearDenpyoDate";
            this.txtGengoYearDenpyoDate.Size = new System.Drawing.Size(22, 20);
            this.txtGengoYearDenpyoDate.TabIndex = 11;
            this.txtGengoYearDenpyoDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGengoYearDenpyoDate.Validating += new System.ComponentModel.CancelEventHandler(this.txtGengoYearDenpyoDate_Validating);
            // 
            // mtbList
            // 
            this.mtbList.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.mtbList.FixedCols = 0;
            this.mtbList.FocusField = null;
            this.mtbList.Location = new System.Drawing.Point(6, 137);
            this.mtbList.Name = "mtbList";
            this.mtbList.NotSelectableCols = 0;
            this.mtbList.SelectRange = null;
            this.mtbList.Size = new System.Drawing.Size(818, 349);
            this.mtbList.TabIndex = 23;
            this.mtbList.Text = "sjMultiTable1";
            this.mtbList.UndoBufferEnabled = false;
            this.mtbList.RecordEnter += new systembase.table.UTable.RecordEnterEventHandler(this.mtbList_RecordEnter);
            this.mtbList.FieldValidating += new systembase.table.UTable.FieldValidatingEventHandler(this.mtbList_FieldValidating);
            this.mtbList.FieldValueChanged += new systembase.table.UTable.FieldValueChangedEventHandler(this.mtbList_FieldValueChanged);
            this.mtbList.FieldEnter += new systembase.table.UTable.FieldEnterEventHandler(this.mtbList_FieldEnter);
            this.mtbList.EditStart += new systembase.table.UTable.EditStartEventHandler(this.mtbList_EditStart);
            this.mtbList.InitializeEditor += new systembase.table.UTable.InitializeEditorEventHandler(this.mtbList_InitializeEditor);
            this.mtbList.Leave += new System.EventHandler(this.mtbList_Leave);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.Controls.Add(this.label1);
            this.panel2.Location = new System.Drawing.Point(39, 118);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(250, 19);
            this.panel2.TabIndex = 908;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(87, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 13);
            this.label1.TabIndex = 910;
            this.label1.Text = "借　　　方";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Transparent;
            this.panel3.Controls.Add(this.label2);
            this.panel3.Location = new System.Drawing.Point(499, 118);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(250, 19);
            this.panel3.TabIndex = 909;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(87, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 13);
            this.label2.TabIndex = 910;
            this.label2.Text = "貸　　　方";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Silver;
            this.panel4.Controls.Add(this.lblCrShohizeiKingaku);
            this.panel4.Controls.Add(this.lblCrDenpyoKingaku);
            this.panel4.Controls.Add(this.lblDrShohizeiKingaku);
            this.panel4.Controls.Add(this.lblDrDenpyoKingaku);
            this.panel4.Controls.Add(this.lblInfo);
            this.panel4.Controls.Add(this.lblGokei);
            this.panel4.Location = new System.Drawing.Point(200, 491);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(359, 39);
            this.panel4.TabIndex = 910;
            // 
            // lblCrShohizeiKingaku
            // 
            this.lblCrShohizeiKingaku.BackColor = System.Drawing.Color.Transparent;
            this.lblCrShohizeiKingaku.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCrShohizeiKingaku.Location = new System.Drawing.Point(258, 21);
            this.lblCrShohizeiKingaku.Name = "lblCrShohizeiKingaku";
            this.lblCrShohizeiKingaku.Size = new System.Drawing.Size(100, 14);
            this.lblCrShohizeiKingaku.TabIndex = 915;
            this.lblCrShohizeiKingaku.Text = "1,234";
            this.lblCrShohizeiKingaku.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblCrDenpyoKingaku
            // 
            this.lblCrDenpyoKingaku.BackColor = System.Drawing.Color.Transparent;
            this.lblCrDenpyoKingaku.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCrDenpyoKingaku.Location = new System.Drawing.Point(258, 4);
            this.lblCrDenpyoKingaku.Name = "lblCrDenpyoKingaku";
            this.lblCrDenpyoKingaku.Size = new System.Drawing.Size(100, 14);
            this.lblCrDenpyoKingaku.TabIndex = 914;
            this.lblCrDenpyoKingaku.Text = "1,234,567,890";
            this.lblCrDenpyoKingaku.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblDrShohizeiKingaku
            // 
            this.lblDrShohizeiKingaku.BackColor = System.Drawing.Color.Transparent;
            this.lblDrShohizeiKingaku.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDrShohizeiKingaku.Location = new System.Drawing.Point(3, 20);
            this.lblDrShohizeiKingaku.Name = "lblDrShohizeiKingaku";
            this.lblDrShohizeiKingaku.Size = new System.Drawing.Size(100, 14);
            this.lblDrShohizeiKingaku.TabIndex = 913;
            this.lblDrShohizeiKingaku.Text = "1,234";
            this.lblDrShohizeiKingaku.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblDrDenpyoKingaku
            // 
            this.lblDrDenpyoKingaku.BackColor = System.Drawing.Color.Transparent;
            this.lblDrDenpyoKingaku.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDrDenpyoKingaku.Location = new System.Drawing.Point(3, 4);
            this.lblDrDenpyoKingaku.Name = "lblDrDenpyoKingaku";
            this.lblDrDenpyoKingaku.Size = new System.Drawing.Size(100, 14);
            this.lblDrDenpyoKingaku.TabIndex = 912;
            this.lblDrDenpyoKingaku.Text = "1,234,567.890";
            this.lblDrDenpyoKingaku.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblInfo
            // 
            this.lblInfo.BackColor = System.Drawing.Color.Transparent;
            this.lblInfo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblInfo.ForeColor = System.Drawing.Color.Red;
            this.lblInfo.Location = new System.Drawing.Point(100, 20);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(157, 15);
            this.lblInfo.TabIndex = 911;
            this.lblInfo.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblGokei
            // 
            this.lblGokei.BackColor = System.Drawing.Color.Silver;
            this.lblGokei.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGokei.Location = new System.Drawing.Point(135, 4);
            this.lblGokei.Name = "lblGokei";
            this.lblGokei.Size = new System.Drawing.Size(100, 30);
            this.lblGokei.TabIndex = 910;
            this.lblGokei.Text = "合　　計";
            this.lblGokei.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblTantosha
            // 
            this.lblTantosha.AutoSize = true;
            this.lblTantosha.BackColor = System.Drawing.Color.Silver;
            this.lblTantosha.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTantosha.Location = new System.Drawing.Point(20, 51);
            this.lblTantosha.Name = "lblTantosha";
            this.lblTantosha.Size = new System.Drawing.Size(49, 13);
            this.lblTantosha.TabIndex = 2;
            this.lblTantosha.Text = "担当者";
            // 
            // lblTantoshaNm
            // 
            this.lblTantoshaNm.BackColor = System.Drawing.Color.Silver;
            this.lblTantoshaNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTantoshaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTantoshaNm.Location = new System.Drawing.Point(144, 47);
            this.lblTantoshaNm.Name = "lblTantoshaNm";
            this.lblTantoshaNm.Size = new System.Drawing.Size(84, 20);
            this.lblTantoshaNm.TabIndex = 4;
            this.lblTantoshaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTantoshaCd
            // 
            this.txtTantoshaCd.AutoSizeFromLength = false;
            this.txtTantoshaCd.DisplayLength = null;
            this.txtTantoshaCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTantoshaCd.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtTantoshaCd.Location = new System.Drawing.Point(89, 46);
            this.txtTantoshaCd.MaxLength = 4;
            this.txtTantoshaCd.Name = "txtTantoshaCd";
            this.txtTantoshaCd.Size = new System.Drawing.Size(54, 23);
            this.txtTantoshaCd.TabIndex = 3;
            this.txtTantoshaCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTantoshaCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtTantoCd_Validating);
            // 
            // lblDenpyoBango
            // 
            this.lblDenpyoBango.AutoSize = true;
            this.lblDenpyoBango.BackColor = System.Drawing.Color.Silver;
            this.lblDenpyoBango.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDenpyoBango.Location = new System.Drawing.Point(20, 87);
            this.lblDenpyoBango.Name = "lblDenpyoBango";
            this.lblDenpyoBango.Size = new System.Drawing.Size(63, 13);
            this.lblDenpyoBango.TabIndex = 6;
            this.lblDenpyoBango.Text = "伝票番号";
            // 
            // txtDenpyoBango
            // 
            this.txtDenpyoBango.AutoSizeFromLength = false;
            this.txtDenpyoBango.DisplayLength = null;
            this.txtDenpyoBango.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDenpyoBango.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtDenpyoBango.Location = new System.Drawing.Point(89, 81);
            this.txtDenpyoBango.MaxLength = 6;
            this.txtDenpyoBango.Name = "txtDenpyoBango";
            this.txtDenpyoBango.Size = new System.Drawing.Size(54, 23);
            this.txtDenpyoBango.TabIndex = 7;
            this.txtDenpyoBango.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDenpyoBango.Validating += new System.ComponentModel.CancelEventHandler(this.txtDenpyoBango_Validating);
            // 
            // lblShohyoBango
            // 
            this.lblShohyoBango.AutoSize = true;
            this.lblShohyoBango.BackColor = System.Drawing.Color.Silver;
            this.lblShohyoBango.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohyoBango.Location = new System.Drawing.Point(430, 87);
            this.lblShohyoBango.Name = "lblShohyoBango";
            this.lblShohyoBango.Size = new System.Drawing.Size(63, 13);
            this.lblShohyoBango.TabIndex = 18;
            this.lblShohyoBango.Text = "証憑番号";
            // 
            // txtShohyoBango
            // 
            this.txtShohyoBango.AutoSizeFromLength = false;
            this.txtShohyoBango.DisplayLength = null;
            this.txtShohyoBango.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShohyoBango.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtShohyoBango.Location = new System.Drawing.Point(499, 81);
            this.txtShohyoBango.MaxLength = 10;
            this.txtShohyoBango.Name = "txtShohyoBango";
            this.txtShohyoBango.Size = new System.Drawing.Size(91, 23);
            this.txtShohyoBango.TabIndex = 19;
            this.txtShohyoBango.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohyoBango_Validating);
            // 
            // rdoKessanKubun1
            // 
            this.rdoKessanKubun1.AutoSize = true;
            this.rdoKessanKubun1.BackColor = System.Drawing.Color.Silver;
            this.rdoKessanKubun1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoKessanKubun1.Location = new System.Drawing.Point(721, 86);
            this.rdoKessanKubun1.Name = "rdoKessanKubun1";
            this.rdoKessanKubun1.Size = new System.Drawing.Size(81, 17);
            this.rdoKessanKubun1.TabIndex = 22;
            this.rdoKessanKubun1.Text = "決算仕訳";
            this.rdoKessanKubun1.UseVisualStyleBackColor = false;
            this.rdoKessanKubun1.CheckedChanged += new System.EventHandler(this.rdo_CheckedChanged);
            // 
            // rdoKessanKubun0
            // 
            this.rdoKessanKubun0.AutoSize = true;
            this.rdoKessanKubun0.BackColor = System.Drawing.Color.Silver;
            this.rdoKessanKubun0.Checked = true;
            this.rdoKessanKubun0.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdoKessanKubun0.Location = new System.Drawing.Point(633, 86);
            this.rdoKessanKubun0.Name = "rdoKessanKubun0";
            this.rdoKessanKubun0.Size = new System.Drawing.Size(81, 17);
            this.rdoKessanKubun0.TabIndex = 21;
            this.rdoKessanKubun0.TabStop = true;
            this.rdoKessanKubun0.Text = "通常仕訳";
            this.rdoKessanKubun0.UseVisualStyleBackColor = false;
            this.rdoKessanKubun0.CheckedChanged += new System.EventHandler(this.rdo_CheckedChanged);
            // 
            // lblMode
            // 
            this.lblMode.AutoSize = true;
            this.lblMode.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMode.Location = new System.Drawing.Point(756, 51);
            this.lblMode.Name = "lblMode";
            this.lblMode.Size = new System.Drawing.Size(63, 13);
            this.lblMode.TabIndex = 915;
            this.lblMode.Text = "《修正》";
            // 
            // lblMessage
            // 
            this.lblMessage.AutoSize = true;
            this.lblMessage.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMessage.Location = new System.Drawing.Point(238, 51);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(175, 13);
            this.lblMessage.TabIndex = 916;
            this.lblMessage.Text = "前回登録伝票番号：000000";
            // 
            // txtMizuageShishoCd
            // 
            this.txtMizuageShishoCd.AutoSizeFromLength = true;
            this.txtMizuageShishoCd.DisplayLength = null;
            this.txtMizuageShishoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMizuageShishoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtMizuageShishoCd.Location = new System.Drawing.Point(469, 46);
            this.txtMizuageShishoCd.MaxLength = 5;
            this.txtMizuageShishoCd.Name = "txtMizuageShishoCd";
            this.txtMizuageShishoCd.Size = new System.Drawing.Size(40, 23);
            this.txtMizuageShishoCd.TabIndex = 917;
            this.txtMizuageShishoCd.TabStop = false;
            this.txtMizuageShishoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMizuageShishoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtMizuageShishoCd_Validating);
            // 
            // lblMizuageShishoNm
            // 
            this.lblMizuageShishoNm.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShishoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShishoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMizuageShishoNm.Location = new System.Drawing.Point(510, 47);
            this.lblMizuageShishoNm.Name = "lblMizuageShishoNm";
            this.lblMizuageShishoNm.Size = new System.Drawing.Size(228, 20);
            this.lblMizuageShishoNm.TabIndex = 919;
            this.lblMizuageShishoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMizuageShisho
            // 
            this.lblMizuageShisho.BackColor = System.Drawing.Color.Silver;
            this.lblMizuageShisho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMizuageShisho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblMizuageShisho.Location = new System.Drawing.Point(424, 43);
            this.lblMizuageShisho.Name = "lblMizuageShisho";
            this.lblMizuageShisho.Size = new System.Drawing.Size(319, 29);
            this.lblMizuageShisho.TabIndex = 918;
            this.lblMizuageShisho.Text = " 支所";
            this.lblMizuageShisho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnShiftF4
            // 
            this.btnShiftF4.Enabled = false;
            this.btnShiftF4.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnShiftF4.Location = new System.Drawing.Point(589, 491);
            this.btnShiftF4.Name = "btnShiftF4";
            this.btnShiftF4.Size = new System.Drawing.Size(65, 45);
            this.btnShiftF4.TabIndex = 920;
            this.btnShiftF4.TabStop = false;
            this.btnShiftF4.Text = "Shift+F4\r\n伝票複写";
            this.btnShiftF4.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnShiftF4.UseVisualStyleBackColor = true;
            this.btnShiftF4.Click += new System.EventHandler(this.btnShiftF4_Click);
            // 
            // lblBiko
            // 
            this.lblBiko.AutoSize = true;
            this.lblBiko.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblBiko.Location = new System.Drawing.Point(198, 6);
            this.lblBiko.Name = "lblBiko";
            this.lblBiko.Size = new System.Drawing.Size(287, 13);
            this.lblBiko.TabIndex = 921;
            this.lblBiko.Text = "０１２３４５６７８９０１２３４５６７８９";
            // 
            // lblBakDenpyoDate
            // 
            this.lblBakDenpyoDate.BackColor = System.Drawing.Color.Silver;
            this.lblBakDenpyoDate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBakDenpyoDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblBakDenpyoDate.Location = new System.Drawing.Point(12, 43);
            this.lblBakDenpyoDate.Name = "lblBakDenpyoDate";
            this.lblBakDenpyoDate.Size = new System.Drawing.Size(220, 29);
            this.lblBakDenpyoDate.TabIndex = 1;
            this.lblBakDenpyoDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Silver;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.label3.Location = new System.Drawing.Point(12, 79);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(135, 29);
            this.label3.TabIndex = 5;
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Silver;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.label4.Location = new System.Drawing.Point(153, 79);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(266, 29);
            this.label4.TabIndex = 8;
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Silver;
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label5.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.label5.Location = new System.Drawing.Point(424, 79);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(172, 29);
            this.label5.TabIndex = 17;
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.Silver;
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label6.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.label6.Location = new System.Drawing.Point(624, 79);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(187, 29);
            this.label6.TabIndex = 20;
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.Silver;
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label7.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.label7.Location = new System.Drawing.Point(198, 490);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(363, 41);
            this.label7.TabIndex = 926;
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ZMDE1031
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.rdoKessanKubun1);
            this.Controls.Add(this.rdoKessanKubun0);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lblShohyoBango);
            this.Controls.Add(this.txtShohyoBango);
            this.Controls.Add(this.lblDayDenpyoDate);
            this.Controls.Add(this.txtDayDenpyoDate);
            this.Controls.Add(this.lblDenpyoBango);
            this.Controls.Add(this.lblDenpyoDate);
            this.Controls.Add(this.lblMonthDenpyoDate);
            this.Controls.Add(this.txtDenpyoBango);
            this.Controls.Add(this.lblYearDenpyoDate);
            this.Controls.Add(this.lblTantosha);
            this.Controls.Add(this.txtMonthDenpyoDate);
            this.Controls.Add(this.lblTantoshaNm);
            this.Controls.Add(this.lblGengoDenpyoDate);
            this.Controls.Add(this.txtTantoshaCd);
            this.Controls.Add(this.txtGengoYearDenpyoDate);
            this.Controls.Add(this.btnShiftF4);
            this.Controls.Add(this.txtMizuageShishoCd);
            this.Controls.Add(this.lblMizuageShishoNm);
            this.Controls.Add(this.lblMizuageShisho);
            this.Controls.Add(this.lblMessage);
            this.Controls.Add(this.lblMode);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.mtbList);
            this.Controls.Add(this.lblBakDenpyoDate);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label7);
            this.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.Name = "ZMDE1031";
            this.Text = "振替伝票入力";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ZMDE1031_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frm_KeyDown);
            this.Controls.SetChildIndex(this.label7, 0);
            this.Controls.SetChildIndex(this.label5, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.Controls.SetChildIndex(this.lblBakDenpyoDate, 0);
            this.Controls.SetChildIndex(this.mtbList, 0);
            this.Controls.SetChildIndex(this.panel2, 0);
            this.Controls.SetChildIndex(this.panel3, 0);
            this.Controls.SetChildIndex(this.panel4, 0);
            this.Controls.SetChildIndex(this.lblMode, 0);
            this.Controls.SetChildIndex(this.lblMessage, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.lblMizuageShisho, 0);
            this.Controls.SetChildIndex(this.lblMizuageShishoNm, 0);
            this.Controls.SetChildIndex(this.txtMizuageShishoCd, 0);
            this.Controls.SetChildIndex(this.btnShiftF4, 0);
            this.Controls.SetChildIndex(this.txtGengoYearDenpyoDate, 0);
            this.Controls.SetChildIndex(this.txtTantoshaCd, 0);
            this.Controls.SetChildIndex(this.lblGengoDenpyoDate, 0);
            this.Controls.SetChildIndex(this.lblTantoshaNm, 0);
            this.Controls.SetChildIndex(this.txtMonthDenpyoDate, 0);
            this.Controls.SetChildIndex(this.lblTantosha, 0);
            this.Controls.SetChildIndex(this.lblYearDenpyoDate, 0);
            this.Controls.SetChildIndex(this.txtDenpyoBango, 0);
            this.Controls.SetChildIndex(this.lblMonthDenpyoDate, 0);
            this.Controls.SetChildIndex(this.lblDenpyoDate, 0);
            this.Controls.SetChildIndex(this.lblDenpyoBango, 0);
            this.Controls.SetChildIndex(this.txtDayDenpyoDate, 0);
            this.Controls.SetChildIndex(this.lblDayDenpyoDate, 0);
            this.Controls.SetChildIndex(this.txtShohyoBango, 0);
            this.Controls.SetChildIndex(this.lblShohyoBango, 0);
            this.Controls.SetChildIndex(this.label6, 0);
            this.Controls.SetChildIndex(this.rdoKessanKubun0, 0);
            this.Controls.SetChildIndex(this.rdoKessanKubun1, 0);
            this.pnlDebug.ResumeLayout(false);
            this.pnlDebug.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblDayDenpyoDate;
        private jp.co.fsi.common.controls.FsiTextBox txtDayDenpyoDate;
        private System.Windows.Forms.Label lblDenpyoDate;
        private System.Windows.Forms.Label lblMonthDenpyoDate;
        private System.Windows.Forms.Label lblYearDenpyoDate;
        private jp.co.fsi.common.controls.FsiTextBox txtMonthDenpyoDate;
        private System.Windows.Forms.Label lblGengoDenpyoDate;
        private jp.co.fsi.common.controls.FsiTextBox txtGengoYearDenpyoDate;
        private jp.co.fsi.common.controls.SjMultiTable mtbList;
        private jp.co.fsi.common.FsiPanel panel2;
        private System.Windows.Forms.Label label1;
        private jp.co.fsi.common.FsiPanel panel3;
        private System.Windows.Forms.Label label2;
        private jp.co.fsi.common.FsiPanel panel4;
        private System.Windows.Forms.Label lblCrShohizeiKingaku;
        private System.Windows.Forms.Label lblCrDenpyoKingaku;
        private System.Windows.Forms.Label lblDrShohizeiKingaku;
        private System.Windows.Forms.Label lblDrDenpyoKingaku;
        private System.Windows.Forms.Label lblInfo;
        private System.Windows.Forms.Label lblGokei;
        private System.Windows.Forms.Label lblTantosha;
        private System.Windows.Forms.Label lblTantoshaNm;
        private jp.co.fsi.common.controls.FsiTextBox txtTantoshaCd;
        private System.Windows.Forms.Label lblDenpyoBango;
        private jp.co.fsi.common.controls.FsiTextBox txtDenpyoBango;
        private System.Windows.Forms.Label lblShohyoBango;
        private jp.co.fsi.common.controls.FsiTextBox txtShohyoBango;
        private System.Windows.Forms.RadioButton rdoKessanKubun1;
        private System.Windows.Forms.RadioButton rdoKessanKubun0;
        private System.Windows.Forms.Label lblMode;
        private System.Windows.Forms.Label lblMessage;
        private jp.co.fsi.common.controls.FsiTextBox txtMizuageShishoCd;
        private System.Windows.Forms.Label lblMizuageShishoNm;
        private System.Windows.Forms.Label lblMizuageShisho;
        protected System.Windows.Forms.Button btnShiftF4;
        private System.Windows.Forms.Label lblBiko;
        private System.Windows.Forms.Label lblBakDenpyoDate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
    }
}