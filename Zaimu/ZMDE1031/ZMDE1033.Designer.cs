﻿namespace jp.co.fsi.zm.zmde1031
{
    partial class ZMDE1033
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTorihikisakiCd = new System.Windows.Forms.Label();
            this.txtTorihikisakiCd = new System.Windows.Forms.TextBox();
            this.dgvList = new System.Windows.Forms.DataGridView();
            this.lblTorihikisakiNm = new System.Windows.Forms.Label();
            this.lblTotal = new System.Windows.Forms.Label();
            this.lblSaikenTotal = new System.Windows.Forms.Label();
            this.lblSaimuTotal = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Text = "元帳照会";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            // 
            // lblTorihikisakiCd
            // 
            this.lblTorihikisakiCd.BackColor = System.Drawing.Color.Silver;
            this.lblTorihikisakiCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTorihikisakiCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTorihikisakiCd.Location = new System.Drawing.Point(17, 43);
            this.lblTorihikisakiCd.Name = "lblTorihikisakiCd";
            this.lblTorihikisakiCd.Size = new System.Drawing.Size(138, 25);
            this.lblTorihikisakiCd.TabIndex = 1;
            this.lblTorihikisakiCd.Text = "取引先";
            this.lblTorihikisakiCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTorihikisakiCd
            // 
            this.txtTorihikisakiCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTorihikisakiCd.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtTorihikisakiCd.Location = new System.Drawing.Point(101, 45);
            this.txtTorihikisakiCd.MaxLength = 4;
            this.txtTorihikisakiCd.Name = "txtTorihikisakiCd";
            this.txtTorihikisakiCd.Size = new System.Drawing.Size(46, 20);
            this.txtTorihikisakiCd.TabIndex = 2;
            this.txtTorihikisakiCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTorihikisakiCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtTorihikisakiCd_Validating);
            // 
            // dgvList
            // 
            this.dgvList.AllowUserToAddRows = false;
            this.dgvList.AllowUserToDeleteRows = false;
            this.dgvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvList.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.dgvList.Location = new System.Drawing.Point(17, 74);
            this.dgvList.MultiSelect = false;
            this.dgvList.Name = "dgvList";
            this.dgvList.ReadOnly = true;
            this.dgvList.RowHeadersVisible = false;
            this.dgvList.RowTemplate.Height = 21;
            this.dgvList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvList.Size = new System.Drawing.Size(530, 296);
            this.dgvList.TabIndex = 3;
            this.dgvList.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvList_CellDoubleClick);
            this.dgvList.Enter += new System.EventHandler(this.dgvList_Enter);
            this.dgvList.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvList_KeyDown);
            // 
            // lblTorihikisakiNm
            // 
            this.lblTorihikisakiNm.BackColor = System.Drawing.Color.Silver;
            this.lblTorihikisakiNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTorihikisakiNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTorihikisakiNm.Location = new System.Drawing.Point(147, 43);
            this.lblTorihikisakiNm.Name = "lblTorihikisakiNm";
            this.lblTorihikisakiNm.Size = new System.Drawing.Size(401, 25);
            this.lblTorihikisakiNm.TabIndex = 902;
            this.lblTorihikisakiNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTotal
            // 
            this.lblTotal.BackColor = System.Drawing.Color.Transparent;
            this.lblTotal.Font = new System.Drawing.Font("ＭＳ ゴシック", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTotal.Location = new System.Drawing.Point(17, 375);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(202, 20);
            this.lblTotal.TabIndex = 903;
            this.lblTotal.Text = "合計";
            this.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblSaikenTotal
            // 
            this.lblSaikenTotal.BackColor = System.Drawing.Color.Transparent;
            this.lblSaikenTotal.Font = new System.Drawing.Font("ＭＳ ゴシック", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSaikenTotal.Location = new System.Drawing.Point(219, 375);
            this.lblSaikenTotal.Name = "lblSaikenTotal";
            this.lblSaikenTotal.Size = new System.Drawing.Size(150, 20);
            this.lblSaikenTotal.TabIndex = 904;
            this.lblSaikenTotal.Text = "1,234,567,890,123";
            this.lblSaikenTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblSaimuTotal
            // 
            this.lblSaimuTotal.BackColor = System.Drawing.Color.Transparent;
            this.lblSaimuTotal.Font = new System.Drawing.Font("ＭＳ ゴシック", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblSaimuTotal.Location = new System.Drawing.Point(369, 375);
            this.lblSaimuTotal.Name = "lblSaimuTotal";
            this.lblSaimuTotal.Size = new System.Drawing.Size(150, 20);
            this.lblSaimuTotal.TabIndex = 905;
            this.lblSaimuTotal.Text = "1,234,567,890,123";
            this.lblSaimuTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ZMDE1033
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.lblSaimuTotal);
            this.Controls.Add(this.lblSaikenTotal);
            this.Controls.Add(this.lblTotal);
            this.Controls.Add(this.lblTorihikisakiNm);
            this.Controls.Add(this.dgvList);
            this.Controls.Add(this.txtTorihikisakiCd);
            this.Controls.Add(this.lblTorihikisakiCd);
            this.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.Name = "ZMDE1033";
            this.ShowFButton = true;
            this.Text = "元帳照会";
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.lblTorihikisakiCd, 0);
            this.Controls.SetChildIndex(this.txtTorihikisakiCd, 0);
            this.Controls.SetChildIndex(this.dgvList, 0);
            this.Controls.SetChildIndex(this.lblTorihikisakiNm, 0);
            this.Controls.SetChildIndex(this.lblTotal, 0);
            this.Controls.SetChildIndex(this.lblSaikenTotal, 0);
            this.Controls.SetChildIndex(this.lblSaimuTotal, 0);
            this.pnlDebug.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTorihikisakiCd;
        private System.Windows.Forms.TextBox txtTorihikisakiCd;
        private System.Windows.Forms.DataGridView dgvList;
        private System.Windows.Forms.Label lblTorihikisakiNm;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.Label lblSaikenTotal;
        private System.Windows.Forms.Label lblSaimuTotal;
    }
}