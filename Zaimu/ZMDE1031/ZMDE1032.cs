﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;
using systembase.table;
using System.Reflection;

namespace jp.co.fsi.zm.zmde1031
{
    /// <summary>
    /// 伝票検索(ZMDE1032)
    /// </summary>
    public partial class ZMDE1032 : BasePgForm
    {
        #region 変数

        UTable.CRecord _activeCRecord;

        // 支所コード
        private int ShishoCode;

        #endregion

        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理用変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ZMDE1032()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)

        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 支所コード設定
            this.ShishoCode = Util.ToInt(this.UInfo.ShishoCd);

            // 工事コード項目を担当者へ割り当て
            this.lblKojiCdFr.Text = "担当者ｺｰﾄﾞ";
            //this.lblKojiCdFr.Visible = false;
            //this.txtKojiCdFr.Visible = false;
            this.lblKojiCdTo.Visible = false;
            this.txtKojiCdTo.Visible = false;

            // Escape F1 F4 F6のみ表示
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF6.Location = this.btnF4.Location;
            this.btnF4.Location = this.btnF3.Location;
            this.btnF1.Location = this.btnF2.Location;
            this.btnF2.Visible = false;
            this.btnF3.Visible = false;
            this.btnF5.Visible = false;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;
            this.ShowFButton = true;

            // 検索条件縦書き
            lblCondition.Text = "検" + "\n\r" + "索" + "\n\r" + "条" + "\n\r" + "件";

            // 伝票日付初期値
            SetJpFr(Util.ConvJpDate(new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1), this.Dba));
            SetJpTo(Util.ConvJpDate(DateTime.Today, this.Dba));

            // グリッド初期化
            InitDetailArea();
            // 初期フォーカス設定
            this.ActiveControl = this.txtGengoYearDenpyoDateFr;
            this.txtGengoYearDenpyoDateFr.Focus();

            // 条件の設定
            if (this.InData != null && !ValChk.IsEmpty(this.InData))
            {
                string inData = (string)this.InData;
                if (inData.Length == 0)
                {
                    // 伝票日付（開始）年にフォーカスを移す
                    this.txtGengoYearDenpyoDateFr.Focus();
                }
                else
                {
                    this.setDenpyoCondition(inData);
                    this.btnF6.PerformClick();
                    if (this.mtbList.Content.Records.Count > 0)
                    {
                        ActiveControl = this.mtbList;
                        mtbList.Focus();
                    }
                }
            }
        }

        /// <summary>
        /// フォーカス移動時処理
        /// </summary>
        protected override void OnMoveFocus()
        {
            // F1 F6(実行⇔決定)制御
            switch (this.ActiveCtlNm)
            {
                case "mtbList":
                    btnF1.Enabled = false;
                    btnF6.Text = "F6" + "\n\r" + "\n\r" + "決定";
                    break;
                case "txtGengoYearDenpyoDateFr":
                case "txtGengoYearDenpyoDateTo":
                case "txtKanjoKamokuCdFr":
                case "txtKanjoKamokuCdTo":
                case "txtHojoKamokuCdFr":
                case "txtHojoKamokuCdTo":
                case "txtZeiKubunFr":
                case "txtZeiKubunTo":
                case "txtBumonCdFr":
                case "txtBumonCdTo":
                case "txtKojiCdFr":   // 工事コード項目を担当者へ割り当て
                    btnF1.Enabled = true;
                    btnF6.Text = "F6" + "\n\r" + "\n\r" + "実行";
                    break;
                //case "txtKojiCdFr":
                //case "txtKojiCdTo":
                //    btnF1.Enabled = false;
                //    btnF6.Text = "F6" + "\n\r" + "\n\r" + "実行";
                //    break;

                default:
                    btnF1.Enabled = false;
                    btnF6.Text = "F6" + "\n\r" + "\n\r" + "実行";
                    break;
            }
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            this.Close();       // 終了
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        public override void PressF1()
        {
            Assembly asm;
            Type t;

            //アクティブコントロールごとの処理
            switch (this.ActiveCtlNm)
            {
                case "txtGengoYearDenpyoDateFr":
                    #region 元号検索
                    // アセンブリのロード
                    //asm = System.Reflection.Assembly.LoadFrom("COMC9011.exe");
                    asm = System.Reflection.Assembly.LoadFrom("CMCM1021.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.com.comc9011.COMC9011");
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblGengoDenpyoDateFr.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.lblGengoDenpyoDateFr.Text = result[1];

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                SetJpFr();
                            }
                        }
                    }
                    #endregion
                    break;

                case "txtGengoYearDenpyoDateTo":
                    #region 元号検索
                    // アセンブリのロード
                    //asm = System.Reflection.Assembly.LoadFrom("COMC9011.exe");
                    asm = System.Reflection.Assembly.LoadFrom("CMCM1021.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.com.comc9011.COMC9011");
                    t = asm.GetType("jp.co.fsi.cm.cmcm1021.CMCM1021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.lblGengoDenpyoDateTo.Text;
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;
                                this.lblGengoDenpyoDateTo.Text = result[1];

                                // 存在しない日付の場合、補正して存在する日付に戻す
                                SetJpTo();
                            }
                        }
                    }
                    #endregion
                    break;
                case "txtKanjoKamokuCdFr":
                case "txtKanjoKamokuCdTo":
                    #region 勘定科目検索
                    // アセンブリのロード
                    //asm = System.Reflection.Assembly.LoadFrom("ZAMC9011.exe");
                    asm = Assembly.LoadFrom("ZMCM1031.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.zam.zamc9011.ZAMC9013");
                    t = asm.GetType("jp.co.fsi.zm.zmcm1031.ZMCM1033");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "2";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;

                                // 検索結果をセット
                                if (this.ActiveCtlNm == "txtKanjoKamokuCdFr")
                                {
                                    this.txtKanjoKamokuCdFr.Text = result[0];
                                }
                                else if (this.ActiveCtlNm == "txtKanjoKamokuCdTo")
                                {
                                    this.txtKanjoKamokuCdTo.Text = result[0];
                                }
                            }
                        }
                    }
                    #endregion
                    break;
                case "txtHojoKamokuCdFr":
                case "txtHojoKamokuCdTo":
                    #region 補助科目検索
                    // アセンブリのロード
                    //asm = System.Reflection.Assembly.LoadFrom("ZAMC9021.exe");
                    asm = System.Reflection.Assembly.LoadFrom("ZMCM1041.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.zam.zamc9021.ZAMC9024");
                    t = asm.GetType("jp.co.fsi.zm.zmcm1041.ZMCM1044");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.InData = this.txtKanjoKamokuCdFr.Text;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;

                                // 検索結果をセット
                                if (this.ActiveCtlNm == "txtHojoKamokuCdFr")
                                {
                                    this.txtHojoKamokuCdFr.Text = result[0];
                                }
                                else if (this.ActiveCtlNm == "txtHojoKamokuCdTo")
                                {
                                    this.txtHojoKamokuCdTo.Text = result[0];
                                }
                            }
                        }
                    }
                    #endregion
                    break;
                case "txtZeiKubunFr":
                case "txtZeiKubunTo":
                    #region 税区分検索
                    // アセンブリのロード
                    //asm = System.Reflection.Assembly.LoadFrom("COMC8011.exe");
                    //asm = Assembly.LoadFrom("ZMCM1061.exe");
                    asm = Assembly.LoadFrom("CMCM1041.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.com.comc8011.COMC8011");
                    //t = asm.GetType("jp.co.fsi.zm.zmcm1061.ZMCM1061");
                    t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            //frm.Par1 = "1";
                            frm.Par1 = "TB_ZM_F_ZEI_KUBUN";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;

                                // 検索結果をセット
                                if (this.ActiveCtlNm == "txtZeiKubunFr")
                                {
                                    this.txtZeiKubunFr.Text = result[0];
                                }
                                else if (this.ActiveCtlNm == "txtZeiKubunTo")
                                {
                                    this.txtZeiKubunTo.Text = result[0];
                                }
                            }
                        }
                    }
                    #endregion
                    break;
                case "txtBumonCdFr":
                case "txtBumonCdTo":
                    #region 部門検索
                    // アセンブリのロード
                    //asm = System.Reflection.Assembly.LoadFrom("COMC8011.exe");  // -> ZAMC9011
                    //asm = System.Reflection.Assembly.LoadFrom("CMCM2041.exe");
                    asm = Assembly.LoadFrom("CMCM1041.exe");
                    // フォーム作成
                    //t = asm.GetType("jp.co.fsi.com.comc8011.COMC8011");
                    //t = asm.GetType("jp.co.fsi.cm.cmcm2041.CMCM2041");
                    t = asm.GetType("jp.co.fsi.cm.cmcm1041.CMCM1041");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            //frm.Par1 = "1";
                            frm.Par1 = "TB_CM_BUMON";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;

                                // 検索結果をセット
                                if (this.ActiveCtlNm == "txtBumonCdFr")
                                {
                                    this.txtBumonCdFr.Text = result[0];
                                }
                                else if (this.ActiveCtlNm == "txtBumonCdTo")
                                {
                                    this.txtBumonCdTo.Text = result[0];
                                }
                            }
                        }
                    }
                    #endregion
                    break;
                case "txtKojiCdFr":
                    #region 担当者検索（工事コード項目を担当者へ割り当て）
                    // アセンブリのロード
                    asm = System.Reflection.Assembly.LoadFrom("CMCM2021.exe");
                    // フォーム作成
                    t = asm.GetType("jp.co.fsi.cm.cmcm2021.CMCM2021");
                    if (t != null)
                    {
                        Object obj = System.Activator.CreateInstance(t);
                        if (obj != null)
                        {
                            // タブの一部として埋め込む
                            BasePgForm frm = (BasePgForm)obj;
                            frm.Par1 = "1";
                            frm.ShowDialog(this);

                            if (frm.DialogResult == DialogResult.OK)
                            {
                                string[] result = (string[])frm.OutData;

                                // 検索結果をセット
                                if (this.ActiveCtlNm == "txtKojiCdFr")
                                {
                                    this.txtKojiCdFr.Text = result[0];
                                }
                            }
                        }
                    }
                    #endregion

                    //case "txtKojiCdTo":
                    // NONE
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            // 抽出結果クリア
            DataLoad(true);

            // 検索条件へフォーカス移動
            this.txtGengoYearDenpyoDateFr.Focus();
            this.txtGengoYearDenpyoDateFr.SelectAll();
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        public override void PressF6()
        {
            // 抽出実行
            DataLoad(false);
            
            // 抽出結果へフォーカス移動
            if (mtbList.Content.Records.Count == 0)
            {
                Msg.Info("該当データはありません。");
            }
            else
            {
                mtbList.Focus();
            }
        }

        /// <summary>
        /// F10キー押下時処理
        /// </summary>
        public override void PressF10()
        {
            base.PressEsc();
        }

        #endregion

        #region イベント

        /// <summary>
        /// 勘定科目コード(自)のチェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKanjoKamokuCdFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidKanjoKamokuCdFr())
            {
                e.Cancel = true;
                this.txtKanjoKamokuCdFr.SelectAll();
            }

            // 補助科目コード入力制御
            if (!ValChk.IsEmpty(this.txtKanjoKamokuCdFr.Text)
                && !ValChk.IsEmpty(this.txtKanjoKamokuCdTo.Text)
                && this.txtKanjoKamokuCdFr.Text == this.txtKanjoKamokuCdTo.Text)
            {
                this.txtHojoKamokuCdFr.Enabled = true;
                this.txtHojoKamokuCdTo.Enabled = true;
            }
            else
            {
                this.txtHojoKamokuCdFr.Text = "";
                this.txtHojoKamokuCdTo.Text = "";
                this.txtHojoKamokuCdFr.Enabled = false;
                this.txtHojoKamokuCdTo.Enabled = false;
            }
        }

        /// <summary>
        /// 勘定科目コード(至)のチェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKanjoKamokuCdTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidKanjoKamokuCdTo())
            {
                e.Cancel = true;
                this.txtKanjoKamokuCdTo.SelectAll();
            }

            // 補助科目コード入力制御
            if (!ValChk.IsEmpty(this.txtKanjoKamokuCdFr.Text)
                && !ValChk.IsEmpty(this.txtKanjoKamokuCdTo.Text)
                && this.txtKanjoKamokuCdFr.Text == this.txtKanjoKamokuCdTo.Text)
            {
                this.txtHojoKamokuCdFr.Enabled = true;
                this.txtHojoKamokuCdTo.Enabled = true;

                this.txtHojoKamokuCdFr.Focus();
            }
            else
            {
                this.txtHojoKamokuCdFr.Text = "";
                this.txtHojoKamokuCdTo.Text = "";
                this.txtHojoKamokuCdFr.Enabled = false;
                this.txtHojoKamokuCdTo.Enabled = false;
            }
        }

        /// <summary>
        /// 補助科目コード(自)のチェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtHojoKamokuCdFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidHojoKamokuCdFr())
            {
                e.Cancel = true;
                this.txtHojoKamokuCdFr.SelectAll();
            }
        }

        /// <summary>
        /// 補助科目コード(至)のチェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtHojoKamokuCdTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidHojoKamokuCdTo())
            {
                e.Cancel = true;
                this.txtHojoKamokuCdTo.SelectAll();
            }
        }

        /// <summary>
        /// 税区分(自)のチェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtZeiKubunFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidZeiKubunFr())
            {
                e.Cancel = true;
                this.txtZeiKubunFr.SelectAll();
            }
        }

        /// <summary>
        /// 税区分(至)のチェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtZeiKubunTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidZeiKubunTo())
            {
                e.Cancel = true;
                this.txtZeiKubunTo.SelectAll();
            }
        }

        /// <summary>
        /// 部門コード(自)のチェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBumonCdFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidBumonCdFr())
            {
                e.Cancel = true;
                this.txtBumonCdFr.SelectAll();
            }
        }

        /// <summary>
        /// 部門コード(至)のチェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBumonCdTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidBumonCdTo())
            {
                e.Cancel = true;
                this.txtBumonCdTo.SelectAll();
            }
        }

        /// <summary>
        /// 工事コード(自)のチェック処理
        /// 工事コード項目を担当者へ割り当て
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKojiCdFr_Validating(object sender, CancelEventArgs e)
        {
            //if (!IsValidKojiCdFr())
            //{
            //    e.Cancel = true;
            //    this.txtKojiCdFr.SelectAll();
            //}

            // 工事コード項目を担当者へ割り当て
            if (!isValidTantoshaCd())
            {
                e.Cancel = true;
                this.txtKojiCdFr.SelectAll();
            }
        }

        /// <summary>
        /// 工事コード(至)のチェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKojiCdTo_Validating(object sender, CancelEventArgs e)
        {
            //if (!IsValidKojiCdTo())
            //{
            //    e.Cancel = true;
            //    this.txtKojiCdTo.SelectAll();
            //}
        }

        /// <summary>
        /// 証憑番号のチェック処理
        /// </summary>
        private void txtShohyoBango_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidShohyoBango())
            {
                e.Cancel = true;
                this.txtShohyoBango.SelectAll();
            }
        }

        /// <summary>
        /// 伝票金額のチェック処理
        /// </summary>
        private void txtKingaku_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidKingaku())
            {
                e.Cancel = true;
                this.txtKingaku.SelectAll();
            }
            // フォーマット
            if (Util.ToDecimal(this.txtKingaku.Text) > 0)
            {
                this.txtKingaku.Text = Util.FormatNum(this.txtKingaku.Text);
            }
            else
            {
                this.txtKingaku.Text = "";
            }
        }

        /// <summary>
        /// 摘要のチェック処理
        /// </summary>
        private void txtTekiyo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidTekiyo())
            {
                e.Cancel = true;
                this.txtTekiyo.SelectAll();

                // Enter処理を無効化
                this._dtFlg = false;
            }
            else
            {
                // Enter処理を有効化
                this._dtFlg = true;
            }
        }

        /// <summary>
        /// 摘要のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTekiyo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Flg)
            {
                // Enter処理を無効化
                this._dtFlg = false;

                // 抽出実行
                DataLoad(false);

                // 抽出結果へフォーカス移動
                if (mtbList.Content.Records.Count == 0)
                {
                    Msg.Info("該当データはありません。");

                    // 検索条件へフォーカス移動
                    this.txtGengoYearDenpyoDateFr.Focus();
                    this.txtGengoYearDenpyoDateFr.SelectAll();
                }
                else
                {
                    mtbList.Focus();
                }
            }
        }

        /// <summary>
        /// 伝票日付(自)・年値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGengoYearDenpyoDateFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtGengoYearDenpyoDateFr.Text, this.txtGengoYearDenpyoDateFr.MaxLength))
            {
                e.Cancel = true;
                this.txtGengoYearDenpyoDateFr.SelectAll();
            }
            else
            {
                this.txtGengoYearDenpyoDateFr.Text = Util.ToString(IsValid.SetYear(this.txtGengoYearDenpyoDateFr.Text));
                CheckJpFr();
                SetJpFr();
            }
        }

        /// <summary>
        /// 伝票日付(自)・月の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonthDenpyoDateFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtMonthDenpyoDateFr.Text, this.txtMonthDenpyoDateFr.MaxLength))
            {
                e.Cancel = true;
                this.txtMonthDenpyoDateFr.SelectAll();
            }
            else
            {
                this.txtMonthDenpyoDateFr.Text = Util.ToString(IsValid.SetMonth(this.txtMonthDenpyoDateFr.Text));
                CheckJpFr();
                SetJpFr();
            }
        }

        /// <summary>
        /// 伝票日付(自)・日の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDayDenpyoDateFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtDayDenpyoDateFr.Text, this.txtDayDenpyoDateFr.MaxLength))
            {
                e.Cancel = true;
                this.txtDayDenpyoDateFr.SelectAll();
            }
            else
            {
                this.txtDayDenpyoDateFr.Text = Util.ToString(IsValid.SetDay(this.txtDayDenpyoDateFr.Text));
                CheckJpFr();
                SetJpFr();
            }
        }

        /// <summary>
        /// 伝票日付(至)・年値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGengoYearDenpyoDateTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsYear(this.txtGengoYearDenpyoDateTo.Text, this.txtGengoYearDenpyoDateTo.MaxLength))
            {
                e.Cancel = true;
                this.txtGengoYearDenpyoDateTo.SelectAll();
            }
            else
            {
                this.txtGengoYearDenpyoDateTo.Text = Util.ToString(IsValid.SetYear(this.txtGengoYearDenpyoDateTo.Text));
                CheckJpTo();
                SetJpTo();
            }
        }

        /// <summary>
        /// 伝票日付(至)・月の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonthDenpyoDateTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsMonth(this.txtMonthDenpyoDateTo.Text, this.txtMonthDenpyoDateTo.MaxLength))
            {
                e.Cancel = true;
                this.txtMonthDenpyoDateTo.SelectAll();
            }
            else
            {
                this.txtMonthDenpyoDateTo.Text = Util.ToString(IsValid.SetMonth(this.txtMonthDenpyoDateTo.Text));
                CheckJpTo();
                SetJpTo();
            }
        }

        /// <summary>
        /// 伝票日付(至)・日の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDayDenpyoDateTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValid.IsDay(this.txtDayDenpyoDateTo.Text, this.txtDayDenpyoDateTo.MaxLength))
            {
                e.Cancel = true;
                this.txtDayDenpyoDateTo.SelectAll();
            }
            else
            {
                this.txtDayDenpyoDateTo.Text = Util.ToString(IsValid.SetDay(this.txtDayDenpyoDateTo.Text));
                CheckJpTo();
                SetJpTo();
            }
        }

        /// <summary>
        /// 税率の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtZeiRt_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidZeiRitsu())
            {
                e.Cancel = true;
                this.txtZeiRt.SelectAll();
            }
        }
        #endregion

        #region イベント(UTable)

        /// <summary>
        /// UTableレコードフォーカス時
        /// </summary>
        private void mtbList_RecordEnter(UTable.CRecord record)
        {
            _activeCRecord = record;
        }

        /// <summary>
        /// UTableフォーカス喪失時
        /// </summary>
        private void mtbList_Leave(object sender, EventArgs e)
        {
            _activeCRecord = null;
        }

        /// <summary>
        /// UTableダブルクリック時
        /// </summary>
        private void mtbList_DoubleClick(object sender, EventArgs e)
        {
            this.ReturnVal();
        }

        /// <summary>
        /// UTableキーダウンイベント前
        /// </summary>
        private void mtbList_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) this.ReturnVal();

            int curIndex = this.mtbList.Content.Records.IndexOf(_activeCRecord);
            UTable.CRecord movRec;
            if (e.KeyCode == Keys.Right || e.KeyCode == Keys.Down)
            {
                if (curIndex < this.mtbList.Content.Records.Count - 1)
                {
                    movRec = this.mtbList.Content.Records[curIndex + 1];
                    movRec.Fields["DENPYO_BANGO"].Focus();
                }
            }
            if (e.KeyCode == Keys.Left || e.KeyCode == Keys.Up)
            {
                if (curIndex > 0)
                {
                    movRec = this.mtbList.Content.Records[curIndex];
                    movRec.Fields["DENPYO_BANGO"].Focus();
                }
            }
        }


        #endregion

        #region privateメソッド

        /// <summary>
        /// 明細部の初期化
        /// </summary>
        private void InitDetailArea()
        {
            UTable.CRecordProvider rp = new UTable.CRecordProvider();
            CLayoutBuilder lb = new CLayoutBuilder(CLayoutBuilder.EOrientation.ROW);
            UTable.CFieldDesc fd;

            #region UTableフィールド設定・配置
            //****************************************************************************************************
            // 次の行・列位置へ各フィールドを配置
            //
            // 　　　 (　　　　　　　借方　　　　　　　　 )　　　　     (　　　　　　　貸方　　　　　　　　 )
            //0 伝票番号 科目CD 科目名 部門CD　 　　　 金額　   証憑番号 工事 科目CD 科目名 部門CD　 　　　 金額　 
            //1 伝票日付 補助CD 補助名 事業区分 税区分 % 消費税 摘要   　　   補助CD 補助名 事業区分 税区分 % 消費税  
            //  0       1      2      3        4       5     6        7    8      9     10       11      12       
            //  0       1      2      3        4     5 6     7        8    9      10    11       12    13 14       
            //
            //(サイズ)
            //  60,     30,    100,   20,      20,   25,75,   70,      120, 30,   100,  20,      20,   25,75,
            //****************************************************************************************************
            // 伝票番号フィールド
            fd = rp.AddField("DENPYO_BANGO", new CSjTextFieldProvider("伝票番号."), lb.Set(0, 0).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.STOP;
            fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            // 伝票日付フィールド
            fd = rp.AddField("DENPYO_DATE", new CSjTextFieldProvider("伝票日付"), lb.Set(1, 0).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            // 借方勘定科目CDフィールド
            fd = rp.AddField("DrKANJO_KAMOKU_CD", new CSjTextFieldProvider("勘定科目"), lb.Set(0, 1).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            fd.MergeCaption = "DrKANJO_KAMOKU_NM";
            // 借方勘定科目名フィールド
            fd = rp.AddField("DrKANJO_KAMOKU_NM", new CSjTextFieldProvider(), lb.Set(0, 2).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.LEFT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            fd.CreateCaption = false;
            // 借方補助科目CDフィールド
            fd = rp.AddField("DrHOJO_KAMOKU_CD", new CSjTextFieldProvider("補助科目"), lb.Set(1, 1).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            fd.MergeCaption = "DrHOJO_KAMOKU_NM";
            // 借方補助科目名フィールド
            fd = rp.AddField("DrHOJO_KAMOKU_NM", new CSjTextFieldProvider(), lb.Set(1, 2).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.LEFT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            fd.CreateCaption = false;
            // 借方部門CDフィールド
            fd = rp.AddField("DrBUMON_CD", new CSjTextFieldProvider("部門"), lb.Set(0, 3).Next(1, 2));
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            // 借方事業区分フィールド
            fd = rp.AddField("DrJIGYO_KUBUN", new CSjTextFieldProvider("事"), lb.Set(1, 3).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            // 借方税区分フィールド
            fd = rp.AddField("DrZEI_KUBUN", new CSjTextFieldProvider("税"), lb.Set(1, 4).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            // 借方伝票金額フィールド
            fd = rp.AddField("DrDENPYO_KINGAKU", new CSjTextFieldProvider("金　額"), lb.Set(0, 5).Next(1, 2));
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            // 借方消費税率フィールド
            fd = rp.AddField("DrZEI_RITSU", new CSjTextFieldProvider("消費税"), lb.Set(1, 5).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            fd.Setting.ForeColor = Color.Blue;
            fd.MergeCaption = "DrSHOHIZEI_KINGAKU";
            // 借方消費税金額フィールド
            fd = rp.AddField("DrSHOHIZEI_KINGAKU", new CSjTextFieldProvider(), lb.Set(1, 6).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            fd.CreateCaption = false;
            // 証憑番号フィールド
            fd = rp.AddField("SHOHYO_BANGO", new CSjTextFieldProvider("証憑番号"), lb.Set(0, 7).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            // 工事フィールド
            //fd = rp.AddField("KOJI_NM", new CSjTextFieldProvider("工　事"), lb.Set(0, 8).Next());
            fd = rp.AddField("KOJI_NM", new CSjTextFieldProvider("　　　"), lb.Set(0, 8).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.LEFT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            // 摘要フィールド
            fd = rp.AddField("TEKIYO", new CSjTextFieldProvider("摘　　　　要"), lb.Set(1, 7).Next(1, 2));
            fd.Setting.HorizontalAlignment = UTable.EHAlign.LEFT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            // 貸方勘定科目CDフィールド
            fd = rp.AddField("CrKANJO_KAMOKU_CD", new CSjTextFieldProvider("勘定科目"), lb.Set(0, 9).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            fd.MergeCaption = "CrKANJO_KAMOKU_NM";
            // 貸方勘定科目名フィールド
            fd = rp.AddField("CrKANJO_KAMOKU_NM", new CSjTextFieldProvider(), lb.Set(0, 10).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.LEFT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            fd.CreateCaption = false;
            // 貸方補助科目CDフィールド
            fd = rp.AddField("CrHOJO_KAMOKU_CD", new CSjTextFieldProvider("補助科目"), lb.Set(1, 9).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            fd.MergeCaption = "CrHOJO_KAMOKU_NM";
            // 貸方補助科目名フィールド
            fd = rp.AddField("CrHOJO_KAMOKU_NM", new CSjTextFieldProvider(), lb.Set(1, 10).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.LEFT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            fd.CreateCaption = false;
            // 貸方部門CDフィールド
            fd = rp.AddField("CrBUMON_CD", new CSjTextFieldProvider("部門"), lb.Set(0, 11).Next(1, 2));
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            // 貸方事業区分フィールド
            fd = rp.AddField("CrJIGYO_KUBUN", new CSjTextFieldProvider("事"), lb.Set(1, 11).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            // 貸方税区分フィールド
            fd = rp.AddField("CrZEI_KUBUN", new CSjTextFieldProvider("税"), lb.Set(1, 12).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            // 貸方伝票金額フィールド
            fd = rp.AddField("CrDENPYO_KINGAKU", new CSjTextFieldProvider("金　額"), lb.Set(0, 13).Next(1, 2));
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            // 借方消費税率フィールド
            fd = rp.AddField("CrZEI_RITSU", new CSjTextFieldProvider("消費税"), lb.Set(1, 13).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            fd.Setting.ForeColor = Color.Blue;
            fd.MergeCaption = "CrSHOHIZEI_KINGAKU";
            // 貸方消費税金額フィールド
            fd = rp.AddField("CrSHOHIZEI_KINGAKU", new CSjTextFieldProvider(), lb.Set(1, 14).Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.TabStop = UTable.ETabStop.NOTSTOP;
            fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            fd.CreateCaption = false;

            #endregion

            this.mtbList.Content.SetRecordProvider(rp);
            this.mtbList.CreateCaption(UTable.EHAlign.MIDDLE);
            //this.mtbList.Cols.SetSize(60, 30, 100, 20, 20, 25, 75, 60, 120, 30, 100, 20, 20, 25, 75);
            this.mtbList.Cols.SetSize(56, 42, 100, 20, 20, 25, 75, 60, 119, 42, 100, 20, 20, 25, 75);
            this.mtbList.Setting.ContentBackColor = SystemColors.AppWorkspace;
            this.mtbList.KeyboardOperation.EnterBehavior = CKeyboardOperation.EBehavior.NONE;
            this.mtbList.KeyboardOperation.TabBehavior = CKeyboardOperation.EBehavior.NEXT_RECORD;

            // フォーカス移動時のヘッダーカラー
            this.mtbList.HeaderContent.Setting.FocusCaptionBackColor = Color.Transparent;
            // 列幅の変更不可
            this.mtbList.Setting.UserColResizable = UTable.EAllow.DISABLE;
        }

        /// <summary>
        /// データの読込
        /// </summary>
        /// <param name="isInitial">初期化フラグ</param>
        private void DataLoad(bool isInitial)
        {
            // 日付値
            DateTime dateFr = Util.ConvAdDate(this.lblGengoDenpyoDateFr.Text,
                this.txtGengoYearDenpyoDateFr.Text,
                this.txtMonthDenpyoDateFr.Text,
                this.txtDayDenpyoDateFr.Text,
                this.Dba);
            DateTime dateTo = Util.ConvAdDate(this.lblGengoDenpyoDateTo.Text,
                this.txtGengoYearDenpyoDateTo.Text,
                this.txtMonthDenpyoDateTo.Text,
                this.txtDayDenpyoDateTo.Text,
                this.Dba);

            // VI仕訳伝票データ
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, isInitial ? "-1" : this.UInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.ShishoCode);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@DENPYO_DATE_FR", SqlDbType.DateTime, 10, dateFr);
            dpc.SetParam("@DENPYO_DATE_TO", SqlDbType.DateTime, 10, dateTo);
            dpc.SetParam("@KANJO_KAMOKU_CD_FR", SqlDbType.Decimal, 6, 
                ValChk.IsEmpty(this.txtKanjoKamokuCdFr.Text) ? "0" : this.txtKanjoKamokuCdFr.Text);
            dpc.SetParam("@KANJO_KAMOKU_CD_TO", SqlDbType.Decimal, 6,
                ValChk.IsEmpty(this.txtKanjoKamokuCdTo.Text) ? "999999" : this.txtKanjoKamokuCdTo.Text);
            dpc.SetParam("@HOJO_KAMOKU_CD_FR", SqlDbType.Decimal, 10,
                ValChk.IsEmpty(this.txtHojoKamokuCdFr.Text) ? "0" : this.txtHojoKamokuCdFr.Text);
            dpc.SetParam("@HOJO_KAMOKU_CD_TO", SqlDbType.Decimal, 10,
                ValChk.IsEmpty(this.txtHojoKamokuCdTo.Text) ? "9999" : this.txtHojoKamokuCdTo.Text);
            dpc.SetParam("@ZEI_KUBUN_FR", SqlDbType.Decimal, 2,
                ValChk.IsEmpty(this.txtZeiKubunFr.Text) ? "0" : this.txtZeiKubunFr.Text);
            dpc.SetParam("@ZEI_KUBUN_TO", SqlDbType.Decimal, 2,
                ValChk.IsEmpty(this.txtZeiKubunTo.Text) ? "99" : this.txtZeiKubunTo.Text);
            dpc.SetParam("@BUMON_CD_FR", SqlDbType.Decimal, 4,
                ValChk.IsEmpty(this.txtBumonCdFr.Text) ? "0" : this.txtBumonCdFr.Text);
            dpc.SetParam("@BUMON_CD_TO", SqlDbType.Decimal, 4,
                ValChk.IsEmpty(this.txtBumonCdTo.Text) ? "9999" : this.txtBumonCdTo.Text);
            //dpc.SetParam("@KOJI_CD_FR", SqlDbType.Decimal, 4,
            //    ValChk.IsEmpty(this.txtKojiCdFr.Text) ? "0" : this.txtKojiCdFr.Text);
            //dpc.SetParam("@KOJI_CD_TO", SqlDbType.Decimal, 4,
            //    ValChk.IsEmpty(this.txtKojiCdTo.Text) ? "9999" : this.txtKojiCdTo.Text);
            dpc.SetParam("@SHOHYO_BANGO", SqlDbType.VarChar, 10, this.txtShohyoBango.Text);
            dpc.SetParam("@KINGAKU", SqlDbType.Decimal, 10, Util.ToDecimal(this.txtKingaku.Text));
            dpc.SetParam("@TEKIYO", SqlDbType.VarChar, 42, "%" + this.txtTekiyo.Text + "%");

            // 工事コード項目を担当者へ割り当て
            dpc.SetParam("@TANTOSHA_CD", SqlDbType.Decimal, 4,
                ValChk.IsEmpty(this.txtKojiCdFr.Text) ? 0 : Util.ToDecimal(this.txtKojiCdFr.Text));
            // 税率
            dpc.SetParam("@ZEI_RITSU", SqlDbType.Decimal, 4,
                ValChk.IsEmpty(this.txtKojiCdFr.Text) ? 0 : Util.ToDecimal(this.txtZeiRt.Text));

            StringBuilder cols;
            StringBuilder where;
            #region cols.Append(照会列)
            cols = new StringBuilder();
            cols.Append("KAISHA_CD");
            cols.Append(", KAIKEI_NENDO");
            cols.Append(", DENPYO_DATE");
            cols.Append(", DENPYO_BANGO");
            cols.Append(", GYO_BANGO");
            cols.Append(", TANTOSHA_CD");
            cols.Append(", TANTOSHA_NM");
            cols.Append(", SHOHYO_BANGO");
            cols.Append(", KARIKATA_KANJO_KAMOKU_CD");
            cols.Append(", KARIKATA_KANJO_KAMOKU_NM");
            cols.Append(", KARIKATA_HOJO_KAMOKU_CD");
            cols.Append(", KARIKATA_HOJO_KAMOKU_NM");
            cols.Append(", KARIKATA_BUMON_CD");
            cols.Append(", KARIKATA_BUMON_NM");
            cols.Append(", KARIKATA_ZEIKOMI_KINGAKU");
            cols.Append(", KARIKATA_ZEINUKI_KINGAKU");
            cols.Append(", KARIKATA_SHOHIZEI_KINGAKU");
            cols.Append(", KARIKATA_ZEI_KUBUN");
            cols.Append(", KARIKATA_ZEI_KUBUN_NM");
            cols.Append(", KARIKATA_KAZEI_KUBUN");
            cols.Append(", KARIKATA_TORIHIKI_KUBUN");
            cols.Append(", KARIKATA_ZEI_RITSU");
            cols.Append(", KARIKATA_JIGYO_KUBUN");
            cols.Append(", TEKIYO");
            cols.Append(", KASHIKATA_KANJO_KAMOKU_CD");
            cols.Append(", KASHIKATA_KANJO_KAMOKU_NM");
            cols.Append(", KASHIKATA_HOJO_KAMOKU_CD");
            cols.Append(", KASHIKATA_HOJO_KAMOKU_NM");
            cols.Append(", KASHIKATA_BUMON_CD");
            cols.Append(", KASHIKATA_BUMON_NM");
            cols.Append(", KASHIKATA_ZEIKOMI_KINGAKU");
            cols.Append(", KASHIKATA_ZEINUKI_KINGAKU");
            cols.Append(", KASHIKATA_SHOHIZEI_KINGAKU");
            cols.Append(", KASHIKATA_ZEI_KUBUN");
            cols.Append(", KASHIKATA_ZEI_KUBUN_NM");
            cols.Append(", KASHIKATA_KAZEI_KUBUN");
            cols.Append(", KASHIKATA_TORIHIKI_KUBUN");
            cols.Append(", KASHIKATA_ZEI_RITSU");
            cols.Append(", KASHIKATA_JIGYO_KUBUN");
            //cols.Append(", KOJI_CD");
            //cols.Append(", KOJI_NM");
            //cols.Append(", KOSHU_CD");
            //cols.Append(", KOSHU_NM");
            cols.Append(", KESSAN_KUBUN");
            #endregion
            where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND SHISHO_CD = @SHISHO_CD ");
            where.Append(" AND KAIKEI_NENDO = @KAIKEI_NENDO");
            where.Append(" AND DENPYO_DATE BETWEEN @DENPYO_DATE_FR AND @DENPYO_DATE_TO");
            where.Append(" AND (");     // 勘定科目範囲
            where.Append("  KARIKATA_KANJO_KAMOKU_CD BETWEEN @KANJO_KAMOKU_CD_FR AND @KANJO_KAMOKU_CD_TO");
            where.Append("  OR KASHIKATA_KANJO_KAMOKU_CD BETWEEN @KANJO_KAMOKU_CD_FR AND @KANJO_KAMOKU_CD_TO");
            where.Append(" )");
            where.Append(" AND (");     // 補助科目範囲
            where.Append("  KARIKATA_HOJO_KAMOKU_CD BETWEEN @HOJO_KAMOKU_CD_FR AND @HOJO_KAMOKU_CD_TO");
            where.Append("  OR KASHIKATA_HOJO_KAMOKU_CD BETWEEN @HOJO_KAMOKU_CD_FR AND @HOJO_KAMOKU_CD_TO");
            where.Append(" )");
            where.Append(" AND (");     // 税区分範囲
            where.Append("  KARIKATA_ZEI_KUBUN BETWEEN @ZEI_KUBUN_FR AND @ZEI_KUBUN_TO");
            where.Append("  OR KASHIKATA_ZEI_KUBUN BETWEEN @ZEI_KUBUN_FR AND @ZEI_KUBUN_TO");
            where.Append(" )");
            where.Append(" AND (");     // 部門範囲
            where.Append("  KARIKATA_BUMON_CD BETWEEN @BUMON_CD_FR AND @BUMON_CD_TO");
            where.Append("  OR KASHIKATA_BUMON_CD BETWEEN @BUMON_CD_FR AND @BUMON_CD_TO");
            where.Append(" )");
            //where.Append(" AND (KOJI_CD BETWEEN @KOJI_CD_FR AND @KOJI_CD_TO)");     // 工事範囲

            // 工事コード項目を担当者へ割り当て
            if (!ValChk.IsEmpty(this.txtKojiCdFr.Text))                              // 担当者
            {
                where.Append(" AND TANTOSHA_CD = @TANTOSHA_CD"); 
            }
            // 税率
            if (!ValChk.IsEmpty(this.txtZeiRt.Text))                                 // 税率
            {
                where.Append(" AND (");
                where.Append("  KARIKATA_ZEI_RITSU = @ZEI_RITSU");
                where.Append("  OR KASHIKATA_ZEI_RITSU = @ZEI_RITSU");
                where.Append(" )");
            }

            if (!ValChk.IsEmpty(this.txtShohyoBango.Text))                          // 証憑番号
            {
                where.Append(" AND SHOHYO_BANGO = @SHOHYO_BANGO");
            }
            if (Util.ToDecimal(this.txtKingaku.Text) > 0)                           // 金額
            {
                where.Append(" AND (");
                where.Append("  KARIKATA_ZEIKOMI_KINGAKU = @KINGAKU");
                where.Append("  OR KASHIKATA_ZEIKOMI_KINGAKU = @KINGAKU");
                where.Append(" )");
            }
            if (!ValChk.IsEmpty(this.txtTekiyo.Text))                               // 摘要
            {
                where.Append(" AND TEKIYO LIKE @TEKIYO");
            }
            if (rdoKessanKubun0.Checked) where.Append(" AND KESSAN_KUBUN = 0");
            if (rdoKessanKubun1.Checked) where.Append(" AND KESSAN_KUBUN = 1");
            DataTable dtDenpyo = this.Dba.GetDataTableByConditionWithParams(
                            Util.ToString(cols)
                            , "VI_ZM_SHIWAKE_DENPYO"
                            , Util.ToString(where), dpc);

            /// **************************************************
            /// UTableレンダリングブロック開始
            /// **************************************************
            using (mtbList.RenderBlock())
            {
                this.mtbList.Content.ClearRecord();         // UTableレコードクリア
                foreach (DataRow dr in dtDenpyo.Rows)
                {
                    // 仕訳伝票をUTableレコードへセット
                    UTable.CRecord rec;
                    rec = this.mtbList.Content.AddRecord();
                    // 伝票情報
                    rec.Fields["DENPYO_BANGO"].Value = Util.ToString(dr["DENPYO_BANGO"]);
                    string[] jpDate = Util.ConvJpDate(Util.ToDate(dr["DENPYO_DATE"]), this.Dba);
                    rec.Fields["DENPYO_DATE"].Value = 
                                                String.Format("{0, 2}", jpDate[2])
                                                + "/" + String.Format("{0, 2}", jpDate[3])
                                                + "/" + String.Format("{0, 2}", jpDate[4]);
                    rec.Fields["SHOHYO_BANGO"].Value = Util.ToString(dr["SHOHYO_BANGO"]);
                    //rec.Fields["KOJI_NM"].Value = Util.ToString(dr["KOJI_NM"]);
                    rec.Fields["TEKIYO"].Value = Util.ToString(dr["TEKIYO"]);

                    // 借方情報
                    rec.Fields["DrKANJO_KAMOKU_CD"].Value = ZaUtil.FormatNum(dr["KARIKATA_KANJO_KAMOKU_CD"]);
                    rec.Fields["DrKANJO_KAMOKU_NM"].Value = Util.ToString(dr["KARIKATA_KANJO_KAMOKU_NM"]);
                    rec.Fields["DrHOJO_KAMOKU_CD"].Value = ZaUtil.FormatNum(dr["KARIKATA_HOJO_KAMOKU_CD"]);
                    rec.Fields["DrHOJO_KAMOKU_NM"].Value = Util.ToString(dr["KARIKATA_HOJO_KAMOKU_NM"]);
                    rec.Fields["DrBUMON_CD"].Value = ZaUtil.FormatNum(dr["KARIKATA_BUMON_CD"]);
                    rec.Fields["DrJIGYO_KUBUN"].Value = ZaUtil.FormatNum(dr["KARIKATA_JIGYO_KUBUN"]);
                    rec.Fields["DrZEI_KUBUN"].Value = ZaUtil.FormatNum(dr["KARIKATA_ZEI_KUBUN"]);
                    rec.Fields["DrZEI_RITSU"].Value = ZaUtil.FormatPercentage(dr["KARIKATA_ZEI_RITSU"]);
                    rec.Fields["DrDENPYO_KINGAKU"].Value = ZaUtil.FormatCur(
                        ZaUtil.GetDenpyoKingaku(Util.ToDecimal(Util.ToString(dr["KARIKATA_ZEIKOMI_KINGAKU"])),
                                                Util.ToDecimal(Util.ToString(dr["KARIKATA_ZEINUKI_KINGAKU"])), this.UInfo));
                    rec.Fields["DrSHOHIZEI_KINGAKU"].Value = ZaUtil.FormatCur(dr["KARIKATA_SHOHIZEI_KINGAKU"]);
                    // 貸方情報
                    rec.Fields["CrKANJO_KAMOKU_CD"].Value = ZaUtil.FormatNum(dr["KASHIKATA_KANJO_KAMOKU_CD"]);
                    rec.Fields["CrKANJO_KAMOKU_NM"].Value = Util.ToString(dr["KASHIKATA_KANJO_KAMOKU_NM"]);
                    rec.Fields["CrHOJO_KAMOKU_CD"].Value = ZaUtil.FormatNum(dr["KASHIKATA_HOJO_KAMOKU_CD"]);
                    rec.Fields["CrHOJO_KAMOKU_NM"].Value = Util.ToString(dr["KASHIKATA_HOJO_KAMOKU_NM"]);
                    rec.Fields["CrBUMON_CD"].Value = ZaUtil.FormatNum(dr["KASHIKATA_BUMON_CD"]);
                    rec.Fields["CrJIGYO_KUBUN"].Value = Util.ToString(dr["KASHIKATA_JIGYO_KUBUN"]);
                    rec.Fields["CrZEI_KUBUN"].Value = ZaUtil.FormatNum(dr["KASHIKATA_ZEI_KUBUN"]);
                    rec.Fields["CrZEI_RITSU"].Value = ZaUtil.FormatPercentage(dr["KASHIKATA_ZEI_RITSU"]);
                    rec.Fields["CrDENPYO_KINGAKU"].Value = ZaUtil.FormatCur(
                        ZaUtil.GetDenpyoKingaku(Util.ToDecimal(Util.ToString(dr["KASHIKATA_ZEIKOMI_KINGAKU"])),
                                                Util.ToDecimal(Util.ToString(dr["KASHIKATA_ZEINUKI_KINGAKU"])),this.UInfo));
                    rec.Fields["CrSHOHIZEI_KINGAKU"].Value = ZaUtil.FormatCur(dr["KASHIKATA_SHOHIZEI_KINGAKU"]);

                    // 伝票金額、消費税がゼロ以下は文字色を赤にする
                    if (Util.ToDecimal(Util.ToString(dr["KARIKATA_ZEINUKI_KINGAKU"])) >= 0)
                    {
                        rec.Fields["DrDENPYO_KINGAKU"].Setting.ForeColor = Color.Black;
                        rec.Fields["DrSHOHIZEI_KINGAKU"].Setting.ForeColor = Color.Black;
                    }
                    else
                    {
                        rec.Fields["DrDENPYO_KINGAKU"].Setting.ForeColor = Color.Red;
                        rec.Fields["DrSHOHIZEI_KINGAKU"].Setting.ForeColor = Color.Red;
                    }
                    if (Util.ToDecimal(dr["KASHIKATA_ZEINUKI_KINGAKU"]) >= 0)
                    {
                        rec.Fields["CrDENPYO_KINGAKU"].Setting.ForeColor = Color.Black;
                        rec.Fields["CrSHOHIZEI_KINGAKU"].Setting.ForeColor = Color.Black;
                    }
                    else
                    {
                        rec.Fields["CrDENPYO_KINGAKU"].Setting.ForeColor = Color.Red;
                        rec.Fields["CrSHOHIZEI_KINGAKU"].Setting.ForeColor = Color.Red;
                    }
                }
            }
            /// **************************************************
            /// UTableレンダリングブロック終了
            /// **************************************************
            mtbList.Render();
        }

        /// <summary>
        /// 年月日(自)の月末入力チェック
        /// </summary>
        /// 
        private void CheckJpFr()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblGengoDenpyoDateFr.Text, this.txtGengoYearDenpyoDateFr.Text,
                this.txtMonthDenpyoDateFr.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDayDenpyoDateFr.Text) > lastDayInMonth)
            {
                this.txtDayDenpyoDateFr.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(自)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJpFr()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            //SetJpFr(Util.FixJpDate(this.lblGengoDenpyoDateFr.Text, this.txtGengoYearDenpyoDateFr.Text,
            //    this.txtMonthDenpyoDateFr.Text, this.txtDayDenpyoDateFr.Text, this.Dba));
            SetJpFr(Util.ConvJpDate(
                    ZaUtil.FixNendoDate(
                    Util.ConvAdDate(this.lblGengoDenpyoDateFr.Text,
                                    this.txtGengoYearDenpyoDateFr.Text,
                                    this.txtMonthDenpyoDateFr.Text,
                                    this.txtDayDenpyoDateFr.Text, this.Dba), this.UInfo), this.Dba));
        }

        /// <summary>
        /// 配列に格納された伝票日付(自)和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpFr(string[] arrJpDate)
        {
            this.lblGengoDenpyoDateFr.Text = arrJpDate[0];
            this.txtGengoYearDenpyoDateFr.Text = arrJpDate[2];
            this.txtMonthDenpyoDateFr.Text = arrJpDate[3];
            this.txtDayDenpyoDateFr.Text = arrJpDate[4];
        }

        /// <summary>
        /// 年月日(至)の月末入力チェック
        /// </summary>
        /// 
        private void CheckJpTo()
        {
            // 月末を超える日が入力された場合、月末として処理(年月が入力されていること前提)
            DateTime tmpDate = Util.ConvAdDate(this.lblGengoDenpyoDateTo.Text, this.txtGengoYearDenpyoDateTo.Text,
                this.txtMonthDenpyoDateTo.Text, "1", this.Dba);
            int lastDayInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);

            if (Util.ToInt(this.txtDayDenpyoDateTo.Text) > lastDayInMonth)
            {
                this.txtDayDenpyoDateTo.Text = Util.ToString(lastDayInMonth);
            }
        }

        /// <summary>
        /// 年月日(至)の正しい和暦への変換処理
        /// </summary>
        /// 
        private void SetJpTo()
        {
            // 本来その元号に存在しない日付である可能性があるので、
            // 一度西暦変換→和暦変換して、正しい和暦に戻す
            //SetJpTo(Util.FixJpDate(this.lblGengoDenpyoDateTo.Text, this.txtGengoYearDenpyoDateTo.Text,
            //    this.txtMonthDenpyoDateTo.Text, this.txtDayDenpyoDateTo.Text, this.Dba));
            SetJpTo(Util.ConvJpDate(
                    ZaUtil.FixNendoDate(
                    Util.ConvAdDate(this.lblGengoDenpyoDateTo.Text,
                                    this.txtGengoYearDenpyoDateTo.Text,
                                    this.txtMonthDenpyoDateTo.Text,
                                    this.txtDayDenpyoDateTo.Text, this.Dba), this.UInfo), this.Dba));
        }

        /// <summary>
        /// 配列に格納された伝票日付(至)和暦を画面にセットします。
        /// </summary>
        /// <param name="arrJpDate">和暦(Utilクラスのメソッドから返却された配列)</param>
        private void SetJpTo(string[] arrJpDate)
        {
            this.lblGengoDenpyoDateTo.Text = arrJpDate[0];
            this.txtGengoYearDenpyoDateTo.Text = arrJpDate[2];
            this.txtMonthDenpyoDateTo.Text = arrJpDate[3];
            this.txtDayDenpyoDateTo.Text = arrJpDate[4];
        }

        /// <summary>
        /// 呼出元に戻り値を返す
        /// </summary>
        private void ReturnVal()
        {
            if (_activeCRecord != null)
            {
                this.OutData = new string[2] {
                Util.ToString(_activeCRecord.Fields["DENPYO_BANGO"].Value),
                this.getDenpyoCondition(),
                };
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        /// <summary>
        /// 検索条件設定
        /// </summary>
        /// <param name="denpyoCondition">検索条件</param>
        private void setDenpyoCondition(string denpyoCondition)
        {
            string[] cnd = denpyoCondition.Split(',');
            if (cnd.Length != 0)
            {
                try
                {
                    string[] d = Util.ConvJpDate(Util.ToDate(cnd[0]), this.Dba);
                    SetJpFr(d);
                    d = Util.ConvJpDate(Util.ToDate(cnd[1]), this.Dba);
                    SetJpTo(d);

                    this.txtKanjoKamokuCdFr.Text = cnd[2];
                    this.txtKanjoKamokuCdTo.Text = cnd[3];
                    this.txtHojoKamokuCdFr.Text = cnd[4];
                    this.txtHojoKamokuCdTo.Text = cnd[5];

                    this.txtZeiKubunFr.Text = cnd[6];
                    this.txtZeiKubunTo.Text = cnd[7];
                    this.txtBumonCdFr.Text = cnd[8];
                    this.txtBumonCdTo.Text = cnd[9];

                    this.txtKojiCdFr.Text = cnd[10];
                    this.txtKojiCdTo.Text = cnd[11];

                    this.txtShohyoBango.Text = cnd[12];
                    this.txtKingaku.Text = cnd[13];
                    this.txtTekiyo.Text = cnd[14];

                    this.txtZeiRt.Text = cnd[15];

                    switch (Util.ToInt(cnd[16]))
                    {
                        case 0:
                            rdoKessanKubun0.Checked = true;
                            break;
                        case 1:
                            rdoKessanKubun1.Checked = true;
                            break;
                        default:
                            rdoKessanKubunAll.Checked = true;
                            break;
                    }
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.Print(ex.Message);
                }
            }
        }

        /// <summary>
        /// 検索条件取得
        /// </summary>
        /// <returns>検索条件配列</returns>
        private string getDenpyoCondition()
        {
            string KessanKubun = "2";
            if (rdoKessanKubun0.Checked) KessanKubun = "0";
            if (rdoKessanKubun1.Checked) KessanKubun = "1";

            string[] cnd = {
                            Util.ConvAdDate(this.lblGengoDenpyoDateFr.Text, this.txtGengoYearDenpyoDateFr.Text, this.txtMonthDenpyoDateFr.Text, this.txtDayDenpyoDateFr.Text, this.Dba).ToString("yyyy/MM/dd"),
                            Util.ConvAdDate(this.lblGengoDenpyoDateTo.Text, this.txtGengoYearDenpyoDateTo.Text, this.txtMonthDenpyoDateTo.Text, this.txtDayDenpyoDateTo.Text, this.Dba).ToString("yyyy/MM/dd"),
                            this.txtKanjoKamokuCdFr.Text,
                            this.txtKanjoKamokuCdTo.Text,
                            this.txtHojoKamokuCdFr.Text,
                            this.txtHojoKamokuCdTo.Text,
                            this.txtZeiKubunFr.Text,
                            this.txtZeiKubunTo.Text,
                            this.txtBumonCdFr.Text,
                            this.txtBumonCdTo.Text,
                            this.txtKojiCdFr.Text,
                            this.txtKojiCdTo.Text,
                            this.txtShohyoBango.Text,
                            this.txtKingaku.Text,
                            this.txtTekiyo.Text,
                            this.txtZeiRt.Text,
                            KessanKubun
            };

            return string.Join(",", cnd);
        }
        #endregion

        #region privateメソッド(入力チェック)

        /// <summary>
        /// 勘定科目コード(自)の入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidKanjoKamokuCdFr()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtKanjoKamokuCdFr.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 範囲データ存在確認
            if (!IsExistKanjyoKamoku())
            {
                Msg.Error("該当する勘定科目コードが入力されていません。明細を確認して下さい。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 勘定科目コード(至)の入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidKanjoKamokuCdTo()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtKanjoKamokuCdTo.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 範囲データ存在確認
            if (!IsExistKanjyoKamoku())
            {
                Msg.Error("該当する勘定科目コードが入力されていません。明細を確認して下さい。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 補助科目コード(自)の入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidHojoKamokuCdFr()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtHojoKamokuCdFr.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 補助科目コード(至)の入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidHojoKamokuCdTo()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtHojoKamokuCdTo.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 税区分(自)の入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidZeiKubunFr()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtZeiKubunFr.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 税区分(至)の入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidZeiKubunTo()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtZeiKubunTo.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 部門コード(自)の入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidBumonCdFr()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtBumonCdFr.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 部門コード(至)の入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidBumonCdTo()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtBumonCdTo.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        ///// <summary>
        ///// 工事コード(自)の入力チェック
        ///// </summary>
        ///// <returns>true:OK/false:NG</returns>
        //private bool IsValidKojiCdFr()
        //{
        //    // 数字のみの入力を許可
        //    if (!ValChk.IsNumber(this.txtKojiCdFr.Text))
        //    {
        //        Msg.Error("入力に誤りがあります。");
        //        return false;
        //    }

        //    return true;
        //}

        ///// <summary>
        ///// 工事コード(至)の入力チェック
        ///// </summary>
        ///// <returns>true:OK/false:NG</returns>
        //private bool IsValidKojiCdTo()
        //{
        //    // 数字のみの入力を許可
        //    if (!ValChk.IsNumber(this.txtKojiCdTo.Text))
        //    {
        //        Msg.Error("入力に誤りがあります。");
        //        return false;
        //    }

        //    return true;
        //}

        /// <summary>
        /// 担当者CDの値チェック処理
        /// </summary>
        /// <returns>true=OK, false=NG</returns>
        private bool isValidTantoshaCd()
        {
            // 数値のみの入力でない場合、エラーメッセージを表示し、フォーカスを移動しない
            if (!ValChk.IsNumber(this.txtKojiCdFr.Text))
            {
                Msg.Notice("数値のみで入力してください。");
                return false;
            }
            // 最大桁数チェック
            else if (!ValChk.IsWithinLength(this.txtKojiCdFr.Text, this.txtKojiCdFr.MaxLength))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 証憑番号の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidShohyoBango()
        {
            // 指定バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtShohyoBango.Text, this.txtShohyoBango.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 伝票金額の入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidKingaku()
        {
            // 10桁数値
            if (!ValChk.IsDecNumWithinLength(this.txtKingaku.Text, 10, 0, true))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 摘要の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidTekiyo()
        {
            // 指定バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtTekiyo.Text, this.txtTekiyo.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 税率の入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidZeiRitsu()
        {
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtZeiRt.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 勘定科目コード範囲の存在チェック
        /// </summary>
        private bool IsExistKanjyoKamoku()
        {
            // 勘定科目コード範囲のいずれかが未入力ならOK
            if (ValChk.IsEmpty(this.txtKanjoKamokuCdFr.Text) || ValChk.IsEmpty(this.txtKanjoKamokuCdTo.Text))
            {
                return true;
            }

            // 勘定科目テーブルの存在確認
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@KANJO_KAMOKU_CD_FR", SqlDbType.Decimal, 6, this.txtKanjoKamokuCdFr.Text);
            dpc.SetParam("@KANJO_KAMOKU_CD_TO", SqlDbType.Decimal, 6, this.txtKanjoKamokuCdTo.Text);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            StringBuilder cols = new StringBuilder();
            cols.Append("KANJO_KAMOKU_CD");
            StringBuilder where = new StringBuilder();
            where.Append("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND KAIKEI_NENDO = @KAIKEI_NENDO");
            where.Append(" AND KANJO_KAMOKU_CD BETWEEN @KANJO_KAMOKU_CD_FR AND @KANJO_KAMOKU_CD_TO");
            DataTable dt = this.Dba.GetDataTableByConditionWithParams(Util.ToString(cols)
                            , "TB_ZM_KANJO_KAMOKU"
                            , Util.ToString(where), dpc);
            return (dt.Rows.Count > 0);
        }
        #endregion
    }
}
