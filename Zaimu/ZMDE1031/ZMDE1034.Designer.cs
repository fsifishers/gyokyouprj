﻿namespace jp.co.fsi.zm.zmde1031
{
    partial class ZMDE1034
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvList = new System.Windows.Forms.DataGridView();
            this.lblTotal = new System.Windows.Forms.Label();
            this.lblDrTotal = new System.Windows.Forms.Label();
            this.lblCrTotal = new System.Windows.Forms.Label();
            this.lblDenpyoDateTo = new System.Windows.Forms.Label();
            this.lblDayDenpyoDateTo = new System.Windows.Forms.Label();
            this.txtDayDenpyoDateTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblMonthDenpyoDateTo = new System.Windows.Forms.Label();
            this.lblYearDenpyoDateTo = new System.Windows.Forms.Label();
            this.txtMonthDenpyoDateTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGengoDenpyoDateTo = new System.Windows.Forms.Label();
            this.txtGengoYearDenpyoDateTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblDayDenpyoDateFr = new System.Windows.Forms.Label();
            this.txtDayDenpyoDateFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblDenpyoDateFr = new System.Windows.Forms.Label();
            this.lblMonthDenpyoDateFr = new System.Windows.Forms.Label();
            this.lblYearDenpyoDateFr = new System.Windows.Forms.Label();
            this.txtMonthDenpyoDateFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblGengoDenpyoDateFr = new System.Windows.Forms.Label();
            this.txtGengoYearDenpyoDateFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKamokuNm = new System.Windows.Forms.Label();
            this.lblKamoku = new System.Windows.Forms.Label();
            this.lblZandaka = new System.Windows.Forms.Label();
            this.txtBiko = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblBakKamoku = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Text = "取引先元帳";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            // 
            // dgvList
            // 
            this.dgvList.AllowUserToAddRows = false;
            this.dgvList.AllowUserToDeleteRows = false;
            this.dgvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvList.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.dgvList.Location = new System.Drawing.Point(12, 117);
            this.dgvList.MultiSelect = false;
            this.dgvList.Name = "dgvList";
            this.dgvList.ReadOnly = true;
            this.dgvList.RowHeadersVisible = false;
            this.dgvList.RowTemplate.Height = 21;
            this.dgvList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvList.Size = new System.Drawing.Size(799, 379);
            this.dgvList.TabIndex = 0;
            this.dgvList.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dgvList_CellValidating);
            this.dgvList.Enter += new System.EventHandler(this.dgvList_Enter);
            this.dgvList.Leave += new System.EventHandler(this.dgvList_Leave);
            // 
            // lblTotal
            // 
            this.lblTotal.BackColor = System.Drawing.Color.Transparent;
            this.lblTotal.Font = new System.Drawing.Font("ＭＳ ゴシック", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTotal.Location = new System.Drawing.Point(12, 499);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(330, 20);
            this.lblTotal.TabIndex = 903;
            this.lblTotal.Text = "合計";
            this.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDrTotal
            // 
            this.lblDrTotal.BackColor = System.Drawing.Color.Transparent;
            this.lblDrTotal.Font = new System.Drawing.Font("ＭＳ ゴシック", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDrTotal.Location = new System.Drawing.Point(342, 499);
            this.lblDrTotal.Name = "lblDrTotal";
            this.lblDrTotal.Size = new System.Drawing.Size(100, 20);
            this.lblDrTotal.TabIndex = 904;
            this.lblDrTotal.Text = "1,234,567,890,123";
            this.lblDrTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblCrTotal
            // 
            this.lblCrTotal.BackColor = System.Drawing.Color.Transparent;
            this.lblCrTotal.Font = new System.Drawing.Font("ＭＳ ゴシック", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCrTotal.Location = new System.Drawing.Point(442, 499);
            this.lblCrTotal.Name = "lblCrTotal";
            this.lblCrTotal.Size = new System.Drawing.Size(100, 20);
            this.lblCrTotal.TabIndex = 905;
            this.lblCrTotal.Text = "1,234,567,890,123";
            this.lblCrTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblDenpyoDateTo
            // 
            this.lblDenpyoDateTo.AutoSize = true;
            this.lblDenpyoDateTo.BackColor = System.Drawing.Color.Silver;
            this.lblDenpyoDateTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDenpyoDateTo.Location = new System.Drawing.Point(312, 85);
            this.lblDenpyoDateTo.Name = "lblDenpyoDateTo";
            this.lblDenpyoDateTo.Size = new System.Drawing.Size(24, 16);
            this.lblDenpyoDateTo.TabIndex = 15;
            this.lblDenpyoDateTo.Text = "～";
            // 
            // lblDayDenpyoDateTo
            // 
            this.lblDayDenpyoDateTo.AutoSize = true;
            this.lblDayDenpyoDateTo.BackColor = System.Drawing.Color.Silver;
            this.lblDayDenpyoDateTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDayDenpyoDateTo.Location = new System.Drawing.Point(532, 85);
            this.lblDayDenpyoDateTo.Name = "lblDayDenpyoDateTo";
            this.lblDayDenpyoDateTo.Size = new System.Drawing.Size(24, 16);
            this.lblDayDenpyoDateTo.TabIndex = 14;
            this.lblDayDenpyoDateTo.Text = "日";
            // 
            // txtDayDenpyoDateTo
            // 
            this.txtDayDenpyoDateTo.AutoSizeFromLength = false;
            this.txtDayDenpyoDateTo.DisplayLength = null;
            this.txtDayDenpyoDateTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDayDenpyoDateTo.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtDayDenpyoDateTo.Location = new System.Drawing.Point(500, 82);
            this.txtDayDenpyoDateTo.MaxLength = 2;
            this.txtDayDenpyoDateTo.Name = "txtDayDenpyoDateTo";
            this.txtDayDenpyoDateTo.Size = new System.Drawing.Size(25, 23);
            this.txtDayDenpyoDateTo.TabIndex = 13;
            this.txtDayDenpyoDateTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDayDenpyoDateTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtDayDenpyoDateTo_Validating);
            // 
            // lblMonthDenpyoDateTo
            // 
            this.lblMonthDenpyoDateTo.AutoSize = true;
            this.lblMonthDenpyoDateTo.BackColor = System.Drawing.Color.Silver;
            this.lblMonthDenpyoDateTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMonthDenpyoDateTo.Location = new System.Drawing.Point(477, 85);
            this.lblMonthDenpyoDateTo.Name = "lblMonthDenpyoDateTo";
            this.lblMonthDenpyoDateTo.Size = new System.Drawing.Size(24, 16);
            this.lblMonthDenpyoDateTo.TabIndex = 12;
            this.lblMonthDenpyoDateTo.Text = "月";
            // 
            // lblYearDenpyoDateTo
            // 
            this.lblYearDenpyoDateTo.AutoSize = true;
            this.lblYearDenpyoDateTo.BackColor = System.Drawing.Color.Silver;
            this.lblYearDenpyoDateTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYearDenpyoDateTo.Location = new System.Drawing.Point(423, 85);
            this.lblYearDenpyoDateTo.Name = "lblYearDenpyoDateTo";
            this.lblYearDenpyoDateTo.Size = new System.Drawing.Size(24, 16);
            this.lblYearDenpyoDateTo.TabIndex = 10;
            this.lblYearDenpyoDateTo.Text = "年";
            // 
            // txtMonthDenpyoDateTo
            // 
            this.txtMonthDenpyoDateTo.AutoSizeFromLength = false;
            this.txtMonthDenpyoDateTo.DisplayLength = null;
            this.txtMonthDenpyoDateTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMonthDenpyoDateTo.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtMonthDenpyoDateTo.Location = new System.Drawing.Point(447, 82);
            this.txtMonthDenpyoDateTo.MaxLength = 2;
            this.txtMonthDenpyoDateTo.Name = "txtMonthDenpyoDateTo";
            this.txtMonthDenpyoDateTo.Size = new System.Drawing.Size(25, 23);
            this.txtMonthDenpyoDateTo.TabIndex = 11;
            this.txtMonthDenpyoDateTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonthDenpyoDateTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthDenpyoDateTo_Validating);
            // 
            // lblGengoDenpyoDateTo
            // 
            this.lblGengoDenpyoDateTo.BackColor = System.Drawing.Color.Silver;
            this.lblGengoDenpyoDateTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengoDenpyoDateTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengoDenpyoDateTo.Location = new System.Drawing.Point(342, 82);
            this.lblGengoDenpyoDateTo.Name = "lblGengoDenpyoDateTo";
            this.lblGengoDenpyoDateTo.Size = new System.Drawing.Size(49, 23);
            this.lblGengoDenpyoDateTo.TabIndex = 8;
            this.lblGengoDenpyoDateTo.Text = "平成";
            this.lblGengoDenpyoDateTo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtGengoYearDenpyoDateTo
            // 
            this.txtGengoYearDenpyoDateTo.AutoSizeFromLength = false;
            this.txtGengoYearDenpyoDateTo.DisplayLength = null;
            this.txtGengoYearDenpyoDateTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGengoYearDenpyoDateTo.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtGengoYearDenpyoDateTo.Location = new System.Drawing.Point(394, 82);
            this.txtGengoYearDenpyoDateTo.MaxLength = 2;
            this.txtGengoYearDenpyoDateTo.Name = "txtGengoYearDenpyoDateTo";
            this.txtGengoYearDenpyoDateTo.Size = new System.Drawing.Size(25, 23);
            this.txtGengoYearDenpyoDateTo.TabIndex = 9;
            this.txtGengoYearDenpyoDateTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGengoYearDenpyoDateTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtGengoYearDenpyoDateTo_Validating);
            // 
            // lblDayDenpyoDateFr
            // 
            this.lblDayDenpyoDateFr.AutoSize = true;
            this.lblDayDenpyoDateFr.BackColor = System.Drawing.Color.Silver;
            this.lblDayDenpyoDateFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDayDenpyoDateFr.Location = new System.Drawing.Point(285, 85);
            this.lblDayDenpyoDateFr.Name = "lblDayDenpyoDateFr";
            this.lblDayDenpyoDateFr.Size = new System.Drawing.Size(24, 16);
            this.lblDayDenpyoDateFr.TabIndex = 7;
            this.lblDayDenpyoDateFr.Text = "日";
            // 
            // txtDayDenpyoDateFr
            // 
            this.txtDayDenpyoDateFr.AutoSizeFromLength = false;
            this.txtDayDenpyoDateFr.DisplayLength = null;
            this.txtDayDenpyoDateFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDayDenpyoDateFr.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtDayDenpyoDateFr.Location = new System.Drawing.Point(253, 82);
            this.txtDayDenpyoDateFr.MaxLength = 2;
            this.txtDayDenpyoDateFr.Name = "txtDayDenpyoDateFr";
            this.txtDayDenpyoDateFr.Size = new System.Drawing.Size(25, 23);
            this.txtDayDenpyoDateFr.TabIndex = 5;
            this.txtDayDenpyoDateFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDayDenpyoDateFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDayDenpyoDateFr_Validating);
            // 
            // lblDenpyoDateFr
            // 
            this.lblDenpyoDateFr.AutoSize = true;
            this.lblDenpyoDateFr.BackColor = System.Drawing.Color.Silver;
            this.lblDenpyoDateFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDenpyoDateFr.Location = new System.Drawing.Point(23, 85);
            this.lblDenpyoDateFr.Name = "lblDenpyoDateFr";
            this.lblDenpyoDateFr.Size = new System.Drawing.Size(72, 16);
            this.lblDenpyoDateFr.TabIndex = 5;
            this.lblDenpyoDateFr.Text = "伝票日付";
            // 
            // lblMonthDenpyoDateFr
            // 
            this.lblMonthDenpyoDateFr.AutoSize = true;
            this.lblMonthDenpyoDateFr.BackColor = System.Drawing.Color.Silver;
            this.lblMonthDenpyoDateFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblMonthDenpyoDateFr.Location = new System.Drawing.Point(230, 85);
            this.lblMonthDenpyoDateFr.Name = "lblMonthDenpyoDateFr";
            this.lblMonthDenpyoDateFr.Size = new System.Drawing.Size(24, 16);
            this.lblMonthDenpyoDateFr.TabIndex = 4;
            this.lblMonthDenpyoDateFr.Text = "月";
            // 
            // lblYearDenpyoDateFr
            // 
            this.lblYearDenpyoDateFr.AutoSize = true;
            this.lblYearDenpyoDateFr.BackColor = System.Drawing.Color.Silver;
            this.lblYearDenpyoDateFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYearDenpyoDateFr.Location = new System.Drawing.Point(176, 85);
            this.lblYearDenpyoDateFr.Name = "lblYearDenpyoDateFr";
            this.lblYearDenpyoDateFr.Size = new System.Drawing.Size(24, 16);
            this.lblYearDenpyoDateFr.TabIndex = 2;
            this.lblYearDenpyoDateFr.Text = "年";
            // 
            // txtMonthDenpyoDateFr
            // 
            this.txtMonthDenpyoDateFr.AutoSizeFromLength = false;
            this.txtMonthDenpyoDateFr.DisplayLength = null;
            this.txtMonthDenpyoDateFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMonthDenpyoDateFr.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtMonthDenpyoDateFr.Location = new System.Drawing.Point(200, 82);
            this.txtMonthDenpyoDateFr.MaxLength = 2;
            this.txtMonthDenpyoDateFr.Name = "txtMonthDenpyoDateFr";
            this.txtMonthDenpyoDateFr.Size = new System.Drawing.Size(25, 23);
            this.txtMonthDenpyoDateFr.TabIndex = 3;
            this.txtMonthDenpyoDateFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMonthDenpyoDateFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtMonthDenpyoDateFr_Validating);
            // 
            // lblGengoDenpyoDateFr
            // 
            this.lblGengoDenpyoDateFr.BackColor = System.Drawing.Color.Silver;
            this.lblGengoDenpyoDateFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGengoDenpyoDateFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGengoDenpyoDateFr.Location = new System.Drawing.Point(95, 82);
            this.lblGengoDenpyoDateFr.Name = "lblGengoDenpyoDateFr";
            this.lblGengoDenpyoDateFr.Size = new System.Drawing.Size(49, 23);
            this.lblGengoDenpyoDateFr.TabIndex = 0;
            this.lblGengoDenpyoDateFr.Text = "平成";
            this.lblGengoDenpyoDateFr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtGengoYearDenpyoDateFr
            // 
            this.txtGengoYearDenpyoDateFr.AutoSizeFromLength = false;
            this.txtGengoYearDenpyoDateFr.DisplayLength = null;
            this.txtGengoYearDenpyoDateFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGengoYearDenpyoDateFr.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtGengoYearDenpyoDateFr.Location = new System.Drawing.Point(147, 82);
            this.txtGengoYearDenpyoDateFr.MaxLength = 2;
            this.txtGengoYearDenpyoDateFr.Name = "txtGengoYearDenpyoDateFr";
            this.txtGengoYearDenpyoDateFr.Size = new System.Drawing.Size(25, 23);
            this.txtGengoYearDenpyoDateFr.TabIndex = 1;
            this.txtGengoYearDenpyoDateFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGengoYearDenpyoDateFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtGengoYearDenpyoDateFr_Validating);
            // 
            // lblKamokuNm
            // 
            this.lblKamokuNm.BackColor = System.Drawing.Color.Silver;
            this.lblKamokuNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKamokuNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKamokuNm.Location = new System.Drawing.Point(95, 49);
            this.lblKamokuNm.Name = "lblKamokuNm";
            this.lblKamokuNm.Size = new System.Drawing.Size(461, 23);
            this.lblKamokuNm.TabIndex = 7;
            this.lblKamokuNm.Text = "平成";
            this.lblKamokuNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKamoku
            // 
            this.lblKamoku.AutoSize = true;
            this.lblKamoku.BackColor = System.Drawing.Color.Silver;
            this.lblKamoku.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKamoku.Location = new System.Drawing.Point(23, 52);
            this.lblKamoku.Name = "lblKamoku";
            this.lblKamoku.Size = new System.Drawing.Size(72, 16);
            this.lblKamoku.TabIndex = 6;
            this.lblKamoku.Text = "勘定科目";
            // 
            // lblZandaka
            // 
            this.lblZandaka.BackColor = System.Drawing.Color.Transparent;
            this.lblZandaka.Font = new System.Drawing.Font("ＭＳ ゴシック", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblZandaka.Location = new System.Drawing.Point(542, 499);
            this.lblZandaka.Name = "lblZandaka";
            this.lblZandaka.Size = new System.Drawing.Size(100, 20);
            this.lblZandaka.TabIndex = 906;
            this.lblZandaka.Text = "1,234,567,890,123";
            this.lblZandaka.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtBiko
            // 
            this.txtBiko.AutoSizeFromLength = false;
            this.txtBiko.DisplayLength = null;
            this.txtBiko.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtBiko.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtBiko.Location = new System.Drawing.Point(712, 45);
            this.txtBiko.MaxLength = 40;
            this.txtBiko.Name = "txtBiko";
            this.txtBiko.Size = new System.Drawing.Size(85, 23);
            this.txtBiko.TabIndex = 907;
            this.txtBiko.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBiko.Visible = false;
            // 
            // lblBakKamoku
            // 
            this.lblBakKamoku.BackColor = System.Drawing.Color.Silver;
            this.lblBakKamoku.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBakKamoku.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.lblBakKamoku.Location = new System.Drawing.Point(15, 45);
            this.lblBakKamoku.Name = "lblBakKamoku";
            this.lblBakKamoku.Size = new System.Drawing.Size(554, 32);
            this.lblBakKamoku.TabIndex = 908;
            this.lblBakKamoku.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Silver;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F);
            this.label1.Location = new System.Drawing.Point(15, 78);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(554, 32);
            this.label1.TabIndex = 909;
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ZMDE1034
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.lblDenpyoDateTo);
            this.Controls.Add(this.lblDayDenpyoDateTo);
            this.Controls.Add(this.txtDayDenpyoDateTo);
            this.Controls.Add(this.lblMonthDenpyoDateTo);
            this.Controls.Add(this.lblYearDenpyoDateTo);
            this.Controls.Add(this.txtMonthDenpyoDateTo);
            this.Controls.Add(this.lblGengoDenpyoDateTo);
            this.Controls.Add(this.txtGengoYearDenpyoDateTo);
            this.Controls.Add(this.lblDayDenpyoDateFr);
            this.Controls.Add(this.txtDayDenpyoDateFr);
            this.Controls.Add(this.lblDenpyoDateFr);
            this.Controls.Add(this.lblMonthDenpyoDateFr);
            this.Controls.Add(this.lblYearDenpyoDateFr);
            this.Controls.Add(this.txtMonthDenpyoDateFr);
            this.Controls.Add(this.lblGengoDenpyoDateFr);
            this.Controls.Add(this.txtGengoYearDenpyoDateFr);
            this.Controls.Add(this.lblKamokuNm);
            this.Controls.Add(this.lblKamoku);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblBakKamoku);
            this.Controls.Add(this.txtBiko);
            this.Controls.Add(this.lblZandaka);
            this.Controls.Add(this.lblCrTotal);
            this.Controls.Add(this.lblDrTotal);
            this.Controls.Add(this.lblTotal);
            this.Controls.Add(this.dgvList);
            this.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.Name = "ZMDE1034";
            this.ShowFButton = true;
            this.Text = "元帳照会";
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.dgvList, 0);
            this.Controls.SetChildIndex(this.lblTotal, 0);
            this.Controls.SetChildIndex(this.lblDrTotal, 0);
            this.Controls.SetChildIndex(this.lblCrTotal, 0);
            this.Controls.SetChildIndex(this.lblZandaka, 0);
            this.Controls.SetChildIndex(this.txtBiko, 0);
            this.Controls.SetChildIndex(this.lblBakKamoku, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.lblKamoku, 0);
            this.Controls.SetChildIndex(this.lblKamokuNm, 0);
            this.Controls.SetChildIndex(this.txtGengoYearDenpyoDateFr, 0);
            this.Controls.SetChildIndex(this.lblGengoDenpyoDateFr, 0);
            this.Controls.SetChildIndex(this.txtMonthDenpyoDateFr, 0);
            this.Controls.SetChildIndex(this.lblYearDenpyoDateFr, 0);
            this.Controls.SetChildIndex(this.lblMonthDenpyoDateFr, 0);
            this.Controls.SetChildIndex(this.lblDenpyoDateFr, 0);
            this.Controls.SetChildIndex(this.txtDayDenpyoDateFr, 0);
            this.Controls.SetChildIndex(this.lblDayDenpyoDateFr, 0);
            this.Controls.SetChildIndex(this.txtGengoYearDenpyoDateTo, 0);
            this.Controls.SetChildIndex(this.lblGengoDenpyoDateTo, 0);
            this.Controls.SetChildIndex(this.txtMonthDenpyoDateTo, 0);
            this.Controls.SetChildIndex(this.lblYearDenpyoDateTo, 0);
            this.Controls.SetChildIndex(this.lblMonthDenpyoDateTo, 0);
            this.Controls.SetChildIndex(this.txtDayDenpyoDateTo, 0);
            this.Controls.SetChildIndex(this.lblDayDenpyoDateTo, 0);
            this.Controls.SetChildIndex(this.lblDenpyoDateTo, 0);
            this.pnlDebug.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvList;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.Label lblDrTotal;
        private System.Windows.Forms.Label lblCrTotal;
        private System.Windows.Forms.Label lblDayDenpyoDateFr;
        private jp.co.fsi.common.controls.FsiTextBox txtDayDenpyoDateFr;
        private System.Windows.Forms.Label lblDenpyoDateFr;
        private System.Windows.Forms.Label lblMonthDenpyoDateFr;
        private System.Windows.Forms.Label lblYearDenpyoDateFr;
        private jp.co.fsi.common.controls.FsiTextBox txtMonthDenpyoDateFr;
        private System.Windows.Forms.Label lblGengoDenpyoDateFr;
        private jp.co.fsi.common.controls.FsiTextBox txtGengoYearDenpyoDateFr;
        private System.Windows.Forms.Label lblDenpyoDateTo;
        private System.Windows.Forms.Label lblDayDenpyoDateTo;
        private jp.co.fsi.common.controls.FsiTextBox txtDayDenpyoDateTo;
        private System.Windows.Forms.Label lblMonthDenpyoDateTo;
        private System.Windows.Forms.Label lblYearDenpyoDateTo;
        private jp.co.fsi.common.controls.FsiTextBox txtMonthDenpyoDateTo;
        private System.Windows.Forms.Label lblGengoDenpyoDateTo;
        private jp.co.fsi.common.controls.FsiTextBox txtGengoYearDenpyoDateTo;
        private System.Windows.Forms.Label lblKamokuNm;
        private System.Windows.Forms.Label lblKamoku;
        private System.Windows.Forms.Label lblZandaka;
        private jp.co.fsi.common.controls.FsiTextBox txtBiko;
        private System.Windows.Forms.Label lblBakKamoku;
        private System.Windows.Forms.Label label1;
    }
}