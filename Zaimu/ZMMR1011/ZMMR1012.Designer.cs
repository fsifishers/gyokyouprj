﻿namespace jp.co.fsi.zm.zmmr1011
{
    partial class ZMMR1012
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblKanjoBango = new System.Windows.Forms.Label();
            this.lblKanjoKamoku = new System.Windows.Forms.Label();
            this.lblJp = new System.Windows.Forms.Label();
            this.mtbList = new jp.co.fsi.common.controls.SjMultiTable();
            this.lblZei = new System.Windows.Forms.Label();
            this.lblGokei = new System.Windows.Forms.Label();
            this.lblKariGokei = new System.Windows.Forms.Label();
            this.lblKariZeiGokei = new System.Windows.Forms.Label();
            this.lblKashiZeiGokei = new System.Windows.Forms.Label();
            this.lblKashiGokei = new System.Windows.Forms.Label();
            this.lblZan = new System.Windows.Forms.Label();
            this.lblWaku = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(731, 23);
            this.lblTitle.Text = "";
            // 
            // btnF8
            // 
            this.btnF8.Text = "F8\r\n\r\n次へ";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(5, 477);
            this.pnlDebug.Size = new System.Drawing.Size(764, 100);
            // 
            // lblKanjoBango
            // 
            this.lblKanjoBango.BackColor = System.Drawing.Color.Transparent;
            this.lblKanjoBango.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKanjoBango.ForeColor = System.Drawing.Color.Black;
            this.lblKanjoBango.Location = new System.Drawing.Point(14, 13);
            this.lblKanjoBango.Name = "lblKanjoBango";
            this.lblKanjoBango.Size = new System.Drawing.Size(50, 20);
            this.lblKanjoBango.TabIndex = 906;
            this.lblKanjoBango.Text = "123456";
            this.lblKanjoBango.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblKanjoKamoku
            // 
            this.lblKanjoKamoku.BackColor = System.Drawing.Color.Transparent;
            this.lblKanjoKamoku.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKanjoKamoku.ForeColor = System.Drawing.Color.Black;
            this.lblKanjoKamoku.Location = new System.Drawing.Point(65, 13);
            this.lblKanjoKamoku.Name = "lblKanjoKamoku";
            this.lblKanjoKamoku.Size = new System.Drawing.Size(134, 20);
            this.lblKanjoKamoku.TabIndex = 907;
            this.lblKanjoKamoku.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblJp
            // 
            this.lblJp.BackColor = System.Drawing.Color.Transparent;
            this.lblJp.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJp.ForeColor = System.Drawing.Color.Black;
            this.lblJp.Location = new System.Drawing.Point(352, 14);
            this.lblJp.Name = "lblJp";
            this.lblJp.Size = new System.Drawing.Size(303, 20);
            this.lblJp.TabIndex = 908;
            this.lblJp.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // mtbList
            // 
            this.mtbList.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.mtbList.FixedCols = 0;
            this.mtbList.FocusField = null;
            this.mtbList.Location = new System.Drawing.Point(13, 36);
            this.mtbList.Name = "mtbList";
            this.mtbList.NotSelectableCols = 0;
            this.mtbList.SelectRange = null;
            this.mtbList.Size = new System.Drawing.Size(726, 441);
            this.mtbList.TabIndex = 912;
            this.mtbList.Text = "SjMultiTable1";
            this.mtbList.UndoBufferEnabled = false;
            // 
            // lblZei
            // 
            this.lblZei.BackColor = System.Drawing.Color.Transparent;
            this.lblZei.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblZei.ForeColor = System.Drawing.Color.Black;
            this.lblZei.Location = new System.Drawing.Point(661, 14);
            this.lblZei.Name = "lblZei";
            this.lblZei.Size = new System.Drawing.Size(78, 20);
            this.lblZei.TabIndex = 913;
            this.lblZei.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblGokei
            // 
            this.lblGokei.BackColor = System.Drawing.SystemColors.Control;
            this.lblGokei.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblGokei.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGokei.ForeColor = System.Drawing.Color.Black;
            this.lblGokei.Location = new System.Drawing.Point(13, 477);
            this.lblGokei.Name = "lblGokei";
            this.lblGokei.Size = new System.Drawing.Size(469, 42);
            this.lblGokei.TabIndex = 914;
            this.lblGokei.Text = "合　　　計";
            this.lblGokei.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblKariGokei
            // 
            this.lblKariGokei.BackColor = System.Drawing.Color.Transparent;
            this.lblKariGokei.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblKariGokei.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKariGokei.ForeColor = System.Drawing.Color.Black;
            this.lblKariGokei.Location = new System.Drawing.Point(481, 497);
            this.lblKariGokei.Name = "lblKariGokei";
            this.lblKariGokei.Size = new System.Drawing.Size(82, 22);
            this.lblKariGokei.TabIndex = 915;
            this.lblKariGokei.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblKariZeiGokei
            // 
            this.lblKariZeiGokei.BackColor = System.Drawing.Color.Transparent;
            this.lblKariZeiGokei.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblKariZeiGokei.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKariZeiGokei.ForeColor = System.Drawing.Color.Black;
            this.lblKariZeiGokei.Location = new System.Drawing.Point(481, 477);
            this.lblKariZeiGokei.Name = "lblKariZeiGokei";
            this.lblKariZeiGokei.Size = new System.Drawing.Size(82, 21);
            this.lblKariZeiGokei.TabIndex = 916;
            this.lblKariZeiGokei.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblKashiZeiGokei
            // 
            this.lblKashiZeiGokei.BackColor = System.Drawing.Color.Transparent;
            this.lblKashiZeiGokei.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblKashiZeiGokei.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKashiZeiGokei.ForeColor = System.Drawing.Color.Black;
            this.lblKashiZeiGokei.Location = new System.Drawing.Point(561, 477);
            this.lblKashiZeiGokei.Name = "lblKashiZeiGokei";
            this.lblKashiZeiGokei.Size = new System.Drawing.Size(82, 21);
            this.lblKashiZeiGokei.TabIndex = 918;
            this.lblKashiZeiGokei.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblKashiGokei
            // 
            this.lblKashiGokei.BackColor = System.Drawing.Color.Transparent;
            this.lblKashiGokei.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblKashiGokei.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKashiGokei.ForeColor = System.Drawing.Color.Black;
            this.lblKashiGokei.Location = new System.Drawing.Point(561, 497);
            this.lblKashiGokei.Name = "lblKashiGokei";
            this.lblKashiGokei.Size = new System.Drawing.Size(82, 22);
            this.lblKashiGokei.TabIndex = 917;
            this.lblKashiGokei.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblZan
            // 
            this.lblZan.BackColor = System.Drawing.Color.Transparent;
            this.lblZan.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblZan.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblZan.ForeColor = System.Drawing.Color.Black;
            this.lblZan.Location = new System.Drawing.Point(642, 497);
            this.lblZan.Name = "lblZan";
            this.lblZan.Size = new System.Drawing.Size(81, 22);
            this.lblZan.TabIndex = 919;
            this.lblZan.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblWaku
            // 
            this.lblWaku.BackColor = System.Drawing.Color.Transparent;
            this.lblWaku.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblWaku.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblWaku.ForeColor = System.Drawing.Color.Black;
            this.lblWaku.Location = new System.Drawing.Point(642, 477);
            this.lblWaku.Name = "lblWaku";
            this.lblWaku.Size = new System.Drawing.Size(81, 22);
            this.lblWaku.TabIndex = 920;
            this.lblWaku.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ZMMR1012
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(756, 580);
            this.Controls.Add(this.lblZei);
            this.Controls.Add(this.mtbList);
            this.Controls.Add(this.lblJp);
            this.Controls.Add(this.lblKanjoKamoku);
            this.Controls.Add(this.lblKanjoBango);
            this.Controls.Add(this.lblZan);
            this.Controls.Add(this.lblKariZeiGokei);
            this.Controls.Add(this.lblKariGokei);
            this.Controls.Add(this.lblGokei);
            this.Controls.Add(this.lblWaku);
            this.Controls.Add(this.lblKashiZeiGokei);
            this.Controls.Add(this.lblKashiGokei);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "ZMMR1012";
            this.ShowFButton = true;
            this.Text = "";
            this.Shown += new System.EventHandler(this.ZMMR1012_Shown);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblKashiGokei, 0);
            this.Controls.SetChildIndex(this.lblKashiZeiGokei, 0);
            this.Controls.SetChildIndex(this.lblWaku, 0);
            this.Controls.SetChildIndex(this.lblGokei, 0);
            this.Controls.SetChildIndex(this.lblKariGokei, 0);
            this.Controls.SetChildIndex(this.lblKariZeiGokei, 0);
            this.Controls.SetChildIndex(this.lblZan, 0);
            this.Controls.SetChildIndex(this.lblKanjoBango, 0);
            this.Controls.SetChildIndex(this.lblKanjoKamoku, 0);
            this.Controls.SetChildIndex(this.lblJp, 0);
            this.Controls.SetChildIndex(this.mtbList, 0);
            this.Controls.SetChildIndex(this.lblZei, 0);
            this.pnlDebug.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblKanjoBango;
        private System.Windows.Forms.Label lblKanjoKamoku;
        private System.Windows.Forms.Label lblJp;
        private jp.co.fsi.common.controls.SjMultiTable mtbList;
        private System.Windows.Forms.Label lblZei;
        private System.Windows.Forms.Label lblGokei;
        private System.Windows.Forms.Label lblKariGokei;
        private System.Windows.Forms.Label lblKariZeiGokei;
        private System.Windows.Forms.Label lblKashiZeiGokei;
        private System.Windows.Forms.Label lblKashiGokei;
        private System.Windows.Forms.Label lblZan;
        private System.Windows.Forms.Label lblWaku;



    }
}