﻿using System;
using System.Collections;
using System.Data;
using System.Text;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.userinfo;
using jp.co.fsi.common.util;

namespace jp.co.fsi.zm.zmmr1011
{
    /// <summary>
    /// モジュール全体で使用するデータアクセスクラスです。
    /// </summary>
    public class ZMMR1011DA
    {
        #region private変数
        /// <summary>
        /// ユーザー情報
        /// </summary>
        UserInfo _uInfo;

        /// <summary>
        /// データアクセスオブジェクト
        /// </summary>
        DbAccess _dba;

        /// <summary>
        /// 設定ファイルアクセスオブジェクト
        /// </summary>
        ConfigLoader _config;

        /// <summary>
        /// 勘定科目リスト（試算表からの呼び出し時）
        /// </summary>
        string _kanjoKamokuList = "";
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="uInfo">操作中のユーザーの情報</param>
        /// <param name="dba">呼び出し元で保持するデータアクセスオブジェクト</param>
        /// <param name="config">呼び出し元で保持する設定ファイルアクセスオブジェクト</param>
        public ZMMR1011DA(UserInfo uInfo, DbAccess dba, ConfigLoader config)
        {
            this._uInfo = uInfo;
            this._dba = dba;
            this._config = config;
            this._kanjoKamokuList = "";
        }
        public ZMMR1011DA(UserInfo uInfo, DbAccess dba, ConfigLoader config, string KanjoKamokuList)
        {
            this._uInfo = uInfo;
            this._dba = dba;
            this._config = config;
            this._kanjoKamokuList = KanjoKamokuList;
        }
        #endregion

        #region publicメソッド
        /// <summary>
        /// 仕訳対象データを取得
        /// </summary>
        /// <param name="condition">条件画面の入力値</param>
        /// <returns>仕訳対象データ</returns>
        public DataTable GetSwkTgtData(Hashtable condition)
        {
            // SetSubjectScript1

            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();

            int shiwakeShori = Util.ToInt(condition["ShiwakeShurui"]);
            int shohizeiShoriHandan = Util.ToInt(condition["ShohizeiShoriHandan"]);
            // 支所コード
            int shishoCd = Util.ToInt(condition["ShishoCode"]);

            string colName = "ZEIKOMI_KINGAKU";
            if (shohizeiShoriHandan == 1)
            {
                colName = "ZEIKOMI_KINGAKU";
            }
            else
            {
                colName = "ZEINUKI_KINGAKU";
            }

            sql.Append(" SELECT");
            sql.Append(" A.SHISHO_CD            AS SHISHO_CD,");
            sql.Append(" A.KANJO_KAMOKU_CD      AS KAMOKU_CD,");
            sql.Append(" MAX(B.KANJO_KAMOKU_NM) AS KAMOKU_NM,");
            sql.Append(" MAX(B.TAISHAKU_KUBUN)  AS TAISHAKU_KUBUN,");
            sql.Append(" SUM(CASE WHEN A.DENPYO_DATE < @DATE_FR"); // 繰越残高なので、開始日前日までのデータ取得で良い（前年度の3月31日に繰越額が入っている)
            sql.Append("         THEN CASE WHEN A.TAISHAKU_KUBUN = B.TAISHAKU_KUBUN");
            //sql.Append("                  THEN A.ZEIKOMI_KINGAKU ELSE A.ZEIKOMI_KINGAKU * -1 END ELSE 0 END) AS KURIKOSHI_ZANDAKA");
            sql.Append("                  THEN A."+ colName + " ELSE A." + colName + " * -1 END ELSE 0 END) AS KURIKOSHI_ZANDAKA");
            sql.Append(" FROM TB_ZM_SHIWAKE_MEISAI AS A");
            sql.Append(" LEFT OUTER JOIN TB_ZM_KANJO_KAMOKU AS B");
            sql.Append(" ON A.KAISHA_CD = B.KAISHA_CD AND");
            sql.Append(" A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD AND");
            sql.Append(" A.KAIKEI_NENDO = B.KAIKEI_NENDO");
            sql.Append(" WHERE");
            sql.Append(" A.KAISHA_CD = @KAISHA_CD AND");

            if (shishoCd != 0)
                sql.Append(" A.SHISHO_CD = @SHISHO_CD AND");

            sql.Append(" A.KAISHA_CD = @KAISHA_CD AND");
            if (this._kanjoKamokuList != "")
            {
                sql.Append(" A.KANJO_KAMOKU_CD IN( " + this._kanjoKamokuList + " ) AND");
            }
            else
            {
                sql.Append(" A.KANJO_KAMOKU_CD BETWEEN @KANJO_KAMOKU_FR AND @KANJO_KAMOKU_TO AND");
            }
            sql.Append(" A.DENPYO_DATE <= @DATE_TO AND"); // 現行版も同様だが、開始日以前(前年度繰越データ＋会計年度開始日）からデータ取得する
            sql.Append(" A.BUMON_CD BETWEEN @BUMON_FR AND @BUMON_TO AND");
            //sql.Append(" A.KOJI_CD BETWEEN @KOJI_FR AND @KOJI_TO AND");
            if (shohizeiShoriHandan == 1)
            {
                sql.Append(" A.MEISAI_KUBUN <= 0 AND");
            }
            else
            {
                sql.Append(" A.MEISAI_KUBUN >= 0 AND ");
            }
            if (shiwakeShori != 9)
            {
                sql.Append(" A.KESSAN_KUBUN = @SHIWAKE_SHORI AND");
            }
            sql.Append(" A.KAIKEI_NENDO = @KAIKEI_NENDO");  // 現行版はテーブルが会計年度ごとに分かれているので、これで良い
            sql.Append(" GROUP BY A.SHISHO_CD, A.KANJO_KAMOKU_CD");
            sql.Append(" ORDER BY A.SHISHO_CD, A.KANJO_KAMOKU_CD");
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            dpc.SetParam("@SHIWAKE_SHORI", SqlDbType.Decimal, 1, shiwakeShori);
            dpc.SetParam("@DATE_FR", SqlDbType.DateTime, Util.ToDate(condition["DtFr"]));
            dpc.SetParam("@DATE_TO", SqlDbType.DateTime, Util.ToDate(condition["DtTo"]));
            dpc.SetParam("@KANJO_KAMOKU_FR", SqlDbType.VarChar, 6, Util.ToString(condition["KnjoKamokuFr"]));
            dpc.SetParam("@KANJO_KAMOKU_TO", SqlDbType.VarChar, 6, Util.ToString(condition["KnjoKamokuTo"]));
            dpc.SetParam("@BUMON_FR", SqlDbType.VarChar, 4, Util.ToString(condition["BumonFr"]));
            dpc.SetParam("@BUMON_TO", SqlDbType.VarChar, 4, Util.ToString(condition["BumonTo"]));
            //dpc.SetParam("@KOJI_FR", SqlDbType.VarChar, 4, Util.ToString(condition["KojiFr"]));
            //dpc.SetParam("@KOJI_TO", SqlDbType.VarChar, 4, Util.ToString(condition["KojiTo"]));

            DataTable dtKanjoKamoku = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dtKanjoKamoku;
        }

        /// <summary>
        /// 相手科目コードを取得
        /// </summary>
        /// <param name="denpyoBango">伝票番号</param>
        /// <param name="denpyoData">伝票日付</param>
        /// <param name="gyoBango">行番号</param>
        /// <param name="taishakuKubun">貸借区分</param>
        /// <param name="kaikeiNendo">会計年度</param>
        /// <param name="shishoCd">支所コード</param>
        /// <returns>相手科目名コード</returns>
        /// 
        public DataTable GetKanjoKamokuNm(int denpyoBango, string denpyoData, int gyoBango, int taishakuKubun, int kaikeiNendo, int shishoCd)
        {
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();

            if (taishakuKubun == 1)
            {
                taishakuKubun = 2;
            }
            else
            {
                taishakuKubun = 1;
            }

            sql.Append(" SELECT");
            sql.Append(" KANJO_KAMOKU_CD,");
            sql.Append(" HOJO_KAMOKU_CD");
            sql.Append(" FROM");
            sql.Append(" TB_ZM_SHIWAKE_MEISAI");
            sql.Append(" WHERE");
            sql.Append(" KAISHA_CD = @KAISHA_CD AND");
            sql.Append(" SHISHO_CD = @SHISHO_CD AND");
            sql.Append(" KAIKEI_NENDO = @KAIKEI_NENDO AND");
            sql.Append(" DENPYO_BANGO = @DENPYO_BANGO AND");
            sql.Append(" DENPYO_DATE = CAST(@DENPYO_DATE AS DATETIME) AND");
            sql.Append(" GYO_BANGO = @GYO_BANGO AND");
            sql.Append(" TAISHAKU_KUBUN = @TAISHAKU_KUBUN AND");
            sql.Append(" MEISAI_KUBUN <= 0 AND");
            sql.Append(" DENPYO_KUBUN = 1");
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, kaikeiNendo);
            dpc.SetParam("@DENPYO_BANGO", SqlDbType.Decimal, 4, denpyoBango);
            dpc.SetParam("@DENPYO_DATE", SqlDbType.DateTime, 4, Util.ToDate(denpyoData));
            dpc.SetParam("@GYO_BANGO", SqlDbType.Decimal, 4, gyoBango);
            dpc.SetParam("@TAISHAKU_KUBUN", SqlDbType.Decimal, 4, taishakuKubun);

            DataTable dtKanjoKamoku = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dtKanjoKamoku;
        }

        /// <summary>
        /// 相手科目名を取得
        /// </summary>
        /// <param name="kanjoKamoku">勘定科目コード</param>
        /// <param name="flag">フラグ</param>
        /// <param name="kaikeiNendo">会計年度</param>
        /// <param name="shishoCd">支所コード</param>
        /// <returns>相手科目名</returns>
        /// 
        public string GetKanjoKamokuNm(int kanjoKamokuCd, int flag, int kaikeiNendo, int shishoCd)
        {
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();

            sql.Append(" SELECT");
            sql.Append(" MAX(B.KANJO_KAMOKU_NM) AS KAMOKU_NM");
            sql.Append(" FROM");
            sql.Append(" TB_ZM_SHIWAKE_MEISAI AS A");
            sql.Append(" LEFT OUTER JOIN");
            sql.Append(" TB_ZM_KANJO_KAMOKU AS B");
            sql.Append(" ON A.KAISHA_CD = B.KAISHA_CD AND");
            sql.Append(" A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD");
            sql.Append(" WHERE");
            sql.Append(" A.KAISHA_CD = @KAISHA_CD AND");
            sql.Append(" A.SHISHO_CD = @SHISHO_CD AND");
            sql.Append(" A.KANJO_KAMOKU_CD = @KANJO_KAMOKU");
            sql.Append(" GROUP BY");
            sql.Append(" A.KANJO_KAMOKU_CD");
            sql.Append(" ORDER BY");
            sql.Append(" A.KANJO_KAMOKU_CD");
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            dpc.SetParam("@KANJO_KAMOKU", SqlDbType.Decimal, 6, kanjoKamokuCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, kaikeiNendo);

            DataTable dtKanjoKamoku = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            string kanjoKamoku;
            if (flag == 0)
            {
                kanjoKamoku = " " + kanjoKamokuCd + " " + Util.ToString(dtKanjoKamoku.Rows[0]["KAMOKU_NM"]);
            }
            else
            {
                kanjoKamoku = Util.ToString(dtKanjoKamoku.Rows[0]["KAMOKU_NM"]);
            }

            return kanjoKamoku;
        }

        /// <summary>
        /// 補助科目名を取得
        /// </summary>
        /// <param name="kanjoKamoku">勘定科目コード</param>
        /// <param name="hojoKamoku">補助科目コード</param>
        /// <param name="kaikeiNendo">会計年度</param>
        /// <param name="shishoCd">支所コード</param>
        /// <returns>補助科目名</returns>
        public string GetHojoKamokuNm(int kanjoKamoku, int hojoKamoku, int kaikeiNendo, int shishoCd)
        {
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();
            string hojoKamokuNm = "";

            if (hojoKamoku > 0)
            {
                sql.Append(" SELECT");
                sql.Append(" HOJO_KAMOKU_NM");
                sql.Append(" FROM");
                sql.Append(" VI_ZM_HOJO_KAMOKU");
                sql.Append(" WHERE");
                sql.Append(" KAISHA_CD = @KAISHA_CD AND");
                sql.Append(" (SHISHO_CD = @SHISHO_CD or SHISHO_CD is null) AND");
                sql.Append(" KAIKEI_NENDO = @KAIKEI_NENDO AND");
                sql.Append(" KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD");
                sql.Append(" AND HOJO_KAMOKU_CD = @HOJO_KAMOKU_CD");
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
                dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
                //dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
                dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, kaikeiNendo);
                dpc.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, kanjoKamoku);
                dpc.SetParam("@HOJO_KAMOKU_CD", SqlDbType.Decimal, 10, hojoKamoku);

                DataTable dtHojoKamoku = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);
                if (dtHojoKamoku.Rows.Count != 0)
                {
                    hojoKamokuNm = Util.ToString(dtHojoKamoku.Rows[0]["HOJO_KAMOKU_NM"]);
                }
            }

            return hojoKamokuNm;
        }

        ///// <summary>
        ///// 工事名を取得
        ///// </summary>
        ///// <param name="kojiCd">工事コード</param>
        ///// <returns>工事名</returns>
        //public string GetKojiNm(int kojiCd)
        //{
        //    DbParamCollection dpc = new DbParamCollection();
        //    StringBuilder sql = new StringBuilder();
        //    string kojiNm;
        //    sql.Append(" SELECT");
        //    sql.Append(" KOJI_NM");
        //    sql.Append(" FROM");
        //    sql.Append(" TB_ZM_KOJI");
        //    sql.Append(" WHERE");
        //    sql.Append(" KAISHA_CD = @KAISHA_CD AND");
        //    sql.Append(" KOJI_CD = " + kojiCd);
        //    dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);

        //    DataTable dtKoji = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

        //    kojiNm = Util.ToString(dtKoji.Rows[0]["KOJI_NM"]);

        //    return kojiNm;
        //}

        /// <summary>
        /// 勘定科目を指定した表示データを取得
        /// </summary>
        /// <param name="condition">条件画面の入力値</param>
        /// <param name="kanjoKamoku">勘定科目データ</param>
        /// <param name="shishoCd">支所コード</param>
        /// <returns>勘定科目を指定した表示データ</returns>
        public DataTable GetTaishakuData(Hashtable condition, string kanjoKamoku, int shishoCd)
        {
            // SetSubject2

            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();

            int shiwakeShori = Util.ToInt(condition["ShiwakeShurui"]);
            int shohizeiShoriHandan = Util.ToInt(condition["ShohizeiShoriHandan"]);

            /* 高速化（forループでSQL実行しなくて済むようにする）で削除
            sql.Append(" SELECT");
            sql.Append(" DENPYO_BANGO,");
            sql.Append(" GYO_BANGO,");
            sql.Append(" TAISHAKU_KUBUN,");
            sql.Append(" DENPYO_DATE,");
            sql.Append(" KANJO_KAMOKU_CD,");
            sql.Append(" KANJO_KAMOKU_NM,");
            sql.Append(" HOJO_KAMOKU_CD,");
            sql.Append(" AITE_KANJO_KAMOKU_CD,");
            sql.Append(" AITE_HOJO_KAMOKU_CD,");
            sql.Append(" HOJO_SHIYO_KUBUN,");
            sql.Append(" AITE_KOJI_CD,");
            sql.Append(" BUMON_CD,");
            sql.Append(" BUMON_NM,");
            sql.Append(" KOJI_CD,");
            sql.Append(" KOJI_NM,");
            sql.Append(" TEKIYO_CD,");
            sql.Append(" TEKIYO,");
            sql.Append(" SHOHYO_BANGO,");
            sql.Append(" ZEIKOMI_KINGAKU AS ZEIKOMI_KINGAKU,");
            sql.Append(" ZEINUKI_KINGAKU AS ZEINUKI_KINGAKU,");
            sql.Append(" SHOHIZEI_KINGAKU AS SHOHIZEI_KINGAKU");
            sql.Append(" FROM");
            sql.Append(" VI_ZM_SHIWAKE_MEISAI_AITE");
            sql.Append(" WHERE");
            sql.Append(" KAISHA_CD = @KAISHA_CD AND");
            sql.Append(" BUMON_CD BETWEEN @BUMON_FR AND @BUMON_TO AND");
            sql.Append(" KANJO_KAMOKU_CD = @KANJO_KAMOKU AND");
            sql.Append(" KOJI_CD BETWEEN @KOJI_FR AND @KOJI_TO AND");
            sql.Append(" DENPYO_DATE BETWEEN @DATE_FR AND @DATE_TO ");
            if (shohizeiShoriHandan == 1)
            {
                sql.Append(" AND MEISAI_KUBUN <= 0 AND");
                sql.Append(" DENPYO_KUBUN = 1");
            }
            if (shiwakeShori != 9)
            {
                sql.Append(" AND KESSAN_KUBUN = " + shiwakeShori);
            }
            sql.Append(" ORDER BY");
            sql.Append(" DENPYO_DATE, DENPYO_BANGO, GYO_BANGO");
            */
            sql.Append(" SELECT"); 
            sql.Append("   A2.SHISHO_CD,");
            sql.Append("   A2.DENPYO_BANGO,");
            sql.Append("   A2.GYO_BANGO,");
            sql.Append("   A2.TAISHAKU_KUBUN,");
            sql.Append("   A2.DENPYO_DATE,");
            sql.Append("   A2.KANJO_KAMOKU_CD,");
            sql.Append("   A2.KANJO_KAMOKU_NM,");
            sql.Append("   A2.HOJO_KAMOKU_CD,");
            sql.Append("   A2.HOJO_SHIYO_KUBUN,");
            //sql.Append("   ISNULL(G.KOJI_CD, 0) AS AITE_KOJI_CD,");
            sql.Append("   ISNULL(G.KANJO_KAMOKU_CD, 0) AS AITE_KANJO_KAMOKU_CD,");
            sql.Append("   ISNULL(G.HOJO_KAMOKU_CD, 0) AS AITE_HOJO_KAMOKU_CD,");
            sql.Append("   A2.BUMON_CD,");
            sql.Append("   A2.BUMON_NM,");
            //sql.Append("   A2.KOJI_CD,");
            //sql.Append("   A2.KOJI_NM,");
            sql.Append("   A2.TEKIYO_CD,");
            sql.Append("   A2.TEKIYO,");
            sql.Append("   A2.SHOHYO_BANGO,");
            sql.Append("   A2.ZEIKOMI_KINGAKU AS ZEIKOMI_KINGAKU,");
            sql.Append("   A2.ZEINUKI_KINGAKU AS ZEINUKI_KINGAKU,");
            sql.Append("   A2.SHOHIZEI_KINGAKU AS SHOHIZEI_KINGAKU,");
            sql.Append("   A2.ZEI_RITSU,");
            sql.Append("   A2.KAIKEI_NENDO,");
            //sql.Append("   ISNULL(H.KOJI_NM, '') AS AITE_KOJI_NM, ");
            sql.Append("   ( ");
            sql.Append("     SELECT ");
            sql.Append("       ISNULL(HOJO_KAMOKU_NM, '') ");                 // 元ネタ：GetHojoKamokuNm() ※JOINすると遅くなる
            sql.Append("     FROM ");
            sql.Append("       VI_ZM_HOJO_KAMOKU ");
            sql.Append("     WHERE ");
            sql.Append("       KANJO_KAMOKU_CD = G.KANJO_KAMOKU_CD ");        // "G.なんとか"は間違いではなくて、元々そうなっていた。
            sql.Append("       AND HOJO_KAMOKU_CD = G.HOJO_KAMOKU_CD ");
            sql.Append("       AND KAIKEI_NENDO = G.KAIKEI_NENDO ");
            sql.Append("       AND (SHISHO_CD = G.SHISHO_CD or SHISHO_CD is null) ");
            sql.Append("       AND KAISHA_CD = G.KAISHA_CD ");
            sql.Append("   ) AS AITE_HOJO_KAMOKU_NM, ");
            sql.Append("   ISNULL(I.KANJO_KAMOKU_NM, '') AS AITE_KANJO_KAMOKU_NM ");
            sql.Append(" FROM ");
            sql.Append(" ( ");
            sql.Append("     SELECT ");                                       // 元ネタ：VI_ZM_SHIWAKE_MEISAI_AITE
            sql.Append("       A.KAISHA_CD");
            sql.Append("       , A.KAIKEI_NENDO");
            sql.Append("       , A.SHISHO_CD");
            sql.Append("       , A.DENPYO_BANGO");
            sql.Append("       , A.GYO_BANGO");
            sql.Append("       , A.TAISHAKU_KUBUN");
            sql.Append("       , A.DENPYO_DATE");
            sql.Append("       , A.KANJO_KAMOKU_CD");
            sql.Append("       , C.KANJO_KAMOKU_NM");
            sql.Append("       , A.HOJO_KAMOKU_CD");
            sql.Append("       , D.HOJO_KAMOKU_NM");
            sql.Append("       , C.HOJO_SHIYO_KUBUN");
            sql.Append("       , A.BUMON_CD");
            sql.Append("       , E.BUMON_NM");
            //sql.Append("       , A.KOJI_CD");
            //sql.Append("       , F.KOJI_NM");
            sql.Append("       , A.TEKIYO");
            sql.Append("       , A.TEKIYO_CD");
            sql.Append("       , B.SHOHYO_BANGO");
            sql.Append("       , A.ZEIKOMI_KINGAKU");
            sql.Append("       , A.ZEINUKI_KINGAKU");
            sql.Append("       , A.SHOHIZEI_KINGAKU");
            sql.Append("       , A.ZEI_RITSU");
            sql.Append("       , ( ");
            sql.Append("         CASE ");
            sql.Append("           WHEN A.TAISHAKU_KUBUN = 1 ");
            sql.Append("           THEN 2 ");
            sql.Append("           ELSE 1 ");
            sql.Append("           END ");
            sql.Append("       ) AS REVERSE_TAISHAKU_KUBUN ");
            sql.Append("       , 0 AS MEISAI_KUBUN ");
            sql.Append("     FROM ");
            sql.Append("       TB_ZM_SHIWAKE_MEISAI AS A ");
            sql.Append("       LEFT OUTER JOIN TB_ZM_SHIWAKE_DENPYO AS B ");
            sql.Append("         ON ( ");
            sql.Append("           A.KAISHA_CD = B.KAISHA_CD "); 
            sql.Append("           AND A.SHISHO_CD = B.SHISHO_CD ");
            sql.Append("           AND A.KAIKEI_NENDO = B.KAIKEI_NENDO ");
            sql.Append("           AND A.DENPYO_BANGO = B.DENPYO_BANGO ");
            sql.Append("         ) ");
            sql.Append("       LEFT OUTER JOIN TB_ZM_KANJO_KAMOKU AS C ");
            sql.Append("         ON ( ");
            sql.Append("           A.KAISHA_CD = C.KAISHA_CD ");
            sql.Append("           AND A.KAIKEI_NENDO = C.KAIKEI_NENDO ");
            sql.Append("           AND A.KANJO_KAMOKU_CD = C.KANJO_KAMOKU_CD");
            sql.Append("         ) ");
            sql.Append("       LEFT OUTER JOIN TB_ZM_HOJO_KAMOKU AS D ");
            sql.Append("         ON ( ");
            sql.Append("           A.KAISHA_CD = D.KAISHA_CD "); 
            sql.Append("           AND A.SHISHO_CD = D.SHISHO_CD ");
            sql.Append("           AND A.KAIKEI_NENDO = D.KAIKEI_NENDO ");
            sql.Append("           AND A.KANJO_KAMOKU_CD = D.KANJO_KAMOKU_CD ");
            sql.Append("           AND A.HOJO_KAMOKU_CD = D.HOJO_KAMOKU_CD ");
            sql.Append("         ) ");
            sql.Append("       LEFT OUTER JOIN TB_CM_BUMON AS E ");
            sql.Append("         ON ( ");
            sql.Append("           A.KAISHA_CD = E.KAISHA_CD ");
            sql.Append("           AND A.BUMON_CD = E.BUMON_CD ");
            sql.Append("         ) ");
            //sql.Append("       LEFT OUTER JOIN TB_ZM_KOJI AS F ");
            //sql.Append("         ON ( ");
            //sql.Append("           A.KAISHA_CD = F.KAISHA_CD ");
            //sql.Append("           AND A.KAIKEI_NENDO = F.KAIKEI_NENDO ");
            //sql.Append("           AND A.KOJI_CD = F.KOJI_CD ");
            //sql.Append("         ) ");
            sql.Append("     WHERE ");
            sql.Append("       A.KAISHA_CD = @KAISHA_CD ");
            sql.Append("       AND A.SHISHO_CD = @SHISHO_CD ");
            //if (shohizeiShoriHandan == 1)
            //{
            sql.Append("       AND A.DENPYO_KUBUN = 1");
            //}
            //else
            //{
            //    sql.Append("       AND A.DENPYO_KUBUN >= 0 ");
            //}
            sql.Append("       AND A.KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD ");
            sql.Append("       AND A.HOJO_KAMOKU_CD >= 0 ");
            if (shohizeiShoriHandan == 1)
            {
                sql.Append("       AND A.MEISAI_KUBUN <= 0 ");
            }
            else
            {
                sql.Append("       AND A.MEISAI_KUBUN >= 0 ");
            }
            sql.Append("       AND A.DENPYO_DATE BETWEEN @DATE_FR AND @DATE_TO ");
            sql.Append("       AND A.BUMON_CD BETWEEN @BUMON_FR AND @BUMON_TO ");
            //sql.Append("       AND A.KOJI_CD BETWEEN @KOJI_FR AND @KOJI_TO ");
            if (shiwakeShori != 9)
            {
                sql.Append(" AND A.KESSAN_KUBUN = " + shiwakeShori);
            }
            sql.Append(" ) AS A2 ");
            sql.Append("  LEFT OUTER JOIN TB_ZM_SHIWAKE_MEISAI AS G ");       // 元ネタ：GetAinteKamokuCd()
            sql.Append("    ON ( ");
            sql.Append("      A2.KAISHA_CD = G.KAISHA_CD ");
            sql.Append("      AND A2.SHISHO_CD = G.SHISHO_CD ");
            sql.Append("      AND A2.DENPYO_BANGO = G.DENPYO_BANGO ");
            sql.Append("      AND A2.GYO_BANGO = G.GYO_BANGO ");
            sql.Append("      AND A2.REVERSE_TAISHAKU_KUBUN = G.TAISHAKU_KUBUN ");
            sql.Append("      AND A2.MEISAI_KUBUN = G.MEISAI_KUBUN ");
            sql.Append("      AND A2.KAIKEI_NENDO = G.KAIKEI_NENDO ");
            sql.Append("      AND A2.DENPYO_DATE = G.DENPYO_DATE ");
            sql.Append("    ) ");
            //sql.Append("  LEFT OUTER JOIN TB_ZM_KOJI AS H ");                // 元ネタ：GetKojiNm() ※相手用
            //sql.Append("    ON ( ");
            //sql.Append("      G.KAISHA_CD = H.KAISHA_CD ");
            //sql.Append("      AND G.KOJI_CD = H.KOJI_CD ");
            //sql.Append("      AND G.KAIKEI_NENDO = H.KAIKEI_NENDO ");
            //sql.Append("    ) ");
            sql.Append("  LEFT OUTER JOIN TB_ZM_KANJO_KAMOKU AS I ");       // 元ネタ：GetKanjoKamokuNm(,)
            sql.Append("    ON ( G.KAISHA_CD = I.KAISHA_CD ");
            sql.Append("     AND G.KANJO_KAMOKU_CD = I.KANJO_KAMOKU_CD ");
            sql.Append("     AND G.KAIKEI_NENDO = I.KAIKEI_NENDO ");
            sql.Append("    ) ");
            sql.Append(" ORDER BY");
            sql.Append(" A2.SHISHO_CD, A2.DENPYO_DATE, A2.DENPYO_BANGO, A2.GYO_BANGO");

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            DateTime dtFr = (DateTime)condition["DtFr"];
            dpc.SetParam("@DATE_FR", SqlDbType.VarChar, 23, dtFr.Date.ToString("yyyy/MM/dd 00:00:00.000"));
            DateTime dtTo = (DateTime)condition["DtTo"];
            dpc.SetParam("@DATE_TO", SqlDbType.VarChar, 23, dtTo.Date.ToString("yyyy/MM/dd 23:59:59.000"));
            dpc.SetParam("@KANJO_KAMOKU_CD", SqlDbType.VarChar, 6, kanjoKamoku);
            dpc.SetParam("@BUMON_FR", SqlDbType.VarChar, 4, Util.ToString(condition["BumonFr"]));
            dpc.SetParam("@BUMON_TO", SqlDbType.VarChar, 4, Util.ToString(condition["BumonTo"]));
            //dpc.SetParam("@KOJI_FR", SqlDbType.VarChar, 4, Util.ToString(condition["KojiFr"]));
            //dpc.SetParam("@KOJI_TO", SqlDbType.VarChar, 4, Util.ToString(condition["KojiTo"]));

            DataTable dsTaishakuData = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dsTaishakuData;
        }
        #endregion
    }
}
