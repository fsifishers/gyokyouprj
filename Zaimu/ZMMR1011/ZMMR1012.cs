﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Reflection;

using systembase.table;

using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.zm.zmmr1011
{
    /// <summary>
    /// 総勘定元帳(ZMMR1012)
    /// </summary>
    public partial class ZMMR1012 : BasePgForm
    {
        #region 構造体
        /// <summary>
        /// 合計情報
        /// </summary>
        private struct Summary
        {
            public decimal kariAmount;
            public decimal kariZei;
            public decimal kashiAmount;
            public decimal kashiZei;
            public decimal zan;

            /// <summary>
            /// 金額をクリア
            /// </summary>
            public void Clear()
            {
                kariAmount = 0;
                kariZei = 0;
                kashiAmount = 0;
                kashiZei = 0;
                zan = 0;
            }
        }
        #endregion

        #region private変数
        /// <summary>
        /// ZMMR1011(条件画面)のオブジェクト(設定内容の取得のため)
        /// </summary>
        ZMMR1011 _pForm;

        /// <summary>
        /// 現在表示しているDataTableのインデックス
        /// </summary>
        int _curDataIdx;

        /// <summary>
        /// 表示データが0件の時のエラーメッセージ表示用フラグ
        /// </summary>
        int _flag;

        /// <summary>
        /// 表示データ最初の勘定科目(ファンクションキーの可視情報取得のため)
        /// </summary>
        string _firstKamokuCd;

        /// <summary>
        /// 表示データ最後の勘定科目(ファンクションキーの可視情報取得のため)
        /// </summary>
        string _lastKamokuCd;

        /// <summary>
        /// 表示データ最初の支所コード(ファンクションキーの可視情報取得のため)
        /// </summary>
        int _firstShishoCd;

        /// <summary>
        /// 表示データ最後の支所コード(ファンクションキーの可視情報取得のため)
        /// </summary>
        int _lastShishoCd;

        /// <summary>
        /// 振替伝票からの戻り
        /// </summary>
        DialogResult _denpyoDialogResult;
        #endregion

        #region プロパティ
        private DataTable _dsTaishakuData = new DataTable();
        /// <summary>
        /// 貸借データ
        /// </summary>
        /// <remarks>
        /// 貸借を仕訳したデータ(1伝票あたり1DataTable)
        /// </remarks>
        public DataTable TaishakuData
        {
            get
            {
                return this._dsTaishakuData;
            }
        }

        private DataTable _dsTmpKanjoKamokuIchiran = new DataTable();

        /// <summary>
        /// 仕訳対象データ
        /// </summary>
        public DataTable TmpKanjoKamokuIchiran
        {
            get
            {
                return this._dsTmpKanjoKamokuIchiran;
            }
        }
        /// <summary>
        /// 新規表示or再表示判断用変数
        /// </summary>
        private int _judgementFlg = new int();
        public int JudgementFlg
        {
            get
            {
                return this._judgementFlg;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ZMMR1012(ZMMR1011 frm)
        {
            this._pForm = frm;

            InitializeComponent();
            BindGotFocusEvent();

            // 新規表示に設定
            this._judgementFlg = 1;
        }
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // タイトル非表示
            this.lblTitle.Visible = false;

            this.ShowFButton = false;
            // 集計中メッセージ表示
            ZMMR1014 msgFrm = new ZMMR1014();
            msgFrm.Show();
            msgFrm.Refresh();

            Boolean viewFlg = true;
            DataTable tmpKanjoKamokuIchiran = this._pForm.SwkTgtData;

            // 仕訳対象データが0件の場合、処理を終了
            // 仕訳対象データが1件以上の場合、処理を実行
            if (tmpKanjoKamokuIchiran.Rows.Count != 0)
            {
                // [金額がｾﾞﾛの科目を印字」の設定を取得
                string inji = Util.ToString(this._pForm.Condition["Inji"]);

                // データを表示
                string kanjoKamoku = Util.ToString(tmpKanjoKamokuIchiran.Rows[this._curDataIdx]["KAMOKU_CD"]);
                // 支所コード
                int shishoCd = Util.ToInt(Util.ToString(tmpKanjoKamokuIchiran.Rows[this._curDataIdx]["SHISHO_CD"]));
                // 貸借データの作成
                ZMMR1011DA da = new ZMMR1011DA(this.UInfo, this.Dba, this.Config);
                //this._dsTaishakuData = da.GetTaishakuData(this._pForm.Condition, kanjoKamoku);
                this._dsTaishakuData = da.GetTaishakuData(this._pForm.Condition, kanjoKamoku, shishoCd);

                // 取得したデータが存在するかどうかを上から確認し、存在すれば表示
                if (this._dsTaishakuData.Rows.Count == 0 && inji == "no" && this._curDataIdx != this._pForm.SwkTgtData.Rows.Count - 1)
                {
                    this._curDataIdx++;
                    InitForm();
                    viewFlg = false;
                }
                else if (this._dsTaishakuData.Rows.Count == 0 && inji == "no" && this._curDataIdx == this._pForm.SwkTgtData.Rows.Count - 1)
                {
                    _flag = 1;
                }
                else
                {
                    _flag = 0;
                    this.ShowFButton = true;

                    // タイトル非表示
                    this.lblTitle.Visible = false;

                    // 勘定科目コードの初期表示
                    this.lblKanjoBango.Text = Util.ToString(tmpKanjoKamokuIchiran.Rows[this._curDataIdx]["KAMOKU_CD"]);
                    // 勘定科目名の初期表示
                    this.lblKanjoKamoku.Text = Util.ToString(tmpKanjoKamokuIchiran.Rows[this._curDataIdx]["KAMOKU_NM"]);

                    // 期間の初期表示
                    string tmpTerm = " ";
                    string[] aryJpDate = Util.ConvJpDate(Util.ToDate(this._pForm.Condition["DtFr"]), this.Dba);
                    tmpTerm += aryJpDate[5];
                    aryJpDate = Util.ConvJpDate(Util.ToDate(this._pForm.Condition["DtTo"]), this.Dba);
                    tmpTerm += " ～ " + aryJpDate[5];
                    this.lblJp.Text = tmpTerm;

                    // 【税込みor税抜き】の初期表示
                    this.lblZei.Text = Util.ToString(this._pForm.Condition["ShohizeiShori"]);

                    // 勘定科目の貸借区分を設定
                    int kanjoKamokuKubun = Util.ToInt(tmpKanjoKamokuIchiran.Rows[this._curDataIdx]["TAISHAKU_KUBUN"]);

                    // デザインを作成
                    //InitDetailArea();
                    if (this.JudgementFlg == 1)
                    {
                        InitDetailArea();
                    }
                    DispData(kanjoKamoku, this._dsTaishakuData, kanjoKamokuKubun);

                    // 最初のデータの勘定科目を取得
                    int i = 0;
                    _firstKamokuCd = Util.ToString(tmpKanjoKamokuIchiran.Rows[i]["KAMOKU_CD"]);
                    // 支所コード
                    _firstShishoCd = Util.ToInt(Util.ToString(tmpKanjoKamokuIchiran.Rows[i]["SHISHO_CD"]));
                    while (i < tmpKanjoKamokuIchiran.Rows.Count - 1)
                    {
                        string kamokuCd = Util.ToString(tmpKanjoKamokuIchiran.Rows[i]["KAMOKU_CD"]);
                        // 支所コード
                        shishoCd = Util.ToInt(Util.ToString(tmpKanjoKamokuIchiran.Rows[i]["SHISHO_CD"]));
                        // 貸借データの作成
                        //_dsTaishakuData = da.GetTaishakuData(this._pForm.Condition, kamokuCd);
                        _dsTaishakuData = da.GetTaishakuData(this._pForm.Condition, kamokuCd, shishoCd);

                        if (this._dsTaishakuData.Rows.Count != 0)
                        {
                            _firstKamokuCd = Util.ToString(this._dsTaishakuData.Rows[0]["KANJO_KAMOKU_CD"]);
                            _firstShishoCd = Util.ToInt(Util.ToString(this._dsTaishakuData.Rows[0]["SHISHO_CD"]));
                            break;
                        }
                        i++;
                    }

                    // 最終データの勘定科目を取得
                    int j = tmpKanjoKamokuIchiran.Rows.Count - 1;
                    _lastKamokuCd = Util.ToString(tmpKanjoKamokuIchiran.Rows[j]["KAMOKU_CD"]);
                    // 支所コード
                    _lastShishoCd = Util.ToInt(Util.ToString(tmpKanjoKamokuIchiran.Rows[j]["SHISHO_CD"]));
                    if (inji == "no")
                    {
                        while (j >= 0)
                        {
                            string kamokuCd = Util.ToString(tmpKanjoKamokuIchiran.Rows[j]["KAMOKU_CD"]);
                            // 支所コード
                            shishoCd = Util.ToInt(Util.ToString(tmpKanjoKamokuIchiran.Rows[j]["SHISHO_CD"]));
                            // 貸借データの作成
                            //this._dsTaishakuData = da.GetTaishakuData(this._pForm.Condition, kamokuCd);
                            this._dsTaishakuData = da.GetTaishakuData(this._pForm.Condition, kamokuCd, shishoCd);

                            if (this._dsTaishakuData.Rows.Count != 0)
                            {
                                _lastKamokuCd = Util.ToString(this._dsTaishakuData.Rows[0]["KANJO_KAMOKU_CD"]);
                                _lastShishoCd = Util.ToInt(Util.ToString(this._dsTaishakuData.Rows[0]["SHISHO_CD"]));
                                break;
                            }
                            j--;
                        }
                    }

                    // 前伝票・次伝票の使用可否制御
                    //ControlPrevNext(kanjoKamoku);
                    ControlPrevNext(kanjoKamoku, _firstShishoCd);
                }
            }

            // フォーカス設定
            this.mtbList.Focus();

            if (viewFlg)
            {
                //// ボタンの表示非表示を設定
                //this.btnF11.Location = this.btnF8.Location;
                //this.btnF10.Location = this.btnF7.Location;
                //this.btnF9.Location = this.btnF6.Location;
                //this.btnF8.Location = this.btnF5.Location;
                //this.btnF7.Location = this.btnF4.Location;

                this.btnF10.Location = this.btnF8.Location;
                this.btnF9.Location = this.btnF7.Location;
                this.btnF8.Location = this.btnF6.Location;
                this.btnF7.Location = this.btnF5.Location;
                this.btnF6.Location = this.btnF4.Location;
                this.btnF5.Location = this.btnF3.Location;
                this.btnF4.Location = this.btnF2.Location;
                this.btnEsc.Location = this.btnF1.Location;
                this.btnEsc.Visible = true;
                this.btnF1.Visible = false;
                this.btnF2.Visible = false;
                this.btnF3.Visible = false;
                this.btnF4.Visible = true;
                this.btnF5.Visible = true;
                this.btnF6.Visible = true;
                this.btnF7.Visible = true;
                this.btnF8.Visible = true;
                this.btnF9.Visible = true;
                this.btnF10.Visible = true;
                this.btnF11.Visible = false;
                this.btnF12.Visible = false;

                this.ShowFButton = true;
            }
            msgFrm.Close();
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            this.DialogResult = this._denpyoDialogResult;
            base.PressEsc();
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
            {
                // プレビュー処理
                ZMMR1011PR pr = new ZMMR1011PR(this.UInfo, this.Dba, this.Config, this.UnqId, this._pForm);
                pr.DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            if (Msg.ConfNmYesNo("印刷","実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                ZMMR1011PR pr = new ZMMR1011PR(this.UInfo, this.Dba, this.Config, this.UnqId, this._pForm);
                pr.DoPrint(false);
            }
        }

        /// <summary>
        /// F6キー押下時処理
        /// PDF出力
        /// </summary>
        public override void PressF6()
        {
            if (Msg.ConfNmYesNo("PDF出力", "実行しますか？") == DialogResult.Yes)
            {
                // プレビュー処理
                ZMMR1011PR pr = new ZMMR1011PR(this.UInfo, this.Dba, this.Config, this.UnqId, this._pForm);
                pr.DoPrint(true, true);
            }
        }

        /// <summary>
        /// F7キー押下時処理
        /// EXCEL出力
        /// </summary>
        public override void PressF7()
        {
            if (Msg.ConfNmYesNo("EXCEL出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                ZMMR1011PR pr = new ZMMR1011PR(this.UInfo, this.Dba, this.Config, this.UnqId, this._pForm);
                pr.DoPrint(false, false, true);
            }





        }

        /// <summary>
        /// F8キー押下時処理
        /// </summary>
        public override void PressF8()
        {
            if (!this.btnF8.Enabled) return;

            this._curDataIdx--;

            DataTable tmpKanjoKamokuIchiran = this._pForm.SwkTgtData;
            string kanjoKamoku = Util.ToString(tmpKanjoKamokuIchiran.Rows[this._curDataIdx]["KAMOKU_CD"]);
            // 支所コード
            int shishoCd = Util.ToInt(Util.ToString(tmpKanjoKamokuIchiran.Rows[this._curDataIdx]["SHISHO_CD"]));

            // [金額がｾﾞﾛの科目を印字」の設定を取得
            string inji = Util.ToString(this._pForm.Condition["Inji"]);

            // 貸借データの作成
            ZMMR1011DA da = new ZMMR1011DA(this.UInfo, this.Dba, this.Config);
            //this._dsTaishakuData = da.GetTaishakuData(this._pForm.Condition, kanjoKamoku);
            this._dsTaishakuData = da.GetTaishakuData(this._pForm.Condition, kanjoKamoku, shishoCd);
            if (this._dsTaishakuData.Rows.Count == 0 && inji == "no")
            {
                PressF8();
            }
            else
            {
                // 勘定科目コードの初期表示
                this.lblKanjoBango.Text = Util.ToString(tmpKanjoKamokuIchiran.Rows[this._curDataIdx]["KAMOKU_CD"]);
                // 勘定科目名の初期表示
                this.lblKanjoKamoku.Text = Util.ToString(tmpKanjoKamokuIchiran.Rows[this._curDataIdx]["KAMOKU_NM"]);
                // 勘定科目の貸借区分を設定
                int kanjoKamokuKubun = Util.ToInt(tmpKanjoKamokuIchiran.Rows[this._curDataIdx]["TAISHAKU_KUBUN"]);

                DispData(kanjoKamoku, this._dsTaishakuData, kanjoKamokuKubun);

                // 前伝票・次伝票の使用可否制御
                //ControlPrevNext(kanjoKamoku);
                ControlPrevNext(kanjoKamoku, shishoCd);

                return;
            }







        }

        /// <summary>
        /// F9キー押下時処理
        /// </summary>
        public override void PressF9()
        {
            if (!this.btnF9.Enabled) return;

            this._curDataIdx++;

            DataTable tmpKanjoKamokuIchiran = this._pForm.SwkTgtData;
            string kanjoKamoku = Util.ToString(tmpKanjoKamokuIchiran.Rows[this._curDataIdx]["KAMOKU_CD"]);
            // 支所コード
            int shishoCd = Util.ToInt(Util.ToString(tmpKanjoKamokuIchiran.Rows[this._curDataIdx]["SHISHO_CD"]));

            // [金額がｾﾞﾛの科目を印字」の設定を取得
            string inji = Util.ToString(this._pForm.Condition["Inji"]);

            // 貸借データの作成
            ZMMR1011DA da = new ZMMR1011DA(this.UInfo, this.Dba, this.Config);
            //this._dsTaishakuData = da.GetTaishakuData(this._pForm.Condition, kanjoKamoku);
            this._dsTaishakuData = da.GetTaishakuData(this._pForm.Condition, kanjoKamoku, shishoCd);
            if (this._dsTaishakuData.Rows.Count == 0 && inji == "no")
            {
                PressF9();
            }
            else
            {
                // 勘定科目コードの初期表示
                this.lblKanjoBango.Text = Util.ToString(tmpKanjoKamokuIchiran.Rows[this._curDataIdx]["KAMOKU_CD"]);
                // 勘定科目名の初期表示
                this.lblKanjoKamoku.Text = Util.ToString(tmpKanjoKamokuIchiran.Rows[this._curDataIdx]["KAMOKU_NM"]);
                // 勘定科目の貸借区分を設定
                int kanjoKamokuKubun = Util.ToInt(tmpKanjoKamokuIchiran.Rows[this._curDataIdx]["TAISHAKU_KUBUN"]);

                DispData(kanjoKamoku, this._dsTaishakuData, kanjoKamokuKubun);

                // 前伝票・次伝票の使用可否制御
                //ControlPrevNext(kanjoKamoku);
                ControlPrevNext(kanjoKamoku, shishoCd);

                return;
            }
        }

        /// <summary>
        /// F10キー押下時処理
        /// PDF出力
        /// </summary>
        public override void PressF10()
        {
            if (this.mtbList.FocusRecord == null) return;

            // 選択行の伝票番号を取得
            String denpyoBango = Util.ToString(this.mtbList.FocusRecord.Fields["伝票番号"].Value);
            // 伝票番号が存在する場合、振替伝票子画面を起動
            if (denpyoBango != "")
            {
                DataTable dt = this._pForm.SwkTgtData;
                // 支所コード
                string shishoCd = Util.ToString(dt.Rows[this._curDataIdx]["SHISHO_CD"]);

                // アセンブリのロード
                //Assembly asm = System.Reflection.Assembly.LoadFrom("ZAME1031.exe");
                Assembly asm = Assembly.LoadFrom("ZMDE1031.exe");
                // フォーム作成
                //Type t = asm.GetType("jp.co.fsi.zam.zame1031.ZAME1031");
                Type t = asm.GetType("jp.co.fsi.zm.zmde1031.ZMDE1031");
                if (t != null)
                {
                    Object obj = Activator.CreateInstance(t);
                    if (obj != null)
                    {
                        // タブの一部として埋め込む
                        BasePgForm frm = (BasePgForm)obj;
                        frm.InData = shishoCd + "," + denpyoBango;
                        frm.ShowDialog(this);

                        if (frm.DialogResult == DialogResult.OK)
                        {
                            // 再表示に設定
                            this._judgementFlg = 2;

                            // 以下でデータの取り直しが（仕訳で科目の追加等があった場合に繰り越しが出ない可能性も）
                            // 基本参照前提で今のままで保留
                            // 仕訳対象データを抽出
                            ZMMR1011DA da = new ZMMR1011DA(this.UInfo, this.Dba, this.Config);
                            this._pForm.SwkTgtData = da.GetSwkTgtData(this._pForm.Condition);

                            // 画面を再読み込み
                            InitForm();
                            //shown();
                            if (this._pForm.SwkTgtData.Rows.Count == 0 || _flag == 1)
                            {
                                Msg.Info("該当するデータがありません。");
                                this.DialogResult = DialogResult.Cancel;
                                this.Close();
                                return;
                            }

                            this._denpyoDialogResult = DialogResult.OK;
                        }
                        else
                        {
                            this._denpyoDialogResult = DialogResult.Cancel;
                        }
                    }
                }
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 明細のレコードプロバイダを返す
        ///   ※これの戻り値はクラス変数にしたほうがいいかも。
        /// </summary>
        /// <returns></returns>
        private UTable.CRecordProvider getDeteilRP()
        {
            UTable.CRecordProvider rp = new UTable.CRecordProvider();
            CLayoutBuilder lb = new CLayoutBuilder(CLayoutBuilder.EOrientation.ROW);
            UTable.CFieldDesc fd;

            fd = rp.AddField("日付", new CTextFieldProvider("日　付"), lb.Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            fd = rp.AddField("伝票番号", new CTextFieldProvider("伝票No."), lb.Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);

            lb.Break();

            fd = rp.AddField("相手科目", new CTextFieldProvider("相 手 科 目"), lb.Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.LEFT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            fd = rp.AddField("補助科目", new CTextFieldProvider("補 助 科 目"), lb.Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.LEFT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);

            lb.Break();

            lb.Ascend();

            fd = rp.AddField("部門", new CTextFieldProvider("部　　門"), lb.Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.LEFT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            fd = rp.AddField("証憑番号", new CTextFieldProvider("証憑番号"), lb.Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.LEFT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            //fd = rp.AddField("工事", new CTextFieldProvider("工　　事"), lb.Next());
            //fd.Setting.HorizontalAlignment = UTable.EHAlign.LEFT;
            fd = rp.AddField("工事", new CTextFieldProvider("　　　　　　　　　　率"), lb.Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            lb.Descend();

            fd = rp.AddField("摘要", new CTextFieldProvider("摘　　要"), lb.Next(1, 3));
            fd.Setting.HorizontalAlignment = UTable.EHAlign.LEFT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);

            lb.Break(3);

            fd = rp.AddField("借方消費税", new CTextFieldProvider(""), lb.Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            fd = rp.AddField("借方金額", new CTextFieldProvider("借　方"), lb.Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);

            lb.Break();

            fd = rp.AddField("貸方消費税", new CTextFieldProvider(""), lb.Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            fd = rp.AddField("貸方金額", new CTextFieldProvider("貸　方"), lb.Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);

            lb.Break();

            fd = rp.AddField("", new CTextFieldProvider(""), lb.Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);
            fd = rp.AddField("残高", new CTextFieldProvider("残　高"), lb.Next());
            fd.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
            fd.Setting.Editable = UTable.EAllow.DISABLE;
            fd.Setting.Font = new Font("ＭＳ ゴシック", 9F, FontStyle.Regular);

            return rp;
        }

        /// <summary>
        /// 明細部の初期化
        /// </summary>
        private void InitDetailArea()
        {
            UTable.CRecordProvider rp = this.getDeteilRP();
            CLayoutBuilder lb = new CLayoutBuilder(CLayoutBuilder.EOrientation.ROW);

            this.mtbList.Content.SetRecordProvider(rp);
            this.mtbList.CreateCaption(UTable.EHAlign.MIDDLE);
            //this.mtbList.Cols.SetSize(75, 120, 100, 60, 100, 80, 80, 80);
            this.mtbList.Cols.SetSize(75, 133, 100, 60, 100, 80, 80, 80);
            this.mtbList.Setting.ContentBackColor = SystemColors.AppWorkspace;

            // フォーカス移動時のヘッダーカラー
            this.mtbList.HeaderContent.Setting.FocusCaptionBackColor = Color.Transparent;
            // 列幅の変更不可
            this.mtbList.Setting.UserColResizable = UTable.EAllow.DISABLE;
        }

        /// <summary>
        /// 仕訳データを表示
        /// </summary>
        private void DispData(string kanjoKamoku, DataTable _dsTaishakuData, int kanjoKamokuKubun)
        {
            Summary sumInfo = new Summary();
            Summary sumMonth = new Summary();

            // まず今表示されている情報はクリア
            this.mtbList.Content.ClearRecord();
            // 勘定科目毎の合計値を初期化
            sumInfo.Clear();

            // 貸借データの作成
            ZMMR1011DA da = new ZMMR1011DA(this.UInfo, this.Dba, this.Config);

            int count = _dsTaishakuData.Rows.Count;
            int kanjoKamokuCd;
            string kanjoKamokuNm = "";
            string bumon = "";
            //string koji = "";
            string tekiyo = "";
            int kingaku;
            int zei;
            string hidukeDate = "";
            DataTable aiteKamokuCd;
            int kaikeiNendo = 0; // 会計年度
            string hojoKamoku = ""; // 補助科目
            int aiteHojoKamokuCd = 0; // 相手先補助科目コード
            // 税率
            string taxRate = "";

            // 明細行描画をブロック（後回しに）する
            using (UTable.CRenderBlock renderBlock = this.mtbList.Content.Table.RenderBlock())
            {
                UTable.CRecord rec;

                // 処理順序を「行追加後に値設定」 → 「値設定後に行追加」に変えるため、レコードプロバイダを取得する
                UTable.CRecordProvider rp = this.getDeteilRP();

                #region 繰越の表示
                //if (Util.ToInt(this._pForm.SwkTgtData.Rows[this._curDataIdx]["KURIKOSHI_ZANDAKA"]) != 0)
                //{
                //    rec = this.mtbList.Content.CreateRecord(rp);

                //    rec.Setting.BackColor = Color.Aquamarine;
                //    rec.Setting.AlterBackColor = Color.Aquamarine;
                //    rec.Fields["日付"].Value = "";
                //    rec.Fields["伝票番号"].Value = "";
                //    rec.Fields["相手科目"].Value = "";
                //    rec.Fields["補助科目"].Value = "";
                //    rec.Fields["部門"].Value = "";
                //    rec.Fields["証憑番号"].Value = "";
                //    //rec.Fields["工事"].Value = "";
                //    rec.Fields["摘要"].Value = "繰越";
                //    rec.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
                //    rec.Fields["借方金額"].Value = "";
                //    rec.Fields["貸方金額"].Value = "";
                //    sumInfo.zan += Util.ToInt(this._pForm.SwkTgtData.Rows[this._curDataIdx]["KURIKOSHI_ZANDAKA"]);
                //    rec.Fields["残高"].Value = Util.FormatNum(sumInfo.zan);

                //    // 明細表示行データ設定（描画後回し）
                //    this.mtbList.Content.AddRecord(rec);
                //}
                rec = this.mtbList.Content.CreateRecord(rp);

                rec.Setting.BackColor = Color.Aquamarine;
                rec.Setting.AlterBackColor = Color.Aquamarine;
                rec.Fields["日付"].Value = "";
                rec.Fields["伝票番号"].Value = "";
                rec.Fields["相手科目"].Value = "";
                rec.Fields["補助科目"].Value = "";
                rec.Fields["部門"].Value = "";
                rec.Fields["証憑番号"].Value = "";
                //rec.Fields["工事"].Value = "";
                rec.Fields["摘要"].Value = "繰越";
                rec.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
                rec.Fields["借方金額"].Value = "";
                rec.Fields["貸方金額"].Value = "";
                if (Util.ToInt(this._pForm.SwkTgtData.Rows[this._curDataIdx]["KURIKOSHI_ZANDAKA"]) != 0)
                {
                    sumInfo.zan += Util.ToInt(this._pForm.SwkTgtData.Rows[this._curDataIdx]["KURIKOSHI_ZANDAKA"]);
                }
                rec.Fields["残高"].Value = Util.FormatNum(sumInfo.zan);

                // 明細表示行データ設定（描画後回し）
                this.mtbList.Content.AddRecord(rec);
                #endregion

                // 明細の背景色設定
                this.mtbList.Setting.BackColor = Color.White;
                this.mtbList.Setting.AlterBackColor = Color.White;
                this.mtbList.Setting.FocusBackColor = Color.Transparent;
                this.mtbList.Setting.FocusRecordBackColor = Color.Transparent;

                // コントロールブレイク用の日付（月）を初期化する
                DateTime denpyoDate = DateTime.Today;
                string[] warekiDate = Util.ConvJpDate(denpyoDate, this.Dba);
                string beforeDate = warekiDate[3];
                for (int i = 0; i < count; i++)
                {
                    // コントロールブレイク用の日付（月）を初期化する
                    denpyoDate = (DateTime)_dsTaishakuData.Rows[i]["DENPYO_DATE"];
                    warekiDate = Util.ConvJpDate(denpyoDate, this.Dba);
                    beforeDate = warekiDate[3];

                    #region 月合計表示
                    if (beforeDate != warekiDate[3])
                    {
                        rec = this.mtbList.Content.CreateRecord(rp);

                        rec.Setting.BackColor = Color.Aquamarine;
                        rec.Setting.AlterBackColor = Color.Aquamarine;
                        rec.Fields["日付"].Value = "";
                        rec.Fields["伝票番号"].Value = "";
                        rec.Fields["相手科目"].Value = "";
                        rec.Fields["補助科目"].Value = "";
                        rec.Fields["部門"].Value = "";
                        rec.Fields["証憑番号"].Value = "";
                        //rec.Fields["工事"].Value = "";
                        rec.Fields["摘要"].Value = beforeDate + " 月 計";
                        rec.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
                        if (sumMonth.kariZei != 0)
                        {
                            rec.Fields["借方消費税"].Value = Util.FormatNum(sumMonth.kariZei);
                        }
                        if (sumMonth.kariAmount != 0)
                        {
                            rec.Fields["借方金額"].Value = Util.FormatNum(sumMonth.kariAmount);
                        }
                        if (sumMonth.kashiZei != 0)
                        {
                            rec.Fields["貸方消費税"].Value = Util.FormatNum(sumMonth.kashiZei);
                        }
                        if (sumMonth.kashiAmount != 0)
                        {
                            rec.Fields["貸方金額"].Value = Util.FormatNum(sumMonth.kashiAmount);
                        }
                        sumMonth.Clear();

                        // 明細表示行データ設定（描画後回し）
                        this.mtbList.Content.AddRecord(rec);
                    }
                    #endregion

                    beforeDate = warekiDate[3];
                    if (warekiDate[3].Length == 1)
                    {
                        warekiDate[3] = " " + warekiDate[3];
                    }
                    if (warekiDate[4].Length == 1)
                    {
                        warekiDate[4] = " " + warekiDate[4];
                    }

                    #region データの表示
                    rec = this.mtbList.Content.CreateRecord(rp);
                    hidukeDate = warekiDate[2] + "/" + warekiDate[3] + "/" + warekiDate[4];
                    rec.Fields["日付"].Value = hidukeDate;
                    if (Util.ToInt(this._pForm.Condition["check01"]) == 0)
                    {
                        rec.Fields["伝票番号"].Value = "";
                    }
                    else
                    {
                        rec.Fields["伝票番号"].Value = Util.ToString(_dsTaishakuData.Rows[i]["DENPYO_BANGO"]);
                    }

                    // 会計年度
                    kaikeiNendo = Util.ToInt(this._dsTaishakuData.Rows[i]["KAIKEI_NENDO"]);

                    // 勘定科目が取得できなければ、諸口と表示
                    kanjoKamokuNm = "　　諸　　口";

                    // 税率
                    taxRate = Util.FormatNum(Util.ToDecimal(this._dsTaishakuData.Rows[i]["ZEI_RITSU"].ToString()));
                    if (taxRate == "0")
                        taxRate = "";

                    // 相手勘定科目コードを取得
                    kanjoKamokuCd = Util.ToInt(this._dsTaishakuData.Rows[i]["AITE_KANJO_KAMOKU_CD"]);

                    // 相手補助科目コード
                    aiteHojoKamokuCd = Util.ToInt(this._dsTaishakuData.Rows[i]["AITE_HOJO_KAMOKU_CD"]);

                    // 相手勘定科目コードを取得できた場合
                    if (kanjoKamokuCd > 0)
                    {
                        // 相手勘定科目コードから相手勘定科目名を取得
                        kanjoKamokuNm = " " + kanjoKamokuCd + " " + Util.ToString(this._dsTaishakuData.Rows[i]["AITE_KANJO_KAMOKU_NM"]);
                        aiteKamokuCd = new DataTable();
                    }
                    else
                    {
                        // 相手勘定科目コードを取得できない場合
                        // 取得した伝票番号、行番号、伝票日付、貸借区分から相手勘定科目コードを取得
                        // 支所コード
                        int shishoCd = Util.ToInt(Util.ToString(this._dsTaishakuData.Rows[i]["SHISHO_CD"]));
                        //aiteKamokuCd = da.GetKanjoKamokuNm(Util.ToInt(this._dsTaishakuData.Rows[i]["DENPYO_BANGO"]),
                        //Util.ToString(this._dsTaishakuData.Rows[i]["DENPYO_DATE"]), Util.ToInt(this._dsTaishakuData.Rows[i]["GYO_BANGO"]),
                        //Util.ToInt(this._dsTaishakuData.Rows[i]["TAISHAKU_KUBUN"]), kaikeiNendo);
                        aiteKamokuCd = da.GetKanjoKamokuNm(Util.ToInt(this._dsTaishakuData.Rows[i]["DENPYO_BANGO"]),
                        Util.ToString(this._dsTaishakuData.Rows[i]["DENPYO_DATE"]), Util.ToInt(this._dsTaishakuData.Rows[i]["GYO_BANGO"]),
                        Util.ToInt(this._dsTaishakuData.Rows[i]["TAISHAKU_KUBUN"]), kaikeiNendo, shishoCd);
                        if (aiteKamokuCd.Rows.Count > 0)
                        {
                            kanjoKamokuCd = Util.ToInt(aiteKamokuCd.Rows[0]["KANJO_KAMOKU_CD"]); // 相手先勘定科目コード
                            aiteHojoKamokuCd = Util.ToInt(aiteKamokuCd.Rows[0]["HOJO_KAMOKU_CD"]);  // 相手先補助科目コード
                        }

                        // 相手勘定科目コード
                        if (kanjoKamokuCd > 0)
                        {
                            // 相手勘定科目コードから相手勘定科目名を取得
                            //kanjoKamokuNm = da.GetKanjoKamokuNm(kanjoKamokuCd, 0, kaikeiNendo);
                            kanjoKamokuNm = da.GetKanjoKamokuNm(kanjoKamokuCd, 0, kaikeiNendo, shishoCd);
                        }
                    }
                    rec.Fields["相手科目"].Value = kanjoKamokuNm;

                    // 補助科目
                    if (Util.ToInt(this._pForm.Condition["check02"]) == 0)
                    {
                        hojoKamoku = "";
                    }
                    else
                    {
                        hojoKamoku = this._dsTaishakuData.Rows[i]["AITE_HOJO_KAMOKU_NM"].ToString();
                        int shishoCd = Util.ToInt(Util.ToString(this._dsTaishakuData.Rows[i]["SHISHO_CD"]));
                        if (hojoKamoku == "" && aiteKamokuCd.Rows.Count > 0)
                        {
                            //  VI_ZM_HOJO_KAMOKUから取得
                            //hojoKamoku = da.GetHojoKamokuNm(kanjoKamokuCd, aiteHojoKamokuCd, kaikeiNendo);
                            hojoKamoku = da.GetHojoKamokuNm(kanjoKamokuCd, aiteHojoKamokuCd, kaikeiNendo, shishoCd);
                        }
                    }
                    rec.Fields["補助科目"].Value = hojoKamoku;

                    // 部門
                    if (Util.ToInt(this._pForm.Condition["check03"]) == 0)
                    {
                        rec.Fields["部門"].Value = "";
                    }
                    else
                    {
                        if (Util.ToInt(_dsTaishakuData.Rows[i]["BUMON_CD"]) != 0)
                        {
                            bumon = _dsTaishakuData.Rows[i]["BUMON_CD"] + " " + _dsTaishakuData.Rows[i]["BUMON_NM"];
                        }
                        rec.Fields["部門"].Value = bumon;
                    }

                    // 証憑番号
                    if (Util.ToInt(this._pForm.Condition["check04"]) == 0)
                    {
                        rec.Fields["証憑番号"].Value = "";
                    }
                    else
                    {
                        rec.Fields["証憑番号"].Value = _dsTaishakuData.Rows[i]["SHOHYO_BANGO"];
                    }

                    //// 工事
                    //if (Util.ToInt(_dsTaishakuData.Rows[i]["AITE_KOJI_CD"]) > 0)
                    //{
                    //    //koji = Util.ToInt(_dsTaishakuData.Rows[i]["AITE_KOJI_CD"]) + " " + da.GetKojiNm(Util.ToInt(_dsTaishakuData.Rows[i]["AITE_KOJI_CD"]));
                    //    koji = Util.ToString(_dsTaishakuData.Rows[i]["AITE_KOJI_CD"]) + " " + _dsTaishakuData.Rows[i]["AITE_KOJI_NM"];
                    //}
                    //else if (Util.ToInt(_dsTaishakuData.Rows[i]["KOJI_CD"]) > 0)
                    //{
                    //    koji = Util.ToInt(_dsTaishakuData.Rows[i]["KOJI_CD"]) + " " + Util.ToInt(_dsTaishakuData.Rows[i]["KOJI_NM"]);
                    //}
                    //rec.Fields["工事"].Value = koji;
                    // 税率
                    rec.Fields["工事"].Value = taxRate;

                    // 適用
                    if (_dsTaishakuData.Rows[i]["TEKIYO"] != null)
                    {
                        tekiyo = Util.ToString(_dsTaishakuData.Rows[i]["TEKIYO"]);
                    }
                    rec.Fields["摘要"].Value = tekiyo;


                    // 消費税
                    if (Util.ToInt(this._pForm.Condition["ShohizeiShoriHandan"]) == 1)
                    {
                        kingaku = Util.ToInt(_dsTaishakuData.Rows[i]["ZEIKOMI_KINGAKU"]);
                        zei = Util.ToInt(_dsTaishakuData.Rows[i]["SHOHIZEI_KINGAKU"]);
                    }
                    else
                    {
                        kingaku = Util.ToInt(_dsTaishakuData.Rows[i]["ZEINUKI_KINGAKU"]);
                        zei = Util.ToInt(_dsTaishakuData.Rows[i]["SHOHIZEI_KINGAKU"]);
                    }

                    if (Util.ToInt(_dsTaishakuData.Rows[i]["TAISHAKU_KUBUN"]) == 1)
                    {
                        if (zei != 0)
                        {
                            if (Util.ToInt(this._pForm.Condition["check05"]) == 0)
                            {
                                rec.Fields["借方消費税"].Value = "";
                            }
                            else
                            {
                                rec.Fields["借方消費税"].Value = Util.FormatNum(zei);
                                sumMonth.kariZei += zei;
                                sumInfo.kariZei += zei;
                            }
                        }
                        rec.Fields["借方金額"].Value = Util.FormatNum(kingaku);
                        sumMonth.kariAmount += kingaku;
                        sumInfo.kariAmount += kingaku;
                        if (kanjoKamokuKubun == 1)
                        {
                            sumInfo.zan += kingaku;
                        }
                        else if (kanjoKamokuKubun == 2)
                        {
                            sumInfo.zan -= kingaku;
                        }
                    }
                    else
                    {
                        if (zei != 0)
                        {
                            if (Util.ToInt(this._pForm.Condition["check05"]) == 0)
                            {
                                rec.Fields["貸方消費税"].Value = "";
                            }
                            else
                            {
                                rec.Fields["貸方消費税"].Value = Util.FormatNum(zei);
                                sumMonth.kashiZei += zei;
                                sumInfo.kashiZei += zei;
                            }
                        }
                        rec.Fields["貸方金額"].Value = Util.FormatNum(kingaku);
                        sumMonth.kashiAmount += kingaku;
                        sumInfo.kashiAmount += kingaku;
                        if (kanjoKamokuKubun == 1)
                        {
                            sumInfo.zan -= kingaku;
                        }
                        else
                        {
                            sumInfo.zan += kingaku;
                        }
                    }

                    // 残高
                    if (sumInfo.zan == 0)
                    {
                        rec.Fields["残高"].Value = "";
                    }
                    else
                    {
                        rec.Fields["残高"].Value = Util.FormatNum(sumInfo.zan);
                    }

                    // 明細表示行データ設定（描画後回し）
                    this.mtbList.Content.AddRecord(rec);
                    #endregion

                    #region 月合計表示
                    if (i == count - 1)
                    {
                        rec = this.mtbList.Content.CreateRecord(rp);

                        rec.Setting.BackColor = Color.Aquamarine;
                        rec.Setting.AlterBackColor = Color.Aquamarine;
                        rec.Fields["日付"].Value = "";
                        rec.Fields["伝票番号"].Value = "";
                        rec.Fields["相手科目"].Value = "";
                        rec.Fields["補助科目"].Value = "";
                        rec.Fields["部門"].Value = "";
                        rec.Fields["証憑番号"].Value = "";
                        //rec.Fields["工事"].Value = "";
                        rec.Fields["摘要"].Value = beforeDate + " 月 計";
                        rec.Setting.HorizontalAlignment = UTable.EHAlign.RIGHT;
                        if (sumMonth.kariZei != 0)
                        {
                            rec.Fields["借方消費税"].Value = Util.FormatNum(sumMonth.kariZei);
                        }
                        if (sumMonth.kariAmount != 0)
                        {
                            rec.Fields["借方金額"].Value = Util.FormatNum(sumMonth.kariAmount);
                        }
                        if (sumMonth.kashiZei != 0)
                        {
                            rec.Fields["貸方消費税"].Value = Util.FormatNum(sumMonth.kashiZei);
                        }
                        if (sumMonth.kashiAmount != 0)
                        {
                            rec.Fields["貸方金額"].Value = Util.FormatNum(sumMonth.kashiAmount);
                        }

                        // 明細表示行データ設定（描画後回し）
                        this.mtbList.Content.AddRecord(rec);

                        sumMonth.Clear();

                    }
                    #endregion
                }

            }// 明細行描画をブロック ここまで

            #region 合計の表示
            // 繰越残高を含まない場合
            if (Util.ToInt(this._pForm.Condition["check07"]) == 0)
            {
                if (sumInfo.kariZei != 0)
                {
                    this.lblKariZeiGokei.Text = Util.FormatNum(sumInfo.kariZei);
                }
                else
                {
                    this.lblKariZeiGokei.Text = "";
                }
                if (sumInfo.kariAmount != 0)
                {
                    this.lblKariGokei.Text = Util.FormatNum(sumInfo.kariAmount);
                }
                else
                {
                    this.lblKariGokei.Text = "";
                }
                if (sumInfo.kashiZei != 0)
                {
                    this.lblKashiZeiGokei.Text = Util.FormatNum(sumInfo.kashiZei);
                }
                else
                {
                    this.lblKashiZeiGokei.Text = "";
                }
                if (sumInfo.kashiAmount != 0)
                {
                    this.lblKashiGokei.Text = Util.FormatNum(sumInfo.kashiAmount);
                }
                else
                {
                    this.lblKashiGokei.Text = "";
                }
            }
            // 繰越残高を含む場合
            else
            {
                // 勘定科目の貸借区分を取得
                int taishakuKubun = Util.ToInt(this._pForm.SwkTgtData.Rows[this._curDataIdx]["TAISHAKU_KUBUN"]);
                // 借方情報を表示
                if (sumInfo.kariZei != 0)
                {
                    this.lblKariZeiGokei.Text = Util.FormatNum(sumInfo.kariZei);
                }
                else
                {
                    this.lblKariZeiGokei.Text = "";
                }
                if (taishakuKubun == 1 && (Util.ToInt(this._pForm.SwkTgtData.Rows[this._curDataIdx]["KURIKOSHI_ZANDAKA"]) + sumInfo.kariAmount) != 0)
                {

                    this.lblKariGokei.Text = Util.FormatNum(
                        Util.ToInt(this._pForm.SwkTgtData.Rows[this._curDataIdx]["KURIKOSHI_ZANDAKA"]) + sumInfo.kariAmount);
                }
                else if (sumInfo.kariAmount != 0)
                {
                    this.lblKariGokei.Text = Util.FormatNum(sumInfo.kariAmount);
                }
                else
                {
                    this.lblKariGokei.Text = "";
                }
                // 貸方情報を表示
                if (sumInfo.kashiZei != 0)
                {
                    this.lblKashiZeiGokei.Text = Util.FormatNum(sumInfo.kashiZei);
                }
                else
                {
                    this.lblKashiZeiGokei.Text = "";
                }
                if (taishakuKubun == 2 && (Util.ToInt(this._pForm.SwkTgtData.Rows[this._curDataIdx]["KURIKOSHI_ZANDAKA"]) + sumInfo.kashiAmount) != 0)
                {
                    this.lblKashiGokei.Text = Util.FormatNum(
                        Util.ToInt(this._pForm.SwkTgtData.Rows[this._curDataIdx]["KURIKOSHI_ZANDAKA"]) + sumInfo.kashiAmount);
                }
                else if (sumInfo.kashiAmount != 0)
                {
                    this.lblKashiGokei.Text = Util.FormatNum(sumInfo.kashiAmount);
                }
                else
                {
                    this.lblKashiGokei.Text = "";
                }
            }
            // 残高を表示
            if (sumInfo.zan != 0)
            {
                this.lblZan.Text = Util.FormatNum(sumInfo.zan);
            }
            else
            {
                this.lblZan.Text = "";
            }
            #endregion

            mtbList.Focus();
        }

        /// <summary>
        /// 前伝票・次伝票を使用可否を判断する
        /// </summary>
        private void ControlPrevNext(string kanjoKamoku, int shishoCd)
        {
            if (this._pForm.SwkTgtData.Rows.Count > 1)
            {
                //if (kanjoKamoku == _firstKamokuCd && kanjoKamoku == _lastKamokuCd)
                if (kanjoKamoku == _firstKamokuCd && kanjoKamoku == _lastKamokuCd &&
                    shishoCd == _firstShishoCd && shishoCd == _lastShishoCd)
                {
                    // 前伝票・次伝票ともに存在しない
                    this.btnF8.Enabled = false;
                    this.btnF9.Enabled = false;
                }
                //else if (kanjoKamoku == _firstKamokuCd)
                else if (kanjoKamoku == _firstKamokuCd && shishoCd == _firstShishoCd)
                {
                    // 前伝票はない
                    this.btnF8.Enabled = false;
                    this.btnF9.Enabled = true;
                }
                //else if (kanjoKamoku == _lastKamokuCd)
                else if (kanjoKamoku == _lastKamokuCd && shishoCd == _lastShishoCd)
                {
                    // 次伝票はない
                    this.btnF8.Enabled = true;
                    this.btnF9.Enabled = false;
                }
                else
                {
                    // 前伝票・次伝票ともに存在する
                    this.btnF8.Enabled = true;
                    this.btnF9.Enabled = true;
                }
            }
            else
            {
                this.btnF8.Enabled = false;
                this.btnF9.Enabled = false;
            }
        }

        /// <summary>
        /// 表示データの有無での表示切替
        /// </summary>
        private void shown()
        {
            if (TmpKanjoKamokuIchiran.Rows.Count == 0 || _flag == 1)
            {
                // デザインを作成(新規表示の場合)
                if (this.JudgementFlg == 1)
                {
                    InitDetailArea();
                }
                // タイトル非表示
                this.lblTitle.Visible = false;

                Msg.Info("該当するデータがありません。");
                this.DialogResult = DialogResult.Cancel;
                this.Close();
                return;
            }
            // 自分自身をアクティブに設定
            this.Activate();
        }
        #endregion

        #region イベント

        private void ZMMR1012_Shown(object sender, EventArgs e)
        {
            if (this._pForm.SwkTgtData.Rows.Count == 0 || _flag == 1)
            {
                InitDetailArea();
                // タイトル非表示
                this.lblTitle.Visible = false;

                Msg.Info("該当するデータがありません。");
                this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
                this.Close();
                return;
            }
            // 自分自身をアクティブに設定
            this.Activate();
        }
        #endregion
    }
}
