﻿namespace jp.co.fsi.zm.zmmr1011
{
    partial class ZMMR1013
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlInjiKomoku = new jp.co.fsi.common.FsiPanel();
            this.chk07 = new System.Windows.Forms.CheckBox();
            this.chk06 = new System.Windows.Forms.CheckBox();
            this.chk05 = new System.Windows.Forms.CheckBox();
            this.chk04 = new System.Windows.Forms.CheckBox();
            this.chk03 = new System.Windows.Forms.CheckBox();
            this.chk02 = new System.Windows.Forms.CheckBox();
            this.chk01 = new System.Windows.Forms.CheckBox();
            this.lblInjiKomoku = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.pnlInjiKomoku.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(0, 23);
            this.lblTitle.Text = "総勘定元帳 [印字設定]";
            this.lblTitle.Visible = false;
            // 
            // btnEsc
            // 
            this.btnEsc.Location = new System.Drawing.Point(3, 49);
            // 
            // btnF1
            // 
            this.btnF1.Visible = false;
            // 
            // btnF2
            // 
            this.btnF2.Visible = false;
            // 
            // btnF3
            // 
            this.btnF3.Visible = false;
            // 
            // btnF4
            // 
            this.btnF4.Visible = false;
            // 
            // btnF5
            // 
            this.btnF5.Visible = false;
            // 
            // btnF7
            // 
            this.btnF7.Visible = false;
            // 
            // btnF6
            // 
            this.btnF6.Location = new System.Drawing.Point(67, 49);
            // 
            // btnF8
            // 
            this.btnF8.Visible = false;
            // 
            // btnF9
            // 
            this.btnF9.Visible = false;
            // 
            // btnF12
            // 
            this.btnF12.Visible = false;
            // 
            // btnF11
            // 
            this.btnF11.Visible = false;
            // 
            // btnF10
            // 
            this.btnF10.Visible = false;
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(5, 176);
            this.pnlDebug.Size = new System.Drawing.Size(190, 100);
            // 
            // pnlInjiKomoku
            // 
            this.pnlInjiKomoku.BackColor = System.Drawing.Color.Silver;
            this.pnlInjiKomoku.Controls.Add(this.chk07);
            this.pnlInjiKomoku.Controls.Add(this.chk06);
            this.pnlInjiKomoku.Controls.Add(this.chk05);
            this.pnlInjiKomoku.Controls.Add(this.chk04);
            this.pnlInjiKomoku.Controls.Add(this.chk03);
            this.pnlInjiKomoku.Controls.Add(this.chk02);
            this.pnlInjiKomoku.Controls.Add(this.chk01);
            this.pnlInjiKomoku.Controls.Add(this.lblInjiKomoku);
            this.pnlInjiKomoku.Location = new System.Drawing.Point(12, 13);
            this.pnlInjiKomoku.Name = "pnlInjiKomoku";
            this.pnlInjiKomoku.Size = new System.Drawing.Size(174, 205);
            this.pnlInjiKomoku.TabIndex = 903;
            // 
            // chk07
            // 
            this.chk07.AutoSize = true;
            this.chk07.ForeColor = System.Drawing.Color.Red;
            this.chk07.Location = new System.Drawing.Point(16, 181);
            this.chk07.Name = "chk07";
            this.chk07.Size = new System.Drawing.Size(156, 16);
            this.chk07.TabIndex = 910;
            this.chk07.Text = "累計に繰越残高を含む。";
            this.chk07.UseVisualStyleBackColor = true;
            // 
            // chk06
            // 
            this.chk06.AutoSize = true;
            this.chk06.Location = new System.Drawing.Point(16, 155);
            this.chk06.Name = "chk06";
            this.chk06.Size = new System.Drawing.Size(144, 16);
            this.chk06.TabIndex = 909;
            this.chk06.Text = "出力日付を印字する。";
            this.chk06.UseVisualStyleBackColor = true;
            // 
            // chk05
            // 
            this.chk05.AutoSize = true;
            this.chk05.Location = new System.Drawing.Point(16, 130);
            this.chk05.Name = "chk05";
            this.chk05.Size = new System.Drawing.Size(132, 16);
            this.chk05.TabIndex = 908;
            this.chk05.Text = "消費税を印字する。";
            this.chk05.UseVisualStyleBackColor = true;
            // 
            // chk04
            // 
            this.chk04.AutoSize = true;
            this.chk04.Location = new System.Drawing.Point(16, 105);
            this.chk04.Name = "chk04";
            this.chk04.Size = new System.Drawing.Size(144, 16);
            this.chk04.TabIndex = 907;
            this.chk04.Text = "証憑番号を印字する。";
            this.chk04.UseVisualStyleBackColor = true;
            // 
            // chk03
            // 
            this.chk03.AutoSize = true;
            this.chk03.Location = new System.Drawing.Point(16, 80);
            this.chk03.Name = "chk03";
            this.chk03.Size = new System.Drawing.Size(120, 16);
            this.chk03.TabIndex = 906;
            this.chk03.Text = "部門を印字する。";
            this.chk03.UseVisualStyleBackColor = true;
            // 
            // chk02
            // 
            this.chk02.AutoSize = true;
            this.chk02.Location = new System.Drawing.Point(16, 55);
            this.chk02.Name = "chk02";
            this.chk02.Size = new System.Drawing.Size(144, 16);
            this.chk02.TabIndex = 905;
            this.chk02.Text = "補助科目を印字する。";
            this.chk02.UseVisualStyleBackColor = true;
            // 
            // chk01
            // 
            this.chk01.AutoSize = true;
            this.chk01.Location = new System.Drawing.Point(16, 30);
            this.chk01.Name = "chk01";
            this.chk01.Size = new System.Drawing.Size(144, 16);
            this.chk01.TabIndex = 904;
            this.chk01.Text = "伝票番号を印字する。";
            this.chk01.UseVisualStyleBackColor = true;
            // 
            // lblInjiKomoku
            // 
            this.lblInjiKomoku.AutoSize = true;
            this.lblInjiKomoku.Location = new System.Drawing.Point(5, 7);
            this.lblInjiKomoku.Name = "lblInjiKomoku";
            this.lblInjiKomoku.Size = new System.Drawing.Size(65, 12);
            this.lblInjiKomoku.TabIndex = 904;
            this.lblInjiKomoku.Text = "■印字項目";
            // 
            // ZMMR1013
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(198, 279);
            this.Controls.Add(this.pnlInjiKomoku);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "ZMMR1013";
            this.ShowFButton = true;
            this.Text = "";
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlInjiKomoku, 0);
            this.pnlDebug.ResumeLayout(false);
            this.pnlInjiKomoku.ResumeLayout(false);
            this.pnlInjiKomoku.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private jp.co.fsi.common.FsiPanel pnlInjiKomoku;
        private System.Windows.Forms.Label lblInjiKomoku;
        private System.Windows.Forms.CheckBox chk01;
        private System.Windows.Forms.CheckBox chk02;
        private System.Windows.Forms.CheckBox chk03;
        private System.Windows.Forms.CheckBox chk04;
        private System.Windows.Forms.CheckBox chk05;
        private System.Windows.Forms.CheckBox chk06;
        private System.Windows.Forms.CheckBox chk07;





    }
}