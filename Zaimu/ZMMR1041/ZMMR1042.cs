﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.zm.zmmr1041
{
    /// <summary>
    /// 補助科目集計表(ZMMR1042)
    /// </summary>
    public partial class ZMMR1042 : BasePgForm
    {
        #region private変数
        /// <summary>
        /// ZAMR2041(条件画面)のオブジェクト(設定内容の取得のため)
        /// </summary>
        ZMMR1041 _pForm;

        /// <summary>
        /// 現在表示しているDataTableのインデックス
        /// </summary>
        int _curDataIdx;

        /// <summary>
        /// 表示データ最初の支所コード(ファンクションキーの可視情報取得のため)
        /// </summary>
        int _firstShishoCd;

        /// <summary>
        /// 表示データ最後の支所コード(ファンクションキーの可視情報取得のため)
        /// </summary>
        int _lastShishoCd;

        List<int> _shishoList;
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ZMMR1042(ZMMR1041 frm)
        {
            this._pForm = frm;

            InitializeComponent();
            BindGotFocusEvent();
        }
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // タイトル非表示
            this.lblTitle.Visible = false;

            DataTable tmpHojoDataIchiran = this._pForm.HojoData;

            if (tmpHojoDataIchiran.Rows.Count > 0)
            {
                // 総合計金額用変数
                Decimal zenZanKei = 0;
                Decimal karikataKei = 0;
                Decimal kashikataKei = 0;
                Decimal zandakaKei = 0;

                //this.dgvList.DataSource = tmpHojoDataIchiran;

                //// ユーザーによるソートを禁止させる
                //foreach (DataGridViewColumn c in this.dgvList.Columns)
                //    c.SortMode = DataGridViewColumnSortMode.NotSortable;

                // 複数支所混在時の処理
                {
                    _shishoList = new List<int>();
                    if (Util.ToInt(Util.ToString(this._pForm.Condition["ShishoCode"])) != 0)
                    {
                        this.btnF8.Visible = false;
                        this.btnF9.Visible = false;
                        this.btnF8.Enabled = false;
                        this.btnF9.Enabled = false;
                    }
                    else
                    {
                        DataView dv = new DataView(tmpHojoDataIchiran.Copy());
                        DataTable dt = dv.ToTable(true, new string[] { "SHISHO_CD" });
                        foreach (DataRow dr in dt.Rows)
                        {
                            _shishoList.Add(Util.ToInt(dr["SHISHO_CD"]));
                        }
                    }

                    // 最初のデータの支所コードを取得
                    int i = 0;
                    // 支所コード
                    _firstShishoCd = Util.ToInt(Util.ToString(tmpHojoDataIchiran.Rows[i]["SHISHO_CD"]));
                    // 最終データの支所コードを取得
                    int j = tmpHojoDataIchiran.Rows.Count - 1;
                    // 支所コード
                    _lastShishoCd = Util.ToInt(Util.ToString(tmpHojoDataIchiran.Rows[j]["SHISHO_CD"]));
                }
                if (Util.ToInt(Util.ToString(this._pForm.Condition["ShishoCode"])) != 0)
                {
                    this.dgvList.DataSource = tmpHojoDataIchiran;
                }
                else
                {
                    DataView dv = new DataView(tmpHojoDataIchiran);
                    dv.RowFilter = "SHISHO_CD = " + _firstShishoCd.ToString();
                    this.dgvList.DataSource = dv.ToTable();
                }

                // ユーザーによるソートを禁止させる
                foreach (DataGridViewColumn c in this.dgvList.Columns)
                    c.SortMode = DataGridViewColumnSortMode.NotSortable;

                // フォントを設定する
                //this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F, FontStyle.Bold);
                this.dgvList.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F, FontStyle.Regular);
                this.dgvList.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                this.dgvList.DefaultCellStyle.Font = new Font("ＭＳ ゴシック", 10F);

                // 列幅を設定する
                this.dgvList.Columns[0].Width = 60;
                this.dgvList.Columns[1].Width = 238;
                this.dgvList.Columns[2].Width = 120;
                this.dgvList.Columns[3].Width = 120;
                this.dgvList.Columns[4].Width = 120;
                this.dgvList.Columns[5].Width = 120;
                this.dgvList.Columns[6].Width = 0;
                this.dgvList.Columns[6].Visible = false;
                this.dgvList.Columns[7].Width = 0;
                this.dgvList.Columns[7].Visible = false;

                // 書式を設定する
                this.dgvList.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                this.dgvList.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                this.dgvList.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                this.dgvList.Columns[2].DefaultCellStyle.Format = "#,0";
                this.dgvList.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                this.dgvList.Columns[3].DefaultCellStyle.Format = "#,0";
                this.dgvList.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                this.dgvList.Columns[4].DefaultCellStyle.Format = "#,0";
                this.dgvList.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                this.dgvList.Columns[5].DefaultCellStyle.Format = "#,0";

                this.dgvList.Columns[2].HeaderText = "前日残高";

                // 不要な列を削除
                this.dgvList.Columns[6].Visible = false;

                // 最初は選択状態にしない
                this.dgvList[0, 0].Selected = false;

                for (int i = 0; i < dgvList.Rows.Count; i++)
                {
                    if (Util.ToString(this.dgvList[0, i].Value) == "")
                    {
                        // 合計行のバックカラーを変更する
                        this.dgvList.Rows[i].DefaultCellStyle.BackColor = Color.FromArgb(200, 255, 200);
                    }
                    else
                    {
                        // 総合計金額を計算
                        zenZanKei += Util.ToDecimal(this.dgvList[2, i].Value);
                        karikataKei += Util.ToDecimal(this.dgvList[3, i].Value);
                        kashikataKei += Util.ToDecimal(this.dgvList[4, i].Value);
                        zandakaKei += Util.ToDecimal(this.dgvList[5, i].Value);
                    }

                    // 金額がマイナスの時は赤色に変更
                    if (Util.ToDecimal(this.dgvList[2, i].Value) < 0)
                    {
                        this.dgvList[2, i].Style.ForeColor = Color.Red;
                    }
                    if (Util.ToDecimal(this.dgvList[3, i].Value) < 0)
                    {
                        this.dgvList[3, i].Style.ForeColor = Color.Red;
                    }
                    if (Util.ToDecimal(this.dgvList[4, i].Value) < 0)
                    {
                        this.dgvList[4, i].Style.ForeColor = Color.Red;
                    }
                    if (Util.ToDecimal(this.dgvList[5, i].Value) < 0)
                    {
                        this.dgvList[5, i].Style.ForeColor = Color.Red;
                    }
                }
                // 総合計金額を表示
                if (zenZanKei != 0)
                {
                    this.lblZenZanKei.Text = Util.FormatNum(zenZanKei);
                }
                this.lblZenZanKei.ForeColor = Color.Black;
                if (zenZanKei < 0)
                {
                    this.lblZenZanKei.ForeColor = Color.Red;
                }
                if (karikataKei != 0)
                {
                    this.lblKarikataKei.Text = Util.FormatNum(karikataKei);
                }
                this.lblKarikataKei.ForeColor = Color.Black;
                if (karikataKei < 0)
                {
                    this.lblKarikataKei.ForeColor = Color.Red;
                }
                if (kashikataKei != 0)
                {
                    this.lblKashikataKei.Text = Util.FormatNum(kashikataKei);
                }
                this.lblKashikataKei.ForeColor = Color.Black;
                if (kashikataKei < 0)
                {
                    this.lblKashikataKei.ForeColor = Color.Red;
                }
                if (zandakaKei != 0)
                {
                    this.lblZandakaKei.Text = Util.FormatNum(zandakaKei);
                }
                this.lblZandakaKei.ForeColor = Color.Black;
                if (zandakaKei < 0)
                {
                    this.lblZandakaKei.ForeColor = Color.Red;
                }

                // タイトル非表示
                this.lblTitle.Visible = false;

                // 期間の初期表示
                string tmpTerm = " ";
                string[] aryJpDate = Util.ConvJpDate(Util.ToDate(this._pForm.Condition["DtFr"]), this.Dba);
                tmpTerm += aryJpDate[5];
                aryJpDate = Util.ConvJpDate(Util.ToDate(this._pForm.Condition["DtTo"]), this.Dba);
                tmpTerm += " ～ " + aryJpDate[5];
                this.lblJp.Text = tmpTerm;

                // 【税込みor税抜き】の初期表示
                this.lblZei.Text = Util.ToString(this._pForm.Condition["ShohizeiShori"]);

                if (Util.ToInt(Util.ToString(this._pForm.Condition["ShishoCode"])) == 0)
                {
                    // 前支所・次支所の使用可否制御
                    ControlPrevNext(_firstShishoCd);
                }
            }
            else
            {
                Msg.Error("該当データがありません。");
                this.Close();
            }

            //// ボタンの表示非表示を設定
            ////this.btnF8.Location = this.btnF6.Location;
            ////this.btnF7.Location = this.btnF5.Location;
            ////this.btnF6.Location = this.btnF4.Location;
            //this.btnF10.Location = this.btnF7.Location;
            //this.btnF9.Location = this.btnF6.Location;
            //this.btnF7.Location = this.btnF4.Location;
            //this.btnF8.Location = this.btnF5.Location;

            //this.btnF5.Location = this.btnF3.Location;
            //this.btnF4.Location = this.btnF2.Location;
            //this.btnEsc.Location = this.btnF1.Location;
            //this.btnF1.Visible = false;
            //this.btnF2.Visible = false;
            //this.btnF3.Visible = false;
            //this.btnF6.Visible = false;
            //if (Util.ToInt(Util.ToString(this._pForm.Condition["ShishoCode"])) != 0)
            //{
            //    this.btnF7.Visible = false;
            //    this.btnF8.Visible = false;
            //    this.btnF10.Location = this.btnF8.Location;
            //    this.btnF9.Location = this.btnF7.Location;
            //}
            //else
            //{
            //    this.btnF7.Visible = true;
            //    this.btnF8.Visible = true;
            //}
            ////this.btnF9.Visible = false;
            ////this.btnF10.Visible = false;
            //this.btnF9.Visible = true;
            //this.btnF10.Visible = true;
            //this.btnF11.Visible = false;
            //this.btnF12.Visible = false;



            this.btnF10.Location = this.btnF8.Location;
            this.btnF9.Location = this.btnF7.Location;
            this.btnF8.Location = this.btnF6.Location;
            this.btnF7.Location = this.btnF5.Location;
            this.btnF6.Location = this.btnF4.Location;
            this.btnF5.Location = this.btnF3.Location;
            this.btnF4.Location = this.btnF2.Location;
            this.btnEsc.Location = this.btnF1.Location;
            this.btnEsc.Visible = true;
            this.btnF1.Visible = false;
            this.btnF2.Visible = false;
            this.btnF3.Visible = false;
            this.btnF4.Visible = true;
            this.btnF5.Visible = true;
            this.btnF6.Visible = true;
            this.btnF7.Visible = true;
            this.btnF8.Visible = true;
            this.btnF9.Visible = true;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;
            if (Util.ToInt(Util.ToString(this._pForm.Condition["ShishoCode"])) != 0)
            {
                this.btnF8.Visible = false;
                this.btnF9.Visible = false;
            }
            else
            {
                this.btnF8.Visible = true;
                this.btnF9.Visible = true;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
            {
                // プレビュー処理
                ZMMR1041PR pr = new ZMMR1041PR(this.UInfo, this.Dba, this.UnqId, this._pForm);
                pr.DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            if (Msg.ConfNmYesNo("印刷","実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                ZMMR1041PR pr = new ZMMR1041PR(this.UInfo, this.Dba, this.UnqId, this._pForm);
                pr.DoPrint(false);
            }
        }

        /// <summary>
        /// F6キー押下時処理
        /// PDF出力
        /// </summary>
        public override void PressF6()
        {
            if (Msg.ConfNmYesNo("PDF出力", "実行しますか？") == DialogResult.Yes)
            {
                // プレビュー処理
                ZMMR1041PR pr = new ZMMR1041PR(this.UInfo, this.Dba, this.UnqId, this._pForm);
                pr.DoPrint(true, true);
            }
        }

        /// <summary>
        /// F7キー押下時処理
        /// EXCEL出力
        /// </summary>
        public override void PressF7()
        {
            if (Msg.ConfNmYesNo("EXCEL出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                ZMMR1041PR pr = new ZMMR1041PR(this.UInfo, this.Dba, this.UnqId, this._pForm);
                pr.DoPrint(false, false, true);
            }
        }

        /// <summary>
        /// F8キー押下時処理
        /// </summary>
        public override void PressF8()
        {
            if (!this.btnF8.Enabled) return;

            this._curDataIdx--;
            // 支所コード
            int shishoCd = this._shishoList[this._curDataIdx];

            DataTable tmpHojoDataIchiran = this._pForm.HojoData;

            // [金額がｾﾞﾛの科目を印字」の設定を取得
            string inji = Util.ToString(this._pForm.Condition["Inji"]);

            if (tmpHojoDataIchiran.Rows.Count == 0 && inji == "no")
            {
                PressF8();
            }
            else
            {
                DataView dv = new DataView(tmpHojoDataIchiran);
                dv.RowFilter = "SHISHO_CD = " + shishoCd.ToString();
                this.dgvList.DataSource = dv.ToTable();

                // 前伝票・次伝票の使用可否制御
                ControlPrevNext(shishoCd);

                // 合計
                CalcTotal();

                return;
            }
        }

        /// <summary>
        /// F9キー押下時処理
        /// </summary>
        public override void PressF9()
        {
            if (!this.btnF9.Enabled) return;

            this._curDataIdx++;
            // 支所コード
            int shishoCd = this._shishoList[this._curDataIdx];

            DataTable tmpHojoDataIchiran = this._pForm.HojoData;

            // [金額がｾﾞﾛの科目を印字」の設定を取得
            string inji = Util.ToString(this._pForm.Condition["Inji"]);

            if (tmpHojoDataIchiran.Rows.Count == 0 && inji == "no")
            {
                PressF9();
            }
            else
            {
                DataView dv = new DataView(tmpHojoDataIchiran);
                dv.RowFilter = "SHISHO_CD = " + shishoCd.ToString();
                this.dgvList.DataSource = dv.ToTable();

                // 前伝票・次伝票の使用可否制御
                ControlPrevNext(shishoCd);

                // 合計
                CalcTotal();

                return;
            }
        }
        #endregion

        #region privateメソッド

        /// <summary>
        /// 前支所・次支所を使用可否を判断する
        /// </summary>
        private void ControlPrevNext(int shishoCd)
        {
            if (this._pForm.HojoData.Rows.Count > 1)
            {
                if (shishoCd == _firstShishoCd && shishoCd == _lastShishoCd)
                {
                    // 前伝票・次伝票ともに存在しない
                    this.btnF8.Enabled = false;
                    this.btnF9.Enabled = false;
                }
                else if (shishoCd == _firstShishoCd)
                {
                    // 前伝票はない
                    this.btnF8.Enabled = false;
                    this.btnF9.Enabled = true;
                }
                else if (shishoCd == _lastShishoCd)
                {
                    // 次伝票はない
                    this.btnF8.Enabled = true;
                    this.btnF9.Enabled = false;
                }
                else
                {
                    // 前伝票・次伝票ともに存在する
                    this.btnF8.Enabled = true;
                    this.btnF9.Enabled = true;
                }
            }
            else
            {
                this.btnF8.Enabled = false;
                this.btnF9.Enabled = false;
            }
        }

        /// <summary>
        /// 合計エリアの計算
        /// </summary>
        private void CalcTotal()
        {
            // 総合計金額用変数
            Decimal zenZanKei = 0;
            Decimal karikataKei = 0;
            Decimal kashikataKei = 0;
            Decimal zandakaKei = 0;

            for (int i = 0; i < dgvList.Rows.Count; i++)
            {
                if (Util.ToString(this.dgvList[0, i].Value) == "")
                {
                    // 合計行のバックカラーを変更する
                    this.dgvList.Rows[i].DefaultCellStyle.BackColor = Color.FromArgb(200, 255, 200);
                }
                else
                {
                    // 総合計金額を計算
                    zenZanKei += Util.ToDecimal(this.dgvList[2, i].Value);
                    karikataKei += Util.ToDecimal(this.dgvList[3, i].Value);
                    kashikataKei += Util.ToDecimal(this.dgvList[4, i].Value);
                    zandakaKei += Util.ToDecimal(this.dgvList[5, i].Value);
                }

                // 金額がマイナスの時は赤色に変更
                if (Util.ToDecimal(this.dgvList[2, i].Value) < 0)
                {
                    this.dgvList[2, i].Style.ForeColor = Color.Red;
                }
                if (Util.ToDecimal(this.dgvList[3, i].Value) < 0)
                {
                    this.dgvList[3, i].Style.ForeColor = Color.Red;
                }
                if (Util.ToDecimal(this.dgvList[4, i].Value) < 0)
                {
                    this.dgvList[4, i].Style.ForeColor = Color.Red;
                }
                if (Util.ToDecimal(this.dgvList[5, i].Value) < 0)
                {
                    this.dgvList[5, i].Style.ForeColor = Color.Red;
                }
            }

            // 総合計金額を表示
            lblZenZanKei.Text = Util.FormatNum(zenZanKei);
            lblKarikataKei.Text = Util.FormatNum(karikataKei);
            lblKashikataKei.Text = Util.FormatNum(kashikataKei);
            lblZandakaKei.Text = Util.FormatNum(zandakaKei);
            // 金額がマイナスの時は赤色に変更
            lblZenZanKei.ForeColor = zenZanKei >= 0 ? Color.Black : Color.Red;
            lblKarikataKei.ForeColor = karikataKei >= 0 ? Color.Black : Color.Red;
            lblKashikataKei.ForeColor = kashikataKei >= 0 ? Color.Black : Color.Red;
            lblZandakaKei.ForeColor = zandakaKei >= 0 ? Color.Black : Color.Red;
        }
        #endregion

    }
}
