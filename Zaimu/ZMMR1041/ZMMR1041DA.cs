﻿using System;
using System.Collections;
using System.Data;
using System.Text;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.userinfo;
using jp.co.fsi.common.util;

namespace jp.co.fsi.zm.zmmr1041
{
    /// <summary>
    /// モジュール全体で使用するデータアクセスクラスです。
    /// </summary>
    public class ZMMR1041DA
    {
        #region private変数
        /// <summary>
        /// ユーザー情報
        /// </summary>
        UserInfo _uInfo;

        /// <summary>
        /// データアクセスオブジェクト
        /// </summary>
        DbAccess _dba;
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="uInfo">操作中のユーザーの情報</param>
        /// <param name="dba">呼び出し元で保持するデータアクセスオブジェクト</param>
        public ZMMR1041DA(UserInfo uInfo, DbAccess dba)
        {
            this._uInfo = uInfo;
            this._dba = dba;
        }
        #endregion

        #region publicメソッド
        /// <summary>
        /// 補助科目集計データを取得
        /// </summary>
        /// <param name="condition">条件画面の入力値</param>
        /// <returns>補助科目集計データ</returns>
        public DataTable GetHojoData(Hashtable condition)
        {
            DbParamCollection dpc = new DbParamCollection();
            StringBuilder sql = new StringBuilder();

            int shiwakeShori = Util.ToInt(condition["ShiwakeShurui"]);
            string zeiKubun;
            if (Util.ToInt(condition["ShohizeiShoriHandan"]) == 1)
            {
                zeiKubun = "ZEIKOMI";
            }
            else
            {
                zeiKubun = "ZEINUKI";
            }
            int shohizeiShoriHandan = Util.ToInt(condition["ShohizeiShoriHandan"]);
            // 支所コード
            int shishoCd = Util.ToInt(condition["ShishoCode"]);
            decimal sCd = 0;
            string ssCd = "";

            Decimal zenZandaka;
            Decimal zandaka;
            string kamokuCd = "";
            string zenZandakaSt;
            string karikata;
            string kasikata;
            string zandakaSt;

            string zenZandakaKeiStr;
            string karikataKeiStr;
            string kasikataKeiStr;
            string zandakaKeiStr;

            // 合計金額
            Decimal zenZandakaKei = 0;
            Decimal karikataKei= 0;
            Decimal kashikataKei = 0;
            Decimal zandakaKei = 0;

            sql.Append(" SELECT");
            sql.Append("     A.SHISHO_CD           AS SHISHO_CD,");
            sql.Append("     A.KANJO_KAMOKU_CD     AS KAMOKU_CD,");
            sql.Append("     B.KANJO_KAMOKU_NM     AS KAMOKU_NM,");
            sql.Append("     B.TAISHAKU_KUBUN      AS TAI_KBN,");
            sql.Append("     A.HOJO_KAMOKU_CD      AS HOJO_KAMOKU_CD,");
            sql.Append("     MAX(CASE WHEN B.HOJO_SHIYO_KUBUN = 1 THEN C.HOJO_KAMOKU_NM ELSE D.TORIHIKISAKI_NM END) AS HOJO_KAMOKU_NM,");
            sql.Append("     SUM(CASE WHEN A.DENPYO_KUBUN = 0 THEN         CASE WHEN  A.TAISHAKU_KUBUN = B.TAISHAKU_KUBUN THEN " + zeiKubun + "_KINGAKU ELSE " + zeiKubun + "_KINGAKU * -1 END     ELSE 0 END)  AS ZEN_ZAN,");
            sql.Append("     SUM(CASE WHEN A.DENPYO_KUBUN = 1 AND A.DENPYO_DATE < CAST(@DATE_FR AS DATETIME) THEN          CASE WHEN A.TAISHAKU_KUBUN = 1 THEN " + zeiKubun + "_KINGAKU ELSE 0 END     ELSE 0 END)  AS KARI_ZEN_ZAN,");
            sql.Append("     SUM(CASE WHEN A.DENPYO_KUBUN = 1 AND A.DENPYO_DATE < CAST(@DATE_FR AS DATETIME) THEN          CASE WHEN A.TAISHAKU_KUBUN = 2 THEN " + zeiKubun + "_KINGAKU ELSE 0 END     ELSE 0 END)  AS KASHI_ZEN_ZAN,");
            sql.Append("     SUM(CASE WHEN A.DENPYO_KUBUN = 1 AND A.DENPYO_DATE >= CAST(@DATE_FR AS DATETIME) THEN          CASE WHEN A.TAISHAKU_KUBUN = 1 THEN " + zeiKubun + "_KINGAKU ELSE 0 END     ELSE 0 END)  AS KARIKATA,");
            sql.Append("     SUM(CASE WHEN A.DENPYO_KUBUN = 1 AND A.DENPYO_DATE >= CAST(@DATE_FR AS DATETIME) THEN          CASE WHEN A.TAISHAKU_KUBUN = 2 THEN " + zeiKubun + "_KINGAKU ELSE 0 END     ELSE 0 END)  AS KASHIKATA");
            sql.Append(" FROM");
            sql.Append("     TB_ZM_SHIWAKE_MEISAI AS A ");
            sql.Append(" LEFT OUTER JOIN TB_ZM_KANJO_KAMOKU AS B ");
            sql.Append("     ON (A.KAISHA_CD     = B.KAISHA_CD) AND (A.KANJO_KAMOKU_CD = B.KANJO_KAMOKU_CD) AND (A.KAIKEI_NENDO = B.KAIKEI_NENDO) ");
            sql.Append(" LEFT OUTER JOIN TB_ZM_HOJO_KAMOKU AS C ");
            //sql.Append("     ON (A.KAISHA_CD = C.KAISHA_CD) AND (A.KANJO_KAMOKU_CD = C.KANJO_KAMOKU_CD) AND (A.HOJO_KAMOKU_CD = C.HOJO_KAMOKU_CD) AND (A.KAIKEI_NENDO = C.KAIKEI_NENDO) ");
            sql.Append("     ON (B.KAISHA_CD = C.KAISHA_CD) AND ((A.SHISHO_CD = C.SHISHO_CD or C.SHISHO_CD is null)) AND (A.KAIKEI_NENDO = C.KAIKEI_NENDO) AND (A.KANJO_KAMOKU_CD = C.KANJO_KAMOKU_CD) AND (A.HOJO_KAMOKU_CD = C.HOJO_KAMOKU_CD)");

            sql.Append(" LEFT OUTER JOIN TB_CM_TORIHIKISAKI AS D ");
            sql.Append("     ON (A.KAISHA_CD     = D.KAISHA_CD) AND (A.HOJO_KAMOKU_CD = D.TORIHIKISAKI_CD)");
            sql.Append(" WHERE");
            sql.Append("     A.KAISHA_CD = @KAISHA_CD AND");

            if (shishoCd != 0)
                sql.Append("     A.SHISHO_CD = @SHISHO_CD AND");

            sql.Append("     A.KAIKEI_NENDO = @KAIKEI_NENDO AND");
            sql.Append("     A.KANJO_KAMOKU_CD BETWEEN @KANJO_KAMOKU_FR  AND @KANJO_KAMOKU_TO AND");
            sql.Append("     A.BUMON_CD BETWEEN @BUMON_FR  AND @BUMON_TO AND");
            sql.Append("     A.HOJO_KAMOKU_CD BETWEEN @HOJO_KAMOKU_FR  AND @HOJO_KAMOKU_TO AND");
            sql.Append("     A.DENPYO_DATE <= CAST(@DATE_TO AS DATETIME)  AND");
            //sql.Append("     A.MEISAI_KUBUN <= 0 AND");
            if (shohizeiShoriHandan == 1)
            {
                sql.Append(" A.MEISAI_KUBUN <= 0 AND");
            }
            else
            {
                sql.Append(" A.MEISAI_KUBUN >= 0 AND");
            }
            //sql.Append("     A.KESSAN_KUBUN = 0 AND");
            sql.Append("     B.HOJO_KAMOKU_UMU = 1");
            if (Util.ToInt(condition["ShiwakeShurui"]) != 9)
            {
                sql.Append(" AND    A.KESSAN_KUBUN = " + condition["ShiwakeShurui"]);
            }
            sql.Append(" GROUP BY");
            sql.Append("     A.SHISHO_CD,");
            sql.Append("     A.KANJO_KAMOKU_CD,");
            sql.Append("     B.KANJO_KAMOKU_NM,");
            sql.Append("     B.TAISHAKU_KUBUN,");
            sql.Append("     A.HOJO_KAMOKU_CD");
            sql.Append(" ORDER BY");
            sql.Append("     A.SHISHO_CD,");
            sql.Append("     A.KANJO_KAMOKU_CD,");
            sql.Append("     A.HOJO_KAMOKU_CD");

            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this._uInfo.KaishaCd);
            dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, shishoCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this._uInfo.KaikeiNendo);
            dpc.SetParam("@SHIWAKE_SHORI", SqlDbType.Decimal, 1, shiwakeShori);
            dpc.SetParam("@DATE_FR", SqlDbType.DateTime, Util.ToDate(condition["DtFr"]));
            dpc.SetParam("@DATE_TO", SqlDbType.DateTime, Util.ToDate(condition["DtTo"]));
            dpc.SetParam("@KANJO_KAMOKU_FR", SqlDbType.Decimal, 6, Util.ToDecimal(Util.ToString(condition["KanjoKamokuFr"])));
            dpc.SetParam("@KANJO_KAMOKU_TO", SqlDbType.Decimal, 6, Util.ToDecimal(Util.ToString(condition["KanjoKamokuTo"])));
            dpc.SetParam("@HOJO_KAMOKU_FR", SqlDbType.Decimal, 10, Util.ToDecimal(Util.ToString(condition["HojoKamokuFr"])));
            dpc.SetParam("@HOJO_KAMOKU_TO", SqlDbType.Decimal, 10, Util.ToDecimal(Util.ToString(condition["HojoKamokuTo"])));
            dpc.SetParam("@BUMON_FR", SqlDbType.VarChar, 4, Util.ToString(condition["BumonFr"]));
            dpc.SetParam("@BUMON_TO", SqlDbType.VarChar, 4, Util.ToString(condition["BumonTo"]));

            DataTable dtHojoKamokuData = this._dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            //計算結果を格納するテーブル
            DataTable dtShukeiData = new DataTable();
            dtShukeiData.Columns.Add("コード");
            dtShukeiData.Columns.Add("勘定科目名");
            dtShukeiData.Columns.Add("前月残高");
            dtShukeiData.Columns.Add("借方");
            dtShukeiData.Columns.Add("貸方");
            dtShukeiData.Columns.Add("残高");
            dtShukeiData.Columns.Add("FLG");
            dtShukeiData.Columns.Add("SHISHO_CD", typeof(decimal));
            foreach (DataRow dt in dtHojoKamokuData.Rows)
            {
                // 科目合計を挿入
                //if (!ValChk.IsEmpty(kamokuCd) && !kamokuCd.Equals(Util.ToString(dt["KAMOKU_CD"])))
                if (!ValChk.IsEmpty(kamokuCd) && (!kamokuCd.Equals(Util.ToString(dt["KAMOKU_CD"])) || !ssCd.Equals(Util.ToString(dt["SHISHO_CD"]))))
                {
                    // 金額が0の場合、非表示とする
                    zenZandakaKeiStr = Util.FormatNum(zenZandakaKei);
                    karikataKeiStr = Util.FormatNum(karikataKei);
                    kasikataKeiStr = Util.FormatNum(kashikataKei);
                    zandakaKeiStr = Util.FormatNum(zandakaKei);
                    if (zenZandakaKei == 0)
                    {
                        zenZandakaKeiStr = "";
                    }
                    if (karikataKei == 0)
                    {
                        karikataKeiStr = "";
                    }
                    if (kashikataKei == 0)
                    {
                        kasikataKeiStr = "";
                    }
                    if (zandakaKei == 0)
                    {
                        zandakaKeiStr = "";
                    }
                    dtShukeiData.Rows.Add("",
                        "合計",
                        zenZandakaKeiStr,
                        karikataKeiStr,
                        kasikataKeiStr,
                        zandakaKeiStr,
                        "",
                        sCd);
                    zenZandakaKei = 0;
                    karikataKei = 0;
                    kashikataKei = 0;
                    zandakaKei = 0;
                    sCd = Util.ToDecimal(Util.ToString(dt["SHISHO_CD"]));
                }
                // 行の始めに勘定科目名を挿入
                //if (ValChk.IsEmpty(kamokuCd) || !kamokuCd.Equals(Util.ToString(dt["KAMOKU_CD"])))
                if (ValChk.IsEmpty(kamokuCd) || (!kamokuCd.Equals(Util.ToString(dt["KAMOKU_CD"])) || !ssCd.Equals(Util.ToString(dt["SHISHO_CD"]))))
                {
                    sCd = Util.ToDecimal(Util.ToString(dt["SHISHO_CD"]));
                    dtShukeiData.Rows.Add(dt["KAMOKU_CD"],
                        dt["KAMOKU_NM"],
                        "",
                        "",
                        "",
                        "",
                        "1",
                        sCd);
                }

                // データの設定
                if (Util.ToInt(dt["TAI_KBN"]) == 1)
                {
                    zenZandaka = Util.ToDecimal(dt["ZEN_ZAN"]) + Util.ToDecimal(dt["KARI_ZEN_ZAN"]) - Util.ToDecimal(dt["KASHI_ZEN_ZAN"]);
                    zenZandakaSt = Util.ToString(zenZandaka);
                    if (zenZandakaSt == "0")
                    {
                        zenZandakaSt = "";
                    }
                }
                else
                {
                    zenZandaka = Util.ToDecimal(dt["ZEN_ZAN"]) - Util.ToDecimal(dt["KARI_ZEN_ZAN"]) + Util.ToDecimal(dt["KASHI_ZEN_ZAN"]);
                    zenZandakaSt = Util.ToString(zenZandaka);
                    if (zenZandakaSt == "0")
                    {
                        zenZandakaSt = "";
                    }
                }
                if (Util.ToDecimal(dt["KARIKATA"]) == 0)
                {
                    karikata = "";
                }
                else
                {
                    karikata = Util.ToString(dt["KARIKATA"]);
                }
                if (Util.ToDecimal(dt["KASHIKATA"]) == 0)
                {
                    kasikata = "";
                }
                else
                {
                    kasikata = Util.ToString(dt["KASHIKATA"]);
                }
                if (Util.ToInt(dt["TAI_KBN"]) == 1)
                {
                    zandaka = zenZandaka + Util.ToDecimal(karikata) - Util.ToDecimal(kasikata);
                    zandakaSt = Util.ToString(zandaka);
                    if (zandakaSt == "0")
                    {
                        zandakaSt = "";
                    }
                }
                else
                {
                    zandaka = zenZandaka - Util.ToDecimal(karikata) + Util.ToDecimal(kasikata);
                    zandakaSt = Util.ToString(zandaka);
                    if (zandakaSt == "0")
                    {
                        zandakaSt = "";
                    }
                }
                // データの追加
                if (Util.ToString(condition["Inji"]) == "no")
                {
                    if (zenZandakaSt != "" || karikata != "" || kasikata != "" || zandakaSt != "")
                    {
                        dtShukeiData.Rows.Add(dt["HOJO_KAMOKU_CD"],
                            dt["HOJO_KAMOKU_NM"],
                            Util.FormatNum(zenZandakaSt),
                            Util.FormatNum(karikata),
                            Util.FormatNum(kasikata),
                            Util.FormatNum(zandakaSt),
                            "",
                            sCd);
                    }
                }
                else
                {
                    dtShukeiData.Rows.Add(dt["HOJO_KAMOKU_CD"],
                        dt["HOJO_KAMOKU_NM"],
                        Util.FormatNum(zenZandakaSt),
                        Util.FormatNum(karikata),
                        Util.FormatNum(kasikata),
                        Util.FormatNum(zandakaSt),
                        "",
                        sCd);
                }

                // 科目コードの保持
                kamokuCd = Util.ToString(dt["KAMOKU_CD"]);
                // 支所コードの保持
                ssCd = Util.ToString(dt["SHISHO_CD"]);

                // 合計金額の計算
                zenZandakaKei += zenZandaka;
                karikataKei += Util.ToDecimal(dt["KARIKATA"]);
                kashikataKei += Util.ToDecimal(dt["KASHIKATA"]);
                zandakaKei += zandaka;
            }
            if (dtShukeiData.Rows.Count > 0)
            {
                // ループ終了後に合計を挿入
                // 金額が0の場合、非表示とする
                zenZandakaKeiStr = Util.FormatNum(zenZandakaKei);
                karikataKeiStr = Util.FormatNum(karikataKei);
                kasikataKeiStr = Util.FormatNum(kashikataKei);
                zandakaKeiStr = Util.FormatNum(zandakaKei);
                if (zenZandakaKei == 0)
                {
                    zenZandakaKeiStr = "";
                }
                if (karikataKei == 0)
                {
                    karikataKeiStr = "";
                }
                if (kashikataKei == 0)
                {
                    kasikataKeiStr = "";
                }
                if (zandakaKei == 0)
                {
                    zandakaKeiStr = "";
                }
                dtShukeiData.Rows.Add("",
                    "合計",
                    zenZandakaKeiStr,
                    karikataKeiStr,
                    kasikataKeiStr,
                    zandakaKeiStr,
                    "",
                    sCd);
            }

            return dtShukeiData;
        }
        #endregion
    }
}
