﻿namespace jp.co.fsi.zm.zmcm1011
{
    partial class ZMCM1011
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblKaishaCd = new System.Windows.Forms.Label();
            this.txtKaishaCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.gbxSenshuInfo = new System.Windows.Forms.GroupBox();
            this.txtPAYAO_NM = new jp.co.fsi.common.controls.FsiTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtPAYAO_NO = new jp.co.fsi.common.controls.FsiTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtMyNumber = new jp.co.fsi.common.controls.FsiTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblCodeBetDate = new System.Windows.Forms.Label();
            this.lblDateDayTo = new System.Windows.Forms.Label();
            this.lblDateMonthTo = new System.Windows.Forms.Label();
            this.lblDateYearTo = new System.Windows.Forms.Label();
            this.txtDateDayTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDateYearTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDateMonthTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblDateGengoTo = new System.Windows.Forms.Label();
            this.lblDateTo = new System.Windows.Forms.Label();
            this.lblDateDayFr = new System.Windows.Forms.Label();
            this.lblDateMonthFr = new System.Windows.Forms.Label();
            this.lblDateYearFr = new System.Windows.Forms.Label();
            this.txtDateDayFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDateYearFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDateMonthFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblDateGengoFr = new System.Windows.Forms.Label();
            this.lblDateFr = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox14 = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKi = new System.Windows.Forms.Label();
            this.lblAddBar = new System.Windows.Forms.Label();
            this.txtYubinBango2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKessankiCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtFax = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtTel = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtJusho2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtJusho1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtYubinBango1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDaihyoshaNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKaishaNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKessankiCd = new System.Windows.Forms.Label();
            this.lblFax = new System.Windows.Forms.Label();
            this.lblTel = new System.Windows.Forms.Label();
            this.lblJusho2 = new System.Windows.Forms.Label();
            this.lblJusho1 = new System.Windows.Forms.Label();
            this.lblYubinBango = new System.Windows.Forms.Label();
            this.lblDaihyoshaNm = new System.Windows.Forms.Label();
            this.lblKaishaNm = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.gbxSenshuInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(818, 23);
            this.lblTitle.Text = "";
            // 
            // btnF3
            // 
            this.btnF3.Text = "F3\r\n\r\nさくじょ";
            // 
            // btnF6
            // 
            this.btnF6.Text = "F6\r\n\r\nとうろく";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.pnlDebug.Location = new System.Drawing.Point(5, 538);
            this.pnlDebug.Size = new System.Drawing.Size(851, 100);
            // 
            // lblKaishaCd
            // 
            this.lblKaishaCd.BackColor = System.Drawing.Color.Silver;
            this.lblKaishaCd.Enabled = false;
            this.lblKaishaCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKaishaCd.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblKaishaCd.Location = new System.Drawing.Point(17, 48);
            this.lblKaishaCd.Name = "lblKaishaCd";
            this.lblKaishaCd.Size = new System.Drawing.Size(246, 25);
            this.lblKaishaCd.TabIndex = 40;
            this.lblKaishaCd.Text = "会社コード";
            this.lblKaishaCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKaishaCd
            // 
            this.txtKaishaCd.AllowDrop = true;
            this.txtKaishaCd.AutoSizeFromLength = false;
            this.txtKaishaCd.DisplayLength = null;
            this.txtKaishaCd.Enabled = false;
            this.txtKaishaCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKaishaCd.Location = new System.Drawing.Point(118, 51);
            this.txtKaishaCd.MaxLength = 4;
            this.txtKaishaCd.Name = "txtKaishaCd";
            this.txtKaishaCd.ReadOnly = true;
            this.txtKaishaCd.Size = new System.Drawing.Size(142, 20);
            this.txtKaishaCd.TabIndex = 41;
            // 
            // gbxSenshuInfo
            // 
            this.gbxSenshuInfo.Controls.Add(this.txtPAYAO_NM);
            this.gbxSenshuInfo.Controls.Add(this.label4);
            this.gbxSenshuInfo.Controls.Add(this.txtPAYAO_NO);
            this.gbxSenshuInfo.Controls.Add(this.label3);
            this.gbxSenshuInfo.Controls.Add(this.txtMyNumber);
            this.gbxSenshuInfo.Controls.Add(this.label1);
            this.gbxSenshuInfo.Controls.Add(this.lblCodeBetDate);
            this.gbxSenshuInfo.Controls.Add(this.lblDateDayTo);
            this.gbxSenshuInfo.Controls.Add(this.lblDateMonthTo);
            this.gbxSenshuInfo.Controls.Add(this.lblDateYearTo);
            this.gbxSenshuInfo.Controls.Add(this.txtDateDayTo);
            this.gbxSenshuInfo.Controls.Add(this.txtDateYearTo);
            this.gbxSenshuInfo.Controls.Add(this.txtDateMonthTo);
            this.gbxSenshuInfo.Controls.Add(this.lblDateGengoTo);
            this.gbxSenshuInfo.Controls.Add(this.lblDateTo);
            this.gbxSenshuInfo.Controls.Add(this.lblDateDayFr);
            this.gbxSenshuInfo.Controls.Add(this.lblDateMonthFr);
            this.gbxSenshuInfo.Controls.Add(this.lblDateYearFr);
            this.gbxSenshuInfo.Controls.Add(this.txtDateDayFr);
            this.gbxSenshuInfo.Controls.Add(this.txtDateYearFr);
            this.gbxSenshuInfo.Controls.Add(this.txtDateMonthFr);
            this.gbxSenshuInfo.Controls.Add(this.lblDateGengoFr);
            this.gbxSenshuInfo.Controls.Add(this.lblDateFr);
            this.gbxSenshuInfo.Controls.Add(this.label2);
            this.gbxSenshuInfo.Controls.Add(this.textBox14);
            this.gbxSenshuInfo.Controls.Add(this.lblKi);
            this.gbxSenshuInfo.Controls.Add(this.lblAddBar);
            this.gbxSenshuInfo.Controls.Add(this.txtYubinBango2);
            this.gbxSenshuInfo.Controls.Add(this.txtKessankiCd);
            this.gbxSenshuInfo.Controls.Add(this.txtFax);
            this.gbxSenshuInfo.Controls.Add(this.txtTel);
            this.gbxSenshuInfo.Controls.Add(this.txtJusho2);
            this.gbxSenshuInfo.Controls.Add(this.txtJusho1);
            this.gbxSenshuInfo.Controls.Add(this.txtYubinBango1);
            this.gbxSenshuInfo.Controls.Add(this.txtDaihyoshaNm);
            this.gbxSenshuInfo.Controls.Add(this.txtKaishaNm);
            this.gbxSenshuInfo.Controls.Add(this.lblKessankiCd);
            this.gbxSenshuInfo.Controls.Add(this.lblFax);
            this.gbxSenshuInfo.Controls.Add(this.lblTel);
            this.gbxSenshuInfo.Controls.Add(this.lblJusho2);
            this.gbxSenshuInfo.Controls.Add(this.lblJusho1);
            this.gbxSenshuInfo.Controls.Add(this.lblYubinBango);
            this.gbxSenshuInfo.Controls.Add(this.lblDaihyoshaNm);
            this.gbxSenshuInfo.Controls.Add(this.lblKaishaNm);
            this.gbxSenshuInfo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxSenshuInfo.Location = new System.Drawing.Point(11, 77);
            this.gbxSenshuInfo.Name = "gbxSenshuInfo";
            this.gbxSenshuInfo.Size = new System.Drawing.Size(800, 374);
            this.gbxSenshuInfo.TabIndex = 3;
            this.gbxSenshuInfo.TabStop = false;
            // 
            // txtPAYAO_NM
            // 
            this.txtPAYAO_NM.AutoSizeFromLength = false;
            this.txtPAYAO_NM.DisplayLength = null;
            this.txtPAYAO_NM.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtPAYAO_NM.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtPAYAO_NM.Location = new System.Drawing.Point(109, 306);
            this.txtPAYAO_NM.MaxLength = 8;
            this.txtPAYAO_NM.Name = "txtPAYAO_NM";
            this.txtPAYAO_NM.Size = new System.Drawing.Size(208, 23);
            this.txtPAYAO_NM.TabIndex = 41;
            this.txtPAYAO_NM.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPAYAO_NO_KeyDown);
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Silver;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.Location = new System.Drawing.Point(6, 306);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(314, 25);
            this.label4.TabIndex = 40;
            this.label4.Text = "パヤオ口座名";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtPAYAO_NO
            // 
            this.txtPAYAO_NO.AutoSizeFromLength = false;
            this.txtPAYAO_NO.DisplayLength = null;
            this.txtPAYAO_NO.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtPAYAO_NO.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtPAYAO_NO.Location = new System.Drawing.Point(109, 278);
            this.txtPAYAO_NO.MaxLength = 7;
            this.txtPAYAO_NO.Name = "txtPAYAO_NO";
            this.txtPAYAO_NO.Size = new System.Drawing.Size(75, 23);
            this.txtPAYAO_NO.TabIndex = 39;
            this.txtPAYAO_NO.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPAYAO_NO_KeyPress);
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Silver;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(6, 277);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(178, 26);
            this.label3.TabIndex = 38;
            this.label3.Text = "パヤオ口座番号";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtMyNumber
            // 
            this.txtMyNumber.AutoSizeFromLength = false;
            this.txtMyNumber.DisplayLength = null;
            this.txtMyNumber.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMyNumber.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtMyNumber.Location = new System.Drawing.Point(109, 239);
            this.txtMyNumber.MaxLength = 13;
            this.txtMyNumber.Name = "txtMyNumber";
            this.txtMyNumber.Size = new System.Drawing.Size(208, 23);
            this.txtMyNumber.TabIndex = 37;
            this.txtMyNumber.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtMyNumber_KeyDown);
            this.txtMyNumber.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPAYAO_NO_KeyPress);
            this.txtMyNumber.Validating += new System.ComponentModel.CancelEventHandler(this.txtMyNumber_Validating);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Silver;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(6, 239);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(314, 25);
            this.label1.TabIndex = 36;
            this.label1.Text = "マイナンバー";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCodeBetDate
            // 
            this.lblCodeBetDate.BackColor = System.Drawing.Color.White;
            this.lblCodeBetDate.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCodeBetDate.ForeColor = System.Drawing.Color.Black;
            this.lblCodeBetDate.Location = new System.Drawing.Point(326, 212);
            this.lblCodeBetDate.Name = "lblCodeBetDate";
            this.lblCodeBetDate.Size = new System.Drawing.Size(22, 21);
            this.lblCodeBetDate.TabIndex = 27;
            this.lblCodeBetDate.Text = "～";
            this.lblCodeBetDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDateDayTo
            // 
            this.lblDateDayTo.BackColor = System.Drawing.Color.Silver;
            this.lblDateDayTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateDayTo.ForeColor = System.Drawing.Color.Black;
            this.lblDateDayTo.Location = new System.Drawing.Point(539, 212);
            this.lblDateDayTo.Name = "lblDateDayTo";
            this.lblDateDayTo.Size = new System.Drawing.Size(20, 18);
            this.lblDateDayTo.TabIndex = 35;
            this.lblDateDayTo.Text = "日";
            this.lblDateDayTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateMonthTo
            // 
            this.lblDateMonthTo.BackColor = System.Drawing.Color.Silver;
            this.lblDateMonthTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateMonthTo.ForeColor = System.Drawing.Color.Black;
            this.lblDateMonthTo.Location = new System.Drawing.Point(484, 212);
            this.lblDateMonthTo.Name = "lblDateMonthTo";
            this.lblDateMonthTo.Size = new System.Drawing.Size(15, 19);
            this.lblDateMonthTo.TabIndex = 33;
            this.lblDateMonthTo.Text = "月";
            this.lblDateMonthTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateYearTo
            // 
            this.lblDateYearTo.BackColor = System.Drawing.Color.Silver;
            this.lblDateYearTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateYearTo.ForeColor = System.Drawing.Color.Black;
            this.lblDateYearTo.Location = new System.Drawing.Point(431, 211);
            this.lblDateYearTo.Name = "lblDateYearTo";
            this.lblDateYearTo.Size = new System.Drawing.Size(17, 21);
            this.lblDateYearTo.TabIndex = 31;
            this.lblDateYearTo.Text = "年";
            this.lblDateYearTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDateDayTo
            // 
            this.txtDateDayTo.AutoSizeFromLength = false;
            this.txtDateDayTo.DisplayLength = null;
            this.txtDateDayTo.Enabled = false;
            this.txtDateDayTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateDayTo.ForeColor = System.Drawing.Color.Black;
            this.txtDateDayTo.Location = new System.Drawing.Point(506, 211);
            this.txtDateDayTo.MaxLength = 2;
            this.txtDateDayTo.Name = "txtDateDayTo";
            this.txtDateDayTo.ReadOnly = true;
            this.txtDateDayTo.Size = new System.Drawing.Size(30, 20);
            this.txtDateDayTo.TabIndex = 34;
            this.txtDateDayTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtDateYearTo
            // 
            this.txtDateYearTo.AutoSizeFromLength = false;
            this.txtDateYearTo.DisplayLength = null;
            this.txtDateYearTo.Enabled = false;
            this.txtDateYearTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateYearTo.ForeColor = System.Drawing.Color.Black;
            this.txtDateYearTo.Location = new System.Drawing.Point(399, 211);
            this.txtDateYearTo.MaxLength = 2;
            this.txtDateYearTo.Name = "txtDateYearTo";
            this.txtDateYearTo.ReadOnly = true;
            this.txtDateYearTo.Size = new System.Drawing.Size(30, 20);
            this.txtDateYearTo.TabIndex = 30;
            this.txtDateYearTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtDateMonthTo
            // 
            this.txtDateMonthTo.AutoSizeFromLength = false;
            this.txtDateMonthTo.DisplayLength = null;
            this.txtDateMonthTo.Enabled = false;
            this.txtDateMonthTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateMonthTo.ForeColor = System.Drawing.Color.Black;
            this.txtDateMonthTo.Location = new System.Drawing.Point(452, 211);
            this.txtDateMonthTo.MaxLength = 2;
            this.txtDateMonthTo.Name = "txtDateMonthTo";
            this.txtDateMonthTo.ReadOnly = true;
            this.txtDateMonthTo.Size = new System.Drawing.Size(30, 20);
            this.txtDateMonthTo.TabIndex = 32;
            this.txtDateMonthTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblDateGengoTo
            // 
            this.lblDateGengoTo.BackColor = System.Drawing.Color.Silver;
            this.lblDateGengoTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDateGengoTo.Enabled = false;
            this.lblDateGengoTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateGengoTo.ForeColor = System.Drawing.Color.Black;
            this.lblDateGengoTo.Location = new System.Drawing.Point(353, 212);
            this.lblDateGengoTo.Name = "lblDateGengoTo";
            this.lblDateGengoTo.Size = new System.Drawing.Size(41, 21);
            this.lblDateGengoTo.TabIndex = 29;
            this.lblDateGengoTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateTo
            // 
            this.lblDateTo.BackColor = System.Drawing.Color.Silver;
            this.lblDateTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateTo.ForeColor = System.Drawing.Color.Black;
            this.lblDateTo.Location = new System.Drawing.Point(351, 208);
            this.lblDateTo.Name = "lblDateTo";
            this.lblDateTo.Size = new System.Drawing.Size(213, 27);
            this.lblDateTo.TabIndex = 28;
            this.lblDateTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateDayFr
            // 
            this.lblDateDayFr.BackColor = System.Drawing.Color.Silver;
            this.lblDateDayFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateDayFr.ForeColor = System.Drawing.Color.Black;
            this.lblDateDayFr.Location = new System.Drawing.Point(294, 213);
            this.lblDateDayFr.Name = "lblDateDayFr";
            this.lblDateDayFr.Size = new System.Drawing.Size(20, 18);
            this.lblDateDayFr.TabIndex = 26;
            this.lblDateDayFr.Text = "日";
            this.lblDateDayFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateMonthFr
            // 
            this.lblDateMonthFr.BackColor = System.Drawing.Color.Silver;
            this.lblDateMonthFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateMonthFr.ForeColor = System.Drawing.Color.Black;
            this.lblDateMonthFr.Location = new System.Drawing.Point(239, 213);
            this.lblDateMonthFr.Name = "lblDateMonthFr";
            this.lblDateMonthFr.Size = new System.Drawing.Size(15, 19);
            this.lblDateMonthFr.TabIndex = 24;
            this.lblDateMonthFr.Text = "月";
            this.lblDateMonthFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateYearFr
            // 
            this.lblDateYearFr.BackColor = System.Drawing.Color.Silver;
            this.lblDateYearFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateYearFr.ForeColor = System.Drawing.Color.Black;
            this.lblDateYearFr.Location = new System.Drawing.Point(186, 212);
            this.lblDateYearFr.Name = "lblDateYearFr";
            this.lblDateYearFr.Size = new System.Drawing.Size(17, 21);
            this.lblDateYearFr.TabIndex = 22;
            this.lblDateYearFr.Text = "年";
            this.lblDateYearFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDateDayFr
            // 
            this.txtDateDayFr.AutoSizeFromLength = false;
            this.txtDateDayFr.DisplayLength = null;
            this.txtDateDayFr.Enabled = false;
            this.txtDateDayFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateDayFr.ForeColor = System.Drawing.Color.Black;
            this.txtDateDayFr.Location = new System.Drawing.Point(261, 212);
            this.txtDateDayFr.MaxLength = 2;
            this.txtDateDayFr.Name = "txtDateDayFr";
            this.txtDateDayFr.ReadOnly = true;
            this.txtDateDayFr.Size = new System.Drawing.Size(30, 20);
            this.txtDateDayFr.TabIndex = 25;
            this.txtDateDayFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtDateYearFr
            // 
            this.txtDateYearFr.AutoSizeFromLength = false;
            this.txtDateYearFr.DisplayLength = null;
            this.txtDateYearFr.Enabled = false;
            this.txtDateYearFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateYearFr.ForeColor = System.Drawing.Color.Black;
            this.txtDateYearFr.Location = new System.Drawing.Point(154, 212);
            this.txtDateYearFr.MaxLength = 2;
            this.txtDateYearFr.Name = "txtDateYearFr";
            this.txtDateYearFr.ReadOnly = true;
            this.txtDateYearFr.Size = new System.Drawing.Size(30, 20);
            this.txtDateYearFr.TabIndex = 21;
            this.txtDateYearFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtDateMonthFr
            // 
            this.txtDateMonthFr.AutoSizeFromLength = false;
            this.txtDateMonthFr.DisplayLength = null;
            this.txtDateMonthFr.Enabled = false;
            this.txtDateMonthFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateMonthFr.ForeColor = System.Drawing.Color.Black;
            this.txtDateMonthFr.Location = new System.Drawing.Point(207, 212);
            this.txtDateMonthFr.MaxLength = 2;
            this.txtDateMonthFr.Name = "txtDateMonthFr";
            this.txtDateMonthFr.ReadOnly = true;
            this.txtDateMonthFr.Size = new System.Drawing.Size(30, 20);
            this.txtDateMonthFr.TabIndex = 23;
            this.txtDateMonthFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblDateGengoFr
            // 
            this.lblDateGengoFr.BackColor = System.Drawing.Color.Silver;
            this.lblDateGengoFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDateGengoFr.Enabled = false;
            this.lblDateGengoFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateGengoFr.ForeColor = System.Drawing.Color.Black;
            this.lblDateGengoFr.Location = new System.Drawing.Point(109, 212);
            this.lblDateGengoFr.Name = "lblDateGengoFr";
            this.lblDateGengoFr.Size = new System.Drawing.Size(41, 21);
            this.lblDateGengoFr.TabIndex = 20;
            this.lblDateGengoFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateFr
            // 
            this.lblDateFr.BackColor = System.Drawing.Color.Silver;
            this.lblDateFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateFr.ForeColor = System.Drawing.Color.Black;
            this.lblDateFr.Location = new System.Drawing.Point(93, 209);
            this.lblDateFr.Name = "lblDateFr";
            this.lblDateFr.Size = new System.Drawing.Size(227, 27);
            this.lblDateFr.TabIndex = 20;
            this.lblDateFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Silver;
            this.label2.Enabled = false;
            this.label2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(6, 209);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 27);
            this.label2.TabIndex = 19;
            this.label2.Text = "適 用 範 囲";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox14
            // 
            this.textBox14.AllowDrop = true;
            this.textBox14.AutoSizeFromLength = false;
            this.textBox14.DisplayLength = null;
            this.textBox14.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox14.Location = new System.Drawing.Point(919, -149);
            this.textBox14.MaxLength = 4;
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(50, 19);
            this.textBox14.TabIndex = 938;
            // 
            // lblKi
            // 
            this.lblKi.BackColor = System.Drawing.Color.Transparent;
            this.lblKi.Enabled = false;
            this.lblKi.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKi.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblKi.Location = new System.Drawing.Point(143, 183);
            this.lblKi.Name = "lblKi";
            this.lblKi.Size = new System.Drawing.Size(25, 25);
            this.lblKi.TabIndex = 18;
            this.lblKi.Text = "期";
            this.lblKi.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblAddBar
            // 
            this.lblAddBar.AutoSize = true;
            this.lblAddBar.BackColor = System.Drawing.Color.Silver;
            this.lblAddBar.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblAddBar.Location = new System.Drawing.Point(154, 71);
            this.lblAddBar.Name = "lblAddBar";
            this.lblAddBar.Size = new System.Drawing.Size(14, 13);
            this.lblAddBar.TabIndex = 6;
            this.lblAddBar.Text = "-";
            // 
            // txtYubinBango2
            // 
            this.txtYubinBango2.AllowDrop = true;
            this.txtYubinBango2.AutoSizeFromLength = false;
            this.txtYubinBango2.BackColor = System.Drawing.Color.White;
            this.txtYubinBango2.DisplayLength = null;
            this.txtYubinBango2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtYubinBango2.Location = new System.Drawing.Point(172, 66);
            this.txtYubinBango2.MaxLength = 4;
            this.txtYubinBango2.Name = "txtYubinBango2";
            this.txtYubinBango2.Size = new System.Drawing.Size(46, 20);
            this.txtYubinBango2.TabIndex = 7;
            this.txtYubinBango2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPAYAO_NO_KeyPress);
            this.txtYubinBango2.Validating += new System.ComponentModel.CancelEventHandler(this.txtYubinBango2_Validating);
            // 
            // txtKessankiCd
            // 
            this.txtKessankiCd.AllowDrop = true;
            this.txtKessankiCd.AutoSizeFromLength = false;
            this.txtKessankiCd.BackColor = System.Drawing.Color.White;
            this.txtKessankiCd.DisplayLength = null;
            this.txtKessankiCd.Enabled = false;
            this.txtKessankiCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKessankiCd.Location = new System.Drawing.Point(109, 185);
            this.txtKessankiCd.MaxLength = 3;
            this.txtKessankiCd.Name = "txtKessankiCd";
            this.txtKessankiCd.ReadOnly = true;
            this.txtKessankiCd.Size = new System.Drawing.Size(28, 20);
            this.txtKessankiCd.TabIndex = 17;
            this.txtKessankiCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtFax
            // 
            this.txtFax.AllowDrop = true;
            this.txtFax.AutoSizeFromLength = false;
            this.txtFax.BackColor = System.Drawing.Color.White;
            this.txtFax.DisplayLength = null;
            this.txtFax.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFax.Location = new System.Drawing.Point(316, 147);
            this.txtFax.MaxLength = 15;
            this.txtFax.Name = "txtFax";
            this.txtFax.Size = new System.Drawing.Size(120, 20);
            this.txtFax.TabIndex = 15;
            this.txtFax.Validating += new System.ComponentModel.CancelEventHandler(this.txtFax_Validating);
            // 
            // txtTel
            // 
            this.txtTel.AllowDrop = true;
            this.txtTel.AutoSizeFromLength = false;
            this.txtTel.BackColor = System.Drawing.Color.White;
            this.txtTel.DisplayLength = null;
            this.txtTel.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTel.Location = new System.Drawing.Point(109, 147);
            this.txtTel.MaxLength = 15;
            this.txtTel.Name = "txtTel";
            this.txtTel.Size = new System.Drawing.Size(120, 20);
            this.txtTel.TabIndex = 13;
            this.txtTel.Validating += new System.ComponentModel.CancelEventHandler(this.txtTel_Validating);
            // 
            // txtJusho2
            // 
            this.txtJusho2.AllowDrop = true;
            this.txtJusho2.AutoSizeFromLength = false;
            this.txtJusho2.BackColor = System.Drawing.Color.White;
            this.txtJusho2.DisplayLength = null;
            this.txtJusho2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtJusho2.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtJusho2.Location = new System.Drawing.Point(109, 120);
            this.txtJusho2.MaxLength = 40;
            this.txtJusho2.Name = "txtJusho2";
            this.txtJusho2.Size = new System.Drawing.Size(287, 20);
            this.txtJusho2.TabIndex = 11;
            this.txtJusho2.Validating += new System.ComponentModel.CancelEventHandler(this.txtJusho2_Validating);
            // 
            // txtJusho1
            // 
            this.txtJusho1.AllowDrop = true;
            this.txtJusho1.AutoSizeFromLength = false;
            this.txtJusho1.BackColor = System.Drawing.Color.White;
            this.txtJusho1.DisplayLength = null;
            this.txtJusho1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtJusho1.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtJusho1.Location = new System.Drawing.Point(109, 93);
            this.txtJusho1.MaxLength = 40;
            this.txtJusho1.Name = "txtJusho1";
            this.txtJusho1.Size = new System.Drawing.Size(287, 20);
            this.txtJusho1.TabIndex = 9;
            this.txtJusho1.Validating += new System.ComponentModel.CancelEventHandler(this.txtJusho1_Validating);
            // 
            // txtYubinBango1
            // 
            this.txtYubinBango1.AllowDrop = true;
            this.txtYubinBango1.AutoSizeFromLength = false;
            this.txtYubinBango1.BackColor = System.Drawing.Color.White;
            this.txtYubinBango1.DisplayLength = null;
            this.txtYubinBango1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtYubinBango1.Location = new System.Drawing.Point(109, 66);
            this.txtYubinBango1.MaxLength = 3;
            this.txtYubinBango1.Name = "txtYubinBango1";
            this.txtYubinBango1.Size = new System.Drawing.Size(39, 20);
            this.txtYubinBango1.TabIndex = 5;
            this.txtYubinBango1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPAYAO_NO_KeyPress);
            this.txtYubinBango1.Validating += new System.ComponentModel.CancelEventHandler(this.txtYubinBango1_Validating);
            // 
            // txtDaihyoshaNm
            // 
            this.txtDaihyoshaNm.AllowDrop = true;
            this.txtDaihyoshaNm.AutoSizeFromLength = false;
            this.txtDaihyoshaNm.BackColor = System.Drawing.Color.White;
            this.txtDaihyoshaNm.DisplayLength = null;
            this.txtDaihyoshaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDaihyoshaNm.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtDaihyoshaNm.Location = new System.Drawing.Point(109, 40);
            this.txtDaihyoshaNm.MaxLength = 30;
            this.txtDaihyoshaNm.Name = "txtDaihyoshaNm";
            this.txtDaihyoshaNm.Size = new System.Drawing.Size(216, 20);
            this.txtDaihyoshaNm.TabIndex = 3;
            this.txtDaihyoshaNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtDaihyosha_Validating);
            // 
            // txtKaishaNm
            // 
            this.txtKaishaNm.AllowDrop = true;
            this.txtKaishaNm.AutoSizeFromLength = false;
            this.txtKaishaNm.BackColor = System.Drawing.Color.White;
            this.txtKaishaNm.DisplayLength = null;
            this.txtKaishaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKaishaNm.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtKaishaNm.Location = new System.Drawing.Point(109, 13);
            this.txtKaishaNm.MaxLength = 40;
            this.txtKaishaNm.Name = "txtKaishaNm";
            this.txtKaishaNm.Size = new System.Drawing.Size(287, 20);
            this.txtKaishaNm.TabIndex = 1;
            this.txtKaishaNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtKaishaNm_Validating);
            // 
            // lblKessankiCd
            // 
            this.lblKessankiCd.BackColor = System.Drawing.Color.Silver;
            this.lblKessankiCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKessankiCd.Enabled = false;
            this.lblKessankiCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKessankiCd.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblKessankiCd.Location = new System.Drawing.Point(6, 183);
            this.lblKessankiCd.Name = "lblKessankiCd";
            this.lblKessankiCd.Size = new System.Drawing.Size(135, 25);
            this.lblKessankiCd.TabIndex = 16;
            this.lblKessankiCd.Text = "決 算 期    第";
            this.lblKessankiCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFax
            // 
            this.lblFax.BackColor = System.Drawing.Color.Silver;
            this.lblFax.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFax.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFax.Location = new System.Drawing.Point(234, 145);
            this.lblFax.Name = "lblFax";
            this.lblFax.Size = new System.Drawing.Size(206, 25);
            this.lblFax.TabIndex = 14;
            this.lblFax.Text = "ＦＡＸ番号";
            this.lblFax.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTel
            // 
            this.lblTel.BackColor = System.Drawing.Color.Silver;
            this.lblTel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTel.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTel.Location = new System.Drawing.Point(6, 145);
            this.lblTel.Name = "lblTel";
            this.lblTel.Size = new System.Drawing.Size(226, 25);
            this.lblTel.TabIndex = 12;
            this.lblTel.Text = "電話番号";
            this.lblTel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblJusho2
            // 
            this.lblJusho2.BackColor = System.Drawing.Color.Silver;
            this.lblJusho2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblJusho2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJusho2.Location = new System.Drawing.Point(6, 118);
            this.lblJusho2.Name = "lblJusho2";
            this.lblJusho2.Size = new System.Drawing.Size(394, 25);
            this.lblJusho2.TabIndex = 10;
            this.lblJusho2.Text = "住所２";
            this.lblJusho2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblJusho1
            // 
            this.lblJusho1.BackColor = System.Drawing.Color.Silver;
            this.lblJusho1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblJusho1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJusho1.Location = new System.Drawing.Point(6, 91);
            this.lblJusho1.Name = "lblJusho1";
            this.lblJusho1.Size = new System.Drawing.Size(394, 25);
            this.lblJusho1.TabIndex = 8;
            this.lblJusho1.Text = "住所１";
            this.lblJusho1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblYubinBango
            // 
            this.lblYubinBango.BackColor = System.Drawing.Color.Silver;
            this.lblYubinBango.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblYubinBango.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblYubinBango.Location = new System.Drawing.Point(6, 64);
            this.lblYubinBango.Name = "lblYubinBango";
            this.lblYubinBango.Size = new System.Drawing.Size(215, 25);
            this.lblYubinBango.TabIndex = 6;
            this.lblYubinBango.Text = "郵便番号";
            this.lblYubinBango.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDaihyoshaNm
            // 
            this.lblDaihyoshaNm.BackColor = System.Drawing.Color.Silver;
            this.lblDaihyoshaNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDaihyoshaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDaihyoshaNm.Location = new System.Drawing.Point(6, 38);
            this.lblDaihyoshaNm.Name = "lblDaihyoshaNm";
            this.lblDaihyoshaNm.Size = new System.Drawing.Size(322, 25);
            this.lblDaihyoshaNm.TabIndex = 2;
            this.lblDaihyoshaNm.Text = "代 表 者 名";
            this.lblDaihyoshaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKaishaNm
            // 
            this.lblKaishaNm.BackColor = System.Drawing.Color.Silver;
            this.lblKaishaNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKaishaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKaishaNm.Location = new System.Drawing.Point(6, 11);
            this.lblKaishaNm.Name = "lblKaishaNm";
            this.lblKaishaNm.Size = new System.Drawing.Size(394, 25);
            this.lblKaishaNm.TabIndex = 0;
            this.lblKaishaNm.Text = "会   社   名";
            this.lblKaishaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ZMCM1011
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(843, 642);
            this.Controls.Add(this.txtKaishaCd);
            this.Controls.Add(this.lblKaishaCd);
            this.Controls.Add(this.gbxSenshuInfo);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "ZMCM1011";
            this.ShowFButton = true;
            this.Text = "ReportSample";
            this.Controls.SetChildIndex(this.gbxSenshuInfo, 0);
            this.Controls.SetChildIndex(this.lblKaishaCd, 0);
            this.Controls.SetChildIndex(this.txtKaishaCd, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.pnlDebug.ResumeLayout(false);
            this.gbxSenshuInfo.ResumeLayout(false);
            this.gbxSenshuInfo.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblKaishaCd;
        private jp.co.fsi.common.controls.FsiTextBox txtKaishaCd;
        private System.Windows.Forms.GroupBox gbxSenshuInfo;
        private System.Windows.Forms.Label lblFax;
        private System.Windows.Forms.Label lblTel;
        private System.Windows.Forms.Label lblJusho2;
        private System.Windows.Forms.Label lblJusho1;
        private System.Windows.Forms.Label lblYubinBango;
        private System.Windows.Forms.Label lblDaihyoshaNm;
        private System.Windows.Forms.Label lblKaishaNm;
        private System.Windows.Forms.Label lblKessankiCd;
        private jp.co.fsi.common.controls.FsiTextBox txtKaishaNm;
        private jp.co.fsi.common.controls.FsiTextBox txtKessankiCd;
        private jp.co.fsi.common.controls.FsiTextBox txtFax;
        private jp.co.fsi.common.controls.FsiTextBox txtTel;
        private jp.co.fsi.common.controls.FsiTextBox txtJusho2;
        private jp.co.fsi.common.controls.FsiTextBox txtJusho1;
        private jp.co.fsi.common.controls.FsiTextBox txtYubinBango1;
        private jp.co.fsi.common.controls.FsiTextBox txtDaihyoshaNm;
        private System.Windows.Forms.Label lblAddBar;
        private jp.co.fsi.common.controls.FsiTextBox txtYubinBango2;
        private System.Windows.Forms.Label lblKi;
        private jp.co.fsi.common.controls.FsiTextBox textBox14;
        private System.Windows.Forms.Label lblCodeBetDate;
        private System.Windows.Forms.Label lblDateDayTo;
        private System.Windows.Forms.Label lblDateMonthTo;
        private System.Windows.Forms.Label lblDateYearTo;
        private common.controls.FsiTextBox txtDateDayTo;
        private common.controls.FsiTextBox txtDateYearTo;
        private common.controls.FsiTextBox txtDateMonthTo;
        private System.Windows.Forms.Label lblDateGengoTo;
        private System.Windows.Forms.Label lblDateTo;
        private System.Windows.Forms.Label lblDateDayFr;
        private System.Windows.Forms.Label lblDateMonthFr;
        private System.Windows.Forms.Label lblDateYearFr;
        private common.controls.FsiTextBox txtDateDayFr;
        private common.controls.FsiTextBox txtDateYearFr;
        private common.controls.FsiTextBox txtDateMonthFr;
        private System.Windows.Forms.Label lblDateGengoFr;
        private System.Windows.Forms.Label lblDateFr;
        private System.Windows.Forms.Label label2;
        private common.controls.FsiTextBox txtMyNumber;
        private System.Windows.Forms.Label label1;
        private common.controls.FsiTextBox txtPAYAO_NM;
        private System.Windows.Forms.Label label4;
        private common.controls.FsiTextBox txtPAYAO_NO;
        private System.Windows.Forms.Label label3;
    }
}