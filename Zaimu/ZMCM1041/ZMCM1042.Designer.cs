﻿namespace jp.co.fsi.zm.zmcm1041
{
    partial class ZMCM1042
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtHojoCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKanjoCd = new System.Windows.Forms.Label();
            this.pnlMain = new jp.co.fsi.common.FsiPanel();
            this.txtHojoKanaNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblkanjoKanaNm = new System.Windows.Forms.Label();
            this.txtHojoNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKanjoNm = new System.Windows.Forms.Label();
            this.lblKanjoK = new System.Windows.Forms.Label();
            this.lblKanjoKamokuCd = new System.Windows.Forms.Label();
            this.txtKanjoKamokuCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.pnlDebug.SuspendLayout();
            this.pnlMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(476, 23);
            this.lblTitle.Text = "補助科目の登録";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(5, 53);
            this.pnlDebug.Size = new System.Drawing.Size(493, 100);
            // 
            // txtHojoCd
            // 
            this.txtHojoCd.AutoSizeFromLength = true;
            this.txtHojoCd.DisplayLength = null;
            this.txtHojoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtHojoCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtHojoCd.Location = new System.Drawing.Point(132, 13);
            this.txtHojoCd.MaxLength = 6;
            this.txtHojoCd.Name = "txtHojoCd";
            this.txtHojoCd.Size = new System.Drawing.Size(48, 20);
            this.txtHojoCd.TabIndex = 2;
            this.txtHojoCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtHojoCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtkanjoCd_Validating);
            // 
            // lblKanjoCd
            // 
            this.lblKanjoCd.BackColor = System.Drawing.Color.Silver;
            this.lblKanjoCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKanjoCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKanjoCd.Location = new System.Drawing.Point(17, 11);
            this.lblKanjoCd.Name = "lblKanjoCd";
            this.lblKanjoCd.Size = new System.Drawing.Size(167, 25);
            this.lblKanjoCd.TabIndex = 1;
            this.lblKanjoCd.Text = "補助科目コード";
            this.lblKanjoCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlMain
            // 
            this.pnlMain.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlMain.Controls.Add(this.txtHojoKanaNm);
            this.pnlMain.Controls.Add(this.lblkanjoKanaNm);
            this.pnlMain.Controls.Add(this.txtHojoNm);
            this.pnlMain.Controls.Add(this.lblKanjoNm);
            this.pnlMain.Location = new System.Drawing.Point(12, 39);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(400, 58);
            this.pnlMain.TabIndex = 3;
            // 
            // txtHojoKanaNm
            // 
            this.txtHojoKanaNm.AutoSizeFromLength = false;
            this.txtHojoKanaNm.DisplayLength = null;
            this.txtHojoKanaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtHojoKanaNm.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtHojoKanaNm.Location = new System.Drawing.Point(118, 30);
            this.txtHojoKanaNm.MaxLength = 30;
            this.txtHojoKanaNm.Name = "txtHojoKanaNm";
            this.txtHojoKanaNm.Size = new System.Drawing.Size(269, 20);
            this.txtHojoKanaNm.TabIndex = 3;
            this.txtHojoKanaNm.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtHojoKanaNm_KeyDown);
            this.txtHojoKanaNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtkanjoKanaNm_Validating);
            // 
            // lblkanjoKanaNm
            // 
            this.lblkanjoKanaNm.BackColor = System.Drawing.Color.Silver;
            this.lblkanjoKanaNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblkanjoKanaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblkanjoKanaNm.Location = new System.Drawing.Point(3, 28);
            this.lblkanjoKanaNm.Name = "lblkanjoKanaNm";
            this.lblkanjoKanaNm.Size = new System.Drawing.Size(387, 25);
            this.lblkanjoKanaNm.TabIndex = 2;
            this.lblkanjoKanaNm.Text = "補助科目カナ名";
            this.lblkanjoKanaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtHojoNm
            // 
            this.txtHojoNm.AutoSizeFromLength = false;
            this.txtHojoNm.DisplayLength = null;
            this.txtHojoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtHojoNm.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtHojoNm.Location = new System.Drawing.Point(118, 5);
            this.txtHojoNm.MaxLength = 40;
            this.txtHojoNm.Name = "txtHojoNm";
            this.txtHojoNm.Size = new System.Drawing.Size(269, 20);
            this.txtHojoNm.TabIndex = 1;
            this.txtHojoNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtkanjoNm_Validating);
            // 
            // lblKanjoNm
            // 
            this.lblKanjoNm.BackColor = System.Drawing.Color.Silver;
            this.lblKanjoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKanjoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKanjoNm.Location = new System.Drawing.Point(3, 3);
            this.lblKanjoNm.Name = "lblKanjoNm";
            this.lblKanjoNm.Size = new System.Drawing.Size(387, 25);
            this.lblKanjoNm.TabIndex = 0;
            this.lblKanjoNm.Text = "補助科目名";
            this.lblKanjoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKanjoK
            // 
            this.lblKanjoK.BackColor = System.Drawing.Color.Silver;
            this.lblKanjoK.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKanjoK.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKanjoK.Location = new System.Drawing.Point(183, 11);
            this.lblKanjoK.Name = "lblKanjoK";
            this.lblKanjoK.Size = new System.Drawing.Size(158, 25);
            this.lblKanjoK.TabIndex = 902;
            this.lblKanjoK.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKanjoKamokuCd
            // 
            this.lblKanjoKamokuCd.BackColor = System.Drawing.Color.Silver;
            this.lblKanjoKamokuCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKanjoKamokuCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKanjoKamokuCd.Location = new System.Drawing.Point(340, 11);
            this.lblKanjoKamokuCd.Name = "lblKanjoKamokuCd";
            this.lblKanjoKamokuCd.Size = new System.Drawing.Size(155, 25);
            this.lblKanjoKamokuCd.TabIndex = 904;
            this.lblKanjoKamokuCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKanjoKamokuCd
            // 
            this.txtKanjoKamokuCd.AutoSizeFromLength = true;
            this.txtKanjoKamokuCd.DisplayLength = null;
            this.txtKanjoKamokuCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKanjoKamokuCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtKanjoKamokuCd.Location = new System.Drawing.Point(260, 13);
            this.txtKanjoKamokuCd.MaxLength = 6;
            this.txtKanjoKamokuCd.Name = "txtKanjoKamokuCd";
            this.txtKanjoKamokuCd.Size = new System.Drawing.Size(80, 20);
            this.txtKanjoKamokuCd.TabIndex = 905;
            this.txtKanjoKamokuCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // ZMCM1042
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(501, 156);
            this.Controls.Add(this.txtKanjoKamokuCd);
            this.Controls.Add(this.lblKanjoKamokuCd);
            this.Controls.Add(this.lblKanjoK);
            this.Controls.Add(this.pnlMain);
            this.Controls.Add(this.txtHojoCd);
            this.Controls.Add(this.lblKanjoCd);
            this.Name = "ZMCM1042";
            this.ShowFButton = true;
            this.Text = "補助科目の登録";
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblKanjoCd, 0);
            this.Controls.SetChildIndex(this.txtHojoCd, 0);
            this.Controls.SetChildIndex(this.pnlMain, 0);
            this.Controls.SetChildIndex(this.lblKanjoK, 0);
            this.Controls.SetChildIndex(this.lblKanjoKamokuCd, 0);
            this.Controls.SetChildIndex(this.txtKanjoKamokuCd, 0);
            this.pnlDebug.ResumeLayout(false);
            this.pnlMain.ResumeLayout(false);
            this.pnlMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private jp.co.fsi.common.controls.FsiTextBox txtHojoCd;
        private System.Windows.Forms.Label lblKanjoCd;
        private jp.co.fsi.common.FsiPanel pnlMain;
        private jp.co.fsi.common.controls.FsiTextBox txtHojoNm;
        private System.Windows.Forms.Label lblKanjoNm;
        private System.Windows.Forms.Label lblkanjoKanaNm;
        private jp.co.fsi.common.controls.FsiTextBox txtHojoKanaNm;
        private System.Windows.Forms.Label lblKanjoK;
        private System.Windows.Forms.Label lblKanjoKamokuCd;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuCd;
    }
}