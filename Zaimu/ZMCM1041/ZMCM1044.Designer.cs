﻿namespace jp.co.fsi.zm.zmcm1041
{
    partial class ZMCM1044
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblKanaName = new System.Windows.Forms.Label();
            this.txtKanaName = new System.Windows.Forms.TextBox();
            this.dgvList = new System.Windows.Forms.DataGridView();
            this.txtKnjoKamkCd = new System.Windows.Forms.TextBox();
            this.lblKnjoKamk = new System.Windows.Forms.Label();
            this.lblKnjoKamok = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(516, 23);
            this.lblTitle.Text = "補助科目検索";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(5, 304);
            this.pnlDebug.Size = new System.Drawing.Size(533, 100);
            // 
            // lblKanaName
            // 
            this.lblKanaName.BackColor = System.Drawing.Color.Silver;
            this.lblKanaName.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKanaName.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKanaName.Location = new System.Drawing.Point(17, 36);
            this.lblKanaName.Name = "lblKanaName";
            this.lblKanaName.Size = new System.Drawing.Size(302, 25);
            this.lblKanaName.TabIndex = 1;
            this.lblKanaName.Text = "カ　ナ　名";
            this.lblKanaName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKanaName
            // 
            this.txtKanaName.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKanaName.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtKanaName.Location = new System.Drawing.Point(136, 38);
            this.txtKanaName.MaxLength = 30;
            this.txtKanaName.Name = "txtKanaName";
            this.txtKanaName.Size = new System.Drawing.Size(180, 20);
            this.txtKanaName.TabIndex = 2;
            this.txtKanaName.Validating += new System.ComponentModel.CancelEventHandler(this.txtKanaName_Validating);
            // 
            // dgvList
            // 
            this.dgvList.AllowUserToAddRows = false;
            this.dgvList.AllowUserToDeleteRows = false;
            this.dgvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvList.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.dgvList.Location = new System.Drawing.Point(13, 64);
            this.dgvList.MultiSelect = false;
            this.dgvList.Name = "dgvList";
            this.dgvList.ReadOnly = true;
            this.dgvList.RowHeadersVisible = false;
            this.dgvList.RowTemplate.Height = 21;
            this.dgvList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvList.Size = new System.Drawing.Size(500, 280);
            this.dgvList.TabIndex = 3;
            this.dgvList.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvList_CellDoubleClick);
            this.dgvList.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvList_KeyDown);
            // 
            // txtKnjoKamkCd
            // 
            this.txtKnjoKamkCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKnjoKamkCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtKnjoKamkCd.Location = new System.Drawing.Point(136, 11);
            this.txtKnjoKamkCd.MaxLength = 6;
            this.txtKnjoKamkCd.Name = "txtKnjoKamkCd";
            this.txtKnjoKamkCd.Size = new System.Drawing.Size(79, 20);
            this.txtKnjoKamkCd.TabIndex = 1;
            this.txtKnjoKamkCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKnjoKamkCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtKnjoKamkCd_Validating);
            // 
            // lblKnjoKamk
            // 
            this.lblKnjoKamk.BackColor = System.Drawing.Color.Silver;
            this.lblKnjoKamk.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKnjoKamk.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKnjoKamk.Location = new System.Drawing.Point(17, 9);
            this.lblKnjoKamk.Name = "lblKnjoKamk";
            this.lblKnjoKamk.Size = new System.Drawing.Size(198, 25);
            this.lblKnjoKamk.TabIndex = 902;
            this.lblKnjoKamk.Text = "勘定科目";
            this.lblKnjoKamk.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKnjoKamok
            // 
            this.lblKnjoKamok.BackColor = System.Drawing.Color.Silver;
            this.lblKnjoKamok.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKnjoKamok.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKnjoKamok.Location = new System.Drawing.Point(213, 9);
            this.lblKnjoKamok.Name = "lblKnjoKamok";
            this.lblKnjoKamok.Size = new System.Drawing.Size(216, 25);
            this.lblKnjoKamok.TabIndex = 904;
            this.lblKnjoKamok.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ZMCM1044
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(525, 407);
            this.Controls.Add(this.lblKnjoKamok);
            this.Controls.Add(this.txtKnjoKamkCd);
            this.Controls.Add(this.lblKnjoKamk);
            this.Controls.Add(this.dgvList);
            this.Controls.Add(this.txtKanaName);
            this.Controls.Add(this.lblKanaName);
            this.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.Name = "ZMCM1044";
            this.Text = "補助科目検索";
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.lblKanaName, 0);
            this.Controls.SetChildIndex(this.txtKanaName, 0);
            this.Controls.SetChildIndex(this.dgvList, 0);
            this.Controls.SetChildIndex(this.lblKnjoKamk, 0);
            this.Controls.SetChildIndex(this.txtKnjoKamkCd, 0);
            this.Controls.SetChildIndex(this.lblKnjoKamok, 0);
            this.pnlDebug.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblKanaName;
        private System.Windows.Forms.TextBox txtKanaName;
        private System.Windows.Forms.DataGridView dgvList;
        private System.Windows.Forms.TextBox txtKnjoKamkCd;
        private System.Windows.Forms.Label lblKnjoKamk;
        private System.Windows.Forms.Label lblKnjoKamok;

    }
}