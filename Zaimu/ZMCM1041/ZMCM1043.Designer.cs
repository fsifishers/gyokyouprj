﻿namespace jp.co.fsi.zm.zmcm1041
{
    partial class ZMCM1043
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtToriSkCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblToriSkCd = new System.Windows.Forms.Label();
            this.pnlMain = new jp.co.fsi.common.FsiPanel();
            this.label6 = new System.Windows.Forms.Label();
            this.txtFax = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDenwa = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtJusho2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtJusho1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtYubin2 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtYubin1 = new jp.co.fsi.common.controls.FsiTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtToriSkKanaNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblToriSkKanaNm = new System.Windows.Forms.Label();
            this.txtToriSkNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblToriSkNm = new System.Windows.Forms.Label();
            this.lblKanjoK = new System.Windows.Forms.Label();
            this.lblKanjoKamokuCd = new System.Windows.Forms.Label();
            this.txtKanjoKamokuCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.pnlDebug.SuspendLayout();
            this.pnlMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(479, 23);
            this.lblTitle.Text = "取引先の登録";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(5, 176);
            this.pnlDebug.Size = new System.Drawing.Size(496, 100);
            // 
            // txtToriSkCd
            // 
            this.txtToriSkCd.AutoSizeFromLength = true;
            this.txtToriSkCd.DisplayLength = null;
            this.txtToriSkCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtToriSkCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtToriSkCd.Location = new System.Drawing.Point(132, 13);
            this.txtToriSkCd.MaxLength = 4;
            this.txtToriSkCd.Name = "txtToriSkCd";
            this.txtToriSkCd.Size = new System.Drawing.Size(48, 20);
            this.txtToriSkCd.TabIndex = 1;
            this.txtToriSkCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtToriSkCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtToriSk_Validating);
            // 
            // lblToriSkCd
            // 
            this.lblToriSkCd.BackColor = System.Drawing.Color.Silver;
            this.lblToriSkCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblToriSkCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblToriSkCd.Location = new System.Drawing.Point(17, 11);
            this.lblToriSkCd.Name = "lblToriSkCd";
            this.lblToriSkCd.Size = new System.Drawing.Size(167, 25);
            this.lblToriSkCd.TabIndex = 1;
            this.lblToriSkCd.Text = "取引先コード";
            this.lblToriSkCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlMain
            // 
            this.pnlMain.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlMain.Controls.Add(this.label6);
            this.pnlMain.Controls.Add(this.txtFax);
            this.pnlMain.Controls.Add(this.txtDenwa);
            this.pnlMain.Controls.Add(this.txtJusho2);
            this.pnlMain.Controls.Add(this.txtJusho1);
            this.pnlMain.Controls.Add(this.label5);
            this.pnlMain.Controls.Add(this.label4);
            this.pnlMain.Controls.Add(this.label3);
            this.pnlMain.Controls.Add(this.label2);
            this.pnlMain.Controls.Add(this.txtYubin2);
            this.pnlMain.Controls.Add(this.txtYubin1);
            this.pnlMain.Controls.Add(this.label1);
            this.pnlMain.Controls.Add(this.txtToriSkKanaNm);
            this.pnlMain.Controls.Add(this.lblToriSkKanaNm);
            this.pnlMain.Controls.Add(this.txtToriSkNm);
            this.pnlMain.Controls.Add(this.lblToriSkNm);
            this.pnlMain.Location = new System.Drawing.Point(12, 39);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(400, 183);
            this.pnlMain.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.Silver;
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label6.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label6.Location = new System.Drawing.Point(147, 53);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(15, 25);
            this.label6.TabIndex = 1000;
            this.label6.Text = "-";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFax
            // 
            this.txtFax.AutoSizeFromLength = false;
            this.txtFax.DisplayLength = null;
            this.txtFax.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFax.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtFax.Location = new System.Drawing.Point(117, 155);
            this.txtFax.MaxLength = 15;
            this.txtFax.Name = "txtFax";
            this.txtFax.Size = new System.Drawing.Size(127, 20);
            this.txtFax.TabIndex = 9;
            this.txtFax.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtFax_KeyDown);
            this.txtFax.Validating += new System.ComponentModel.CancelEventHandler(this.txtFax_Validating);
            // 
            // txtDenwa
            // 
            this.txtDenwa.AutoSizeFromLength = false;
            this.txtDenwa.DisplayLength = null;
            this.txtDenwa.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDenwa.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtDenwa.Location = new System.Drawing.Point(117, 130);
            this.txtDenwa.MaxLength = 15;
            this.txtDenwa.Name = "txtDenwa";
            this.txtDenwa.Size = new System.Drawing.Size(126, 20);
            this.txtDenwa.TabIndex = 8;
            this.txtDenwa.Validating += new System.ComponentModel.CancelEventHandler(this.txtDenwa_Validating);
            // 
            // txtJusho2
            // 
            this.txtJusho2.AutoSizeFromLength = false;
            this.txtJusho2.DisplayLength = null;
            this.txtJusho2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtJusho2.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtJusho2.Location = new System.Drawing.Point(117, 105);
            this.txtJusho2.MaxLength = 30;
            this.txtJusho2.Name = "txtJusho2";
            this.txtJusho2.Size = new System.Drawing.Size(269, 20);
            this.txtJusho2.TabIndex = 7;
            this.txtJusho2.Validating += new System.ComponentModel.CancelEventHandler(this.txtJushoo_Validating);
            // 
            // txtJusho1
            // 
            this.txtJusho1.AutoSizeFromLength = false;
            this.txtJusho1.DisplayLength = null;
            this.txtJusho1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtJusho1.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtJusho1.Location = new System.Drawing.Point(117, 80);
            this.txtJusho1.MaxLength = 30;
            this.txtJusho1.Name = "txtJusho1";
            this.txtJusho1.Size = new System.Drawing.Size(269, 20);
            this.txtJusho1.TabIndex = 6;
            this.txtJusho1.Validating += new System.ComponentModel.CancelEventHandler(this.txtJusho_Validating);
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Silver;
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label5.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label5.Location = new System.Drawing.Point(3, 153);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(244, 25);
            this.label5.TabIndex = 10;
            this.label5.Text = "FAX番号";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Silver;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.Location = new System.Drawing.Point(3, 128);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(244, 25);
            this.label4.TabIndex = 9;
            this.label4.Text = "電話番号";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Silver;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(3, 103);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(387, 25);
            this.label3.TabIndex = 8;
            this.label3.Text = "住所2";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Silver;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(3, 78);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(387, 25);
            this.label2.TabIndex = 7;
            this.label2.Text = "住所1";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtYubin2
            // 
            this.txtYubin2.AutoSizeFromLength = false;
            this.txtYubin2.DisplayLength = null;
            this.txtYubin2.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtYubin2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtYubin2.Location = new System.Drawing.Point(162, 55);
            this.txtYubin2.MaxLength = 4;
            this.txtYubin2.Name = "txtYubin2";
            this.txtYubin2.Size = new System.Drawing.Size(39, 20);
            this.txtYubin2.TabIndex = 5;
            this.txtYubin2.Validating += new System.ComponentModel.CancelEventHandler(this.txtYubinn_Validating);
            // 
            // txtYubin1
            // 
            this.txtYubin1.AutoSizeFromLength = false;
            this.txtYubin1.DisplayLength = null;
            this.txtYubin1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtYubin1.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtYubin1.Location = new System.Drawing.Point(117, 55);
            this.txtYubin1.MaxLength = 3;
            this.txtYubin1.Name = "txtYubin1";
            this.txtYubin1.Size = new System.Drawing.Size(28, 20);
            this.txtYubin1.TabIndex = 4;
            this.txtYubin1.Validating += new System.ComponentModel.CancelEventHandler(this.txtYubin_Validating);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Silver;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(3, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(201, 25);
            this.label1.TabIndex = 4;
            this.label1.Text = "郵便";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtToriSkKanaNm
            // 
            this.txtToriSkKanaNm.AutoSizeFromLength = false;
            this.txtToriSkKanaNm.DisplayLength = null;
            this.txtToriSkKanaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtToriSkKanaNm.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.txtToriSkKanaNm.Location = new System.Drawing.Point(118, 30);
            this.txtToriSkKanaNm.MaxLength = 30;
            this.txtToriSkKanaNm.Name = "txtToriSkKanaNm";
            this.txtToriSkKanaNm.Size = new System.Drawing.Size(269, 20);
            this.txtToriSkKanaNm.TabIndex = 3;
            this.txtToriSkKanaNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtToriSkKanaNm_Validating);
            // 
            // lblToriSkKanaNm
            // 
            this.lblToriSkKanaNm.BackColor = System.Drawing.Color.Silver;
            this.lblToriSkKanaNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblToriSkKanaNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblToriSkKanaNm.Location = new System.Drawing.Point(3, 28);
            this.lblToriSkKanaNm.Name = "lblToriSkKanaNm";
            this.lblToriSkKanaNm.Size = new System.Drawing.Size(387, 25);
            this.lblToriSkKanaNm.TabIndex = 2;
            this.lblToriSkKanaNm.Text = "取引先カナ名";
            this.lblToriSkKanaNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtToriSkNm
            // 
            this.txtToriSkNm.AutoSizeFromLength = false;
            this.txtToriSkNm.DisplayLength = null;
            this.txtToriSkNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtToriSkNm.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtToriSkNm.Location = new System.Drawing.Point(118, 5);
            this.txtToriSkNm.MaxLength = 40;
            this.txtToriSkNm.Name = "txtToriSkNm";
            this.txtToriSkNm.Size = new System.Drawing.Size(269, 20);
            this.txtToriSkNm.TabIndex = 2;
            this.txtToriSkNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtToriSkNm_Validating);
            // 
            // lblToriSkNm
            // 
            this.lblToriSkNm.BackColor = System.Drawing.Color.Silver;
            this.lblToriSkNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblToriSkNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblToriSkNm.Location = new System.Drawing.Point(3, 3);
            this.lblToriSkNm.Name = "lblToriSkNm";
            this.lblToriSkNm.Size = new System.Drawing.Size(387, 25);
            this.lblToriSkNm.TabIndex = 0;
            this.lblToriSkNm.Text = "取引先名";
            this.lblToriSkNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKanjoK
            // 
            this.lblKanjoK.BackColor = System.Drawing.Color.Silver;
            this.lblKanjoK.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKanjoK.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKanjoK.Location = new System.Drawing.Point(183, 11);
            this.lblKanjoK.Name = "lblKanjoK";
            this.lblKanjoK.Size = new System.Drawing.Size(158, 25);
            this.lblKanjoK.TabIndex = 902;
            this.lblKanjoK.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKanjoKamokuCd
            // 
            this.lblKanjoKamokuCd.BackColor = System.Drawing.Color.Silver;
            this.lblKanjoKamokuCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKanjoKamokuCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKanjoKamokuCd.Location = new System.Drawing.Point(340, 11);
            this.lblKanjoKamokuCd.Name = "lblKanjoKamokuCd";
            this.lblKanjoKamokuCd.Size = new System.Drawing.Size(155, 25);
            this.lblKanjoKamokuCd.TabIndex = 904;
            this.lblKanjoKamokuCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKanjoKamokuCd
            // 
            this.txtKanjoKamokuCd.AutoSizeFromLength = true;
            this.txtKanjoKamokuCd.DisplayLength = null;
            this.txtKanjoKamokuCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKanjoKamokuCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtKanjoKamokuCd.Location = new System.Drawing.Point(260, 13);
            this.txtKanjoKamokuCd.MaxLength = 6;
            this.txtKanjoKamokuCd.Name = "txtKanjoKamokuCd";
            this.txtKanjoKamokuCd.Size = new System.Drawing.Size(80, 20);
            this.txtKanjoKamokuCd.TabIndex = 999;
            this.txtKanjoKamokuCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // ZMCM1043
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(504, 279);
            this.Controls.Add(this.txtKanjoKamokuCd);
            this.Controls.Add(this.lblKanjoKamokuCd);
            this.Controls.Add(this.lblKanjoK);
            this.Controls.Add(this.pnlMain);
            this.Controls.Add(this.txtToriSkCd);
            this.Controls.Add(this.lblToriSkCd);
            this.Name = "ZMCM1043";
            this.ShowFButton = true;
            this.Text = "取引先の登録";
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblToriSkCd, 0);
            this.Controls.SetChildIndex(this.txtToriSkCd, 0);
            this.Controls.SetChildIndex(this.pnlMain, 0);
            this.Controls.SetChildIndex(this.lblKanjoK, 0);
            this.Controls.SetChildIndex(this.lblKanjoKamokuCd, 0);
            this.Controls.SetChildIndex(this.txtKanjoKamokuCd, 0);
            this.pnlDebug.ResumeLayout(false);
            this.pnlMain.ResumeLayout(false);
            this.pnlMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private jp.co.fsi.common.controls.FsiTextBox txtToriSkCd;
        private System.Windows.Forms.Label lblToriSkCd;
        private jp.co.fsi.common.FsiPanel pnlMain;
        private jp.co.fsi.common.controls.FsiTextBox txtToriSkNm;
        private System.Windows.Forms.Label lblToriSkNm;
        private System.Windows.Forms.Label lblToriSkKanaNm;
        private jp.co.fsi.common.controls.FsiTextBox txtToriSkKanaNm;
        private System.Windows.Forms.Label lblKanjoK;
        private System.Windows.Forms.Label lblKanjoKamokuCd;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoKamokuCd;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private jp.co.fsi.common.controls.FsiTextBox txtYubin2;
        private jp.co.fsi.common.controls.FsiTextBox txtYubin1;
        private System.Windows.Forms.Label label1;
        private jp.co.fsi.common.controls.FsiTextBox txtFax;
        private jp.co.fsi.common.controls.FsiTextBox txtDenwa;
        private jp.co.fsi.common.controls.FsiTextBox txtJusho2;
        private jp.co.fsi.common.controls.FsiTextBox txtJusho1;
        private System.Windows.Forms.Label label6;
    }
}