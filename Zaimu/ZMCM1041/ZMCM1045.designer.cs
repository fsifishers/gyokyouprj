﻿namespace jp.co.fsi.zm.zmcm1041
{
    partial class ZMCM1045
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbxTokuisakiCd = new System.Windows.Forms.GroupBox();
            this.lblKanjoCdTo = new System.Windows.Forms.Label();
            this.lblCodeBet = new System.Windows.Forms.Label();
            this.txtKanjoCdFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKanjoCdFr = new System.Windows.Forms.Label();
            this.txtKanjoCdTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.pnlDebug.SuspendLayout();
            this.gbxTokuisakiCd.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.BackColor = System.Drawing.Color.MidnightBlue;
            this.lblTitle.Size = new System.Drawing.Size(609, 23);
            this.lblTitle.Text = "補助科目の印刷";
            this.lblTitle.UseWaitCursor = true;
            this.lblTitle.Visible = false;
            // 
            // btnF2
            // 
            this.btnF2.Visible = false;
            // 
            // btnF3
            // 
            this.btnF3.Visible = false;
            // 
            // btnF7
            // 
            this.btnF7.Visible = false;
            // 
            // btnF6
            // 
            this.btnF6.Visible = false;
            // 
            // btnF8
            // 
            this.btnF8.Visible = false;
            // 
            // btnF9
            // 
            this.btnF9.Visible = false;
            // 
            // btnF10
            // 
            this.btnF10.Visible = false;
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(5, 40);
            this.pnlDebug.Size = new System.Drawing.Size(642, 100);
            // 
            // gbxTokuisakiCd
            // 
            this.gbxTokuisakiCd.Controls.Add(this.lblKanjoCdTo);
            this.gbxTokuisakiCd.Controls.Add(this.lblCodeBet);
            this.gbxTokuisakiCd.Controls.Add(this.txtKanjoCdFr);
            this.gbxTokuisakiCd.Controls.Add(this.lblKanjoCdFr);
            this.gbxTokuisakiCd.Controls.Add(this.txtKanjoCdTo);
            this.gbxTokuisakiCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxTokuisakiCd.ForeColor = System.Drawing.Color.Black;
            this.gbxTokuisakiCd.Location = new System.Drawing.Point(8, 8);
            this.gbxTokuisakiCd.Name = "gbxTokuisakiCd";
            this.gbxTokuisakiCd.Size = new System.Drawing.Size(614, 75);
            this.gbxTokuisakiCd.TabIndex = 904;
            this.gbxTokuisakiCd.TabStop = false;
            this.gbxTokuisakiCd.Text = "勘定科目コード範囲";
            // 
            // lblKanjoCdTo
            // 
            this.lblKanjoCdTo.BackColor = System.Drawing.Color.Silver;
            this.lblKanjoCdTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKanjoCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKanjoCdTo.Location = new System.Drawing.Point(357, 32);
            this.lblKanjoCdTo.Name = "lblKanjoCdTo";
            this.lblKanjoCdTo.Size = new System.Drawing.Size(217, 20);
            this.lblKanjoCdTo.TabIndex = 4;
            this.lblKanjoCdTo.Text = "最　後";
            this.lblKanjoCdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCodeBet
            // 
            this.lblCodeBet.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCodeBet.Location = new System.Drawing.Point(285, 31);
            this.lblCodeBet.Name = "lblCodeBet";
            this.lblCodeBet.Size = new System.Drawing.Size(18, 20);
            this.lblCodeBet.TabIndex = 2;
            this.lblCodeBet.Text = "～";
            this.lblCodeBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKanjoCdFr
            // 
            this.txtKanjoCdFr.AutoSizeFromLength = false;
            this.txtKanjoCdFr.DisplayLength = null;
            this.txtKanjoCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKanjoCdFr.Location = new System.Drawing.Point(13, 32);
            this.txtKanjoCdFr.MaxLength = 6;
            this.txtKanjoCdFr.Name = "txtKanjoCdFr";
            this.txtKanjoCdFr.Size = new System.Drawing.Size(50, 20);
            this.txtKanjoCdFr.TabIndex = 2;
            this.txtKanjoCdFr.Text = "123456";
            this.txtKanjoCdFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKanjoCdFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtKanjoCdFr_Validating);
            // 
            // lblKanjoCdFr
            // 
            this.lblKanjoCdFr.BackColor = System.Drawing.Color.Silver;
            this.lblKanjoCdFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKanjoCdFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKanjoCdFr.Location = new System.Drawing.Point(65, 32);
            this.lblKanjoCdFr.Name = "lblKanjoCdFr";
            this.lblKanjoCdFr.Size = new System.Drawing.Size(217, 20);
            this.lblKanjoCdFr.TabIndex = 1;
            this.lblKanjoCdFr.Text = "先　頭";
            this.lblKanjoCdFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKanjoCdTo
            // 
            this.txtKanjoCdTo.AutoSizeFromLength = false;
            this.txtKanjoCdTo.DisplayLength = null;
            this.txtKanjoCdTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKanjoCdTo.Location = new System.Drawing.Point(306, 32);
            this.txtKanjoCdTo.MaxLength = 6;
            this.txtKanjoCdTo.Name = "txtKanjoCdTo";
            this.txtKanjoCdTo.Size = new System.Drawing.Size(50, 20);
            this.txtKanjoCdTo.TabIndex = 3;
            this.txtKanjoCdTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKanjoCdTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtKanjoCdTo_KeyDown);
            this.txtKanjoCdTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtKanjoCdTo_Validating);
            // 
            // ZMCM1045
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(634, 143);
            this.Controls.Add(this.gbxTokuisakiCd);
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "ZMCM1045";
            this.ShowFButton = true;
            this.Text = "補助科目の印刷";
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.gbxTokuisakiCd, 0);
            this.pnlDebug.ResumeLayout(false);
            this.gbxTokuisakiCd.ResumeLayout(false);
            this.gbxTokuisakiCd.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbxTokuisakiCd;
        private System.Windows.Forms.Label lblKanjoCdTo;
        private System.Windows.Forms.Label lblCodeBet;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoCdFr;
        private System.Windows.Forms.Label lblKanjoCdFr;
        private jp.co.fsi.common.controls.FsiTextBox txtKanjoCdTo;

    }
}