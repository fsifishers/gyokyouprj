﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Reflection;
using System.Windows.Forms;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.zm.zmcm1041
{
    /// <summary>
    /// 補助科目の登録(ZMCM1042)
    /// </summary>
    public partial class ZMCM1042 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(新規)
        /// </summary>
        private const string MODE_NEW = "1";

        /// <summary>
        /// モード(編集)
        /// </summary>
        private const string MODE_EDIT = "2";
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ZMCM1042()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="par1">引数1</param>
        public ZMCM1042(string par1)
            : base(par1)
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // 引数：Par1／モード(1:新規、2:変更)、InData：補助科目コード
            if (MODE_NEW.Equals(this.Par1))
            {
                // 新規モードの初期表示
                InitDispOnNew();
                this.btnF3.Enabled = false;
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 編集モードの初期表示
                InitDispOnEdit();
            }
            else
            {
                // 不正な起動として閉じる
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }

            // タイトルは非表示
            this.lblTitle.Visible = false;
            // EscapeとF1のみ表示
            this.ShowFButton = true;
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF1.Location = this.btnF2.Location;
            this.btnF6.Location = this.btnF4.Location;
            this.btnF2.Visible = false;
            this.btnF3.Visible = true;
            this.btnF4.Visible = false;
            this.btnF5.Visible = false;
            this.btnF6.Visible = true;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }
        
        /// <summary>
        /// F3キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF3();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF3()
        {
            if (!this.btnF3.Enabled)
                return;

            Array indata = (Array)InData;

            // 新規モードの場合は処理させない
            if (this.Par1.Equals(MODE_NEW)) return;
            // 削除する前に再度コードの存在チェック
            if (IsCodeUsing())
            {
                Msg.Error("補助科目コードが使用されているため、削除できません。");
                return;
            }
            if (Msg.ConfYesNo("削除しますか？") == DialogResult.No)
            {
                // 「いいえ」を押されたら処理終了
                return;
            }
            // 削除処理
            try
            {
                // データ削除
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
                // 外部から支所コード渡しの時
                if (!ValChk.IsEmpty(this.Par2))
                {
                    dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.Par2);
                }
                else
                {
                    dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.UInfo.ShishoCd);
                }
                dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 6, this.UInfo.KaikeiNendo);
                dpc.SetParam("@HOJO_KAMOKU_CD", SqlDbType.Decimal, 10, Util.ToString(indata.GetValue(1)));
                dpc.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, this.txtKanjoKamokuCd.Text);
                this.Dba.Delete("TB_ZM_HOJO_KAMOKU", "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND KAIKEI_NENDO = @KAIKEI_NENDO AND KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD AND HOJO_KAMOKU_CD = @HOJO_KAMOKU_CD", dpc);

                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// F6キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF6();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF6()
        {
            if (!this.btnF6.Enabled)
                return;

            // 確認メッセージを表示
            string msg = (MODE_NEW.Equals(this.Par1) ? "登録" : "更新") + "しますか？";
            if (Msg.ConfYesNo(msg) == DialogResult.No)
            {
                this.txtHojoKanaNm.Focus();
                // 「いいえ」を押されたら処理終了
                return;
            }

            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            // 入力値をバインドパラメータとしてセットする
            ArrayList alParamsZmhojo = SetZmHojoParams();

            try
            {
                this.Dba.BeginTransaction();

                if (MODE_NEW.Equals(this.Par1))
                {
                    if (IsCodeExists())
                    {
                        Msg.Error("補助科目情報が登録済みです。");
                        return;
                    }

                    // データ登録
                    // 補助科目
                    this.Dba.Insert("TB_ZM_HOJO_KAMOKU", (DbParamCollection)alParamsZmhojo[0]);
                }
                else if (MODE_EDIT.Equals(this.Par1))
                {
                    
                    // データ更新
                    // 補助科目
                    this.Dba.Update("TB_ZM_HOJO_KAMOKU",
                        (DbParamCollection)alParamsZmhojo[1],
                        "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD AND HOJO_KAMOKU_CD = @HOJO_KAMOKU_CD AND KAIKEI_NENDO = @KAIKEI_NENDO",
                        (DbParamCollection)alParamsZmhojo[0]);
                }
                // トランザクションをコミット
                this.Dba.Commit();
            }
            finally
            {
                // ロールバック
                this.Dba.Rollback();
            }

            // DialogResultに「OK」をセットし結果を返却
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        #endregion

        #region イベント
        /// <summary>
        /// 補助科目コードの検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtkanjoCd_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidhojoCd())
            {
                e.Cancel = true;
                this.txtHojoCd.SelectAll();
            }
        }

        /// <summary>
        /// 補助科目名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtkanjoNm_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidhojoKanaNm())
            {
                e.Cancel = true;
                this.txtHojoNm.SelectAll();
            }
        }

        /// <summary>
        /// 補助科目カナ名の検証
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtkanjoKanaNm_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidhojoKanaNm())
            {
                e.Cancel = true;
                this.txtHojoKanaNm.SelectAll();
            }
        }

        /// <summary>
        /// 補助科目カナ名Enter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtHojoKanaNm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.PressF6();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 新規モードの初期表示
        /// </summary>
        private void InitDispOnNew()
        {
            Array indata = (Array)InData;

            // 初期値、入力制御を実装

            // 補助科目コードの初期値を取得
            // 補助科目の中でのMAX+1を初期表示する
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            // 外部から支所コード渡しの時
            if (!ValChk.IsEmpty(this.Par2))
            {
                dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.Par2);
            }
            else
            {
                dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.UInfo.ShishoCd);
            }
            dpc.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, Util.ToString(indata.GetValue(0)));
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 6, this.UInfo.KaikeiNendo);
            StringBuilder where = new StringBuilder();
            where.Append(" KAISHA_CD = @KAISHA_CD");
            where.Append(" AND SHISHO_CD = @SHISHO_CD");
            where.Append(" AND KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD");
            where.Append(" AND KAIKEI_NENDO = @KAIKEI_NENDO");

            DataTable dtMaxhojo =
                this.Dba.GetDataTableByConditionWithParams("MAX(HOJO_KAMOKU_CD) AS MAX_CD",
                    "TB_ZM_HOJO_KAMOKU", Util.ToString(where), dpc);
            if (dtMaxhojo.Rows.Count > 0 && !ValChk.IsEmpty(dtMaxhojo.Rows[0]["MAX_CD"]))
            {
                this.txtHojoCd.Text = Util.ToString(Util.ToInt(dtMaxhojo.Rows[0]["MAX_CD"]) + 1);
            }
            else
            {
                this.txtHojoCd.Text = "1";
            }


            this.txtKanjoKamokuCd.Text = Util.ToString(indata.GetValue(0));
            this.lblKanjoK.Text = " 勘定科目 ";
            this.lblKanjoKamokuCd.Text = this.Dba.GetName(this.UInfo, "TB_ZM_KANJO_KAMOKU", this.UInfo.ShishoCd, this.txtKanjoKamokuCd.Text);
            this.txtKanjoKamokuCd.Enabled = false;
        }

        /// <summary>
        /// 編集モードの初期表示
        /// </summary>
        private void InitDispOnEdit()
        {
            Array indata = (Array)InData;

            // 現在DBに登録されている値、入力制御を実装
            StringBuilder cols = new StringBuilder();
            cols.Append("A.KAISHA_CD");
            cols.Append(" ,A.KANJO_KAMOKU_CD");
            cols.Append(" ,A.HOJO_KAMOKU_CD");
            cols.Append(" ,A.HOJO_KAMOKU_NM");
            cols.Append(" ,A.HOJO_KAMOKU_KANA_NM");

            StringBuilder from = new StringBuilder();
            from.Append("TB_ZM_HOJO_KAMOKU AS A");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            // 外部から支所コード渡しの時
            if (!ValChk.IsEmpty(this.Par2))
            {
                dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.Par2);
            }
            else
            {
                dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.UInfo.ShishoCd);
            }
            dpc.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, Util.ToString(indata.GetValue(0)));
            dpc.SetParam("@HOJO_KAMOKU_CD", SqlDbType.Decimal, 10, Util.ToString(indata.GetValue(1)));
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 6, this.UInfo.KaikeiNendo);

            DataTable dtDispData =
                this.Dba.GetDataTableByConditionWithParams(
                    Util.ToString(cols), Util.ToString(from),
                    "A.KAISHA_CD = @KAISHA_CD AND A.SHISHO_CD = @SHISHO_CD AND A.KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD AND A.HOJO_KAMOKU_CD = @HOJO_KAMOKU_CD AND A.KAIKEI_NENDO = @KAIKEI_NENDO",
                    dpc);

            if (dtDispData.Rows.Count == 0)
            {
                Msg.Error("不正な起動です。終了します。");
                this.Close();
            }

            // 取得した内容を表示
            DataRow drDispData = dtDispData.Rows[0];
            this.txtHojoCd.Text = Util.ToString(drDispData["HOJO_KAMOKU_CD"]);
            this.txtHojoNm.Text = Util.ToString(drDispData["HOJO_KAMOKU_NM"]);
            this.txtHojoKanaNm.Text = Util.ToString(drDispData["HOJO_KAMOKU_KANA_NM"]);
            this.lblKanjoK.Text = " 勘定科目 ";
            this.txtKanjoKamokuCd.Text = Util.ToString(drDispData["KANJO_KAMOKU_CD"]);
            this.lblKanjoKamokuCd.Text = this.Dba.GetName(this.UInfo, "TB_ZM_KANJO_KAMOKU", this.UInfo.ShishoCd, this.txtKanjoKamokuCd.Text);
            //　勘定科目コード入力不可　
            this.txtHojoCd.Enabled = false;
            this.txtKanjoKamokuCd.Enabled = false;
            if (IsCodeUsing())
            {
                // 使用されているので削除不可
                this.btnF3.Enabled = false;
            }
            else
            {
                // 使用されていないので削除OK
                this.btnF3.Enabled = true;
            }
        }

        /// <summary>
        /// 補助科目コードの入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidhojoCd()
        {
            // 入力が空の場合にエラー表示
            if (ValChk.IsEmpty(this.txtHojoCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }
            // 数字のみの入力を許可
            if (!ValChk.IsNumber(this.txtHojoCd.Text))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            // 既に存在するコードを入力した場合はエラーとする
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 6, this.UInfo.KaishaCd);
            // 外部から支所コード渡しの時
            if (!ValChk.IsEmpty(this.Par2))
            {
                dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.Par2);
            }
            else
            {
                dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.UInfo.ShishoCd);
            }
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 6, this.UInfo.KaikeiNendo);
            dpc.SetParam("@HOJO_KAMOKU_CD", SqlDbType.Decimal, 10, this.txtHojoCd.Text);
            dpc.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, this.txtKanjoKamokuCd.Text);
            StringBuilder where = new StringBuilder("KAISHA_CD = @KAISHA_CD");
            where.Append(" AND SHISHO_CD = @SHISHO_CD");
            where.Append(" AND KAIKEI_NENDO = @KAIKEI_NENDO");
            where.Append(" AND KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD");
            where.Append(" AND HOJO_KAMOKU_CD = @HOJO_KAMOKU_CD");
            DataTable dtkanjo =
                this.Dba.GetDataTableByConditionWithParams("HOJO_KAMOKU_CD",
                    "TB_ZM_HOJO_KAMOKU", Util.ToString(where), dpc);
            if (dtkanjo.Rows.Count > 0)
            {
                Msg.Error("既に存在する補助科目コードと重複しています。");
                return false;
            }
            return true;
        }

        /// <summary>
        /// 補助科目名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidhojoNm()
        {
            // 40バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtHojoNm.Text, this.txtHojoNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 補助科目カナ名の入力チェック
        /// </summary>
        /// <returns>true:OK、false:NG</returns>
        private bool IsValidhojoKanaNm()
        {
            // 30バイトを超えていたらエラー
            if (!ValChk.IsWithinLength(this.txtHojoKanaNm.Text, this.txtHojoKanaNm.MaxLength))
            {
                Msg.Error("入力に誤りがあります。");
                return false;
            }

            return true;
        }
        
        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            if (MODE_NEW.Equals(this.Par1))
            {
                // 補助科目コードのチェック
                if (!IsValidhojoCd())
                {
                    this.txtHojoCd.Focus();
                    return false;
                }
            }

            // 補助科目名のチェック
            if (!IsValidhojoNm())
            {
                this.txtHojoNm.Focus();
                return false;
            }

            // 補助科目カナ名のチェック
            if (!IsValidhojoKanaNm())
            {
                this.txtHojoKanaNm.Focus();
                return false;
            }
            return true;
        }

        /// <summary>
        /// TB_ZM_HOJO_KAMOKUに更新するためのパラメータ設定をします。
        /// </summary>
        /// <returns>
        /// 設定されたパラメータ情報のリスト
        /// 登録処理：DbParamCollection*1
        /// 更新処理：DbParamCollection*2(Where句,Set句)
        /// </returns>
        private ArrayList SetZmHojoParams()
        {
            ArrayList alParams = new ArrayList();
            DbParamCollection updParam = new DbParamCollection();

            if (MODE_NEW.Equals(this.Par1))
            {
                // 会社コードと勘定科目コードと補助科目コードと会計年度を更新パラメータに設定
                updParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                // 外部から支所コード渡しの時
                if (!ValChk.IsEmpty(this.Par2))
                {
                    updParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.Par2);
                }
                else
                {
                    updParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.UInfo.ShishoCd);
                }
                updParam.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, this.txtKanjoKamokuCd.Text);
                updParam.SetParam("@HOJO_KAMOKU_CD", SqlDbType.Decimal, 10, this.txtHojoCd.Text);
                updParam.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 6, this.UInfo.KaikeiNendo);
                // 登録日
                updParam.SetParam("@REGIST_DATE", SqlDbType.DateTime, "@NOWTIME");
            }
            else if (MODE_EDIT.Equals(this.Par1))
            {
                // 会社コードと勘定科目コードと補助科目コードと会計年度をWhere句のパラメータに設定
                DbParamCollection whereParam = new DbParamCollection();
                whereParam.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
                // 外部から支所コード渡しの時
                if (!ValChk.IsEmpty(this.Par2))
                {
                    whereParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.Par2);
                }
                else
                {
                    whereParam.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.UInfo.ShishoCd);
                }
                whereParam.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, this.txtKanjoKamokuCd.Text);
                whereParam.SetParam("@HOJO_KAMOKU_CD", SqlDbType.Decimal, 10, this.txtHojoCd.Text);
                whereParam.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 6, this.UInfo.KaikeiNendo);
                alParams.Add(whereParam);
            }

            // 補助科目名
            updParam.SetParam("@HOJO_KAMOKU_NM", SqlDbType.VarChar, 40, this.txtHojoNm.Text);
            // 補助科目カナ名
            updParam.SetParam("@HOJO_KAMOKU_KANA_NM", SqlDbType.VarChar, 40, this.txtHojoKanaNm.Text);
            // 更新日
            updParam.SetParam("@UPDATE_DATE", SqlDbType.DateTime, "@NOWTIME");
            alParams.Add(updParam);

            return alParams;
        }

        /// <summary>
        /// 対象の補助科目コードがマスタ登録されているかどうかをチェックします。
        /// </summary>
        /// <returns>true:使用されている/false:使用されていない</returns>
        private bool IsCodeExists()
        {
            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 2, this.UInfo.KaishaCd);
            // 外部から支所コード渡しの時
            if (!ValChk.IsEmpty(this.Par2))
            {
                dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.Par2);
            }
            else
            {
                dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.UInfo.ShishoCd);
            }
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 6, this.UInfo.KaikeiNendo);
            dpc.SetParam("@HOJO_KAMOKU_CD", SqlDbType.Decimal, 10, this.txtHojoCd.Text);
            dpc.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, this.txtKanjoKamokuCd.Text);
            DataTable dtZmhojokamoku = this.Dba.GetDataTableByConditionWithParams("*", "TB_ZM_HOJO_KAMOKU", "KAISHA_CD = @KAISHA_CD AND SHISHO_CD = @SHISHO_CD AND KAIKEI_NENDO = @KAIKEI_NENDO AND KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD AND HOJO_KAMOKU_CD = @HOJO_KAMOKU_CD", dpc);

            if (dtZmhojokamoku.Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 対象の勘定科目コードがトランテーブルで使用されているかどうかをチェックします。
        /// </summary>
        /// <returns>true:使用されている/false:使用されていない</returns>
        private bool IsCodeUsing()
        {
            Array indata = (Array)InData;

            // 削除可否チェック
            // 指定した勘定科目コードが使用されているかどうかをチェック
            // 仕訳明細
            StringBuilder from = new StringBuilder();
            from.Append(" TB_ZM_SHIWAKE_MEISAI ");

            StringBuilder where = new StringBuilder();
            where.Append(" KAISHA_CD = @KAISHA_CD");
            where.Append(" AND SHISHO_CD = @SHISHO_CD");
            where.Append(" AND KAIKEI_NENDO = @KAIKEI_NENDO");
            where.Append(" AND KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD");
            where.Append(" AND HOJO_KAMOKU_CD = @HOJO_KAMOKU_CD");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            // 外部から支所コード渡しの時
            if (!ValChk.IsEmpty(this.Par2))
            {
                dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.Par2);
            }
            else
            {
                dpc.SetParam("@SHISHO_CD", SqlDbType.Decimal, 4, this.UInfo.ShishoCd);
            }
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 6, this.UInfo.KaikeiNendo);
            dpc.SetParam("@HOJO_KAMOKU_CD", SqlDbType.Decimal, 10, Util.ToString(indata.GetValue(1)));
            dpc.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, this.txtKanjoKamokuCd.Text);

            DataTable dtSwkMsi = this.Dba.GetDataTableByConditionWithParams("HOJO_KAMOKU_CD", from.ToString(), where.ToString(), dpc);

            if (dtSwkMsi.Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion
    }
}
