﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.zm.zmmr1031
{
    /// <summary>
    /// 合計残高試算表(ZMMR1032)
    /// </summary>
    public partial class ZMMR1032 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// タブコントロールindex
        /// </summary>
        private const int LIST_F1 = 1;
        private const int LIST_F2 = 2;
        private const int LIST_F3 = 3;
        #endregion

        #region private変数
        /// <summary>
        /// ZAMR2022(条件画面)のオブジェクト(設定内容の取得のため)
        /// </summary>
        ZMMR1031 _pForm;

        /// <summary>
        /// DataGridView表示有無のFlag
        /// </summary>
        int _viewFlag = 0;

        int _prevList = 0;
        #endregion

        #region プロパティ
        /// <summary>
        /// 勘定科目毎金額格納するGridView用データテーブル
        /// </summary>
        private DataTable _dtKanjoKamokuKingaku = new DataTable();
        public DataTable KanjoKamokuKingaku
        {
            get
            {
                return this._dtKanjoKamokuKingaku;
            }
        }

        /// <summary>
        /// F6貸借対照表GridView用データテーブル
        /// </summary>
        private DataTable _dtF8 = new DataTable();
        public DataTable F8
        {
            get
            {
                return this._dtF8;
            }
        }

        /// <summary>
        /// 貸借対照表の法定準備金額保持用配列
        /// </summary>
        private Decimal[] _dtTaishakuHoteiJunbikin = new Decimal[1];
        public Decimal[] TaishakuHoteiJunbikin
        {
            get
            {
                return this._dtTaishakuHoteiJunbikin;
            }
        }

        /// <summary>
        /// 損益計算書の当期未処分剰余金、当残金額保持用配列
        /// </summary>
        private Decimal[] _dtSonekiMishobunZenzan = new Decimal[1];
        public Decimal[] tokiMishobunzenZanKingakuSoneki
        {
            get
            {
                return this._dtSonekiMishobunZenzan;
            }
        }

        /// <summary>
        /// F7損益計算書GridView用データテーブル
        /// </summary>
        private DataTable _dtF9 = new DataTable();
        public DataTable F9
        {
            get
            {
                return this._dtF9;
            }
        }

        /// <summary>
        /// F8製造原価GridView用データテーブル
        /// </summary>
        private DataTable _dtF10 = new DataTable();
        public DataTable F10
        {
            get
            {
                return this._dtF10;
            }
        }

        /// <summary>
        /// 法定準備金保持用変数
        /// </summary>
        private DateTime _dtKikanKaishibi;
        public DateTime kikanKaishibi
        {
            get
            {
                return this._dtKikanKaishibi;
            }
        }
        private Decimal _dtHoteJunbikin = 0;
        public Decimal hoteJunbikin
        {
            get
            {
                return this._dtHoteJunbikin;
            }
        }

        /// <summary>
        /// 当期未処分剰余金保持用変数
        /// </summary>
        private Decimal _dtTokiMishobunJoyokin = 0;
        public Decimal tokiMishobunJoyokin
        {
            get
            {
                return this._dtTokiMishobunJoyokin;
            }
        }

        /// <summary>
        /// 新規表示or再表示判断用変数
        /// </summary>
        private int _judgementFlg = new int();
        public int JudgementFlg
        {
            get
            {
                return this._judgementFlg;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ZMMR1032(ZMMR1031 frm)
        {
            this._pForm = frm;

            InitializeComponent();
            BindGotFocusEvent();

            // 新規表示に設定
            this._judgementFlg = 1;
        }
        #endregion

        #region メソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // タイトルは非表示
            this.lblTitle.Visible = false;

            this.ShowFButton = false;

            if (!DispSetKikan())
            {
                return;
            }

            // 設定の読み込み
            try
            {
                // 元帳ボタン表示制御
                this._prevList = Util.ToInt(this.Config.LoadPgConfig(Constants.SubSys.Zai, "ZMMR1031", "Setting", "PreviewList"));
            }
            catch (Exception ex)
            {
                this._prevList = 0;
                Console.WriteLine(ex.Message);
            }

            // 更新中メッセージ表示
            ZMMR1034 msgFrm = new ZMMR1034();
            msgFrm.Show();
            msgFrm.Refresh();

            // 画面の初期表示
            if (this.JudgementFlg == 1)
                InitDetailArea();
            DispDataSet();
            DispData(F8, LIST_F1);
            this.btnF8.Enabled = false;
            _viewFlag = 1;
            // DataGridView表示有無のFlag判断
            if (_viewFlag == 0)
            {
                DispData(F9, LIST_F2);
                // F7、F8キーを押下不可に設定
                this.btnF9.Enabled = false;
                this.btnF10.Enabled = false;

            }
            // 金額がｾﾞﾛの科目を印字する場合
            string injiHandan = Util.ToString(this._pForm.Condition["Inji"]);
            if (injiHandan == "no")
            {
                this.btnF10.Enabled = false;
            }
            // フォーカス設定
            this.mtbList.Focus();

            //// ボタンの表示非表示を設定
            if (this._prevList != 0)
            {
                this.btnF11.Location = this.btnF9.Location;
                this.btnF12.Location = this.btnF10.Location;
            }
            this.btnF10.Location = this.btnF8.Location;
            this.btnF9.Location = this.btnF7.Location;
            this.btnF8.Location = this.btnF6.Location;
            this.btnF7.Location = this.btnF5.Location;
            this.btnF6.Location = this.btnF4.Location;
            this.btnF5.Location = this.btnF3.Location;
            this.btnF4.Location = this.btnF2.Location;
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF1.Visible = false;
            this.btnF2.Visible = false;
            this.btnF3.Visible = false;
            this.btnF9.Visible = true;
            this.btnF10.Visible = true;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;
            if (this._prevList != 0)
            {
                this.btnF11.Visible = true;
                this.btnF12.Visible = true;
            }

            this.ShowFButton = true;
            msgFrm.Close();
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        public override void PressF4()
        {
            if (Msg.ConfNmYesNo("プレビュー", "実行しますか？") == DialogResult.Yes)
            {
                // プレビュー処理
                ZMMR1031PR pr = new ZMMR1031PR(this.UInfo, this.Dba, this.Config, this.UnqId, this._pForm);
                pr.DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        public override void PressF5()
        {
            if (Msg.ConfYesNo("実行しますか？") == DialogResult.Yes)
            {
                //印刷処理
                ZMMR1031PR pr = new ZMMR1031PR(this.UInfo, this.Dba, this.Config, this.UnqId, this._pForm);
                pr.DoPrint(false);
            }
        }

        /// <summary>
        /// F6キー押下時処理
        /// PDF出力
        /// </summary>
        public override void PressF6()
        {
            if (Msg.ConfNmYesNo("PDF出力", "実行しますか？") == DialogResult.Yes)
            {
                // プレビュー処理
                ZMMR1031PR pr = new ZMMR1031PR(this.UInfo, this.Dba, this.Config, this.UnqId, this._pForm);
                pr.DoPrint(true, true);
            }
        }

        /// <summary>
        /// F7キー押下時処理
        /// EXCEL出力
        /// </summary>
        public override void PressF7()
        {
            if (Msg.ConfNmYesNo("EXCEL出力", "実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                ZMMR1031PR pr = new ZMMR1031PR(this.UInfo, this.Dba, this.Config, this.UnqId, this._pForm);
                pr.DoPrint(false, false, true);
            }
        }

        /// <summary>
        /// F8キー押下時処理
        /// </summary>
        public override void PressF8()
        {
            DispData(F8, LIST_F1);
            // F6キーを押下不可に設定
            this.btnF8.Enabled = false;
            // F7,F8キーを押下可に設定
            this.btnF9.Enabled = true;
            this.btnF10.Enabled = true;
            // 金額がｾﾞﾛの科目を印字する場合
            string injiHandan = Util.ToString(this._pForm.Condition["Inji"]);
            if (injiHandan == "no")
            {
                this.btnF10.Enabled = false;
            }
        }

        /// <summary>
        /// F9キー押下時処理
        /// </summary>
        public override void PressF9()
        {
            DispData(F9, LIST_F2);
            // F7キーを押下不可に設定
            this.btnF9.Enabled = false;
            // F6,F8キーを押下可に設定
            this.btnF8.Enabled = true;
            this.btnF10.Enabled = true;
            // 金額がｾﾞﾛの科目を印字する場合
            string injiHandan = Util.ToString(this._pForm.Condition["Inji"]);
            if (injiHandan == "no")
            {
                this.btnF10.Enabled = false;
            }
        }

        /// <summary>
        /// F11キー押下時処理
        /// </summary>
        public override void PressF10()
        {
            DispData(F10, LIST_F3);
            // F8キーを押下不可に設定
            this.btnF10.Enabled = false;
            // F6,F7キーを押下可に設定
            this.btnF8.Enabled = true;
            this.btnF9.Enabled = true;
        }

        /// <summary>
        /// F11キー押下時処理
        /// </summary>
        public override void PressF11()
        {
            #region 総勘定元帳表示
            if (this._prevList != 0)
            {
                if (!this.btnF11.Enabled)
                    return;
            }

            int shishoCd = Util.ToInt(Util.ToString(this._pForm.Condition["ShishoCode"]));

            // 支所合算の場合は不可
            if (shishoCd != 0)
            {
                string st = Util.ToString(this.mtbList.SelectedRows[0].Cells["startKmkCd"].Value);
                string ed = Util.ToString(this.mtbList.SelectedRows[0].Cells["endKmkCd"].Value);
                string li = Util.ToString(this.mtbList.SelectedRows[0].Cells["listKmkCd"].Value).Replace(",-1", "");

                decimal kari = Util.ToDecimal(Util.ToString(this.mtbList.SelectedRows[0].Cells["KarikataHassei"].Value));
                decimal kasi = Util.ToDecimal(Util.ToString(this.mtbList.SelectedRows[0].Cells["KashikataHassei"].Value));

                if (st != "" && (kari != 0 || kasi != 0))
                {
                    DataTable dt = GetKanjoKamoku(li);
                    if (dt.Rows.Count > 0)
                    {
                        Assembly asm;
                        Type t;
                        // ココでは総勘定元帳
                        asm = Assembly.LoadFrom("ZMMR1011.exe");
                        t = asm.GetType("jp.co.fsi.zm.zmmr1011.ZMMR1011");
                        if (t != null)
                        {
                            Object obj = Activator.CreateInstance(t);
                            if (obj != null)
                            {
                                string[] inData = new string[6];
                                inData[0] = Util.ToString(shishoCd);
                                inData[1] = Util.ToDate(this._pForm.Condition["DtFr"]).ToString("yyyy/MM/dd");
                                inData[2] = Util.ToDate(this._pForm.Condition["DtTo"]).ToString("yyyy/MM/dd"); ;
                                inData[3] = st;
                                inData[4] = ed;
                                inData[5] = li;

                                // タブの一部として埋め込む
                                BasePgForm frm = (BasePgForm)obj;
                                frm.InData = inData;
                                frm.ShowDialog(this);

                                if (frm.DialogResult == DialogResult.OK)
                                {
                                    // 再表示に設定
                                    this._judgementFlg = 2;

                                    // 画面を再読み込み
                                    InitForm();
                                    shown();
                                }
                            }
                        }
                    }
                }
            }
            #endregion
        }

        /// <summary>
        /// F12キー押下時処理
        /// </summary>
        public override void PressF12()
        {
            #region 補助元帳表示（勘定科目が単一設定のみで補助科目が有りの場合）
            if (this._prevList != 0)
            {
                if (!this.btnF12.Enabled)
                    return;
            }

            int shishoCd = Util.ToInt(Util.ToString(this._pForm.Condition["ShishoCode"]));

            // 支所合算の場合は不可
            if (shishoCd != 0)
            {
                string st = Util.ToString(this.mtbList.SelectedRows[0].Cells["startKmkCd"].Value);
                string ed = Util.ToString(this.mtbList.SelectedRows[0].Cells["endKmkCd"].Value);

                decimal kari = Util.ToDecimal(Util.ToString(this.mtbList.SelectedRows[0].Cells["KarikataHassei"].Value));
                decimal kasi = Util.ToDecimal(Util.ToString(this.mtbList.SelectedRows[0].Cells["KashikataHassei"].Value));

                if (st != "" && (kari != 0 || kasi != 0))
                {
                    if (st == ed)
                    {
                        DataTable dt = GetKanjoKamoku(Util.ToInt(st));
                        if (dt.Rows.Count > 0)
                        {
                            Assembly asm;
                            Type t = null;
                            if (Util.ToInt(dt.Rows[0]["HOJO_KAMOKU_UMU"]) == 1)
                            {
                                // 補助ありの場合は補助元帳
                                asm = Assembly.LoadFrom("ZMMR1021.exe");
                                t = asm.GetType("jp.co.fsi.zm.zmmr1021.ZMMR1021");
                            }
                            if (t != null)
                            {
                                Object obj = Activator.CreateInstance(t);
                                if (obj != null)
                                {
                                    string[] inData = new string[5];
                                    inData[0] = Util.ToString(shishoCd);
                                    inData[1] = Util.ToDate(this._pForm.Condition["DtFr"]).ToString("yyyy/MM/dd");
                                    inData[2] = Util.ToDate(this._pForm.Condition["DtTo"]).ToString("yyyy/MM/dd");
                                    inData[3] = st;
                                    inData[4] = ed;

                                    // タブの一部として埋め込む
                                    BasePgForm frm = (BasePgForm)obj;
                                    frm.InData = inData;
                                    frm.ShowDialog(this);

                                    if (frm.DialogResult == DialogResult.OK)
                                    {
                                        // 再表示に設定
                                        this._judgementFlg = 2;

                                        // 画面を再読み込み
                                        InitForm();
                                        shown();
                                    }
                                }
                            }
                        }
                    }
                }

            }
            #endregion
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 明細部の初期化
        /// </summary>
        private void InitDetailArea()
        {
            // 部門範囲を設定
            // 開始と終了が未設定の場合は、【全社】を表示
            string bumonNmFr = Util.ToString(this._pForm.Condition["BumonNmFr"]);
            string bumonNmTo = Util.ToString(this._pForm.Condition["BumonNmTo"]);
            if (bumonNmFr == "先　頭" && bumonNmTo == "最　後")
            {
                this.lblKaishaSettei.Text = "【全社】";
            }
            else
            {
                this.lblKaishaSettei.Text = bumonNmFr +  "　～　" + bumonNmTo;
            }

            // 期間の初期表示
            string tmpTerm = " ";
            string[] aryJpDateFr = Util.ConvJpDate(Util.ToDate(this._pForm.Condition["DtFr"]), this.Dba);
            tmpTerm += aryJpDateFr[5];
            string[] aryJpDateTo = Util.ConvJpDate(Util.ToDate(this._pForm.Condition["DtTo"]), this.Dba);
            tmpTerm += " ～ " + aryJpDateTo[5];
            this.lblJp.Text = tmpTerm;

            // 【税込みor税抜き】の初期表示
            this.lblZei.Text = Util.ToString(this._pForm.Condition["ShohizeiShori"]);

            #region  GridViewの残高カラム名切り替え
            // 現在設定されている会計年度を取得
            DateTime kaikeiNendoKaishiBi = Util.ToDate(this.UInfo.KaikeiSettings["KAIKEI_KIKAN_KAISHIBI"]);
            // 取得したデータを和暦に変換
            string[] warekiKaikeiNendoKaishiBiDate = Util.ConvJpDate(kaikeiNendoKaishiBi, this.Dba);

            // 期間の開始日付が設定会計年度開始日付と一致する場合、期首残高を設定
            if (aryJpDateFr[6] == warekiKaikeiNendoKaishiBiDate[6])
            {
                mtbList.Columns[1].HeaderText = "期首残高";
            }
            // 期間の開始日が1日の場合、前月残高を設定
            else if (aryJpDateFr[4] == "1")
            {
                mtbList.Columns[1].HeaderText = "前月残高";
            }
            // 期間の開始日付が上記以外だった場合、前日残高を設定
            else
            {
                mtbList.Columns[1].HeaderText = "前日残高";
            }
            #endregion

            // セル内文字の配置設定
            this.mtbList.Columns["Zandaka"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.mtbList.Columns["KarikataHassei"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.mtbList.Columns["KashikataHassei"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.mtbList.Columns["TouZan"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            // 科目名で幅埋め
            this.mtbList.Columns["KamokuNm"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }

        /// <summary>
        /// 保持用データをセット
        /// </summary>
        private Boolean DispSetKikan()
        {
            // 該当会計年度の会計期間開始日を取得
            DataTable dt = Util.GetKaikeiKikan(this.UInfo.KaikeiNendo, this.Dba);
            if (dt.Rows.Count == 0)
            {
                Msg.Info("該当会計年度の情報が取得できませんでした。");
                return false;
            }

            this._dtKikanKaishibi = Util.ToDate(dt.Rows[0]["KAIKEI_KIKAN_KAISHIBI"]);

            return true;
        }

        /// <summary>
        /// データを格納
        /// </summary>
        private void DispDataSet()
        {
            #region 前準備
            #region コメント
            // 対象データテーブルにカラムを5列ずつ定義
            KanjoKamokuKingaku.Columns.Add("shukeiKeisanShiki", Type.GetType("System.String"));
            KanjoKamokuKingaku.Columns.Add("zandakaKingaku", Type.GetType("System.Int64"));
            KanjoKamokuKingaku.Columns.Add("karikataKingaku", Type.GetType("System.Int64"));
            KanjoKamokuKingaku.Columns.Add("kashikataKingaku", Type.GetType("System.Int64"));
            KanjoKamokuKingaku.Columns.Add("touzanKingaku", Type.GetType("System.Int64"));
            #endregion

            ZMMR1031DA da = new ZMMR1031DA(this.UInfo, this.Dba, this.Config);

            // F6:貸借対照表、F7:損益計算書、F8:製造原価の大項目名を取得
            this._dtF8 = da.GetKamokuDaiKomoku(LIST_F1);
            this._dtF9 = da.GetKamokuDaiKomoku(LIST_F2);
            this._dtF10 = da.GetKamokuDaiKomoku(LIST_F3);

            #region コメント
            //DataTable kanjoKamokuIchiran;
            //DataTable kingakuData;
            //DataRow row;
            //int shukeiKubun;
            //string kamokuNm;
            //int kamokuBunrui;
            //int meisaiKubun;
            //int mojiShubetsu;
            //int daiKomokuTaishakuKubun;
            //int shoKomokuTaishakuKubun;
            //string shukeiKeisanShiki;
            //decimal zenZanKingaku;
            //decimal karikataKingaku;
            //decimal kashikataKingaku;
            //decimal zandakaKingaku;
            //int taishoNo;
            //string taishoNm;
            //int gyoBango;
            //string jokenPlus = "";
            //string jokenMinus = "";
            //int i = 0;
            //int j = 0;
            #endregion

            #endregion

            #region コメント

            //// 法定準備金の金額を保持
            //DataTable dt = da.GetHoteJunbiki(kikanKaishibi, this._pForm.Condition);
            //// 名護へ合わせ
            ////if(dt.Rows.Count > 0)
            //if (dt.Rows.Count > 0 && Util.ToInt(dt.Rows[0]["JUNBIKIN"]) > 0)
            //{
            //    this._dtHoteJunbikin = Util.ToDecimal(dt.Rows[0]["JUNBIKIN"]);
            //}
            //// 当期未処分剰余金の金額を保持
            //dt = da.GetTokiMishobunJoyokin(this._pForm.Condition);
            //// 名護へ合わせ
            ////if (dt.Rows.Count > 0)
            //if (dt.Rows.Count > 0 && Util.ToDecimal(dt.Rows[0]["KINGAKU"]) != 0)
            //{
            //    this._dtTokiMishobunJoyokin = Util.ToDecimal(dt.Rows[0]["KINGAKU"]);
            //}

            //// F6:貸借対照表、F7:損益計算書、F8:製造原価の大項目名毎の小項目データを取得し、格納
            //#region F8:製造原価
            //while (F8.Rows.Count > i)
            //{
            //    meisaiKubun = Util.ToInt(F8.Rows[i]["MEISAI_KUBUN"]);
            //    // 明細区分が0以外のデータのみ処理
            //    if (meisaiKubun != 0)
            //    {
            //        kamokuNm = Util.ToString(F8.Rows[i]["KAMOKU_BUNRUI_NM"]);
            //        shukeiKeisanShiki = Util.ToString(F8.Rows[i]["SHUKEI_KEISANSHIKI"]);
            //        mojiShubetsu = Util.ToInt(F8.Rows[i]["MOJI_SHUBETSU"]);
            //        daiKomokuTaishakuKubun = Util.ToInt(F8.Rows[i]["TAISHAKU_KUBUN"]);
            //        kamokuBunrui = Util.ToInt(F8.Rows[i]["KAMOKU_BUNRUI"]);
            //        kanjoKamokuIchiran = da.GetKamokuShoKomoku(LIST_F3, kamokuBunrui);

            //        // 小項目データを取得
            //        while (kanjoKamokuIchiran.Rows.Count > j)
            //        {
            //            zenZanKingaku = 0;
            //            karikataKingaku = 0;
            //            kashikataKingaku = 0;
            //            zandakaKingaku = 0;
            //            taishoNo = Util.ToInt(kanjoKamokuIchiran.Rows[j]["KANJO_KAMOKU_CD"]);
            //            taishoNm = Util.ToString(kanjoKamokuIchiran.Rows[j]["KANJO_KAMOKU_NM"]);
            //            gyoBango = Util.ToInt(kanjoKamokuIchiran.Rows[j]["GYO_BANGO"]);
            //            shoKomokuTaishakuKubun = Util.ToInt(kanjoKamokuIchiran.Rows[j]["TAISHAKU_KUBUN"]);

            //            // 金額データを取得
            //            kingakuData = da.GetKamokuKingaku(taishoNo, this._pForm.Condition);
            //            if (kingakuData.Rows.Count != 0)
            //            {
            //                zenZanKingaku = Util.ToDecimal(kingakuData.Rows[0]["ZEN_ZAN"]);
            //                karikataKingaku = Util.ToDecimal(kingakuData.Rows[0]["KARIKATA"]);
            //                kashikataKingaku = Util.ToDecimal(kingakuData.Rows[0]["KASHIKATA"]);
            //                zandakaKingaku = Util.ToDecimal(kingakuData.Rows[0]["ZANDAKA"]);
            //            }

            //            // F8:金額をデータテーブルに格納
            //            row = KanjoKamokuKingaku.NewRow();
            //            row["shukeiKeisanShiki"] = shukeiKeisanShiki;
            //            // 小項目の貸借区分が貸方の場合、-1を掛ける
            //            if (shoKomokuTaishakuKubun == 1)
            //            {
            //                row["zandakaKingaku"] = zenZanKingaku;
            //                row["karikataKingaku"] = karikataKingaku;
            //                row["kashikataKingaku"] = kashikataKingaku;
            //                row["touzanKingaku"] = zandakaKingaku;
            //            }
            //            else
            //            {
            //                row["zandakaKingaku"] = zenZanKingaku * -1;
            //                row["karikataKingaku"] = karikataKingaku * -1;
            //                row["kashikataKingaku"] = kashikataKingaku * -1;
            //                row["touzanKingaku"] = zandakaKingaku * -1;
            //            }
            //            KanjoKamokuKingaku.Rows.Add(row);
            //            j++;
            //        }
            //    }
            //    j = 0;
            //    i++;
            //}
            //#endregion
            //i = 0;
            //#region F7:損益計算書
            //while (F7.Rows.Count > i)
            //{
            //    shukeiKubun = Util.ToInt(F7.Rows[i]["SHUKEI_KUBUN"]);
            //    //集計区分が2のデータのみ処理
            //    if (shukeiKubun == 2)
            //    {
            //        kamokuNm = Util.ToString(F7.Rows[i]["KAMOKU_BUNRUI_NM"]);
            //        taishoNo = Util.ToInt(F7.Rows[j]["KAMOKU_BUNRUI"]);
            //        meisaiKubun = Util.ToInt(F7.Rows[i]["MEISAI_KUBUN"]);
            //        shukeiKeisanShiki = Util.ToString(F7.Rows[i]["SHUKEI_KEISANSHIKI"]);
            //        mojiShubetsu = Util.ToInt(F7.Rows[i]["MOJI_SHUBETSU"]);
            //        daiKomokuTaishakuKubun = Util.ToInt(F7.Rows[i]["TAISHAKU_KUBUN"]);
            //        kamokuBunrui = Util.ToInt(F7.Rows[i]["KAMOKU_BUNRUI"]);
            //        kanjoKamokuIchiran = da.GetKamokuShoKomoku(LIST_F2, kamokuBunrui);

            //        // 小項目データを取得
            //        while (kanjoKamokuIchiran.Rows.Count > j)
            //        {
            //            zenZanKingaku = 0;
            //            karikataKingaku = 0;
            //            kashikataKingaku = 0;
            //            zandakaKingaku = 0;
            //            taishoNo = Util.ToInt(kanjoKamokuIchiran.Rows[j]["KANJO_KAMOKU_CD"]);
            //            taishoNm = Util.ToString(kanjoKamokuIchiran.Rows[j]["KANJO_KAMOKU_NM"]);
            //            gyoBango = Util.ToInt(kanjoKamokuIchiran.Rows[j]["GYO_BANGO"]);
            //            shoKomokuTaishakuKubun = Util.ToInt(kanjoKamokuIchiran.Rows[j]["TAISHAKU_KUBUN"]);

            //            // 金額データを取得
            //            kingakuData = da.GetKamokuKingaku(taishoNo, this._pForm.Condition);
            //            if (kingakuData.Rows.Count != 0)
            //            {
            //                zenZanKingaku = Util.ToDecimal(kingakuData.Rows[0]["ZEN_ZAN"]);
            //                karikataKingaku = Util.ToDecimal(kingakuData.Rows[0]["KARIKATA"]);
            //                kashikataKingaku = Util.ToDecimal(kingakuData.Rows[0]["KASHIKATA"]);
            //                zandakaKingaku = Util.ToDecimal(kingakuData.Rows[0]["ZANDAKA"]);
            //            }

            //            // F7:金額をデータテーブルに格納
            //            row = KanjoKamokuKingaku.NewRow();
            //            row["shukeiKeisanShiki"] = shukeiKeisanShiki;
            //            // 小項目の貸借区分が貸方の場合、-1を掛ける
            //            if (shoKomokuTaishakuKubun == 2 || Util.ToDecimal(kanjoKamokuIchiran.Rows[j]["M_TAISHAKU_KUBUN"]) == 2)
            //            //if (shoKomokuTaishakuKubun == 2)
            //            {
            //                zenZanKingaku = zenZanKingaku * -1;
            //                karikataKingaku = karikataKingaku * -1;
            //                kashikataKingaku = kashikataKingaku * -1;
            //                zandakaKingaku = zandakaKingaku * -1;
            //            }
            //            row["zandakaKingaku"] = zenZanKingaku;
            //            row["karikataKingaku"] = karikataKingaku;
            //            row["kashikataKingaku"] = kashikataKingaku;
            //            row["touzanKingaku"] = zandakaKingaku;
            //            KanjoKamokuKingaku.Rows.Add(row);
            //            j++;
            //        }
            //    }
            //    j = 0;
            //    i++;
            //}
            //#endregion
            //i = 0;
            //#region F6:貸借対照表
            //while (F6.Rows.Count > i)
            //{
            //    meisaiKubun = Util.ToInt(F6.Rows[i]["MEISAI_KUBUN"]);
            //    kamokuBunrui = Util.ToInt(F6.Rows[i]["KAMOKU_BUNRUI"]);
            //    kamokuNm = Util.ToString(F6.Rows[i]["KAMOKU_BUNRUI_NM"]);
            //    shukeiKeisanShiki = Util.ToString(F6.Rows[i]["SHUKEI_KEISANSHIKI"]);
            //    mojiShubetsu = Util.ToInt(F6.Rows[i]["MOJI_SHUBETSU"]);
            //    daiKomokuTaishakuKubun = Util.ToInt(F6.Rows[i]["TAISHAKU_KUBUN"]);
            //    kanjoKamokuIchiran = da.GetKamokuShoKomoku(LIST_F1, kamokuBunrui);

            //    // 明細区分が0以外のデータのみ処理
            //    if (meisaiKubun != 0)
            //    {
            //        // 小項目データを取得、格納
            //        while (kanjoKamokuIchiran.Rows.Count > j)
            //        {
            //            zenZanKingaku = 0;
            //            karikataKingaku = 0;
            //            kashikataKingaku = 0;
            //            zandakaKingaku = 0;
            //            taishoNo = Util.ToInt(kanjoKamokuIchiran.Rows[j]["KANJO_KAMOKU_CD"]);
            //            taishoNm = Util.ToString(kanjoKamokuIchiran.Rows[j]["KANJO_KAMOKU_NM"]);
            //            gyoBango = Util.ToInt(kanjoKamokuIchiran.Rows[j]["GYO_BANGO"]);
            //            shoKomokuTaishakuKubun = Util.ToInt(kanjoKamokuIchiran.Rows[j]["TAISHAKU_KUBUN"]);

            //            // 金額データを取得
            //            kingakuData = da.GetKamokuKingaku(taishoNo, this._pForm.Condition);
            //            if (kingakuData.Rows.Count != 0)
            //            {
            //                zenZanKingaku = Util.ToDecimal(kingakuData.Rows[0]["ZEN_ZAN"]);
            //                karikataKingaku = Util.ToDecimal(kingakuData.Rows[0]["KARIKATA"]);
            //                kashikataKingaku = Util.ToDecimal(kingakuData.Rows[0]["KASHIKATA"]);
            //                zandakaKingaku = Util.ToDecimal(kingakuData.Rows[0]["ZANDAKA"]);
            //            }

            //            // F6:金額をデータテーブルに格納
            //            row = KanjoKamokuKingaku.NewRow();
            //            row["shukeiKeisanShiki"] = kamokuBunrui;
            //            // 小項目の貸借区分が貸方の場合、-1を掛ける
            //            if (shoKomokuTaishakuKubun == 1)
            //            {
            //                row["zandakaKingaku"] = zenZanKingaku;
            //                row["karikataKingaku"] = karikataKingaku;
            //                row["kashikataKingaku"] = kashikataKingaku;
            //                row["touzanKingaku"] = zandakaKingaku;
            //            }
            //            else
            //            {
            //                row["zandakaKingaku"] = zenZanKingaku * -1;
            //                row["karikataKingaku"] = karikataKingaku * -1;
            //                row["kashikataKingaku"] = kashikataKingaku * -1;
            //                row["touzanKingaku"] = zandakaKingaku * -1;
            //            }
            //            KanjoKamokuKingaku.Rows.Add(row);
            //            j++;
            //        }
            //    }
            //    // 明細区分が0で、科目分類が0以上のデータのみ処理
            //    if (meisaiKubun == 0 && kamokuBunrui > 0)
            //    {
            //        // 集計計算式にて計算
            //        ArrayList stTargetPlus = new ArrayList();
            //        ArrayList stTargetMinus = new ArrayList();
            //        object sumZandakaKingakuPlus;
            //        object sumKarikataKingakuPlus;
            //        object sumKashikataKingakuPlus;
            //        object sumTouzanKingakuPlus;
            //        object sumZandakaKingakuMinus;
            //        object sumKarikataKingakuMinus;
            //        object sumKashikataKingakuMinus;
            //        object sumTouzanKingakuMinus;
            //        int findPlus;
            //        int findMinus;
            //        int flag = 0;
            //        int first = 0;
            //        int plusMinusCheckFlag = 0;
            //        int deleteCount;

            //        #region 集計計算式の"+"と"-"を分別
            //        while (flag == 0)
            //        {
            //            // "+"と"-"の文字を検索
            //            findPlus = shukeiKeisanShiki.IndexOf("+");
            //            findMinus = shukeiKeisanShiki.IndexOf("-");
            //            // "+"も"-"も無ければ、処理終了
            //            if (findPlus <= 0 && findMinus <= 0)
            //            {
            //                if (first == 0)
            //                {
            //                    stTargetPlus.Add(shukeiKeisanShiki);
            //                }
            //                else
            //                {
            //                    if (plusMinusCheckFlag == 0)
            //                    {
            //                        stTargetPlus.Add(shukeiKeisanShiki);
            //                    }
            //                    else if (plusMinusCheckFlag == 1)
            //                    {
            //                        stTargetMinus.Add(shukeiKeisanShiki);
            //                    }
            //                }
            //                flag = 1;
            //            }
            //            // "+"又は"-"があれば分けて格納
            //            else
            //            {
            //                // 最初の処理 1つ目の項目を格納し、文字列から削除
            //                if (first == 0)
            //                {
            //                    // "+"の時の処理
            //                    if ((findMinus > findPlus && findPlus > 0) || (findPlus > findMinus && findMinus <= 0))
            //                    {
            //                        stTargetPlus.Add(shukeiKeisanShiki.Substring(0, findPlus));
            //                        shukeiKeisanShiki = shukeiKeisanShiki.Substring(findPlus);
            //                    }
            //                    // "-"の時の処理
            //                    else if ((findPlus > findMinus && findMinus > 0) || (findMinus > findPlus && findPlus <= 0))
            //                    {
            //                        stTargetPlus.Add(shukeiKeisanShiki.Substring(0, findMinus));
            //                        shukeiKeisanShiki = shukeiKeisanShiki.Substring(findMinus);
            //                    }
            //                }
            //                // 2つ目以降の処理 項目を順番に格納し、文字列から削除
            //                else
            //                {
            //                    // 削除する文字数を取得
            //                    if ((findPlus > findMinus && findMinus > 0) || (findMinus > findPlus && findPlus <= 0))
            //                    {
            //                        deleteCount = findMinus;
            //                    }
            //                    else
            //                    {
            //                        deleteCount = findPlus;
            //                    }
            //                    // 項目を格納
            //                    if (plusMinusCheckFlag == 0)
            //                    {
            //                        stTargetPlus.Add(shukeiKeisanShiki.Substring(0, deleteCount));
            //                        shukeiKeisanShiki = shukeiKeisanShiki.Substring(deleteCount);
            //                    }
            //                    else if (plusMinusCheckFlag == 1)
            //                    {
            //                        stTargetMinus.Add(shukeiKeisanShiki.Substring(0, deleteCount));
            //                        shukeiKeisanShiki = shukeiKeisanShiki.Substring(deleteCount);
            //                    }
            //                }
            //                // "+"と"-"の文字を検索
            //                findPlus = shukeiKeisanShiki.IndexOf("+");
            //                findMinus = shukeiKeisanShiki.IndexOf("-");
            //                // 次の項目がが"+"か"-"なのかの判断フラグを設定
            //                if (findPlus == 0)
            //                {
            //                    plusMinusCheckFlag = 0;
            //                }
            //                else if (findMinus == 0)
            //                {
            //                    plusMinusCheckFlag = 1;
            //                }
            //                // "+"又は"-"の文字を削除
            //                shukeiKeisanShiki = shukeiKeisanShiki.Substring(1);
            //            }
            //            first = 1;
            //        }
            //        #endregion

            //        #region 条件式の作成
            //        // "+"の項目の条件式を作成
            //        j = 0;
            //        while (stTargetPlus.Count > j)
            //        {
            //            if (j == 0)
            //            {
            //                jokenPlus = "shukeiKeisanShiki = " + stTargetPlus[j];
            //            }
            //            else
            //            {
            //                jokenPlus += " OR shukeiKeisanShiki = " + stTargetPlus[j];
            //            }

            //            j++;
            //        }
            //        // "-"の項目の条件式を作成
            //        j = 0;
            //        while (stTargetMinus.Count > j)
            //        {
            //            if (j == 0)
            //            {
            //                jokenMinus = "shukeiKeisanShiki = " + stTargetMinus[j];
            //            }
            //            else
            //            {
            //                jokenMinus += " OR shukeiKeisanShiki = " + stTargetMinus[j];
            //            }

            //            j++;
            //        }
            //        #endregion

            //        #region 残高金額、借方金額、貸方金額、当残金額の合計値を計算
            //        if (jokenPlus != "")
            //        {
            //            sumZandakaKingakuPlus = KanjoKamokuKingaku.Compute("SUM(zandakaKingaku)", jokenPlus);
            //            sumKarikataKingakuPlus = KanjoKamokuKingaku.Compute("SUM(karikataKingaku)", jokenPlus);
            //            sumKashikataKingakuPlus = KanjoKamokuKingaku.Compute("SUM(kashikataKingaku)", jokenPlus);
            //            sumTouzanKingakuPlus = KanjoKamokuKingaku.Compute("SUM(touzanKingaku)", jokenPlus);
            //        }
            //        else
            //        {
            //            sumZandakaKingakuPlus = 0;
            //            sumKarikataKingakuPlus = 0;
            //            sumKashikataKingakuPlus = 0;
            //            sumTouzanKingakuPlus = 0;
            //        }
            //        if (jokenMinus != "")
            //        {
            //            sumZandakaKingakuMinus = KanjoKamokuKingaku.Compute("SUM(zandakaKingaku)", jokenMinus);
            //            sumKarikataKingakuMinus = KanjoKamokuKingaku.Compute("SUM(karikataKingaku)", jokenMinus);
            //            sumKashikataKingakuMinus = KanjoKamokuKingaku.Compute("SUM(kashikataKingaku)", jokenMinus);
            //            sumTouzanKingakuMinus = KanjoKamokuKingaku.Compute("SUM(touzanKingaku)", jokenMinus);
            //        }
            //        else
            //        {
            //            sumZandakaKingakuMinus = 0;
            //            sumKarikataKingakuMinus = 0;
            //            sumKashikataKingakuMinus = 0;
            //            sumTouzanKingakuMinus = 0;
            //        }
            //        zenZanKingaku = Util.ToDecimal(sumZandakaKingakuPlus) + Util.ToDecimal(sumZandakaKingakuMinus);
            //        zandakaKingaku = Util.ToDecimal(sumTouzanKingakuPlus) + Util.ToDecimal(sumTouzanKingakuMinus);

            //        // 貸借区分が借方の場合
            //        if (daiKomokuTaishakuKubun == 1)
            //        {
            //            karikataKingaku = 0;
            //            kashikataKingaku = (zandakaKingaku + zenZanKingaku) * -1;
            //        }
            //        else
            //        {
            //            karikataKingaku = (zandakaKingaku - zenZanKingaku) * -1;
            //            kashikataKingaku = 0;
            //        }
            //        #endregion

            //        row = KanjoKamokuKingaku.NewRow();
            //        row["shukeiKeisanShiki"] = kamokuBunrui;
            //        row["zandakaKingaku"] = zenZanKingaku;
            //        row["karikataKingaku"] = karikataKingaku;
            //        row["kashikataKingaku"] = kashikataKingaku;
            //        row["touzanKingaku"] = zandakaKingaku;
            //        KanjoKamokuKingaku.Rows.Add(row);
            //    }
            //    j = 0;
            //    i++;
            //}
            //#endregion

            #endregion
        }

        /// <summary>
        /// データを表示
        /// </summary>
        private void DispData(DataTable F, int LIST_F)
        {
            // タイトルの表示
            if (LIST_F == 1)
            {
                this.lblGridViewTitle.Text = "<<貸借対照表>>";
            }
            else if (LIST_F == 2)
            {
                this.lblGridViewTitle.Text = "<<損益計算書>>";
            }
            else
            {
                this.lblGridViewTitle.Text = "<<製造原価報告書>>";
            }

            ZMMR1031DA da = new ZMMR1031DA(this.UInfo, this.Dba, this.Config);

            // まず今表示されている情報はクリア
            this.mtbList.Rows.Clear();

            string injiHandan = Util.ToString(this._pForm.Condition["Inji"]);
            DataTable kanjoKamokuIchiran;
            //DataRow[] dataRows;
            //int shukeiKubun;
            string kamokuNm;
            //int kamokuBunrui;
            //int meisaiKubun;
            //int meisaiKomokuSu;
            int mojiShubetsu;
            //int daiKomokuTaishakuKubun;
            //int shoKomokuTaishakuKubun;
            //int hyojijuni;
            //int shukeikubun;
            //int HoteiJunbikin = 0;
            //string shukeiKeisanShiki;
            decimal zenZanKingaku;
            decimal karikataKingaku;
            decimal kashikataKingaku;
            decimal zandakaKingaku;
            //decimal toukizandaka = 0;
            //decimal toukikarikata = 0;
            //decimal toukikashikata = 0;
            //decimal toukitouzan = 0;
            string tmpZenZanKingaku;
            string tmpKarikataKingaku;
            string tmpKashikataKingaku;
            string tmpZandakaKingaku;
            //string tmpSumZandakaKingaku = "";
            //string tmpSumKarikataKingaku = "";
            //string tmpSumKashikataKingaku = "";
            //string tmpSumTouzanKingaku = "";
            //string taishoNm;
            //string joken;
            //string jokenPlus = "";
            //string jokenMinus = "";
            //int gyoBango;
            //int gyoBangoNext;
            //bool rowAddFlg = false;
            int colorCount = 0; // GridView行の色設定用カウント変数
            Color color01; // 残高文字色設定用フラグ変数
            Color color02; // 借方発生文字色設定用フラグ変数
            Color color03; // 貸方発生文字色設定用フラグ変数
            Color color04; // 当残文字色設定用フラグ変数
            //int i = 0;
            //int j = 0;

            string kanjoKamokuFr = "";
            string kanjoKamokuTo = "";
            string kanjoKamokuLi = "";

            // 集計処理
            if (!da.Summary(this._pForm.Condition))
            {
                Msg.Info("該当するデータがありません。");
                this.Close();
                return;
            }

            if (F.Rows.Count > 0)
            {
                // 作成した集計情報を元に画面の作成
                kanjoKamokuIchiran = null;
                DataView dv = null;
                if (LIST_F == 1)
                {
                    dv = new DataView(da.TaishakuTaishohyo.Copy());
                }
                else if (LIST_F == 2)
                {
                    dv = new DataView(da.SonekiKeisansho.Copy());
                }
                else if (LIST_F == 3)
                {
                    dv = new DataView(da.SeizouGenkaHoukokusho.Copy());
                }

                // 画面表示用の場合は空行の排除
                try
                {
                    dv.RowFilter = LIST_F == 3 ? "OUTPUT_KUBUN = 1" : "KAMOKU_BUNRUI_NM <> ''";
                    kanjoKamokuIchiran = dv.ToTable();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    return;
                }

                foreach (DataRow r in kanjoKamokuIchiran.Rows)
                {
                    color01 = Color.Black;
                    color02 = Color.Black;
                    color03 = Color.Black;
                    color04 = Color.Black;
                    kamokuNm = Util.ToString(r["KAMOKU_BUNRUI_NM"]);
                    mojiShubetsu = Util.ToInt(r["MOJI_SHUBETSU"]);

                    // 元帳表示用科目の保持
                    kanjoKamokuFr = Util.ToString(r["KANJO_KAMOKU_CD_START"]);
                    kanjoKamokuTo = Util.ToString(r["KANJO_KAMOKU_CD_END"]);
                    kanjoKamokuLi = Util.ToString(r["KANJO_KAMOKU_CD_LIST"]);

                    // 金額データを取得
                    zenZanKingaku = Util.ToDecimal(r["ZANDAKA_KINGAKU"]);
                    karikataKingaku = Util.ToDecimal(r["KARIKATA_KINGAKU"]);
                    kashikataKingaku = Util.ToDecimal(r["KASHIKATA_KINGAKU"]);
                    zandakaKingaku = Util.ToDecimal(r["TOUZAN_KINGAKU"]);

                    #region 全て同じ扱いに
                    if (zenZanKingaku < 0)//  < 0
                    {
                        tmpZenZanKingaku = Util.FormatNum(Util.ToString(zenZanKingaku));
                        color01 = Color.Red;
                    }
                    else if (zenZanKingaku > 0)
                    {
                        tmpZenZanKingaku = Util.FormatNum(Util.ToString(zenZanKingaku));
                        color01 = Color.Black;
                    }
                    else
                    {
                        tmpZenZanKingaku = "";
                    }

                    if (karikataKingaku < 0)
                    {
                        tmpKarikataKingaku = Util.FormatNum(Util.ToString(karikataKingaku));
                        color02 = Color.Red;
                    }
                    else if (karikataKingaku > 0)
                    {
                        tmpKarikataKingaku = Util.FormatNum(Util.ToString(karikataKingaku));
                        color02 = Color.Black;
                    }
                    else
                    {
                        tmpKarikataKingaku = "";
                    }

                    if (kashikataKingaku < 0)
                    {
                        tmpKashikataKingaku = Util.FormatNum(Util.ToString(kashikataKingaku));
                        color03 = Color.Red;
                    }
                    else if (kashikataKingaku > 0)
                    {
                        tmpKashikataKingaku = Util.FormatNum(Util.ToString(kashikataKingaku));
                        color03 = Color.Black;
                    }
                    else
                    {
                        tmpKashikataKingaku = "";
                    }

                    if (zandakaKingaku < 0)
                    {
                        tmpZandakaKingaku = Util.FormatNum(Util.ToString(zandakaKingaku));
                        color04 = Color.Red;
                    }
                    else if (zandakaKingaku > 0)
                    {
                        tmpZandakaKingaku = Util.FormatNum(Util.ToString(zandakaKingaku));
                        color04 = Color.Black;
                    }
                    else
                    {
                        tmpZandakaKingaku = "";
                    }
                    #endregion

                    // 金額がｾﾞﾛの科目を印字する場合、あるいは印字しない場合で金額が0でない場合
                    if ((injiHandan == "yes")
                        || (tmpZenZanKingaku != "" || tmpKarikataKingaku != "" || tmpKashikataKingaku != "" || tmpZandakaKingaku != ""))
                    {

                        // DataGridView表示有無のFlagを1に設定
                        _viewFlag = 1;

                        this.mtbList.Rows.Add(kamokuNm, tmpZenZanKingaku, tmpKarikataKingaku, tmpKashikataKingaku, tmpZandakaKingaku, kanjoKamokuFr, kanjoKamokuTo, kanjoKamokuLi);

                        // 文字種別が太字(1)の場合、背景に色を設定
                        if (mojiShubetsu == 0)
                        {
                            this.mtbList.Rows[colorCount].DefaultCellStyle.BackColor = Color.White;
                        }
                        else
                        {
                            this.mtbList.Rows[colorCount].DefaultCellStyle.BackColor = Color.Aquamarine;
                        }

                        // 金額の文字色を設定
                        this.mtbList[1, colorCount].Style.ForeColor = color01;
                        this.mtbList[2, colorCount].Style.ForeColor = color02;
                        this.mtbList[3, colorCount].Style.ForeColor = color03;
                        this.mtbList[4, colorCount].Style.ForeColor = color04;
                        colorCount++;
                    }
                }
            }

            #region コメント
            //// 大項目名毎の小項目データを取得し、GridViewに表示
            //while (F.Rows.Count > i)
            //{
            //    color01 = Color.Black;
            //    color02 = Color.Black;
            //    color03 = Color.Black;
            //    color04 = Color.Black;
            //    kamokuNm = Util.ToString(F.Rows[i]["KAMOKU_BUNRUI_NM"]);
            //    meisaiKubun = Util.ToInt(F.Rows[i]["MEISAI_KUBUN"]);
            //    meisaiKomokuSu = Util.ToInt(F.Rows[i]["MEISAI_KOMOKUSU"]);
            //    kamokuBunrui = Util.ToInt(F.Rows[i]["KAMOKU_BUNRUI"]);
            //    kanjoKamokuIchiran = da.GetKamokuShoKomoku(LIST_F, kamokuBunrui);
            //    shukeiKeisanShiki = Util.ToString(F.Rows[i]["SHUKEI_KEISANSHIKI"]);
            //    mojiShubetsu = Util.ToInt(F.Rows[i]["MOJI_SHUBETSU"]);
            //    daiKomokuTaishakuKubun = Util.ToInt(F.Rows[i]["TAISHAKU_KUBUN"]);
            //    hyojijuni = Util.ToInt(F7.Rows[i]["HYOJI_JUNI"]);
            //    shukeikubun = Util.ToInt(F7.Rows[i]["SHUKEI_KUBUN"]);

            //    joken = "shukeiKeisanShiki = " + kamokuBunrui;
            //    dataRows = KanjoKamokuKingaku.Select(joken);
            //    #region 小項目データをGridViewに表示
            //    j = 0;
            //    // 明細項目数が1以上の場合のみ処理
            //    if (meisaiKomokuSu > 0)
            //    {
            //        // 一旦金額を初期化
            //        zenZanKingaku = 0;
            //        karikataKingaku = 0;
            //        kashikataKingaku = 0;
            //        zandakaKingaku = 0;
            //        rowAddFlg = false;

            //        while (kanjoKamokuIchiran.Rows.Count > j)
            //        {

            //            color01 = Color.Black;
            //            color02 = Color.Black;
            //            color03 = Color.Black;
            //            color04 = Color.Black;
            //            taishoNm = Util.ToString(kanjoKamokuIchiran.Rows[j]["KANJO_KAMOKU_NM"]);
            //            int taishoNo = Util.ToInt(kanjoKamokuIchiran.Rows[j]["KANJO_KAMOKU_CD"]);
            //            gyoBango = Util.ToInt(kanjoKamokuIchiran.Rows[j]["GYO_BANGO"]);
            //            shoKomokuTaishakuKubun = Util.ToInt(kanjoKamokuIchiran.Rows[j]["TAISHAKU_KUBUN"]);

            //            // 元帳表示用科目の保持
            //            if (kanjoKamokuFr == "")
            //            {
            //                kanjoKamokuFr = taishoNo.ToString();
            //            }
            //            kanjoKamokuTo = taishoNo.ToString();

            //            // 金額データを取得
            //            zenZanKingaku += Util.ToDecimal(dataRows[j]["zandakaKingaku"]);
            //            karikataKingaku += Util.ToDecimal(dataRows[j]["karikataKingaku"]);
            //            kashikataKingaku += Util.ToDecimal(dataRows[j]["kashikataKingaku"]);
            //            zandakaKingaku += Util.ToDecimal(dataRows[j]["touzanKingaku"]);

            //            // 未収金だけマイナス表示加工？
            //            //勘定科目CD141以上147以下
            //            //if (141 <= taishoNo && taishoNo <= 147)
            //            if (this._pForm.KAMOKU_BUNRUI_KEIZAI_JIGYO_MISHUKIN == kamokuBunrui)
            //            {
            //                #region 科目分類11133のみ
            //                if (zenZanKingaku < 0)//  < 0
            //                {
            //                    tmpZenZanKingaku = Util.FormatNum(Util.ToString(zenZanKingaku));
            //                    color01 = Color.Red;
            //                }
            //                else if (zenZanKingaku > 0)
            //                {
            //                    tmpZenZanKingaku = Util.FormatNum(Util.ToString(zenZanKingaku));
            //                    color01 = Color.Black;
            //                }
            //                else
            //                {
            //                    tmpZenZanKingaku = "";
            //                }

            //                if (karikataKingaku < 0)
            //                {
            //                    tmpKarikataKingaku = Util.FormatNum(Util.ToString(karikataKingaku));
            //                    color02 = Color.Red;
            //                }
            //                else if (karikataKingaku > 0)
            //                {
            //                    tmpKarikataKingaku = Util.FormatNum(Util.ToString(karikataKingaku));
            //                    color02 = Color.Black;
            //                }
            //                else
            //                {
            //                    tmpKarikataKingaku = "";
            //                }

            //                if (kashikataKingaku < 0)
            //                {
            //                    tmpKashikataKingaku = Util.FormatNum(Util.ToString(kashikataKingaku));
            //                    color03 = Color.Red;
            //                }
            //                else if (kashikataKingaku > 0)
            //                {
            //                    tmpKashikataKingaku = Util.FormatNum(Util.ToString(kashikataKingaku));
            //                    color03 = Color.Black;
            //                }
            //                else
            //                {
            //                    tmpKashikataKingaku = "";
            //                }

            //                if (zandakaKingaku < 0)
            //                {
            //                    tmpZandakaKingaku = Util.FormatNum(Util.ToString(zandakaKingaku));
            //                    color04 = Color.Red;
            //                }
            //                else if (zandakaKingaku > 0)
            //                {
            //                    tmpZandakaKingaku = Util.FormatNum(Util.ToString(zandakaKingaku));
            //                    color04 = Color.Black;
            //                }
            //                else
            //                {
            //                    tmpZandakaKingaku = "";
            //                }
            //                #endregion
            //            }
            //            else
            //            {
            //                #region それ以外
            //                if (zenZanKingaku > 0)
            //                {
            //                    tmpZenZanKingaku = Util.FormatNum(Util.ToString(zenZanKingaku));
            //                }
            //                else if (zenZanKingaku < 0)
            //                {
            //                    tmpZenZanKingaku = Util.FormatNum(Util.ToString(zenZanKingaku * -1));
            //                }
            //                else
            //                {
            //                    tmpZenZanKingaku = "";
            //                }
            //                if (karikataKingaku > 0)
            //                {
            //                    tmpKarikataKingaku = Util.FormatNum(Util.ToString(karikataKingaku));
            //                }
            //                else if (karikataKingaku < 0)
            //                {
            //                    tmpKarikataKingaku = Util.FormatNum(Util.ToString(karikataKingaku * -1));
            //                }
            //                else
            //                {
            //                    tmpKarikataKingaku = "";
            //                }
            //                if (kashikataKingaku > 0)
            //                {
            //                    tmpKashikataKingaku = Util.FormatNum(Util.ToString(kashikataKingaku));
            //                }
            //                else if (kashikataKingaku < 0)
            //                {
            //                    tmpKashikataKingaku = Util.FormatNum(Util.ToString(kashikataKingaku * -1));
            //                }
            //                else
            //                {
            //                    tmpKashikataKingaku = "";
            //                }
            //                if (zandakaKingaku > 0)
            //                {
            //                    tmpZandakaKingaku = Util.FormatNum(Util.ToString(zandakaKingaku));
            //                }
            //                else if (zandakaKingaku < 0)
            //                {
            //                    tmpZandakaKingaku = Util.FormatNum(Util.ToString(zandakaKingaku * -1));
            //                }
            //                else
            //                {
            //                    tmpZandakaKingaku = "";
            //                }
            //                #endregion
            //            }
                        
            //            // 金額がｾﾞﾛの科目を印字する場合、あるいは印字しない場合で金額が0でない場合
            //            if ((injiHandan == "yes")
            //                || (tmpZenZanKingaku != "" || tmpKarikataKingaku != "" || tmpKashikataKingaku != "" || tmpZandakaKingaku != ""))
            //            {
            //                // 次の行を参照して、行番号が同じならば行追加をせずに足しこむ
            //                try
            //                {
            //                    gyoBangoNext = Util.ToInt(kanjoKamokuIchiran.Rows[j+1]["GYO_BANGO"]);
            //                    if (gyoBango == gyoBangoNext)
            //                    {
            //                        rowAddFlg = false;
            //                    }
            //                    else
            //                    {
            //                        rowAddFlg = true;
            //                    }
            //                }
            //                catch (Exception)
            //                {
            //                    // 次の行はない
            //                    rowAddFlg = true;
            //                }

            //                if (rowAddFlg)
            //                {
            //                    // DataGridView表示有無のFlagを1に設定
            //                    _viewFlag = 1;

            //                    // 元帳表示用科目の保持
            //                    //this.mtbList.Rows.Add(taishoNm, tmpZenZanKingaku, tmpKarikataKingaku, tmpKashikataKingaku, tmpZandakaKingaku);
            //                    this.mtbList.Rows.Add(taishoNm, tmpZenZanKingaku, tmpKarikataKingaku, tmpKashikataKingaku, tmpZandakaKingaku, kanjoKamokuFr, kanjoKamokuTo);
            //                    this.mtbList.Rows[colorCount].DefaultCellStyle.BackColor = Color.White;
            //                    // 金額の文字色を設定
            //                    this.mtbList[1, colorCount].Style.ForeColor = color01;
            //                    this.mtbList[2, colorCount].Style.ForeColor = color02;
            //                    this.mtbList[3, colorCount].Style.ForeColor = color03;
            //                    this.mtbList[4, colorCount].Style.ForeColor = color04;
            //                    colorCount++;
            //                    // 金額をクリア
            //                    zenZanKingaku = 0;
            //                    karikataKingaku = 0;
            //                    kashikataKingaku = 0;
            //                    zandakaKingaku = 0;

            //                    kanjoKamokuFr = "";
            //                    kanjoKamokuTo = "";
            //                }
            //            }
            //            j++;
            //        }
            //    }
            //    #endregion

            //    #region 大項目データをGridViewに表示
            //    // 集計計算式にて計算
            //    ArrayList stTargetPlus = new ArrayList();
            //    ArrayList stTargetMinus = new ArrayList();
            //    int findPlus;
            //    int findMinus;
            //    int flag = 0;
            //    int first = 0;
            //    int plusMinusCheckFlag = 0;
            //    int deleteCount;
            //    object sumZandakaKingakuPlus;
            //    object sumKarikataKingakuPlus;
            //    object sumKashikataKingakuPlus;
            //    object sumTouzanKingakuPlus;
            //    object sumZandakaKingakuMinus;
            //    object sumKarikataKingakuMinus;
            //    object sumKashikataKingakuMinus;
            //    object sumTouzanKingakuMinus;

            //    shukeiKubun = Util.ToInt(F.Rows[i]["SHUKEI_KUBUN"]);

            //    #region 貸借対照表の大項目
            //    if (LIST_F == 1)
            //    {
            //        flag = 0;
            //        jokenPlus = "";
            //        jokenMinus = "";
            //        #region 明細区分が0以外の場合
            //        if (meisaiKubun != 0)
            //        {
            //            joken = "shukeiKeisanShiki = " + shukeiKeisanShiki;
            //            // 残高金額、借方金額、貸方金額、当残金額の合計値を計算
            //            // 貸借区分が借方の場合
            //            if (daiKomokuTaishakuKubun == 1)             
            //            {
            //                zenZanKingaku = Util.ToDecimal(KanjoKamokuKingaku.Compute("SUM(zandakaKingaku)", joken));
            //                karikataKingaku = Util.ToDecimal(KanjoKamokuKingaku.Compute("SUM(karikataKingaku)", joken));
            //                kashikataKingaku = Util.ToDecimal(KanjoKamokuKingaku.Compute("SUM(kashikataKingaku)", joken));
            //                zandakaKingaku = Util.ToDecimal(KanjoKamokuKingaku.Compute("SUM(touzanKingaku)", joken));
            //            }
            //            // 貸借区分が貸方の場合
            //            else
            //            {
            //                zenZanKingaku = Util.ToDecimal(KanjoKamokuKingaku.Compute("SUM(zandakaKingaku)", joken)) * -1;
            //                karikataKingaku = Util.ToDecimal(KanjoKamokuKingaku.Compute("SUM(karikataKingaku)", joken)) * -1;
            //                kashikataKingaku = Util.ToDecimal(KanjoKamokuKingaku.Compute("SUM(kashikataKingaku)", joken)) * -1;
            //                zandakaKingaku = Util.ToDecimal(KanjoKamokuKingaku.Compute("SUM(touzanKingaku)", joken)) * -1;
            //            }
            //            // 残高
            //            if (zenZanKingaku == 0)
            //            {
            //                tmpSumZandakaKingaku = "";
            //            }
            //            else if (zenZanKingaku < 0)
            //            {
            //                tmpSumZandakaKingaku = Util.ToString(zenZanKingaku);
            //                color01 = Color.Red;
            //            }
            //            else
            //            {
            //                tmpSumZandakaKingaku = Util.ToString(zenZanKingaku);
            //            }
            //            // 借方発生
            //            if (karikataKingaku == 0)
            //            {
            //                tmpSumKarikataKingaku = "";
            //            }
            //            else if (karikataKingaku < 0)
            //            {
            //                tmpSumKarikataKingaku = Util.ToString(karikataKingaku);
            //                color02 = Color.Red;
            //            }
            //            else
            //            {
            //                tmpSumKarikataKingaku = Util.ToString(karikataKingaku);
            //            }
            //            // 貸方発生
            //            if (kashikataKingaku == 0)
            //            {
            //                tmpSumKashikataKingaku = "";
            //            }
            //            else if (kashikataKingaku < 0)
            //            {
            //                tmpSumKashikataKingaku = Util.ToString(kashikataKingaku);
            //                color03 = Color.Red;
            //            }
            //            else
            //            {
            //                tmpSumKashikataKingaku = Util.ToString(kashikataKingaku);
            //            }
            //            // 当残
            //            if (zandakaKingaku == 0)
            //            {
            //                tmpSumTouzanKingaku = "";
            //            }
            //            else if (zandakaKingaku < 0)
            //            {
            //                tmpSumTouzanKingaku = Util.ToString(zandakaKingaku);
            //                color04 = Color.Red;
            //            }
            //            else
            //            {
            //                tmpSumTouzanKingaku = Util.ToString(zandakaKingaku);
            //            }
            //            // 貸借対照表の当期未処分剰余金行への行への設定用
            //            // 損益計算書の【当期未処分剰余金】科目分類コード時の設定用
            //            //if (kamokuBunrui == 13320)
            //            if (kamokuBunrui == this._pForm.KAMOKU_BUNRUI_HOTE_JUNBIKIN)
            //            {
            //                HoteiJunbikin = Util.ToInt(kashikataKingaku);
            //            }
            //        }
            //        #endregion
            //        #region 明細区分が0の場合
            //        else
            //        {
            //            #region 集計計算式の"+"と"-"を分別
            //            while (flag == 0)
            //            {
            //                // "+"と"-"の文字を検索
            //                findPlus = shukeiKeisanShiki.IndexOf("+");
            //                findMinus = shukeiKeisanShiki.IndexOf("-");
            //                // "+"も"-"も無ければ、処理終了
            //                if (findPlus <= 0 && findMinus <= 0)
            //                {
            //                    if (first == 0)
            //                    {
            //                        stTargetPlus.Add(shukeiKeisanShiki);
            //                    }
            //                    else
            //                    {
            //                        if (plusMinusCheckFlag == 0)
            //                        {
            //                            stTargetPlus.Add(shukeiKeisanShiki);
            //                        }
            //                        else if (plusMinusCheckFlag == 1)
            //                        {
            //                            stTargetMinus.Add(shukeiKeisanShiki);
            //                        }
            //                    }
            //                    flag = 1;
            //                }
            //                // "+"又は"-"があれば分けて格納
            //                else
            //                {
            //                    // 最初の処理 1つ目の項目を格納し、文字列から削除
            //                    if (first == 0)
            //                    {
            //                        // "+"の時の処理
            //                        if ((findMinus > findPlus && findPlus > 0) || (findPlus > findMinus && findMinus <= 0))
            //                        {
            //                            stTargetPlus.Add(shukeiKeisanShiki.Substring(0, findPlus));
            //                            shukeiKeisanShiki = shukeiKeisanShiki.Substring(findPlus);
            //                        }
            //                        // "-"の時の処理
            //                        else if ((findPlus > findMinus && findMinus > 0) || (findMinus > findPlus && findPlus <= 0))
            //                        {
            //                            stTargetPlus.Add(shukeiKeisanShiki.Substring(0, findMinus));
            //                            shukeiKeisanShiki = shukeiKeisanShiki.Substring(findMinus);
            //                        }
            //                    }
            //                    // 2つ目以降の処理 項目を順番に格納し、文字列から削除
            //                    else
            //                    {
            //                        // 削除する文字数を取得
            //                        if ((findPlus > findMinus && findMinus > 0) || (findMinus > findPlus && findPlus <= 0))
            //                        {
            //                            deleteCount = findMinus;
            //                        }
            //                        else
            //                        {
            //                            deleteCount = findPlus;
            //                        }
            //                        // 項目を格納
            //                        if (plusMinusCheckFlag == 0)
            //                        {
            //                            stTargetPlus.Add(shukeiKeisanShiki.Substring(0, deleteCount));
            //                            shukeiKeisanShiki = shukeiKeisanShiki.Substring(deleteCount);
            //                        }
            //                        else if (plusMinusCheckFlag == 1)
            //                        {
            //                            stTargetMinus.Add(shukeiKeisanShiki.Substring(0, deleteCount));
            //                            shukeiKeisanShiki = shukeiKeisanShiki.Substring(deleteCount);
            //                        }
            //                    }
            //                    // "+"と"-"の文字を検索
            //                    findPlus = shukeiKeisanShiki.IndexOf("+");
            //                    findMinus = shukeiKeisanShiki.IndexOf("-");
            //                    // 次の項目がが"+"か"-"なのかの判断フラグを設定
            //                    if (findPlus == 0)
            //                    {
            //                        plusMinusCheckFlag = 0;
            //                    }
            //                    else if (findMinus == 0)
            //                    {
            //                        plusMinusCheckFlag = 1;
            //                    }
            //                    // "+"又は"-"の文字を削除
            //                    shukeiKeisanShiki = shukeiKeisanShiki.Substring(1);
            //                }
            //                first = 1;
            //            }
            //            #endregion

            //            #region 条件式の作成
            //            // "+"の項目の条件式を作成
            //            j = 0;
            //            while (stTargetPlus.Count > j)
            //            {
            //                if (j == 0)
            //                {
            //                    jokenPlus = "shukeiKeisanShiki = " + stTargetPlus[j];
            //                }
            //                else
            //                {
            //                    jokenPlus += " OR shukeiKeisanShiki = " + stTargetPlus[j];
            //                }

            //                j++;
            //            }
            //            // "-"の項目の条件式を作成
            //            j = 0;
            //            while (stTargetMinus.Count > j)
            //            {
            //                if (j == 0)
            //                {
            //                    jokenMinus = "shukeiKeisanShiki = " + stTargetMinus[j];
            //                }
            //                else
            //                {
            //                    jokenMinus += " OR shukeiKeisanShiki = " + stTargetMinus[j];
            //                }

            //                j++;
            //            }
            //            #endregion

            //            #region 残高金額、借方金額、貸方金額、当残金額の合計値を計算
            //            if (jokenPlus != "")
            //            {
            //                sumZandakaKingakuPlus = KanjoKamokuKingaku.Compute("SUM(zandakaKingaku)", jokenPlus);
            //                sumKarikataKingakuPlus = KanjoKamokuKingaku.Compute("SUM(karikataKingaku)", jokenPlus);
            //                sumKashikataKingakuPlus = KanjoKamokuKingaku.Compute("SUM(kashikataKingaku)", jokenPlus);
            //                sumTouzanKingakuPlus = KanjoKamokuKingaku.Compute("SUM(touzanKingaku)", jokenPlus);
            //            }
            //            else
            //            {
            //                sumZandakaKingakuPlus = 0;
            //                sumKarikataKingakuPlus = 0;
            //                sumKashikataKingakuPlus = 0;
            //                sumTouzanKingakuPlus = 0;
            //            }
            //            if (jokenMinus != "")
            //            {
            //                sumZandakaKingakuMinus = KanjoKamokuKingaku.Compute("SUM(zandakaKingaku)", jokenMinus);
            //                sumKarikataKingakuMinus = KanjoKamokuKingaku.Compute("SUM(karikataKingaku)", jokenMinus);
            //                sumKashikataKingakuMinus = KanjoKamokuKingaku.Compute("SUM(kashikataKingaku)", jokenMinus);
            //                sumTouzanKingakuMinus = KanjoKamokuKingaku.Compute("SUM(touzanKingaku)", jokenMinus);
            //            }
            //            else
            //            {
            //                sumZandakaKingakuMinus = 0;
            //                sumKarikataKingakuMinus = 0;
            //                sumKashikataKingakuMinus = 0;
            //                sumTouzanKingakuMinus = 0;
            //            }
            //            zenZanKingaku = Util.ToDecimal(sumZandakaKingakuPlus) + Util.ToDecimal(sumZandakaKingakuMinus);
            //            karikataKingaku = Util.ToDecimal(sumKarikataKingakuPlus) + Util.ToDecimal(sumKarikataKingakuMinus);
            //            kashikataKingaku = Util.ToDecimal(sumKashikataKingakuPlus) + Util.ToDecimal(sumKashikataKingakuMinus);
            //            zandakaKingaku = Util.ToDecimal(sumTouzanKingakuPlus) + Util.ToDecimal(sumTouzanKingakuMinus);

            //            #region 勘定科目が"当期未処分剰余金"、"(うち当期剰余金)"の場合
            //            // 名護へ合わせ
            //            //if (F.Rows.Count - 4 == i || F.Rows.Count - 3 == i)
            //            if (F.Rows.Count - 4 == i || F.Rows.Count - 5 == i)
            //            {
            //                #region 対象区分が借方の場合
            //                // 対象区分が借方の場合
            //                if (daiKomokuTaishakuKubun == 1)
            //                {
            //                    karikataKingaku = zandakaKingaku - zenZanKingaku;
            //                    kashikataKingaku = zandakaKingaku + zenZanKingaku;

            //                    // 当残が0以上の場合、借方発生に記載
            //                    if (zandakaKingaku >= 0)
            //                    {
            //                        // 残高
            //                        if (zenZanKingaku == 0)
            //                        {
            //                            tmpSumZandakaKingaku = "";
            //                        }
            //                        else if (zenZanKingaku < 0)
            //                        {
            //                            tmpSumZandakaKingaku = Util.ToString(zenZanKingaku);
            //                            color01 = Color.Red;
            //                        }
            //                        else
            //                        {
            //                            tmpSumZandakaKingaku = Util.ToString(zenZanKingaku);
            //                        }
            //                        // 借方発生
            //                        if (kashikataKingaku == 0)
            //                        {
            //                            tmpSumKarikataKingaku = "";
            //                        }
            //                        else if (kashikataKingaku < 0)
            //                        {
            //                            tmpSumKarikataKingaku = Util.ToString(karikataKingaku);
            //                            color02 = Color.Red;
            //                        }
            //                        else
            //                        {
            //                            tmpSumKarikataKingaku = Util.ToString(karikataKingaku);
            //                        }
            //                        // 貸方発生
            //                        tmpSumKashikataKingaku = Util.ToString(kashikataKingaku);
            //                        // 当残
            //                        if (zandakaKingaku == 0)
            //                        {
            //                            tmpSumTouzanKingaku = "";
            //                        }
            //                        else if (zandakaKingaku < 0)
            //                        {
            //                            tmpSumTouzanKingaku = Util.ToString(zandakaKingaku);
            //                            color04 = Color.Red;
            //                        }
            //                        else
            //                        {
            //                            tmpSumTouzanKingaku = Util.ToString(zandakaKingaku);
            //                        }
            //                    }
            //                    // 0未満の場合、貸方発生に記載
            //                    else
            //                    {
            //                        // 残高
            //                        if (zenZanKingaku == 0)
            //                        {
            //                            tmpSumZandakaKingaku = "";
            //                        }
            //                        else if (zenZanKingaku < 0)
            //                        {
            //                            tmpSumZandakaKingaku = Util.ToString(zenZanKingaku);
            //                            color01 = Color.Red;
            //                        }
            //                        else
            //                        {
            //                            tmpSumZandakaKingaku = Util.ToString(zenZanKingaku);
            //                        }
            //                        // 借方発生
            //                        tmpSumKarikataKingaku = "";
            //                        // 貸方発生
            //                        if (kashikataKingaku == 0)
            //                        {
            //                            tmpSumKashikataKingaku = "";
            //                        }
            //                        else if (kashikataKingaku < 0)
            //                        {
            //                            tmpSumKashikataKingaku = Util.ToString(kashikataKingaku);
            //                            color03 = Color.Red;
            //                        }
            //                        else
            //                        {
            //                            tmpSumKashikataKingaku = Util.ToString(kashikataKingaku);
            //                        }
            //                        // 当残
            //                        if (zandakaKingaku == 0)
            //                        {
            //                            tmpSumTouzanKingaku = "";
            //                        }
            //                        else if (zandakaKingaku < 0)
            //                        {
            //                            tmpSumTouzanKingaku = Util.ToString(zandakaKingaku);
            //                            color04 = Color.Red;
            //                        }
            //                        else
            //                        {
            //                            tmpSumTouzanKingaku = Util.ToString(zandakaKingaku);
            //                        }
            //                    }
            //                }
            //                #endregion
            //                #region 対象区分が貸方の場合
            //                else
            //                {
            //                    zenZanKingaku = zenZanKingaku * -1;
            //                    karikataKingaku = zandakaKingaku + zenZanKingaku;
            //                    zandakaKingaku = zandakaKingaku * -1;
            //                    kashikataKingaku = zandakaKingaku - zenZanKingaku;
            //                    // 名護へ合わせ
            //                    // if (F.Rows.Count - 4 == i)// 27期
            //                    if (F.Rows.Count - 5 == i)
            //                    {
            //                        zandakaKingaku = zenZanKingaku - HoteiJunbikin + kashikataKingaku;
            //                    }

            //                    // 当残が0以上の場合、貸方発生に記載
            //                    if (zandakaKingaku >= 0)
            //                    {
            //                        // 残高
            //                        if (zenZanKingaku == 0)
            //                        {
            //                            tmpSumZandakaKingaku = "";
            //                        }
            //                        else if (zenZanKingaku < 0)
            //                        {
            //                            tmpSumZandakaKingaku = Util.ToString(zenZanKingaku);
            //                            color01 = Color.Red;
            //                        }
            //                        else
            //                        {
            //                            tmpSumZandakaKingaku = Util.ToString(zenZanKingaku);
            //                        }
            //                        // 勘定科目が"当期未処分剰余金"の借方金額
            //                        // 名護へ合わせ
            //                        //     if (F.Rows.Count - 4 == i)
            //                        if (F.Rows.Count - 5 == i)
            //                        {
            //                            // 借方発生
            //                            tmpSumKarikataKingaku = Util.ToString(HoteiJunbikin);
            //                            TaishakuHoteiJunbikin[0] = Util.ToDecimal(tmpSumKarikataKingaku);
            //                            if (HoteiJunbikin == 0)
            //                            {
            //                                tmpSumKarikataKingaku = "";
            //                            }
            //                        }
            //                        // 勘定科目が"(うち当期剰余金)"の借方金額
            //                        else
            //                        {
            //                            // 借方発生
            //                            tmpSumKarikataKingaku = "";
            //                        }
            //                        // 貸方発生
            //                        if (kashikataKingaku == 0)
            //                        {
            //                            tmpSumKashikataKingaku = "";
            //                        }
            //                        else if (kashikataKingaku < 0)
            //                        {
            //                            tmpSumKashikataKingaku = Util.ToString(kashikataKingaku);
            //                            color03 = Color.Red;
            //                        }
            //                        else
            //                        {
            //                            tmpSumKashikataKingaku = Util.ToString(kashikataKingaku);
            //                        }
            //                        // 当残
            //                        if (zandakaKingaku == 0)
            //                        {
            //                            tmpSumTouzanKingaku = "";
            //                        }
            //                        else if (zandakaKingaku < 0)
            //                        {
            //                            tmpSumTouzanKingaku = Util.ToString(zandakaKingaku);
            //                            color04 = Color.Red;
            //                        }
            //                        else
            //                        {
            //                            tmpSumTouzanKingaku = Util.ToString(zandakaKingaku);
            //                        }
            //                    }
            //                    // 0未満の場合、借方発生に記載
            //                    else
            //                    {
            //                        // 残高
            //                        if (zenZanKingaku == 0)
            //                        {
            //                            tmpSumZandakaKingaku = "";
            //                        }
            //                        else if (zenZanKingaku < 0)
            //                        {
            //                            tmpSumZandakaKingaku = Util.ToString(zenZanKingaku);
            //                            color01 = Color.Red;
            //                        }
            //                        else
            //                        {
            //                            tmpSumZandakaKingaku = Util.ToString(zenZanKingaku);
            //                        }
            //                        // 借方発生
            //                        if (karikataKingaku == 0)
            //                        {
            //                            tmpSumKarikataKingaku = "";
            //                        }
            //                        else if (karikataKingaku < 0)
            //                        {
            //                            tmpSumKarikataKingaku = Util.ToString(karikataKingaku);
            //                            color02 = Color.Red;
            //                        }
            //                        else
            //                        {
            //                            tmpSumKarikataKingaku = Util.ToString(karikataKingaku);
            //                        }
            //                        // 貸方発生
            //                        tmpSumKashikataKingaku = "";
            //                        // 当残
            //                        if (zandakaKingaku == 0)
            //                        {
            //                            tmpSumTouzanKingaku = "";
            //                        }
            //                        else if (zandakaKingaku < 0)
            //                        {
            //                            tmpSumTouzanKingaku = Util.ToString(zandakaKingaku);
            //                            color04 = Color.Red;
            //                        }
            //                        else
            //                        {
            //                            tmpSumTouzanKingaku = Util.ToString(zandakaKingaku);
            //                        }
            //                    }
            //                }
            //                #endregion

            //                //当期未処分剰余金を保持
            //                // 名護へ合わせ
            //                //  if (F.Rows.Count - 4 == i)
            //                if (F.Rows.Count - 5 == i)
            //                {
            //                    toukizandaka = Util.ToDecimal(tmpSumZandakaKingaku);
            //                    toukikarikata = Util.ToDecimal(tmpSumKarikataKingaku);
            //                    toukikashikata = Util.ToDecimal(tmpSumKashikataKingaku);
            //                    toukitouzan = Util.ToDecimal(tmpSumTouzanKingaku);
            //                }
            //            }
            //            #endregion
            //            #region 最後の合計の処理
            //            else if(F.Rows.Count - 2 == i || F.Rows.Count - 1 == i )
            //            {
            //                // 貸借区分が貸方の場合
            //                if (daiKomokuTaishakuKubun == 2)
            //                {
            //                    zenZanKingaku = zenZanKingaku * -1;
            //                    karikataKingaku = karikataKingaku * -1;
            //                    kashikataKingaku = kashikataKingaku * -1;
            //                    zandakaKingaku = zandakaKingaku * -1;
            //                }
            //                zenZanKingaku = zenZanKingaku + toukizandaka;
            //                karikataKingaku = karikataKingaku + toukikarikata;
            //                kashikataKingaku = kashikataKingaku + toukikashikata;
            //                zandakaKingaku = zandakaKingaku + toukitouzan;

            //                // 残高
            //                if (zenZanKingaku == 0)
            //                {
            //                    tmpSumZandakaKingaku = "";
            //                }
            //                else if (zenZanKingaku < 0)
            //                {
            //                    tmpSumZandakaKingaku = Util.ToString(zenZanKingaku);
            //                    color01 = Color.Red;
            //                }
            //                else
            //                {
            //                    tmpSumZandakaKingaku = Util.ToString(zenZanKingaku);
            //                }
            //                // 借方発生
            //                if (karikataKingaku < 0)
            //                {
            //                    tmpSumKarikataKingaku = "";
            //                }
            //                else
            //                {
            //                    tmpSumKarikataKingaku = Util.ToString(karikataKingaku);
            //                }
            //                // 貸方発生
            //                if (kashikataKingaku < 0)
            //                {
            //                    tmpSumKashikataKingaku = "";
            //                }
            //                else
            //                {
            //                    tmpSumKashikataKingaku = Util.ToString(kashikataKingaku);
            //                }
            //                // 当残
            //                if (zandakaKingaku == 0)
            //                {
            //                    tmpSumTouzanKingaku = "";
            //                }
            //                else if (zandakaKingaku < 0)
            //                {
            //                    tmpSumTouzanKingaku = Util.ToString(zandakaKingaku);
            //                    color04 = Color.Red;
            //                }
            //                else
            //                {
            //                    tmpSumTouzanKingaku = Util.ToString(zandakaKingaku);
            //                }
            //            }
            //            #endregion
            //            #region それ以外の場合
            //            else
            //            {
            //                // 貸借区分が貸方の場合
            //                if (daiKomokuTaishakuKubun == 2)
            //                {
            //                    zenZanKingaku = zenZanKingaku * -1;
            //                    karikataKingaku = karikataKingaku * -1;
            //                    kashikataKingaku = kashikataKingaku * -1;
            //                    zandakaKingaku = zandakaKingaku * -1;
            //                }
            //                // 残高
            //                if (zenZanKingaku == 0)
            //                {
            //                    tmpSumZandakaKingaku = "";
            //                }
            //                else if (zenZanKingaku < 0)
            //                {
            //                    tmpSumZandakaKingaku = Util.ToString(zenZanKingaku);
            //                    color01 = Color.Red;
            //                }
            //                else
            //                {
            //                    tmpSumZandakaKingaku = Util.ToString(zenZanKingaku);
            //                }
            //                // 借方発生
            //                if (karikataKingaku < 0)
            //                {
            //                    tmpSumKarikataKingaku = "";
            //                }
            //                else
            //                {
            //                    tmpSumKarikataKingaku = Util.ToString(karikataKingaku);
            //                }
            //                // 貸方発生
            //                if (kashikataKingaku < 0)
            //                {
            //                    tmpSumKashikataKingaku = "";
            //                }
            //                else 
            //                {
            //                    tmpSumKashikataKingaku = Util.ToString(kashikataKingaku);
            //                }
            //                // 当残
            //                if (zandakaKingaku == 0)
            //                {
            //                    tmpSumTouzanKingaku = "";
            //                }
            //                else if (zandakaKingaku < 0)
            //                {
            //                    tmpSumTouzanKingaku = Util.ToString(zandakaKingaku);
            //                    color04 = Color.Red;
            //                }
            //                else
            //                {
            //                    tmpSumTouzanKingaku = Util.ToString(zandakaKingaku);
            //                }
            //            }
            //            #endregion
            //            #endregion
            //        }
            //        #endregion
            //        // 2016-04-28 追記 kisemori
            //        //当期未処分剰余金を保持
            //        // 名護へ合わせ
            //        //if (F.Rows.Count - 4 == i || F.Rows.Count - 2 <= i)
            //        if (F.Rows.Count - 5 == i || F.Rows.Count - 2 <= i)
            //        {
            //            tmpSumZandakaKingaku = Util.ToString(Util.ToDecimal(tmpSumZandakaKingaku) - hoteJunbikin + tokiMishobunJoyokin);
            //            tmpSumTouzanKingaku = Util.ToString(Util.ToDecimal(tmpSumTouzanKingaku) - hoteJunbikin + tokiMishobunJoyokin);
            //        }
            //    }
            //    #endregion

            //    #region 損益計算書の大項目
            //    else if (LIST_F == 2)
            //    {
            //        flag = 0;
            //        jokenPlus = "";
            //        jokenMinus = "";
            //        // 集計区分が2の場合
            //        if (shukeiKubun == 2)
            //        {
            //            joken = "shukeiKeisanShiki = " + shukeiKeisanShiki;
            //            // 残高金額、借方金額、貸方金額、当残金額の合計値を計算
            //            // 貸借区分が借方の場合
            //            if (daiKomokuTaishakuKubun == 1)
            //            {
            //                zenZanKingaku = Util.ToDecimal(KanjoKamokuKingaku.Compute("SUM(zandakaKingaku)", joken));
            //                karikataKingaku = Util.ToDecimal(KanjoKamokuKingaku.Compute("SUM(karikataKingaku)", joken));
            //                kashikataKingaku = Util.ToDecimal(KanjoKamokuKingaku.Compute("SUM(kashikataKingaku)", joken));
            //                zandakaKingaku = Util.ToDecimal(KanjoKamokuKingaku.Compute("SUM(touzanKingaku)", joken));
            //            }
            //            // 貸借区分が貸方の場合
            //            else
            //            {
            //                zenZanKingaku = Util.ToDecimal(KanjoKamokuKingaku.Compute("SUM(zandakaKingaku)", joken)) * - 1;
            //                karikataKingaku = Util.ToDecimal(KanjoKamokuKingaku.Compute("SUM(karikataKingaku)", joken)) * -1;
            //                kashikataKingaku = Util.ToDecimal(KanjoKamokuKingaku.Compute("SUM(kashikataKingaku)", joken)) * -1;
            //                zandakaKingaku = Util.ToDecimal(KanjoKamokuKingaku.Compute("SUM(touzanKingaku)", joken)) * -1;
            //            }
            //            // 勘定科目が【当期未処分剰余金】の場合 // 未処分剰余金
            //            //if (kamokuBunrui == 22400)
            //            if (kamokuBunrui == this._pForm.KAMOKU_BUNRUI_TOKI_MISYOBUN_JOYOKIN)
            //            {
            //                //zandakaKingaku = zandakaKingaku * -1;
            //                //zenZanKingaku = zenZanKingaku * -1;
            //                karikataKingaku = TaishakuHoteiJunbikin[0];
            //                // 借方>貸方の場合
            //                if (karikataKingaku > kashikataKingaku)
            //                {
            //                    // 借方発生
            //                    tmpSumKarikataKingaku = Util.ToString(karikataKingaku - kashikataKingaku - hoteJunbikin);
            //                    // 貸方発生
            //                    tmpSumKashikataKingaku = "";
            //                    zandakaKingaku = zenZanKingaku - karikataKingaku + tokiMishobunJoyokin;
            //                }
            //                // 上記以外の場合
            //                else
            //                {
            //                    // 借方発生
            //                    tmpSumKarikataKingaku = Util.ToString(karikataKingaku);
            //                    //// 借方発生
            //                    //tmpSumKarikataKingaku = "";
            //                    // 貸方発生
            //                    // 名護へ合わせ
            //                    //tmpSumKashikataKingaku = "";
            //                    tmpSumKashikataKingaku = Util.ToString(kashikataKingaku);
            //                    zandakaKingaku = zenZanKingaku - karikataKingaku + kashikataKingaku + tokiMishobunJoyokin;
            //                    //zandakaKingaku = zenZanKingaku - karikataKingaku + kashikataKingaku + tokiMishobunJoyokin;
            //                    zenZanKingaku = zandakaKingaku - karikataKingaku + kashikataKingaku;
            //                }
            //            }
            //            // 残高
            //            if (zenZanKingaku == 0)
            //            {
            //                tmpSumZandakaKingaku = "";
            //            }
            //            else if (zenZanKingaku < 0)
            //            {
            //                // tmpSumZandakaKingaku = Util.ToString(zenZanKingaku - hoteJunbikin + tokiMishobunJoyokin);
            //                tmpSumZandakaKingaku = Util.ToString(zenZanKingaku);
            //                color01 = Color.Red;
            //            }
            //            else
            //            {
            //                // tmpSumZandakaKingaku = Util.ToString(zenZanKingaku - hoteJunbikin + tokiMishobunJoyokin);
            //                tmpSumZandakaKingaku = Util.ToString(zenZanKingaku);
            //            }
            //            // 借方発生
            //            if (karikataKingaku == 0)
            //            {
            //                tmpSumKarikataKingaku = "";
            //            }
            //            else if (karikataKingaku < 0)
            //            {
            //                tmpSumKarikataKingaku = Util.ToString(karikataKingaku);
            //                color02 = Color.Red;
            //            }
            //            else
            //            {
            //                tmpSumKarikataKingaku = Util.ToString(karikataKingaku);
            //            }
            //            // 貸方発生
            //            if (kashikataKingaku == 0)
            //            {
            //                tmpSumKashikataKingaku = "";
            //            }
            //            else if (kashikataKingaku < 0)
            //            {
            //                tmpSumKashikataKingaku = Util.ToString(kashikataKingaku);
            //                color03 = Color.Red;
            //            }
            //            else
            //            {
            //                tmpSumKashikataKingaku = Util.ToString(kashikataKingaku);
            //            }
            //            // 当残
            //            if (zandakaKingaku == 0)
            //            {
            //                tmpSumTouzanKingaku = "";
            //                tokiMishobunzenZanKingakuSoneki[0] = zandakaKingaku;
            //            }
            //            else if (zandakaKingaku < 0)
            //            {
            //                // tmpSumTouzanKingaku = Util.ToString(zandakaKingaku - hoteJunbikin + tokiMishobunJoyokin);
            //                tmpSumTouzanKingaku = Util.ToString(zandakaKingaku);
            //                tokiMishobunzenZanKingakuSoneki[0] = zandakaKingaku;
            //                color04 = Color.Red;
            //            }
            //            else
            //            {
            //                // tmpSumTouzanKingaku = Util.ToString(zandakaKingaku - hoteJunbikin + tokiMishobunJoyokin);
            //                tmpSumTouzanKingaku = Util.ToString(zandakaKingaku);
            //                tokiMishobunzenZanKingakuSoneki[0] = zandakaKingaku;
            //            }
            //        }
            //        // 集計区分が3の場合
            //        else if (shukeiKubun == 3)
            //        {
            //            #region 集計計算式の"+"と"-"を分別
            //            while (flag == 0)
            //            {
            //                // "+"と"-"の文字を検索
            //                findPlus = shukeiKeisanShiki.IndexOf("+");
            //                findMinus = shukeiKeisanShiki.IndexOf("-");
            //                // "+"も"-"も無ければ、処理終了
            //                if (findPlus <= 0 && findMinus <= 0)
            //                {
            //                    if (first == 0)
            //                    {
            //                        stTargetPlus.Add(shukeiKeisanShiki);
            //                    }
            //                    else
            //                    {
            //                        if (plusMinusCheckFlag == 0)
            //                        {
            //                            stTargetPlus.Add(shukeiKeisanShiki);
            //                        }
            //                        else if (plusMinusCheckFlag == 1)
            //                        {
            //                            stTargetMinus.Add(shukeiKeisanShiki);
            //                        }
            //                    }
            //                    flag = 1;
            //                }
            //                // "+"又は"-"があれば分けて格納
            //                else
            //                {
            //                    // 最初の処理 1つ目の項目を格納し、文字列から削除
            //                    if (first == 0)
            //                    {
            //                        // "+"の時の処理
            //                        if ((findMinus > findPlus && findPlus > 0) || (findPlus > findMinus && findMinus <= 0))
            //                        {
            //                            stTargetPlus.Add(shukeiKeisanShiki.Substring(0, findPlus));
            //                            shukeiKeisanShiki = shukeiKeisanShiki.Substring(findPlus);
            //                        }
            //                        // "-"の時の処理
            //                        else if ((findPlus > findMinus && findMinus > 0) || (findMinus > findPlus && findPlus <= 0))
            //                        {
            //                            stTargetPlus.Add(shukeiKeisanShiki.Substring(0, findMinus));
            //                            shukeiKeisanShiki = shukeiKeisanShiki.Substring(findMinus);
            //                        }
            //                    }
            //                    // 2つ目以降の処理 項目を順番に格納し、文字列から削除
            //                    else
            //                    {
            //                        // 削除する文字数を取得
            //                        if ((findPlus > findMinus && findMinus > 0) || (findMinus > findPlus && findPlus <= 0))
            //                        {
            //                            deleteCount = findMinus;
            //                        }
            //                        else
            //                        {
            //                            deleteCount = findPlus;
            //                        }
            //                        // 項目を格納
            //                        if (plusMinusCheckFlag == 0)
            //                        {
            //                            stTargetPlus.Add(shukeiKeisanShiki.Substring(0, deleteCount));
            //                            shukeiKeisanShiki = shukeiKeisanShiki.Substring(deleteCount);
            //                        }
            //                        else if (plusMinusCheckFlag == 1)
            //                        {
            //                            stTargetMinus.Add(shukeiKeisanShiki.Substring(0, deleteCount));
            //                            shukeiKeisanShiki = shukeiKeisanShiki.Substring(deleteCount);
            //                        }
            //                    }
            //                    // "+"と"-"の文字を検索
            //                    findPlus = shukeiKeisanShiki.IndexOf("+");
            //                    findMinus = shukeiKeisanShiki.IndexOf("-");
            //                    // 次の項目がが"+"か"-"なのかの判断フラグを設定
            //                    if (findPlus == 0)
            //                    {
            //                        plusMinusCheckFlag = 0;
            //                    }
            //                    else if (findMinus == 0)
            //                    {
            //                        plusMinusCheckFlag = 1;
            //                    }
            //                    // "+"又は"-"の文字を削除
            //                    shukeiKeisanShiki = shukeiKeisanShiki.Substring(1);
            //                }
            //                first = 1;
            //            }
            //            #endregion

            //            #region 条件式の作成
            //            // "+"の項目の条件式を作成
            //            j = 0;
            //            while (stTargetPlus.Count > j)
            //            {
            //                if (j == 0)
            //                {
            //                    jokenPlus = "shukeiKeisanShiki = " + stTargetPlus[j];
            //                }
            //                else
            //                {
            //                    jokenPlus += " OR shukeiKeisanShiki = " + stTargetPlus[j];
            //                }

            //                j++;
            //            }
            //            // "-"の項目の条件式を作成
            //            j = 0;
            //            while (stTargetMinus.Count > j)
            //            {
            //                if (j == 0)
            //                {
            //                    jokenMinus = "shukeiKeisanShiki = " + stTargetMinus[j];
            //                }
            //                else
            //                {
            //                    jokenMinus += " OR shukeiKeisanShiki = " + stTargetMinus[j];
            //                }

            //                j++;
            //            }
            //            #endregion

            //            #region 残高金額、借方金額、貸方金額、当残金額の合計値を計算
            //            if (jokenPlus != "")
            //            {
            //                sumZandakaKingakuPlus = KanjoKamokuKingaku.Compute("SUM(zandakaKingaku)", jokenPlus);
            //                sumKarikataKingakuPlus = KanjoKamokuKingaku.Compute("SUM(karikataKingaku)", jokenPlus);
            //                sumKashikataKingakuPlus = KanjoKamokuKingaku.Compute("SUM(kashikataKingaku)", jokenPlus);
            //                sumTouzanKingakuPlus = KanjoKamokuKingaku.Compute("SUM(touzanKingaku)", jokenPlus);
            //            }
            //            else
            //            {
            //                sumZandakaKingakuPlus = 0;
            //                sumKarikataKingakuPlus = 0;
            //                sumKashikataKingakuPlus = 0;
            //                sumTouzanKingakuPlus = 0;
            //            }
            //            if (jokenMinus != "")
            //            {
            //                sumZandakaKingakuMinus = KanjoKamokuKingaku.Compute("SUM(zandakaKingaku)", jokenMinus);
            //                sumKarikataKingakuMinus = KanjoKamokuKingaku.Compute("SUM(karikataKingaku)", jokenMinus);
            //                sumKashikataKingakuMinus = KanjoKamokuKingaku.Compute("SUM(kashikataKingaku)", jokenMinus);
            //                sumTouzanKingakuMinus = KanjoKamokuKingaku.Compute("SUM(touzanKingaku)", jokenMinus);
            //            }
            //            else
            //            {
            //                sumZandakaKingakuMinus = 0;
            //                sumKarikataKingakuMinus = 0;
            //                sumKashikataKingakuMinus = 0;
            //                sumTouzanKingakuMinus = 0;
            //            }

            //            zenZanKingaku = Util.ToDecimal(sumZandakaKingakuPlus) + Util.ToDecimal(sumZandakaKingakuMinus);
            //            karikataKingaku = Util.ToDecimal(sumKarikataKingakuMinus) - Util.ToDecimal(sumKarikataKingakuPlus);
            //            kashikataKingaku = Util.ToDecimal(sumKashikataKingakuMinus) - Util.ToDecimal(sumKashikataKingakuPlus);
            //            zandakaKingaku = Util.ToDecimal(sumTouzanKingakuPlus) + Util.ToDecimal(sumTouzanKingakuMinus);

            //            #region 最後の勘定科目"【未処分剰余金】"の場合
            //            if (F.Rows.Count - 1 == i)
            //            {
            //                // 対象区分が借方の場合
            //                if (daiKomokuTaishakuKubun == 1)
            //                {
            //                    karikataKingaku = zandakaKingaku - zenZanKingaku;
            //                    kashikataKingaku = zandakaKingaku + zenZanKingaku;

            //                    zandakaKingaku = tokiMishobunzenZanKingakuSoneki[0] + karikataKingaku;
            //                    // 当残が0以上の場合、借方発生に記載
            //                    if (zandakaKingaku >= 0)
            //                    {
            //                        // 残高
            //                        /*if (zenZanKingaku == 0)
            //                        {
            //                            tmpSumZandakaKingaku = "";
            //                        }
            //                        else if (zenZanKingaku < 0)
            //                        {
            //                            tmpSumZandakaKingaku = Util.ToString(zenZanKingaku);
            //                            color01 = Color.Red;
            //                        }
            //                        else
            //                        {
            //                            tmpSumZandakaKingaku = Util.ToString(zenZanKingaku);
            //                        }*/
            //                        // 残高
            //                        if (tokiMishobunzenZanKingakuSoneki[0] == 0)
            //                        {
            //                            tmpSumZandakaKingaku = "";
            //                        }
            //                        else if (tokiMishobunzenZanKingakuSoneki[0] < 0)
            //                        {
            //                            tmpSumZandakaKingaku = Util.ToString(tokiMishobunzenZanKingakuSoneki[0] - hoteJunbikin + tokiMishobunJoyokin);
            //                            color01 = Color.Red;
            //                        }
            //                        else
            //                        {
            //                            tmpSumZandakaKingaku = Util.ToString(tokiMishobunzenZanKingakuSoneki[0] - hoteJunbikin + tokiMishobunJoyokin);
            //                        }
            //                        // 借方発生
            //                        if (kashikataKingaku == 0)
            //                        {
            //                            tmpSumKarikataKingaku = "";
            //                        }
            //                        else if (kashikataKingaku < 0)
            //                        {
            //                            tmpSumKarikataKingaku = Util.ToString(karikataKingaku);
            //                            color02 = Color.Red;
            //                        }
            //                        else
            //                        {
            //                            tmpSumKarikataKingaku = Util.ToString(karikataKingaku);
            //                        }
            //                        // 貸方発生
            //                        tmpSumKashikataKingaku = Util.ToString(kashikataKingaku);
            //                        // 当残
            //                        if (zandakaKingaku == 0)
            //                        {
            //                            tmpSumTouzanKingaku = "";
            //                        }
            //                        else if (zandakaKingaku < 0)
            //                        {
            //                            tmpSumTouzanKingaku = Util.ToString(zandakaKingaku - hoteJunbikin + tokiMishobunJoyokin);
            //                            color04 = Color.Red;
            //                        }
            //                        else
            //                        {
            //                            tmpSumTouzanKingaku = Util.ToString(zandakaKingaku - hoteJunbikin + tokiMishobunJoyokin);
            //                        }
            //                    }
            //                    // 0未満の場合、貸方発生に記載
            //                    else
            //                    {
            //                        // 残高
            //                        /*if (zenZanKingaku == 0)
            //                        {
            //                            tmpSumZandakaKingaku = "";
            //                        }
            //                        else if (zenZanKingaku < 0)
            //                        {
            //                            tmpSumZandakaKingaku = Util.ToString(zenZanKingaku);
            //                            color01 = Color.Red;
            //                        }
            //                        else
            //                        {
            //                            tmpSumZandakaKingaku = Util.ToString(zenZanKingaku);
            //                        }*/
            //                        // 残高
            //                        if (tokiMishobunzenZanKingakuSoneki[0] == 0)
            //                        {
            //                            tmpSumZandakaKingaku = "";
            //                        }
            //                        else if (tokiMishobunzenZanKingakuSoneki[0] < 0)
            //                        {
            //                            tmpSumZandakaKingaku = Util.ToString(tokiMishobunzenZanKingakuSoneki[0] - hoteJunbikin + tokiMishobunJoyokin);
            //                            color01 = Color.Red;
            //                        }
            //                        else
            //                        {
            //                            tmpSumZandakaKingaku = Util.ToString(tokiMishobunzenZanKingakuSoneki[0] - hoteJunbikin + tokiMishobunJoyokin);
            //                        }
            //                        // 借方発生
            //                        tmpSumKarikataKingaku = "";
            //                        // 貸方発生
            //                        if (kashikataKingaku == 0)
            //                        {
            //                            tmpSumKashikataKingaku = "";
            //                        }
            //                        else if (kashikataKingaku < 0)
            //                        {
            //                            tmpSumKashikataKingaku = Util.ToString(kashikataKingaku);
            //                            color03 = Color.Red;
            //                        }
            //                        else
            //                        {
            //                            tmpSumKashikataKingaku = Util.ToString(kashikataKingaku);
            //                        }
            //                        // 当残
            //                        if (zandakaKingaku == 0)
            //                        {
            //                            tmpSumTouzanKingaku = "";
            //                        }
            //                        else if (zandakaKingaku < 0)
            //                        {
            //                            tmpSumTouzanKingaku = Util.ToString(zandakaKingaku - hoteJunbikin + tokiMishobunJoyokin);
            //                            color04 = Color.Red;
            //                        }
            //                        else
            //                        {
            //                            tmpSumTouzanKingaku = Util.ToString(zandakaKingaku - hoteJunbikin + tokiMishobunJoyokin);
            //                        }
            //                    }
            //                }
            //                // 対象区分が貸方の場合
            //                else
            //                {
            //                    zenZanKingaku = zenZanKingaku * -1;
            //                    karikataKingaku = zandakaKingaku + zenZanKingaku;
            //                    zandakaKingaku = zandakaKingaku * -1;
            //                    kashikataKingaku = zandakaKingaku - zenZanKingaku;

            //                    zandakaKingaku = tokiMishobunzenZanKingakuSoneki[0] + kashikataKingaku;
            //                    // 当残が0以上の場合、貸方発生に記載
            //                    if (zandakaKingaku >= 0)
            //                    {
            //                        // 残高
            //                        /*if (zenZanKingaku == 0)
            //                        {
            //                            tmpSumZandakaKingaku = "";
            //                        }
            //                        else if (zenZanKingaku < 0)
            //                        {
            //                            tmpSumZandakaKingaku = Util.ToString(zenZanKingaku);
            //                            color01 = Color.Red;
            //                        }
            //                        else
            //                        {
            //                            tmpSumZandakaKingaku = Util.ToString(zenZanKingaku);
            //                        }*/
            //                        // 残高
            //                        if (tokiMishobunzenZanKingakuSoneki[0] == 0)
            //                        {
            //                            tmpSumZandakaKingaku = "";
            //                        }
            //                        else if (tokiMishobunzenZanKingakuSoneki[0] < 0)
            //                        {
            //                            // 名護へ合わせ
            //                            //tmpSumZandakaKingaku = Util.ToString(tokiMishobunzenZanKingakuSoneki[0] - hoteJunbikin + tokiMishobunJoyokin);
            //                            tmpSumZandakaKingaku = Util.ToString(tokiMishobunzenZanKingakuSoneki[0] - hoteJunbikin);
            //                            color01 = Color.Red;
            //                        }
            //                        else
            //                        {
            //                            // 名護へ合わせ
            //                            //tmpSumZandakaKingaku = Util.ToString(tokiMishobunzenZanKingakuSoneki[0] - hoteJunbikin + tokiMishobunJoyokin);
            //                            tmpSumZandakaKingaku = Util.ToString(tokiMishobunzenZanKingakuSoneki[0] - hoteJunbikin);
            //                        }
            //                        // 借方発生
            //                        tmpSumKarikataKingaku = "";
            //                        // 貸方発生
            //                        if (kashikataKingaku == 0)
            //                        {
            //                            tmpSumKashikataKingaku = "";
            //                        }
            //                        else if (kashikataKingaku < 0)
            //                        {
            //                            tmpSumKashikataKingaku = Util.ToString(kashikataKingaku);
            //                            color03 = Color.Red;
            //                        }
            //                        else
            //                        {
            //                            tmpSumKashikataKingaku = Util.ToString(kashikataKingaku);
            //                        }
            //                        // 当残
            //                        if (zandakaKingaku == 0)
            //                        {
            //                            tmpSumTouzanKingaku = "";
            //                        }
            //                        else if (zandakaKingaku < 0)
            //                        {
            //                            // 名護へ合わせ
            //                            //tmpSumTouzanKingaku = Util.ToString(zandakaKingaku - hoteJunbikin + tokiMishobunJoyokin);
            //                            tmpSumTouzanKingaku = Util.ToString(zandakaKingaku - hoteJunbikin);
            //                            color04 = Color.Red;
            //                        }
            //                        else
            //                        {
            //                            // 名護へ合わせ
            //                            //tmpSumTouzanKingaku = Util.ToString(zandakaKingaku - hoteJunbikin + tokiMishobunJoyokin);
            //                            tmpSumTouzanKingaku = Util.ToString(zandakaKingaku - hoteJunbikin);
            //                        }
            //                    }
            //                    // 0未満の場合、借方発生に記載
            //                    else
            //                    {
            //                        // 残高
            //                        /*if (zenZanKingaku == 0)
            //                        {
            //                            tmpSumZandakaKingaku = "";
            //                        }
            //                        else if (zenZanKingaku < 0)
            //                        {
            //                            tmpSumZandakaKingaku = Util.ToString(zenZanKingaku);
            //                            color01 = Color.Red;
            //                        }
            //                        else
            //                        {
            //                            tmpSumZandakaKingaku = Util.ToString(zenZanKingaku);
            //                        }*/
            //                        // 残高
            //                        if (tokiMishobunzenZanKingakuSoneki[0] == 0)
            //                        {
            //                            tmpSumZandakaKingaku = "";
            //                        }
            //                        else if (tokiMishobunzenZanKingakuSoneki[0] < 0)
            //                        {
            //                            // 名護へ合わせ
            //                            //tmpSumZandakaKingaku = Util.ToString(tokiMishobunzenZanKingakuSoneki[0] - hoteJunbikin + tokiMishobunJoyokin);
            //                            tmpSumZandakaKingaku = Util.ToString(tokiMishobunzenZanKingakuSoneki[0] - hoteJunbikin);
            //                            color01 = Color.Red;
            //                        }
            //                        else
            //                        {
            //                            // 名護へ合わせ
            //                            //tmpSumZandakaKingaku = Util.ToString(tokiMishobunzenZanKingakuSoneki[0] - hoteJunbikin + tokiMishobunJoyokin);
            //                            tmpSumZandakaKingaku = Util.ToString(tokiMishobunzenZanKingakuSoneki[0] - hoteJunbikin);
            //                        }
            //                        // 借方発生
            //                        if (karikataKingaku == 0)
            //                        {
            //                            tmpSumKarikataKingaku = "";
            //                        }
            //                        else if (karikataKingaku < 0)
            //                        {
            //                            tmpSumKarikataKingaku = Util.ToString(karikataKingaku);
            //                            color02 = Color.Red;
            //                        }
            //                        else
            //                        {
            //                            tmpSumKarikataKingaku = Util.ToString(karikataKingaku);
            //                        }
            //                        // 貸方発生
            //                        tmpSumKashikataKingaku = "";
            //                        // 当残
            //                        if (zandakaKingaku == 0)
            //                        {
            //                            tmpSumTouzanKingaku = "";
            //                        }
            //                        else if (zandakaKingaku < 0)
            //                        {
            //                            // 名護へ合わせ
            //                            //tmpSumTouzanKingaku = Util.ToString(zandakaKingaku - hoteJunbikin + tokiMishobunJoyokin);
            //                            tmpSumTouzanKingaku = Util.ToString(zandakaKingaku - hoteJunbikin);
            //                            color04 = Color.Red;
            //                        }
            //                        else
            //                        {
            //                            // 名護へ合わせ
            //                            //tmpSumTouzanKingaku = Util.ToString(zandakaKingaku - hoteJunbikin + tokiMishobunJoyokin);
            //                            tmpSumTouzanKingaku = Util.ToString(zandakaKingaku - hoteJunbikin);
            //                        }
            //                    }
            //                }
            //            }
            //            #endregion
            //            #region それ以外の場合
            //            else
            //            {
            //                // 貸借区分が借方の場合
            //                if (daiKomokuTaishakuKubun == 1)
            //                {
            //                    // 借方>貸方の場合
            //                    if (karikataKingaku > kashikataKingaku)
            //                    {
            //                        // 借方発生
            //                        tmpSumKarikataKingaku = "";
            //                        // 貸方発生
            //                        tmpSumKashikataKingaku = Util.ToString((kashikataKingaku - karikataKingaku) * - 1);
            //                    }
            //                    // 借方<貸方の場合
            //                    else if (karikataKingaku < kashikataKingaku)
            //                    {
            //                        // 借方発生
            //                        tmpSumKarikataKingaku = Util.ToString((karikataKingaku - kashikataKingaku) * -1);
            //                        // 貸方発生
            //                        tmpSumKashikataKingaku = "";
            //                    }
            //                    // 借方==貸方の場合
            //                    else
            //                    {
            //                        // 借方発生
            //                        tmpSumKarikataKingaku = "";
            //                        // 貸方発生
            //                        tmpSumKashikataKingaku = "";
            //                    }
            //                }
            //                // 貸借区分が貸方の場合
            //                else
            //                {
            //                    zandakaKingaku = zandakaKingaku * -1;
            //                    zenZanKingaku = zenZanKingaku * -1;
            //                    // karikataKingaku = zenZanKingaku - zandakaKingaku;
            //                    // 借方>貸方の場合
            //                    if (karikataKingaku > kashikataKingaku)
            //                    {
            //                        // 借方発生
            //                        tmpSumKarikataKingaku = Util.ToString(karikataKingaku - kashikataKingaku);
            //                        // 貸方発生
            //                        tmpSumKashikataKingaku = "";
            //                    }
            //                    // 借方<貸方の場合
            //                    else if (karikataKingaku < kashikataKingaku)
            //                    {
            //                        // 借方発生
            //                        tmpSumKarikataKingaku = "";
            //                        // 貸方発生
            //                        tmpSumKashikataKingaku = Util.ToString(kashikataKingaku - karikataKingaku);
            //                    }
            //                    // 借方==貸方の場合
            //                    else
            //                    {
            //                        // 借方発生
            //                        tmpSumKarikataKingaku = "";
            //                        // 貸方発生
            //                        tmpSumKashikataKingaku = "";
            //                    }
            //                }

            //                // 残高
            //                if (zenZanKingaku == 0)
            //                {
            //                    tmpSumZandakaKingaku = "";
            //                }
            //                else if (zenZanKingaku < 0)
            //                {
            //                    tmpSumZandakaKingaku = Util.ToString(zenZanKingaku);
            //                    color01 = Color.Red;
            //                }
            //                else
            //                {
            //                    tmpSumZandakaKingaku = Util.ToString(zenZanKingaku);
            //                }
            //                // 当残
            //                if (zandakaKingaku == 0)
            //                {
            //                    tmpSumTouzanKingaku = "";
            //                }
            //                else if (zandakaKingaku < 0)
            //                {
            //                    tmpSumTouzanKingaku = Util.ToString(zandakaKingaku);
            //                    color04 = Color.Red;
            //                }
            //                else
            //                {
            //                    tmpSumTouzanKingaku = Util.ToString(zandakaKingaku);
            //                }
            //            }
            //            #endregion
            //            #endregion
            //        }
            //    }
            //    #endregion

            //    #region 製造原価報告書の大項目
            //    else if (LIST_F == 3)
            //    {
            //        flag = 0;
            //        jokenPlus = "";
            //        jokenMinus = "";
            //        // 明細区分が0以外の場合
            //        if (meisaiKubun != 0)
            //        {
            //            joken = "shukeiKeisanShiki = " + shukeiKeisanShiki;
            //            // 残高金額、借方金額、貸方金額、当残金額の合計値を計算
            //            tmpSumZandakaKingaku = Util.ToString(KanjoKamokuKingaku.Compute("SUM(zandakaKingaku)", joken));
            //            tmpSumKarikataKingaku = Util.ToString(KanjoKamokuKingaku.Compute("SUM(karikataKingaku)", joken));
            //            tmpSumKashikataKingaku = Util.ToString(KanjoKamokuKingaku.Compute("SUM(kashikataKingaku)", joken));
            //            tmpSumTouzanKingaku = Util.ToString(KanjoKamokuKingaku.Compute("SUM(touzanKingaku)", joken));
            //        }
            //        // 明細区分が0の場合
            //        else
            //        {
            //            #region 集計計算式の"+"と"-"を分別
            //            while (flag == 0)
            //            {
            //                // "+"と"-"の文字を検索
            //                findPlus = shukeiKeisanShiki.IndexOf("+");
            //                findMinus = shukeiKeisanShiki.IndexOf("-");
            //                // "+"も"-"も無ければ、処理終了
            //                if (findPlus <= 0 && findMinus <= 0)
            //                {
            //                    if (first == 0)
            //                    {
            //                        stTargetPlus.Add(shukeiKeisanShiki);
            //                    }
            //                    else
            //                    {
            //                        if (plusMinusCheckFlag == 0)
            //                        {
            //                            stTargetPlus.Add(shukeiKeisanShiki);
            //                        }
            //                        else if (plusMinusCheckFlag == 1)
            //                        {
            //                            stTargetMinus.Add(shukeiKeisanShiki);
            //                        }
            //                    }
            //                    flag = 1;
            //                }
            //                // "+"又は"-"があれば分けて格納
            //                else
            //                {
            //                    // 最初の処理 1つ目の項目を格納し、文字列から削除
            //                    if (first == 0)
            //                    {
            //                        // "+"の時の処理
            //                        if ((findMinus > findPlus && findPlus > 0) || (findPlus > findMinus && findMinus <= 0))
            //                        {
            //                            stTargetPlus.Add(shukeiKeisanShiki.Substring(0, findPlus));
            //                            shukeiKeisanShiki = shukeiKeisanShiki.Substring(findPlus);
            //                        }
            //                        // "-"の時の処理
            //                        else if ((findPlus > findMinus && findMinus > 0) || (findMinus > findPlus && findPlus <= 0))
            //                        {
            //                            stTargetPlus.Add(shukeiKeisanShiki.Substring(0, findMinus));
            //                            shukeiKeisanShiki = shukeiKeisanShiki.Substring(findMinus);
            //                        }
            //                    }
            //                    // 2つ目以降の処理 項目を順番に格納し、文字列から削除
            //                    else
            //                    {
            //                        // 削除する文字数を取得
            //                        if ((findPlus > findMinus && findMinus > 0) || (findMinus > findPlus && findPlus <= 0))
            //                        {
            //                            deleteCount = findMinus;
            //                        }
            //                        else
            //                        {
            //                            deleteCount = findPlus;
            //                        }
            //                        // 項目を格納
            //                        if (plusMinusCheckFlag == 0)
            //                        {
            //                            stTargetPlus.Add(shukeiKeisanShiki.Substring(0, deleteCount));
            //                            shukeiKeisanShiki = shukeiKeisanShiki.Substring(deleteCount);
            //                        }
            //                        else if (plusMinusCheckFlag == 1)
            //                        {
            //                            stTargetMinus.Add(shukeiKeisanShiki.Substring(0, deleteCount));
            //                            shukeiKeisanShiki = shukeiKeisanShiki.Substring(deleteCount);
            //                        }
            //                    }
            //                    // "+"と"-"の文字を検索
            //                    findPlus = shukeiKeisanShiki.IndexOf("+");
            //                    findMinus = shukeiKeisanShiki.IndexOf("-");
            //                    // 次の項目がが"+"か"-"なのかの判断フラグを設定
            //                    if (findPlus == 0)
            //                    {
            //                        plusMinusCheckFlag = 0;
            //                    }
            //                    else if (findMinus == 0)
            //                    {
            //                        plusMinusCheckFlag = 1;
            //                    }
            //                    // "+"又は"-"の文字を削除
            //                    shukeiKeisanShiki = shukeiKeisanShiki.Substring(1);
            //                }
            //                first = 1;
            //            }
            //            #endregion

            //            #region 条件式の作成
            //            // "+"の項目の条件式を作成
            //            j = 0;
            //            while (stTargetPlus.Count > j)
            //            {
            //                if (j == 0)
            //                {
            //                    jokenPlus = "shukeiKeisanShiki = " + stTargetPlus[j];
            //                }
            //                else
            //                {
            //                    jokenPlus += " OR shukeiKeisanShiki = " + stTargetPlus[j];
            //                }

            //                j++;
            //            }
            //            // "-"の項目の条件式を作成
            //            j = 0;
            //            while (stTargetMinus.Count > j)
            //            {
            //                if (j == 0)
            //                {
            //                    jokenMinus = "shukeiKeisanShiki = " + stTargetMinus[j];
            //                }
            //                else
            //                {
            //                    jokenMinus += " OR shukeiKeisanShiki = " + stTargetMinus[j];
            //                }

            //                j++;
            //            }
            //            #endregion

            //            #region 残高金額、借方金額、貸方金額、当残金額の合計値を計算
            //            if (jokenPlus != "")
            //            {
            //                sumZandakaKingakuPlus = KanjoKamokuKingaku.Compute("SUM(zandakaKingaku)", jokenPlus);
            //                sumKarikataKingakuPlus = KanjoKamokuKingaku.Compute("SUM(karikataKingaku)", jokenPlus);
            //                sumKashikataKingakuPlus = KanjoKamokuKingaku.Compute("SUM(kashikataKingaku)", jokenPlus);
            //                sumTouzanKingakuPlus = KanjoKamokuKingaku.Compute("SUM(touzanKingaku)", jokenPlus);
            //            }
            //            else
            //            {
            //                sumZandakaKingakuPlus = 0;
            //                sumKarikataKingakuPlus = 0;
            //                sumKashikataKingakuPlus = 0;
            //                sumTouzanKingakuPlus = 0;
            //            }
            //            if (jokenMinus != "")
            //            {
            //                sumZandakaKingakuMinus = KanjoKamokuKingaku.Compute("SUM(zandakaKingaku)", jokenMinus);
            //                sumKarikataKingakuMinus = KanjoKamokuKingaku.Compute("SUM(karikataKingaku)", jokenMinus);
            //                sumKashikataKingakuMinus = KanjoKamokuKingaku.Compute("SUM(kashikataKingaku)", jokenMinus);
            //                sumTouzanKingakuMinus = KanjoKamokuKingaku.Compute("SUM(touzanKingaku)", jokenMinus);
            //            }
            //            else
            //            {
            //                sumZandakaKingakuMinus = 0;
            //                sumKarikataKingakuMinus = 0;
            //                sumKashikataKingakuMinus = 0;
            //                sumTouzanKingakuMinus = 0;
            //            }

            //            zenZanKingaku = Util.ToDecimal(sumZandakaKingakuPlus) - Util.ToDecimal(sumZandakaKingakuMinus);
            //            karikataKingaku = Util.ToDecimal(sumKarikataKingakuPlus) - Util.ToDecimal(sumKarikataKingakuMinus);
            //            kashikataKingaku = Util.ToDecimal(sumKashikataKingakuPlus) - Util.ToDecimal(sumKashikataKingakuMinus);
            //            zandakaKingaku = Util.ToDecimal(sumTouzanKingakuPlus) - Util.ToDecimal(sumTouzanKingakuMinus);

            //            #region 最後の勘定科目"【未処分剰余金】"の場合
            //            if (F.Rows.Count == i)
            //            {
            //                // 残高
            //                if (zenZanKingaku == 0)
            //                {
            //                    tmpSumZandakaKingaku = "";
            //                }
            //                else if (zenZanKingaku < 0)
            //                {
            //                    tmpSumZandakaKingaku = Util.ToString(zenZanKingaku);
            //                    color01 = Color.Red;
            //                }
            //                else
            //                {
            //                    tmpSumZandakaKingaku = Util.ToString(zenZanKingaku);
            //                }
            //                // 借方発生
            //                if (karikataKingaku == 0)
            //                {
            //                    tmpSumKarikataKingaku = "";
            //                }
            //                else if (karikataKingaku < 0)
            //                {
            //                    tmpSumKarikataKingaku = Util.ToString(karikataKingaku);
            //                    color02 = Color.Red;
            //                }
            //                else
            //                {
            //                    tmpSumKarikataKingaku = Util.ToString(karikataKingaku);
            //                }
            //                // 貸方発生
            //                if (kashikataKingaku == 0)
            //                {
            //                    tmpSumKashikataKingaku = "";
            //                }
            //                else if (kashikataKingaku < 0)
            //                {
            //                    tmpSumKashikataKingaku = Util.ToString(kashikataKingaku);
            //                    color03 = Color.Red;
            //                }
            //                else
            //                {
            //                    tmpSumKashikataKingaku = Util.ToString(kashikataKingaku);
            //                }
            //                // 当残
            //                if (zandakaKingaku == 0)
            //                {
            //                    tmpSumTouzanKingaku = "";
            //                }
            //                else if (kashikataKingaku < 0)
            //                {
            //                    tmpSumTouzanKingaku = Util.ToString(zandakaKingaku);
            //                    color04 = Color.Red;
            //                }
            //                else
            //                {
            //                    tmpSumTouzanKingaku = Util.ToString(zandakaKingaku);
            //                }
            //            }
            //            #endregion
            //            #region それ以外の場合
            //            else
            //            {
            //                // 対象区分が借方の場合
            //                if (daiKomokuTaishakuKubun == 1)
            //                {
            //                    // 当残が0以上の場合、借方発生に記載
            //                    if (zandakaKingaku >= 0)
            //                    {
            //                        // 残高
            //                        if (zenZanKingaku == 0)
            //                        {
            //                            tmpSumZandakaKingaku = "";
            //                        }
            //                        else if (zenZanKingaku < 0)
            //                        {
            //                            tmpSumZandakaKingaku = Util.ToString(zenZanKingaku);
            //                            color01 = Color.Red;
            //                        }
            //                        else
            //                        {
            //                            tmpSumZandakaKingaku = Util.ToString(zenZanKingaku);
            //                        }
            //                        // 借方発生
            //                        if (zandakaKingaku == 0)
            //                        {
            //                            tmpSumKarikataKingaku = "";
            //                        }
            //                        else
            //                        {
            //                            tmpSumKarikataKingaku = Util.ToString(zandakaKingaku);
            //                        }
            //                        // 貸方発生
            //                        tmpSumKashikataKingaku = "";
            //                        // 当残
            //                        if (zandakaKingaku == 0)
            //                        {
            //                            tmpSumTouzanKingaku = "";
            //                        }
            //                        else if (zandakaKingaku < 0)
            //                        {
            //                            tmpSumTouzanKingaku = Util.ToString(zandakaKingaku);
            //                            color04 = Color.Red;
            //                        }
            //                        else
            //                        {
            //                            tmpSumTouzanKingaku = Util.ToString(zandakaKingaku);
            //                        }
            //                    }
            //                    // 0未満の場合、貸方発生に記載
            //                    else
            //                    {
            //                        // 残高
            //                        if (zenZanKingaku == 0)
            //                        {
            //                            tmpSumZandakaKingaku = "";
            //                        }
            //                        else if (zenZanKingaku < 0)
            //                        {
            //                            tmpSumZandakaKingaku = Util.ToString(zenZanKingaku);
            //                            color01 = Color.Red;
            //                        }
            //                        else
            //                        {
            //                            tmpSumZandakaKingaku = Util.ToString(zenZanKingaku);
            //                        }
            //                        // 借方発生
            //                        tmpSumKarikataKingaku = "";
            //                        // 貸方発生
            //                        if (zandakaKingaku == 0)
            //                        {
            //                            tmpSumKashikataKingaku = "";
            //                        }
            //                        else
            //                        {
            //                            tmpSumKashikataKingaku = Util.ToString(zandakaKingaku * -1);
            //                        }
            //                        // 当残
            //                        if (zandakaKingaku == 0)
            //                        {
            //                            tmpSumTouzanKingaku = "";
            //                        }
            //                        else if (zandakaKingaku < 0)
            //                        {
            //                            tmpSumTouzanKingaku = Util.ToString(zandakaKingaku);
            //                            color04 = Color.Red;
            //                        }
            //                        else
            //                        {
            //                            tmpSumTouzanKingaku = Util.ToString(zandakaKingaku);
            //                        }
            //                    }
            //                }
            //                // 対象区分が貸方の場合
            //                else
            //                {
            //                    // 当残が0以上の場合、貸方発生に記載
            //                    if (zandakaKingaku >= 0)
            //                    {
            //                        // 残高
            //                        if (zenZanKingaku == 0)
            //                        {
            //                            tmpSumZandakaKingaku = "";
            //                        }
            //                        else if (zenZanKingaku < 0)
            //                        {
            //                            tmpSumZandakaKingaku = Util.ToString(zenZanKingaku);
            //                            color01 = Color.Red;
            //                        }
            //                        else
            //                        {
            //                            tmpSumZandakaKingaku = Util.ToString(zenZanKingaku);
            //                        }
            //                        // 借方発生
            //                        tmpSumKarikataKingaku = "";
            //                        // 貸方発生
            //                        if (zandakaKingaku == 0)
            //                        {
            //                            tmpSumKashikataKingaku = "";
            //                        }
            //                        else
            //                        {
            //                            tmpSumKashikataKingaku = Util.ToString(zandakaKingaku);
            //                        }
            //                        // 当残
            //                        if (zandakaKingaku == 0)
            //                        {
            //                            tmpSumTouzanKingaku = "";
            //                        }
            //                        else if (zandakaKingaku < 0)
            //                        {
            //                            tmpSumTouzanKingaku = Util.ToString(zandakaKingaku);
            //                            color04 = Color.Red;
            //                        }
            //                        else
            //                        {
            //                            tmpSumTouzanKingaku = Util.ToString(zandakaKingaku);
            //                        }
            //                    }
            //                    // 0未満の場合、借方発生に記載
            //                    else
            //                    {
            //                        // 残高
            //                        if (zenZanKingaku == 0)
            //                        {
            //                            tmpSumZandakaKingaku = "";
            //                        }
            //                        else if (zenZanKingaku < 0)
            //                        {
            //                            tmpSumZandakaKingaku = Util.ToString(zenZanKingaku);
            //                            color01 = Color.Red;
            //                        }
            //                        else
            //                        {
            //                            tmpSumZandakaKingaku = Util.ToString(zenZanKingaku);
            //                        }
            //                        // 借方発生
            //                        if (zandakaKingaku == 0)
            //                        {
            //                            tmpSumKarikataKingaku = "";
            //                        }
            //                        else
            //                        {
            //                            tmpSumKarikataKingaku = Util.ToString(zandakaKingaku * -1);
            //                        }
            //                        // 貸方発生
            //                        tmpSumKashikataKingaku = "";
            //                        // 当残
            //                        if (zandakaKingaku == 0)
            //                        {
            //                            tmpSumTouzanKingaku = "";
            //                        }
            //                        else if (zandakaKingaku < 0)
            //                        {
            //                            tmpSumTouzanKingaku = Util.ToString(zandakaKingaku);
            //                            color04 = Color.Red;
            //                        }
            //                        else
            //                        {
            //                            tmpSumTouzanKingaku = Util.ToString(zandakaKingaku);
            //                        }
            //                    }
            //                }
            //            }
            //            #endregion
            //            #endregion
            //        }
            //    }
            //    #endregion

            //    #region 文字種別が太字(1)の場合、背景に色を設定
            //    // 金額がｾﾞﾛの科目を印字する場合
            //    if (injiHandan == "yes")
            //    {
            //        if (kanjoKamokuIchiran.Rows.Count != 0 || meisaiKubun == 0)
            //        {
            //            this.mtbList.Rows.Add(kamokuNm, Util.FormatNum(tmpSumZandakaKingaku), 
            //                Util.FormatNum(tmpSumKarikataKingaku), Util.FormatNum(tmpSumKashikataKingaku), Util.FormatNum(tmpSumTouzanKingaku));
            //            // 文字種別が太字(1)の場合、背景に色を設定
            //            if (mojiShubetsu == 0)
            //            {
            //                this.mtbList.Rows[colorCount].DefaultCellStyle.BackColor = Color.White;
            //            }
            //            else
            //            {
            //                this.mtbList.Rows[colorCount].DefaultCellStyle.BackColor = Color.Aquamarine;
            //            }
            //            // 金額の文字色を設定
            //            this.mtbList[1, colorCount].Style.ForeColor = color01;
            //            this.mtbList[2, colorCount].Style.ForeColor = color02;
            //            this.mtbList[3, colorCount].Style.ForeColor = color03;
            //            this.mtbList[4, colorCount].Style.ForeColor = color04;
            //            colorCount++;
            //        }
            //    }
            //    // 金額がｾﾞﾛの科目を印字しない場合
            //    else
            //    {
            //        if (meisaiKubun == 0 || 
            //            ((tmpSumZandakaKingaku != "" || tmpSumKarikataKingaku != "" || tmpSumKashikataKingaku != "" || tmpSumTouzanKingaku != "") &&
            //            kanjoKamokuIchiran.Rows.Count != 0))
            //        {
            //            this.mtbList.Rows.Add(kamokuNm, Util.FormatNum(tmpSumZandakaKingaku), 
            //                Util.FormatNum(tmpSumKarikataKingaku), Util.FormatNum(tmpSumKashikataKingaku), Util.FormatNum(tmpSumTouzanKingaku));
            //            // 文字種別が太字(1)の場合、背景に色を設定
            //            if (mojiShubetsu == 0)
            //            {
            //                this.mtbList.Rows[colorCount].DefaultCellStyle.BackColor = Color.White;
            //            }
            //            else
            //            {
            //                this.mtbList.Rows[colorCount].DefaultCellStyle.BackColor = Color.Aquamarine;
            //            }
            //            // 金額の文字色を設定
            //            this.mtbList[1, colorCount].Style.ForeColor = color01;
            //            this.mtbList[2, colorCount].Style.ForeColor = color02;
            //            this.mtbList[3, colorCount].Style.ForeColor = color03;
            //            this.mtbList[4, colorCount].Style.ForeColor = color04;
            //            colorCount++;
            //        }
            //    }
            //    #endregion
            //    j = 0;
            //    i++;
            //    #endregion
            //}
            #endregion

        }
        #endregion

        #region イベント

        private void ZMMR1032_Shown(object sender, EventArgs e)
        {
            if (_viewFlag == 0)
            {
                // 表示されている情報をクリア
                this.mtbList.Rows.Clear();
                Msg.Info("該当するデータがありません。");
                this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
                this.Close();
                return;
            }
            // 自分自身をアクティブに設定
            this.Activate();
        }

        /// <summary>
        /// グリッド行移動
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mtbList_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (this._prevList == 0) return;

            if (e.RowIndex == 0) return;

            int shishoCd = Util.ToInt(Util.ToString(this._pForm.Condition["ShishoCode"]));
            if (shishoCd != 0)
            {
                this.btnF11.Enabled = false;
                this.btnF12.Enabled = false;
                string st = Util.ToString(this.mtbList["startKmkCd", e.RowIndex].Value);
                string ed = Util.ToString(this.mtbList["endKmkCd", e.RowIndex].Value);
                decimal kari = Util.ToDecimal(Util.ToString(this.mtbList["KarikataHassei", e.RowIndex].Value));
                decimal kasi = Util.ToDecimal(Util.ToString(this.mtbList["KashikataHassei", e.RowIndex].Value));
                if (st != "" && (kari != 0 || kasi != 0))
                {
                    DataTable dt = GetKanjoKamoku(Util.ToInt(st));
                    if (dt.Rows.Count > 0)
                    {
                        this.btnF11.Enabled = true;
                        if (Util.ToInt(dt.Rows[0]["HOJO_KAMOKU_UMU"]) == 1)
                        {
                            if (st == ed)
                            {
                                this.btnF12.Enabled = true;
                            }
                        }
                    }
                }
            }
            else
            {
                this.btnF11.Enabled = false;
                this.btnF12.Enabled = false;
            }
        }
        #endregion

        #region privateメソッド

        private DataTable GetDataTable()
        {

            DataTable dt = new DataTable();

            var cols = mtbList.Columns;
            foreach (DataGridViewColumn c in cols)
            {
                if (c.ValueType != null)
                {
                    dt.Columns.Add(c.Name, c.ValueType);
                }
                else
                {
                    dt.Columns.Add(c.Name);
                }
            }

            var rows = mtbList.Rows;
            foreach (DataGridViewRow r in rows)
            {
                List<object> array = new List<object>();
                foreach (DataGridViewCell cell in r.Cells)
                {
                    array.Add(cell.Value);
                }
                dt.Rows.Add(array.ToArray());
            }
            return dt;
        }

        /// <summary>
        /// 表示データの有無での表示切替
        /// </summary>
        private void shown()
        {
            if (_viewFlag == 0)
            {
                // デザインを作成(新規表示の場合)
                if (this.JudgementFlg == 1)
                {
                    InitDetailArea();
                }
                // タイトル非表示
                this.lblTitle.Visible = false;

                Msg.Info("該当するデータがありません。");
                this.DialogResult = DialogResult.Cancel;
                this.Close();
                return;
            }
            // 自分自身をアクティブに設定
            this.Activate();
        }

        /// <summary>
        /// VI_勘定科目から設定を取得
        /// </summary>
        /// <param name="kanjoKmkCd">勘定科目コード</param>
        /// <returns>VI_勘定科目から取得したデータ</returns>
        private DataTable GetKanjoKamoku(int kanjoKamokuCd)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT ");
            sql.Append("  * ");
            sql.Append("FROM ");
            sql.Append("  VI_ZM_KANJO_KAMOKU ");
            sql.Append("WHERE ");
            sql.Append("    KAISHA_CD     = @KAISHA_CD ");
            sql.Append("AND KAIKEI_NENDO  = @KAIKEI_NENDO ");
            sql.Append("AND KANJO_KAMOKU_CD = @KANJO_KAMOKU_CD ");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);
            dpc.SetParam("@KANJO_KAMOKU_CD", SqlDbType.Decimal, 6, kanjoKamokuCd);

            DataTable dtResult = this.Dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dtResult;
        }
        private DataTable GetKanjoKamoku(string kanjoKamokuCd)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT ");
            sql.Append("  * ");
            sql.Append("FROM ");
            sql.Append("  VI_ZM_KANJO_KAMOKU ");
            sql.Append("WHERE ");
            sql.Append("    KAISHA_CD     = @KAISHA_CD ");
            sql.Append("AND KAIKEI_NENDO  = @KAIKEI_NENDO ");
            sql.Append("AND KANJO_KAMOKU_CD IN( " + kanjoKamokuCd + " ) ");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@KAISHA_CD", SqlDbType.Decimal, 4, this.UInfo.KaishaCd);
            dpc.SetParam("@KAIKEI_NENDO", SqlDbType.Decimal, 4, this.UInfo.KaikeiNendo);

            DataTable dtResult = this.Dba.GetDataTableFromSqlWithParams(sql.ToString(), dpc);

            return dtResult;
        }
        #endregion
    }
}
