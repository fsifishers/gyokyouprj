﻿using System;
using System.Data;

using jp.co.fsi.common.report;
using jp.co.fsi.common.util;

using System.Globalization;

namespace jp.co.fsi.zm.zmmr1031
{
    /// <summary>
    /// ZMMR103111R の帳票
    /// </summary>
    public partial class ZMMR10311R : BaseReport
    {

        public ZMMR10311R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }

        /// <summary>
        /// ページヘッダーの設定
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pageHeader_Format(object sender, EventArgs e)
        {
            if (this.txtValue_Group.Text == "0" || this.txtValue_Group.Text == "1")
            {
                this.lblTitle.Text = "合計残高試算表［貸借対照表］";
            }
            else
            {
                this.lblTitle.Text = "合計残高試算表［損益計算書］";
            }
        }

        /// <summary>
        /// ページヘッダー(貸借区分01)の設定
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grpHeaderTaishaku01_Format(object sender, EventArgs e)
        {
            if (this.txtValue_Group.Text == "0")
            {
                this.grpHeaderTaishaku01.Visible = true;
                this.grpFooterTaishaku01.Visible = true;
            }
            else
            {
                this.grpHeaderTaishaku01.Visible = false;
                this.grpFooterTaishaku01.Visible = false;
            }

            //this.lblTitle01_03.Text = "借方";
            //this.lblTitle01_04.Text = "貸方";
            //this.lblTitle01_05.Text = "残高";
        }

        /// <summary>
        /// ページヘッダー(貸借区分02)の設定
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grpHeaderTaishaku02_Format(object sender, EventArgs e)
        {
            if (this.txtValue_Group.Text == "1")
            {
                this.grpHeaderTaishaku02.Visible = true;
                this.grpFooterTaishaku02.Visible = true;
            }
            else
            {
                this.grpHeaderTaishaku02.Visible = false;
                this.grpFooterTaishaku02.Visible = false;
            }
        }

        /// <summary>
        /// ページヘッダー(損益計算書01)の設定
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grpHeaderSoneki01_Format(object sender, EventArgs e)
        {
            if (this.txtValue_Group.Text == "2")
            {
                this.grpHeaderSoneki01.Visible = true;
                this.grpFooterSoneki01.Visible = true;
            }
            else
            {
                this.grpHeaderSoneki01.Visible = false;
                this.grpFooterSoneki01.Visible = false;
            }
        }

        /// <summary>
        /// ページヘッダー(損益計算書02)の設定
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grpHeaderSoneki02_Format(object sender, EventArgs e)
        {
            if (this.txtValue_Group.Text == "3")
            {
                this.grpHeaderSoneki02.Visible = true;
                this.grpFooterSoneki02.Visible = true;
            }
            else
            {
                this.grpHeaderSoneki02.Visible = false;
                this.grpFooterSoneki02.Visible = false;
            }
        }

        /// <summary>
        /// ページヘッダー(損益計算書03)の設定
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grpHeaderSoneki03_Format(object sender, EventArgs e)
        {
            if (this.txtValue_Group.Text == "4")
            {
                this.grpHeaderSoneki03.Visible = true;
                this.grpFooterSoneki03.Visible = true;
            }
            else
            {
                this.grpHeaderSoneki03.Visible = false;
                this.grpFooterSoneki03.Visible = false;
            }
        }

        /// <summary>
        /// ページヘッダー(損益計算書04)の設定
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grpHeaderSoneki04_Format(object sender, EventArgs e)
        {
            if (this.txtValue_Group.Text == "5")
            {
                this.grpHeaderSoneki04.Visible = true;
                this.grpFooterSoneki04.Visible = true;
            }
            else
            {
                this.grpHeaderSoneki04.Visible = false;
                this.grpFooterSoneki04.Visible = false;
            }
        }

        private void ZAMR2031R_ReportStart(object sender, EventArgs e)
        {

        }

        private void grpFooterTaishaku01_Format(object sender, EventArgs e)
        {

        }
    }
}
