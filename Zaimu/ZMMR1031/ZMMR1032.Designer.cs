﻿namespace jp.co.fsi.zm.zmmr1031
{
    partial class ZMMR1032
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.lblKaishaSettei = new System.Windows.Forms.Label();
            this.lblJp = new System.Windows.Forms.Label();
            this.lblZei = new System.Windows.Forms.Label();
            this.lblGridViewTitle = new System.Windows.Forms.Label();
            this.mtbList = new System.Windows.Forms.DataGridView();
            this.KamokuNm = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Zandaka = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KarikataHassei = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KashikataHassei = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TouZan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.startKmkCd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.endKmkCd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.listKmkCd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlDebug.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mtbList)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.BackColor = System.Drawing.Color.White;
            this.lblTitle.Size = new System.Drawing.Size(743, 23);
            this.lblTitle.Text = "";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(5, 489);
            this.pnlDebug.Size = new System.Drawing.Size(760, 100);
            // 
            // lblKaishaSettei
            // 
            this.lblKaishaSettei.BackColor = System.Drawing.Color.Transparent;
            this.lblKaishaSettei.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKaishaSettei.ForeColor = System.Drawing.Color.Black;
            this.lblKaishaSettei.Location = new System.Drawing.Point(14, 13);
            this.lblKaishaSettei.Name = "lblKaishaSettei";
            this.lblKaishaSettei.Size = new System.Drawing.Size(141, 20);
            this.lblKaishaSettei.TabIndex = 0;
            this.lblKaishaSettei.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblJp
            // 
            this.lblJp.BackColor = System.Drawing.Color.Transparent;
            this.lblJp.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJp.ForeColor = System.Drawing.Color.Black;
            this.lblJp.Location = new System.Drawing.Point(356, 14);
            this.lblJp.Name = "lblJp";
            this.lblJp.Size = new System.Drawing.Size(303, 20);
            this.lblJp.TabIndex = 4;
            this.lblJp.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblZei
            // 
            this.lblZei.BackColor = System.Drawing.Color.Transparent;
            this.lblZei.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblZei.ForeColor = System.Drawing.Color.Black;
            this.lblZei.Location = new System.Drawing.Point(661, 14);
            this.lblZei.Name = "lblZei";
            this.lblZei.Size = new System.Drawing.Size(78, 20);
            this.lblZei.TabIndex = 5;
            this.lblZei.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblGridViewTitle
            // 
            this.lblGridViewTitle.BackColor = System.Drawing.Color.Transparent;
            this.lblGridViewTitle.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGridViewTitle.ForeColor = System.Drawing.Color.Black;
            this.lblGridViewTitle.Location = new System.Drawing.Point(239, 13);
            this.lblGridViewTitle.Name = "lblGridViewTitle";
            this.lblGridViewTitle.Size = new System.Drawing.Size(138, 20);
            this.lblGridViewTitle.TabIndex = 3;
            this.lblGridViewTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // mtbList
            // 
            this.mtbList.AllowUserToAddRows = false;
            this.mtbList.AllowUserToDeleteRows = false;
            this.mtbList.AllowUserToResizeColumns = false;
            this.mtbList.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gray;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.mtbList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.mtbList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.mtbList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.KamokuNm,
            this.Zandaka,
            this.KarikataHassei,
            this.KashikataHassei,
            this.TouZan,
            this.startKmkCd,
            this.endKmkCd,
            this.listKmkCd});
            this.mtbList.Location = new System.Drawing.Point(13, 39);
            this.mtbList.MultiSelect = false;
            this.mtbList.Name = "mtbList";
            this.mtbList.ReadOnly = true;
            this.mtbList.RowHeadersVisible = false;
            this.mtbList.RowTemplate.Height = 21;
            this.mtbList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.mtbList.Size = new System.Drawing.Size(726, 493);
            this.mtbList.TabIndex = 902;
            this.mtbList.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.mtbList_RowEnter);
            // 
            // KamokuNm
            // 
            this.KamokuNm.HeaderText = "科　目　名";
            this.KamokuNm.Name = "KamokuNm";
            this.KamokuNm.ReadOnly = true;
            this.KamokuNm.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.KamokuNm.Width = 230;
            // 
            // Zandaka
            // 
            this.Zandaka.HeaderText = "前月残高";
            this.Zandaka.Name = "Zandaka";
            this.Zandaka.ReadOnly = true;
            this.Zandaka.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Zandaka.Width = 110;
            // 
            // KarikataHassei
            // 
            this.KarikataHassei.HeaderText = "借方発生";
            this.KarikataHassei.Name = "KarikataHassei";
            this.KarikataHassei.ReadOnly = true;
            this.KarikataHassei.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.KarikataHassei.Width = 110;
            // 
            // KashikataHassei
            // 
            this.KashikataHassei.HeaderText = "貸方発生";
            this.KashikataHassei.Name = "KashikataHassei";
            this.KashikataHassei.ReadOnly = true;
            this.KashikataHassei.Width = 110;
            // 
            // TouZan
            // 
            this.TouZan.HeaderText = "当　残";
            this.TouZan.Name = "TouZan";
            this.TouZan.ReadOnly = true;
            this.TouZan.Width = 110;
            // 
            // startKmkCd
            // 
            this.startKmkCd.HeaderText = "";
            this.startKmkCd.Name = "startKmkCd";
            this.startKmkCd.ReadOnly = true;
            this.startKmkCd.Visible = false;
            // 
            // endKmkCd
            // 
            this.endKmkCd.HeaderText = "";
            this.endKmkCd.Name = "endKmkCd";
            this.endKmkCd.ReadOnly = true;
            this.endKmkCd.Visible = false;
            // 
            // listKmkCd
            // 
            this.listKmkCd.HeaderText = "";
            this.listKmkCd.Name = "listKmkCd";
            this.listKmkCd.ReadOnly = true;
            this.listKmkCd.Visible = false;
            // 
            // ZMMR1032
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(752, 592);
            this.Controls.Add(this.lblGridViewTitle);
            this.Controls.Add(this.mtbList);
            this.Controls.Add(this.lblZei);
            this.Controls.Add(this.lblJp);
            this.Controls.Add(this.lblKaishaSettei);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "ZMMR1032";
            this.ShowFButton = true;
            this.Text = "";
            this.Shown += new System.EventHandler(this.ZMMR1032_Shown);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblKaishaSettei, 0);
            this.Controls.SetChildIndex(this.lblJp, 0);
            this.Controls.SetChildIndex(this.lblZei, 0);
            this.Controls.SetChildIndex(this.mtbList, 0);
            this.Controls.SetChildIndex(this.lblGridViewTitle, 0);
            this.pnlDebug.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mtbList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblKaishaSettei;
        private System.Windows.Forms.Label lblJp;
        private System.Windows.Forms.Label lblZei;
        private System.Windows.Forms.Label lblGridViewTitle;
        private System.Windows.Forms.DataGridView mtbList;
        private System.Windows.Forms.DataGridViewTextBoxColumn KamokuNm;
        private System.Windows.Forms.DataGridViewTextBoxColumn Zandaka;
        private System.Windows.Forms.DataGridViewTextBoxColumn KarikataHassei;
        private System.Windows.Forms.DataGridViewTextBoxColumn KashikataHassei;
        private System.Windows.Forms.DataGridViewTextBoxColumn TouZan;
        private System.Windows.Forms.DataGridViewTextBoxColumn startKmkCd;
        private System.Windows.Forms.DataGridViewTextBoxColumn endKmkCd;
        private System.Windows.Forms.DataGridViewTextBoxColumn listKmkCd;
    }
}