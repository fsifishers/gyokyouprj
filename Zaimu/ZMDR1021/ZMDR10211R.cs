﻿using System;
using System.Data;
using System.Collections.Generic;

using jp.co.fsi.common.report;
using jp.co.fsi.common.util;

namespace jp.co.fsi.zm.zmdr1021
{
    /// <summary>
    /// ZAMR1051R の概要の説明です。
    /// </summary>
    public partial class ZMDR10211R : BaseReport
    {
        public List<string> nmList;

        public ZMDR10211R(DataTable tgtData) : base(tgtData)
        {
            //
            // デザイナー サポートに必要なメソッドです。
            //
            InitializeComponent();
        }

        /// <summary>
        /// ページヘッダーの設定
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pageHeader_Format(object sender, EventArgs e)
        {
            ////西暦から和暦に変換
            //CultureInfo culture = new CultureInfo("ja-JP", true);
            //culture.DateTimeFormat.Calendar = new JapaneseCalendar();
            //txtToday.Text = DateTime.Now.ToString("ggyy年M月d日", culture);

            //this.lblIn01.Text = "組合長";
            //this.lblIn02.Text = "課\r\n\r\n長";
            //this.lblIn03.Text = "係\r\n\r\n長";
            //this.lblIn04.Text = "照\r\n\r\n査";
            //this.lblIn05.Text = "起\r\n\r\n票";
            this.lblIn01.Text = nmList[0];
            this.lblIn02.Text = nmList[1];
            this.lblIn03.Text = nmList[2];
            this.lblIn04.Text = nmList[3];
            this.lblIn05.Text = nmList[4];
        }

        string strValue01;
        string strValue02;
        private void Detail_BeforePrint(object sender, EventArgs e)
        {
            // 伝票日付の重複データの表示制御
            if (txtValue01.Text != strValue01)
            {
                txtValue01.Visible = true;
            }
            else
            {
                txtValue01.Visible = false;
            }
            // 伝票番号の重複データの表示制御
            if (txtValue02.Text != strValue02)
            {
                txtValue02.Visible = true;
            }
            else
            {
                // 重複データは非表示にします。
                txtValue02.Visible = false;
            }
            strValue01 = txtValue01.Text;
            strValue02 = txtValue02.Text;
        }

        private void pageHeader_AfterPrint(object sender, EventArgs e)
        {
            // 改ページ後、最初のデータは必ず出力します。
            strValue01 = "";
            strValue02 = "";
        }

        private void groupFooter_Format(object sender, EventArgs e)
        {
            this.txtValue08Total.Text = Util.FormatNum(this.txtValue08Total.Text);
            this.txtValue14Total.Text = Util.FormatNum(this.txtValue14Total.Text);
        }
    }
}
