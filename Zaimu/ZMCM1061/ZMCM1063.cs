﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Reflection;
using System.Windows.Forms;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.forms;
using jp.co.fsi.common.util;

namespace jp.co.fsi.zm.zmcm1061
{
    /// <summary>
    /// 税区分の印刷(ZMCM1063)
    /// </summary>
    public partial class ZMCM1063 : BasePgForm
    {
        #region 定数
        /// <summary>
        /// モード(新規)
        /// </summary>
        private const string MODE_NEW = "1";

        /// <summary>
        /// モード(編集)
        /// </summary>
        private const string MODE_EDIT = "2";
        #endregion

        #region プロパティ
        /// <summary>
        /// 画面上最後となるフォーカスのEnterボタン押下時処理用変数
        /// </summary>
        private bool _dtFlg = new bool();
        public bool Flg
        {
            get
            {
                return this._dtFlg;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ZMCM1063()
        {
            InitializeComponent();
            // GotFocusEventを紐づける
            BindGotFocusEvent();
        }
        #endregion

        #region protectedメソッド(継承)
        /// <summary>
        /// 起動時の初期処理
        /// </summary>
        protected override void InitForm()
        {
            // タイトルは非表示
            this.lblTitle.Visible = false;

            // ボタンの表示非表示を設定
            this.btnF1.Visible = true;
            this.btnF2.Visible = false;
            this.btnF3.Visible = false;
            this.btnF4.Visible = true;
            this.btnF5.Visible = true;
            this.btnF6.Visible = false;
            this.btnF7.Visible = false;
            this.btnF8.Visible = false;
            this.btnF9.Visible = false;
            this.btnF10.Visible = false;
            this.btnF11.Visible = false;
            this.btnF12.Visible = false;
            // ボタンの位置調整
            this.btnEsc.Location = this.btnF1.Location;
            this.btnF1.Location = this.btnF2.Location;
            this.btnF5.Location = this.btnF4.Location;
            this.btnF4.Location = this.btnF3.Location;

            // Enter処理を無効化
            this._dtFlg = false;
        }

        /// <summary>
        /// Escボタンクリック時処理
        /// </summary>
        public override void PressEsc()
        {
            // DialogResultとしてCancelを返却する
            this.DialogResult = DialogResult.Cancel;
            base.PressEsc();
        }

        /// <summary>
        /// F1キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF1();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF1()
        {
            ZMCM1064 frm1064 = new ZMCM1064();
            frm1064.ShowDialog(this);
            
            //MEMO:現状アクティブなコントロールごとに処理を実装してください。
            switch (this.ActiveCtlNm)
            {
                case "txtZeiKubunFr":
                    if (frm1064.DialogResult == DialogResult.OK)
                    {
                        string[] result = (string[])frm1064.OutData;

                        this.txtZeiKubunFr.Text = result[0];
                        this.lblZeiKubunFr.Text = result[1];
                    }
                    frm1064.Dispose();
                    break;
                case "txtZeiKubunTo":
                    if (frm1064.DialogResult == DialogResult.OK)
                    {
                        string[] result = (string[])frm1064.OutData;

                        this.txtZeiKubunTo.Text = result[0];
                        this.lblZeiKubunTo.Text = result[1];
                    }
                    frm1064.Dispose();
                    break;
            }
        }

        /// <summary>
        /// F4キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF4();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF4()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfYesNo("実行しますか？") == DialogResult.Yes)
            {
                // プレビュー処理
                DoPrint(true);
            }
        }

        /// <summary>
        /// F5キー押下時処理
        /// </summary>
        /// <remarks>
        /// 親クラスで定義されているメソッドですが、親クラスでは特に何も実装されてないので
        /// base.PressF5();は呼び出さなくて構いません。
        /// </remarks>
        public override void PressF5()
        {
            // 全項目を再度入力値チェック
            if (!ValidateAll())
            {
                // エラーありの場合ここで処理終了
                return;
            }

            if (Msg.ConfYesNo("実行しますか？") == DialogResult.Yes)
            {
                // 印刷処理
                DoPrint(false);
            }
        }
        #endregion

        #region イベント
        /// <summary>
        /// 税区分範囲(自)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtZeiKubunFr_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidZeiKubunFr())
            {
                e.Cancel = true;
                this.txtZeiKubunFr.SelectAll();
            }
        }

        /// <summary>
        /// 税区分範囲(至)の値チェック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtZeiKubunTo_Validating(object sender, CancelEventArgs e)
        {
            if (!IsValidZeiKubunTo())
            {
                e.Cancel = true;
                this.txtZeiKubunTo.SelectAll();

                // Enter処理を無効化
                this._dtFlg = false;
            }
            else
            {
                // Enter処理を有効化
                this._dtFlg = true;
            }
        }

        /// <summary>
        /// 税区分範囲(至)のEnter押下時処理
        /// (画面上最後のフォーム)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtZeiKubunTo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.Flg)
            {
                // Enter処理を無効化
                this._dtFlg = false;

                // 全項目を再度入力値チェック
                if (!ValidateAll())
                {
                    // エラーありの場合ここで処理終了
                    return;
                }

                if (Msg.ConfYesNo("実行しますか？") == DialogResult.Yes)
                {
                    // プレビュー処理
                    DoPrint(true);
                }
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 税区分範囲(自)の入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidZeiKubunFr()
        {
            // 未入力の場合、「先頭」を表示
            if (ValChk.IsEmpty(this.txtZeiKubunFr.Text))
            {
                this.lblZeiKubunFr.Text = "先　頭";
                return true;
            }
            // 数値のみの入力を許可
            else if (!ValChk.IsNumber(this.txtZeiKubunFr.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }
            else
            {
                // コードを元に名称を取得する
                // 取得された場合、名称をラベルに反映する
                DataTable dtResult = this.GetZeiKubunData(this.txtZeiKubunFr.Text);
                if (dtResult.Rows.Count > 0)
                {
                    this.lblZeiKubunFr.Text = dtResult.Rows[0]["ZEI_KUBUN_NM"].ToString();
                }
                else
                {
                    Msg.Error("入力に誤りがあります。");
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 税区分範囲(至)の入力チェック
        /// </summary>
        /// <returns>true:OK/false:NG</returns>
        private bool IsValidZeiKubunTo()
        {
            // 未入力の場合、「最後」を表示
            if (ValChk.IsEmpty(this.txtZeiKubunTo.Text))
            {
                this.lblZeiKubunTo.Text = "最　後";
                return true;
            }
            // 数値のみの入力を許可
            else if (!ValChk.IsNumber(this.txtZeiKubunTo.Text))
            {
                Msg.Notice("入力に誤りがあります。");
                return false;
            }
            else
            {
                // コードを元に名称を取得する
                // 取得された場合、名称をラベルに反映する
                DataTable dtResult = this.GetZeiKubunData(this.txtZeiKubunTo.Text);
                if (dtResult.Rows.Count > 0)
                {
                    this.lblZeiKubunTo.Text = dtResult.Rows[0]["ZEI_KUBUN_NM"].ToString();
                }
                else
                {
                    this.lblZeiKubunTo.Text = "";
                }
            }

            return true;
        }

        /// <summary>
        /// 全項目を入力チェック
        /// </summary>
        /// <returns>true:OK,false:NG</returns>
        private bool ValidateAll()
        {
            // 税区分範囲(自)のチェック
            if (!IsValidZeiKubunFr())
            {
                this.txtZeiKubunFr.Focus();
                this.txtZeiKubunFr.SelectAll();
                return false;
            }

            // 税区分範囲(至)のチェック
            if (!IsValidZeiKubunTo())
            {
                this.txtZeiKubunTo.Focus();
                this.txtZeiKubunTo.SelectAll();
                return false;
            }

            return true;
        }

        /// <summary>
        /// 税区分名を取得
        /// </summary>
        /// <returns>集計表設定の取得したデータ</returns>
        private DataTable GetZeiKubunData(String zeiKubunNo)
        {
            StringBuilder Sql = new StringBuilder();
            Sql.Append("SELECT");
            Sql.Append(" ZEI_KUBUN_NM ");
            Sql.Append("FROM");
            Sql.Append(" TB_ZM_F_ZEI_KUBUN ");
            Sql.Append("WHERE");
            Sql.Append(" ZEI_KUBUN = @ZEI_KUBUN");

            DbParamCollection dpc = new DbParamCollection();
            dpc.SetParam("@ZEI_KUBUN", SqlDbType.Decimal, 2, zeiKubunNo);

            DataTable dtResult = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);

            return dtResult;
        }

        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        private void DoPrint(bool isPreview)
        {
            try
            {
                this.Dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                bool dataFlag = MakeOutputData();

                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    StringBuilder cols = new StringBuilder();
                    cols.Append("  ITEM01");
                    cols.Append(" ,ITEM02");
                    cols.Append(" ,ITEM03");
                    cols.Append(" ,ITEM04");
                    cols.Append(" ,ITEM05");
                    cols.Append(" ,ITEM06");
                    cols.Append(" ,ITEM07");
                    cols.Append(" ,ITEM08");

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);

                    // データの取得
                    DataTable dtOutput = this.Dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_ZM_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    if (isPreview)
                    {
                        // 帳票オブジェクトをインスタンス化
                        ZMCM1061R rpt = new ZMCM1061R(dtOutput);
                        PreviewForm pFrm = new PreviewForm(rpt, this.UnqId);
                        // プレビュー画面表示
                        pFrm.Show();
                    }
                    else
                    {
                        // 帳票オブジェクトをインスタンス化
                        ZMCM1061R rpt = new ZMCM1061R(dtOutput);
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
            }
            finally
            {
                this.Dba.Rollback();
            }
        }

        /// <summary>
        /// 出力データを作成します。
        /// </summary>
        /// <returns>DBから取得した情報を加工した出力データ</returns>
        private bool MakeOutputData()
        {
            #region データを取得
            /* 税区分範囲を取得 */
            string ZeiKubunCdFr;
            string ZeiKubunCdTo;
            if (Util.ToString(this.txtZeiKubunFr.Text) != "")
            {
                ZeiKubunCdFr = this.txtZeiKubunFr.Text;
            }
            else
            {
                ZeiKubunCdFr = "0";
            }
            if (Util.ToString(this.txtZeiKubunTo.Text) != "")
            {
                ZeiKubunCdTo = this.txtZeiKubunTo.Text;
            }
            else
            {
                ZeiKubunCdTo = "99";
            }

            StringBuilder Sql = new StringBuilder();
            DbParamCollection dpc = new DbParamCollection();
            // 税区分ビューからデータを取得して表示
            Sql.Append("SELECT");
            Sql.Append(" A.ZEI_KUBUN,");
            Sql.Append(" 0 AS KUBUN,");
            Sql.Append(" A.ZEI_KUBUN_NM,");
            Sql.Append(" A.KAZEI_KUBUN,");
            Sql.Append(" A.KAZEI_KUBUN_NM,");
            Sql.Append(" A.TORIHIKI_KUBUN,");
            Sql.Append(" A.TORIHIKI_KUBUN_NM,");
            Sql.Append(" A.ZEI_RITSU,");
            Sql.Append(" A.TAISHAKU_KUBUN,");
            Sql.Append(" B.TAISHAKU_KUBUN_NM ");
            Sql.Append("FROM");
            Sql.Append(" VI_ZM_ZEI_KUBUN AS A ");
            Sql.Append("LEFT JOIN");
            Sql.Append(" TB_ZM_F_TAISHAKU_KUBUN AS B ");
            Sql.Append("ON");
            Sql.Append(" A.TAISHAKU_KUBUN = B.TAISHAKU_KUBUN ");
            Sql.Append("WHERE");
            Sql.Append(" A.ZEI_KUBUN BETWEEN @ZEI_KUBUN_FR AND @ZEI_KUBUN_TO ");
            Sql.Append("UNION ALL ");
            Sql.Append("SELECT");
            Sql.Append(" A.ZEI_KUBUN,");
            Sql.Append(" 1 AS KUBUN,");
            Sql.Append(" '' AS ZEI_KUBUN_NM,");
            Sql.Append(" 0  AS KAZEI_KUBUN,");
            Sql.Append(" '' AS KAZEI_KUBUN_NM,");
            Sql.Append(" 0  AS TORIHIKI_KUBUN,");
            Sql.Append(" '　適用開始日：' + CONVERT(varchar, A.TEKIYOU_KAISHI, 111) AS TORIHIKI_KUBUN_NM,");
            Sql.Append(" A.SHIN_ZEI_RITSU AS ZEI_RITSU,");
            Sql.Append(" 0  AS TAISHAKU_KUBUN,");
            Sql.Append(" '' AS TAISHAKU_KUBUN_NM ");
            Sql.Append("FROM");
            Sql.Append(" TB_ZM_F_ZEI_KBN_SHIN_ZEI_RT AS A ");
            Sql.Append("WHERE");
            Sql.Append(" A.ZEI_KUBUN BETWEEN @ZEI_KUBUN_FR AND @ZEI_KUBUN_TO ");
            Sql.Append("ORDER BY");
            Sql.Append(" 1, 2");
            dpc.SetParam("@ZEI_KUBUN_FR", SqlDbType.Decimal, 2, ZeiKubunCdFr);
            dpc.SetParam("@ZEI_KUBUN_TO", SqlDbType.Decimal, 2, ZeiKubunCdTo);
            DataTable dtResult = this.Dba.GetDataTableFromSqlWithParams(Util.ToString(Sql), dpc);
            #endregion

            #region データを設定
            int i = 0;
            foreach (DataRow dr in dtResult.Rows)
            {
                Sql = new StringBuilder();
                dpc = new DbParamCollection();

                Sql.Append("INSERT INTO PR_ZM_TBL(");
                Sql.Append(" GUID");
                Sql.Append(" ,SORT");
                Sql.Append(" ,ITEM01");
                Sql.Append(" ,ITEM02");
                Sql.Append(" ,ITEM03");
                Sql.Append(" ,ITEM04");
                Sql.Append(" ,ITEM05");
                Sql.Append(" ,ITEM06");
                Sql.Append(" ,ITEM07");
                Sql.Append(" ,ITEM08");
                Sql.Append(") ");
                Sql.Append("VALUES(");
                Sql.Append("  @GUID");
                Sql.Append(" ,@SORT");
                Sql.Append(" ,@ITEM01");
                Sql.Append(" ,@ITEM02");
                Sql.Append(" ,@ITEM03");
                Sql.Append(" ,@ITEM04");
                Sql.Append(" ,@ITEM05");
                Sql.Append(" ,@ITEM06");
                Sql.Append(" ,@ITEM07");
                Sql.Append(" ,@ITEM08");
                Sql.Append(") ");
                // データを設定
                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
                dpc.SetParam("@SORT", SqlDbType.VarChar, 4, i);
                dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this.UInfo.KaishaNm); // 会社名
                // 税区分NO
                if ((int)dr["KUBUN"] == 0)
                {
                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, dr["ZEI_KUBUN"].ToString());
                }
                else
                {
                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, "");
                }
                dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, dr["ZEI_KUBUN_NM"].ToString()); // 税区分
                // 課税区分NO
                if ((int)dr["KUBUN"] == 0)
                {
                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, dr["KAZEI_KUBUN"].ToString());
                }
                else
                {
                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, "");
                }
                dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, dr["KAZEI_KUBUN_NM"].ToString()); // 課税区分
                // 取引区分NO
                if ((int)dr["KUBUN"] == 0)
                {
                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, dr["TORIHIKI_KUBUN"].ToString());
                }
                else
                {
                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, "");
                }
                dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, dr["TORIHIKI_KUBUN_NM"].ToString()); // 取引区分
                dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, Util.FormatNum(dr["ZEI_RITSU"].ToString(), 1)); // 税率

                this.Dba.ModifyBySql(Util.ToString(Sql), dpc);
                i++;
            }
            #endregion

            // 印刷ワークテーブルのデータ件数を取得
            dpc = new DbParamCollection();
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this.UnqId);
            DataTable tmpdtPR_ZM_TBL = this.Dba.GetDataTableByConditionWithParams(
                "SORT",
                "PR_ZM_TBL",
                "GUID = @GUID",
                dpc);

            bool dataFlag;
            if (tmpdtPR_ZM_TBL.Rows.Count > 0)
            {
                dataFlag = true;
            }
            else
            {
                dataFlag = false;
            }

            return dataFlag;
        }
        #endregion
    }
}
