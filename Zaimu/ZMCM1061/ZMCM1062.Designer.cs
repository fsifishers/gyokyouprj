﻿namespace jp.co.fsi.zm.zmcm1061
{
    partial class ZMCM1062
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.txtZeiKubunCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblZeiKubunCd = new System.Windows.Forms.Label();
            this.pnlMain = new jp.co.fsi.common.FsiPanel();
            this.txtGridEdit = new jp.co.fsi.common.controls.FsiTextBox();
            this.dgvInputList = new System.Windows.Forms.DataGridView();
            this.gyo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tekiyouKaishiBi = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.zeiRitsu = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtZeiRitsu = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblPercent = new System.Windows.Forms.Label();
            this.lblZeiRitsu = new System.Windows.Forms.Label();
            this.lblTaishakuKubunSelect = new System.Windows.Forms.Label();
            this.txtTaishakuKubunCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblTaishakuKubunCd = new System.Windows.Forms.Label();
            this.lblTorihikiKubunNm = new System.Windows.Forms.Label();
            this.txtTorihikiKubunCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblTorihikiKubunCd = new System.Windows.Forms.Label();
            this.lblKazeiKubunCdSelect = new System.Windows.Forms.Label();
            this.txtKazeiKubunCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKazeiKubunCd = new System.Windows.Forms.Label();
            this.txtZeiKubunNm = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblZeiKubunNm = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.pnlMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInputList)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(567, 23);
            this.lblTitle.Text = "税区分の登録";
            // 
            // btnF2
            // 
            this.btnF2.Text = "F2";
            // 
            // btnF3
            // 
            this.btnF3.Text = "F3\r\n\r\n削除";
            // 
            // btnF4
            // 
            this.btnF4.Text = "F4";
            // 
            // btnF5
            // 
            this.btnF5.Text = "F5";
            // 
            // btnF6
            // 
            this.btnF6.Text = "F6\r\n\r\n保存";
            // 
            // btnF8
            // 
            this.btnF8.Text = "F8\r\n\r\n行削除";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(5, 137);
            this.pnlDebug.Size = new System.Drawing.Size(584, 100);
            // 
            // txtZeiKubunCd
            // 
            this.txtZeiKubunCd.AutoSizeFromLength = true;
            this.txtZeiKubunCd.DisplayLength = null;
            this.txtZeiKubunCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtZeiKubunCd.ImeMode = System.Windows.Forms.ImeMode.Alpha;
            this.txtZeiKubunCd.Location = new System.Drawing.Point(132, 13);
            this.txtZeiKubunCd.MaxLength = 2;
            this.txtZeiKubunCd.Name = "txtZeiKubunCd";
            this.txtZeiKubunCd.Size = new System.Drawing.Size(20, 20);
            this.txtZeiKubunCd.TabIndex = 2;
            this.txtZeiKubunCd.Text = "0";
            this.txtZeiKubunCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtZeiKubunCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtZeiKubunCd_Validating);
            // 
            // lblZeiKubunCd
            // 
            this.lblZeiKubunCd.BackColor = System.Drawing.Color.Silver;
            this.lblZeiKubunCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblZeiKubunCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblZeiKubunCd.Location = new System.Drawing.Point(17, 11);
            this.lblZeiKubunCd.Name = "lblZeiKubunCd";
            this.lblZeiKubunCd.Size = new System.Drawing.Size(138, 25);
            this.lblZeiKubunCd.TabIndex = 1;
            this.lblZeiKubunCd.Text = "税区分";
            this.lblZeiKubunCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlMain
            // 
            this.pnlMain.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlMain.Controls.Add(this.txtGridEdit);
            this.pnlMain.Controls.Add(this.dgvInputList);
            this.pnlMain.Controls.Add(this.txtZeiRitsu);
            this.pnlMain.Controls.Add(this.lblPercent);
            this.pnlMain.Controls.Add(this.lblZeiRitsu);
            this.pnlMain.Controls.Add(this.lblTaishakuKubunSelect);
            this.pnlMain.Controls.Add(this.txtTaishakuKubunCd);
            this.pnlMain.Controls.Add(this.lblTaishakuKubunCd);
            this.pnlMain.Controls.Add(this.lblTorihikiKubunNm);
            this.pnlMain.Controls.Add(this.txtTorihikiKubunCd);
            this.pnlMain.Controls.Add(this.lblTorihikiKubunCd);
            this.pnlMain.Controls.Add(this.lblKazeiKubunCdSelect);
            this.pnlMain.Controls.Add(this.txtKazeiKubunCd);
            this.pnlMain.Controls.Add(this.lblKazeiKubunCd);
            this.pnlMain.Controls.Add(this.txtZeiKubunNm);
            this.pnlMain.Controls.Add(this.lblZeiKubunNm);
            this.pnlMain.Location = new System.Drawing.Point(12, 39);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(571, 133);
            this.pnlMain.TabIndex = 3;
            // 
            // txtGridEdit
            // 
            this.txtGridEdit.AutoSizeFromLength = true;
            this.txtGridEdit.BackColor = System.Drawing.Color.White;
            this.txtGridEdit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtGridEdit.DisplayLength = null;
            this.txtGridEdit.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtGridEdit.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtGridEdit.Location = new System.Drawing.Point(467, 74);
            this.txtGridEdit.MaxLength = 10;
            this.txtGridEdit.Name = "txtGridEdit";
            this.txtGridEdit.Size = new System.Drawing.Size(76, 20);
            this.txtGridEdit.TabIndex = 902;
            this.txtGridEdit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGridEdit.Visible = false;
            this.txtGridEdit.Validating += new System.ComponentModel.CancelEventHandler(this.txtGridEdit_Validating);
            // 
            // dgvInputList
            // 
            this.dgvInputList.AllowUserToAddRows = false;
            this.dgvInputList.AllowUserToDeleteRows = false;
            this.dgvInputList.AllowUserToResizeColumns = false;
            this.dgvInputList.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvInputList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvInputList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvInputList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.gyo,
            this.tekiyouKaishiBi,
            this.zeiRitsu});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvInputList.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgvInputList.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgvInputList.ImeMode = System.Windows.Forms.ImeMode.Alpha;
            this.dgvInputList.Location = new System.Drawing.Point(359, 28);
            this.dgvInputList.MultiSelect = false;
            this.dgvInputList.Name = "dgvInputList";
            this.dgvInputList.RowHeadersVisible = false;
            this.dgvInputList.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvInputList.RowTemplate.Height = 21;
            this.dgvInputList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvInputList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvInputList.Size = new System.Drawing.Size(204, 89);
            this.dgvInputList.TabIndex = 14;
            this.dgvInputList.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvInputList_CellEnter);
            this.dgvInputList.Scroll += new System.Windows.Forms.ScrollEventHandler(this.dgvInputList_Scroll);
            this.dgvInputList.Enter += new System.EventHandler(this.dgvInputList_Enter);
            this.dgvInputList.MouseUp += new System.Windows.Forms.MouseEventHandler(this.dgvInputList_MouseUp);
            // 
            // gyo
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.gyo.DefaultCellStyle = dataGridViewCellStyle2;
            this.gyo.HeaderText = "行";
            this.gyo.Name = "gyo";
            this.gyo.ReadOnly = true;
            this.gyo.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.gyo.Width = 30;
            // 
            // tekiyouKaishiBi
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.tekiyouKaishiBi.DefaultCellStyle = dataGridViewCellStyle3;
            this.tekiyouKaishiBi.HeaderText = "適用開始日";
            this.tekiyouKaishiBi.Name = "tekiyouKaishiBi";
            this.tekiyouKaishiBi.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // zeiRitsu
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.zeiRitsu.DefaultCellStyle = dataGridViewCellStyle4;
            this.zeiRitsu.HeaderText = "税率";
            this.zeiRitsu.Name = "zeiRitsu";
            this.zeiRitsu.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.zeiRitsu.Width = 50;
            // 
            // txtZeiRitsu
            // 
            this.txtZeiRitsu.AutoSizeFromLength = false;
            this.txtZeiRitsu.BackColor = System.Drawing.SystemColors.Window;
            this.txtZeiRitsu.DisplayLength = null;
            this.txtZeiRitsu.Enabled = false;
            this.txtZeiRitsu.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtZeiRitsu.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtZeiRitsu.Location = new System.Drawing.Point(118, 105);
            this.txtZeiRitsu.MaxLength = 4;
            this.txtZeiRitsu.Name = "txtZeiRitsu";
            this.txtZeiRitsu.ReadOnly = true;
            this.txtZeiRitsu.Size = new System.Drawing.Size(44, 20);
            this.txtZeiRitsu.TabIndex = 12;
            this.txtZeiRitsu.Text = "0.0";
            this.txtZeiRitsu.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtZeiRitsu.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtZeiRitsu_KeyDown);
            this.txtZeiRitsu.Validating += new System.ComponentModel.CancelEventHandler(this.txtZeiRitsu_Validating);
            // 
            // lblPercent
            // 
            this.lblPercent.BackColor = System.Drawing.Color.Silver;
            this.lblPercent.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblPercent.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblPercent.Location = new System.Drawing.Point(164, 103);
            this.lblPercent.Name = "lblPercent";
            this.lblPercent.Size = new System.Drawing.Size(25, 25);
            this.lblPercent.TabIndex = 13;
            this.lblPercent.Text = "％";
            this.lblPercent.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblZeiRitsu
            // 
            this.lblZeiRitsu.BackColor = System.Drawing.Color.Silver;
            this.lblZeiRitsu.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblZeiRitsu.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblZeiRitsu.Location = new System.Drawing.Point(3, 103);
            this.lblZeiRitsu.Name = "lblZeiRitsu";
            this.lblZeiRitsu.Size = new System.Drawing.Size(162, 25);
            this.lblZeiRitsu.TabIndex = 11;
            this.lblZeiRitsu.Text = "税率";
            this.lblZeiRitsu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTaishakuKubunSelect
            // 
            this.lblTaishakuKubunSelect.BackColor = System.Drawing.Color.Silver;
            this.lblTaishakuKubunSelect.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTaishakuKubunSelect.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTaishakuKubunSelect.Location = new System.Drawing.Point(140, 78);
            this.lblTaishakuKubunSelect.Name = "lblTaishakuKubunSelect";
            this.lblTaishakuKubunSelect.Size = new System.Drawing.Size(213, 25);
            this.lblTaishakuKubunSelect.TabIndex = 10;
            this.lblTaishakuKubunSelect.Text = "1：借方　2：貸方";
            this.lblTaishakuKubunSelect.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTaishakuKubunCd
            // 
            this.txtTaishakuKubunCd.AutoSizeFromLength = false;
            this.txtTaishakuKubunCd.DisplayLength = null;
            this.txtTaishakuKubunCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTaishakuKubunCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtTaishakuKubunCd.Location = new System.Drawing.Point(118, 80);
            this.txtTaishakuKubunCd.MaxLength = 1;
            this.txtTaishakuKubunCd.Name = "txtTaishakuKubunCd";
            this.txtTaishakuKubunCd.Size = new System.Drawing.Size(20, 20);
            this.txtTaishakuKubunCd.TabIndex = 9;
            this.txtTaishakuKubunCd.Text = "0";
            this.txtTaishakuKubunCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTaishakuKubunCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtTaishakuKubunCd_Validating);
            // 
            // lblTaishakuKubunCd
            // 
            this.lblTaishakuKubunCd.BackColor = System.Drawing.Color.Silver;
            this.lblTaishakuKubunCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTaishakuKubunCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTaishakuKubunCd.Location = new System.Drawing.Point(3, 78);
            this.lblTaishakuKubunCd.Name = "lblTaishakuKubunCd";
            this.lblTaishakuKubunCd.Size = new System.Drawing.Size(138, 25);
            this.lblTaishakuKubunCd.TabIndex = 8;
            this.lblTaishakuKubunCd.Text = "貸借区分";
            this.lblTaishakuKubunCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTorihikiKubunNm
            // 
            this.lblTorihikiKubunNm.BackColor = System.Drawing.Color.Silver;
            this.lblTorihikiKubunNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTorihikiKubunNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTorihikiKubunNm.Location = new System.Drawing.Point(140, 53);
            this.lblTorihikiKubunNm.Name = "lblTorihikiKubunNm";
            this.lblTorihikiKubunNm.Size = new System.Drawing.Size(213, 25);
            this.lblTorihikiKubunNm.TabIndex = 7;
            this.lblTorihikiKubunNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTorihikiKubunCd
            // 
            this.txtTorihikiKubunCd.AutoSizeFromLength = false;
            this.txtTorihikiKubunCd.DisplayLength = null;
            this.txtTorihikiKubunCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTorihikiKubunCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtTorihikiKubunCd.Location = new System.Drawing.Point(118, 55);
            this.txtTorihikiKubunCd.MaxLength = 2;
            this.txtTorihikiKubunCd.Name = "txtTorihikiKubunCd";
            this.txtTorihikiKubunCd.Size = new System.Drawing.Size(20, 20);
            this.txtTorihikiKubunCd.TabIndex = 6;
            this.txtTorihikiKubunCd.Text = "0";
            this.txtTorihikiKubunCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTorihikiKubunCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtTorihikiKubunCd_Validating);
            // 
            // lblTorihikiKubunCd
            // 
            this.lblTorihikiKubunCd.BackColor = System.Drawing.Color.Silver;
            this.lblTorihikiKubunCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTorihikiKubunCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblTorihikiKubunCd.Location = new System.Drawing.Point(3, 53);
            this.lblTorihikiKubunCd.Name = "lblTorihikiKubunCd";
            this.lblTorihikiKubunCd.Size = new System.Drawing.Size(138, 25);
            this.lblTorihikiKubunCd.TabIndex = 5;
            this.lblTorihikiKubunCd.Text = "取引区分";
            this.lblTorihikiKubunCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKazeiKubunCdSelect
            // 
            this.lblKazeiKubunCdSelect.BackColor = System.Drawing.Color.Silver;
            this.lblKazeiKubunCdSelect.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKazeiKubunCdSelect.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKazeiKubunCdSelect.Location = new System.Drawing.Point(140, 28);
            this.lblKazeiKubunCdSelect.Name = "lblKazeiKubunCdSelect";
            this.lblKazeiKubunCdSelect.Size = new System.Drawing.Size(213, 25);
            this.lblKazeiKubunCdSelect.TabIndex = 4;
            this.lblKazeiKubunCdSelect.Text = "0：非課税 1：課税 ２：不課税";
            this.lblKazeiKubunCdSelect.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKazeiKubunCd
            // 
            this.txtKazeiKubunCd.AutoSizeFromLength = false;
            this.txtKazeiKubunCd.DisplayLength = null;
            this.txtKazeiKubunCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKazeiKubunCd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtKazeiKubunCd.Location = new System.Drawing.Point(118, 30);
            this.txtKazeiKubunCd.MaxLength = 1;
            this.txtKazeiKubunCd.Name = "txtKazeiKubunCd";
            this.txtKazeiKubunCd.Size = new System.Drawing.Size(20, 20);
            this.txtKazeiKubunCd.TabIndex = 3;
            this.txtKazeiKubunCd.Text = "0";
            this.txtKazeiKubunCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKazeiKubunCd.Validating += new System.ComponentModel.CancelEventHandler(this.txtKazeiKubunCd_Validating);
            // 
            // lblKazeiKubunCd
            // 
            this.lblKazeiKubunCd.BackColor = System.Drawing.Color.Silver;
            this.lblKazeiKubunCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKazeiKubunCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKazeiKubunCd.Location = new System.Drawing.Point(3, 28);
            this.lblKazeiKubunCd.Name = "lblKazeiKubunCd";
            this.lblKazeiKubunCd.Size = new System.Drawing.Size(138, 25);
            this.lblKazeiKubunCd.TabIndex = 2;
            this.lblKazeiKubunCd.Text = "課税区分";
            this.lblKazeiKubunCd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtZeiKubunNm
            // 
            this.txtZeiKubunNm.AutoSizeFromLength = false;
            this.txtZeiKubunNm.DisplayLength = null;
            this.txtZeiKubunNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtZeiKubunNm.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtZeiKubunNm.Location = new System.Drawing.Point(118, 5);
            this.txtZeiKubunNm.MaxLength = 30;
            this.txtZeiKubunNm.Name = "txtZeiKubunNm";
            this.txtZeiKubunNm.Size = new System.Drawing.Size(446, 20);
            this.txtZeiKubunNm.TabIndex = 1;
            // 
            // lblZeiKubunNm
            // 
            this.lblZeiKubunNm.BackColor = System.Drawing.Color.Silver;
            this.lblZeiKubunNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblZeiKubunNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblZeiKubunNm.Location = new System.Drawing.Point(3, 3);
            this.lblZeiKubunNm.Name = "lblZeiKubunNm";
            this.lblZeiKubunNm.Size = new System.Drawing.Size(563, 25);
            this.lblZeiKubunNm.TabIndex = 0;
            this.lblZeiKubunNm.Text = "税区分名称";
            this.lblZeiKubunNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ZMCM1062
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(592, 240);
            this.Controls.Add(this.pnlMain);
            this.Controls.Add(this.txtZeiKubunCd);
            this.Controls.Add(this.lblZeiKubunCd);
            this.Name = "ZMCM1062";
            this.ShowFButton = true;
            this.Text = "税区分の登録";
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblZeiKubunCd, 0);
            this.Controls.SetChildIndex(this.txtZeiKubunCd, 0);
            this.Controls.SetChildIndex(this.pnlMain, 0);
            this.pnlDebug.ResumeLayout(false);
            this.pnlMain.ResumeLayout(false);
            this.pnlMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInputList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private jp.co.fsi.common.controls.FsiTextBox txtZeiKubunCd;
        private System.Windows.Forms.Label lblZeiKubunCd;
        private jp.co.fsi.common.FsiPanel pnlMain;
        private jp.co.fsi.common.controls.FsiTextBox txtZeiKubunNm;
        private System.Windows.Forms.Label lblZeiKubunNm;
        private System.Windows.Forms.Label lblKazeiKubunCd;
        private jp.co.fsi.common.controls.FsiTextBox txtKazeiKubunCd;
        private System.Windows.Forms.Label lblKazeiKubunCdSelect;
        private System.Windows.Forms.Label lblTorihikiKubunCd;
        private System.Windows.Forms.Label lblTorihikiKubunNm;
        private jp.co.fsi.common.controls.FsiTextBox txtTorihikiKubunCd;
        private System.Windows.Forms.Label lblTaishakuKubunSelect;
        private jp.co.fsi.common.controls.FsiTextBox txtTaishakuKubunCd;
        private System.Windows.Forms.Label lblTaishakuKubunCd;
        private System.Windows.Forms.Label lblZeiRitsu;
        private jp.co.fsi.common.controls.FsiTextBox txtZeiRitsu;
        private System.Windows.Forms.Label lblPercent;
        private System.Windows.Forms.DataGridView dgvInputList;
        private jp.co.fsi.common.controls.FsiTextBox txtGridEdit;
        private System.Windows.Forms.DataGridViewTextBoxColumn gyo;
        private System.Windows.Forms.DataGridViewTextBoxColumn tekiyouKaishiBi;
        private System.Windows.Forms.DataGridViewTextBoxColumn zeiRitsu;
    }
}