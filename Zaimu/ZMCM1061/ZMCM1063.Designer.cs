﻿namespace jp.co.fsi.zm.zmcm1061
{
    partial class ZMCM1063
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtZeiKubunFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblZeiKubunFr = new System.Windows.Forms.Label();
            this.gbxZeiKubun = new System.Windows.Forms.GroupBox();
            this.lblCodeBet = new System.Windows.Forms.Label();
            this.lblZeiKubunTo = new System.Windows.Forms.Label();
            this.txtZeiKubunTo = new jp.co.fsi.common.controls.FsiTextBox();
            this.pnlDebug.SuspendLayout();
            this.gbxZeiKubun.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(654, 23);
            this.lblTitle.Text = "税区分の印刷";
            // 
            // btnEsc
            // 
            this.btnEsc.Text = "Esc\r\n\r\n戻る";
            // 
            // btnF2
            // 
            this.btnF2.Text = "F2";
            // 
            // btnF3
            // 
            this.btnF3.Text = "F3";
            // 
            // btnF4
            // 
            this.btnF4.Text = "F4\r\n\r\nプレビュー";
            // 
            // btnF6
            // 
            this.btnF6.Text = "F6";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Location = new System.Drawing.Point(5, 56);
            this.pnlDebug.Size = new System.Drawing.Size(671, 97);
            // 
            // txtZeiKubunFr
            // 
            this.txtZeiKubunFr.AutoSizeFromLength = false;
            this.txtZeiKubunFr.DisplayLength = null;
            this.txtZeiKubunFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtZeiKubunFr.ImeMode = System.Windows.Forms.ImeMode.Alpha;
            this.txtZeiKubunFr.Location = new System.Drawing.Point(15, 32);
            this.txtZeiKubunFr.MaxLength = 2;
            this.txtZeiKubunFr.Name = "txtZeiKubunFr";
            this.txtZeiKubunFr.Size = new System.Drawing.Size(28, 20);
            this.txtZeiKubunFr.TabIndex = 2;
            this.txtZeiKubunFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtZeiKubunFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtZeiKubunFr_Validating);
            // 
            // lblZeiKubunFr
            // 
            this.lblZeiKubunFr.BackColor = System.Drawing.Color.Silver;
            this.lblZeiKubunFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblZeiKubunFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblZeiKubunFr.Location = new System.Drawing.Point(49, 32);
            this.lblZeiKubunFr.Name = "lblZeiKubunFr";
            this.lblZeiKubunFr.Size = new System.Drawing.Size(275, 20);
            this.lblZeiKubunFr.TabIndex = 2;
            this.lblZeiKubunFr.Text = "先　頭";
            this.lblZeiKubunFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gbxZeiKubun
            // 
            this.gbxZeiKubun.Controls.Add(this.lblCodeBet);
            this.gbxZeiKubun.Controls.Add(this.lblZeiKubunTo);
            this.gbxZeiKubun.Controls.Add(this.txtZeiKubunTo);
            this.gbxZeiKubun.Controls.Add(this.lblZeiKubunFr);
            this.gbxZeiKubun.Controls.Add(this.txtZeiKubunFr);
            this.gbxZeiKubun.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxZeiKubun.Location = new System.Drawing.Point(8, 12);
            this.gbxZeiKubun.Name = "gbxZeiKubun";
            this.gbxZeiKubun.Size = new System.Drawing.Size(662, 86);
            this.gbxZeiKubun.TabIndex = 902;
            this.gbxZeiKubun.TabStop = false;
            this.gbxZeiKubun.Text = "税区分範囲";
            // 
            // lblCodeBet
            // 
            this.lblCodeBet.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCodeBet.Location = new System.Drawing.Point(324, 32);
            this.lblCodeBet.Name = "lblCodeBet";
            this.lblCodeBet.Size = new System.Drawing.Size(18, 20);
            this.lblCodeBet.TabIndex = 16;
            this.lblCodeBet.Text = "～";
            this.lblCodeBet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblZeiKubunTo
            // 
            this.lblZeiKubunTo.BackColor = System.Drawing.Color.Silver;
            this.lblZeiKubunTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblZeiKubunTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblZeiKubunTo.Location = new System.Drawing.Point(378, 32);
            this.lblZeiKubunTo.Name = "lblZeiKubunTo";
            this.lblZeiKubunTo.Size = new System.Drawing.Size(275, 20);
            this.lblZeiKubunTo.TabIndex = 3;
            this.lblZeiKubunTo.Text = "最　後";
            this.lblZeiKubunTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtZeiKubunTo
            // 
            this.txtZeiKubunTo.AutoSizeFromLength = false;
            this.txtZeiKubunTo.DisplayLength = null;
            this.txtZeiKubunTo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtZeiKubunTo.ImeMode = System.Windows.Forms.ImeMode.Alpha;
            this.txtZeiKubunTo.Location = new System.Drawing.Point(344, 32);
            this.txtZeiKubunTo.MaxLength = 2;
            this.txtZeiKubunTo.Name = "txtZeiKubunTo";
            this.txtZeiKubunTo.Size = new System.Drawing.Size(28, 20);
            this.txtZeiKubunTo.TabIndex = 4;
            this.txtZeiKubunTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtZeiKubunTo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtZeiKubunTo_KeyDown);
            this.txtZeiKubunTo.Validating += new System.ComponentModel.CancelEventHandler(this.txtZeiKubunTo_Validating);
            // 
            // ZMCM1063
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(679, 154);
            this.Controls.Add(this.gbxZeiKubun);
            this.Name = "ZMCM1063";
            this.ShowFButton = true;
            this.Text = "税区分の印刷";
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.gbxZeiKubun, 0);
            this.pnlDebug.ResumeLayout(false);
            this.gbxZeiKubun.ResumeLayout(false);
            this.gbxZeiKubun.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private jp.co.fsi.common.controls.FsiTextBox txtZeiKubunFr;
        private System.Windows.Forms.Label lblZeiKubunFr;
        private System.Windows.Forms.GroupBox gbxZeiKubun;
        private System.Windows.Forms.Label lblCodeBet;
        private System.Windows.Forms.Label lblZeiKubunTo;
        private jp.co.fsi.common.controls.FsiTextBox txtZeiKubunTo;

    }
}