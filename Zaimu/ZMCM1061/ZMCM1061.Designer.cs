﻿namespace jp.co.fsi.zm.zmcm1061
{
    partial class ZMCM1061
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvList = new System.Windows.Forms.DataGridView();
            this.txtZeiKubunNm = new System.Windows.Forms.TextBox();
            this.lblZeiKubunNm = new System.Windows.Forms.Label();
            this.btnEnter = new System.Windows.Forms.Button();
            this.pnlDebug.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Text = "税区分の登録";
            // 
            // btnF2
            // 
            this.btnF2.Text = "F2";
            // 
            // btnF3
            // 
            this.btnF3.Text = "F3";
            // 
            // btnF4
            // 
            this.btnF4.Text = "F4\r\n\r\n追加";
            // 
            // btnF6
            // 
            this.btnF6.Text = "F6";
            // 
            // btnF12
            // 
            this.btnF12.Text = "F12";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Controls.Add(this.btnEnter);
            this.pnlDebug.Size = new System.Drawing.Size(847, 100);
            this.pnlDebug.Controls.SetChildIndex(this.btnF6, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF7, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF5, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF8, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF4, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF9, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF3, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF10, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF2, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF11, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF1, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnF12, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnEsc, 0);
            this.pnlDebug.Controls.SetChildIndex(this.btnEnter, 0);
            // 
            // dgvList
            // 
            this.dgvList.AllowUserToAddRows = false;
            this.dgvList.AllowUserToDeleteRows = false;
            this.dgvList.AllowUserToResizeColumns = false;
            this.dgvList.AllowUserToResizeRows = false;
            this.dgvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvList.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.dgvList.Location = new System.Drawing.Point(17, 77);
            this.dgvList.MultiSelect = false;
            this.dgvList.Name = "dgvList";
            this.dgvList.ReadOnly = true;
            this.dgvList.RowHeadersVisible = false;
            this.dgvList.RowTemplate.Height = 21;
            this.dgvList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvList.Size = new System.Drawing.Size(585, 359);
            this.dgvList.TabIndex = 3;
            this.dgvList.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvList_CellDoubleClick);
            this.dgvList.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvList_KeyDown);
            // 
            // txtZeiKubunNm
            // 
            this.txtZeiKubunNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtZeiKubunNm.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.txtZeiKubunNm.Location = new System.Drawing.Point(98, 49);
            this.txtZeiKubunNm.MaxLength = 30;
            this.txtZeiKubunNm.Name = "txtZeiKubunNm";
            this.txtZeiKubunNm.Size = new System.Drawing.Size(231, 20);
            this.txtZeiKubunNm.TabIndex = 1;
            this.txtZeiKubunNm.Click += new System.EventHandler(this.txtZeiKubunNm_Click);
            this.txtZeiKubunNm.Validating += new System.ComponentModel.CancelEventHandler(this.txtZeiKubunNm_Validating);
            // 
            // lblZeiKubunNm
            // 
            this.lblZeiKubunNm.BackColor = System.Drawing.Color.Silver;
            this.lblZeiKubunNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblZeiKubunNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblZeiKubunNm.Location = new System.Drawing.Point(17, 47);
            this.lblZeiKubunNm.Name = "lblZeiKubunNm";
            this.lblZeiKubunNm.Size = new System.Drawing.Size(315, 25);
            this.lblZeiKubunNm.TabIndex = 902;
            this.lblZeiKubunNm.Text = "税区分名称";
            this.lblZeiKubunNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnEnter
            // 
            this.btnEnter.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnEnter.Location = new System.Drawing.Point(391, 28);
            this.btnEnter.Name = "btnEnter";
            this.btnEnter.Size = new System.Drawing.Size(65, 45);
            this.btnEnter.TabIndex = 904;
            this.btnEnter.TabStop = false;
            this.btnEnter.Text = "Enter\r\n\r\n決定";
            this.btnEnter.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnEnter.UseVisualStyleBackColor = true;
            this.btnEnter.Visible = false;
            // 
            // ZMCM1061
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 638);
            this.Controls.Add(this.txtZeiKubunNm);
            this.Controls.Add(this.lblZeiKubunNm);
            this.Controls.Add(this.dgvList);
            this.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf;
            this.Name = "ZMCM1061";
            this.Text = "税区分の登録";
            this.Shown += new System.EventHandler(this.frm_Shown);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.Controls.SetChildIndex(this.dgvList, 0);
            this.Controls.SetChildIndex(this.lblZeiKubunNm, 0);
            this.Controls.SetChildIndex(this.txtZeiKubunNm, 0);
            this.pnlDebug.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvList;
        private System.Windows.Forms.TextBox txtZeiKubunNm;
        private System.Windows.Forms.Label lblZeiKubunNm;
        protected System.Windows.Forms.Button btnEnter;
    }
}