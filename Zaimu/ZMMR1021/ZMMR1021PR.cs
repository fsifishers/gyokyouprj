﻿using System.Data;
using System.Text;
using System.Windows.Forms;

using GrapeCity.ActiveReports;

using jp.co.fsi.common.constants;
using jp.co.fsi.common.dataaccess;
using jp.co.fsi.common.userinfo;
using jp.co.fsi.common.util;
using jp.co.fsi.common.forms;

namespace jp.co.fsi.zm.zmmr1021
{
    /// <summary>
    /// モジュール全体で使用するデータアクセスクラスです。
    /// </summary>
    public class ZMMR1021PR
    {
        #region 構造体
        /// <summary>
        /// 合計情報
        /// </summary>
        private struct Summary
        {
            public decimal kariAmount;
            public decimal kariZei;
            public decimal kashiAmount;
            public decimal kashiZei;
            public decimal zan;

            /// <summary>
            /// 金額をクリア
            /// </summary>
            public void Clear()
            {
                kariAmount = 0;
                kariZei = 0;
                kashiAmount = 0;
                kashiZei = 0;
                zan = 0;
            }
        }
        #endregion

        #region private変数
        /// <summary>
        /// ZAMR2021(条件画面)のオブジェクト(設定内容の取得のため)
        /// </summary>
        ZMMR1021 _pForm;

        /// <summary>
        /// ユーザー情報
        /// </summary>
        UserInfo _uInfo;

        /// <summary>
        /// データアクセスオブジェクト
        /// </summary>
        DbAccess _dba;

        /// <summary>
        /// 設定ファイルアクセスオブジェクト
        /// </summary>
        ConfigLoader _config;

        /// <summary>
        /// ユニークID
        /// </summary>
        string _unqId;

        /// <summary>
        /// 出力日付
        /// </summary>
        string _outputDate;
        #endregion

        #region プロパティ
        private DataTable _dsTaishakuData = new DataTable();
        /// <summary>
        /// 貸借データ
        /// </summary>
        /// <remarks>
        /// 貸借を仕訳したデータ(1伝票あたり1DataTable)
        /// </remarks>
        public DataTable TaishakuData
        {
            get
            {
                return this._dsTaishakuData;
            }
        }
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="uInfo">操作中のユーザーの情報</param>
        /// <param name="dba">呼び出し元で保持するデータアクセスオブジェクト</param>
        /// <param name="config">呼び出し元で保持する設定ファイルアクセスオブジェクト</param>
        public ZMMR1021PR(UserInfo uInfo, DbAccess dba, ConfigLoader config, string unuqId, ZMMR1021 frm)
        {
            this._uInfo = uInfo;
            this._dba = dba;
            this._config = config;
            this._unqId = unuqId;
            this._pForm = frm;
        }
        #endregion

        #region publicメソッド
        /// <summary>
        /// 帳票を印刷する
        /// </summary>
        /// <param name="isPreview">プレビュー処理かどうか(true:プレビュー、false:印刷)</param>
        //public void DoPrint(bool isPreview)
        public void DoPrint(bool isPreview, bool isPdf = false, bool isExcel = false, bool isCsv = false)
        {
            ZMMR1024 msgFrm = null;
            try
            {
                // 集計中メッセージ表示
                msgFrm = new ZMMR1024();
                msgFrm.Show();
                msgFrm.Refresh();

                bool dataFlag;

                this._dba.BeginTransaction();

                //// 帳票出力用にワークテーブルにデータを作成
                dataFlag = MakeWkData();
                //DataTable dtOutput = MakeWkData();
                //dataFlag = InsertWkData(dtOutput);

                // 帳票出力
                if (dataFlag)
                {
                    // 取得列の定義
                    StringBuilder cols = new StringBuilder();
                    cols.Append("  ITEM01");
                    cols.Append(" ,ITEM02");
                    cols.Append(" ,ITEM03");
                    cols.Append(" ,ITEM04");
                    cols.Append(" ,ITEM05");
                    cols.Append(" ,ITEM06");
                    cols.Append(" ,ITEM07");
                    cols.Append(" ,ITEM08");
                    cols.Append(" ,ITEM09");
                    cols.Append(" ,ITEM10");
                    cols.Append(" ,ITEM11");
                    cols.Append(" ,ITEM12");
                    cols.Append(" ,ITEM13");
                    cols.Append(" ,ITEM14");
                    cols.Append(" ,ITEM15");
                    cols.Append(" ,ITEM16");
                    cols.Append(" ,ITEM17");
                    cols.Append(" ,ITEM18");
                    cols.Append(" ,ITEM19");
                    cols.Append(" ,ITEM20");
                    cols.Append(" ,ITEM21");

                    // バインドパラメータの設定
                    DbParamCollection dpc = new DbParamCollection();
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this._unqId);

                    // データの取得
                    DataTable dtOutput = this._dba.GetDataTableByConditionWithParams(
                        Util.ToString(cols), "PR_ZM_TBL", "GUID = @GUID", "SORT ASC", dpc);

                    // 帳票オブジェクトをインスタンス化
                    ZMMR10211R rpt = new ZMMR10211R(dtOutput);

                    rpt.Document.Printer.DocumentName = Util.ToString(this._pForm.Condition["ReportName"]);
                    rpt.Document.Name = Util.ToString(this._pForm.Condition["ReportName"]);

                    if (isExcel)
                    {
                        GrapeCity.ActiveReports.Export.Excel.Section.XlsExport xlsExport1 = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
                        //SetExcelSetting(xlsExport1);
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Zai, rpt.Document.Name, 2);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            xlsExport1.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("EXCEL出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPdf)
                    {
                        GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport p = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                        rpt.Run();
                        string saveFileName = Util.GetSavePath(Constants.SubSys.Zai, rpt.Document.Name, 1);
                        if (!ValChk.IsEmpty(saveFileName))
                        {
                            p.Export(rpt.Document, saveFileName);
                            Msg.InfoNm("PDF出力", "保存しました。");
                            Util.OpenFolder(saveFileName);
                        }
                    }
                    else if (isPreview)
                    {
                        // プレビュー画面表示
                        PreviewForm pFrm = new PreviewForm(rpt, this._unqId);
                        pFrm.WindowState = FormWindowState.Maximized;
                        pFrm.Show();
                    }
                    else
                    {
                        // 直接印刷
                        rpt.Run(false);
                        rpt.Document.Print(true, true, false);
                    }
                }
                else
                {
                    Msg.Info("該当するデータがありません。");
                }
            }
            /*bool dataFlag;
            try
            {
                this._dba.BeginTransaction();

                // 帳票出力用にワークテーブルにデータを作成
                dataFlag = MakeWkData();
                this._dba.Commit();
            }
            catch (Exception e)
            {
                Msg.Error(e.Message);
                this._dba.Rollback();
                return;
            }
            finally
            {
                //this._dba.Rollback();
            }

            // 帳票出力
            if (dataFlag)
            {
                Report rpt = new Report();
                rpt.OutputReport(Path.Combine(Util.GetPath(), Constants.REP_DIR, "ZAMR2021.mdb"), "R_ZAMR2021", this._unqId, isPreview);
            }

            // ワークテーブルに作成したデータを削除
            try
            {
                this._dba.BeginTransaction();

                // 帳票出力用に作成したデータを削除
                DbParamCollection dpc = new DbParamCollection();
                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this._unqId);
                this._dba.Delete("PR_ZM_TBL", "GUID = @GUID", dpc);
                this._dba.Commit();
            }
            catch (Exception e)
            {
                Msg.Error(e.Message);
                this._dba.Rollback();
            }*/
            finally
            {
                //this._dba.Rollback();

                msgFrm.Close();
            }
        }
        #endregion

        #region privateメソッド
        /// <summary>
        /// 抽出条件を元にワークテーブルのデータを作成します。
        /// </summary>
        private bool MakeWkData()
        {
            #region ループ準備処理
            ZMMR1021DA da = new ZMMR1021DA(this._uInfo, this._dba, this._config);
            Summary sumInfo = new Summary();
            Summary sumMonth = new Summary();
            DataTable aiteKamokuCd = new DataTable();

            int i = 0; // ループカウント変数
            int j = 0; // 貸借データ表示用ループカウント変数
            int dbSORT = 1; // ソート用カウント変数
            int pageCount = 1; // 改ページ時繰越判断用カウント
            //string kurikoshiTekiyo = ""; // 改ページ時に表示する摘要内容を格納する変数
            string denpyoDate = ""; // 日付用変数
            int hojoKamokuCd; // 補助科目用変数
            string hojoKamokuNm = ""; // 補助科目名用変数
            int aiteKanjoKamokuCd; // 相手勘定科目用変数
            string aiteKanjoKamokuNm = ""; // 相手勘定科目名用変数
            int aiteHojoKamokuCd = 0; // 相手補助科目用変数
            string aiteHojoKamokuNm = ""; // 補助科目名用変数
            int taishakuKubun = 0; // 勘定科目の貸借区分用変数
            int kaikeiNendo = 0; // 会計年度
            int kanjoKamokuCd = Util.ToInt(this._pForm.Condition["KanjoKamokuCd"]); // 勘定科目コードを取得
            string kanjoKamokuNm = Util.ToString(this._pForm.Condition["KanjoKamokuNm"]); // 勘定科目名を取得
            string zeiHyojiJoho = Util.ToString(this._pForm.Condition["ShohizeiShori"]); // 税表示情報を取得
            string injiJoho = Util.ToString(this._pForm.Condition["Inji"]); // 印字設定情報を取得
            string[] hikakuDenpyoDateWareki = new string[] { "", "", "0", "" };
            // 支所コード
            int shishoCd = 0;
            // 空回避用
            int kanjoKamokuRowCount = 0;
            // 改ページ用
            string keyData = "";
            // 出力日付
            string[] tmpnowDate = Util.ConvJpDate(Util.ToDate(this._pForm.Condition["ShurutyokuDt"]), this._dba);
            _outputDate = tmpnowDate[5];
            // 税率
            string taxRate = "";

            decimal lineCount = 0m;
            #endregion

            #region 仕訳対象勘定科目毎にデータを印刷ワークテーブルに登録
            while (this._pForm.HojoKamokuData.Rows.Count > i)
            {
                sumInfo.Clear(); // 勘定科目毎の合計値を初期化
                pageCount = 1; // 改ページ時繰越判断用カウントを初期化

                // 入力された情報を元にワークテーブルに更新をする
                DbParamCollection dpc = new DbParamCollection();
                StringBuilder Sql = new StringBuilder();
                // 補助科目コードの取得
                hojoKamokuCd = Util.ToInt(this._pForm.HojoKamokuData.Rows[i]["HOJO_KAMOKU_CD"]);
                // 支所コード
                shishoCd = Util.ToInt(Util.ToString(this._pForm.HojoKamokuData.Rows[i]["SHISHO_CD"]));
                // 補助科目名の取得
                //hojoKamokuNm = da.GetHojoKamokuNm(kanjoKamokuCd, hojoKamokuCd);
                hojoKamokuNm = da.GetHojoKamokuNm(kanjoKamokuCd, hojoKamokuCd, shishoCd);
                // 勘定科目の貸借区分を取得
                //DataTable kamokuJoho = da.GetKamokuJoho(this._pForm.Condition, hojoKamokuCd);
                DataTable kamokuJoho = da.GetKamokuJoho(this._pForm.Condition, hojoKamokuCd, shishoCd);
                if (kamokuJoho.Rows.Count != 0)
                {
                    taishakuKubun = Util.ToInt(kamokuJoho.Rows[0]["TAISHAKU_KUBUN"]);
                }
                else
                {
                    taishakuKubun = 0;
                }
                // 貸借データの作成
                //this._dsTaishakuData = da.GetTaishakuData(this._pForm.Condition, hojoKamokuCd);
                this._dsTaishakuData = da.GetTaishakuData(this._pForm.Condition, hojoKamokuCd, shishoCd);
                // 残高の取得
                if (kamokuJoho.Rows.Count != 0)
                {
                    sumInfo.zan += Util.ToInt(kamokuJoho.Rows[0]["KURIKOSHI_ZANDAKA"]);
                }
                else
                {
                    sumInfo.zan += 0;
                }

                // 金額がｾﾞﾛの科目を印字する場合
                kanjoKamokuRowCount = this._dsTaishakuData.Rows.Count;
                if (injiJoho == "yes" || this._dsTaishakuData.Rows.Count > 0)
                {
                    #region 繰越データを表示
                    #region インサートテーブル
                    Sql = new StringBuilder();
                    dpc = new DbParamCollection();
                    Sql.Append("INSERT INTO PR_ZM_TBL(");
                    Sql.Append("  GUID");
                    Sql.Append(" ,SORT");
                    Sql.Append(" ,ITEM01");
                    Sql.Append(" ,ITEM02");
                    Sql.Append(" ,ITEM03");
                    Sql.Append(" ,ITEM04");
                    Sql.Append(" ,ITEM05");
                    Sql.Append(" ,ITEM06");
                    Sql.Append(" ,ITEM13");
                    Sql.Append(" ,ITEM18");
                    Sql.Append(" ,ITEM19");
                    Sql.Append(" ,ITEM20");
                    Sql.Append(") ");
                    Sql.Append("VALUES(");
                    Sql.Append("  @GUID");
                    Sql.Append(" ,@SORT");
                    Sql.Append(" ,@ITEM01");
                    Sql.Append(" ,@ITEM02");
                    Sql.Append(" ,@ITEM03");
                    Sql.Append(" ,@ITEM04");
                    Sql.Append(" ,@ITEM05");
                    Sql.Append(" ,@ITEM06");
                    Sql.Append(" ,@ITEM13");
                    Sql.Append(" ,@ITEM18");
                    Sql.Append(" ,@ITEM19");
                    Sql.Append(" ,@ITEM20");
                    Sql.Append(") ");
                    #endregion

                    #region データを設定
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this._unqId);
                    dpc.SetParam("@SORT", SqlDbType.VarChar, 9, dbSORT);
                    dbSORT++;
                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this._uInfo.KaishaNm); // 会社名
                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, zeiHyojiJoho); // 税表示情報
                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, kanjoKamokuCd); // 勘定科目番号
                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, kanjoKamokuNm); // 勘定科目名
                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, hojoKamokuCd); // 補助科目
                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, hojoKamokuNm); // 補助科目名

                    dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200, "繰　越"); // 摘要
                    dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, Util.FormatNum(sumInfo.zan)); // 残高
                    // 出力日付
                    if (Util.ToInt(this._pForm.Condition["check06"]) == 0)
                    {
                        dpc.SetParam("@ITEM19", SqlDbType.VarChar, 200, ""); // 出力日付
                    }
                    else
                    {
                        //dpc.SetParam("@ITEM19", SqlDbType.VarChar, 200, Util.ToDate(this._pForm.Condition["ShurutyokuDt"]).Date.ToString("yyyy/MM/dd")); // 出力日付
                        dpc.SetParam("@ITEM19", SqlDbType.VarChar, 200, _outputDate); // 出力日付
                    }
                    keyData = shishoCd.ToString("000");
                    keyData += Util.ToInt(kanjoKamokuCd).ToString("00000000") + hojoKamokuCd.ToString("00000000");
                    dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, keyData);
                    #endregion

                    this._dba.ModifyBySql(Util.ToString(Sql), dpc);
                    #endregion
                }

                #region 実データ登録
                while (this._dsTaishakuData.Rows.Count > j)
                {
                    #region インサートテーブル
                    Sql = new StringBuilder();
                    dpc = new DbParamCollection();
                    Sql.Append("INSERT INTO PR_ZM_TBL(");
                    Sql.Append("  GUID");
                    Sql.Append(" ,SORT");
                    Sql.Append(" ,ITEM01");
                    Sql.Append(" ,ITEM02");
                    Sql.Append(" ,ITEM03");
                    Sql.Append(" ,ITEM04");
                    Sql.Append(" ,ITEM05");
                    Sql.Append(" ,ITEM06");
                    Sql.Append(" ,ITEM07");
                    Sql.Append(" ,ITEM08");
                    Sql.Append(" ,ITEM09");
                    Sql.Append(" ,ITEM10");
                    Sql.Append(" ,ITEM11");
                    Sql.Append(" ,ITEM12");
                    Sql.Append(" ,ITEM13");
                    Sql.Append(" ,ITEM14");
                    Sql.Append(" ,ITEM15");
                    Sql.Append(" ,ITEM16");
                    Sql.Append(" ,ITEM17");
                    Sql.Append(" ,ITEM18");
                    Sql.Append(" ,ITEM19");
                    Sql.Append(" ,ITEM20");
                    Sql.Append(" ,ITEM21");
                    Sql.Append(") ");
                    Sql.Append("VALUES(");
                    Sql.Append("  @GUID");
                    Sql.Append(" ,@SORT");
                    Sql.Append(" ,@ITEM01");
                    Sql.Append(" ,@ITEM02");
                    Sql.Append(" ,@ITEM03");
                    Sql.Append(" ,@ITEM04");
                    Sql.Append(" ,@ITEM05");
                    Sql.Append(" ,@ITEM06");
                    Sql.Append(" ,@ITEM07");
                    Sql.Append(" ,@ITEM08");
                    Sql.Append(" ,@ITEM09");
                    Sql.Append(" ,@ITEM10");
                    Sql.Append(" ,@ITEM11");
                    Sql.Append(" ,@ITEM12");
                    Sql.Append(" ,@ITEM13");
                    Sql.Append(" ,@ITEM14");
                    Sql.Append(" ,@ITEM15");
                    Sql.Append(" ,@ITEM16");
                    Sql.Append(" ,@ITEM17");
                    Sql.Append(" ,@ITEM18");
                    Sql.Append(" ,@ITEM19");
                    Sql.Append(" ,@ITEM20");
                    Sql.Append(" ,@ITEM21");
                    Sql.Append(") ");
                    #endregion

                    #region データを設定
                    dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this._unqId);
                    dpc.SetParam("@SORT", SqlDbType.VarChar, 9, dbSORT);
                    dbSORT++;
                    dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this._uInfo.KaishaNm); // 会社名
                    dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, zeiHyojiJoho); // 税表示情報
                    dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, kanjoKamokuCd); // 勘定科目番号
                    dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, kanjoKamokuNm); // 勘定科目名
                    dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, hojoKamokuCd); // 補助科目
                    dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, hojoKamokuNm); // 補助科目名
                    denpyoDate = Util.ToString(this._dsTaishakuData.Rows[j]["DENPYO_DATE"]);
                    // 日付範囲を和暦で保持
                    string[] denpyoDateWareki = Util.ConvJpDate(denpyoDate, this._dba);
                    if (denpyoDateWareki[3].Length == 1)
                    {
                        denpyoDateWareki[3] = " " + denpyoDateWareki[3];
                    }
                    if (denpyoDateWareki[4].Length == 1)
                    {
                        denpyoDateWareki[4] = " " + denpyoDateWareki[4];
                    }
                    denpyoDate = denpyoDateWareki[2] + "/" + denpyoDateWareki[3] + "/" + denpyoDateWareki[4];
                    dpc.SetParam("@ITEM07", SqlDbType.VarChar, 200, denpyoDate); // 日付
                    // 伝票番号
                    if (Util.ToInt(this._pForm.Condition["check01"]) == 0)
                    {
                        dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, "");
                    }
                    else
                    {
                        dpc.SetParam("@ITEM08", SqlDbType.VarChar, 200, this._dsTaishakuData.Rows[j]["DENPYO_BANGO"]);
                    }

                    // 会計年度
                    kaikeiNendo = Util.ToInt(this._dsTaishakuData.Rows[j]["KAIKEI_NENDO"]);

                    // 勘定科目が取得できなければ、諸口と表示
                    aiteKanjoKamokuNm = "　　諸　　口";

                    // 税率
                    taxRate = Util.FormatNum(Util.ToDecimal(this._dsTaishakuData.Rows[j]["ZEI_RITSU"].ToString()));
                    if (taxRate == "0")
                        taxRate = "";

                    // 相手補助科目コード
                    aiteHojoKamokuCd = Util.ToInt(this._dsTaishakuData.Rows[j]["AITE_HOJO_KAMOKU_CD"]);

                    // 相手勘定科目コード ADD By M.Touma 2015/03/19
                    aiteKanjoKamokuCd = Util.ToInt(this._dsTaishakuData.Rows[j]["AITE_KANJO_KAMOKU_CD"]);

                    // 相手勘定科目が取れた場合
                    if (aiteKanjoKamokuCd > 0)
                    {
                        aiteKanjoKamokuNm = this._dsTaishakuData.Rows[j]["AITE_KANJO_KAMOKU_NM"].ToString(); // ADD By M.Touma 2015/03/18
                        aiteKamokuCd = new DataTable();
                    }
                    else
                    {
                        // 相手勘定科目が取れない場合
                        // 支所コード
                        shishoCd = Util.ToInt(Util.ToString(this._dsTaishakuData.Rows[j]["SHISHO_CD"]));
                        // 取得した伝票番号、行番号、伝票日付、貸借区分から相手勘定科目コードを取得
                        //aiteKamokuCd = da.GetAinteKamokuCd(Util.ToInt(this._dsTaishakuData.Rows[j]["DENPYO_BANGO"]),
                        //this._dsTaishakuData.Rows[j]["DENPYO_DATE"].ToString(), Util.ToInt(this._dsTaishakuData.Rows[j]["GYO_BANGO"]),
                        //Util.ToInt(this._dsTaishakuData.Rows[j]["TAISHAKU_KUBUN"]), kaikeiNendo);
                        aiteKamokuCd = da.GetAinteKamokuCd(Util.ToInt(this._dsTaishakuData.Rows[j]["DENPYO_BANGO"]),
                        this._dsTaishakuData.Rows[j]["DENPYO_DATE"].ToString(), Util.ToInt(this._dsTaishakuData.Rows[j]["GYO_BANGO"]),
                        Util.ToInt(this._dsTaishakuData.Rows[j]["TAISHAKU_KUBUN"]), kaikeiNendo, shishoCd);
                        if (aiteKamokuCd.Rows.Count > 0)
                        {
                            aiteKanjoKamokuCd = Util.ToInt(aiteKamokuCd.Rows[0]["KANJO_KAMOKU_CD"]); // 相手勘定科目コード
                            aiteHojoKamokuCd = Util.ToInt(aiteKamokuCd.Rows[0]["HOJO_KAMOKU_CD"]);   // 相手補助科目番号
                        }

                        // 相手勘定科目コードが取得できれば、相手勘定科目名を取得する
                        if (aiteKanjoKamokuCd > 0)
                        {
                            //aiteKanjoKamokuNm = da.GetKanjoKamokuNm(aiteKanjoKamokuCd, 1, kaikeiNendo);
                            aiteKanjoKamokuNm = da.GetKanjoKamokuNm(aiteKanjoKamokuCd, 1, kaikeiNendo, shishoCd);
                        }
                    }

                    // 勘定科目コードを設定する
                    if (aiteKanjoKamokuCd > 0)
                    {
                        dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, aiteKanjoKamokuCd); // 相手科目番号
                    }
                    else
                    {
                        dpc.SetParam("@ITEM09", SqlDbType.VarChar, 200, ""); // 相手科目番号
                    }

                    dpc.SetParam("@ITEM10", SqlDbType.VarChar, 200, aiteKanjoKamokuNm); // 相手科目名

                    if (Util.ToInt(this._pForm.Condition["check02"]) == 0)
                    {
                        dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, ""); // 相手補助科目番号
                        dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, ""); // 相手補助科目名
                    }
                    else
                    {
                        // 支所コード
                        shishoCd = Util.ToInt(Util.ToString(this._dsTaishakuData.Rows[j]["SHISHO_CD"]));
                        // 相手補助科目名取得
                        aiteHojoKamokuNm = this._dsTaishakuData.Rows[j]["AITE_HOJO_KAMOKU_NM"].ToString();
                        if (aiteHojoKamokuNm == "" && aiteKamokuCd.Rows.Count > 0)
                        {
                            // 相手補助科目名をVI_ZM_HOJO_KAMOKUから取得
                            //aiteHojoKamokuNm = da.GetAiteHojoKamokuNm(aiteKanjoKamokuCd
                            //    , aiteHojoKamokuCd
                            //    , kaikeiNendo);
                            aiteHojoKamokuNm = da.GetAiteHojoKamokuNm(aiteKanjoKamokuCd
                                , aiteHojoKamokuCd
                                , kaikeiNendo
                                , shishoCd);
                        }

                        // 相手補助科目番号設定
                        if (aiteKanjoKamokuCd > 0 && aiteHojoKamokuCd > 0)
                        {
                            dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, aiteHojoKamokuCd);
                        }
                        else
                        {
                            dpc.SetParam("@ITEM11", SqlDbType.VarChar, 200, ""); // 相手補助科目番号

                            aiteHojoKamokuNm = ""; // 相手補助科目名
                        }
                        dpc.SetParam("@ITEM12", SqlDbType.VarChar, 200, aiteHojoKamokuNm); // 相手補助科目名
                    }

                    dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200, this._dsTaishakuData.Rows[j]["TEKIYO"]); // 摘要

                    // 貸借区分が借方の場合
                    if (Util.ToInt(this._dsTaishakuData.Rows[j]["TAISHAKU_KUBUN"]) == 1)
                    {
                        if (Util.ToInt(this._dsTaishakuData.Rows[j]["SHOHIZEI_KINGAKU"]) == 0 || Util.ToInt(this._pForm.Condition["check05"]) == 0)
                        {
                            dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, ""); // 借方消費税
                        }
                        else
                        {
                            dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, Util.FormatNum(this._dsTaishakuData.Rows[j]["SHOHIZEI_KINGAKU"])); // 借方消費税
                        }
                        if (Util.ToInt(this._pForm.Condition["ShohizeiShoriHandan"]) == 1)
                        {
                            dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200, Util.FormatNum(this._dsTaishakuData.Rows[j]["ZEIKOMI_KINGAKU"])); // 借方金額
                            sumMonth.kariAmount += Util.ToInt(this._dsTaishakuData.Rows[j]["ZEIKOMI_KINGAKU"]);
                            sumInfo.kariAmount += Util.ToInt(this._dsTaishakuData.Rows[j]["ZEIKOMI_KINGAKU"]);
                            // 勘定科目貸借区分が借方の場合
                            if (taishakuKubun == 1)
                            {
                                sumInfo.zan += Util.ToInt(this._dsTaishakuData.Rows[j]["ZEIKOMI_KINGAKU"]);
                            }
                            // 勘定科目貸借区分が貸方の場合
                            else
                            {
                                sumInfo.zan -= Util.ToInt(this._dsTaishakuData.Rows[j]["ZEIKOMI_KINGAKU"]);
                            }
                        }
                        else
                        {
                            dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200, Util.FormatNum(this._dsTaishakuData.Rows[j]["ZEINUKI_KINGAKU"])); // 借方金額
                            sumMonth.kariAmount += Util.ToInt(this._dsTaishakuData.Rows[j]["ZEINUKI_KINGAKU"]);
                            sumInfo.kariAmount += Util.ToInt(this._dsTaishakuData.Rows[j]["ZEINUKI_KINGAKU"]);
                            // 勘定科目貸借区分が借方の場合
                            if (taishakuKubun == 1)
                            {
                                sumInfo.zan += Util.ToInt(this._dsTaishakuData.Rows[j]["ZEINUKI_KINGAKU"]);
                            }
                            // 勘定科目貸借区分が貸方の場合
                            else
                            {
                                sumInfo.zan -= Util.ToInt(this._dsTaishakuData.Rows[j]["ZEINUKI_KINGAKU"]);
                            }
                        }
                        sumMonth.kariZei += Util.ToInt(this._dsTaishakuData.Rows[j]["SHOHIZEI_KINGAKU"]);
                        sumInfo.kariZei += Util.ToInt(this._dsTaishakuData.Rows[j]["SHOHIZEI_KINGAKU"]);
                        dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, ""); // 貸方消費税
                        dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, ""); // 貸方金額
                    }
                    // 貸借区分が貸方の場合
                    else
                    {
                        dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, ""); // 借方消費税
                        dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200, ""); // 借方金額
                        if (Util.ToInt(this._dsTaishakuData.Rows[j]["SHOHIZEI_KINGAKU"]) == 0 || Util.ToInt(this._pForm.Condition["check05"]) == 0)
                        {
                            dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, ""); // 貸方消費税
                        }
                        else
                        {
                            dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, Util.FormatNum(this._dsTaishakuData.Rows[j]["SHOHIZEI_KINGAKU"])); // 貸方消費税
                        }
                        if (Util.ToInt(this._pForm.Condition["ShohizeiShoriHandan"]) == 1)
                        {
                            dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, Util.FormatNum(this._dsTaishakuData.Rows[j]["ZEIKOMI_KINGAKU"])); // 貸方金額
                            sumMonth.kashiAmount += Util.ToInt(this._dsTaishakuData.Rows[j]["ZEIKOMI_KINGAKU"]);
                            sumInfo.kashiAmount += Util.ToInt(this._dsTaishakuData.Rows[j]["ZEIKOMI_KINGAKU"]);
                            // 勘定科目貸借区分が借方の場合
                            if (taishakuKubun == 1)
                            {
                                sumInfo.zan -= Util.ToInt(this._dsTaishakuData.Rows[j]["ZEIKOMI_KINGAKU"]);
                            }
                            // 勘定科目貸借区分が貸方の場合
                            else
                            {
                                sumInfo.zan += Util.ToInt(this._dsTaishakuData.Rows[j]["ZEIKOMI_KINGAKU"]);
                            }
                        }
                        else
                        {
                            dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, Util.FormatNum(this._dsTaishakuData.Rows[j]["ZEINUKI_KINGAKU"])); // 貸方金額
                            sumMonth.kashiAmount += Util.ToInt(this._dsTaishakuData.Rows[j]["ZEINUKI_KINGAKU"]);
                            sumInfo.kashiAmount += Util.ToInt(this._dsTaishakuData.Rows[j]["ZEINUKI_KINGAKU"]);
                            // 勘定科目貸借区分が借方の場合
                            if (taishakuKubun == 1)
                            {
                                sumInfo.zan -= Util.ToInt(this._dsTaishakuData.Rows[j]["ZEINUKI_KINGAKU"]);
                            }
                            // 勘定科目貸借区分が貸方の場合
                            else
                            {
                                sumInfo.zan += Util.ToInt(this._dsTaishakuData.Rows[j]["ZEINUKI_KINGAKU"]);
                            }
                        }
                        sumMonth.kashiZei += Util.ToInt(this._dsTaishakuData.Rows[j]["SHOHIZEI_KINGAKU"]);
                        sumInfo.kashiZei += Util.ToInt(this._dsTaishakuData.Rows[j]["SHOHIZEI_KINGAKU"]);
                    }

                    dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, Util.FormatNum(sumInfo.zan)); // 残高

                    // 出力日付
                    if (Util.ToInt(this._pForm.Condition["check06"]) == 0)
                    {
                        dpc.SetParam("@ITEM19", SqlDbType.VarChar, 200, ""); // 出力日付
                    }
                    else
                    {
                        //dpc.SetParam("@ITEM19", SqlDbType.VarChar, 200, Util.ToDate(this._pForm.Condition["ShurutyokuDt"]).Date.ToString("yyyy/MM/dd")); // 出力日付
                        dpc.SetParam("@ITEM19", SqlDbType.VarChar, 200, _outputDate); // 出力日付
                    }
                    keyData = shishoCd.ToString("000");
                    keyData += Util.ToInt(kanjoKamokuCd).ToString("00000000") + hojoKamokuCd.ToString("00000000");
                    dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, keyData);
                    dpc.SetParam("@ITEM21", SqlDbType.VarChar, 200, taxRate);
                    #endregion

                    // DBへ出力する
                    this._dba.ModifyBySql(Util.ToString(Sql), dpc);

                    // 実績のカウント
                    lineCount++;

                    // 現月と次月をを比較し、月が一致しなければ月合計を表示
                    if (j + 1 != this._dsTaishakuData.Rows.Count)
                    {
                        denpyoDate = Util.ToString(this._dsTaishakuData.Rows[j + 1]["DENPYO_DATE"]);
                        // 日付範囲を和暦で保持
                        hikakuDenpyoDateWareki = Util.ConvJpDate(denpyoDate, this._dba);
                        if (hikakuDenpyoDateWareki[3].Length == 1)
                        {
                            hikakuDenpyoDateWareki[3] = " " + hikakuDenpyoDateWareki[3];
                        }
                    }

                    pageCount++;

                    #region 登録データ数が22の倍数の時、データを次ﾍﾟｰｼﾞへ繰越
                    // 改ページ時は、繰越の印字をする
                    if (this.judgeNewPage(pageCount))
                    {
                        //dbSORT = this.printKurikoshiGyo(dbSORT
                        //    , sumInfo, zeiHyojiJoho
                        //    , kanjoKamokuCd, kanjoKamokuNm
                        //    , hojoKamokuCd, hojoKamokuNm
                        //    );
                        dbSORT = this.printKurikoshiGyo(dbSORT
                            , sumInfo, zeiHyojiJoho
                            , kanjoKamokuCd, kanjoKamokuNm
                            , hojoKamokuCd, hojoKamokuNm
                            , shishoCd
                            );

                        pageCount = 1;
                    }
                    #endregion

                    // 月が変わるとき 又は 勘定科目最終行の場合は、合計行を印字する
                    if (Util.ToInt(denpyoDateWareki[3]) != Util.ToInt(hikakuDenpyoDateWareki[3])
                        || j + 1 == this._dsTaishakuData.Rows.Count)
                    {
                        #region 月合計データを表示
                        // インサートテーブル 合計（累計）行出力
                        //dbSORT = this.printTotalRow(dbSORT
                        //    , zeiHyojiJoho           // 税表示情報
                        //    , kanjoKamokuCd          // 勘定科目番号
                        //    , kanjoKamokuNm          // 勘定科目名
                        //    , hojoKamokuCd           // 補助科目
                        //    , hojoKamokuNm           // 補助科目名
                        //    , "              " + denpyoDateWareki[3] + " 月 計" // 適用
                        //    , sumMonth.kariZei       // 借方消費税勘定科目合計
                        //    , sumMonth.kariAmount    // 借方金額勘定科目合計
                        //    , sumMonth.kashiZei      // 貸方消費税勘定科目合計
                        //    , sumMonth.kashiAmount   // 貸方金額勘定科目合計
                        //    , sumInfo.zan            // 残高
                        //    , false                  // 残高印字フラグ
                        //    );
                        dbSORT = this.printTotalRow(dbSORT
                            , zeiHyojiJoho           // 税表示情報
                            , kanjoKamokuCd          // 勘定科目番号
                            , kanjoKamokuNm          // 勘定科目名
                            , hojoKamokuCd           // 補助科目
                            , hojoKamokuNm           // 補助科目名
                            , "              " + denpyoDateWareki[3] + " 月 計" // 適用
                            , sumMonth.kariZei       // 借方消費税勘定科目合計
                            , sumMonth.kariAmount    // 借方金額勘定科目合計
                            , sumMonth.kashiZei      // 貸方消費税勘定科目合計
                            , sumMonth.kashiAmount   // 貸方金額勘定科目合計
                            , sumInfo.zan            // 残高
                            , false                  // 残高印字フラグ
                            , shishoCd
                            );

                        // 月毎の合計値を初期化
                        sumMonth.Clear();
                        #endregion

                        pageCount++;

                        #region 登録データ数が22の倍数の時、データを次ﾍﾟｰｼﾞへ繰越
                        // 改ページ時は、繰越の印字をする
                        if (this.judgeNewPage(pageCount))
                        {
                            //dbSORT = this.printKurikoshiGyo(dbSORT
                            //    , sumInfo, zeiHyojiJoho
                            //    , kanjoKamokuCd, kanjoKamokuNm
                            //    , hojoKamokuCd, hojoKamokuNm
                            //    );
                            dbSORT = this.printKurikoshiGyo(dbSORT
                                , sumInfo, zeiHyojiJoho
                                , kanjoKamokuCd, kanjoKamokuNm
                                , hojoKamokuCd, hojoKamokuNm
                                , shishoCd
                                );

                            pageCount = 1;
                        }
                        #endregion
                    }

                    j++;
                }

                // 明細出力が終わったら累計を出力する
                if (this._dsTaishakuData.Rows.Count <= j)
                {
                    // インサートテーブル 合計（累計）行出力
                    //dbSORT = this.printTotalRow(dbSORT
                    //    , zeiHyojiJoho           // 税表示情報
                    //    , kanjoKamokuCd          // 勘定科目番号
                    //    , kanjoKamokuNm          // 勘定科目名
                    //    , hojoKamokuCd           // 補助科目
                    //    , hojoKamokuNm           // 補助科目名
                    //    , "               累 計" // 適用
                    //    , sumInfo.kariZei        // 借方消費税勘定科目合計
                    //    , sumInfo.kariAmount     // 借方金額勘定科目合計
                    //    , sumInfo.kashiZei       // 貸方消費税勘定科目合計
                    //    , sumInfo.kashiAmount    // 貸方金額勘定科目合計
                    //    , sumInfo.zan            // 残高
                    //    , true                   // 残高印字フラグ
                    //    );
                    dbSORT = this.printTotalRow(dbSORT
                        , zeiHyojiJoho           // 税表示情報
                        , kanjoKamokuCd          // 勘定科目番号
                        , kanjoKamokuNm          // 勘定科目名
                        , hojoKamokuCd           // 補助科目
                        , hojoKamokuNm           // 補助科目名
                        , "               累 計" // 適用
                        , sumInfo.kariZei        // 借方消費税勘定科目合計
                        , sumInfo.kariAmount     // 借方金額勘定科目合計
                        , sumInfo.kashiZei       // 貸方消費税勘定科目合計
                        , sumInfo.kashiAmount    // 貸方金額勘定科目合計
                        , sumInfo.zan            // 残高
                        , true                   // 残高印字フラグ
                        , shishoCd
                        );
                }
                #endregion
                j = 0;
                i++;
            }

            // 印字データが無い場合
            if (lineCount == 0)
                return false;

            #endregion

            return true;
        }

        /// <summary>
        /// 改ページ判定
        /// </summary>
        /// <param name="pageCount">あるページの現在印刷行</param>
        /// <returns></returns>
        private bool judgeNewPage(int pageCount)
        {
            // １ページで22行以上出力された場合に改ページする
            if (pageCount >= 22)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 繰越印字
        /// </summary>
        /// <param name="dbSORT">印刷行番号（ソート順）</param>
        /// <param name="sumInfo">勘定科目毎の合計値</param>
        /// <param name="zeiHyojiJoho">税表示情報</param>
        /// <param name="kanjoKamokuCd">勘定科目コード</param>
        /// <param name="kanjoKamokuNm">勘定科目名</param>
        /// <praam name="hojoKamokuCd">補助科目コード</praam>
        /// <param name="hojoKamokuNm">補助科目名</param>
        /// <param name="shishoCd">支所コード</param>
        /// <returns>印刷行番号（ソート順）</returns>
        private int printKurikoshiGyo(int dbSORT, Summary sumInfo, string zeiHyojiJoho, int kanjoKamokuCd, string kanjoKamokuNm, int hojoKamokuCd, string hojoKamokuNm, int shishoCd)
        {

            StringBuilder Sql = new StringBuilder();
            DbParamCollection dpc;
            string[] tekiyo = new string[] { "  ＜ 次 頁 へ 繰 越 ＞", "  ＜ 前 頁 よ り 繰 越 ＞" };

            #region インサートテーブル
            Sql.Append("INSERT INTO PR_ZM_TBL(");
            Sql.Append("  GUID");
            Sql.Append(" ,SORT");
            Sql.Append(" ,ITEM01");
            Sql.Append(" ,ITEM02");
            Sql.Append(" ,ITEM03");
            Sql.Append(" ,ITEM04");
            Sql.Append(" ,ITEM05");
            Sql.Append(" ,ITEM06");
            Sql.Append(" ,ITEM13");
            Sql.Append(" ,ITEM15");
            Sql.Append(" ,ITEM17");
            Sql.Append(" ,ITEM18");
            Sql.Append(" ,ITEM19");
            Sql.Append(" ,ITEM20");
            Sql.Append(") ");
            Sql.Append("VALUES(");
            Sql.Append("  @GUID");
            Sql.Append(" ,@SORT");
            Sql.Append(" ,@ITEM01");
            Sql.Append(" ,@ITEM02");
            Sql.Append(" ,@ITEM03");
            Sql.Append(" ,@ITEM04");
            Sql.Append(" ,@ITEM05");
            Sql.Append(" ,@ITEM06");
            Sql.Append(" ,@ITEM13");
            Sql.Append(" ,@ITEM15");
            Sql.Append(" ,@ITEM17");
            Sql.Append(" ,@ITEM18");
            Sql.Append(" ,@ITEM19");
            Sql.Append(" ,@ITEM20");
            Sql.Append(") ");
            #endregion

            #region データを設定
            // 出力日付
            string outputDate = "";
            if (Util.ToInt(this._pForm.Condition["check06"]) != 0)
            {
                //outputDate = Util.ToDate(this._pForm.Condition["ShurutyokuDt"]).Date.ToString("yyyy/MM/dd");
                outputDate = _outputDate;
            }

            for (int i = 0; i < 2; i++)
            {
                dpc = new DbParamCollection();
                dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this._unqId);
                dpc.SetParam("@SORT", SqlDbType.VarChar, 9, dbSORT);
                dbSORT++;
                dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this._uInfo.KaishaNm); // 会社名
                dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, zeiHyojiJoho); // 税表示情報
                dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, kanjoKamokuCd); // 勘定科目コード
                dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, kanjoKamokuNm); // 勘定科目名
                dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, hojoKamokuCd); // 補助科目コード
                dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, hojoKamokuNm); // 補助科目名
                dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200, tekiyo[i]); // 摘要
                dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200, Util.FormatNum(sumInfo.kariAmount)); // 借方金額月合計
                dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, Util.FormatNum(sumInfo.kashiAmount)); // 貸方金額月合計
                dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, Util.FormatNum(sumInfo.zan)); // 残高
                dpc.SetParam("@ITEM19", SqlDbType.VarChar, 200, outputDate); // 出力日付

                string keyData = shishoCd.ToString("000");
                keyData += Util.ToInt(kanjoKamokuCd).ToString("00000000") + hojoKamokuCd.ToString("00000000");
                dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, keyData);

                // DB書き込み
                this._dba.ModifyBySql(Sql.ToString(), dpc);
            }
            #endregion

            return dbSORT;
        }

        /// <summary>
        /// 合計行出力
        /// </summary>
        /// <param name="dbSORT">印刷行番号（ソート順）</param>
        /// <param name="zeiHyojiJoho">税表示情報</param>
        /// <param name="kanjoKamokuCd">勘定科目番号</param>
        /// <param name="kanjoKamokuNm">勘定科目名</param>
        /// <param name="hojoKamokuCd">補助科目コード</param>
        /// <param name="hojoKamokuNm">補助科目名</param>
        /// <param name="tekiyo">摘要</param>
        /// <param name="kariZei">借方消費税月合計</param>
        /// <param name="kariAmount">借方金額月合計</param>
        /// <param name="kashiZei">貸方消費税月合計</param>
        /// <param name="kashiAmount">貸方金額月合計</param>
        /// <param name="zan">残高</param>
        /// <param name="zanPrintFlg">残高印字フラグ</param>
        /// <param name="shishoCd">支所コード</param>
        /// <returns>印刷行番号（ソート順）</returns>
        private int printTotalRow(int dbSORT, string zeiHyojiJoho, int kanjoKamokuCd, string kanjoKamokuNm,
              int hojoKamokuCd, string hojoKamokuNm, string tekiyo,
            decimal kariZei, decimal kariAmount, decimal kashiZei, decimal kashiAmount, decimal zan, bool zanPrintFlg, int shishoCd
            )
        {
            #region 累計データを表示
            StringBuilder Sql = new StringBuilder();
            DbParamCollection dpc = new DbParamCollection();

            #region インサートテーブル
            Sql = new StringBuilder();
            dpc = new DbParamCollection();
            Sql.Append("INSERT INTO PR_ZM_TBL(");
            Sql.Append("  GUID");
            Sql.Append(" ,SORT");
            Sql.Append(" ,ITEM01");
            Sql.Append(" ,ITEM02");
            Sql.Append(" ,ITEM03");
            Sql.Append(" ,ITEM04");
            Sql.Append(" ,ITEM05");
            Sql.Append(" ,ITEM06");
            Sql.Append(" ,ITEM13");
            Sql.Append(" ,ITEM14");
            Sql.Append(" ,ITEM15");
            Sql.Append(" ,ITEM16");
            Sql.Append(" ,ITEM17");
            Sql.Append(" ,ITEM18");
            Sql.Append(" ,ITEM19");
            Sql.Append(" ,ITEM20");
            Sql.Append(") ");
            Sql.Append("VALUES(");
            Sql.Append("  @GUID");
            Sql.Append(" ,@SORT");
            Sql.Append(" ,@ITEM01");
            Sql.Append(" ,@ITEM02");
            Sql.Append(" ,@ITEM03");
            Sql.Append(" ,@ITEM04");
            Sql.Append(" ,@ITEM05");
            Sql.Append(" ,@ITEM06");
            Sql.Append(" ,@ITEM13");
            Sql.Append(" ,@ITEM14");
            Sql.Append(" ,@ITEM15");
            Sql.Append(" ,@ITEM16");
            Sql.Append(" ,@ITEM17");
            Sql.Append(" ,@ITEM18");
            Sql.Append(" ,@ITEM19");
            Sql.Append(" ,@ITEM20");
            Sql.Append(") ");
            #endregion

            #region データを設定
            dpc.SetParam("@GUID", SqlDbType.VarChar, 36, this._unqId);
            dpc.SetParam("@SORT", SqlDbType.VarChar, 9, dbSORT);
            dbSORT++;
            dpc.SetParam("@ITEM01", SqlDbType.VarChar, 200, this._uInfo.KaishaNm); // 会社名
            dpc.SetParam("@ITEM02", SqlDbType.VarChar, 200, zeiHyojiJoho); // 税表示情報
            dpc.SetParam("@ITEM03", SqlDbType.VarChar, 200, kanjoKamokuCd); // 勘定科目番号
            dpc.SetParam("@ITEM04", SqlDbType.VarChar, 200, kanjoKamokuNm); // 勘定科目名
            dpc.SetParam("@ITEM05", SqlDbType.VarChar, 200, hojoKamokuCd); // 補助科目
            dpc.SetParam("@ITEM06", SqlDbType.VarChar, 200, hojoKamokuNm); // 補助科目名
            dpc.SetParam("@ITEM13", SqlDbType.VarChar, 200, tekiyo); // 摘要

            // 借方消費税勘定科目合計
            if (kariZei == 0)
            {
                dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, "");
            }
            else
            {
                dpc.SetParam("@ITEM14", SqlDbType.VarChar, 200, Util.FormatNum(kariZei));
            }

            // 借方金額勘定科目合計
            if (kariAmount == 0)
            {
                dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200, "");
            }
            else
            {
                dpc.SetParam("@ITEM15", SqlDbType.VarChar, 200, Util.FormatNum(kariAmount));
            }

            // 貸方消費税勘定科目合計
            if (kashiZei == 0)
            {
                dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, "");
            }
            else
            {
                dpc.SetParam("@ITEM16", SqlDbType.VarChar, 200, Util.FormatNum(kashiZei));
            }

            // 貸方金額勘定科目合計
            if (kashiAmount == 0)
            {
                dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, "");
            }
            else
            {
                dpc.SetParam("@ITEM17", SqlDbType.VarChar, 200, Util.FormatNum(kashiAmount));
            }

            // 残高
            if (zanPrintFlg)
            {
                dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, Util.FormatNum(zan));
            }
            else
            {
                dpc.SetParam("@ITEM18", SqlDbType.VarChar, 200, "");
            }

            // 出力日付
            if (Util.ToInt(this._pForm.Condition["check06"]) == 0)
            {
                dpc.SetParam("@ITEM19", SqlDbType.VarChar, 200, ""); // 出力日付
            }
            else
            {
                dpc.SetParam("@ITEM19", SqlDbType.VarChar, 200, Util.ToDate(this._pForm.Condition["ShurutyokuDt"]).Date.ToString("yyyy/MM/dd")); // 出力日付
            }

            string keyData = shishoCd.ToString("000");
            keyData += Util.ToInt(kanjoKamokuCd).ToString("00000000") + hojoKamokuCd.ToString("00000000");
            dpc.SetParam("@ITEM20", SqlDbType.VarChar, 200, keyData);
            #endregion

            // 印刷用ワークテーブルに書き込む
            this._dba.ModifyBySql(Util.ToString(Sql), dpc);

            #endregion
            return dbSORT;
        }
        #endregion
    }
}
