﻿namespace jp.co.fsi.zm.zmmr1021
{
    /// <summary>
    /// ZAMR2021R の概要の説明です。
    /// </summary>
    partial class ZMMR10211R
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(ZMMR10211R));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.textBox6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCompanyName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtShohinCd = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtHyojiDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTanaban = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtPageCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtToday = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.textBox5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.textBox8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtJuni = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtIrisu = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSouko = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtShohinNm = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtKesusu01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBarasu01 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtKesusu02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBarasu02 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTyoboKingaku = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBarasu03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtKesusu03 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtSaiKingaku = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtKikaku = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.line17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.textBox7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.groupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.groupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShohinCd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHyojiDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTanaban)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtJuni)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIrisu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSouko)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShohinNm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKesusu01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBarasu01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKesusu02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBarasu02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTyoboKingaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBarasu03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKesusu03)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSaiKingaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKikaku)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.textBox6,
            this.txtTitle02,
            this.txtTitle03,
            this.txtTitle01,
            this.txtCompanyName,
            this.label1,
            this.txtShohinCd,
            this.txtHyojiDate,
            this.txtTanaban,
            this.textBox1,
            this.textBox2,
            this.label2,
            this.label3,
            this.txtPageCount,
            this.txtToday,
            this.textBox3,
            this.textBox4,
            this.textBox5,
            this.line9,
            this.line1,
            this.line3,
            this.line4,
            this.line5,
            this.line6,
            this.line7,
            this.line8,
            this.line10,
            this.textBox8});
            this.pageHeader.Height = 1.188976F;
            this.pageHeader.Name = "pageHeader";
            this.pageHeader.Format += new System.EventHandler(this.pageHeader_Format);
            // 
            // textBox6
            // 
            this.textBox6.Height = 0.3937007F;
            this.textBox6.Left = 0.02086614F;
            this.textBox6.MultiLine = false;
            this.textBox6.Name = "textBox6";
            this.textBox6.Style = "background-color: Cyan; font-family: ＭＳ ゴシック; font-size: 9pt; font-weight: bold; " +
    "text-align: center; vertical-align: middle";
            this.textBox6.Text = null;
            this.textBox6.Top = 0.7929134F;
            this.textBox6.Width = 7.45945F;
            // 
            // txtTitle02
            // 
            this.txtTitle02.Height = 0.1956693F;
            this.txtTitle02.Left = 0.7188977F;
            this.txtTitle02.MultiLine = false;
            this.txtTitle02.Name = "txtTitle02";
            this.txtTitle02.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold;" +
    " text-align: center; vertical-align: middle; ddo-char-set: 128";
            this.txtTitle02.Text = "相  手  科  目";
            this.txtTitle02.Top = 0.8972442F;
            this.txtTitle02.Width = 1.801969F;
            // 
            // txtTitle03
            // 
            this.txtTitle03.Height = 0.1956693F;
            this.txtTitle03.Left = 2.520866F;
            this.txtTitle03.MultiLine = false;
            this.txtTitle03.Name = "txtTitle03";
            this.txtTitle03.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold;" +
    " text-align: center; vertical-align: middle; ddo-char-set: 128";
            this.txtTitle03.Text = "摘     要";
            this.txtTitle03.Top = 0.8972442F;
            this.txtTitle03.Width = 1.682282F;
            // 
            // txtTitle01
            // 
            this.txtTitle01.Height = 0.1956693F;
            this.txtTitle01.Left = 0.01574803F;
            this.txtTitle01.MultiLine = false;
            this.txtTitle01.Name = "txtTitle01";
            this.txtTitle01.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold;" +
    " text-align: center; vertical-align: middle; ddo-char-set: 128";
            this.txtTitle01.Text = "年月日";
            this.txtTitle01.Top = 0.8972442F;
            this.txtTitle01.Width = 0.7031497F;
            // 
            // txtCompanyName
            // 
            this.txtCompanyName.DataField = "ITEM01";
            this.txtCompanyName.Height = 0.1968504F;
            this.txtCompanyName.Left = 0F;
            this.txtCompanyName.MultiLine = false;
            this.txtCompanyName.Name = "txtCompanyName";
            this.txtCompanyName.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 1";
            this.txtCompanyName.Text = "1";
            this.txtCompanyName.Top = 0.4118111F;
            this.txtCompanyName.Width = 2.871654F;
            // 
            // label1
            // 
            this.label1.Height = 0.2F;
            this.label1.HyperLink = null;
            this.label1.Left = 0F;
            this.label1.Name = "label1";
            this.label1.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.label1.Text = "【全社】";
            this.label1.Top = 0.211811F;
            this.label1.Width = 0.5858268F;
            // 
            // txtShohinCd
            // 
            this.txtShohinCd.DataField = "ITEM06";
            this.txtShohinCd.Height = 0.1688976F;
            this.txtShohinCd.Left = 3.49567F;
            this.txtShohinCd.MultiLine = false;
            this.txtShohinCd.Name = "txtShohinCd";
            this.txtShohinCd.Style = "font-family: ＭＳ 明朝; font-size: 12pt; font-weight: bold; text-align: left; ddo-cha" +
    "r-set: 1";
            this.txtShohinCd.Text = "6 ";
            this.txtShohinCd.Top = 0.2429134F;
            this.txtShohinCd.Width = 1.619685F;
            // 
            // txtHyojiDate
            // 
            this.txtHyojiDate.DataField = "ITEM03";
            this.txtHyojiDate.Height = 0.2F;
            this.txtHyojiDate.Left = 2.871654F;
            this.txtHyojiDate.Name = "txtHyojiDate";
            this.txtHyojiDate.Style = "font-family: ＭＳ 明朝; font-size: 12pt; font-weight: bold; text-align: right; vertic" +
    "al-align: middle; ddo-char-set: 1";
            this.txtHyojiDate.Text = "3";
            this.txtHyojiDate.Top = 0.01181102F;
            this.txtHyojiDate.Width = 0.581496F;
            // 
            // txtTanaban
            // 
            this.txtTanaban.DataField = "ITEM05";
            this.txtTanaban.Height = 0.1688976F;
            this.txtTanaban.Left = 2.871654F;
            this.txtTanaban.MultiLine = false;
            this.txtTanaban.Name = "txtTanaban";
            this.txtTanaban.Style = "font-family: ＭＳ 明朝; font-size: 12pt; font-weight: bold; text-align: right; ddo-ch" +
    "ar-set: 1";
            this.txtTanaban.Text = "5";
            this.txtTanaban.Top = 0.2429134F;
            this.txtTanaban.Width = 0.5814955F;
            // 
            // textBox1
            // 
            this.textBox1.DataField = "ITEM04";
            this.textBox1.Height = 0.2F;
            this.textBox1.Left = 3.49567F;
            this.textBox1.MultiLine = false;
            this.textBox1.Name = "textBox1";
            this.textBox1.Style = "font-family: ＭＳ 明朝; font-size: 12pt; font-weight: bold; text-align: center; ddo-c" +
    "har-set: 1";
            this.textBox1.Text = "4普通預金";
            this.textBox1.Top = 0.01181102F;
            this.textBox1.Width = 1.015354F;
            // 
            // textBox2
            // 
            this.textBox2.DataField = "ITEM02";
            this.textBox2.Height = 0.1968504F;
            this.textBox2.Left = 6.535433F;
            this.textBox2.MultiLine = false;
            this.textBox2.Name = "textBox2";
            this.textBox2.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; vertical-align: middle; ddo-char-set: 1";
            this.textBox2.Text = "2";
            this.textBox2.Top = 0.5228347F;
            this.textBox2.Width = 0.8464575F;
            // 
            // label2
            // 
            this.label2.Height = 0.2F;
            this.label2.HyperLink = null;
            this.label2.Left = 6.535433F;
            this.label2.Name = "label2";
            this.label2.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.label2.Text = "【補助元帳】";
            this.label2.Top = 0.3228347F;
            this.label2.Width = 0.846457F;
            // 
            // label3
            // 
            this.label3.Height = 0.2F;
            this.label3.HyperLink = null;
            this.label3.Left = 7.223228F;
            this.label3.Name = "label3";
            this.label3.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.label3.Text = "頁";
            this.label3.Top = 0F;
            this.label3.Width = 0.2212596F;
            // 
            // txtPageCount
            // 
            this.txtPageCount.Height = 0.2F;
            this.txtPageCount.Left = 6.984252F;
            this.txtPageCount.MultiLine = false;
            this.txtPageCount.Name = "txtPageCount";
            this.txtPageCount.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.txtPageCount.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtPageCount.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.txtPageCount.Text = "999";
            this.txtPageCount.Top = 0F;
            this.txtPageCount.Width = 0.2433071F;
            // 
            // txtToday
            // 
            this.txtToday.DataField = "ITEM19";
            this.txtToday.Height = 0.1999999F;
            this.txtToday.Left = 5.504331F;
            this.txtToday.Name = "txtToday";
            this.txtToday.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.txtToday.Text = "txtToday";
            this.txtToday.Top = 0F;
            this.txtToday.Width = 1.479921F;
            // 
            // textBox3
            // 
            this.textBox3.Height = 0.1956694F;
            this.textBox3.Left = 5.196851F;
            this.textBox3.MultiLine = false;
            this.textBox3.Name = "textBox3";
            this.textBox3.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold;" +
    " text-align: center; vertical-align: middle; ddo-char-set: 128";
            this.textBox3.Text = "貸  方";
            this.textBox3.Top = 0.8972442F;
            this.textBox3.Width = 0.8937006F;
            // 
            // textBox4
            // 
            this.textBox4.Height = 0.1956693F;
            this.textBox4.Left = 6.141733F;
            this.textBox4.MultiLine = false;
            this.textBox4.Name = "textBox4";
            this.textBox4.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold;" +
    " text-align: center; vertical-align: middle; ddo-char-set: 128";
            this.textBox4.Text = "残   高";
            this.textBox4.Top = 0.8972442F;
            this.textBox4.Width = 1.338583F;
            // 
            // textBox5
            // 
            this.textBox5.Height = 0.1956693F;
            this.textBox5.Left = 4.251969F;
            this.textBox5.MultiLine = false;
            this.textBox5.Name = "textBox5";
            this.textBox5.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold;" +
    " text-align: center; vertical-align: middle; ddo-char-set: 128";
            this.textBox5.Text = "借  方";
            this.textBox5.Top = 0.8972442F;
            this.textBox5.Width = 0.8594489F;
            // 
            // line9
            // 
            this.line9.Height = 0F;
            this.line9.Left = 0F;
            this.line9.LineWeight = 1F;
            this.line9.Name = "line9";
            this.line9.Top = 1.186614F;
            this.line9.Width = 7.480315F;
            this.line9.X1 = 0F;
            this.line9.X2 = 7.480315F;
            this.line9.Y1 = 1.186614F;
            this.line9.Y2 = 1.186614F;
            // 
            // line1
            // 
            this.line1.Height = 0.3937007F;
            this.line1.Left = 6.102362F;
            this.line1.LineWeight = 2F;
            this.line1.Name = "line1";
            this.line1.Top = 0.7929134F;
            this.line1.Width = 0F;
            this.line1.X1 = 6.102362F;
            this.line1.X2 = 6.102362F;
            this.line1.Y1 = 1.186614F;
            this.line1.Y2 = 0.7929134F;
            // 
            // line3
            // 
            this.line3.Height = 0.3937013F;
            this.line3.Left = 5.157481F;
            this.line3.LineWeight = 2F;
            this.line3.Name = "line3";
            this.line3.Top = 0.7917318F;
            this.line3.Width = 0F;
            this.line3.X1 = 5.157481F;
            this.line3.X2 = 5.157481F;
            this.line3.Y1 = 1.185433F;
            this.line3.Y2 = 0.7917318F;
            // 
            // line4
            // 
            this.line4.Height = 0.393701F;
            this.line4.Left = 4.20315F;
            this.line4.LineWeight = 2F;
            this.line4.Name = "line4";
            this.line4.Top = 0.792913F;
            this.line4.Width = 0F;
            this.line4.X1 = 4.20315F;
            this.line4.X2 = 4.20315F;
            this.line4.Y1 = 1.186614F;
            this.line4.Y2 = 0.792913F;
            // 
            // line5
            // 
            this.line5.Height = 0.3937013F;
            this.line5.Left = 2.520866F;
            this.line5.LineWeight = 2F;
            this.line5.Name = "line5";
            this.line5.Top = 0.7917318F;
            this.line5.Width = 0F;
            this.line5.X1 = 2.520866F;
            this.line5.X2 = 2.520866F;
            this.line5.Y1 = 1.185433F;
            this.line5.Y2 = 0.7917318F;
            // 
            // line6
            // 
            this.line6.Height = 0.393701F;
            this.line6.Left = 0.7149607F;
            this.line6.LineWeight = 2F;
            this.line6.Name = "line6";
            this.line6.Top = 0.792913F;
            this.line6.Width = 0F;
            this.line6.X1 = 0.7149607F;
            this.line6.X2 = 0.7149607F;
            this.line6.Y1 = 1.186614F;
            this.line6.Y2 = 0.792913F;
            // 
            // line7
            // 
            this.line7.Height = 0.3937013F;
            this.line7.Left = 7.480315F;
            this.line7.LineWeight = 2F;
            this.line7.Name = "line7";
            this.line7.Top = 0.7917318F;
            this.line7.Width = 0F;
            this.line7.X1 = 7.480315F;
            this.line7.X2 = 7.480315F;
            this.line7.Y1 = 1.185433F;
            this.line7.Y2 = 0.7917318F;
            // 
            // line8
            // 
            this.line8.Height = 0.393701F;
            this.line8.Left = 0.007874016F;
            this.line8.LineWeight = 2F;
            this.line8.Name = "line8";
            this.line8.Top = 0.792913F;
            this.line8.Width = 0F;
            this.line8.X1 = 0.007874016F;
            this.line8.X2 = 0.007874016F;
            this.line8.Y1 = 1.186614F;
            this.line8.Y2 = 0.792913F;
            // 
            // line10
            // 
            this.line10.Height = 0.001181066F;
            this.line10.Left = 0F;
            this.line10.LineWeight = 2F;
            this.line10.Name = "line10";
            this.line10.Top = 0.7917323F;
            this.line10.Width = 7.480315F;
            this.line10.X1 = 0F;
            this.line10.X2 = 7.480315F;
            this.line10.Y1 = 0.7917323F;
            this.line10.Y2 = 0.7929134F;
            // 
            // textBox8
            // 
            this.textBox8.Height = 0.1956692F;
            this.textBox8.Left = 4.005906F;
            this.textBox8.MultiLine = false;
            this.textBox8.Name = "textBox8";
            this.textBox8.Style = "background-color: Cyan; font-family: ＭＳ 明朝; font-size: 9.75pt; font-weight: bold;" +
    " text-align: center; vertical-align: middle";
            this.textBox8.Text = "率";
            this.textBox8.Top = 0.8972442F;
            this.textBox8.Width = 0.1773621F;
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtJuni,
            this.txtIrisu,
            this.txtShohinNm,
            this.txtKesusu01,
            this.txtKesusu02,
            this.txtBarasu02,
            this.txtTyoboKingaku,
            this.txtBarasu03,
            this.txtKesusu03,
            this.txtSaiKingaku,
            this.line2,
            this.txtKikaku,
            this.line11,
            this.line12,
            this.line13,
            this.line14,
            this.line15,
            this.line16,
            this.line17,
            this.textBox7,
            this.txtSouko,
            this.txtBarasu01});
            this.detail.Height = 0.396063F;
            this.detail.Name = "detail";
            // 
            // txtJuni
            // 
            this.txtJuni.DataField = "ITEM04";
            this.txtJuni.Height = 0.2F;
            this.txtJuni.Left = 9.770473F;
            this.txtJuni.MultiLine = false;
            this.txtJuni.Name = "txtJuni";
            this.txtJuni.Style = "font-family: ＭＳ ゴシック; font-size: 9.75pt";
            this.txtJuni.Text = "04";
            this.txtJuni.Top = 1.352428F;
            this.txtJuni.Width = 1.157086F;
            // 
            // txtIrisu
            // 
            this.txtIrisu.DataField = "ITEM09";
            this.txtIrisu.Height = 0.1688976F;
            this.txtIrisu.Left = 0.7500001F;
            this.txtIrisu.MultiLine = false;
            this.txtIrisu.Name = "txtIrisu";
            this.txtIrisu.Style = "font-family: ＭＳ 明朝; font-size: 9pt; text-align: right; vertical-align: middle; dd" +
    "o-char-set: 128";
            this.txtIrisu.Text = "92345";
            this.txtIrisu.Top = 0F;
            this.txtIrisu.Width = 0.4031496F;
            // 
            // txtSouko
            // 
            this.txtSouko.DataField = "ITEM10";
            this.txtSouko.Height = 0.1688976F;
            this.txtSouko.Left = 1.181102F;
            this.txtSouko.MultiLine = false;
            this.txtSouko.Name = "txtSouko";
            this.txtSouko.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 1";
            this.txtSouko.Text = "10";
            this.txtSouko.Top = 0F;
            this.txtSouko.Width = 1.299213F;
            // 
            // txtShohinNm
            // 
            this.txtShohinNm.DataField = "ITEM07";
            this.txtShohinNm.Height = 0.1688976F;
            this.txtShohinNm.Left = 0.03661418F;
            this.txtShohinNm.MultiLine = false;
            this.txtShohinNm.Name = "txtShohinNm";
            this.txtShohinNm.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; ddo-char-set: 1";
            this.txtShohinNm.Text = "7";
            this.txtShohinNm.Top = 0.02086614F;
            this.txtShohinNm.Width = 0.6783465F;
            // 
            // txtKesusu01
            // 
            this.txtKesusu01.DataField = "ITEM11";
            this.txtKesusu01.Height = 0.1688976F;
            this.txtKesusu01.Left = 0.7500001F;
            this.txtKesusu01.MultiLine = false;
            this.txtKesusu01.Name = "txtKesusu01";
            this.txtKesusu01.Style = "font-family: ＭＳ 明朝; font-size: 9pt; text-align: right; vertical-align: middle; dd" +
    "o-char-set: 128";
            this.txtKesusu01.Text = "11";
            this.txtKesusu01.Top = 0.1897638F;
            this.txtKesusu01.Width = 0.4031496F;
            // 
            // txtBarasu01
            // 
            this.txtBarasu01.DataField = "ITEM12";
            this.txtBarasu01.Height = 0.1688976F;
            this.txtBarasu01.Left = 1.181102F;
            this.txtBarasu01.MultiLine = false;
            this.txtBarasu01.Name = "txtBarasu01";
            this.txtBarasu01.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 1";
            this.txtBarasu01.Text = "12,000.00";
            this.txtBarasu01.Top = 0.1897638F;
            this.txtBarasu01.Width = 1.299213F;
            // 
            // txtKesusu02
            // 
            this.txtKesusu02.DataField = "ITEM14";
            this.txtKesusu02.Height = 0.1688976F;
            this.txtKesusu02.Left = 4.246457F;
            this.txtKesusu02.MultiLine = false;
            this.txtKesusu02.Name = "txtKesusu02";
            this.txtKesusu02.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.txtKesusu02.Text = "14";
            this.txtKesusu02.Top = 0F;
            this.txtKesusu02.Width = 0.8283467F;
            // 
            // txtBarasu02
            // 
            this.txtBarasu02.DataField = "ITEM15";
            this.txtBarasu02.Height = 0.1688976F;
            this.txtBarasu02.Left = 4.218898F;
            this.txtBarasu02.MultiLine = false;
            this.txtBarasu02.Name = "txtBarasu02";
            this.txtBarasu02.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.txtBarasu02.Text = "9,999,999,999";
            this.txtBarasu02.Top = 0.1897638F;
            this.txtBarasu02.Width = 0.9110236F;
            // 
            // txtTyoboKingaku
            // 
            this.txtTyoboKingaku.DataField = "ITEM13";
            this.txtTyoboKingaku.Height = 0.1688976F;
            this.txtTyoboKingaku.Left = 2.55315F;
            this.txtTyoboKingaku.MultiLine = false;
            this.txtTyoboKingaku.Name = "txtTyoboKingaku";
            this.txtTyoboKingaku.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; ddo-char-set: 1";
            this.txtTyoboKingaku.Text = "13,000,000";
            this.txtTyoboKingaku.Top = 0.1897638F;
            this.txtTyoboKingaku.Width = 1.630314F;
            // 
            // txtBarasu03
            // 
            this.txtBarasu03.DataField = "ITEM17";
            this.txtBarasu03.Height = 0.1688976F;
            this.txtBarasu03.Left = 5.165355F;
            this.txtBarasu03.MultiLine = false;
            this.txtBarasu03.Name = "txtBarasu03";
            this.txtBarasu03.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.txtBarasu03.Text = "9,999,999,999";
            this.txtBarasu03.Top = 0.1897638F;
            this.txtBarasu03.Width = 0.9149609F;
            // 
            // txtKesusu03
            // 
            this.txtKesusu03.DataField = "ITEM16";
            this.txtKesusu03.Height = 0.1688976F;
            this.txtKesusu03.Left = 5.244095F;
            this.txtKesusu03.MultiLine = false;
            this.txtKesusu03.Name = "txtKesusu03";
            this.txtKesusu03.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; vertical-align: middle;" +
    " ddo-char-set: 1";
            this.txtKesusu03.Text = "16";
            this.txtKesusu03.Top = 0F;
            this.txtKesusu03.Width = 0.8283463F;
            // 
            // txtSaiKingaku
            // 
            this.txtSaiKingaku.DataField = "ITEM18";
            this.txtSaiKingaku.Height = 0.1688976F;
            this.txtSaiKingaku.Left = 6.220473F;
            this.txtSaiKingaku.MultiLine = false;
            this.txtSaiKingaku.Name = "txtSaiKingaku";
            this.txtSaiKingaku.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtSaiKingaku.Text = "999,999,999,999";
            this.txtSaiKingaku.Top = 0.1897638F;
            this.txtSaiKingaku.Width = 1.204724F;
            // 
            // line2
            // 
            this.line2.Height = 0F;
            this.line2.Left = 0F;
            this.line2.LineWeight = 1F;
            this.line2.Name = "line2";
            this.line2.Top = 0.3897638F;
            this.line2.Width = 7.480315F;
            this.line2.X1 = 0F;
            this.line2.X2 = 7.480315F;
            this.line2.Y1 = 0.3897638F;
            this.line2.Y2 = 0.3897638F;
            // 
            // txtKikaku
            // 
            this.txtKikaku.DataField = "ITEM08";
            this.txtKikaku.Height = 0.1688976F;
            this.txtKikaku.Left = 0.03661418F;
            this.txtKikaku.Name = "txtKikaku";
            this.txtKikaku.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: right; ddo-char-set: 1";
            this.txtKikaku.Text = "8";
            this.txtKikaku.Top = 0.1897638F;
            this.txtKikaku.Width = 0.6783465F;
            // 
            // line11
            // 
            this.line11.Height = 0.3897638F;
            this.line11.Left = 0.007874016F;
            this.line11.LineWeight = 2F;
            this.line11.Name = "line11";
            this.line11.Top = 0F;
            this.line11.Width = 9.313226E-10F;
            this.line11.X1 = 0.007874017F;
            this.line11.X2 = 0.007874016F;
            this.line11.Y1 = 0.3897638F;
            this.line11.Y2 = 0F;
            // 
            // line12
            // 
            this.line12.Height = 0.3897638F;
            this.line12.Left = 0.7188976F;
            this.line12.LineWeight = 2F;
            this.line12.Name = "line12";
            this.line12.Top = 0F;
            this.line12.Width = 1.192093E-07F;
            this.line12.X1 = 0.7188977F;
            this.line12.X2 = 0.7188976F;
            this.line12.Y1 = 0.3897638F;
            this.line12.Y2 = 0F;
            // 
            // line13
            // 
            this.line13.Height = 0.3897638F;
            this.line13.Left = 2.520866F;
            this.line13.LineWeight = 2F;
            this.line13.Name = "line13";
            this.line13.Top = 0F;
            this.line13.Width = 0F;
            this.line13.X1 = 2.520866F;
            this.line13.X2 = 2.520866F;
            this.line13.Y1 = 0.3897638F;
            this.line13.Y2 = 0F;
            // 
            // line14
            // 
            this.line14.Height = 0.3897638F;
            this.line14.Left = 4.20315F;
            this.line14.LineWeight = 2F;
            this.line14.Name = "line14";
            this.line14.Top = 0F;
            this.line14.Width = 0F;
            this.line14.X1 = 4.20315F;
            this.line14.X2 = 4.20315F;
            this.line14.Y1 = 0.3897638F;
            this.line14.Y2 = 0F;
            // 
            // line15
            // 
            this.line15.Height = 0.3897638F;
            this.line15.Left = 5.157481F;
            this.line15.LineWeight = 2F;
            this.line15.Name = "line15";
            this.line15.Top = 0F;
            this.line15.Width = 0F;
            this.line15.X1 = 5.157481F;
            this.line15.X2 = 5.157481F;
            this.line15.Y1 = 0.3897638F;
            this.line15.Y2 = 0F;
            // 
            // line16
            // 
            this.line16.Height = 0.3897638F;
            this.line16.Left = 6.102362F;
            this.line16.LineWeight = 2F;
            this.line16.Name = "line16";
            this.line16.Top = 0F;
            this.line16.Width = 0F;
            this.line16.X1 = 6.102362F;
            this.line16.X2 = 6.102362F;
            this.line16.Y1 = 0.3897638F;
            this.line16.Y2 = 0F;
            // 
            // line17
            // 
            this.line17.Height = 0.3897638F;
            this.line17.Left = 7.480315F;
            this.line17.LineWeight = 2F;
            this.line17.Name = "line17";
            this.line17.Top = 0F;
            this.line17.Width = 0F;
            this.line17.X1 = 7.480315F;
            this.line17.X2 = 7.480315F;
            this.line17.Y1 = 0.3897638F;
            this.line17.Y2 = 0F;
            // 
            // textBox7
            // 
            this.textBox7.DataField = "ITEM21";
            this.textBox7.Height = 0.1688976F;
            this.textBox7.Left = 4.005906F;
            this.textBox7.MultiLine = false;
            this.textBox7.Name = "textBox7";
            this.textBox7.Style = "font-family: ＭＳ 明朝; font-size: 9.75pt; text-align: left; vertical-align: middle; " +
    "ddo-char-set: 1";
            this.textBox7.Text = "92345";
            this.textBox7.Top = 0.02086614F;
            this.textBox7.Width = 0.1566929F;
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            // 
            // groupHeader1
            // 
            this.groupHeader1.DataField = "ITEM20";
            this.groupHeader1.Height = 0F;
            this.groupHeader1.Name = "groupHeader1";
            this.groupHeader1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
            this.groupHeader1.UnderlayNext = true;
            // 
            // groupFooter1
            // 
            this.groupFooter1.Height = 0F;
            this.groupFooter1.Name = "groupFooter1";
            // 
            // ZMMR10211R
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.6692914F;
            this.PageSettings.Margins.Left = 0.3937008F;
            this.PageSettings.Margins.Right = 0.3937008F;
            this.PageSettings.Margins.Top = 0.3937007F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Portrait;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.267716F;
            this.PrintWidth = 7.484252F;
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.groupHeader1);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.groupFooter1);
            this.Sections.Add(this.pageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-style: normal; text-decoration: none; font-weight: normal; font-size: 10pt; " +
            "color: Black; font-family: \"MS UI Gothic\"; ddo-char-set: 128", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; font-family: \"MS UI Gothic\"; ddo-char-set: 12" +
            "8", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 14pt; font-weight: bold; font-style: inherit; font-family: \"MS UI Goth" +
            "ic\"; ddo-char-set: 128", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ddo-char-set: 128", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.textBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCompanyName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShohinCd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHyojiDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTanaban)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtJuni)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIrisu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSouko)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShohinNm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKesusu01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBarasu01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKesusu02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBarasu02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTyoboKingaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBarasu03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKesusu03)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSaiKingaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtKikaku)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCompanyName;
        private GrapeCity.ActiveReports.SectionReportModel.Label label1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtShohinCd;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHyojiDate;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTanaban;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox2;
        private GrapeCity.ActiveReports.SectionReportModel.Label label2;
        private GrapeCity.ActiveReports.SectionReportModel.Label label3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPageCount;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtToday;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox5;
        private GrapeCity.ActiveReports.SectionReportModel.Line line9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtJuni;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtIrisu;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSouko;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtShohinNm;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKikaku;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKesusu01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBarasu01;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKesusu02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBarasu02;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTyoboKingaku;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBarasu03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtKesusu03;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSaiKingaku;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader groupHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter groupFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.Line line1;
        private GrapeCity.ActiveReports.SectionReportModel.Line line3;
        private GrapeCity.ActiveReports.SectionReportModel.Line line4;
        private GrapeCity.ActiveReports.SectionReportModel.Line line5;
        private GrapeCity.ActiveReports.SectionReportModel.Line line6;
        private GrapeCity.ActiveReports.SectionReportModel.Line line7;
        private GrapeCity.ActiveReports.SectionReportModel.Line line8;
        private GrapeCity.ActiveReports.SectionReportModel.Line line10;
        private GrapeCity.ActiveReports.SectionReportModel.Line line11;
        private GrapeCity.ActiveReports.SectionReportModel.Line line12;
        private GrapeCity.ActiveReports.SectionReportModel.Line line13;
        private GrapeCity.ActiveReports.SectionReportModel.Line line14;
        private GrapeCity.ActiveReports.SectionReportModel.Line line15;
        private GrapeCity.ActiveReports.SectionReportModel.Line line16;
        private GrapeCity.ActiveReports.SectionReportModel.Line line17;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox textBox7;
    }
}
