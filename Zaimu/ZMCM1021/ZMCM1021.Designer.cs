﻿namespace jp.co.fsi.zm.zmcm1021
{
    partial class ZMCM1021
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblKariukeShohizeiKamoku = new System.Windows.Forms.Label();
            this.txtKariukeShohizeiKamokuCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.gbxSenshuInfo = new System.Windows.Forms.GroupBox();
            this.lblDateDayFr = new System.Windows.Forms.Label();
            this.lblDateMonthFr = new System.Windows.Forms.Label();
            this.lblDateYearFr = new System.Windows.Forms.Label();
            this.txtDateDayFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDateYearFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtDateMonthFr = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblDateGengoFr = new System.Windows.Forms.Label();
            this.lblDateFr = new System.Windows.Forms.Label();
            this.lblKariukeShohizeiKamokuNm = new System.Windows.Forms.Label();
            this.lblKaribaraiShohizeiKamokuNm = new System.Windows.Forms.Label();
            this.lblShohizeiHasuShoriNm = new System.Windows.Forms.Label();
            this.lblShohizeiNyuryokuHohoNm = new System.Windows.Forms.Label();
            this.lblJigyoKubunNm = new System.Windows.Forms.Label();
            this.lblKojoHohoNm = new System.Windows.Forms.Label();
            this.lblKazeiHohoNm = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtChihoShohizeiRitsu = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblChihoShohizeiRitsu = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox14 = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKaribaraiShohizeiKamokuCd = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtShinShohizeiRitsu = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKyuShohizeiRitsu = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtShohizeiHasuShori = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtShohizeiNyuryokuHoho = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtJigyoKubun = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKojoHoho = new jp.co.fsi.common.controls.FsiTextBox();
            this.txtKazeiHoho = new jp.co.fsi.common.controls.FsiTextBox();
            this.lblKaribaraiShohizeiKamoku = new System.Windows.Forms.Label();
            this.lblShinShohizeiRitsu = new System.Windows.Forms.Label();
            this.lblKyuShohizeiRitsu = new System.Windows.Forms.Label();
            this.lblShohizeiHasuShori = new System.Windows.Forms.Label();
            this.lblShohizeiNyuryokuHoho = new System.Windows.Forms.Label();
            this.lblJigyoKubun = new System.Windows.Forms.Label();
            this.lblKojoHoho = new System.Windows.Forms.Label();
            this.lblKazeiHoho = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.pnlDebug.SuspendLayout();
            this.gbxSenshuInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Size = new System.Drawing.Size(818, 23);
            this.lblTitle.Text = "";
            // 
            // btnF3
            // 
            this.btnF3.Text = "F3\r\n\r\nさくじょ";
            // 
            // btnF6
            // 
            this.btnF6.Text = "F6\r\n\r\nとうろく";
            // 
            // pnlDebug
            // 
            this.pnlDebug.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.pnlDebug.Location = new System.Drawing.Point(5, 538);
            this.pnlDebug.Size = new System.Drawing.Size(851, 100);
            // 
            // lblKariukeShohizeiKamoku
            // 
            this.lblKariukeShohizeiKamoku.BackColor = System.Drawing.Color.Silver;
            this.lblKariukeShohizeiKamoku.Enabled = false;
            this.lblKariukeShohizeiKamoku.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKariukeShohizeiKamoku.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblKariukeShohizeiKamoku.Location = new System.Drawing.Point(6, 274);
            this.lblKariukeShohizeiKamoku.Name = "lblKariukeShohizeiKamoku";
            this.lblKariukeShohizeiKamoku.Size = new System.Drawing.Size(153, 25);
            this.lblKariukeShohizeiKamoku.TabIndex = 23;
            this.lblKariukeShohizeiKamoku.Text = "仮受消費税科目";
            this.lblKariukeShohizeiKamoku.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtKariukeShohizeiKamokuCd
            // 
            this.txtKariukeShohizeiKamokuCd.AllowDrop = true;
            this.txtKariukeShohizeiKamokuCd.AutoSizeFromLength = false;
            this.txtKariukeShohizeiKamokuCd.DisplayLength = null;
            this.txtKariukeShohizeiKamokuCd.Enabled = false;
            this.txtKariukeShohizeiKamokuCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKariukeShohizeiKamokuCd.Location = new System.Drawing.Point(109, 277);
            this.txtKariukeShohizeiKamokuCd.MaxLength = 6;
            this.txtKariukeShohizeiKamokuCd.Name = "txtKariukeShohizeiKamokuCd";
            this.txtKariukeShohizeiKamokuCd.ReadOnly = true;
            this.txtKariukeShohizeiKamokuCd.Size = new System.Drawing.Size(47, 20);
            this.txtKariukeShohizeiKamokuCd.TabIndex = 24;
            this.txtKariukeShohizeiKamokuCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // gbxSenshuInfo
            // 
            this.gbxSenshuInfo.Controls.Add(this.lblDateDayFr);
            this.gbxSenshuInfo.Controls.Add(this.lblDateMonthFr);
            this.gbxSenshuInfo.Controls.Add(this.lblDateYearFr);
            this.gbxSenshuInfo.Controls.Add(this.txtDateDayFr);
            this.gbxSenshuInfo.Controls.Add(this.txtDateYearFr);
            this.gbxSenshuInfo.Controls.Add(this.txtDateMonthFr);
            this.gbxSenshuInfo.Controls.Add(this.lblDateGengoFr);
            this.gbxSenshuInfo.Controls.Add(this.lblDateFr);
            this.gbxSenshuInfo.Controls.Add(this.lblKariukeShohizeiKamokuNm);
            this.gbxSenshuInfo.Controls.Add(this.lblKaribaraiShohizeiKamokuNm);
            this.gbxSenshuInfo.Controls.Add(this.lblShohizeiHasuShoriNm);
            this.gbxSenshuInfo.Controls.Add(this.lblShohizeiNyuryokuHohoNm);
            this.gbxSenshuInfo.Controls.Add(this.lblJigyoKubunNm);
            this.gbxSenshuInfo.Controls.Add(this.lblKojoHohoNm);
            this.gbxSenshuInfo.Controls.Add(this.lblKazeiHohoNm);
            this.gbxSenshuInfo.Controls.Add(this.label4);
            this.gbxSenshuInfo.Controls.Add(this.txtChihoShohizeiRitsu);
            this.gbxSenshuInfo.Controls.Add(this.lblChihoShohizeiRitsu);
            this.gbxSenshuInfo.Controls.Add(this.label3);
            this.gbxSenshuInfo.Controls.Add(this.label1);
            this.gbxSenshuInfo.Controls.Add(this.txtKariukeShohizeiKamokuCd);
            this.gbxSenshuInfo.Controls.Add(this.lblKariukeShohizeiKamoku);
            this.gbxSenshuInfo.Controls.Add(this.textBox14);
            this.gbxSenshuInfo.Controls.Add(this.txtKaribaraiShohizeiKamokuCd);
            this.gbxSenshuInfo.Controls.Add(this.txtShinShohizeiRitsu);
            this.gbxSenshuInfo.Controls.Add(this.txtKyuShohizeiRitsu);
            this.gbxSenshuInfo.Controls.Add(this.txtShohizeiHasuShori);
            this.gbxSenshuInfo.Controls.Add(this.txtShohizeiNyuryokuHoho);
            this.gbxSenshuInfo.Controls.Add(this.txtJigyoKubun);
            this.gbxSenshuInfo.Controls.Add(this.txtKojoHoho);
            this.gbxSenshuInfo.Controls.Add(this.txtKazeiHoho);
            this.gbxSenshuInfo.Controls.Add(this.lblKaribaraiShohizeiKamoku);
            this.gbxSenshuInfo.Controls.Add(this.lblShinShohizeiRitsu);
            this.gbxSenshuInfo.Controls.Add(this.lblKyuShohizeiRitsu);
            this.gbxSenshuInfo.Controls.Add(this.lblShohizeiHasuShori);
            this.gbxSenshuInfo.Controls.Add(this.lblShohizeiNyuryokuHoho);
            this.gbxSenshuInfo.Controls.Add(this.lblJigyoKubun);
            this.gbxSenshuInfo.Controls.Add(this.lblKojoHoho);
            this.gbxSenshuInfo.Controls.Add(this.lblKazeiHoho);
            this.gbxSenshuInfo.Controls.Add(this.label5);
            this.gbxSenshuInfo.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbxSenshuInfo.Location = new System.Drawing.Point(11, 39);
            this.gbxSenshuInfo.Name = "gbxSenshuInfo";
            this.gbxSenshuInfo.Size = new System.Drawing.Size(409, 311);
            this.gbxSenshuInfo.TabIndex = 3;
            this.gbxSenshuInfo.TabStop = false;
            // 
            // lblDateDayFr
            // 
            this.lblDateDayFr.BackColor = System.Drawing.Color.Silver;
            this.lblDateDayFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateDayFr.ForeColor = System.Drawing.Color.Black;
            this.lblDateDayFr.Location = new System.Drawing.Point(295, 20);
            this.lblDateDayFr.Name = "lblDateDayFr";
            this.lblDateDayFr.Size = new System.Drawing.Size(20, 18);
            this.lblDateDayFr.TabIndex = 959;
            this.lblDateDayFr.Text = "日";
            this.lblDateDayFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateMonthFr
            // 
            this.lblDateMonthFr.BackColor = System.Drawing.Color.Silver;
            this.lblDateMonthFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateMonthFr.ForeColor = System.Drawing.Color.Black;
            this.lblDateMonthFr.Location = new System.Drawing.Point(240, 20);
            this.lblDateMonthFr.Name = "lblDateMonthFr";
            this.lblDateMonthFr.Size = new System.Drawing.Size(15, 19);
            this.lblDateMonthFr.TabIndex = 957;
            this.lblDateMonthFr.Text = "月";
            this.lblDateMonthFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateYearFr
            // 
            this.lblDateYearFr.BackColor = System.Drawing.Color.Silver;
            this.lblDateYearFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateYearFr.ForeColor = System.Drawing.Color.Black;
            this.lblDateYearFr.Location = new System.Drawing.Point(187, 19);
            this.lblDateYearFr.Name = "lblDateYearFr";
            this.lblDateYearFr.Size = new System.Drawing.Size(17, 21);
            this.lblDateYearFr.TabIndex = 955;
            this.lblDateYearFr.Text = "年";
            this.lblDateYearFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDateDayFr
            // 
            this.txtDateDayFr.AutoSizeFromLength = false;
            this.txtDateDayFr.DisplayLength = null;
            this.txtDateDayFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateDayFr.ForeColor = System.Drawing.Color.Black;
            this.txtDateDayFr.Location = new System.Drawing.Point(262, 19);
            this.txtDateDayFr.MaxLength = 2;
            this.txtDateDayFr.Name = "txtDateDayFr";
            this.txtDateDayFr.Size = new System.Drawing.Size(30, 20);
            this.txtDateDayFr.TabIndex = 3;
            this.txtDateDayFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateDayFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateDayFr_Validating);
            // 
            // txtDateYearFr
            // 
            this.txtDateYearFr.AutoSizeFromLength = false;
            this.txtDateYearFr.DisplayLength = null;
            this.txtDateYearFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateYearFr.ForeColor = System.Drawing.Color.Black;
            this.txtDateYearFr.Location = new System.Drawing.Point(155, 19);
            this.txtDateYearFr.MaxLength = 2;
            this.txtDateYearFr.Name = "txtDateYearFr";
            this.txtDateYearFr.Size = new System.Drawing.Size(30, 20);
            this.txtDateYearFr.TabIndex = 1;
            this.txtDateYearFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateYearFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateYearFr_Validating);
            // 
            // txtDateMonthFr
            // 
            this.txtDateMonthFr.AutoSizeFromLength = false;
            this.txtDateMonthFr.DisplayLength = null;
            this.txtDateMonthFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDateMonthFr.ForeColor = System.Drawing.Color.Black;
            this.txtDateMonthFr.Location = new System.Drawing.Point(208, 19);
            this.txtDateMonthFr.MaxLength = 2;
            this.txtDateMonthFr.Name = "txtDateMonthFr";
            this.txtDateMonthFr.Size = new System.Drawing.Size(30, 20);
            this.txtDateMonthFr.TabIndex = 2;
            this.txtDateMonthFr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDateMonthFr.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateMonthFr_Validating);
            // 
            // lblDateGengoFr
            // 
            this.lblDateGengoFr.BackColor = System.Drawing.Color.Silver;
            this.lblDateGengoFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDateGengoFr.Enabled = false;
            this.lblDateGengoFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateGengoFr.ForeColor = System.Drawing.Color.Black;
            this.lblDateGengoFr.Location = new System.Drawing.Point(110, 19);
            this.lblDateGengoFr.Name = "lblDateGengoFr";
            this.lblDateGengoFr.Size = new System.Drawing.Size(41, 21);
            this.lblDateGengoFr.TabIndex = 952;
            this.lblDateGengoFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateFr
            // 
            this.lblDateFr.BackColor = System.Drawing.Color.Silver;
            this.lblDateFr.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDateFr.ForeColor = System.Drawing.Color.Black;
            this.lblDateFr.Location = new System.Drawing.Point(109, 16);
            this.lblDateFr.Name = "lblDateFr";
            this.lblDateFr.Size = new System.Drawing.Size(263, 27);
            this.lblDateFr.TabIndex = 953;
            this.lblDateFr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKariukeShohizeiKamokuNm
            // 
            this.lblKariukeShohizeiKamokuNm.BackColor = System.Drawing.Color.Silver;
            this.lblKariukeShohizeiKamokuNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKariukeShohizeiKamokuNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKariukeShohizeiKamokuNm.Location = new System.Drawing.Point(160, 274);
            this.lblKariukeShohizeiKamokuNm.Name = "lblKariukeShohizeiKamokuNm";
            this.lblKariukeShohizeiKamokuNm.Size = new System.Drawing.Size(223, 25);
            this.lblKariukeShohizeiKamokuNm.TabIndex = 950;
            this.lblKariukeShohizeiKamokuNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKaribaraiShohizeiKamokuNm
            // 
            this.lblKaribaraiShohizeiKamokuNm.BackColor = System.Drawing.Color.Silver;
            this.lblKaribaraiShohizeiKamokuNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKaribaraiShohizeiKamokuNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKaribaraiShohizeiKamokuNm.Location = new System.Drawing.Point(160, 248);
            this.lblKaribaraiShohizeiKamokuNm.Name = "lblKaribaraiShohizeiKamokuNm";
            this.lblKaribaraiShohizeiKamokuNm.Size = new System.Drawing.Size(223, 25);
            this.lblKaribaraiShohizeiKamokuNm.TabIndex = 949;
            this.lblKaribaraiShohizeiKamokuNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShohizeiHasuShoriNm
            // 
            this.lblShohizeiHasuShoriNm.BackColor = System.Drawing.Color.Silver;
            this.lblShohizeiHasuShoriNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShohizeiHasuShoriNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohizeiHasuShoriNm.Location = new System.Drawing.Point(136, 149);
            this.lblShohizeiHasuShoriNm.Name = "lblShohizeiHasuShoriNm";
            this.lblShohizeiHasuShoriNm.Size = new System.Drawing.Size(237, 25);
            this.lblShohizeiHasuShoriNm.TabIndex = 948;
            this.lblShohizeiHasuShoriNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShohizeiNyuryokuHohoNm
            // 
            this.lblShohizeiNyuryokuHohoNm.BackColor = System.Drawing.Color.Silver;
            this.lblShohizeiNyuryokuHohoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShohizeiNyuryokuHohoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohizeiNyuryokuHohoNm.Location = new System.Drawing.Point(136, 123);
            this.lblShohizeiNyuryokuHohoNm.Name = "lblShohizeiNyuryokuHohoNm";
            this.lblShohizeiNyuryokuHohoNm.Size = new System.Drawing.Size(237, 25);
            this.lblShohizeiNyuryokuHohoNm.TabIndex = 947;
            this.lblShohizeiNyuryokuHohoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblJigyoKubunNm
            // 
            this.lblJigyoKubunNm.BackColor = System.Drawing.Color.Silver;
            this.lblJigyoKubunNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblJigyoKubunNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJigyoKubunNm.Location = new System.Drawing.Point(136, 97);
            this.lblJigyoKubunNm.Name = "lblJigyoKubunNm";
            this.lblJigyoKubunNm.Size = new System.Drawing.Size(237, 25);
            this.lblJigyoKubunNm.TabIndex = 946;
            this.lblJigyoKubunNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKojoHohoNm
            // 
            this.lblKojoHohoNm.BackColor = System.Drawing.Color.Silver;
            this.lblKojoHohoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKojoHohoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKojoHohoNm.Location = new System.Drawing.Point(136, 71);
            this.lblKojoHohoNm.Name = "lblKojoHohoNm";
            this.lblKojoHohoNm.Size = new System.Drawing.Size(237, 25);
            this.lblKojoHohoNm.TabIndex = 945;
            this.lblKojoHohoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKazeiHohoNm
            // 
            this.lblKazeiHohoNm.BackColor = System.Drawing.Color.Silver;
            this.lblKazeiHohoNm.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKazeiHohoNm.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKazeiHohoNm.Location = new System.Drawing.Point(136, 45);
            this.lblKazeiHohoNm.Name = "lblKazeiHohoNm";
            this.lblKazeiHohoNm.Size = new System.Drawing.Size(237, 25);
            this.lblKazeiHohoNm.TabIndex = 944;
            this.lblKazeiHohoNm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Enabled = false;
            this.label4.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label4.Location = new System.Drawing.Point(349, 212);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(25, 25);
            this.label4.TabIndex = 943;
            this.label4.Text = "％";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtChihoShohizeiRitsu
            // 
            this.txtChihoShohizeiRitsu.AllowDrop = true;
            this.txtChihoShohizeiRitsu.AutoSizeFromLength = false;
            this.txtChihoShohizeiRitsu.BackColor = System.Drawing.Color.White;
            this.txtChihoShohizeiRitsu.DisplayLength = null;
            this.txtChihoShohizeiRitsu.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtChihoShohizeiRitsu.Location = new System.Drawing.Point(309, 215);
            this.txtChihoShohizeiRitsu.MaxLength = 4;
            this.txtChihoShohizeiRitsu.Name = "txtChihoShohizeiRitsu";
            this.txtChihoShohizeiRitsu.Size = new System.Drawing.Size(36, 20);
            this.txtChihoShohizeiRitsu.TabIndex = 19;
            this.txtChihoShohizeiRitsu.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtChihoShohizeiRitsu.Validating += new System.ComponentModel.CancelEventHandler(this.txtChihoShohizeiritsu);
            // 
            // lblChihoShohizeiRitsu
            // 
            this.lblChihoShohizeiRitsu.BackColor = System.Drawing.Color.Silver;
            this.lblChihoShohizeiRitsu.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblChihoShohizeiRitsu.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblChihoShohizeiRitsu.Location = new System.Drawing.Point(180, 213);
            this.lblChihoShohizeiRitsu.Name = "lblChihoShohizeiRitsu";
            this.lblChihoShohizeiRitsu.Size = new System.Drawing.Size(168, 25);
            this.lblChihoShohizeiRitsu.TabIndex = 18;
            this.lblChihoShohizeiRitsu.Text = "地 方 消 費 税 率";
            this.lblChihoShohizeiRitsu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Enabled = false;
            this.label3.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Location = new System.Drawing.Point(149, 212);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(25, 25);
            this.label3.TabIndex = 940;
            this.label3.Text = "％";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Enabled = false;
            this.label1.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Location = new System.Drawing.Point(149, 188);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(25, 25);
            this.label1.TabIndex = 939;
            this.label1.Text = "％";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBox14
            // 
            this.textBox14.AllowDrop = true;
            this.textBox14.AutoSizeFromLength = false;
            this.textBox14.DisplayLength = null;
            this.textBox14.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox14.Location = new System.Drawing.Point(919, -149);
            this.textBox14.MaxLength = 4;
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(50, 19);
            this.textBox14.TabIndex = 938;
            // 
            // txtKaribaraiShohizeiKamokuCd
            // 
            this.txtKaribaraiShohizeiKamokuCd.AllowDrop = true;
            this.txtKaribaraiShohizeiKamokuCd.AutoSizeFromLength = false;
            this.txtKaribaraiShohizeiKamokuCd.BackColor = System.Drawing.SystemColors.Control;
            this.txtKaribaraiShohizeiKamokuCd.DisplayLength = null;
            this.txtKaribaraiShohizeiKamokuCd.Enabled = false;
            this.txtKaribaraiShohizeiKamokuCd.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKaribaraiShohizeiKamokuCd.Location = new System.Drawing.Point(109, 250);
            this.txtKaribaraiShohizeiKamokuCd.MaxLength = 6;
            this.txtKaribaraiShohizeiKamokuCd.Name = "txtKaribaraiShohizeiKamokuCd";
            this.txtKaribaraiShohizeiKamokuCd.ReadOnly = true;
            this.txtKaribaraiShohizeiKamokuCd.Size = new System.Drawing.Size(47, 20);
            this.txtKaribaraiShohizeiKamokuCd.TabIndex = 21;
            this.txtKaribaraiShohizeiKamokuCd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtShinShohizeiRitsu
            // 
            this.txtShinShohizeiRitsu.AllowDrop = true;
            this.txtShinShohizeiRitsu.AutoSizeFromLength = false;
            this.txtShinShohizeiRitsu.BackColor = System.Drawing.Color.White;
            this.txtShinShohizeiRitsu.DisplayLength = null;
            this.txtShinShohizeiRitsu.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShinShohizeiRitsu.Location = new System.Drawing.Point(109, 215);
            this.txtShinShohizeiRitsu.MaxLength = 4;
            this.txtShinShohizeiRitsu.Name = "txtShinShohizeiRitsu";
            this.txtShinShohizeiRitsu.Size = new System.Drawing.Size(36, 20);
            this.txtShinShohizeiRitsu.TabIndex = 17;
            this.txtShinShohizeiRitsu.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShinShohizeiRitsu.Validating += new System.ComponentModel.CancelEventHandler(this.txtShinShohizeiRitsu_Validating);
            // 
            // txtKyuShohizeiRitsu
            // 
            this.txtKyuShohizeiRitsu.AllowDrop = true;
            this.txtKyuShohizeiRitsu.AutoSizeFromLength = false;
            this.txtKyuShohizeiRitsu.BackColor = System.Drawing.Color.White;
            this.txtKyuShohizeiRitsu.DisplayLength = null;
            this.txtKyuShohizeiRitsu.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKyuShohizeiRitsu.Location = new System.Drawing.Point(109, 189);
            this.txtKyuShohizeiRitsu.MaxLength = 4;
            this.txtKyuShohizeiRitsu.Name = "txtKyuShohizeiRitsu";
            this.txtKyuShohizeiRitsu.Size = new System.Drawing.Size(36, 20);
            this.txtKyuShohizeiRitsu.TabIndex = 15;
            this.txtKyuShohizeiRitsu.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKyuShohizeiRitsu.Validating += new System.ComponentModel.CancelEventHandler(this.txtKyuShohizeiRitsu_Validating);
            // 
            // txtShohizeiHasuShori
            // 
            this.txtShohizeiHasuShori.AllowDrop = true;
            this.txtShohizeiHasuShori.AutoSizeFromLength = false;
            this.txtShohizeiHasuShori.BackColor = System.Drawing.Color.White;
            this.txtShohizeiHasuShori.DisplayLength = null;
            this.txtShohizeiHasuShori.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShohizeiHasuShori.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtShohizeiHasuShori.Location = new System.Drawing.Point(109, 151);
            this.txtShohizeiHasuShori.MaxLength = 1;
            this.txtShohizeiHasuShori.Name = "txtShohizeiHasuShori";
            this.txtShohizeiHasuShori.Size = new System.Drawing.Size(22, 20);
            this.txtShohizeiHasuShori.TabIndex = 13;
            this.txtShohizeiHasuShori.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShohizeiHasuShori.Validating += new System.ComponentModel.CancelEventHandler(this.txtShohizeiHasuShori_Validating);
            // 
            // txtShohizeiNyuryokuHoho
            // 
            this.txtShohizeiNyuryokuHoho.AllowDrop = true;
            this.txtShohizeiNyuryokuHoho.AutoSizeFromLength = false;
            this.txtShohizeiNyuryokuHoho.BackColor = System.Drawing.SystemColors.Control;
            this.txtShohizeiNyuryokuHoho.DisplayLength = null;
            this.txtShohizeiNyuryokuHoho.Enabled = false;
            this.txtShohizeiNyuryokuHoho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtShohizeiNyuryokuHoho.ImeMode = System.Windows.Forms.ImeMode.On;
            this.txtShohizeiNyuryokuHoho.Location = new System.Drawing.Point(109, 125);
            this.txtShohizeiNyuryokuHoho.MaxLength = 1;
            this.txtShohizeiNyuryokuHoho.Name = "txtShohizeiNyuryokuHoho";
            this.txtShohizeiNyuryokuHoho.ReadOnly = true;
            this.txtShohizeiNyuryokuHoho.Size = new System.Drawing.Size(22, 20);
            this.txtShohizeiNyuryokuHoho.TabIndex = 11;
            this.txtShohizeiNyuryokuHoho.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtJigyoKubun
            // 
            this.txtJigyoKubun.AllowDrop = true;
            this.txtJigyoKubun.AutoSizeFromLength = false;
            this.txtJigyoKubun.BackColor = System.Drawing.Color.White;
            this.txtJigyoKubun.DisplayLength = null;
            this.txtJigyoKubun.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtJigyoKubun.Location = new System.Drawing.Point(109, 99);
            this.txtJigyoKubun.MaxLength = 1;
            this.txtJigyoKubun.Name = "txtJigyoKubun";
            this.txtJigyoKubun.Size = new System.Drawing.Size(22, 20);
            this.txtJigyoKubun.TabIndex = 9;
            this.txtJigyoKubun.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtJigyoKubun.Validating += new System.ComponentModel.CancelEventHandler(this.txtJigyoKubun_Validating);
            // 
            // txtKojoHoho
            // 
            this.txtKojoHoho.AllowDrop = true;
            this.txtKojoHoho.AutoSizeFromLength = false;
            this.txtKojoHoho.BackColor = System.Drawing.Color.White;
            this.txtKojoHoho.DisplayLength = null;
            this.txtKojoHoho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKojoHoho.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtKojoHoho.Location = new System.Drawing.Point(109, 73);
            this.txtKojoHoho.MaxLength = 1;
            this.txtKojoHoho.Name = "txtKojoHoho";
            this.txtKojoHoho.Size = new System.Drawing.Size(22, 20);
            this.txtKojoHoho.TabIndex = 7;
            this.txtKojoHoho.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKojoHoho.Validating += new System.ComponentModel.CancelEventHandler(this.txtKojoHoho_Validating);
            // 
            // txtKazeiHoho
            // 
            this.txtKazeiHoho.AllowDrop = true;
            this.txtKazeiHoho.AutoSizeFromLength = false;
            this.txtKazeiHoho.BackColor = System.Drawing.Color.White;
            this.txtKazeiHoho.DisplayLength = null;
            this.txtKazeiHoho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtKazeiHoho.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtKazeiHoho.Location = new System.Drawing.Point(109, 47);
            this.txtKazeiHoho.MaxLength = 1;
            this.txtKazeiHoho.Name = "txtKazeiHoho";
            this.txtKazeiHoho.Size = new System.Drawing.Size(22, 20);
            this.txtKazeiHoho.TabIndex = 5;
            this.txtKazeiHoho.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtKazeiHoho.Validating += new System.ComponentModel.CancelEventHandler(this.txtKazeiHoho_Validating);
            // 
            // lblKaribaraiShohizeiKamoku
            // 
            this.lblKaribaraiShohizeiKamoku.BackColor = System.Drawing.Color.Silver;
            this.lblKaribaraiShohizeiKamoku.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKaribaraiShohizeiKamoku.Enabled = false;
            this.lblKaribaraiShohizeiKamoku.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKaribaraiShohizeiKamoku.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblKaribaraiShohizeiKamoku.Location = new System.Drawing.Point(6, 248);
            this.lblKaribaraiShohizeiKamoku.Name = "lblKaribaraiShohizeiKamoku";
            this.lblKaribaraiShohizeiKamoku.Size = new System.Drawing.Size(153, 25);
            this.lblKaribaraiShohizeiKamoku.TabIndex = 20;
            this.lblKaribaraiShohizeiKamoku.Text = "仮払消費税科目";
            this.lblKaribaraiShohizeiKamoku.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShinShohizeiRitsu
            // 
            this.lblShinShohizeiRitsu.BackColor = System.Drawing.Color.Silver;
            this.lblShinShohizeiRitsu.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShinShohizeiRitsu.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShinShohizeiRitsu.Location = new System.Drawing.Point(6, 213);
            this.lblShinShohizeiRitsu.Name = "lblShinShohizeiRitsu";
            this.lblShinShohizeiRitsu.Size = new System.Drawing.Size(142, 25);
            this.lblShinShohizeiRitsu.TabIndex = 16;
            this.lblShinShohizeiRitsu.Text = "新 消 費 税 率";
            this.lblShinShohizeiRitsu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKyuShohizeiRitsu
            // 
            this.lblKyuShohizeiRitsu.BackColor = System.Drawing.Color.Silver;
            this.lblKyuShohizeiRitsu.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKyuShohizeiRitsu.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKyuShohizeiRitsu.Location = new System.Drawing.Point(6, 187);
            this.lblKyuShohizeiRitsu.Name = "lblKyuShohizeiRitsu";
            this.lblKyuShohizeiRitsu.Size = new System.Drawing.Size(142, 25);
            this.lblKyuShohizeiRitsu.TabIndex = 14;
            this.lblKyuShohizeiRitsu.Text = "旧 消 費 税 率";
            this.lblKyuShohizeiRitsu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShohizeiHasuShori
            // 
            this.lblShohizeiHasuShori.BackColor = System.Drawing.Color.Silver;
            this.lblShohizeiHasuShori.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShohizeiHasuShori.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohizeiHasuShori.Location = new System.Drawing.Point(6, 149);
            this.lblShohizeiHasuShori.Name = "lblShohizeiHasuShori";
            this.lblShohizeiHasuShori.Size = new System.Drawing.Size(128, 25);
            this.lblShohizeiHasuShori.TabIndex = 12;
            this.lblShohizeiHasuShori.Text = "消費税端数処理";
            this.lblShohizeiHasuShori.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblShohizeiNyuryokuHoho
            // 
            this.lblShohizeiNyuryokuHoho.BackColor = System.Drawing.Color.Silver;
            this.lblShohizeiNyuryokuHoho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblShohizeiNyuryokuHoho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblShohizeiNyuryokuHoho.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblShohizeiNyuryokuHoho.Location = new System.Drawing.Point(6, 123);
            this.lblShohizeiNyuryokuHoho.Name = "lblShohizeiNyuryokuHoho";
            this.lblShohizeiNyuryokuHoho.Size = new System.Drawing.Size(128, 25);
            this.lblShohizeiNyuryokuHoho.TabIndex = 10;
            this.lblShohizeiNyuryokuHoho.Text = "消費税入力方法";
            this.lblShohizeiNyuryokuHoho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblJigyoKubun
            // 
            this.lblJigyoKubun.BackColor = System.Drawing.Color.Silver;
            this.lblJigyoKubun.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblJigyoKubun.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblJigyoKubun.Location = new System.Drawing.Point(6, 97);
            this.lblJigyoKubun.Name = "lblJigyoKubun";
            this.lblJigyoKubun.Size = new System.Drawing.Size(128, 25);
            this.lblJigyoKubun.TabIndex = 8;
            this.lblJigyoKubun.Text = "主たる事業区分";
            this.lblJigyoKubun.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKojoHoho
            // 
            this.lblKojoHoho.BackColor = System.Drawing.Color.Silver;
            this.lblKojoHoho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKojoHoho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKojoHoho.Location = new System.Drawing.Point(6, 71);
            this.lblKojoHoho.Name = "lblKojoHoho";
            this.lblKojoHoho.Size = new System.Drawing.Size(128, 25);
            this.lblKojoHoho.TabIndex = 6;
            this.lblKojoHoho.Text = "控  除  方  法";
            this.lblKojoHoho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKazeiHoho
            // 
            this.lblKazeiHoho.BackColor = System.Drawing.Color.Silver;
            this.lblKazeiHoho.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblKazeiHoho.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblKazeiHoho.Location = new System.Drawing.Point(6, 45);
            this.lblKazeiHoho.Name = "lblKazeiHoho";
            this.lblKazeiHoho.Size = new System.Drawing.Size(128, 25);
            this.lblKazeiHoho.TabIndex = 4;
            this.lblKazeiHoho.Text = "課  税  方  法";
            this.lblKazeiHoho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Silver;
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label5.Font = new System.Drawing.Font("ＭＳ ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label5.Location = new System.Drawing.Point(6, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(142, 28);
            this.label5.TabIndex = 960;
            this.label5.Text = "適 用 開 始 日";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ZMCM1021
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(843, 642);
            this.Controls.Add(this.gbxSenshuInfo);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Name = "ZMCM1021";
            this.ShowFButton = true;
            this.Text = "ReportSample";
            this.Controls.SetChildIndex(this.gbxSenshuInfo, 0);
            this.Controls.SetChildIndex(this.pnlDebug, 0);
            this.Controls.SetChildIndex(this.lblTitle, 0);
            this.pnlDebug.ResumeLayout(false);
            this.gbxSenshuInfo.ResumeLayout(false);
            this.gbxSenshuInfo.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblKariukeShohizeiKamoku;
        private jp.co.fsi.common.controls.FsiTextBox txtKariukeShohizeiKamokuCd;
        private System.Windows.Forms.GroupBox gbxSenshuInfo;
        private System.Windows.Forms.Label lblShinShohizeiRitsu;
        private System.Windows.Forms.Label lblKyuShohizeiRitsu;
        private System.Windows.Forms.Label lblShohizeiHasuShori;
        private System.Windows.Forms.Label lblShohizeiNyuryokuHoho;
        private System.Windows.Forms.Label lblJigyoKubun;
        private System.Windows.Forms.Label lblKojoHoho;
        private System.Windows.Forms.Label lblKazeiHoho;
        private System.Windows.Forms.Label lblKaribaraiShohizeiKamoku;
        private jp.co.fsi.common.controls.FsiTextBox txtKazeiHoho;
        private jp.co.fsi.common.controls.FsiTextBox txtKaribaraiShohizeiKamokuCd;
        private jp.co.fsi.common.controls.FsiTextBox txtShinShohizeiRitsu;
        private jp.co.fsi.common.controls.FsiTextBox txtKyuShohizeiRitsu;
        private jp.co.fsi.common.controls.FsiTextBox txtShohizeiHasuShori;
        private jp.co.fsi.common.controls.FsiTextBox txtShohizeiNyuryokuHoho;
        private jp.co.fsi.common.controls.FsiTextBox txtJigyoKubun;
        private jp.co.fsi.common.controls.FsiTextBox txtKojoHoho;
        private jp.co.fsi.common.controls.FsiTextBox textBox14;
        private System.Windows.Forms.Label label4;
        private common.controls.FsiTextBox txtChihoShohizeiRitsu;
        private System.Windows.Forms.Label lblChihoShohizeiRitsu;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblKariukeShohizeiKamokuNm;
        private System.Windows.Forms.Label lblKaribaraiShohizeiKamokuNm;
        private System.Windows.Forms.Label lblShohizeiHasuShoriNm;
        private System.Windows.Forms.Label lblShohizeiNyuryokuHohoNm;
        private System.Windows.Forms.Label lblJigyoKubunNm;
        private System.Windows.Forms.Label lblKojoHohoNm;
        private System.Windows.Forms.Label lblKazeiHohoNm;
        private System.Windows.Forms.Label lblDateDayFr;
        private System.Windows.Forms.Label lblDateMonthFr;
        private System.Windows.Forms.Label lblDateYearFr;
        private common.controls.FsiTextBox txtDateDayFr;
        private common.controls.FsiTextBox txtDateYearFr;
        private common.controls.FsiTextBox txtDateMonthFr;
        private System.Windows.Forms.Label lblDateGengoFr;
        private System.Windows.Forms.Label lblDateFr;
        private System.Windows.Forms.Label label5;

    }
}