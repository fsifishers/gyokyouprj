﻿using jp.co.fsi.common;
using jp.co.fsi.common.util;
using jp.co.fsi.Login;
using jp.co.fsi.Menu;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace jp.co.fsi
{
	static class Program
	{
		/// <summary>
		/// アプリケーションのメイン エントリ ポイントです。
		/// </summary>
		[STAThread]
		static void Main()
		{
            //管理者として自分自身を起動する
            System.Diagnostics.ProcessStartInfo psi =
                new System.Diagnostics.ProcessStartInfo();
            //動詞に「runas」をつける
            psi.Verb = "runas";

			try
			{
				ConfigLoader cLdr = new ConfigLoader();

				string msgs = SSHConnection.SSHConnect();


				// Config.xmlの[UserInfo][doLogin]のフラグで起動画面を切り替える。
				if ("1" == cLdr.LoadCommonConfig("UserInfo", "dologin"))
				{
					Application.EnableVisualStyles();
					Application.SetCompatibleTextRenderingDefault(false);
					Application.Run(new FrmLogin());
				}
				else
				{
					Application.EnableVisualStyles();
					Application.SetCompatibleTextRenderingDefault(false);
					Application.Run(new FrmMenu());
				}

				SSHConnection.SSHDisConnect();

			}
			catch (Exception ex)
			{

				MessageBox.Show("起動時のエラー:" + ex.Message);

			}



		}
	}
}
